ALTER
ALGORITHM=UNDEFINED
DEFINER=`rCpp9uvJpztw`@`%`
SQL SECURITY DEFINER
VIEW `nscc_beauty_onduty_status` AS
select if(((`nbo`.`note_` = 'KOSONG') or (`nbo`.`note_` = 'ISHOMA')),timediff(now(),`nbo`.`time_start`),timediff(`nbo`.`estimate_end`,now())) AS `diff_`,`nbo`.`beauty_onduty_id` AS `beauty_onduty_id`,`nbo`.`beauty_id` AS `beauty_id`,`nbo`.`note_` AS `note_`,`nbo`.`tgl` AS `tgl`,cast(`nbo`.`time_start` as time) AS `time_start`,cast(`nbo`.`estimate_end` as time) AS `estimate_end`,`nb`.`nama_beauty` AS `nama_beauty`,`nb`.`gol_id` AS `gol_id`,`nb`.`kode_beauty` AS `kode_beauty`,(case `nbo`.`note_` when 'KOSONG' then 1 when 'ISHOMA' then 2 else 0 end) AS `urut`,sum(ifnull(`nbsl`.`qty`,0)) AS `total` from (((`nscc_beauty_onduty` `nbo` join `nscc_beauty` `nb` on((`nbo`.`beauty_id` = `nb`.`beauty_id`))) left join `nscc_beauty_services` `nbs` on(((`nbo`.`beauty_id` = `nbs`.`beauty_id`) and (`nbs`.`visible` = 1)))) left join `nscc_viewpembantu_beautyonduty` `nbsl` on((`nbsl`.`salestrans_details` = `nbs`.`salestrans_details`))) where (`nbo`.`tgl` = cast(now() as date)) group by `nbo`.`beauty_id` order by (case `nbo`.`note_` when 'KOSONG' then 1 when 'ISHOMA' then 2 else 0 end),cast(`nbo`.`time_start` as time) ;

CREATE
ALGORITHM=UNDEFINED
DEFINER=`rCpp9uvJpztw`@`%`
SQL SECURITY DEFINER
VIEW `nscc_viewpembantu_beautyonduty`AS
select `x`.`qty` AS `qty`,`x`.`salestrans_details` AS `salestrans_details` from (`nscc_salestrans_details` `x` join `nscc_salestrans` `s` on((`x`.`salestrans_id` = `s`.`salestrans_id`))) where (`s`.`tgl` = cast(now() as date)) group by `x`.`salestrans_details` ;

ALTER
ALGORITHM=UNDEFINED
DEFINER=`rCpp9uvJpztw`@`%`
SQL SECURITY DEFINER
VIEW `nscc_antrian_history_max` AS
SELECT
	`nah1`.`id_antrian` AS `id_antrian`,
	`nah1`.`bagian` AS `bagian`,
	max(`nah1`.`timestamp`) AS `waktu`
FROM
	`nscc_antrian_history` `nah1`
WHERE
	(
		cast(`nah1`.`timestamp` AS date) = cast(now() AS date)
	)
GROUP BY
	`nah1`.`id_antrian`,
	`nah1`.`bagian` ;