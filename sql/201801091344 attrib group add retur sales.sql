ALTER TABLE `nscc_grup_attr` ADD COLUMN `coa_jual_return`  varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `grup_id`;
DROP TRIGGER `nscc_grup_attr_before_insert`;
CREATE DEFINER=`rCpp9uvJpztw`@`%` TRIGGER `nscc_grup_attr_before_insert` BEFORE INSERT ON `nscc_grup_attr`
  FOR EACH ROW BEGIN
  IF NEW.grup_attr_id IS NULL OR LENGTH(NEW.grup_attr_id) = 0
  THEN
    SET NEW.grup_attr_id = UUID();
  END IF;
END;
DROP TRIGGER `nscc_grup_attr_before_update`;
CREATE DEFINER=`rCpp9uvJpztw`@`%` TRIGGER `nscc_grup_attr_before_update` BEFORE UPDATE ON `nscc_grup_attr`
  FOR EACH ROW BEGIN
  IF !(OLD.vat <=> NEW.vat AND OLD.tax <=> NEW.tax
       AND OLD.coa_jual <=> NEW.coa_jual AND OLD.coa_sales_disc <=> NEW.coa_sales_disc
       AND OLD.coa_sales_hpp <=> NEW.coa_sales_hpp AND OLD.coa_purchase <=> NEW.coa_purchase
       AND OLD.coa_purchase_return <=> NEW.coa_purchase_return AND OLD.coa_purchase_disc <=> NEW.coa_purchase_disc
       AND OLD.store <=> NEW.store AND OLD.grup_id <=> NEW.grup_id)
  THEN
    SET NEW.up = 0;
  END IF;
END;