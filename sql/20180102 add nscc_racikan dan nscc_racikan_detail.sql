CREATE TABLE `nscc_racikan` (
`racikan_id`  varchar(36) NOT NULL ,
`barang_id`  varchar(36) NOT NULL ,
`qty`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
`satuan`  varchar(15) NOT NULL ,
`note`  varchar(100) NULL DEFAULT NULL ,
`user_id`  varchar(50) NOT NULL ,
`tdate`  datetime NOT NULL ,
PRIMARY KEY (`racikan_id`)
)
ENGINE=InnoDB
ROW_FORMAT=Compact
;
CREATE TABLE `nscc_racikan_detail` (
`racikan_detail_id`  varchar(36) NOT NULL ,
`barang_id`  varchar(36) NOT NULL ,
`qty`  decimal(30,2) NOT NULL DEFAULT 0.00 ,
`satuan`  varchar(15) NOT NULL ,
`racikan_id`  varchar(36) NOT NULL ,
PRIMARY KEY (`racikan_detail_id`)
)
ENGINE=InnoDB
ROW_FORMAT=Compact
;