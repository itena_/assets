SET FOREIGN_KEY_CHECKS = 0;

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_antrian_history_max` AS
  SELECT
    nah1.id_antrian,
    nah1.bagian,
    MAX(nah1.`timestamp`) waktu
  FROM
    nscc_antrian_history AS nah1
  GROUP BY nah1.id_antrian, nah1.bagian;

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_antrian_history_group` AS
  SELECT nah.*
  FROM nscc_antrian_history AS nah
    INNER JOIN nscc_antrian_history_max a
      ON nah.id_antrian = a.id_antrian AND nah.bagian = a.bagian AND nah.`timestamp` = a.waktu;


CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_salestrans_header_all` AS
  SELECT
    ns.salestrans_id,
    ns.doc_ref,
    ns.store,
    ns.tgl,
    ns.customer_id,
    ns.add_durasi,
    ns.add_jasa,
    ns.total_durasi,
    ns.end_estimate,
    ns.pending,
    ns.end_at,
    ns.counter,
    ns.id_antrian,
    ns.tdate,
    ns.start_at
  FROM nscc_salestrans AS ns
  WHERE ns.type_ = 1
  UNION ALL
  SELECT
    ns.salestrans_id,
    ns.doc_ref_sales AS doc_ref,
    ns.store,
    ns.tgl,
    ns.customer_id,
    ns.add_durasi,
    ns.add_jasa,
    ns.total_durasi,
    ns.end_estimate,
    ns.pending,
    ns.end_at,
    ns.counter,
    ns.id_antrian,
    ns.tdate,
    ns.start_at
  FROM nscc_salestrans AS ns
  WHERE ns.type_ = -1;


CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_dokter` AS
  SELECT
    ne.employee_id   AS dokter_id,
    ne.nama_employee AS nama_dokter,
    ne.gol_id,
    ne.kode_employee AS kode_dokter,
    ne.active,
    ne.store,
    ne.up,
    ne.tipe
  FROM
    nscc_employees AS ne
    INNER JOIN nscc_tipe_employee AS nte ON ne.tipe = nte.tipe_employee_id
  WHERE
    nte.kode = 'DK';

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_beauty_service_list` AS
  SELECT
    a.salestrans_id,
    a.doc_ref,
    ng.kategori_id,
    a.tgl,
    nsd.salestrans_details,
    nb.barang_id,
    Sum(nsd.qty) AS qty,
    nsd.jasa_dokter,
    nsd.dokter_id,
    dok.active,
    nb.kode_barang,
    nb.nama_barang,
    a.store,
    nc.nama_customer,
    nc.no_customer,
    a.start_at,
    a.end_at,
    a.pending,
    a.end_estimate,
    a.total_durasi,
    a.customer_id,
    a.add_durasi,
    a.add_jasa,
    a.counter,
    a.id_antrian,
    a.tdate
  FROM nscc_salestrans_header_all AS a
    INNER JOIN nscc_salestrans_details AS nsd ON nsd.salestrans_id = a.salestrans_id
    LEFT JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
    LEFT JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
    LEFT JOIN nscc_customers nc ON a.customer_id = nc.customer_id
    LEFT JOIN nscc_dokter dok ON nsd.dokter_id = dok.dokter_id
  WHERE ng.kategori_id = 1
  GROUP BY a.doc_ref, nsd.barang_id
  HAVING qty <> 0;

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_beauty_service_view` AS
  SELECT
    nbsl.salestrans_id,
    nbsl.doc_ref,
    nbsl.kategori_id,
    nbsl.tgl,
    nbsl.salestrans_details,
    nbsl.barang_id,
    nbsl.qty,
    nbsl.jasa_dokter,
    nbsl.dokter_id,
    nbsl.active,
    nbsl.kode_barang,
    nbsl.nama_barang,
    nbsl.store,
    nbsl.nama_customer,
    nbsl.no_customer,
    nbsl.counter,
    nbsl.id_antrian,
    nbsl.tdate,
    nbsl.end_at,
    nbsl.start_at,
    MAX(CASE WHEN nbs.tipe = 1
      THEN nbs.beauty_id
        ELSE NULL END)                             beauty_id,
    MAX(CASE WHEN nbs.tipe = 2
      THEN nbs.beauty_id
        ELSE NULL END)                             beauty2_id,
    MAX(CASE WHEN nbs.tipe = 3
      THEN nbs.beauty_id
        ELSE NULL END)                             beauty3_id,
    MAX(CASE WHEN nbs.tipe = 4
      THEN nbs.beauty_id
        ELSE NULL END)                             beauty4_id,
    MAX(CASE WHEN nbs.tipe = 5
      THEN nbs.beauty_id
        ELSE NULL END)                             beauty5_id,
    COALESCE(IF(nbs.tipe = 1, nbs.final, NULL), IF(nbs.tipe = 2, nbs.final, NULL),
             IF(nbs.tipe = 3, nbs.final, NULL), IF(nbs.tipe = 4, nbs.final, NULL),
             IF(nbs.tipe = 5, nbs.final, NULL), 0) final
  FROM
    nscc_beauty_service_list AS nbsl
    LEFT JOIN nscc_beauty_services AS nbs ON nbs.salestrans_details = nbsl.salestrans_details AND nbs.visible = 1
  GROUP BY nbsl.salestrans_details;


CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_antrian_perawatan` AS
  SELECT
    aa.id_antrian,
    aa.nomor_pasien,
    aa.customer_id,
    aa.alasan,
    aa.nomor_antrian,
    aa.hold,
    aa.bagian,
    aa.counter,
    aa.tanggal,
    aa.`timestamp`,
    aa.medis,
    aa.end_,
    nbs.salestrans_id,
    nbs.doc_ref,
    nbs.tgl,
    nbs.barang_id,
    nbs.qty,
    nbs.jasa_dokter,
    nbs.dokter_id,
    nbs.kode_barang,
    nbs.nama_barang,
    nbs.nama_customer,
    nbs.no_customer,
    nbs.tdate,
    nbs.end_at,
    nbs.kategori_id
  FROM
    aisha_antrian AS aa
    LEFT JOIN nscc_beauty_service_view AS nbs
      ON nbs.id_antrian = aa.id_antrian AND (nbs.kategori_id IS NULL OR nbs.kategori_id = 1);
#GROUP BY nbs.salestrans_id ;

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_apoteker` AS
  SELECT
    ne.employee_id   AS apoteker_id,
    ne.nama_employee AS nama_apoteker,
    ne.gol_id,
    ne.kode_employee AS kode_apoteker,
    ne.active,
    ne.store,
    ne.up,
    ne.tipe
  FROM
    nscc_employees AS ne
    INNER JOIN nscc_tipe_employee AS nte ON ne.tipe = nte.tipe_employee_id
  WHERE
    nte.kode = 'APT';

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_bank_transfer` AS
  SELECT
    nbt.trans_no,
    nbt.ref,
    nbt.tgl              trans_date,
    nbt.amount,
    (SELECT nbt1.bank_id
     FROM nscc_bank_trans nbt1
     WHERE nbt1.type_ = 11 AND nbt1.amount <= 0 AND nbt1.trans_no = nbt.trans_no
     LIMIT 1)            bank_act_asal,
    COALESCE((SELECT nbt1.bank_id
              FROM nscc_bank_trans nbt1
              WHERE nbt1.type_ = 11 AND nbt1.amount >= 0 AND nbt1.trans_no = nbt.trans_no
              LIMIT 1, 1),
             (SELECT nbt1.bank_id
              FROM nscc_bank_trans nbt1
              WHERE nbt1.type_ = 11 AND nbt1.amount >= 0 AND nbt1.trans_no = nbt.trans_no
              LIMIT 1))  bank_act_tujuan,
    IFNULL((SELECT ABS(nbt1.amount)
            FROM nscc_bank_trans nbt1
            WHERE nbt1.type_ = 12 AND nbt1.trans_no = nbt.trans_no
            LIMIT 1), 0) charge,
    nc.memo_             memo,
    nbt.store
  FROM nscc_bank_trans AS nbt
    LEFT JOIN nscc_comments nc ON (nbt.trans_no = nc.type_no AND nbt.type_ = nc.type)
  WHERE nbt.type_ = 11 AND nbt.visible = 1
  GROUP BY trans_no;

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_barang_jual` AS
SELECT
  coalesce(`nj`.`price`, 0) AS `price`,
  `nb`.`barang_id`          AS `barang_id`,
  `nb`.`kode_barang`        AS `kode_barang`,
  `nb`.`nama_barang`        AS `nama_barang`,
  `nb`.`ket`                AS `ket`,
  `nb`.`grup_id`            AS `grup_id`,
  `nb`.`active`             AS `active`,
  `nb`.`sat`                AS `sat`,
  `nb`.`up`                 AS `up`,
  `nb`.`tipe_barang_id`     AS `tipe_barang_id`,
  nj.store
FROM (`nscc_barang` `nb` LEFT JOIN `nscc_jual` `nj` ON ((`nj`.`barang_id` = `nb`.`barang_id`)));

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_beauty` AS
  SELECT
    ne.employee_id   AS beauty_id,
    ne.nama_employee AS nama_beauty,
    ne.gol_id,
    ne.kode_employee AS kode_beauty,
    ne.active,
    ne.store,
    ne.up,
    ne.tipe
  FROM
    nscc_employees AS ne
    INNER JOIN nscc_tipe_employee AS nte ON ne.tipe = nte.tipe_employee_id
  WHERE
    nte.kode = 'BT';

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_beauty_offduty` AS
  SELECT
    nb.beauty_id,
    nb.nama_beauty,
    nb.gol_id,
    nb.kode_beauty,
    nb.active,
    nb.store,
    nb.up,
    nb.tipe,
    IF(nbo.note_ = 'KOSONG', 1, 0) off_
  FROM
    nscc_beauty AS nb
    LEFT JOIN nscc_beauty_onduty nbo ON nb.beauty_id = nbo.beauty_id AND nbo.tgl = DATE(NOW());

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_beauty_onduty_status` AS
  SELECT
    IF(nbo.note_ = 'KOSONG' OR nbo.note_ = 'ISHOMA', TIMEDIFF(NOW(), nbo.time_start),
       TIMEDIFF(nbo.estimate_end, NOW())) diff_,
    nbo.beauty_onduty_id,
    nbo.beauty_id,
    nbo.note_,
    nbo.tgl,
    TIME(nbo.time_start)                  time_start,
    TIME(nbo.estimate_end)                estimate_end,
    nb.nama_beauty,
    nb.gol_id,
    nb.kode_beauty,
    CASE nbo.note_
    WHEN 'KOSONG'
      THEN 1
    WHEN 'ISHOMA'
      THEN 2
    ELSE 0 END AS                         urut,
    SUM(IFNULL(nbsl.qty, 0))              total
  FROM
    nscc_beauty_onduty AS nbo
    INNER JOIN nscc_beauty AS nb ON nbo.beauty_id = nb.beauty_id
    LEFT JOIN nscc_beauty_services AS nbs ON nbo.beauty_id = nbs.beauty_id AND nbs.visible = 1
    LEFT JOIN nscc_beauty_service_list AS nbsl
      ON nbs.salestrans_details = nbsl.salestrans_details AND nbsl.tgl = DATE(NOW())
  WHERE nbo.tgl = DATE(NOW())
  GROUP BY nbo.beauty_id
  ORDER BY urut, time_start;

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_clinical_in` AS
SELECT
  `nttc`.`tipe_clinical_id` AS `tipe_clinical_id`,
  `nttc`.`nama`             AS `nama`,
  `nttc`.`up`               AS `up`,
  `nttc`.`arus`             AS `arus`
FROM `nscc_tipe_trans_clinical` `nttc`
WHERE (`nttc`.`arus` = 1);

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_clinical_out` AS
SELECT
  `nttc`.`tipe_clinical_id` AS `tipe_clinical_id`,
  `nttc`.`nama`             AS `nama`,
  `nttc`.`up`               AS `up`,
  `nttc`.`arus`             AS `arus`
FROM `nscc_tipe_trans_clinical` `nttc`
WHERE (`nttc`.`arus` = -(1));

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_customers_point` AS
SELECT
  `nc`.`customer_id`            AS `customer_id`,
  `nc`.`nama_customer`          AS `nama_customer`,
  `nc`.`no_customer`            AS `no_customer`,
  `nc`.`tempat_lahir`           AS `tempat_lahir`,
  `nc`.`tgl_lahir`              AS `tgl_lahir`,
  `nc`.`email`                  AS `email`,
  `nc`.`telp`                   AS `telp`,
  `nc`.`alamat`                 AS `alamat`,
  `nc`.`awal`                   AS `awal`,
  `nc`.`akhir`                  AS `akhir`,
  `nc`.`store`                  AS `store`,
  `nc`.`kecamatan_id`           AS `kecamatan_id`,
  `nc`.`kota_id`                AS `kota_id`,
  `nc`.`provinsi_id`            AS `provinsi_id`,
  `nc`.`negara_id`              AS `negara_id`,
  `nc`.`status_cust_id`         AS `status_cust_id`,
  `nc`.`sex`                    AS `sex`,
  `nc`.`kerja`                  AS `kerja`,
  coalesce(sum(`npt`.`qty`), 0) AS `point`
FROM (`nscc_customers` `nc` LEFT JOIN `nscc_point_trans` `npt` ON ((`nc`.`customer_id` = `npt`.`customer_id`)))
GROUP BY `nc`.`customer_id`, `nc`.`nama_customer`, `nc`.`no_customer`, `nc`.`tempat_lahir`, `nc`.`tgl_lahir`,
  `nc`.`email`, `nc`.`telp`, `nc`.`alamat`, `nc`.`awal`, `nc`.`akhir`, `nc`.`store`, `nc`.`kecamatan_id`,
  `nc`.`kota_id`, `nc`.`provinsi_id`, `nc`.`negara_id`, `nc`.`status_cust_id`, `nc`.`sex`, `nc`.`kerja`;

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_diskon_view` AS
  SELECT
    nd.diskon_id,
    nd.ref_id,
    nd.barang_id,
    nb.kode_barang,
    nama_barang,
    nd.`value`,
    nd.store,
    nd.type_,
    COALESCE(nsc.nama_status, np.nama_promosi) nama_diskon
  FROM nscc_diskon AS nd
    LEFT JOIN nscc_status_cust AS nsc ON nd.ref_id = nsc.status_cust_id AND nd.type_ = 0
    LEFT JOIN nscc_promosi AS np ON nd.ref_id = np.promosi_id AND nd.type_ = 1
    LEFT JOIN nscc_barang AS nb ON nb.barang_id = nd.barang_id;


ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_dropping_sisa` AS
SELECT
  npd.qty - COALESCE(Sum(ntbd.qty), 0) AS qty,
  npd.barang_id,
  npd.dropping_id
FROM nscc_dropping_details AS npd
  LEFT JOIN nscc_receive_dropping AS ntb ON npd.dropping_id = ntb.dropping_id AND npd.visible = 1
  LEFT JOIN nscc_receive_dropping_details AS ntbd ON ntbd.receive_dropping_id = ntb.receive_dropping_id
                                                     AND ntbd.barang_id = npd.barang_id AND ntbd.visible = 1 AND
                                                     npd.visible = 1
GROUP BY npd.barang_id, ntb.dropping_id
HAVING qty > 0;

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_fo` AS
  SELECT
    ne.employee_id   AS fo_id,
    ne.nama_employee AS nama_fo,
    ne.gol_id,
    ne.kode_employee AS kode_fo,
    ne.active,
    ne.store,
    ne.up,
    ne.tipe
  FROM
    nscc_employees AS ne
    INNER JOIN nscc_tipe_employee AS nte ON ne.tipe = nte.tipe_employee_id
  WHERE
    nte.kode = 'FO';

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_konsul_salestrans` AS
  SELECT
    nkd.konsul_detil_id,
    nkd.barang_id,
    nkd.qty,
    nj.price,
    nj.disc,
    nj.discrp,
    nj.store,
    nkd.ketpot,
    nkd.konsul_id
  FROM
    nscc_konsul_detil AS nkd
    INNER JOIN nscc_jual AS nj ON nkd.barang_id = nj.barang_id;

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_mgm_view` AS
SELECT
  `nm`.`mgm_id`             AS `mgm_id`,
  `nm`.`customer_new_id`    AS `customer_new_id`,
  `nm`.`customer_friend_id` AS `customer_friend_id`,
  `nm`.`salestrans_id`      AS `salestrans_id`,
  `nm`.`tdate`              AS `tdate`,
  `nm`.`id_user`            AS `id_user`,
  `nm`.`up`                 AS `up`,
  `nm`.`tgl`                AS `tgl`,
  `nm`.`doc_ref`            AS `doc_ref`,
  `nm`.`store`              AS `store`,
  `nc1`.`nama_customer`     AS `customer_new_name`,
  `nc2`.`nama_customer`     AS `customer_friend_name`,
  `ns`.`doc_ref`            AS `sales_doc_ref`
FROM (((`nscc_mgm` `nm`
  JOIN `nscc_customers` `nc1` ON ((`nm`.`customer_new_id` = `nc1`.`customer_id`))) JOIN `nscc_customers` `nc2`
    ON ((`nm`.`customer_friend_id` = `nc2`.`customer_id`))) JOIN `nscc_salestrans` `ns`
    ON ((`nm`.`salestrans_id` = `ns`.`salestrans_id`)));

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_monitor_antrian` AS
  SELECT
    aa.id_antrian,
    aa.nomor_pasien,
    aa.nomor_antrian,
    aa.tanggal,
    aa.counter                                                           counter_,
    MAX(CASE nahg.bagian
        WHEN 'daftar'
          THEN CONCAT('[', TIME(nahg.timestamp), '] ', nahg.action) END) daftar,
    MAX(CASE nahg.bagian
        WHEN 'counter'
          THEN CONCAT('[', TIME(nahg.timestamp), '] ', nahg.action) END) counter,
    MAX(CASE nahg.bagian
        WHEN 'medis'
          THEN CONCAT('[', TIME(nahg.timestamp), '] ', nahg.action) END) konsul,
    MAX(CASE nahg.bagian
        WHEN 'kasir'
          THEN CONCAT('[', TIME(nahg.timestamp), '] ', nahg.action) END) kasir,
    aa.alasan
  FROM
    aisha_antrian AS aa
    LEFT JOIN nscc_antrian_history_group AS nahg ON nahg.id_antrian = aa.id_antrian
  GROUP BY aa.id_antrian;

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_msd_view` AS
SELECT
  `nm`.`msd_id`        AS `msd_id`,
  `nm`.`customer_id`   AS `customer_id`,
  `nm`.`salestrans_id` AS `salestrans_id`,
  `nm`.`tdate`         AS `tdate`,
  `nm`.`id_user`       AS `id_user`,
  `nm`.`up`            AS `up`,
  `nm`.`tgl`           AS `tgl`,
  `nm`.`doc_ref`       AS `doc_ref`,
  `nm`.`store`         AS `store`,
  `nc`.`nama_customer` AS `nama_customer`,
  `ns`.`doc_ref`       AS `doc_ref_sales`
FROM ((`nscc_msd` `nm`
  JOIN `nscc_customers` `nc` ON ((`nm`.`customer_id` = `nc`.`customer_id`))) JOIN `nscc_salestrans` `ns`
    ON ((`nm`.`salestrans_id` = `ns`.`salestrans_id`)));

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_narstrans_view` AS
SELECT
  `ns`.`tgl`           AS `tgl`,
  `ns`.`salestrans_id` AS `salestrans_id`,
  `ns`.`doc_ref`       AS `doc_ref`,
  `ns`.`tdate`         AS `tdate`,
  `ns`.`user_id`       AS `user_id`,
  `ns`.`bruto`         AS `bruto`,
  `ns`.`disc`          AS `disc`,
  `ns`.`discrp`        AS `discrp`,
  `ns`.`totalpot`      AS `totalpot`,
  `ns`.`total`         AS `total`,
  `ns`.`ketdisc`       AS `ketdisc`,
  `ns`.`vat`           AS `vat`,
  `ns`.`customer_id`   AS `customer_id`,
  `ns`.`doc_ref_sales` AS `doc_ref_sales`,
  `ns`.`audit`         AS `audit`,
  `ns`.`store`         AS `store`,
  `ns`.`printed`       AS `printed`,
  `ns`.`override`      AS `override`,
  `ns`.`bayar`         AS `bayar`,
  `ns`.`kembali`       AS `kembali`,
  `ns`.`dokter_id`     AS `dokter_id`,
  `ns`.`log`           AS `log`,
  `ns`.`up`            AS `up`,
  `ns`.`total_discrp1` AS `total_discrp1`,
  `ns`.`type_`         AS `type_`,
  `nc`.`nama_customer` AS `nama_customer`,
  `nc`.`no_customer`   AS `no_customer`,
  `ns`.`rounding`      AS `rounding`
FROM (`nscc_salestrans` `ns`
  JOIN `nscc_customers` `nc` ON ((`ns`.`customer_id` = `nc`.`customer_id`)))
WHERE (`ns`.`type_` = 1)
UNION SELECT
        `ns`.`tgl`           AS `tgl`,
        `ns`.`salestrans_id` AS `salestrans_id`,
        `ns`.`doc_ref_sales` AS `doc_ref`,
        `ns`.`tdate`         AS `tdate`,
        `ns`.`user_id`       AS `user_id`,
        `ns`.`bruto`         AS `bruto`,
        `ns`.`disc`          AS `disc`,
        `ns`.`discrp`        AS `discrp`,
        `ns`.`totalpot`      AS `totalpot`,
        `ns`.`total`         AS `total`,
        `ns`.`ketdisc`       AS `ketdisc`,
        `ns`.`vat`           AS `vat`,
        `ns`.`customer_id`   AS `customer_id`,
        `ns`.`doc_ref_sales` AS `doc_ref_sales`,
        `ns`.`audit`         AS `audit`,
        `ns`.`store`         AS `store`,
        `ns`.`printed`       AS `printed`,
        `ns`.`override`      AS `override`,
        `ns`.`bayar`         AS `bayar`,
        `ns`.`kembali`       AS `kembali`,
        `ns`.`dokter_id`     AS `dokter_id`,
        `ns`.`log`           AS `log`,
        `ns`.`up`            AS `up`,
        `ns`.`total_discrp1` AS `total_discrp1`,
        `ns`.`type_`         AS `type_`,
        `nc`.`nama_customer` AS `nama_customer`,
        `nc`.`no_customer`   AS `no_customer`,
        `ns`.`rounding`      AS `rounding`
      FROM (`nscc_salestrans` `ns`
        JOIN `nscc_customers` `nc` ON ((`ns`.`customer_id` = `nc`.`customer_id`)))
      WHERE (`ns`.`type_` = -(1));

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_order_dropping_sisa` AS
SELECT
  npd.qty - COALESCE(Sum(ntbd.qty), 0) AS qty,
  npd.barang_id,
  npd.order_dropping_id
FROM nscc_order_dropping_details AS npd
  LEFT JOIN nscc_dropping AS ntb ON npd.order_dropping_id = ntb.order_dropping_id AND npd.visible = 1
  LEFT JOIN nscc_dropping_details AS ntbd ON ntbd.dropping_id = ntb.dropping_id
                                             AND ntbd.barang_id = npd.barang_id AND ntbd.visible = 1 AND npd.visible = 1
GROUP BY npd.barang_id, ntb.order_dropping_id
HAVING qty > 0;

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_paket_barang_cmp` AS
SELECT
  `nb`.`barang_id`         AS `barang_id`,
  `nb`.`kode_barang`       AS `kode_barang`,
  `nb`.`nama_barang`       AS `nama_barang`,
  `nb`.`ket`               AS `ket`,
  `nb`.`grup_id`           AS `grup_id`,
  `nb`.`active`            AS `active`,
  `nb`.`sat`               AS `sat`,
  `nb`.`up`                AS `up`,
  `nb`.`tipe_barang_id`    AS `tipe_barang_id`,
  `npb`.`paket_details_id` AS `paket_details_id`,
  `npb`.`disc`             AS `disc`
FROM (`nscc_paket_barang` `npb`
  JOIN `nscc_barang` `nb` ON ((`npb`.`barang_id` = `nb`.`barang_id`)));

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_paket_details_trans` AS
SELECT
  `npd`.`paket_details_id` AS `paket_details_id`,
  `npd`.`kategori_name`    AS `kategori_name`,
  `npd`.`paket_id`         AS `paket_id`,
  `npb`.`barang_id`        AS `barang_id`,
  `npb`.`disc`             AS `disc`
FROM (`nscc_paket_details` `npd` LEFT JOIN `nscc_paket_barang` `npb`
    ON ((`npb`.`paket_details_id` = `npd`.`paket_details_id`)))
GROUP BY `npb`.`paket_details_id`;

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_pelunasan_sisa_utang` AS
SELECT
  nti.transfer_item_id,
  nti.tgl,
  nti.doc_ref_other                                                            no_faktur,
  nti.total                                                                    nilai,
  nti.doc_ref,
  nti.total - COALESCE(-nti1.total, 0) - COALESCE(SUM(npud.kas_dibayar), 0) AS sisa,
  nti.supplier_id,
  nti.store
FROM nscc_transfer_item nti
  LEFT JOIN nscc_pelunasan_utang_detil npud ON nti.transfer_item_id = npud.transfer_item_id
  LEFT JOIN nscc_transfer_item nti1 ON nti.doc_ref = nti1.doc_ref_other AND nti1.type_ = 1
GROUP BY nti.transfer_item_id
HAVING sisa != 0;

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_perawatan_tambahan` AS
  SELECT
    aa.id_antrian,
    nbs.salestrans_id,
    nbs.doc_ref,
    nbs.tgl,
    nbs.nama_customer,
    nbs.no_customer,
    nbs.tdate,
    nbs.start_at,
    nbs.end_at
  FROM
    nscc_beauty_service_view AS nbs
    LEFT JOIN aisha_antrian AS aa ON nbs.id_antrian = aa.id_antrian
  WHERE aa.id_antrian IS NULL OR aa.id_antrian = '' #AND nbs.tgl = DATE(NOW())
  GROUP BY salestrans_id;

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_po_sisa_terima` AS
SELECT
  npd.qty - COALESCE(Sum(ntbd.qty), 0) AS qty,
  npd.barang_id,
  npd.po_id
FROM nscc_po_details AS npd
  LEFT JOIN nscc_terima_barang AS ntb ON npd.po_id = ntb.po_id AND npd.visible = 1
  LEFT JOIN nscc_terima_barang_details AS ntbd ON ntbd.terima_barang_id = ntb.terima_barang_id
                                                  AND ntbd.barang_id = npd.barang_id AND ntbd.visible = 1 AND
                                                  npd.visible = 1
GROUP BY npd.barang_id, ntb.po_id
HAVING qty > 0;

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_point_in` AS
SELECT
  `npg`.`promo_generic_id` AS `promo_generic_id`,
  `npg`.`customer_id`      AS `customer_id`,
  `npg`.`event_id`         AS `event_id`,
  `npg`.`qty`              AS `qty`,
  `npg`.`tdate`            AS `tdate`,
  `npg`.`id_user`          AS `id_user`,
  `npg`.`doc_ref`          AS `doc_ref`,
  `npg`.`note`             AS `note`,
  `npg`.`store`            AS `store`,
  `npg`.`tgl`              AS `tgl`,
  `nc`.`nama_customer`     AS `nama_customer`
FROM (`nscc_promo_generic` `npg`
  JOIN `nscc_customers` `nc` ON ((`npg`.`customer_id` = `nc`.`customer_id`)))
WHERE (`npg`.`qty` > 0);

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_point_out` AS
SELECT
  `npg`.`promo_generic_id` AS `promo_generic_id`,
  `npg`.`customer_id`      AS `customer_id`,
  `npg`.`event_id`         AS `event_id`,
  -(`npg`.`qty`)           AS `qty`,
  `npg`.`tdate`            AS `tdate`,
  `npg`.`id_user`          AS `id_user`,
  `npg`.`doc_ref`          AS `doc_ref`,
  `npg`.`note`             AS `note`,
  `npg`.`store`            AS `store`,
  `npg`.`tgl`              AS `tgl`,
  `nc`.`nama_customer`     AS `nama_customer`
FROM (`nscc_promo_generic` `npg`
  JOIN `nscc_customers` `nc` ON ((`npg`.`customer_id` = `nc`.`customer_id`)))
WHERE (`npg`.`qty` < 0);

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_rpg` AS
SELECT
  `nu`.`ulpt_id`        AS `rpg_id`,
  `nu`.`doc_ref`        AS `doc_ref`,
  `nu`.`salestrans_id`  AS `salestrans_id`,
  `nu`.`tdate`          AS `tdate`,
  `nu`.`id_user`        AS `id_user`,
  `nu`.`tgl`            AS `tgl`,
  -(`nu`.`amount`)      AS `amount`,
  `nu`.`note`           AS `note`,
  `nu`.`bank_id`        AS `bank_id`,
  `nu`.`account_code`   AS `account_code`,
  `nu`.`lunas`          AS `lunas`,
  `nu`.`parent_id`      AS `parent_id`,
  `nu`.`tipe`           AS `tipe`,
  `nu`.`store`          AS `store`,
  `nuv`.`nama_customer` AS `nama_customer`,
  `nuv`.`doc_ref`       AS `ulpt_doc_ref`,
  `nuv`.`salestrans_id` AS `ulpt_salestrans_id`,
  `nuv`.`customer_id`   AS `customer_id`
FROM (`nscc_ulpt` `nu` LEFT JOIN `nscc_ulpt_view` `nuv` ON ((`nu`.`parent_id` = `nuv`.`ulpt_id`)))
WHERE (`nu`.`tipe` = 'R');


ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_salestrans_view` AS
SELECT
  ns.tgl           AS tgl,
  ns.salestrans_id AS salestrans_id,
  ns.doc_ref       AS doc_ref,
  ns.tdate         AS tdate,
  ns.user_id       AS user_id,
  ns.bruto         AS bruto,
  ns.disc          AS disc,
  ns.discrp        AS discrp,
  ns.totalpot      AS totalpot,
  ns.total         AS total,
  ns.ketdisc       AS ketdisc,
  ns.vat           AS vat,
  ns.customer_id   AS customer_id,
  ns.doc_ref_sales AS doc_ref_sales,
  ns.audit         AS audit,
  ns.store         AS store,
  ns.printed       AS printed,
  ns.override      AS override,
  ns.bayar         AS bayar,
  ns.kembali       AS kembali,
  ns.dokter_id     AS dokter_id,
  ns.log           AS log,
  ns.up            AS up,
  ns.total_discrp1 AS total_discrp1,
  ns.type_         AS type_,
  nc.nama_customer AS nama_customer,
  nc.no_customer   AS no_customer,
  ns.rounding      AS rounding,
  ns.total_durasi,
  ns.end_estimate,
  ns.konsul_id,
  ns.pending,
  ns.end_at,
  ns.start_at,
  ns.kmr_total,
  ns.kmr_persen,
  ns.parent_id,
  ns.retur_note,
  ns.add_durasi,
  ns.add_jasa,
  ns.counter,
  ns.id_antrian
FROM
  (
      `nscc_salestrans` `ns`
      JOIN `nscc_customers` `nc` ON (
      (
        `ns`.`customer_id` = `nc`.`customer_id`
      )
      )
  )
WHERE
  (`ns`.`type_` = 1);

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_salestrans_details_barang` AS
  SELECT
    nsv.tgl,
    nsv.salestrans_id,
    nsv.doc_ref,
    nsv.tdate,
    nsv.user_id,
    nsv.bruto,
    nsv.disc,
    nsv.discrp,
    nsv.totalpot,
    nsv.total,
    nsv.ketdisc,
    nsv.vat,
    nsv.customer_id,
    nsv.doc_ref_sales,
    nsv.audit,
    nsv.store,
    nsv.printed,
    nsv.override,
    nsv.bayar,
    nsv.kembali,
    nsv.dokter_id,
    nsv.log,
    nsv.up,
    nsv.total_discrp1,
    nsv.type_,
    nsv.nama_customer,
    nsv.no_customer,
    nsv.rounding,
    nsd.salestrans_details,
    nsd.barang_id,
    nsd.qty,
    nsd.disc      AS d_disc,
    nsd.discrp    AS d_discrp,
    nsd.ketpot,
    nsd.vat       AS d_vat,
    nsd.vatrp,
    nsd.bruto     AS d_bruto,
    nsd.total     AS d_total,
    nsd.total_pot,
    nsd.price,
    nsd.jasa_dokter,
    nsd.dokter_id AS d_dokter_id,
    nsd.disc_name,
    nsd.hpp,
    nsd.cost,
    nsd.disc1,
    nsd.discrp1,
    nsd.paket_trans_id,
    nsd.paket_details_id,
    nsd.durasi,
    nsd.konsul_detil_id,
    nb.kode_barang,
    nb.nama_barang,
    nb.ket,
    nb.grup_id,
    nb.active,
    nb.sat,
    nb.tipe_barang_id,
    nb.barcode,
    ng.nama_grup,
    ng.kategori_id,
    nsv.total_durasi,
    nsv.end_estimate,
    nsv.konsul_id,
    nsv.pending,
    nsv.end_at,
    nsv.start_at,
    nsv.kmr_total,
    nsv.kmr_persen,
    nsv.parent_id,
    nsv.retur_note,
    nsv.counter,
    nsv.add_jasa,
    nsv.add_durasi,
    nsv.id_antrian
  FROM
    nscc_salestrans_view AS nsv
    INNER JOIN nscc_salestrans_details AS nsd ON nsd.salestrans_id = nsv.salestrans_id
    INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
    INNER JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id;

CREATE
  ALGORITHM = UNDEFINED
  SQL SECURITY DEFINER
VIEW `nscc_salestrans_details_view` AS
  SELECT
    nsd.salestrans_details,
    nsd.barang_id,
    nsd.salestrans_id,
    nsd.qty,
    nsd.disc,
    nsd.discrp,
    nsd.ketpot,
    nsd.vat,
    nsd.vatrp,
    nsd.bruto,
    nsd.total,
    nsd.total_pot,
    nsd.price,
    nsd.jasa_dokter,
    nsd.dokter_id,
    disc_name,
    paket_trans_id,
    paket_details_id,
    MAX(CASE WHEN nbs.tipe = 1
      THEN nbs.beauty_id
        ELSE NULL END) beauty_id,
    MAX(CASE WHEN nbs.tipe = 2
      THEN nbs.beauty_id
        ELSE NULL END) beauty2_id,
    nb.kode_barang,
    nb.nama_barang
  FROM
    nscc_barang nb
    INNER JOIN nscc_salestrans_details nsd ON nb.barang_id = nsd.barang_id
    LEFT JOIN nscc_beauty_services nbs ON nbs.salestrans_details = nsd.salestrans_details
  GROUP BY nsd.salestrans_details;

ALTER
ALGORITHM = UNDEFINED
DEFINER =`root`@`127.0.0.1`
SQL SECURITY DEFINER
VIEW `nscc_ulpt_view` AS
SELECT
  `nu`.`ulpt_id`        AS `ulpt_id`,
  `nu`.`doc_ref`        AS `doc_ref`,
  `nu`.`salestrans_id`  AS `salestrans_id`,
  `nu`.`tdate`          AS `tdate`,
  `nu`.`id_user`        AS `id_user`,
  `nu`.`tgl`            AS `tgl`,
  `nu`.`amount`         AS `amount`,
  `nu`.`note`           AS `note`,
  `nu`.`bank_id`        AS `bank_id`,
  `nu`.`account_code`   AS `account_code`,
  `nu`.`lunas`          AS `lunas`,
  `nu`.`parent_id`      AS `parent_id`,
  `nsv`.`customer_id`   AS `customer_id`,
  `nsv`.`nama_customer` AS `nama_customer`,
  `nu`.`tipe`           AS `tipe`,
  `nu`.`store`          AS `store`,
  `nu`.`card_id`        AS `card_id`,
  `nsv`.`no_customer`   AS `no_customer`,
  `nu`.`tipe_ulpt`      AS `tipe_ulpt`
FROM (`nscc_ulpt` `nu` LEFT JOIN `nscc_salestrans_view` `nsv` ON ((`nu`.`salestrans_id` = `nsv`.`salestrans_id`)))
WHERE (`nu`.`tipe` = 'U');

DROP VIEW `nscc_transaksi_absen`;

SET FOREIGN_KEY_CHECKS = 1;

