/*
Navicat MariaDB Data Transfer

Source Server         : SERVER NATASHA
Source Server Version : 100124
Source Host           : 10.0.0.131:3306
Source Database       : posnsc01

Target Server Type    : MariaDB
Target Server Version : 100124
File Encoding         : 65001

Date: 2018-04-05 15:59:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for nscc_asset_periode
-- ----------------------------

CREATE TABLE IF NOT EXISTS `nscc_asset_periode` (
  `asset_periode_id` varchar(50) NOT NULL,
  `asset_trans_id` varchar(50) NOT NULL,
  `asset_id` varchar(50) DEFAULT NULL,
  `asset_group_id` varchar(50) DEFAULT NULL,
  `docref` varchar(50) DEFAULT NULL,
  `docref_other` varchar(50) DEFAULT NULL,
  `asset_trans_name` varchar(100) DEFAULT NULL,
  `asset_trans_branch` varchar(100) DEFAULT NULL,
  `asset_trans_price` varchar(100) DEFAULT NULL,
  `asset_trans_new_price` varchar(50) DEFAULT NULL,
  `asset_trans_date` varchar(100) DEFAULT NULL,
  `description` tinytext,
  `class` varchar(50) DEFAULT NULL,
  `period` varchar(50) DEFAULT NULL,
  `tariff` varchar(50) DEFAULT NULL,
  `tglpenyusutan` varchar(50) DEFAULT NULL,
  `penyusutanperbulan` varchar(50) DEFAULT NULL,
  `penyusutanpertahun` varchar(50) DEFAULT NULL,
  `balance` varchar(50) DEFAULT NULL,
  `status` enum('1','0') DEFAULT NULL,
  `startdate` varchar(50) DEFAULT NULL,
  `enddate` varchar(50) DEFAULT NULL,
  `barang_id` varchar(50) DEFAULT NULL,
  `ati` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`asset_periode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
