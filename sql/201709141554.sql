/*
	Purchasing dan Warehouse
*/

ALTER TABLE `nscc_dropping` MODIFY COLUMN `tdate`  datetime NOT NULL AFTER `note`;
ALTER TABLE `nscc_dropping` MODIFY COLUMN `order_dropping_id`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `store`;
ALTER TABLE `nscc_dropping` ADD COLUMN `store_penerima`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `order_dropping_id`;
ALTER TABLE `nscc_dropping` ADD COLUMN `approved`  tinyint(3) NOT NULL DEFAULT 0 AFTER `store_penerima`;
ALTER TABLE `nscc_dropping` ADD COLUMN `approved_user_id`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `approved`;
ALTER TABLE `nscc_dropping` ADD COLUMN `approved_date`  datetime NULL DEFAULT NULL AFTER `approved_user_id`;
ALTER TABLE `nscc_dropping` MODIFY COLUMN `lunas`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 AFTER `approved_date`;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_dropping_BEFORE_INSERT` BEFORE INSERT ON `nscc_dropping`
FOR EACH ROW BEGIN
IF NEW.dropping_id IS NULL OR LENGTH(NEW.dropping_id) = 0 THEN
  SET NEW.dropping_id = UUID();
  END IF;
END;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_dropping_BEFORE_UPDATE` BEFORE UPDATE ON `nscc_dropping`
FOR EACH ROW BEGIN
	IF !(OLD.tgl  <=> NEW.tgl 
		AND OLD.doc_ref <=> NEW.doc_ref
		AND OLD.note <=> NEW.note
		AND OLD.tdate <=> NEW.tdate
		AND OLD.user_id <=> NEW.user_id
		AND OLD.type_ <=> NEW.type_
		AND OLD.store <=> NEW.store
		AND OLD.order_dropping_id <=> NEW.order_dropping_id
		AND OLD.store_penerima <=> NEW.store_penerima
		AND OLD.approved <=> NEW.approved
		AND OLD.approved_user_id <=> NEW.approved_user_id
		AND OLD.approved_date <=> NEW.approved_date
		AND OLD.lunas <=> NEW.lunas
	) THEN
		SET NEW.up = 0;
	END IF;
END;

CREATE DEFINER=`root`@`%` TRIGGER `nscc_dropping_details_BEFORE_INSERT` BEFORE INSERT ON `nscc_dropping_details`
FOR EACH ROW BEGIN
IF NEW.dropping_detail_id IS NULL OR LENGTH(NEW.dropping_detail_id) = 0 THEN
  SET NEW.dropping_detail_id = UUID();
  END IF;
END;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_dropping_details_BEFORE_UPDATE` BEFORE UPDATE ON `nscc_dropping_details`
FOR EACH ROW BEGIN
IF !(OLD.qty  <=> NEW.qty AND OLD.barang_id <=> NEW.barang_id 
	AND OLD.visible <=> NEW.visible AND OLD.dropping_id <=> NEW.dropping_id
) THEN 
		UPDATE nscc_dropping t SET t.up = 0 WHERE t.dropping_id = NEW.dropping_id;
		UPDATE nscc_dropping t SET t.up = 0 WHERE t.dropping_id = OLD.dropping_id;
  END IF;
END;

ALTER TABLE `nscc_order_dropping` MODIFY COLUMN `doc_ref`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `tgl`;
ALTER TABLE `nscc_order_dropping` MODIFY COLUMN `tdate`  datetime NOT NULL AFTER `note`;
ALTER TABLE `nscc_order_dropping` ADD COLUMN `store_pengirim`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `store`;
ALTER TABLE `nscc_order_dropping` ADD COLUMN `approved`  tinyint(3) NOT NULL DEFAULT 0 AFTER `store_pengirim`;
ALTER TABLE `nscc_order_dropping` ADD COLUMN `approved_user_id`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `approved`;
ALTER TABLE `nscc_order_dropping` ADD COLUMN `approved_date`  datetime NULL DEFAULT NULL AFTER `approved_user_id`;
ALTER TABLE `nscc_order_dropping` MODIFY COLUMN `lunas`  tinyint(3) NOT NULL DEFAULT '-1' AFTER `approved_date`;
ALTER TABLE `nscc_order_dropping` ADD CONSTRAINT `nscc_order_dropping_fk1` FOREIGN KEY (`store_pengirim`) REFERENCES `nscc_store` (`store_kode`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `nscc_order_dropping` ADD CONSTRAINT `nscc_order_dropping_fk0` FOREIGN KEY (`store`) REFERENCES `nscc_store` (`store_kode`) ON DELETE RESTRICT ON UPDATE CASCADE;
CREATE INDEX `nscc_order_dropping_fk1_idx` ON `nscc_order_dropping`(`store_pengirim`) USING BTREE ;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_order_dropping_BEFORE_INSERT` BEFORE INSERT ON `nscc_order_dropping`
FOR EACH ROW BEGIN
  IF NEW.order_dropping_id IS NULL OR LENGTH(NEW.order_dropping_id) = 0 THEN
      SET NEW.order_dropping_id = UUID();
  END IF;
 END;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_order_dropping_BEFORE_UPDATE` BEFORE UPDATE ON `nscc_order_dropping`
FOR EACH ROW BEGIN
  IF !(OLD.tgl  <=> NEW.tgl 
        AND OLD.doc_ref <=> NEW.doc_ref
        AND OLD.note <=> NEW.note
        AND OLD.tdate <=> NEW.tdate
        AND OLD.user_id <=> NEW.user_id
        AND OLD.type_ <=> NEW.type_
        AND OLD.store <=> NEW.store
        AND OLD.store_pengirim <=> NEW.store_pengirim
        AND OLD.approved <=> NEW.approved
        AND OLD.approved_user_id <=> NEW.approved_user_id
        AND OLD.approved_date <=> NEW.approved_date
        AND OLD.lunas <=> NEW.lunas
   ) THEN
        SET NEW.up = 0;
   END IF;
END;

CREATE DEFINER=`root`@`%` TRIGGER `nscc_order_dropping_details_BEFORE_INSERT` BEFORE INSERT ON `nscc_order_dropping_details`
FOR EACH ROW BEGIN
IF NEW.order_dropping_detail_id IS NULL OR LENGTH(NEW.order_dropping_detail_id) = 0 THEN
  SET NEW.order_dropping_detail_id = UUID();
  END IF;
END;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_order_dropping_details_BEFORE_UPDATE` BEFORE UPDATE ON `nscc_order_dropping_details`
FOR EACH ROW BEGIN
IF !(OLD.qty  <=> NEW.qty AND OLD.barang_id <=> NEW.barang_id 
	AND OLD.visible <=> NEW.visible AND OLD.order_dropping_id <=> NEW.order_dropping_id
) THEN 
		UPDATE nscc_order_dropping nod SET nod.up = 0 WHERE nod.order_dropping_id = NEW.order_dropping_id;
		UPDATE nscc_order_dropping nod SET nod.up = 0 WHERE nod.order_dropping_id = OLD.order_dropping_id;
  END IF;
END;

ALTER TABLE `nscc_po` MODIFY COLUMN `tgl`  date NOT NULL AFTER `doc_ref`;
ALTER TABLE `nscc_po` MODIFY COLUMN `tgl_delivery`  date NOT NULL AFTER `tgl`;
ALTER TABLE `nscc_po` MODIFY COLUMN `note`  varchar(600) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `tgl_delivery`;
ALTER TABLE `nscc_po` MODIFY COLUMN `acc_charge`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `note`;
ALTER TABLE `nscc_po` ADD COLUMN `pr_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `acc_charge`;
ALTER TABLE `nscc_po` MODIFY COLUMN `doc_ref_pr`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `pr_id`;
ALTER TABLE `nscc_po` MODIFY COLUMN `divisi`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `doc_ref_pr`;
ALTER TABLE `nscc_po` MODIFY COLUMN `store`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `divisi`;
ALTER TABLE `nscc_po` MODIFY COLUMN `status`  smallint(6) NOT NULL AFTER `store`;
ALTER TABLE `nscc_po` MODIFY COLUMN `tgl_status_change`  datetime NULL DEFAULT NULL AFTER `status`;
ALTER TABLE `nscc_po` MODIFY COLUMN `id_user`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `tgl_status_change`;
ALTER TABLE `nscc_po` MODIFY COLUMN `tdate`  datetime NOT NULL AFTER `id_user`;
ALTER TABLE `nscc_po` MODIFY COLUMN `ship_to_company`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `ship_to_address`;
ALTER TABLE `nscc_po` MODIFY COLUMN `ship_to_city`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `ship_to_company`;
ALTER TABLE `nscc_po` MODIFY COLUMN `ship_to_country`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `ship_to_city`;
ALTER TABLE `nscc_po` MODIFY COLUMN `ship_to_phone`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `ship_to_country`;
ALTER TABLE `nscc_po` MODIFY COLUMN `ship_to_fax`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `ship_to_phone`;
ALTER TABLE `nscc_po` MODIFY COLUMN `supplier_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `ship_to_fax`;
ALTER TABLE `nscc_po` MODIFY COLUMN `supp_nama`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `supplier_id`;
ALTER TABLE `nscc_po` MODIFY COLUMN `supp_address`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `supp_nama`;
ALTER TABLE `nscc_po` MODIFY COLUMN `supp_company`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `supp_address`;
ALTER TABLE `nscc_po` MODIFY COLUMN `supp_city`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `supp_company`;
ALTER TABLE `nscc_po` MODIFY COLUMN `supp_country`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `supp_city`;
ALTER TABLE `nscc_po` MODIFY COLUMN `supp_phone`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `supp_country`;
ALTER TABLE `nscc_po` MODIFY COLUMN `supp_fax`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `supp_phone`;
ALTER TABLE `nscc_po` MODIFY COLUMN `supp_email`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `supp_fax`;
ALTER TABLE `nscc_po` MODIFY COLUMN `termofpayment`  int(11) NOT NULL DEFAULT 0 AFTER `supp_email`;
ALTER TABLE `nscc_po` MODIFY COLUMN `nama_rek`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `termofpayment`;
ALTER TABLE `nscc_po` MODIFY COLUMN `no_rek`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `nama_rek`;
ALTER TABLE `nscc_po` MODIFY COLUMN `bank_rek`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `no_rek`;
ALTER TABLE `nscc_po` MODIFY COLUMN `sub_total`  decimal(30,2) NOT NULL AFTER `bank_rek`;
ALTER TABLE `nscc_po` MODIFY COLUMN `disc`  decimal(5,2) NOT NULL DEFAULT 0.00 AFTER `sub_total`;
ALTER TABLE `nscc_po` MODIFY COLUMN `total_disc_rp`  decimal(30,2) NOT NULL AFTER `disc`;
ALTER TABLE `nscc_po` MODIFY COLUMN `total_dpp`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `total_disc_rp`;
ALTER TABLE `nscc_po` MODIFY COLUMN `ppn`  char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'N' AFTER `total_dpp`;
ALTER TABLE `nscc_po` MODIFY COLUMN `tax_rp`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `ppn`;
ALTER TABLE `nscc_po` MODIFY COLUMN `total_pph_rp`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `tax_rp`;
ALTER TABLE `nscc_po` ADD COLUMN `closed_id_user`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `received`;
ALTER TABLE `nscc_po` ADD COLUMN `closed_tdate`  datetime NULL DEFAULT NULL AFTER `closed_id_user`;
ALTER TABLE `nscc_po` ADD COLUMN `type_`  tinyint(4) NOT NULL DEFAULT 1 AFTER `closed_tdate`;
#ALTER TABLE `nscc_po` ADD CONSTRAINT `nscc_po_fk1` FOREIGN KEY (`pr_id`) REFERENCES `nscc_pr` (`pr_id`) ON DELETE SET NULL ON UPDATE CASCADE;
#CREATE INDEX `nscc_po_fk1_idx` ON `nscc_po`(`pr_id`) USING BTREE ;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_po_BEFORE_INSERT` BEFORE INSERT ON `nscc_po`
FOR EACH ROW BEGIN
IF NEW.po_id IS NULL OR LENGTH(NEW.po_id) = 0 THEN
  SET NEW.po_id = UUID();
  END IF;
END;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_po_BEFORE_UPDATE` BEFORE UPDATE ON `nscc_po`
FOR EACH ROW BEGIN
	IF !(OLD.doc_ref <=> NEW.doc_ref 
        AND OLD.tgl <=> NEW.tgl
        AND OLD.tgl_delivery <=> NEW.tgl_delivery
        AND OLD.note <=> NEW.note
        AND OLD.acc_charge <=> NEW.acc_charge
        AND OLD.pr_id <=> NEW.pr_id
        AND OLD.doc_ref_pr <=> NEW.doc_ref_pr
        AND OLD.divisi <=> NEW.divisi
        AND OLD.store <=> NEW.store
        AND OLD.status <=> NEW.status
        AND OLD.tgl_status_change <=> NEW.tgl_status_change
		AND OLD.id_user <=> NEW.id_user
        AND OLD.tdate <=> NEW.tdate
        
        AND OLD.nama <=> NEW.nama
        AND OLD.ship_to_address <=> NEW.ship_to_address
		AND OLD.ship_to_company <=> NEW.ship_to_company
		AND OLD.ship_to_city <=> NEW.ship_to_city
		AND OLD.ship_to_country <=> NEW.ship_to_country
		AND OLD.ship_to_phone <=> NEW.ship_to_phone
		AND OLD.ship_to_fax <=> NEW.ship_to_fax
        
		AND OLD.supplier_id <=> NEW.supplier_id
		AND OLD.supp_nama <=> NEW.supp_nama
		AND OLD.supp_address <=> NEW.supp_address
		AND OLD.supp_city <=> NEW.supp_city
		AND OLD.supp_country <=> NEW.supp_country
		AND OLD.supp_phone <=> NEW.supp_phone
		AND OLD.supp_fax <=> NEW.supp_fax
		AND OLD.supp_email <=> NEW.supp_email
		AND OLD.termofpayment <=> NEW.termofpayment
		AND OLD.nama_rek <=> NEW.nama_rek
		AND OLD.no_rek <=> NEW.no_rek
		AND OLD.bank_rek <=> NEW.bank_rek
        
		AND OLD.sub_total <=> NEW.sub_total
		AND OLD.disc <=> NEW.disc
		AND OLD.total_disc_rp <=> NEW.total_disc_rp
		AND OLD.total_dpp <=> NEW.total_dpp
		AND OLD.tax_rp <=> NEW.tax_rp
		AND OLD.total_pph_rp <=> NEW.total_pph_rp
		AND OLD.total <=> NEW.total
        
        AND OLD.closed_id_user <=> NEW.closed_id_user
        AND OLD.closed_tdate <=> NEW.closed_tdate
	) THEN
		set NEW.up = 0;
	END IF;
END;

ALTER TABLE `nscc_po_details` MODIFY COLUMN `po_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `po_details_id`;
ALTER TABLE `nscc_po_details` MODIFY COLUMN `seq`  int(11) NOT NULL DEFAULT 0 AFTER `po_id`;
ALTER TABLE `nscc_po_details` MODIFY COLUMN `description`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `seq`;
ALTER TABLE `nscc_po_details` MODIFY COLUMN `barang_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `description`;
ALTER TABLE `nscc_po_details` MODIFY COLUMN `sub_total`  decimal(30,2) NOT NULL AFTER `price`;
ALTER TABLE `nscc_po_details` MODIFY COLUMN `disc`  decimal(5,2) NOT NULL DEFAULT 0.00 AFTER `sub_total`;
ALTER TABLE `nscc_po_details` MODIFY COLUMN `disc_rp`  decimal(30,2) NOT NULL AFTER `disc`;
ALTER TABLE `nscc_po_details` MODIFY COLUMN `total_disc`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `disc_rp`;
ALTER TABLE `nscc_po_details` MODIFY COLUMN `total_dpp`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `total_disc`;
ALTER TABLE `nscc_po_details` MODIFY COLUMN `ppn`  decimal(5,2) NOT NULL DEFAULT 0.00 AFTER `total_dpp`;
ALTER TABLE `nscc_po_details` MODIFY COLUMN `ppn_rp`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `ppn`;
ALTER TABLE `nscc_po_details` MODIFY COLUMN `pph`  decimal(5,2) NOT NULL DEFAULT 0.00 AFTER `ppn_rp`;
ALTER TABLE `nscc_po_details` MODIFY COLUMN `pph_rp`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `pph`;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_po_details_BEFORE_INSERT` BEFORE INSERT ON `nscc_po_details`
FOR EACH ROW BEGIN
IF NEW.po_details_id IS NULL OR LENGTH(NEW.po_details_id) = 0 THEN
  SET NEW.po_details_id = UUID();
  END IF;
END;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_po_details_BEFORE_UPDATE` BEFORE UPDATE ON `nscc_po_details`
FOR EACH ROW BEGIN
    IF !(OLD.po_id  <=> NEW.po_id
		AND OLD.seq <=> NEW.seq 
		AND OLD.description <=> NEW.description
        AND OLD.barang_id <=> NEW.barang_id
        AND OLD.qty <=> NEW.qty
		AND OLD.price <=> NEW.price
		AND OLD.sub_total <=> NEW.sub_total
		AND OLD.disc <=> NEW.disc
		AND OLD.disc_rp <=> NEW.disc_rp
		AND OLD.total_disc <=> NEW.total_disc
		AND OLD.total_dpp <=> NEW.total_dpp
		AND OLD.ppn <=> NEW.ppn
		AND OLD.ppn_rp <=> NEW.ppn_rp
		AND OLD.pph <=> NEW.pph
		AND OLD.pph_rp <=> NEW.pph_rp
		AND OLD.total <=> NEW.total
		AND OLD.charge <=> NEW.charge
		AND OLD.visible <=> NEW.visible
) THEN 
		UPDATE nscc_po t SET t.up = 0 WHERE t.po_id = NEW.po_id;
		UPDATE nscc_po t SET t.up = 0 WHERE t.po_id = OLD.po_id;
  END IF;
END;

ALTER TABLE `nscc_receive_dropping` MODIFY COLUMN `dropping_id`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `store`;
ALTER TABLE `nscc_receive_dropping` ADD COLUMN `store_pengirim`  varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `dropping_id`;
ALTER TABLE `nscc_receive_dropping` MODIFY COLUMN `lunas`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 AFTER `store_pengirim`;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_receive_dropping_BEFORE_INSERT` BEFORE INSERT ON `nscc_receive_dropping`
FOR EACH ROW BEGIN
IF NEW.receive_dropping_id IS NULL OR LENGTH(NEW.receive_dropping_id) = 0 THEN
  SET NEW.receive_dropping_id = UUID();
  END IF;
END;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_receive_dropping_BEFORE_UPDATE` BEFORE UPDATE ON `nscc_receive_dropping`
FOR EACH ROW BEGIN
	IF !(OLD.tgl  <=> NEW.tgl 
		AND OLD.doc_ref <=> NEW.doc_ref
		AND OLD.note <=> NEW.note
		AND OLD.tdate <=> NEW.tdate
		AND OLD.user_id <=> NEW.user_id
		AND OLD.type_ <=> NEW.type_
		AND OLD.store <=> NEW.store
		AND OLD.dropping_id <=> NEW.dropping_id
		AND OLD.store_pengirim <=> NEW.store_pengirim
		AND OLD.lunas <=> NEW.lunas
	) THEN
		SET NEW.up = 0;
	END IF;
END;

CREATE DEFINER=`root`@`%` TRIGGER `nscc_receive_dropping_details_BEFORE_INSERT` BEFORE INSERT ON `nscc_receive_dropping_details`
FOR EACH ROW BEGIN
IF NEW.receive_dropping_detail_id IS NULL OR LENGTH(NEW.receive_dropping_detail_id) = 0 THEN
  SET NEW.receive_dropping_detail_id = UUID();
  END IF;
END;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_receive_dropping_details_BEFORE_UPDATE` BEFORE UPDATE ON `nscc_receive_dropping_details`
FOR EACH ROW BEGIN
IF !(OLD.qty  <=> NEW.qty AND OLD.barang_id <=> NEW.barang_id 
	AND OLD.visible <=> NEW.visible AND OLD.receive_dropping_id <=> NEW.receive_dropping_id
) THEN 
		UPDATE nscc_receive_dropping t SET t.up = 0 WHERE t.receive_dropping_id = NEW.receive_dropping_id;
		UPDATE nscc_receive_dropping t SET t.up = 0 WHERE t.receive_dropping_id = OLD.receive_dropping_id;
  END IF;
END;

ALTER TABLE `nscc_terima_barang` ADD COLUMN `no_sj`  varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `store`;
ALTER TABLE `nscc_terima_barang` ADD COLUMN `tgl_sj`  date NULL DEFAULT NULL AFTER `no_sj`;
ALTER TABLE `nscc_terima_barang` ADD COLUMN `lock_edit`  tinyint(1) NULL DEFAULT 0 AFTER `tgl_sj`;
ALTER TABLE `nscc_terima_barang` ADD COLUMN `status`  tinyint(1) NOT NULL DEFAULT '-1' AFTER `lock_edit`;
DROP TRIGGER `nscc_terima_barang_before_insert`;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_terima_barang_before_insert` BEFORE INSERT ON `nscc_terima_barang`
FOR EACH ROW BEGIN
IF NEW.terima_barang_id IS NULL OR LENGTH(NEW.terima_barang_id) = 0 THEN
  SET NEW.terima_barang_id = UUID();
  END IF;
END;
DROP TRIGGER `nscc_terima_barang_before_update`;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_terima_barang_before_update` BEFORE UPDATE ON `nscc_terima_barang`
FOR EACH ROW BEGIN
	IF !(OLD.po_id  <=> NEW.po_id
		AND OLD.doc_ref <=> NEW.doc_ref 
		AND OLD.id_user <=> NEW.id_user
        AND OLD.tdate <=> NEW.tdate
        AND OLD.tgl <=> NEW.tgl
		AND OLD.store <=> NEW.store
		AND OLD.no_sj <=> NEW.no_sj
		AND OLD.tgl_sj <=> NEW.tgl_sj
		AND OLD.lock_edit <=> NEW.lock_edit
		AND OLD.status <=> NEW.status
	) THEN
		set NEW.up = 0;
	END IF;
END;

ALTER TABLE `nscc_terima_barang_details` MODIFY COLUMN `terima_barang_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `terima_barang_details_id`;
ALTER TABLE `nscc_terima_barang_details` MODIFY COLUMN `seq`  int(11) NOT NULL DEFAULT 0 AFTER `terima_barang_id`;
ALTER TABLE `nscc_terima_barang_details` MODIFY COLUMN `description`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `seq`;
ALTER TABLE `nscc_terima_barang_details` MODIFY COLUMN `barang_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `description`;
DROP TRIGGER `nscc_terima_barang_details_before_insert`;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_terima_barang_details_before_insert` BEFORE INSERT ON `nscc_terima_barang_details`
FOR EACH ROW BEGIN
IF NEW.terima_barang_details_id IS NULL OR LENGTH(NEW.terima_barang_details_id) = 0 THEN
  SET NEW.terima_barang_details_id = UUID();
  END IF;
END;
DROP TRIGGER `nscc_terima_barang_details_before_update`;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_terima_barang_details_before_update` BEFORE UPDATE ON `nscc_terima_barang_details`
FOR EACH ROW BEGIN
	IF !(OLD.qty  <=> NEW.qty AND OLD.description <=> NEW.description 
		AND OLD.seq <=> NEW.seq AND OLD.visible <=> NEW.visible 
		AND OLD.terima_barang_id <=> NEW.terima_barang_id
		AND OLD.barang_id <=> NEW.barang_id) THEN
		UPDATE nscc_terima_barang nt SET nt.up = 0 WHERE nt.terima_barang_id = NEW.terima_barang_id;
		UPDATE nscc_terima_barang nt SET nt.up = 0 WHERE nt.terima_barang_id = OLD.terima_barang_id;
	END IF;
END;

ALTER TABLE `nscc_transfer_item` MODIFY COLUMN `type_`  tinyint(4) NOT NULL DEFAULT 0 AFTER `user_id`;
ALTER TABLE `nscc_transfer_item` MODIFY COLUMN `tgl_jatuh_tempo`  date NULL DEFAULT NULL AFTER `supplier_id`;
ALTER TABLE `nscc_transfer_item` MODIFY COLUMN `terima_barang_id`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL AFTER `tgl_jatuh_tempo`;
ALTER TABLE `nscc_transfer_item` ADD COLUMN `no_fakturpajak`  varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `terima_barang_id`;
ALTER TABLE `nscc_transfer_item` ADD COLUMN `sub_total`  decimal(30,2) NOT NULL AFTER `no_fakturpajak`;
ALTER TABLE `nscc_transfer_item` MODIFY COLUMN `disc`  decimal(5,2) NOT NULL DEFAULT 0.00 AFTER `sub_total`;
ALTER TABLE `nscc_transfer_item` ADD COLUMN `total_disc_rp`  decimal(30,2) NOT NULL AFTER `disc`;
ALTER TABLE `nscc_transfer_item` ADD COLUMN `total_dpp`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `total_disc_rp`;
ALTER TABLE `nscc_transfer_item` ADD COLUMN `tax_rp`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `total_dpp`;
ALTER TABLE `nscc_transfer_item` ADD COLUMN `total_pph_rp`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `tax_rp`;
ALTER TABLE `nscc_transfer_item` MODIFY COLUMN `total`  decimal(30,2) NOT NULL AFTER `total_pph_rp`;
ALTER TABLE `nscc_transfer_item` ADD COLUMN `closed_id_user`  varchar(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `total`;
ALTER TABLE `nscc_transfer_item` ADD COLUMN `closed_tdate`  datetime NULL DEFAULT NULL AFTER `closed_id_user`;
ALTER TABLE `nscc_transfer_item` ADD COLUMN `status`  tinyint(3) UNSIGNED NOT NULL DEFAULT 0 AFTER `closed_tdate`;
DROP TRIGGER `nscc_transfer_item_before_insert`;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_transfer_item_before_insert` BEFORE INSERT ON `nscc_transfer_item`
FOR EACH ROW BEGIN
IF NEW.transfer_item_id IS NULL OR LENGTH(NEW.transfer_item_id) = 0 THEN
  SET NEW.transfer_item_id = UUID();
  END IF;
END;
DROP TRIGGER `nscc_transfer_item_before_update`;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_transfer_item_before_update` BEFORE UPDATE ON `nscc_transfer_item`
FOR EACH ROW BEGIN  
	IF !(OLD.tgl  <=> NEW.tgl
		AND OLD.doc_ref <=> NEW.doc_ref 
		AND OLD.note <=> NEW.note 
        AND OLD.tdate <=> NEW.tdate
		AND OLD.doc_ref_other <=> NEW.doc_ref_other 
        AND OLD.user_id <=> NEW.user_id
		AND OLD.type_ <=> NEW.type_ 
        AND OLD.store <=> NEW.store
		AND OLD.supplier_id <=> NEW.supplier_id 
		AND OLD.tgl_jatuh_tempo <=> NEW.tgl_jatuh_tempo
        AND OLD.terima_barang_id <=> NEW.terima_barang_id
        AND OLD.no_fakturpajak <=> NEW.no_fakturpajak
		AND OLD.sub_total <=> NEW.sub_total 
		AND OLD.disc <=> NEW.disc 
        AND OLD.total_disc_rp <=> NEW.total_disc_rp
        AND OLD.total_dpp <=> NEW.total_dpp
        AND OLD.tax_rp <=> NEW.tax_rp 
        AND OLD.total_pph_rp <=> NEW.total_pph_rp
        AND OLD.total <=> NEW.total
        AND OLD.status <=> NEW.status
        AND OLD.closed_id_user <=> NEW.closed_id_user
        AND OLD.closed_tdate <=> NEW.closed_tdate
	) THEN
	  SET NEW.up = 0;
	END IF;
END;

ALTER TABLE `nscc_transfer_item_details` ADD COLUMN `sub_total`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `price`;
ALTER TABLE `nscc_transfer_item_details` MODIFY COLUMN `disc`  decimal(5,2) NOT NULL DEFAULT 0.00 AFTER `sub_total`;
ALTER TABLE `nscc_transfer_item_details` ADD COLUMN `disc_rp`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `disc`;
ALTER TABLE `nscc_transfer_item_details` ADD COLUMN `total_disc`  decimal(30,2) NULL DEFAULT NULL AFTER `disc_rp`;
ALTER TABLE `nscc_transfer_item_details` ADD COLUMN `total_dpp`  decimal(30,2) NULL DEFAULT NULL AFTER `total_disc`;
ALTER TABLE `nscc_transfer_item_details` ADD COLUMN `ppn`  decimal(5,2) NOT NULL DEFAULT 0.00 AFTER `total_dpp`;
ALTER TABLE `nscc_transfer_item_details` ADD COLUMN `ppn_rp`  decimal(30,2) NOT NULL DEFAULT 0.00 AFTER `ppn`;
ALTER TABLE `nscc_transfer_item_details` ADD COLUMN `pph`  decimal(5,2) NULL DEFAULT NULL AFTER `ppn_rp`;
ALTER TABLE `nscc_transfer_item_details` ADD COLUMN `pph_rp`  decimal(30,2) NULL DEFAULT NULL AFTER `pph`;
ALTER TABLE `nscc_transfer_item_details` ADD COLUMN `charge`  varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL AFTER `total`;
ALTER TABLE `nscc_transfer_item_details` ADD COLUMN `visible`  tinyint(1) NULL DEFAULT 1 AFTER `charge`;
DROP TRIGGER `nscc_transfer_item_details_before_insert`;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_transfer_item_details_before_insert` BEFORE INSERT ON `nscc_transfer_item_details`
FOR EACH ROW BEGIN
IF NEW.transfer_item_details_id IS NULL OR LENGTH(NEW.transfer_item_details_id) = 0 THEN
  SET NEW.transfer_item_details_id = UUID();
END IF;
UPDATE nscc_transfer_item nti SET nti.up = 0 WHERE nti.transfer_item_id = NEW.transfer_item_id;
END;
DROP TRIGGER `nscc_transfer_item_details_before_update`;
CREATE DEFINER=`root`@`%` TRIGGER `nscc_transfer_item_details_before_update` BEFORE UPDATE ON `nscc_transfer_item_details`
FOR EACH ROW BEGIN
IF !(OLD.qty  <=> NEW.qty
		AND OLD.barang_id <=> NEW.barang_id 
		AND OLD.transfer_item_id <=> NEW.transfer_item_id 
        AND OLD.price <=> NEW.price
        AND OLD.sub_total <=> NEW.sub_total
        AND OLD.disc <=> NEW.disc
		AND OLD.disc_rp <=> NEW.disc_rp 
		AND OLD.total_disc <=> NEW.total_disc
		AND OLD.total_dpp <=> NEW.total_dpp
		AND OLD.ppn <=> NEW.ppn 
        AND OLD.ppn_rp <=> NEW.ppn_rp
		AND OLD.pph <=> NEW.pph 
        AND OLD.pph_rp <=> NEW.pph_rp
		AND OLD.total <=> NEW.total 
		AND OLD.charge <=> NEW.charge 
		AND OLD.visible <=> NEW.visible 
	) THEN	  
			UPDATE nscc_transfer_item nti SET nti.up = 0 WHERE nti.transfer_item_id = NEW.transfer_item_id;
			UPDATE nscc_transfer_item nti SET nti.up = 0 WHERE nti.transfer_item_id = OLD.transfer_item_id;
END IF;
END;

#VIEW

CREATE 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`%` 
SQL SECURITY DEFINER 
VIEW `nscc_dropping_approval`AS 
select 1 AS `jenis`,`d`.`dropping_id` AS `id_dropping_id`,NULL AS `id_order_dropping_id`,`d`.`tgl` AS `tgl`,`d`.`doc_ref` AS `doc_ref`,`d`.`note` AS `note`,`d`.`tdate` AS `tdate`,`d`.`user_id` AS `user_id`,`d`.`type_` AS `type_`,`d`.`store` AS `store`,`d`.`order_dropping_id` AS `order_dropping_id`,`d`.`store_penerima` AS `store_penerima`,NULL AS `store_pengirim`,`d`.`approved` AS `approved`,`d`.`approved_user_id` AS `approved_user_id`,`d`.`approved_date` AS `approved_date`,`d`.`lunas` AS `lunas`,`d`.`up` AS `up` from adwis.`nscc_dropping` `d` union select 0 AS `jenis`,NULL AS `id_dropping_id`,`od`.`order_dropping_id` AS `id_order_dropping_id`,`od`.`tgl` AS `tgl`,`od`.`doc_ref` AS `doc_ref`,`od`.`note` AS `note`,`od`.`tdate` AS `tdate`,`od`.`user_id` AS `user_id`,`od`.`type_` AS `type_`,`od`.`store` AS `store`,NULL AS `order_dropping_id`,NULL AS `store_penerima`,`od`.`store_pengirim` AS `store_pengirim`,`od`.`approved` AS `approved`,`od`.`approved_user_id` AS `approved_user_id`,`od`.`approved_date` AS `approved_date`,`od`.`lunas` AS `lunas`,`od`.`up` AS `up` from adwis.`nscc_order_dropping` `od` where (`od`.`lunas` <> -(1)) ;

CREATE 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`%` 
SQL SECURITY DEFINER 
VIEW `nscc_dropping_detail_view`AS 
select `dd`.`barang_id` AS `barang_id`,sum(`dd`.`qty`) AS `qty`,`d`.`order_dropping_id` AS `order_dropping_id` from (adwis.`nscc_dropping_details` `dd` left join adwis.`nscc_dropping` `d` on((`dd`.`dropping_id` = `d`.`dropping_id`))) where (`dd`.`visible` = 1) group by `dd`.`barang_id`,`d`.`order_dropping_id` ;

ALTER 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`%` 
SQL SECURITY DEFINER 
VIEW `nscc_dropping_sisa` AS 
select (`dd`.`qty` - coalesce(sum(`rdd`.`qty`),0)) AS `qty`,`dd`.`barang_id` AS `barang_id`,`dd`.`dropping_id` AS `dropping_id` from ((`adwis`.`nscc_dropping_details` `dd` left join `adwis`.`nscc_receive_dropping` `rd` on(((`rd`.`dropping_id` = `dd`.`dropping_id`) and (`dd`.`visible` = 1)))) left join `adwis`.`nscc_receive_dropping_details` `rdd` on(((`rdd`.`receive_dropping_id` = `rd`.`receive_dropping_id`) and (`rdd`.`barang_id` = `dd`.`barang_id`) and (`rdd`.`visible` = 1) and (`dd`.`visible` = 1)))) group by `dd`.`barang_id`,`dd`.`dropping_id` having (`qty` > 0) ;

ALTER 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`%` 
SQL SECURITY DEFINER 
VIEW `nscc_order_dropping_sisa` AS 
select (`odd`.`qty` - ifnull(`t`.`qty`,0)) AS `qty`,`odd`.`barang_id` AS `barang_id`,`odd`.`order_dropping_id` AS `order_dropping_id` from (`adwis`.`nscc_order_dropping_details` `odd` left join `adwis`.`nscc_dropping_detail_view` `t` on(((`t`.`order_dropping_id` = `odd`.`order_dropping_id`) and (`t`.`barang_id` = `odd`.`barang_id`)))) where (`odd`.`visible` = 1) group by `odd`.`barang_id`,`odd`.`order_dropping_id` having (`qty` > 0) ;

ALTER 
ALGORITHM=UNDEFINED 
DEFINER=`root`@`%` 
SQL SECURITY DEFINER 
VIEW `nscc_po_sisa_terima` AS 
select `npd`.`po_details_id` AS `po_details_id`,`npd`.`po_id` AS `po_id`,`npd`.`seq` AS `seq`,`npd`.`barang_id` AS `barang_id`,(`npd`.`qty` - coalesce(sum(`ntbd`.`qty`),0)) AS `qty`,`npd`.`price` AS `price`,`npd`.`total` AS `total`,`npd`.`disc_rp` AS `disc_rp`,`npd`.`sub_total` AS `sub_total`,`npd`.`ppn` AS `ppn`,`npd`.`ppn_rp` AS `ppn_rp`,`npd`.`pph` AS `pph`,`npd`.`pph_rp` AS `pph_rp`,`npd`.`disc` AS `disc`,`npd`.`total_disc` AS `total_disc`,`npd`.`total_dpp` AS `total_dpp`,`npd`.`charge` AS `charge`,`npd`.`visible` AS `visible` from ((`adwis`.`nscc_po_details` `npd` left join `adwis`.`nscc_terima_barang` `ntb` on(((`npd`.`po_id` = `ntb`.`po_id`) and (`npd`.`visible` = 1)))) left join `adwis`.`nscc_terima_barang_details` `ntbd` on(((`ntbd`.`terima_barang_id` = `ntb`.`terima_barang_id`) and (`ntbd`.`barang_id` = `npd`.`barang_id`) and (`ntbd`.`visible` = 1) and (`npd`.`visible` = 1)))) where (`npd`.`visible` = 1) group by `npd`.`barang_id`,`npd`.`po_id` having (`qty` > 0) ;