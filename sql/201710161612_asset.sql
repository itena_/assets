CREATE TABLE `nscc_asset_group` (
	`asset_group_id` VARCHAR(50) NOT NULL,
	`tariff` DOUBLE NULL DEFAULT NULL,
	`period` INT(10) NULL DEFAULT NULL,
	`desc` VARCHAR(100) NULL DEFAULT NULL,
	`golongan` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`asset_group_id`),
	INDEX `idx_class_asset` (`asset_group_id`) USING BTREE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;


CREATE TABLE `nscc_asset` (
	`asset_id` VARCHAR(50) NOT NULL COLLATE 'latin1_swedish_ci',
	`asset_name` VARCHAR(150) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`doc_ref` VARCHAR(100) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`branch` VARCHAR(15) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`qty` VARCHAR(15) NULL DEFAULT NULL,
	`description` VARCHAR(200) NULL DEFAULT NULL,
	`date_acquisition` DATE NULL DEFAULT NULL,
	`price_acquisition` VARCHAR(30) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`new_price_acquisition` VARCHAR(30) NULL DEFAULT NULL COLLATE 'latin1_swedish_ci',
	`asset_group_id` VARCHAR(50) NULL DEFAULT NULL,
	`status` ENUM('1','0') NULL DEFAULT NULL,
	PRIMARY KEY (`asset_id`),
	INDEX `id_asset` (`asset_id`) USING BTREE,
	INDEX `nscc_asset_ibfk_1` (`asset_group_id`),
	CONSTRAINT `nscc_asset_ibfk_1` FOREIGN KEY (`asset_group_id`) REFERENCES `nscc_asset_group` (`asset_group_id`) ON UPDATE CASCADE ON DELETE CASCADE
)
COLLATE='utf8mb4_general_ci'
ENGINE=InnoDB
;


CREATE TABLE `nscc_asset_detail` (
	`asset_trans_id` VARCHAR(50) NOT NULL,
	`asset_id` VARCHAR(50) NULL DEFAULT NULL,
	`asset_group_id` VARCHAR(50) NULL DEFAULT NULL,
	`docref` VARCHAR(50) NULL DEFAULT NULL,
	`docref_other` VARCHAR(50) NULL DEFAULT NULL,
	`asset_trans_name` VARCHAR(100) NULL DEFAULT NULL,
	`asset_trans_branch` VARCHAR(100) NULL DEFAULT NULL,
	`asset_trans_price` VARCHAR(100) NULL DEFAULT NULL,
	`asset_trans_new_price` VARCHAR(50) NULL DEFAULT NULL,
	`asset_trans_date` VARCHAR(100) NULL DEFAULT NULL,
	`description` TINYTEXT NULL,
	`status` ENUM('1','0') NULL DEFAULT NULL,
	PRIMARY KEY (`asset_trans_id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
