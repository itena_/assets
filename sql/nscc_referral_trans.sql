/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50616
Source Host           : localhost:3306
Source Database       : nox_pusat_1902

Target Server Type    : MYSQL
Target Server Version : 50616
File Encoding         : 65001

Date: 2018-03-12 12:24:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for nscc_referral_trans
-- ----------------------------
DROP TABLE IF EXISTS `nscc_referral_trans`;
CREATE TABLE `nscc_referral_trans` (
  `referral_trans` varchar(50) NOT NULL,
  `sales_id` varchar(50) DEFAULT NULL,
  `referral_id` varchar(50) DEFAULT NULL,
  `persen_bonus` int(10) DEFAULT NULL,
  `bonus` int(20) DEFAULT NULL,
  `tdate` date DEFAULT NULL,
  `store` varchar(50) DEFAULT NULL,
  `id_user` varchar(50) DEFAULT NULL,
  `visible` int(1) DEFAULT NULL,
  PRIMARY KEY (`referral_trans`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
