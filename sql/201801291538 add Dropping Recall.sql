﻿-- 
-- Script was generated by Devart dbForge Studio for MySQL, Version 7.2.78.0
-- Product Home Page: http://www.devart.com/dbforge/mysql/studio
-- Script date 29/01/2018 15:39:56
-- Source server version: 5.6.16
-- Source connection string: User Id=root;Host=localhost;Character Set=utf8
-- Target server version: 10.1.23
-- Target connection string: User Id=root;Host=10.0.0.113;Database=posng;Protocol=Ssh;Character Set=utf8;SSH Host=172.169.222.151;SSH Port=22;SSH User=it;SSH Authentication Type=Password
-- Run this script against posng to synchronize it with posng
-- Please backup your target database before running this script
-- 

--
-- Disable foreign keys
--
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

SET NAMES 'utf8';

USE posng;


--
-- Create table "nscc_dropping_recall"
--
CREATE TABLE nscc_dropping_recall (
  dropping_recall_id VARCHAR(36) NOT NULL,
  tgl DATE NOT NULL,
  doc_ref VARCHAR(45) NOT NULL,
  note VARCHAR(45) DEFAULT NULL,
  tdate DATETIME NOT NULL,
  user_id VARCHAR(36) NOT NULL,
  status VARCHAR(45) NOT NULL DEFAULT '0',
  store VARCHAR(45) NOT NULL,
  up TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (dropping_recall_id),
  UNIQUE INDEX doc_ref_UNIQUE (doc_ref),
  INDEX nscc_dropping_recall_fk0_idx (store),
  CONSTRAINT nscc_dropping_recall_fk0 FOREIGN KEY (store)
    REFERENCES nscc_store(store_kode) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci;

--
-- Create table "nscc_dropping_recall_details"
--
CREATE TABLE nscc_dropping_recall_details (
  dropping_recall_details_id VARCHAR(36) NOT NULL,
  dropping_recall_id VARCHAR(36) NOT NULL,
  dropping_id VARCHAR(36) NOT NULL,
  seq TINYINT(4) NOT NULL DEFAULT 0,
  barang_id VARCHAR(36) NOT NULL,
  qty INT(11) NOT NULL DEFAULT 0,
  price DECIMAL(30, 2) NOT NULL DEFAULT 0.00,
  visible TINYINT(4) NOT NULL DEFAULT 1,
  PRIMARY KEY (dropping_recall_details_id),
  INDEX nscc_dropping_recall_details_fk0_idx (dropping_recall_id),
  INDEX nscc_dropping_recall_details_fk1_idx (dropping_id),
  INDEX nscc_dropping_recall_details_fk3_idx (barang_id),
  CONSTRAINT nscc_dropping_recall_details_fk0 FOREIGN KEY (dropping_recall_id)
    REFERENCES nscc_dropping_recall(dropping_recall_id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT nscc_dropping_recall_details_fk1 FOREIGN KEY (dropping_id)
    REFERENCES nscc_dropping(dropping_id) ON DELETE RESTRICT ON UPDATE CASCADE,
  CONSTRAINT nscc_dropping_recall_details_fk3 FOREIGN KEY (barang_id)
    REFERENCES nscc_barang(barang_id) ON DELETE RESTRICT ON UPDATE CASCADE
)
ENGINE = INNODB
CHARACTER SET utf8mb4
COLLATE utf8mb4_general_ci;

--
-- Alter view "nscc_dropping_sisa"
--
CREATE OR REPLACE 
VIEW nscc_dropping_sisa
AS
	select ((`dd`.`qty` - coalesce(sum(`rdd`.`qty`),0)) - coalesce(sum(`drd`.`qty`),0)) AS `qty`,`dd`.`barang_id` AS `barang_id`,`dd`.`dropping_id` AS `dropping_id`,`dd`.`price` AS `price` from (((`nscc_dropping_details` `dd` left join `nscc_receive_dropping` `rd` on(((`rd`.`dropping_id` = `dd`.`dropping_id`) and (`dd`.`visible` = 1)))) left join `nscc_receive_dropping_details` `rdd` on(((`rdd`.`receive_dropping_id` = `rd`.`receive_dropping_id`) and (`rdd`.`barang_id` = `dd`.`barang_id`) and (`rdd`.`visible` = 1) and (`dd`.`visible` = 1)))) left join `nscc_dropping_recall_details` `drd` on(((`drd`.`dropping_id` = `dd`.`dropping_id`) and (`drd`.`barang_id` = `dd`.`barang_id`) and (`drd`.`visible` = 1) and (`dd`.`visible` = 1)))) group by `dd`.`barang_id`,`dd`.`dropping_id` having (`qty` > 0);

--
-- Enable foreign keys
--
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;