ALTER TABLE nscc_asset ADD barang_id VARCHAR(50);
ALTER TABLE nscc_asset ADD ati VARCHAR(50);
ALTER TABLE nscc_asset ADD created_at DATETIME;
ALTER TABLE nscc_asset ADD updated_at DATETIME;

ALTER TABLE nscc_asset_detail ADD barang_id VARCHAR(50);
ALTER TABLE nscc_asset_detail ADD ati VARCHAR(50);
ALTER TABLE nscc_asset_detail ADD created_at DATETIME;
ALTER TABLE nscc_asset_detail ADD updated_at DATETIME;

ALTER TABLE nscc_asset_periode ADD barang_id VARCHAR(50);
ALTER TABLE nscc_asset_periode ADD ati VARCHAR(50);
ALTER TABLE nscc_asset_periode ADD created_at DATETIME;
ALTER TABLE nscc_asset_periode ADD updated_at DATETIME;
 