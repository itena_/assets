<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <link rel="shortcut icon" href="<?php echo bu(); ?>/favicon.png?v=<?php echo md5_file( 'favicon.png' ) ?>">
    <title><?php echo CHtml::encode( Yii::app()->name ); ?></title>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/xtheme-gray.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/extjs.css"/>
    <style>
        #drop {
            border: 2px dashed #BBBBBB;
            border-radius: 5px;
            color: #BBBBBB;
            font: 20pt bold, "Vollkorn";
            padding: 25px;
            text-align: center;
        }

        * {
            font-size: 12px;
            font-family: Candara;
        }
    </style>
</head>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/adapter/ext/ext-base.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/ext-all.js"></script>
<body>
<?php echo $content; ?>
<iframe id="iframe" sandbox="allow-same-origin" style="display: none"></iframe>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib.min.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/login.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/sha512.js"></script>
<script type="text/javascript"
        src="<?php echo Yii::app()->request->baseUrl; ?>/js/client.min.js"></script>
<script>
    var client = new ClientJS();
    var BrowserData = client.getBrowserData();
    var Fingerprint = client.getFingerprint();
    var UserAgent = client.getUserAgent();
    var Browser = client.getBrowser();
    var BrowserVersion = client.getBrowserVersion();
    var BrowserMajorVersion = client.getBrowserMajorVersion();
    var IE = client.isIE();
    var Chrome = client.isChrome();
    var Firefox = client.isFirefox();
    var Safari = client.isSafari();
    var Opera = client.isOpera();
    var Engine = client.getEngine();
    var EngineVersion = client.getEngineVersion();
    var OS = client.getOS();
    var OSVersion = client.getOSVersion();
    var Windows = client.isWindows();
    var MAC = client.isMac();
    var Linux = client.isLinux();
    var Ubuntu = client.isUbuntu();
    var Solaris = client.isSolaris();
    var Device = client.getDevice();
    var DeviceType = client.getDeviceType();
    var DeviceVendor = client.getDeviceVendor();
    var CPU = client.getCPU();
    var Mobile = client.isMobile();
    var MobileMajor = client.isMobileMajor();
    var MobileAndroid = client.isMobileAndroid();
    var MobileOpera = client.isMobileOpera();
    var MobileWindows = client.isMobileWindows();
    var MobileBlackBerry = client.isMobileBlackBerry();
    var MobileIOS = client.isMobileIOS();
    var Iphone = client.isIphone();
    var Ipad = client.isIpad();
    var Ipod = client.isIpod();
    var ScreenPrint = client.getScreenPrint();
    var ColorDepth = client.getColorDepth();
    var CurrentResolution = client.getCurrentResolution();
    var AvailableResolution = client.getAvailableResolution();
    var DeviceXDPI = client.getDeviceXDPI();
    var DeviceYDPI = client.getDeviceYDPI();
    var Plugins = client.getPlugins();
    var Java = client.isJava();
    var JavaVersion = client.getJavaVersion();
    var Flash = client.isFlash();
    var FlashVersion = client.getFlashVersion();
    var Silverlight = client.isSilverlight();
    var SilverlightVersion = client.getSilverlightVersion();
    var MimeTypes = client.getMimeTypes();
    var IsMimeTypes = client.isMimeTypes();
    var Font = client.isFont();
    var Fonts = client.getFonts();
    var LocalStorage = client.isLocalStorage();
    var SessionStorage = client.isSessionStorage();
    var Cookie = client.isCookie();
    var TimeZone = client.getTimeZone();
    var Language = client.getLanguage();
    var SystemLanguage = client.getSystemLanguage();
    var Canvas = client.isCanvas()
    var IP_LOCAL = "0.0.0.0";
    var IP_PUBLIC = "0.0.0.0";
    getIPs(function (ipcall) {
        console.log(ipcall);
        IP_LOCAL = ipcall;
    });
    getJSON('https://api.ipify.org?format=json',
        function (err, data) {
            if (err !== null) {
                console.log('Something went wrong: ' + err);
            } else {
                IP_PUBLIC = data.ip;
                console.log(data.ip);
            }
        });
</script>
</body>
</html>
