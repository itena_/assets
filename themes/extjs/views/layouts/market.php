<html>
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="<?php echo bu(); ?>/favicon.png?v=<?php echo md5_file('favicon.png') ?>">
    <title><?php echo CHtml::encode(Yii::app()->name); ?></title>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/ext-all.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/resources/css/xtheme-gray.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/grid/grid-examples.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/css/RowEditor.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/extjs.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/shared/icons/silk.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/aspnet/aspnet.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/silk_v013/silk013.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/form/combos.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/js/ext340/examples/ux/fileuploadfield/css/fileuploadfield.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui.theme.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="<?php echo Yii::app()->request->baseUrl; ?>/css/ui.jqgrid.css"/>
    <style>
        #drop {
            border: 2px dashed #BBBBBB;
            border-radius: 5px;
            color: #BBBBBB;
            font: 20pt bold, "Vollkorn";
            padding: 25px;
            text-align: center;
        }

        * {
            font-size: 12px;
            font-family: Candara;
        }

        .container {
            display: table;
        }

        .search-item-table {
            display: table-row;
            color: #8B1D51;
        }

        .cell4 {
            display: table-cell;
            /*border: solid;*/
            /*border-width: thin;*/
            padding-left: 5px;
            padding-right: 5px;
        }

        .x-grid3-row td, .x-grid3-summary-row td {
            vertical-align: middle !important;
        }

        .custom-sales-details .x-grid-row-selected .x-grid-cell-first {
            padding-left: 5px;
        }

        @media screen and (-webkit-min-device-pixel-ratio: 0) {
            .x-grid3-cell, /* Normal grid cell */
            .x-grid3-gcell { /* Grouped grid cell (esp. in head)*/
                box-sizing: border-box;
            }
        }

        /* Text items */
        .big-text .x-toolbar-text {
            font-size: 14px;
        }

        .big-text .x-btn-text {
            font-size: 14px;
            margin: 5px;
        }

        /* Button text */
        .big-text .x-btn-default-toolbar-small .x-btn-inner {
            font-size: 14px;
        }

        .big-label {
            font-size: 14px;
        }

        .x-grid-row .custom-column {
            background-color: #ecf;
            color: #090;
            font-weight: bold;
        }

        .x-grid3-row.error-row .x-grid3-cell {
            background-color: #ffe2e2 !important;
            color: #900 !important;
        }

        .x-grid3-row.valid-row .x-grid3-cell {
            background-color: #e2ffe2 !important;
            color: #090 !important;
        }
    </style>
</head>
<body>
<?php echo $content; ?>
</body>
</html>
