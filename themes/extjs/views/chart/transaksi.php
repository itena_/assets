<script type="text/javascript">
    window.onload = function () {
        var data = '<?php echo $data; ?>';
        var json = JSON.parse(data);
        var chart = new CanvasJS.Chart("chartContainer",
            {
                //theme: "light2", // "light2", "dark1", "dark2"
                backgroundColor: null,
                animationEnabled: true,
                title: {
                    text: "Sales <?php echo $chart_title?>"
                },
                legend: {
                    verticalAlign: "bottom",
                    horizontalAlign: "center"
                },
                axisX:{
                    labelFontSize: 14,
                    labelAngle: -40,
                    interval: 1
                },
                data: [
                    {
                        // Change type to "bar", "area", "spline", "pie", "line", etc.
                        type: "<?php echo $chart_type?>",
                        dataPoints: json
                    }
                ]
            });
        chart.render();
    }
</script>