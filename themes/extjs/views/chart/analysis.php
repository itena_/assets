<!--<script type="text/javascript">
    window.onload = function () {
        var chart = new CanvasJS.Chart("chartContainer",{
            title:{
                text: "Analysis"
            },
            axisX:{
                valueFormatString: "####",
                interval: 1
            },
            axisY:[{
                title: "Linear Scale",
                lineColor: "#369EAD",
                titleFontColor: "#369EAD",
                labelFontColor: "#369EAD"
            },
                {
                    title: "Logarithmic Scale",
                    logarithmic: true,
                    lineColor: "#C24642",
                    titleFontColor: "#C24642",
                    labelFontColor: "#C24642"
                }],
            axisY2:[{
                title: "Linear Scale",
                lineColor: "#7F6084",
                titleFontColor: "#7F6084",
                labelFontColor: "#7F6084"
            },
                {
                    title: "Logarithmic Scale",
                    logarithmic: true,
                    interval: 1,
                    lineColor: "#86B402",
                    titleFontColor: "#86B402",
                    labelFontColor: "#86B402"
                }],

            data: [
                {
                    type: "column",
                    showInLegend: true,
                    //axisYIndex: 0, //Defaults to Zero
                    name: "Budget",
                    xValueFormatString: "####",
                    dataPoints: [
                        { x: 2006, y: 6 },
                        { x: 2007, y: 2 },
                        { x: 2008, y: 5 },
                        { x: 2009, y: 7 },
                        { x: 2010, y: 1 },
                        { x: 2011, y: 5 },
                        { x: 2012, y: 5 },
                        { x: 2013, y: 2 },
                        { x: 2014, y: 2 }
                    ]
                },
                {
                    type: "spline",
                    showInLegend: true,
                    axisYIndex: 1, //Defaults to Zero
                    name: "AmountBudget",
                    xValueFormatString: "####",
                    dataPoints: [
                        { x: 2006, y: 15 },
                        { x: 2007, y: 3 },
                        { x: 2008, y: 20 },
                        { x: 2009, y: 10 },
                        { x: 2010, y: 30 },
                        { x: 2011, y: 10 },
                        { x: 2012, y: 600 },
                        { x: 2013, y: 20 },
                        { x: 2014, y: 2 }
                    ]
                },
                {
                    type: "column",
                    showInLegend: true,
                    axisYType: "secondary",
                    //axisYIndex: 0, //Defaults to Zero
                    name: "Realization",
                    xValueFormatString: "####",
                    dataPoints: [
                        { x: 2006, y: 12 },
                        { x: 2007, y: 20 },
                        { x: 2008, y: 28 },
                        { x: 2009, y: 34 },
                        { x: 2010, y: 24 },
                        { x: 2011, y: 45 },
                        { x: 2012, y: 15 },
                        { x: 2013, y: 34 },
                        { x: 2014, y: 22 }
                    ]
                },
                {
                    type: "column",
                    showInLegend: true,
                    axisYType: "secondary",
                    //axisYIndex: 0, //Defaults to Zero
                    name: "Realization",
                    xValueFormatString: "####",
                    dataPoints: [
                        { x: 2006, y: 12 },
                        { x: 2007, y: 20 },
                        { x: 2008, y: 28 },
                        { x: 2009, y: 34 },
                        { x: 2010, y: 24 },
                        { x: 2011, y: 45 },
                        { x: 2012, y: 15 },
                        { x: 2013, y: 34 },
                        { x: 2014, y: 22 }
                    ]
                },
                {
                    type: "line",
                    showInLegend: true,
                    axisYType: "secondary",
                    axisYIndex: 1, //When axisYType is secondary, axisYIndex indexes to secondary Y axis & not to primary Y axis
                    name: "AmountRealization",
                    xValueFormatString: "####",
                    dataPoints: [
                        { x: 2006, y: 86 },
                        { x: 2007, y: 15 },
                        { x: 2008, y: 27 },
                        { x: 2009, y: 78 },
                        { x: 2010, y: 46 },
                        { x: 2011, y: 70 },
                        { x: 2012, y: 50 },
                        { x: 2013, y: 60 },
                        { x: 2014, y: 50 }
                    ]
                }
            ]
        });

        chart.render();
    }
</script>-->

<script type="text/javascript">
    window.onload = function () {

        var dataBudget = '<?php echo $dataBudget; ?>';
        var dataRealization = '<?php echo $dataRealization; ?>';
        var dataAchievement = '<?php echo $dataAchievement; ?>';

        var jsonBudget = JSON.parse(dataBudget);
        var jsonRealization = JSON.parse(dataRealization);
        var jsonAchievement = JSON.parse(dataAchievement);

        var chart = new CanvasJS.Chart("chartContainer",
            {
                //theme: "dark1", // "light2", "dark1", "dark2"
                backgroundColor: null,
                animationEnabled: true,
                title: {
                    text: "Budget Analysis <?php echo $chart_title?>"
                },
                legend: {
                    verticalAlign: "bottom",
                    horizontalAlign: "center"
                },
                axisX:{
                    labelFontSize: 14,
                    labelAngle: -40,
                    interval: 0
                },
                axisY:{
                    labelFontSize: 8,
                    labelAngle: 0,
                    interval: 0
                },
                data: [
                    {
                        // Change type to "bar", "area", "spline", "pie", "line", etc.
                        type: "<?php echo $chart_type?>",
                        name: "Plan",
                        dataPoints: jsonBudget,
                        showInLegend: true,
                    },
                    {
                        // Change type to "bar", "area", "spline", "pie", "line", etc.
                        type: "<?php echo $chart_type?>",
                        name: "Realization",
                        dataPoints: jsonRealization,
                        showInLegend: true,
                    },
                    {
                        // Change type to "bar", "area", "spline", "pie", "line", etc.
                        type: "<?php echo $chart_type?>",
                        name: "Achievement",
                        dataPoints: jsonAchievement,
                        showInLegend: true,
                    }
                ]
            });
        chart.render();
    }
</script>