<script type="text/javascript">
    window.onload = function () {
        var data = '<?php echo $data; ?>';
        var json = JSON.parse(data);
        var chart = new CanvasJS.Chart("chartContainer",
            {
                backgroundColor: null,
                animationEnabled: true,
                title: {
                    text: "Budget Realizations <?php echo $chart_title?>"
                },
                legend: {
                    verticalAlign: "bottom",
                    horizontalAlign: "center"
                },
                axisX:{
                    labelFontSize: 14,
                    labelAngle: -40,
                    interval: 1
                },
                data: [
                    {
                        type: "<?php echo $chart_type?>",
                        dataPoints: json
                    }
                ]
            });
        chart.render();
    }
</script>