<h1>Sales</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>

<?
$this->pageTitle = 'Sales';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'extraRowPos' => 'below',
    'mergeColumns' => is_report_excel() ? array() : array('tdate'),
    'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
        array(
            'header' => 'Month',
            'name' => 'tdate',
            'footer' => 'Total',
            'footerHtmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Amount',
            'name' => 'amount',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalsales, 2)
        ),
    )
));
?>