<h1>Report Asset Depreciation</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>Branch/Store : <?= $branch ?></h3>
<h3>Category : <?= $categoryname ?></h3>
<?
$this->pageTitle = 'Report Asset Depreciation';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    //'mergeColumns' => is_report_excel() ? array() : array('ati', 'asset_trans_date', 'asset_trans_name', 'asset_trans_branch','class','period','tariff'),
    //'extraRowColumns' => is_report_excel() ? array() : array('activa'),
    //'extraRowPos' => 'below',
    //'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(

        array(
            'header' => 'Activa',
            'name' => 'activa',

        ),
        array(
            'header' => 'Asset',
            'name' => 'asset'
        ),
        array(
            'header' => 'Store',
            'name' => 'store'
        ),
        array(
            'header' => 'Description',
            'name' => 'keterangan'
        ),
        array(
            'header' => 'Period',
            'name' => 'period'
        ),
        array(
            'header' => 'Acquisition Date',
            'name' => 'tdate',
            //'footer' => 'Total'
        ),
        array(
            'header' => 'Depreciation End',
            'name' => 'depreciationend',

        ),
        array(
            'header' => 'Acquisition Price',
            'name' => 'acquisitionprice',
            'value' => function ($data) {
                return format_number_report($data['acquisitionprice'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Total',
            'footerHtmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Depreciation Permonth',
            'name' => 'depreciationpermonth',
            'value' => function ($data) {
                return format_number_report($data['depreciationpermonth'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totaldep, 2)
        ),
        array(
            'header' => 'Depreciation Accumulation (sum)',
            'name' => 'depreciationaccumulationbyparam',
            'value' => function ($data) {
                return format_number_report($data['depreciationaccumulationbyparam'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totaldepbyparam, 2)
        ),
        array(
            'header' => 'Depreciation Accumulation',
            'name' => 'akumulasibyparam',
            'value' => function ($data) {
                return format_number_report($data['akumulasibyparam'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalakumulasibyparam, 2)
        ),
        array(
            'header' => 'Nilai Buku',
            'name' => 'nilaibukubyparam',
            'value' => function ($data) {
                return format_number_report($data['nilaibukubyparam'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalnilaibukubyparam, 2)
        ),
        array(
            'header' => 'Depreciation Accumulation (now)',
            'name' => 'depreciationaccumulation',
            'value' => function ($data) {
                return format_number_report($data['depreciationaccumulation'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalacc, 2)
        ),
        array(
            'header' => 'Nilai Buku (now)',
            'name' => 'nilaibuku',
            'value' => function ($data) {
                return format_number_report($data['nilaibuku'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalnilaibuku, 2)
        ),
    )
));
?>