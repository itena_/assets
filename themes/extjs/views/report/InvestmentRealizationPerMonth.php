<h1>Investment Realization PerMonth</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>

<?
$this->pageTitle = 'Investment Realization PerMonth';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array('group_name','account_name'),
    'extraRowPos' => 'below',
    'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
        array(
            'header' => 'Group Name',
            'name' => 'group_name',
        ),
        array(
            'header' => 'Account Name',
            'name' => 'account_name',
        ),
        array(
            'header' => 'January',
            'name' => 'january',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'February',
            'name' => 'february',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'March',
            'name' => 'march',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'April',
            'name' => 'april',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'May',
            'name' => 'may',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'June',
            'name' => 'june',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'July',
            'name' => 'july',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'August',
            'name' => 'august',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'September',
            'name' => 'september',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Oktober',
            'name' => 'oktober',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'November',
            'name' => 'november',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'December',
            'name' => 'december',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Total'
        ),
        array(
            'header' => 'Total',
            'name' => 'total',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2),
        )
    )
));


