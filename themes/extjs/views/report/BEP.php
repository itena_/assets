<h1>BEP</h1>
</br>
<h3>YEAR : <?= $from ?></h3>
</br>
<h3>PENJUALAN OBAT      :  Rp. <?= format_number_report($totalobat, 2) ?></h3>
<h3>PENJUALAN APOTEK    :  Rp. <?= format_number_report($totalapotek, 2) ?></h3>
<h3>PENJUALAN JASA      :  Rp. <?= format_number_report($totaljasa, 2) ?></h3>
<h3>TOTAL PENJUALAN     :  Rp. <?= format_number_report($totalpenjualan, 2) ?></h3>
</br>
<h3>HPP OBAT      :  Rp. <?= format_number_report($hpppo, 2) ?></h3>
<h3>HPP APOTEK    :  Rp. <?= format_number_report($hpppa, 2) ?></h3>
<h3>HPP JASA      :  Rp. <?= format_number_report($hpppj, 2) ?></h3>
<h3>TOTAL HPP (<?= $hpp ?>%)  :  Rp. <?= format_number_report($totalhpp, 2) ?></h3>

</br>

<h3>LABA KOTOR      :  Rp. <?= format_number_report($labakotor, 2) ?></h3>
<?
$this->pageTitle = 'BEP';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dppenjualan,
    //'mergeColumns' => is_report_excel() ? array() : array('group_name','account_name'),
    //'extraRowPos' => 'above',
    //'extraRowColumns' => array('category_name'),
    //'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
        /*        array(
                    'header' => 'Group Name',
                    'name' => 'group_name',
                ),*/
        array(
            'header' => 'Sales',
            'name' => 'category_name',
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Total'
        ),
        array(
            'header' => 'Amount',
            'name' => 'total',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Rp. '.format_number_report($totalpenjualan, 2)
        ),
        array(
            'header' => '%',
            'name' => 'hpp',
            'value' => ''.$hpp,
        ),
        array(
            'header' => 'Tahun 1',
            'name' => 'total',
            'value' => ''.$totalpenjualan*12,
            'htmlOptions' => array('style' => 'text-align: right;'),

            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Rp. '.format_number_report($totalpenjualan, 2)
        ),
        array(
            'header' => 'Tahun 2',
            'name' => 'total',
            'value' => ''.$totalpenjualan*24,
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Rp. '.format_number_report($totalpenjualan, 2)
        ),
        array(
            'header' => 'Tahun 3',
            'name' => 'total',
            'value' => ''.$totalpenjualan*36,
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Rp. '.format_number_report($totalpenjualan, 2)
        ),
        array(
            'header' => 'Tahun 4',
            'name' => 'total',
            'value' => ''.$totalpenjualan*48,
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Rp. '.format_number_report($totalpenjualan, 2)
        ),
        array(
            'header' => 'Tahun 5',
            'name' => 'total',
            'value' => ''.$totalpenjualan*60,
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Rp. '.format_number_report($totalpenjualan, 2)
        ),


    )



));
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dpbiaya,
    'mergeColumns' => is_report_excel() ? array() : array('group_name','account_name'),
    //'extraRowPos' => 'above',
    //'extraRowColumns' => array('group_name'),
    //'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
/*        array(
            'header' => 'Group Name',
            'name' => 'group_name',
        ),*/
        array(
            'header' => 'Account',
            'name' => 'account_name',
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Total'
        ),
        array(
            'header' => 'Amount',
            'name' => 'total',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Rp. '.format_number_report($totalbiaya, 2)
        ),
        array(
            'header' => '%',
            'name' => 'hpp',
            'value' => ''.$hpp,
        ),
        array(
            'header' => 'Tahun 1',
            'name' => 'total',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Rp. '.format_number_report($totalbiaya, 2)
        ),
        array(
            'header' => 'Tahun 2',
            'name' => 'total',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Rp. '.format_number_report($totalbiaya, 2)
        ),
        array(
            'header' => 'Tahun 3',
            'name' => 'total',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Rp. '.format_number_report($totalbiaya, 2)
        ),
        array(
            'header' => 'Tahun 4',
            'name' => 'total',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Rp. '.format_number_report($totalbiaya, 2)
        ),
        array(
            'header' => 'Tahun 5',
            'name' => 'total',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Rp. '.format_number_report($totalbiaya, 2)
        ),


    )



));



?>
<h3>TOTAL BIAYA         :  Rp. <?= format_number_report($totalbiaya, 2) ?></h3>
<h3>LABA BERSIH         :  Rp. <?= format_number_report($lababersih, 2) ?></h3>
<h3>INVESTASI           :  Rp. <?= format_number_report($totalinvestasi, 2) ?></h3>

<h3>BEP                 : <?= Round($totalinvestasi/$lababersih) ?> Tahun.</h3>


<?

$rows = 10; // amout of tr
$cols = 10;// amjount of td
function drawTable($rows, $cols){
    echo "<table border='1'>";

    for($tr=1;$tr<=$rows;$tr++){


        echo "<tr>";
        for($td=1;$td<=$cols;$td++){
            echo "<td align='center'>".$tr*$td."</td>";
        }
        echo "</tr>";

    }

    echo "</table>";
}

drawTable(Round($totalinvestasi/$lababersih), Round($totalinvestasi/$lababersih));
?>
