<h1>Report Asset Total (Non Active)</h1>
<h3>From : <?= $start ?></h3>
<h3>To : <?= $to ?></h3>
<h3>Asset Category : <?= $category ?></h3>
<h3>Branch/Store : <?= $branch ?></h3>
<?
$this->pageTitle = 'Report Asset Total (Non Active)';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    //'mergeColumns' => is_report_excel() ? array() : array('docref'),
    
    'columns' => array(

        array(
            'header' => 'Category',
            'name' => 'category'
        ),
        array(
            'header' => 'Asset',
            'name' => 'asset'
        ),
        array(
            'header' => 'Asset Code',
            'name' => 'kode'
        ),
        array(
            'header' => 'Qty',
            'name' => 'qty',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Total',
            'footerHtmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Total',
            'name' => 'total',
            'value' => function ($data) {
                return format_number_report($data['total'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2)
        ),
        array(
            'header' => 'Total Nilai Buku',
            'name' => 'balance',
            'value' => function ($data) {
                return format_number_report($data['balance'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalbalance, 2)
        )
    )
));
?>