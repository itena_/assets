<h1>Produksi Summary</h1>
<h3>FROM : <?=$start?></h3>
<h3>TO : <?=$to?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?php
$this->pageTitle='Produksi Summary';

$columns = [];
$rec = $dp->rawData;
if(count($rec)>0)
foreach($rec[0] as $k=>$v){
    $clm = array(
        'name' => $k
    );
    
    switch ($k){
        case 'tgl': $clm['header'] = 'Tgl';break;
        case 'doc_ref': $clm['header'] = 'Docref Produksi';break;
        case 'kode_barang': $clm['header'] = 'Kode Barang';break;
        case 'nama_barang': $clm['header'] = 'Nama Barang';break;
        case 'name': $clm['header'] = 'User';break;
        case 'doc_ref_produksi': $clm['header'] = 'No. Referensi';break;
        case 'qty':
            $clm['header'] = 'Qty';
            $clm['value'] = function ($data) {
                    return format_number_report($data['qty'], 2);
                };
            $clm['htmlOptions'] = array ('style' => 'text-align: right;' );
            break;
    }
    
    array_push($columns, $clm);
}

$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array(),
    'columns' => $columns
));
?>