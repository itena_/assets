<h1>Fee Card</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Fee Card';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => array('tgl'),
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Payment Method',
            'name' => 'nama_bank'
        ),
        array(
            'header' => 'Card',
            'name' => 'card_name',
            'footer' => "Total :",
            'footerHtmlOptions' => array('style' => 'text-align: right; font-weight: bold;'),
        ),
        array(
            'header' => 'Amount',
            'name' => 'amount',
            'value' => function ($data) {
                return format_number_report($data['amount'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right; font-weight: bold;'),
            'footer' => format_number_report($amount, 2)
        ),
        array(
            'header' => 'Fee',
            'name' => 'fee',
            'value' => function ($data) {
                return format_number_report($data['fee'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right; font-weight: bold;'),
            'footer' => format_number_report($fee, 2)
        ),
        array(
            'header' => 'Piutang Card',
            'name' => 'piutang',
            'value' => function ($data) {
                return format_number_report($data['piutang'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right; font-weight: bold;'),
            'footer' => format_number_report($piutang, 2)
        ),
    ),
));
?>