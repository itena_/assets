<h1>Report Piutang</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<?
$this->pageTitle = 'Report Piutang';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $R_ReportPiutang,
    'columns' => array(
        array(
            'header' => 'Customer',
            'name' => 'customer'
        ),
        array(
            'header' => 'Saldo Awal',
            'name' => 'saldo_awal',
	        'value' => function ($data) {
		        return format_number_report($data['saldo_awal'], 2);
	        },
	        'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Debet',
            'name' => 'debet',
	        'value' => function ($data) {
		        return format_number_report($data['debet'], 2);
	        },
	        'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Kredit',
            'name' => 'kredit',
	        'value' => function ($data) {
		        return format_number_report($data['kredit'], 2);
	        },
	        'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Saldo akhir',
            'name' => 'saldo_akhir',
            'value' => function ($data) {
                return format_number_report($data['saldo_akhir'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));
?>