<h1>Rekening Koran</h1>
<h3>FROM : <?=$start?></h3>
<h3>TO : <?=$to?></h3>
<h3>BANK : <?=$bank->nama_bank; ?></h3>
<?
$this->pageTitle='Rekening Koran - '.$bank->nama_bank;;
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Doc. Ref',
            'name' => 'ref'
        ),
        array(
            'header' => 'Branch',
            'name' => 'store'
        ),
        array(
            'header' => 'Memo',
            'name' => 'memo_'
        ),
        array(
            'header' => 'Comments',
            'name' => 'comment_'
        ),
        array(
            'header' => 'Debit',
            'name' => 'Debit',
            'value' => function ($data) {
                    return format_number_report($data['Debit'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'Credit',
            'name' => 'Credit',
            'value' => function ($data) {
                    return format_number_report($data['Credit'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'Balance',
            'name' => 'Balance',
            'value' => function ($data) {
                    return format_number_report($data['Balance'], 2);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        )
    )
));
?>