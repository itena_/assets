<h1>Info Efektivitas</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Info Efektivitas';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Informasi',
            'name' => 'info_name'
        ),
        array(
            'header' => 'Total',
            'name' => 'total',
            'value' => function ($data) {
                return format_number_report($data['total'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    ),
));
?>