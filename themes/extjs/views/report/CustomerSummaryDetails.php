<h1>Customer Summary Details</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>CUSTOMER : <?= $customer ?></h3>
<?
$this->pageTitle = 'Sales Summary Receipt Details';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => is_report_excel() ? array() : array('doc_ref', 'tgl', 'ketdisc', 'store'),
    'extraRowColumns' => is_report_excel() ? array() : array('doc_ref'),
    'extraRowPos' => 'below',
    'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
        array(
            'header' => "No. ". $pt_negara ." Receipt",
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Comments',
            'name' => 'ketdisc'
        ),
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Store',
            'name' => 'store'
        ),
        array(
            'header' => 'Item Code',
            'name' => 'kode_barang',
            'footer' => 'Total'
        ),
        array(
            'header' => 'Quantity',
            'name' => 'qty',
            'value' => function ($data) {
                return format_number_report($data['qty']);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($qty)
        ),
        array(
            'header' => 'Price',
            'name' => 'price',
            'value' => function ($data) {
                return format_number_report($data['price'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($price, 2)
        ),
        array(
            'header' => 'Bruto',
            'name' => 'bruto',
            'value' => function ($data) {
                return format_number_report($data['bruto'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($bruto, 2)
        ),
        array(
            'header' => 'Discount',
            'name' => 'discrp',
            'value' => function ($data) {
                return format_number_report($data['discrp'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($discrp, 2)
        ),
        array(
            'header' => 'VAT',
            'name' => 'vatrp',
            'value' => function ($data) {
                return format_number_report($data['vatrp'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($vatrp, 2)
        ),
        array(
            'header' => 'Total',
            'name' => 'total',
            'value' => function ($data) {
                return format_number_report($data['total'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total, 2)
        )
    ),
));
?>