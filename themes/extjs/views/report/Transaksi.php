<h1>Sales</h1>
<h3>SHOW BY : <?= $showby ?></h3>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>

<?
$this->pageTitle = 'Sales';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'extraRowPos' => 'below',
    'mergeColumns' => is_report_excel() ? array() : array('docref','tdate','kodeoutlet','jumlahoutlet'),
    'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
        array(
            'header' => 'Doc.Ref',
            'name' => 'docref'
        ),
        array(
            'header' => 'Date',
            'name' => 'tdate'
        ),
        array(
            'header' => 'Outlet',
            'name' => 'kodeoutlet'
        ),
        array(
            'header' => 'Num Of Outlet',
            'name' => 'jumlahoutlet'
        ),
        array(
            'header' => 'Group Product',
            'name' => 'kodegroup'
        ),
        array(
            'header' => 'Product',
            'name' => 'kodeproduk'
        ),
        array(
            'header' => 'Price',
            'name' => 'harga',
            'htmlOptions' => array('style' => 'text-align: right;'),
        ),
        array(
            'header' => 'Qty',
            'name' => 'qty',
            'htmlOptions' => array('style' => 'text-align: right;'),
        ),
        array(
            'header' => '%',
            'name' => 'persentase',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footer' => 'Total'
        ),
        array(
            'header' => 'Sales(Qty)',
            'name' => 'salesqty',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalsalesqty, 2)
        ),
        array(
            'header' => 'Sales(Rp)',
            'name' => 'salesrp',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalsalesrp, 2)
        ),
    )
));
?>