<h1>Sales Detail</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>

<?
$this->pageTitle = 'Sales Detail';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'extraRowPos' => 'below',
    'mergeColumns' => is_report_excel() ? array() : array('docref','tdate'),
    'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["total_faktur"],2)."</span>"',
    'columns' => array(
        array(
            'header' => 'Doc.Ref',
            'name' => 'docref'
        ),
        array(
            'header' => 'Date',
            'name' => 'tdate'
        ),
        array(
            'header' => 'Name',
            'name' => 'name',
        ),
        array(
            'header' => 'Descriptions',
            'name' => 'description',
            'footer' => 'Total',
            'footerHtmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Amount',
            'name' => 'amount',
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($totalsales, 2)
        ),
    )
));
?>