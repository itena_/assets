<h1>Rekap Purchase Order</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>Supplier : <?= $supplier_name ?></h3>
<?
$this->pageTitle = 'Rekap Purchase Order';
$gridColums = array();
array_push($gridColums,
        array(
            'header' => 'Kode Barang',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Nama Barang',
            'name' => 'nama_barang'
        ),
        array(
            'header' => 'Satuan',
            'name' => 'sat'
        )
);

foreach($store as $s){
    array_push($gridColums,
        array(
            'header' => $s['store'],
            'name' => $s['store']
        )
    );
}

array_push($gridColums,
        array(
            'header' => 'Total',
            'name' => 'total',
            'value' => function ($data) {
                return number_format($data['total']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
);
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => $gridColums
));