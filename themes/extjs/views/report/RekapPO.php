<?
    switch($status){
        case "All": break;
        case PO_OPEN                : $status = "OPEN"; break;
        case PO_PARTIALLY_RECEIVED  : $status = "PARTIALLY RECEIVED"; break;
        case PO_RECEIVED            : $status = "RECEIVED"; break;
        case PO_CLOSED              : $status = "CLOSED"; break;
        default                     : $status = "All";
    }
?>
<h1><? echo $show_detail?'Rekap Purchase Order Details':'Rekap Purchase Order'; ?></h1>
<table>
    <tbody>
        <tr><td>Date</td><td> : <?=$from?> to <?=$to?></td></tr>
        <tr><td>Supplier</td><td> : <?=$supplier?></td></tr>
        <tr><td>Product</td><td> : <?=$barang?></td></tr>
        <tr><td>Status</td><td> : <?=$status?></td></tr>
    </tbody>
</table>
<?
$gridColums = array();
    array_push($gridColums,
        array(
            'header' => 'TGL',
            'name' => 'tgl',
            'htmlOptions' => array ('style' => 'white-space: nowrap;' )
        ),
        array(
            'header' => 'NO. PO',
            'name' => 'doc_ref',
            'htmlOptions' => array ('style' => 'white-space: nowrap;' ),
        ),
        array(
            'header' => 'STORE',
            'name' => 'store',
            'htmlOptions' => array ('style' => 'white-space: nowrap;' ),
        ),
        array(
            'header' => 'SUPPLIER',
            'name' => 'supplier_name',
            'htmlOptions' => array ('style' => 'white-space: nowrap;' )
        )
    );
    
if($show_detail==TRUE){
    array_push($gridColums,
        array(
            'header' => 'KODE PRODUCT',
            'name' => 'kode_barang',
            'htmlOptions' => array ('style' => 'white-space:nowrap;' )
        ),
        array(
            'header' => 'NAMA PRODUCT',
            'name' => 'nama_barang',
            'htmlOptions' => array ('style' => 'white-space:nowrap;' )
        ),
        array(
            'header' => 'SATUAN',
            'name' => 'sat',
            'htmlOptions' => array ('style' => 'white-space:nowrap;' )
        ),
        array(
            'header' => 'QTY PO',
            'value' => function ($data) {
                return number_format($data['qty'],2,",",".");
            },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'PRICE',
            'value' => function ($data) {
                return number_format($data['price'],2,",",".");
            },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'TGL DIBUTUHKAN',
            'name' => 'tgl_delivery',
            'htmlOptions' => array ('style' => 'white-space:nowrap;text-align:center;' )
        ),
        array(
            'header' => 'TGL SURAT JALAN',
            'name' => 'tgl_sj',
            'htmlOptions' => array ('style' => 'white-space:nowrap;' ),
        ),
        array(
            'header' => 'QTY SURAT JALAN',
            'value' => function ($data) {
                return number_format($data['qty_sj'],2,",",".");
            },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'QTY OUTSTANDING',
            'value' => function ($data) {
                return number_format($data['qty_outstanding'],2,",",".");
            },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'QTY RETURN',
            'value' => function ($data) {
                return number_format($data['qty_returned'],2,",",".");
            },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        )
    );
}
    array_push($gridColums,
        array(
            'header' => 'STATUS',
            'value' => function ($data) {
                $status = "";
                switch($data['status']){
                    case PO_OPEN                : $status = "OPEN"; break;
                    case PO_PARTIALLY_RECEIVED  : $status = "PARTIALLY RECEIVED"; break;
                    case PO_RECEIVED            : $status = "RECEIVED"; break;
                    case PO_CLOSED              : $status = "CLOSED"; break;
                }
                return $status;
            },
            'htmlOptions' => array ('style' => 'text-align: center;' )
        )
    );
    
$this->widget('CGridViewPlus', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => $gridColums
));
?>