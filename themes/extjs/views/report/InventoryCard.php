<h1><?=$this->pageTitle ?></h1>
<h2>ITEM : <?= $item->kode_barang ?> - <?= $item->nama_barang ?></h2>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
//$this->pageTitle = 'Inventory Card';
$id = Yii::app()->user->getId();
$user = Users::model()->findByPk($id);
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => "No. ". $pt_negara ." Receipt",
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Cost Price',
            'name' => 'price',
            'visible' => $user->is_available_role(351),
            'value' => function ($data) {
                return format_number_report($data['price'], 2);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Beginning Balance',
            'name' => 'before',
            'value' => function ($data) {
                return format_number_report($data['before']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Quantity In',
            'name' => 'in',
            'value' => function ($data) {
                return format_number_report($data['in']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Quantity Out',
            'name' => 'out',
            'value' => function ($data) {
                return format_number_report($data['out']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Ending Balance',
            'name' => 'after',
            'value' => function ($data) {
                return format_number_report($data['after']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        )
    )
));
?>