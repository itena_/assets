<h1>Bonus Details</h1>
<h3>FROM : <?= $start ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>BRANCH : <?= $store ?></h3>
<?
$this->pageTitle = 'Bonus Details';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeType'=> 'nested',
    'mergeColumns' => array(),
    'extraRowColumns' => array('nama_employee'),
    'extraRowPos' => 'below',
    'extraRowTotals' => function ($data, $row, &$totals) {
        if (!isset($totals['sum'])) {
            $totals['sum'] = 0;
        }
        $totals['sum'] += $data['amount_bonus'];
    },
    'extraRowExpression' => '"<span class=\"subtotal\">Total ".$data["nama_employee"]." Nominal : ".format_number_report($totals["sum"])."</span>"',
    'columns' => array(
        array(
            'header' => 'Date',
            'name' => 'tgl'
        ),
        array(
            'header' => 'Employee Name',
            'name' => 'nama_employee'
        ),
        array(
            'header' => 'Customer',
            'name' => 'nama_customer'
        ),
        array(
            'header' => 'No. Receipt',
            'name' => 'doc_ref'
        ),
        array(
            'header' => 'Item Code',
            'name' => 'kode_barang',
            'footer' => "Total All Bonus"
        ),
        array(
            'header' => 'Price',
            'name' => 'price',
            'value' => function ($data) {
                return format_number_report($data['price']);
            },
            'htmlOptions' => array('style' => 'text-align: right;')
        ),    
        array(
            'header' => 'Bonus',
            'name' => 'amount_bonus',
            'value' => function ($data) {
                return format_number_report($data['amount_bonus']);
            },
            'htmlOptions' => array('style' => 'text-align: right;'),
            'footerHtmlOptions' => array('style' => 'text-align: right;'),
            'footer' => format_number_report($total_bonus)
        ),
        array(
            'header' => 'Jenis',
            'name' => 'bonus_name'
        )
    ),
));
?>