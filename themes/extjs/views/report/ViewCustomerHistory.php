<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <title>Customer History</title>
    <style type="text/css">
        body {
            font-family: helvetica, tahoma, verdana, sans-serif;
            padding: 20px;
            padding-top: 32px;
            font-size: 13px;
            background-color: #F8DBE6 !important;
            color: #8b1d51 !important;
        }

        .grid-view table.items {
            background: white;
            border-collapse: collapse;
            width: 100%;
            border: 1px #a83a6e solid !important;
        }

        .grid-view table.items th, .grid-view table.items td {
            color: #8b1d51 !important;
            font-size: 0.9em;
            border: 1px white solid;
            border-color: #ba4c80 #a83a6e #a83a6e !important;
            padding: 0.3em;
        }

        .grid-view table.items th {
            color: #8b1d51 !important;
            background: #ffe8ff !important;
            text-align: center;
        }

        .grid-view table.items th a {
            color: #EEE;
            font-weight: bold;
            text-decoration: none;
        }

        .grid-view table.items th a:hover {
            color: #FFF;
        }
        .grid-view table.items tr.even {
            background: #ecceed;
        }

        .grid-view table.items tr.odd {
            background: #ffe8ff;
        }

        .grid-view table.items tr.selected {
            background: #cbadcc;
            border-color: #67002d;
        }

        .grid-view table.items tr:hover.selected {
            border-color: #982a5e;
            background: #eed0ef;
        }

        .grid-view table.items tbody tr:hover {
            background: #FFF5FD;
        }

        .grid-view .link-column img {
            border: 0;
        }

        .grid-view .button-column {
            text-align: center;
            width: 60px;
        }

        .grid-view .button-column img {
            border: 0;
        }

        .grid-view .checkbox-column {
            width: 15px;
        }

        .grid-view .summary {
            margin: 0 0 5px 0;
            text-align: right;
        }

        .grid-view .pager {
            margin: 5px 0 0 0;
            text-align: right;
        }

        .grid-view .empty {
            font-style: italic;
        }

        .grid-view .filters input,
        .grid-view .filters select {
            width: 100%;
            border: 1px solid #ccc;
        }

        /* grid border */
        .grid-view table.items th, .grid-view table.items td {
            border: 1px solid #a83a6e !important;
        }

        /* disable selection for extrarows */
        .grid-view td.extrarow {
            background: none repeat scroll 0 0 #F8F8F8;
        }

        .subtotal {
            font-size: 14px;
            color: brown;
            font-weight: bold;
        }
    </style>
</head>
<body>
<div style="height: 100px; left: 0px; top: 0px;">
    <img alt="" src="<?= bu() .URLLOGO ?>">
</div>
<h1>Customer History</h1>
<h2>After <?= $start; ?></h2>
<h3>
    Customer Number : <?=$cust->no_customer?><br>
    Customer Name : <?=$cust->nama_customer?><br>
    Birthday : <?=sql2date($cust->tgl_lahir,'dd MMM yyyy')?><br>
    Phone : <?=$cust->telp?><br>
    Address : <?=$cust->alamat?><br>
</h3>
<?php
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    'mergeColumns' => array('doc_ref'),
    'columns' => array(
        array(
            'header' => "No. ". $pt_negara ." Receipt",
            'name' => 'doc_ref',
            'value' => function ($data) {
                    return $data['doc_ref'] . " (" .sql2date($data['tgl'],'dd MMM yyyy') . ")";
                },
        ),
        array(
            'header' => 'Item Code',
            'name' => 'kode_barang'
        ),
        array(
            'header' => 'Quantity',
            'name' => 'qty',
            'value' => function ($data) {
                    return number_format($data['qty']);
                },
            'htmlOptions' => array ('style' => 'text-align: right;' )
        ),
        array(
            'header' => 'Unit',
            'name' => 'sat'
        ),
    )
));
?>
</body>
</html>