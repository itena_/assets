<h1>All Investment Analysis</h1>
<h3>FROM : <?= $from ?></h3>
<h3>TO : <?= $to ?></h3>
<h3>ACCOUNT CODE : <?= $accountcode ?></h3>
<h3>ACCOUNT NAME : <?= $accountname ?></h3><?
$this->pageTitle = 'All Investment Analysis';
$this->widget('ext.groupgridview.GroupGridView', array(
    'id' => 'the-table',
    'dataProvider' => $dp,
    //'mergeColumns' => is_report_excel() ? array() : array('account_name','alltotal'),
    'extraRowPos' => 'below',
    'extraRowExpression' => '"<span class=\"subtotal\">Total Transaction : ".format_number_report($data["AmountBudget"],2)."</span>"',
    'columns' => array(

        array(
            'header' => 'Account Code',
            'name' => 'AccountCode'
        ),
        array(
            'header' => 'Account Name',
            'name' => 'AccountName'
        ),
        array(
            'header' => 'Amount Plan',
            'name' => 'AmountBudget',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Amount Realization',
            'name' => 'AmountRealization',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => 'Achievement',
            'name' => 'Achievement',
            'htmlOptions' => array('style' => 'text-align: right;')
        ),
        array(
            'header' => '%',
            'name' => 'AchievementPercent',
            'htmlOptions' => array('style' => 'text-align: center;')
        )
    )
));
?>