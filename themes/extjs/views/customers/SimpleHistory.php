<?php
$ref = null;
$same = false;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        #tbody-hor-minimalis-b::-webkit-scrollbar {
            width: 0px;
            background: transparent;
        }

        ::-webkit-scrollbar-thumb {
            /*background: #FF0000;*/
            background: #FFF;
        }

        #hor-minimalist-b {
            font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
            font-size: 12px;
            background: #fff;
            margin: 25px;
            border-collapse: collapse;
            text-align: left;
            height: <?php echo $height; ?>;
        }

        .simpletable {
            display: flex;
            flex-flow: column;
            height: 100%;
            width: 95%;
        }

        #hor-minimalist-b th {
            font-size: 14px;
            font-weight: normal;
            color: #039;
            padding: 10px 8px;
            border-bottom: 2px solid #6678b1;
            border-right: 1px solid #6678b1;
        }

        #hor-minimalist-b td {
            border-bottom: 1px solid #ccc;
            border-right: 1px solid #ccc;
            color: #669;
            padding: 6px 8px;
        }

        #hor-minimalist-b tbody tr:hover td {
            color: #009;
        }

        #hor-minimalist-b tbody {
            /* body takes all the remaining available space */
            flex: 1 1 auto;
            display: block;
            overflow-y: scroll;
        }

        .simpletable thead tr,
        .simpletable tfoot tr,
        .simpletable tbody tr {
            display: table;
            table-layout: fixed;
            width: 100%;
        }

        .faktur {
            width: 20%;
            overflow-wrap: break-word;
        }

        .tgl {
            width: 10%;
        }

        .cbg {
            width:10%;
            overflow-wrap: break-word;
        }
        .qty {
            width: 5%;
        }

        .comm {
            width: 5%;
        }

        .diag {
            width: 20%;
            overflow-wrap: break-word;
        }

        .scroll {
            overflow: scroll;
        }
        table tr td
        {
            font-size: 14px;
        }
    </style>
</head>
<body>
<table id="hor-minimalist-b" class="simpletable " summary="Employee Pay Sheet">
    <thead>
    <tr>
        <th scope="col"class="faktur">No Faktur</th>
        <th scope="col" class="tgl">Tanggal</th>
        <th scope="col" class="tgl">Kode</th>
        <th scope="col" class="qty" style="text-align: right;">Qty</th>
        <!--        <th scope="col" class="qty">Unit</th>
                <th scope="col" class="faktur">Note</th>-->

        <th scope="col" class="diag">Note</th>
        <th scope="col" class="diag">Com</th>
        <th scope="col" class="cbg">Cabang</th>
        <th scope="col" class="diag">Diagnosa</th>
    </tr>
    </thead>
    <?php if (empty($pasien)) : ?>
        <tfoot>
        <tr>
            <th colspan="100%" style="text-align: center;">
                No Data
            </th>
        </tr>
        </tfoot>
    <?php else : ?>
        <tbody id="tbody-hor-minimalis-b">
        <?php foreach ($pasien as $item) : ?>
            <?php
            $kode='';
            if (isset($item['diagnosa_id'])) {
                if ($ref != $item['konsul_id']) {
                    $ref = $item['konsul_id'];
                    $same = false;
                } else {
                    $same = true;
                }
                $no_faktur = '';
                $tgl = $item['tgld'];
                $ketdisc = '';
                $kd_brng = '';
                $qty = '';
                $diag = $item['diag'];
                $note = $item['note_'];
            } else {
                if ($ref != $item['no_faktur']) {
                    $ref = $item['no_faktur'];
                    $same = false;
                } else {
                    $same = true;
                }
                $no_faktur = $item['no_faktur'];
                $tgl = $item['tgl'];
                $ketdisc = $item['ketdisc'];
                $kd_brng = $item['kd_brng'];
                $qty = number_format($item['qty'], 0);
                $diag = '';
                $note = $item['ketpot'];
                $kode = $item['kode'];
            }
            ?>
            <tr>
                <td class="faktur"><?php echo($same ? '' : $no_faktur); ?></td>
                <td class="tgl"><?php echo($same ? '' : $tgl); ?></td>
                <td class="cbg"><?php echo $kd_brng; ?></td>
                <td class="qty" style="text-align: right;"><?php echo $qty; ?></td>
                <td class="diag"><?php echo $note; ?></td>
                <td class="diag"><?php echo($same ? '' : $ketdisc); ?></td>
                <td class="cbg"><?php echo $kode; ?></td>
                <td class="diag"><?php echo $diag; ?></td>

            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php endif; ?>
</table>
</body>
</html>