// function launchQZ() {
//     if (!qz.websocket.isActive()) {
//         startConnection({retries: 5, delay: 1});
//     }
// }
function chr(i) {
    return String.fromCharCode(i);
}
var __cutPaper = [{type: 'raw', data: chr(27) + chr(105), options: {language: 'ESCP', dotDensity: 'double'}}];
var __feedPaper = [{
    type: 'raw',
    data: chr(27) + chr(100) + chr(6),
    options: {language: 'ESCP', dotDensity: 'double'}
}];
var __feedReversePaper = [{
    type: 'raw',
    data: chr(27) + chr(75) + chr(3),
    options: {language: 'ESCP'}
}];
var __openCashDrawer = [{
    type: 'raw',
    data: chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r",
    options: {language: 'ESCP', dotDensity: 'double'}
}];
function __getPrintData(tipe_) {
    var __printDataTM82 = [{
        type: 'raw', data: chr(27) + chr(64) + //initial
        chr(27) + chr(51) + chr(50), //+ //space vertical
        //chr(27) + chr(77) + chr(49),// font B
        options: {language: 'ESCP', dotDensity: 'double'}
    }];
    var __printDataTM81 = [{
        type: 'raw', data: chr(27) + chr(64) + //initial
        chr(27) + chr(51) + chr(25), //+ //space vertical
        //chr(27) + chr(77) + chr(49),// font B
        options: {language: 'ESCP', dotDensity: 'double'}
    }];
    var __printDataTMU220 = [{
        type: 'raw', data: chr(27) + chr(64) + //initial
        chr(27) + chr(51) + chr(25), //+ //space vertical
        //chr(27) + chr(77) + chr(49),// font B
        options: {language: 'ESCP', dotDensity: 'double'}
    }];
    var __print = '';
    switch (tipe_) {
        case 'tmt82' :
            __print = __printDataTM82;
            break;
        case 'tmt81' :
            __print = __printDataTM81;
            break;
        case 'tmtu220' :
            __print = __printDataTMU220;
            break;
        default :
            __print = __printDataTM82;
            break;
    }
    return __print;
}
PRINTER_RECEIPT = 'default';
__printData = [{
    type: 'raw', data: chr(27) + chr(64) + //initial
    chr(27) + chr(51) + chr(50), //+ //space vertical
    //chr(27) + chr(77) + chr(49),// font B
    options: {language: 'ESCP', dotDensity: 'double'}
}];
PRINTER_STOCKER = 'default';
__printDataStocker = [{
    type: 'raw', data: chr(27) + chr(64) + //initial
    chr(27) + chr(51) + chr(50) + //space vertical
    chr(27) + chr(77) + chr(49),// font B
    options: {language: 'ESCP', dotDensity: 'double'}
}];
PRINTER_BEAUTY = 'default';
__printDataBeauty = [{
    type: 'raw', data: chr(27) + chr(64) + //initial
    chr(27) + chr(51) + chr(25),// + //space vertical
    //  chr(27) + chr(77) + chr(49),// font B
    options: {language: 'ESCP', dotDensity: 'double'}
}];
PRINTER_CARD = 'default';
COM_POSIFLEX = 'COM3';
POSCUSTOMERREADONLY = false;
POSCUSTOMERDEFAULT = '';
var local = localStorage.getItem("settingClient");
if (local !== null) {
    var data_ = JSON.parse(local);
    var __value = JSON.search(data_, '//data[name_="PRINTER_RECEIPT"]/value_')[0];
    if (__value !== '' && __value !== undefined) {
        PRINTER_RECEIPT = __value.name;
        __printData = __getPrintData(__value.tipe);
    }
    __value = JSON.search(data_, '//data[name_="PRINTER_STOCKER"]/value_')[0];
    if (__value !== '' && __value !== undefined) {
        PRINTER_STOCKER = __value.name;
        __printDataStocker = __getPrintData(__value.tipe);
    }
    __value = JSON.search(data_, '//data[name_="PRINTER_BEAUTY"]/value_')[0];
    if (__value !== '' && __value !== undefined) {
        PRINTER_BEAUTY = __value.name;
        __printDataBeauty = __getPrintData(__value.tipe);
    }
    __value = JSON.search(data_, '//data[name_="PRINTER_CARD"]/value_')[0];
    if (__value !== '' && __value !== undefined) {
        PRINTER_CARD = __value;
    }
    __value = JSON.search(data_, '//data[name_="COM_POSIFLEX"]/value_')[0];
    if (__value !== '' && __value !== undefined) {
        COM_POSIFLEX = __value;
    }
    __value = JSON.search(data_, '//data[name_="POSCUSTOMERREADONLY"]/value_')[0];
    if (__value !== '' && __value !== undefined) {
        POSCUSTOMERREADONLY = __value;
    }
}
function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}
function openSerialPort(serialBegin, serialEnd, serialWidth, serialPort) {
    var widthVal = serialWidth;
    if (!widthVal) {
        widthVal = null;
    }
    var bounds = {
        begin: serialBegin,
        end: serialEnd,
        width: widthVal
    };
    qz.serial.openPort(serialPort, bounds).then(function () {
        displayMessage("Serial port opened");
    }).catch(displayError);
}
function __sendSerialData(serialPort, serialCmd) {
    var properties = {
        baudRate: 9600,
        dataBits: 8,
        stopBits: 1,
        parity: "NONE",
        flowControl: "NONE"
    };
    qz.serial.sendDataNwis(serialPort, serialCmd, properties).catch(displayError);
}
function closeSerialPort(serialPort) {
    qz.serial.closePort(serialPort).then(function () {
        displayMessage("Serial port closed");
    }).catch(displayError);
}
window.qzVersion = 0;
var cfg = null;
function getUpdatedConfig() {
    // if (cfg == null) {
    cfg = qz.configs.create(null);
    // }
    updateConfig();
    return cfg;
}
function updateConfig() {
    var pxlSize = null;
    var pxl = false;
    if (pxl) {
        pxlSize = {
            width: "",
            height: ""
        };
    }
    var pxlMargins = "0";
    var margin = false;
    if (margin) {
        pxlMargins = {
            top: "0",
            right: "0",
            bottom: "0",
            left: "0"
        };
    }
    var copies = 1;
    var jobName = null;
    var rawTab = true;
    if (rawTab) {
        copies = "1";
        jobName = "";
    } else {
        copies = "1";
        jobName = "";
    }
    cfg.reconfigure({
        language: "escp",
        altPrinting: false,
        encoding: "",
        endOfDoc: "",
        perSpool: "1",
        colorType: "grayscale",
        copies: copies,
        jobName: jobName,
        density: "",
        duplex: false,
        interpolation: "",
        margins: pxlMargins,
        orientation: "",
        paperThickness: "",
        printerTray: "",
        rotation: "0",
        scaleContent: false,
        size: pxlSize,
        units: "in"
    });
}
function updateState(text, css) {
    console.log(text);
}
function displayMessage(msg, css) {
    console.log(msg);
}
function displayError(err) {
    console.log(err);
}
function findVersion() {
    qz.api.getVersion().then(function (data) {
        displayMessage("qzVersion : " + data);
        window.qzVersion = data;
    }).catch(displayError);
}
function handleConnectionError(err) {
    updateState('Error', 'danger');
    if (err.target != undefined) {
        if (err.target.readyState >= 2) { //if CLOSING or CLOSED
            displayError("Connection to QZ Tray was closed");
        } else {
            displayError("A connection error occurred, check log for details");
            console.error(err);
        }
    } else {
        displayError(err);
    }
}
function setPrinter(printer) {
    var cf = getUpdatedConfig();
    cf.setPrinter(printer);
    if (typeof printer === 'object' && printer.name == undefined) {
        var shown;
        if (printer.file != undefined) {
            shown = "FILE: " + printer.file;
        }
        if (printer.host != undefined) {
            shown = "HOST: " + printer.host + ":" + printer.port;
        }
        console.log(shown);
    } else {
        if (printer.name != undefined) {
            printer = printer.name;
        }
        if (printer == undefined) {
            printer = 'NONE';
        }
        console.log(printer);
    }
}
function startConnection(config) {
    if (!qz.websocket.isActive()) {
        updateState('Waiting', 'default');
        qz.websocket.connect(config).then(function () {
            updateState('Active', 'success');
            findVersion();
        }).catch(handleConnectionError);
    }
}
function endConnection() {
    if (qz.websocket.isActive()) {
        qz.websocket.disconnect().then(function () {
            updateState('Inactive', 'default');
        }).catch(handleConnectionError);
    } else {
        displayMessage('No active connection with QZ exists.', 'alert-warning');
    }
}
/**
 * Optionally used to deploy multiple versions of the applet for mixed
 * environments.  Oracle uses document.write(), which puts the applet at the
 * top of the page, bumping all HTML content down.
 */
function is_enable_tools() {
    try {
        var gui = require('nw.gui');
        if (gui != null) {
            var win = gui.Window.get();
            win.maximize();
            // win.showDevTools();
            return true;
        }
    } catch (err) {
        console.log(err.message);
        return false;
    }
}
function notReady() {
    // If applet is not loaded, display an error
    if (!isLoaded()) {
        return true;
    }
    // If a printer hasn't been selected, display a message.
    //        else if (!qz.getPrinter()) {
    //            console.log('Please select a printer first by using the "Detect Printer" button.');
    //            return true;
    //        }
    return false;
}
function isLoaded() {
    // if (!is_enable_tools()) {
    //     return false;
    // }
    if (!qz) {
        console.log('Error:\n\n\tPrint plugin is NOT loaded!');
        return false;
    } else {
        try {
            if (!qz.websocket.isActive()) {
                //console.log('Error:\n\n\tPrint plugin is loaded but NOT active!');
                startConnection();
                return false;
            }
        } catch (err) {
            console.log('Error:\n\n\t' + err);
            return false;
        }
    }
    return true;
}
function findPrinter(query, set) {
    qz.printers.find(query).then(function (data) {
        displayMessage("Found: " + data);
        if (set) {
            setPrinter(data);
        }
    }).catch(displayError);
}
function findDefaultPrinter(set) {
    qz.printers.getDefault().then(function (data) {
        displayMessage("Found: " + data);
        if (set) {
            setPrinter(data);
        }
    }).catch(displayError);
}
function findPrinterReceipt() {
    var name = PRINTER_RECEIPT;
    findPrinter(name, true);
    // if (isLoaded()) {
    //     qz.findPrinter(name);
    //     window['qzDoneFinding'] = function () {
    //         var printer = qz.getPrinter();
    //         console.log(printer !== null ? 'Printer found: "' + printer +
    //         '" after searching for "' + name + '"' : 'Printer "' +
    //         name + '" not found.');
    //         window['qzDoneFinding'] = null;
    //     };
    //     while (!qz.isDoneFinding()) {
    //     }
    // }
}
function findPrinterCard() {
    var name = PRINTER_CARD;
    findPrinter(name, true);
    // if (isLoaded()) {
    //     qz.findPrinter(name);
    //     window['qzDoneFinding'] = function () {
    //         var printer = qz.getPrinter();
    //         console.log(printer !== null ? 'Printer found: "' + printer +
    //         '" after searching for "' + name + '"' : 'Printer "' +
    //         name + '" not found.');
    //         window['qzDoneFinding'] = null;
    //     };
    //     while (!qz.isDoneFinding()) {
    //     }
    // }
}
function appendEPCL(data) {
    if (data == null || data.length == 0) {
        return;
    }
    qz.appendHex('x1b');
    qz.append(data);
    qz.appendHex('x0D');
}
function printHTML(printer, msg) {
    if (notReady()) {
        return;
    }
    if (printer == 'default') {
        findDefaultPrinter(true);
    } else {
        findPrinter(printer, true);
    }
    var config = getUpdatedConfig();
    var printData = [
        {type: 'raw', format: 'hex', data: '1B4D'},
        {type: 'raw', data: msg}
    ];
    setTimeout(function () {
        qz.print(config, printData).catch(displayError);
    }, 500);
    // var exec = require('child_process').exec,
    //     child;
    // msg = base64_encode(msg);
    // child = exec('java -jar ./qz.jar printHTML "' + printer + '" ' + msg, 'shell',
    //     function (error, stdout, stderr) {
    //         console.log('stdout: ' + stdout);
    //         console.log('stderr: ' + stderr);
    //         if (error !== null) {
    //             console.log('exec error: ' + error);
    //         }
    //     });
}
function print(printer, printData) {
    if (notReady()) {
        return;
    }
    setTimeout(function () {
        if (printer == 'default') {
            qz.printers.getDefault().then(function (data) {
                var cf = getUpdatedConfig();
                cf.setPrinter(data);
                qz.print(cf, printData).catch(displayError);
            }).catch(displayError);
        } else {
            qz.printers.find(printer).then(function (data) {
                var cf = getUpdatedConfig();
                cf.setPrinter(data);
                qz.print(cf, printData).catch(displayError);
            }).catch(displayError);
        }
    }, 1000);
}
function opencashdrawer(printer) {
    if (notReady()) {
        return;
    }
    printRaw(printer, 'hex', '1B401B70301919');
}
function printCard(type, cust_no, cust_name, since, valid) {
    //alert(type + " " + cust_no + " " + cust_name + " " + since + " " + valid);
    //appendEPCL('+RIB');
    //appendEPCL('+C 4');
    //appendEPCL('F');
    //switch (type) {
    //    case 'MEN':
    //        appendEPCL('T 50 450 0 1 0 75 1 ' + cust_no);
    //        appendEPCL('T 50 520 0 1 0 65 1 ' + cust_name);
    //        appendEPCL('T 535 575 0 1 0 45 1 ' + since);
    //        appendEPCL('T 785 575 0 1 0 45 1 ' + valid);
    //        break;
    //    case 'TEEN':
    //        appendEPCL('T 50 450 0 1 0 75 1 ' + cust_no);
    //        appendEPCL('T 50 520 0 1 0 65 1 ' + cust_name);
    //        appendEPCL('T 170 575 0 1 0 45 1 ' + valid);
    //        appendEPCL('T 785 575 0 1 0 45 1 ' + since);
    //        break;
    //    case 'WOMEN':
    //        appendEPCL('T 50 400 0 1 0 75 1 ' + cust_no);
    //        appendEPCL('T 50 470 0 1 0 65 1 ' + cust_name);
    //        appendEPCL('T 170 580 0 1 0 45 1 ' + since);
    //        appendEPCL('T 650 580 0 1 0 45 1 ' + valid);
    //        break;
    //}
    //appendEPCL('I');
    //qz.print();
    switch (type) {
        case 'MEN':
            cust_no = 'T 50 450 0 1 0 75 1 ' + cust_no;
            cust_name = 'T 50 520 0 1 0 65 1 ' + cust_name;
            since = 'T 535 575 0 1 0 45 1 ' + since;
            valid = 'T 785 575 0 1 0 45 1 ' + valid;
            break;
        case 'TEEN':
            cust_no = 'T 50 450 0 1 0 75 1 ' + cust_no;
            cust_name = 'T 50 520 0 1 0 65 1 ' + cust_name;
            since = 'T 170 575 0 1 0 45 1 ' + valid;
            valid = 'T 785 575 0 1 0 45 1 ' + since;
            break;
        case 'WOMEN':
            cust_no = 'T 50 400 0 1 0 75 1 ' + cust_no;
            cust_name = 'T 50 470 0 1 0 65 1 ' + cust_name;
            since = 'T 170 580 0 1 0 45 1 ' + since;
            valid = 'T 650 580 0 1 0 45 1 ' + valid;
            break;
    }
    msg = '{"cust_no":"' + cust_no + '","cust_name":"' + cust_name +
        '","since":"' + since + '","valid":"' + valid + '"}';
    var exec = require('child_process').exec,
        child;
    msg = base64_encode(msg);
    child = exec('java -jar ./qz.jar printCard "' + PRINTER_CARD + '" ' + msg, 'shell',
        function (error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            console.log('stderr: ' + stderr);
            if (error !== null) {
                console.log('exec error: ' + error);
            }
        });
}
function sendSerialData(msg) {
    if (notReady()) {
        return;
    }
    __sendSerialData(COM_POSIFLEX, msg);
    // if (notReady()) {
    //     return;
    // }
    // var exec = require('child_process').exec,
    //     child;
    // msg = base64_encode(msg);
    // child = exec('java -jar ./qz.jar ld ' + COM_POSIFLEX + ' ' + msg, 'shell',
    //     function (error, stdout, stderr) {
    //         console.log('stdout: ' + stdout);
    //         console.log('stderr: ' + stderr);
    //         if (error !== null) {
    //             console.log('exec error: ' + error);
    //         }
    //     });
}