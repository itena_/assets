jun.RestrictDateWin = Ext.extend(Ext.Window, {
    title: 'Restrict Date',
    modez:1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-RestrictDate',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                        {
                            xtype: "combo",
                            typeAhead: !0,
                            triggerAction: "all",
                            lazyRender: !0,
                            mode: "local",
                            fieldLabel: "user_id",
                            store: jun.rztUsers,
                            hiddenName: "user_id",
                            valueField: "id",
                            forceSelection: !0,
                            displayField: "user_id",
                            anchor: "100%"
                        },
                        /* {
                            xtype: 'textfield',
                            fieldLabel: 'user_id',
                            hideLabel:false,
                            //hidden:true,
                            name:'user_id',
                            id:'user_idid',
                            ref:'../user_id',
                            maxLength: 50,
                            //allowBlank: ,
                            anchor: '100%'
                        },  */
                        {
                            xtype: 'xdatefield',
                            ref:'../start_date',
                            fieldLabel: 'start_date',
                            name:'start_date',
                            id:'start_dateid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%',
                            minValue: '2017-08-15',
                            maxValue: '2017-08-25'                            
                        }, 
                        {
                            xtype: 'xdatefield',
                            ref:'../end_date',
                            fieldLabel: 'end_date',
                            name:'end_date',
                            id:'end_dateid',
                            format: 'd M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        }, 
                        /*  
                        {
                            xtype: 'xdatefield',
                            ref:'../created',
                            fieldLabel: 'created',
                            name:'created',
                            id:'createdid',
                            format: 'd M Y',
                            //allowBlank: ,
                            anchor: '100%'                            
                        }, 
                        {
                            xtype: 'xdatefield',
                            ref:'../updated',
                            fieldLabel: 'updated',
                            name:'updated',
                            id:'updatedid',
                            format: 'd M Y',
                            //allowBlank: ,
                            anchor: '100%'                            
                        }, 
                        {
                            xtype: 'textfield',
                            fieldLabel: 'author',
                            hideLabel:false,
                            //hidden:true,
                            name:'author',
                            id:'authorid',
                            ref:'../author',
                            maxLength: 50,
                            //allowBlank: ,
                            anchor: '100%'
                        }, 
                        {
                            xtype: 'textfield',
                            fieldLabel: 'updater',
                            hideLabel:false,
                            //hidden:true,
                            name:'updater',
                            id:'updaterid',
                            ref:'../updater',
                            maxLength: 50,
                            //allowBlank: ,
                            anchor: '100%'
                        }, 
                        {
                            xtype: 'textfield',
                            fieldLabel: 'up',
                            hideLabel:false,
                            //hidden:true,
                            name:'up',
                            id:'upid',
                            ref:'../up',
                            //allowBlank: ,
                            anchor: '100%'
                        },  
                          */                          
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.rztUsers.reload();
        jun.RestrictDateWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'Restrictdate/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'Restrictdate/create/';
                }
             
            Ext.getCmp('form-RestrictDate').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztRestrictDate.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-RestrictDate').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});