jun.PurchaseCoaDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PurchaseCoaDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PurchaseCoaDetailStoreId',
            url: 'PurchaseCoaDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'purchase_coa_detail_id'},
                {name: 'purchase_coa_id'},
                {name: 'type_'},
                /*
                 * Tab Debit
                 */
                {name: 'vat', type:'float'},
                {name: 'dpp', type:'float'},
                {name: 'dppph21', type:'float'},
                {name: 'dppph23', type:'float'},
                {name: 'dppph25', type:'float'},
                {name: 'dppph42', type:'float'},
                {name: 'dppph26', type:'float'},
                {name: 'dppph22', type:'float'},
                {name: 'beban_ppn', type:'float'},
                {name: 'beban_pph', type:'float'},
                {name: 'other_tax', type:'float'},
                {name: 'biaya_materai_ekspedisi', type:'float'},
                {name: 'forex_loss', type:'float'},
                /*
                 * Tab Kredit
                 */
                {name: 'pph_21', type:'float'},
                {name: 'pph23', type:'float'},
                {name: 'pph42', type:'float'},
                {name: 'pph25_29', type:'float'},
                {name: 'round', type:'float'},
                {name: 'forex_gain', type:'float'},
                {name: 'total', type:'float'},
                {name: 'note'},
                {name: 'store_kode'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        var total = this.sum("total");
        Ext.getCmp('total_purchase_coaid').setValue(total);
    }
});
jun.rztPurchaseCoaDetail = new jun.PurchaseCoaDetailstore();