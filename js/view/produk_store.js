jun.Produkstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Produkstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ProdukStoreId',
            url: 'projection/Produk',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'produk_id'},
                {name:'kodeproduk'},
                {name:'nama'},
                {name:'harga'},
                {name:'hargabeli'},
                {name:'isi'},
                {name:'groupproduk_id'},
                {name:'unit_id'},
                {name:'businessunit_id'},
                {name:'created_at'},
                {name:'uploadfile'},
                {name:'uploaddate'},
                
            ]
        }, cfg));
    }
});
jun.rztProduk = new jun.Produkstore();
jun.rztProdukLib = new jun.Produkstore();
jun.rztProdukLib.load();
