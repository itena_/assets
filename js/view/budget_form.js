jun.BudgetWin = Ext.extend(Ext.Window, {
    title: 'Budget Plans',
    modez:1,
    width: 400,
    height: 236,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-Budget',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    /*                                                                 {
                                    xtype: 'textfield',
                                    fieldLabel: 'Account',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'account_id',
                                    id:'account_idid',
                                    ref:'../account_id',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                },*/
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../account_id',
                        displayField: 'account_code',
                        emptyText: "Account",
                        anchor: '100%'
                    },
                    /*{
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        style: 'margin-bottom:2px',
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'COA',
                        //store: this.mode,
                        store: jun.rztChartMasterCmp,
                        ref: '../../account_code',
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                            "</div></tpl>"),
                        listWidth: 500,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        displayField: 'account_code',
                        width: 150
                    },*/
                                                                     {
                                    xtype: 'numericfield',
                                    fieldLabel: 'Amount',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'amount',
                                    id:'amountid',
                                    ref:'../amount',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                                                     },
                    {
                        xtype: 'xdatefield',
                        ref:'../tdate',
                        fieldLabel: 'Date',
                        name:'tdate',
                        id:'tdateid',
                        format: 'M Y',
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                            xtype: 'textarea',
                            fieldLabel: 'Description',
                            hideLabel:false,
                            //hidden:true,
                            name:'description',
                            id:'descriptionid',
                            ref:'../description',
                            anchor: '100%'
                            //allowBlank: 1
                        }, 

                                /*                                     {
                                    xtype: 'xdatefield',
                                    fieldLabel: 'Created At',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'created_at',
                                    id:'created_atid',
                                    ref:'../created_at',
                                    maxLength: 20,
                                    format: 'M Y',
                                    //allowBlank: 1,
                                    anchor: '100%'
                                },*/
                    /*                                    {
                       xtype: 'textfield',
                       fieldLabel: 'businessunit_id',
                       hideLabel:false,
                       //hidden:true,
                       name:'businessunit_id',
                       id:'businessunit_idid',
                       ref:'../businessunit_id',
                       maxLength: 50,
                       //allowBlank: ,
                       anchor: '100%'
                   },
                                                        {
                       xtype: 'textfield',
                       fieldLabel: 'uploadfile',
                       hideLabel:false,
                       //hidden:true,
                       name:'uploadfile',
                       id:'uploadfileid',
                       ref:'../uploadfile',
                       maxLength: 100,
                       //allowBlank: 1,
                       anchor: '100%'
                   },
                                                        {
                       xtype: 'textfield',
                       fieldLabel: 'uploaddate',
                       hideLabel:false,
                       //hidden:true,
                       name:'uploaddate',
                       id:'uploaddateid',
                       ref:'../uploaddate',
                       maxLength: 6,
                       //allowBlank: 1,
                       anchor: '100%'
                   }, */
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.BudgetWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz= 'projection/Budget/create/';
            /*if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'Budget/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'projection/Budget/create/';
                }*/
             
            Ext.getCmp('form-Budget').getForm().submit({
                url:urlz,
                timeOut: 1000,
                params: {
                    id: this.id,
                    mode: this.modez
                },
                scope: this,
                success: function(f,a){
                    jun.rztBudget.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-Budget').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});