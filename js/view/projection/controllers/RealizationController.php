<?php

class RealizationController extends GxController {

    public function actionCreate()
    {
        //$model = new Businessunit();
        if (!Yii::app()->request->isAjaxRequest)
            return;

        if (isset($_POST) && !empty($_POST))
        {

            $is_new = $_POST['mode'] == 0;
            $gp_id = $_POST['id'];

            //$detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();


            try{
                $model = $is_new ? new Realization : $this->loadModel($gp_id, "Realization");
                $businessunit = Businessunit::model()->findByAttributes(['businessunit_code' => BUSINESSUNIT]);

                if ($is_new) {
                    $ref = new Reference();
                    //$docref = $ref->get_next_reference(PO);
                } else {
                    //AssetDetail::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $as_id));
                    //AssetPeriode::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $as_id));
                    //$docref = $model->doc_ref;
                }

                foreach($_POST as $k=>$v){
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Realization'][$k] = $v;
                }
                $model->attributes = $_POST['Realization'];

                $model->businessunit_id = $businessunit->businessunit_id;
                $model->created_at = new CDbExpression('NOW()');

                $msg = "Data berhasil di simpan.";
                $status = true;

                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Realization')) . CHtml::errorSummary($model));

                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            }
            catch (Exception $ex)
            {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }

            echo CJSON::encode(array(
                'success'=>$status,
                'msg'=>$msg));
            Yii::app()->end();
        }
    }

public function actionUpdate($id) {
$model = $this->loadModel($id, 'Realization');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['Realization'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['Realization'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->realization_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->realization_id));
}
}
}

public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'Realization')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}


public function actionIndex() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}

$criteria = new CDbCriteria();

    if (isset($_POST['tdate'])) {
        $criteria->addCondition('tdate = date(:tdate)');
        $param[':tdate'] = $_POST['tdate'];
    }
    if (isset($_POST['description'])) {
        $criteria->addCondition('description like :description');
        $param[':description'] = '%' . $_POST['description'] . '%';
    }
    if (isset($_POST['account_code'])) {
        $criteria->addCondition('account_code like :account_code');
        $param[':account_code'] = '%' . $_POST['account_code'] . '%';
    }
    if (isset($_POST['amount'])) {
        $criteria->addCondition('amount like :amount');
        $param[':amount'] = '%' . $_POST['amount'] . '%';
    }

if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
(isset($_POST['limit']) && isset($_POST['start']))) {
$criteria->limit = $limit;
$criteria->offset = $start;
}
    $criteria->addCondition('businessunit_id = :businessunit_id');
    $param[':businessunit_id'] = BUSINESSUNITID;

    $criteria->params = $param;
    $criteria->order = "account_code ASC";

$model = RealizationView::model()->findAll($criteria);
$total = RealizationView::model()->count($criteria);

$this->renderJson($model, $total);

}

}