<?php

class TransaksiController extends GxController {

    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));

        if (isset($_POST) && !empty($_POST))
        {

            $is_new = $_POST['mode'] == 0;
            $id = $_POST['id'];
            $docref="";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();

            try{
                $model = $is_new ? new Transaksi: $this->loadModel($id, "Transaksi");
                $businessunit = Businessunit::model()->findByAttributes(['businessunit_code' => BUSINESSUNIT]);

                $last = $this->getLastRow($businessunit->businessunit_id) + 1;


                if ($is_new) {
                    $docref = BUSINESSUNIT.'/'.date('m').'/'.date('y').'/'.$last;
                } else {
                    //AssetDetail::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $as_id));
                    //AssetPeriode::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $as_id));
                    TransaksiDetail::model()->deleteAll("transaksi_id = :transaksi_id", array(':transaksi_id' => $id));
                    $docref = $model->doc_ref;
                }

                foreach($_POST as $k=>$v){
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Transaksi'][$k] = $v;
                }
                $_POST['Transaksi']['doc_ref'] = $docref;

                $time = strtotime($_POST['Transaksi']['tdate']);
                $tdate = date('Y-m-d',$time);

                $model->attributes = $_POST['Transaksi'];
                $model->businessunit_id = $businessunit->businessunit_id;
                $model->tdate = $tdate;
                $model->created_at = new CDbExpression('NOW()');
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Transaksi')) . CHtml::errorSummary($model));

                foreach ($detils as $detil) {
                    $item_details = new TransaksiDetail;

                    $_POST['TransaksiDetail']['transaksi_id'] = $model->transaksi_id;
                    $_POST['TransaksiDetail']['produk_id'] = $detil['produk_id'];
                    $_POST['TransaksiDetail']['outlet_id'] = $detil['outlet_id'];
                    $_POST['TransaksiDetail']['groupproduk_id'] = $detil['groupproduk_id'];
                    $_POST['TransaksiDetail']['businessunit_id'] = $model->businessunit_id;

                    $_POST['TransaksiDetail']['namaoutlet'] = $detil['namaoutlet'];
                    $_POST['TransaksiDetail']['kodeoutlet'] = $detil['kodeoutlet'];
                    $_POST['TransaksiDetail']['kodeproduk'] = $detil['kodeproduk'];
                    $_POST['TransaksiDetail']['kodegroupproduk'] = $detil['kodegroupproduk'];
                    $_POST['TransaksiDetail']['harga'] = $detil['harga'];
                    $_POST['TransaksiDetail']['hargabeli'] = $detil['hargabeli'];
                    $_POST['TransaksiDetail']['jmloutlet'] = $detil['jmloutlet'];
                    $_POST['TransaksiDetail']['qty'] = $detil['qty'];
                    $_POST['TransaksiDetail']['persentase'] = $detil['persentase'];
                    $_POST['TransaksiDetail']['salesqty'] = $detil['salesqty'];
                    $_POST['TransaksiDetail']['salesrp'] = $detil['salesrp'];
                    $_POST['TransaksiDetail']['salesrpbeli'] = $detil['salesrpbeli'];
                    $_POST['TransaksiDetail']['tdate'] = $model->tdate;
                    $_POST['TransaksiDetail']['created_at'] = new CDbExpression('NOW()');

                    $item_details->attributes = $_POST['TransaksiDetail'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'TransaksiDetail')) . CHtml::errorSummary($item_details));
                }

                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            }
            catch (Exception $ex)
            {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }

            echo CJSON::encode(array(
                'success'=>$status,
                'msg'=>$msg));
            Yii::app()->end();
        }
    }

    public function getLastRow($id)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('businessunit_id',$id);
        $model = Transaksi::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

public function actionUpdate($id) {
$model = $this->loadModel($id, 'Transaksi');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['Transaksi'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['Transaksi'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->transaksi_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->transaksi_id));
}
}
}

public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'Transaksi')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}


    public function actionIndex() {
        if(isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if(isset($_POST['start'])){
            $start = $_POST['start'];

        } else {
            $start = 0;
        }

        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
        (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }

        $criteria->addCondition('businessunit_id = :businessunit_id');
        $param[':businessunit_id'] = BUSINESSUNITID;

        $criteria->params = $param;
        $criteria->order = "doc_ref DESC";

        $model = Transaksi::model()->findAll($criteria);
        $total = Transaksi::model()->count($criteria);

        $this->renderJson($model, $total);

    }

}