<?php
Yii::import('application.modules.projection.models._base.BaseTransaksiView');

class TransaksiView extends BaseTransaksiView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->transaksi_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->transaksi_id = $uuid;
        }
        return parent::beforeValidate();
    }
}