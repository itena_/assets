<?php
Yii::import('application.modules.projection.models._base.BaseGroupproduk');

class Groupproduk extends BaseGroupproduk
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->groupproduk_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->groupproduk_id = $uuid;
        }
        return parent::beforeValidate();
    }
}