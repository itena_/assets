<?php
Yii::import('application.modules.projection.models._base.BaseUnit');

class Unit extends BaseUnit
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->unit_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->unit_id = $uuid;
        }
        return parent::beforeValidate();
    }
}