<?php
Yii::import('application.modules.projection.models._base.BaseRealizationView');

class RealizationView extends BaseRealizationView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->realization_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->realization_id = $uuid;
        }
        return parent::beforeValidate();
    }
}