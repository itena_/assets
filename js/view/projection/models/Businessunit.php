<?php
Yii::import('application.modules.projection.models._base.BaseBusinessunit');

class Businessunit extends BaseBusinessunit
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->businessunit_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->businessunit_id = $uuid;
        }
        return parent::beforeValidate();
    }
}