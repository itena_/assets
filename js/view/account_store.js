jun.Accountstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Accountstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AccountStoreId',
            url: 'projection/Account',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'account_id'},
                {name:'account_code'},
                {name:'account_name'},
                {name:'status'},
                {name:'description'},
                {name:'norek'},
                {name:'businessunit_id'},
                {name:'group_id'},
                {name:'created_at'},
                {name:'uploadfile'},
                {name:'uploaddate'},
                {name:'group_name'},
                {name:'group_code'}
                
            ]
        }, cfg));
    }
});
jun.rztAccount = new jun.Accountstore();
jun.rztAccountLib = new jun.Accountstore();
jun.rztAccountLib.load();
