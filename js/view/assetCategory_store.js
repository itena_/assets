jun.AssetCategorystore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetCategorystore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetCategoryStoreId',
            url: 'asset/AssetCategory',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name:'category_id'},
                {name:'category_code'},
                {name:'category_name'},
                {name:'category_desc'},
                {name:'timestamp'},
                
            ]
        }, cfg));
    }
});
jun.rztAssetCategory = new jun.AssetCategorystore();
jun.rztAssetCategoryLib = new jun.AssetCategorystore();
jun.rztAssetCategoryLib.load();
//jun.rztAssetCategory.load();
