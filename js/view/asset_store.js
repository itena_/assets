jun.Assetstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Assetstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetStoreId',
            url: 'asset/Asset',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'asset_id'},
                {name:'barang_id'},
                {name:'source'},
                {name:'status'},
                {name:'asset_name'},
                {name:'ati'},
                {name:'doc_ref'},
                {name:'branch'},
                {name:'qty'},
                {name:'description'},
                {name:'date_acquisition'},
                {name:'price_acquisition'},
                {name:'new_price_acquisition'},
                {name:'asset_group_id'},
                {name:'created_at'},
                {name:'updated_at'},
                {name:'category'},
                {name:'category_id'},
                {name:'hide'}
                
                
            ]
        }, cfg));
    }
});
jun.rztAsset = new jun.Assetstore();
//jun.rztAsset.load();
