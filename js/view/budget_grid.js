jun.BudgetGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"Budget Plans",
        id:'docs-jun.BudgetGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
        /*                {
			header:'budget_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'budget_id',
			width:100
		},*/
                                {
			header:'Account Code',
			sortable:true,
			resizable:true,                        
            dataIndex:'account_code',
			width:100,
                                    filter: {xtype: "textfield"}
		},
        {
            header:'Account Name',
            sortable:true,
            resizable:true,
            dataIndex:'account_name',
            width:100,
            filter: {xtype: "textfield"}
        },
                                {
			header:'Amount',
			sortable:true,
			resizable:true,                        
            dataIndex:'amount',
			width:100,
                                    filter: {xtype: "textfield"}
		},
                                {
			header:'Description',
			sortable:true,
			resizable:true,                        
            dataIndex:'description',
			width:100,
                                    filter: {xtype: "textfield"}
		},
                                {
			header:'Date',
			sortable:true,
			resizable:true,                        
            dataIndex:'tdatef',
			width:100,
                                    filter: {
                                        xtype: "xdatefield",
                                        format: 'm/Y'
                                    }
		},
        /*                        {
			header:'created_at',
			sortable:true,
			resizable:true,                        
            dataIndex:'created_at',
			width:100
		},*/
                		/*
                {
			header:'businessunit_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'businessunit_id',
			width:100
		},
                                {
			header:'uploadfile',
			sortable:true,
			resizable:true,                        
            dataIndex:'uploadfile',
			width:100
		},
                                {
			header:'uploaddate',
			sortable:true,
			resizable:true,                        
            dataIndex:'uploaddate',
			width:100
		},
                		*/
		
	],
	initComponent: function(){
	this.store = jun.rztBudget;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Edit',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Delete',
                        ref: '../btnDelete'
                    },
                    {
                        xtype:'tbseparator',
                        ref: '../separator',
                    },
                    {
                        xtype: 'button',
                        text: 'Export',
                        ref: '../btnExport',
                        iconCls: 'silk13-page_white_excel'
                    }
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		jun.BudgetGrid.superclass.initComponent.call(this);
	        this.btnAdd.on('Click', this.loadForm, this);
                this.btnEdit.on('Click', this.loadEditForm, this);
                this.btnDelete.on('Click', this.deleteRec, this);
                this.btnExport.on('Click', this.export, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},

        export: function(){
            var form = new jun.ReportBudget({modez:0});
            form.show();
        },

        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
        },
        
        loadForm: function(){
            var form = new jun.BudgetWin({modez:0});
            form.show();
        },
        
        loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();
            
            //var dodol = this.store.getAt(0);
             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.budget_id;
            var form = new jun.BudgetWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'projection/Budget/delete/id/' + record.json.budget_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztBudget.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });
        
        }
})
