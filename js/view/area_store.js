jun.Areastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Areastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AreaStoreId',
            url: 'Area',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'area_id'},
                {name: 'area_name'},
                {name: 'store_list'}
            ]
        }, cfg));
    }
});
jun.rztArea = new jun.Areastore();
jun.rztAreaCmp = new jun.Areastore();

jun.StoreAreastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.StoreAreastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'StoreAreaStoreId',
            url: 'Area/Store',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'store_area_id'},
                {name: 'store'},
                {name: 'area_id'}
            ]
        }, cfg));
    }
});
jun.rztStoreArea = new jun.StoreAreastore();
