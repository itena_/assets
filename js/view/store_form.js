jun.StoreWin = Ext.extend(Ext.Window, {
    title: 'Branch',
    modez: 1,
    width: 400,
    height: 281,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Store',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Code',
                        hideLabel: false,
                        //hidden:true,
                        name: 'store_kode',
                        id: 'store_kodeid',
                        ref: '../store_kode',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_store',
                        id: 'nama_storeid',
                        ref: '../nama_store',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Address',
                        hideLabel: false,
                        //hidden:true,
                        name: 'alamat',
                        id: 'alamatstoreid',
                        ref: '../alamatstore',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Telp',
                        hideLabel: false,
                        //hidden:true,
                        name: 'telp',
                        id: 'telpstoreid',
                        ref: '../telpstore',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Email',
                        hideLabel: false,
                        //hidden:true,
                        name: 'email',
                        id: 'emailstoreid',
                        ref: '../emailstore',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Region',
                        store: jun.rztWilayahCmp,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kode_wilayah} - {nama_wilayah}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'wilayah',
                        valueField: 'wilayah_id',
                        //emptyText: "Region",
                        displayField: 'nama_wilayah',
                        anchor: '100%',
                        allowBlank: false
                    },
/*                    {
                        xtype: 'textfield',
                        fieldLabel: 'Beban FinAcc',
                        hideLabel: false,
                        //hidden:true,
                        name: 'beban_acc',
                        id: 'beban_accid',
                        ref: '../beban_acc',
                        maxLength: 4,
                        //allowBlank: ,
                        anchor: '100%'
                    },*/
/*                    {
                        xtype: 'textarea',
                        fieldLabel: 'Connection',
                        hideLabel: false,
                        //hidden:true,
                        name: 'conn',
                        id: 'connid',
                        ref: '../conn',
                        height: 52,
                        anchor: '100%'
                        //allowBlank: 1
                    }*/
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.StoreWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Store/update/id/' + this.id;
        } else {
            urlz = 'Store/create/';
        }
        Ext.getCmp('form-Store').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztStore.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Store').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});