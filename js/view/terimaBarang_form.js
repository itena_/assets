jun.TerimaBarangWin = Ext.extend(Ext.Window, {
    title: 'Penerimaan Barang',
    modez: 1,
    width: 900,
    height: 475,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    resizable: !1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 5px',
                id: 'form-TerimaBarang',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        maxLength: 50,
                        readOnly: true,
                        width: 175,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tanggal:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        name: 'tgl',
                        format: 'd M Y',
                        readOnly: !HEADOFFICE,
                        allowBlank: false,
                        value: DATE_NOW,
                        width: 175,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "No. PO:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        ref: '../po',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztPoInReceiveCmp,
                        hiddenName: 'po_id',
                        valueField: 'po_id',
                        displayField: 'doc_ref',
                        hideTrigger: true,
                        minChars: 1,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "tr.search-item",
                        title: '<div class="x-grid3-header">' +
                        '<table cellspacing="0" class="mfcombobox">' +
                        '<thead><tr class="x-grid3-hd-row"><th width="139px">Nomor PO</th>' +
                        '<th width="80px">Tanggal</th><th>Supplier</th></tr></thead></table></div>',
                        tpl: new Ext.XTemplate('<table cellspacing="0" class="mfcombobox x-grid3-row-table">',
                            // '<thead><tr style="background:#eeeeee;">',
                            // '<th width="150">Nomor PO</th><th width="80">Tanggal</th><th>Supplier</th></tr></thead>',
                            '<tbody><tpl for="."><tr class="search-item">',
                            '<td width="150px" class="x-grid3-col x-grid3-cell x-grid3-td-0 x-grid3-cell-first ">{doc_ref}</td>',
                            '<td width="80px" class="class="x-grid3-col x-grid3-cell x-grid3-td-0">{tgl:date("j-M-Y")}</td>',
                            '<td class="class="x-grid3-col x-grid3-cell x-grid3-td-0" >{supp_company}</td>',
                            '</tr></tpl></tbody></table>'),
                        allowBlank: false,
                        listWidth: 500,
                        lastQuery: "",
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Branch:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        //readOnly: !HEADOFFICE,
                        readOnly: true,
                        width: 175,
                        x: 375,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "No. SJ:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'No. SJ',
                        allowBlank: false,
                        name: 'no_sj',
                        maxLength: 50,
                        width: 175,
                        x: 375,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Tgl. SJ:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'xdatefield',
                        fieldLabel: 'Tgl SJ',
                        allowBlank: false,
                        name: 'tgl_sj',
                        format: 'd M Y',
                        width: 175,
                        x: 375,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 595,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        name: 'note',
                        width: 275,
                        height: 60,
                        x: 595,
                        y: 22
                    },
                    {
                        xtype: 'hidden',
                        name: 'disc',
                        ref: '../disc'
                    },
                    {
                        xtype: 'hidden',
                        name: 'termofpayment',
                        ref: '../termofpayment'
                    },
                    new jun.TerimaBarangDetailsGrid({
                        x: 5,
                        y: 95,
                        anchor: "100% 100%",
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                        readOnly: (!(this.modez < 2)?true:false)
                    })
                ]
            }
        ];
        
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TerimaBarangWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.po.on('select', this.onPoSelect, this);
        this.po.on('beforequery', this.onPoBeforequery, this);
        this.on("close", this.onWinClose, this);
        
        this.btnCancel.setText(this.modez < 2? 'Cancel':'Close');
        switch(this.modez) {
        case 0:
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
            break;
        case 1 :
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(this.rejectPR?false:true);
            break;
        default :
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.formz.getForm().applyToFields({ readOnly: true });
            break;
        }
    },
    onPoSelect: function (c, r, i) {
        this.disc.setValue(r.get('disc'));
        this.termofpayment.setValue(r.get('termofpayment'));
        
        this.griddetils.store.baseParams = {};
        this.griddetils.store.baseParams = {
            po_id: r.get('po_id')
        };
        this.griddetils.store.reload();
    },
    onPoBeforequery: function (c, q, fa) {
        jun.rztPoInReceiveCmp.setBaseParam('terimaBarang', 1);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'TerimaBarang/create/';
        Ext.getCmp('form-TerimaBarang').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    this.griddetils.store.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztCreateTerimaBarang.reload();
                
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TerimaBarang').getForm().reset();
                    this.griddetils.store.removeAll();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onWinClose: function () {
        this.griddetils.store.removeAll();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});