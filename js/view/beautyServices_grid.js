jun.BeautyServicesGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "BeautyServices",
    id: 'docs-jun.BeautyServicesGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'nscc_beauty_services_id',
            sortable: true,
            resizable: true,
            dataIndex: 'nscc_beauty_services_id',
            width: 100
        },
        {
            header: 'total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100
        },
        {
            header: 'beauty_id',
            sortable: true,
            resizable: true,
            dataIndex: 'beauty_id',
            width: 100
        },
        {
            header: 'salestrans_details',
            sortable: true,
            resizable: true,
            dataIndex: 'salestrans_details',
            width: 100
        },
    ],
    initComponent: function () {
        this.store = jun.rztBeautyServices;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.BeautyServicesGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.BeautyServicesWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a service");
            return;
        }
        var idz = selectedz.json.nscc_beauty_services_id;
        var form = new jun.BeautyServicesWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a service");
            return;
        }
        Ext.Ajax.request({
            url: 'BeautyServices/delete/id/' + record.json.nscc_beauty_services_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBeautyServices.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.beautytransGrid = Ext.extend(Ext.grid.EditorGridPanel, {
    title: "Service",
    id: 'docs-jun.beautytransGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    // viewConfig: {
    //     forceFit: true
    // },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    // plugins: [new Ext.ux.grid.RowEditor({saveText: 'Update'})],
    clicksToEdit: 1,
    columns: [
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'No. Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'no_customer',
            width: 200
        },
        {
            header: 'Customers Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 200
        },
        {
            header: 'Items Code',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_barang',
            width: 200
        },
        {
            header: 'Items Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_barang',
            width: 200
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 25,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Perawat 1',
            sortable: true,
            resizable: true,
            dataIndex: 'beauty_id',
            renderer: jun.renderBeauty,
            editor: {
                xtype: 'combo',
                //typeAhead: true,
                fieldLabel: 'Beauty 1',
                ref: '../beauty1',
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: jun.rztBeauty1Cmp,
                id: '1beauty_id',
                hiddenName: 'beauty1_id',
                valueField: 'beauty_id',
                displayField: 'nama_beauty',
                allowBlank: true,
                anchor: '100%'
            }
        },
        {
            header: 'Perawat 2',
            sortable: true,
            resizable: true,
            dataIndex: 'beauty2_id',
            renderer: jun.renderBeauty,
            editor: {
                xtype: 'combo',
                //typeAhead: true,
                fieldLabel: 'Beauty 2',
                ref: '../beauty2',
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: jun.rztBeauty2Cmp,
                id: '2beauty_id',
                hiddenName: 'beauty2_id',
                valueField: 'beauty_id',
                displayField: 'nama_beauty',
                allowBlank: true,
                anchor: '100%'
            }
        },
        {
            header: 'Beauty 3',
            sortable: true,
            resizable: true,
            dataIndex: 'beauty3_id',
            renderer: jun.renderBeauty,
            editor: {
                xtype: 'combo',
                //typeAhead: true,
                fieldLabel: 'Beauty 3',
                ref: '../beauty3',
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: jun.rztBeauty3Cmp,
                id: '3beauty_id',
                hiddenName: 'beauty3_id',
                valueField: 'beauty_id',
                displayField: 'nama_beauty',
                allowBlank: true,
                anchor: '100%'
            }
        },
        {
            header: 'Beauty 4',
            sortable: true,
            resizable: true,
            dataIndex: 'beauty4_id',
            renderer: jun.renderBeauty,
            editor: {
                xtype: 'combo',
                //typeAhead: true,
                fieldLabel: 'Beauty 4',
                ref: '../beauty4',
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: jun.rztBeauty4Cmp,
                id: '4beauty_id',
                hiddenName: 'beauty4_id',
                valueField: 'beauty_id',
                displayField: 'nama_beauty',
                allowBlank: true,
                anchor: '100%'
            }
        },
        {
            header: 'Beauty 5',
            sortable: true,
            resizable: true,
            dataIndex: 'beauty5_id',
            renderer: jun.renderBeauty,
            editor: {
                xtype: 'combo',
                //typeAhead: true,
                fieldLabel: 'Beauty 5',
                ref: '../beauty5',
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                store: jun.rztBeauty5Cmp,
                id: '5beauty_id',
                hiddenName: 'beauty5_id',
                valueField: 'beauty_id',
                displayField: 'nama_beauty',
                allowBlank: true,
                anchor: '100%'
            }
        },
        {
            header: 'Dokter',
            sortable: true,
            resizable: true,
            dataIndex: 'dokter_id',
            renderer: jun.renderDokter,
            editor: {
                xtype: 'combo',
                //typeAhead: true,
                fieldLabel: 'Doctor',
                ref: '../dokter',
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                id: 'dokteredit_id',
                store: jun.rztDokterStatus,
                hiddenName: 'dokter_id',
                valueField: 'dokter_id',
                displayField: 'nama_dokter',
                allowBlank: true,
                anchor: '100%'
            }
        },
        {
            header: 'Active',
            sortable: true,
            resizable: true,
            dataIndex: 'active',
            width: 100,
            editor: {
                xtype: 'numericfield',
                readOnly: true,
                id: 'dokteractive'
            }
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
    ],
    initComponent: function () {
        jun.rztBeutytrans.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglbeutygridid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    //b.params.tgl = Ext.getCmp('tglbeutygridid').getValue();
                    b.params.mode = "grid";
                }
            }
        });
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
        if (jun.rztBeautyLib.getTotalCount() === 0) {
            jun.rztBeautyLib.load();
        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
        if (jun.rztBeauty1Cmp.getTotalCount() === 0) {
            jun.rztBeauty1Cmp.load();
        }
        if (jun.rztBeauty2Cmp.getTotalCount() === 0) {
            jun.rztBeauty2Cmp.load();
        }
        if (jun.rztBeauty3Cmp.getTotalCount() === 0) {
            jun.rztBeauty3Cmp.load();
        }
        if (jun.rztBeauty4Cmp.getTotalCount() === 0) {
            jun.rztBeauty4Cmp.load();
        }
        if (jun.rztBeauty5Cmp.getTotalCount() === 0) {
            jun.rztBeauty5Cmp.load();
        }
        this.store = jun.rztBeutytrans;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Edit Service',
                    ref: '../btnAdd'
                            // hidden: true
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Post / Final',
                    ref: '../btnFinal'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglbeutygridid'
                },
                {
                    xtype: 'form',
                    ref: '../formhidden'
                            //RUMUS form hidden buat penampungan sementara
                            //jasa beauty dan dokter
                }
            ]
        };
        jun.beautytransGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnFinal.on('Click', this.onbtnFinalclick, this);
        this.tgl.on('select', this.refreshTgl, this);
        // Ext.getCmp("1beauty_id").on('select', this.beautyOnSelect, this);
        // Ext.getCmp("2beauty_id").on('select', this.beautyOnSelect, this);
        // Ext.getCmp("3beauty_id").on('select', this.beautyOnSelect, this);
        // Ext.getCmp("4beauty_id").on('select', this.beautyOnSelect, this);
        // Ext.getCmp("5beauty_id").on('select', this.beautyOnSelect, this);
        // Ext.getCmp("dokteredit_id").on('select', this.beautyOnSelect, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    onbtnFinalclick: function () {
        var tgl = this.tgl.getValue();
        if (tgl == '' || tgl == undefined) {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Date must selected!',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            return;
        }
        Ext.MessageBox.confirm('Confirmation',
                'Are you really sure POST / FINAL data service on ' + tgl.format('F j, Y') + '?', function (btn) {
            if (btn == 'no') {
                return;
            }
            Ext.Ajax.request({
                url: 'SalestransDetails/final/',
                method: 'POST',
                scope: this,
                params: {
                    tgl: tgl
                },
                success: function (f, a) {
                    jun.rztBeutytrans.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }, this);
    },
    refreshTgl: function () {
        this.store.reload();
    },
    beautyOnSelect: function (c, r, i) { //RUMUS fungsi form hidden semetara
        var urlz = 'SalestransDetails/update/';
//        console.log(this.record);
//        console.log(c.getId() == "2beauty_id");
        this.formhidden.getForm().submit({
            scope: this,
            url: urlz,
            params: {
                salestrans_details: this.record.get("salestrans_details"),
                beauty_id: c.getId() == "1beauty_id" ? c.getValue() : this.record.get("beauty_id"),
                beauty2_id: c.getId() == "2beauty_id" ? c.getValue() : this.record.get("beauty2_id"),
                beauty3_id: c.getId() == "3beauty_id" ? c.getValue() : this.record.get("beauty3_id"),
                beauty4_id: c.getId() == "4beauty_id" ? c.getValue() : this.record.get("beauty4_id"),
                beauty5_id: c.getId() == "5beauty_id" ? c.getValue() : this.record.get("beauty5_id"),
                dokter_id: c.getId() == "dokteredit_id" ? c.getValue() : this.record.get("dokter_id"),
                jasa_dokter: this.record.get("jasa_dokter")
            },
            success: function (f, a) {
                jun.rztBeutytrans.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        //Ext.getCmp("beauty2wkid").setValue(Ext.getCmp('beauty2_id'));
        var idz = selectedz.json.salestrans_id;
        var form = new jun.beautytransWin({modez: 1, idrecord: idz, final: selectedz.json.final});
        form.formz.getForm().loadRecord(this.record);
        form.show(this);
        form.beauty2.setValue(Ext.getCmp("beauty2_id").getValue());
        form.onbtnSaveclick();
        //this.store.reload();
    }
});
//--------------------
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

jun.allbeautytransGrid = Ext.extend(Ext.grid.EditorGridPanel, {
    title: "Jasa Beauty-KMR-KMT",
    id: 'docs-jun.allbeautytransGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    clicksToEdit: 1,
    frame: true,
    listeners: {
        beforeedit: function(grid){
            if (grid.field === 'kmt') {
                if (grid.record.data.kode_barang === '-') {
                    return false;
                }
            }
            if (grid.field === 'kmr') {
                if (grid.record.data.kode_barang !== '-') {
                    return false;
                }
            }
            if (grid.field === 'beauty_id') {
                if (grid.record.data.kode_barang === '-') {
                    return false;
                }
            }
            //console.log(grid.record.data);
            //console.log(grid.field);
        },
        afteredit: function(grid){
            if (grid.field === 'kmt' || grid.field === 'beauty_id')
                var add_param = {
                    salestrans_details : grid.record.data.salestrans_details,
                    mode : 'KMT',
                    kmt : grid.record.data.kmt,
                    beauty : grid.record.data.beauty_id
                };
            else
                var add_param = {
                    salestrans_details : grid.record.data.salestrans_details,
                    mode : 'KMR',
                    kmr : grid.record.data.kmr,
                    salestrans_id : grid.record.data.salestrans_id
                };


            Ext.Ajax.request({
                url: 'SalestransDetails/kmrkmt/',
                method: 'POST',
                scope: this,
                params: add_param,
                success: function (f, a) {
                    //jun.rztBeutytrans.reload();
                    var response = Ext.decode(f.responseText);
                    if (!response.success)
                        Ext.MessageBox.show({
                            title: 'Info',
                            msg: response.msg,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        });
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
            console.log('OK');
        }
    },
    columns: [
            {
                header: 'No. Receipt',
                // sortable: true,
                resizable: true,
                dataIndex: 'doc_ref',
                width: 100
            },
            {
                header: 'No. Customers',
                // sortable: true,
                resizable: true,
                dataIndex: 'no_customer',
                width: 200
            },
            {
                header: 'Customers Name',
                // sortable: true,
                resizable: true,
                dataIndex: 'nama_customer',
                width: 200
            },
            {
                header: 'Items Code',
                // sortable: true,
                resizable: true,
                dataIndex: 'kode_barang',
                width: 200
            },
            {
                header: 'Items Name',
                // sortable: true,
                resizable: true,
                dataIndex: 'nama_barang',
                width: 200
            },
            {
                header: 'Qty',
                // sortable: true,
                resizable: true,
                dataIndex: 'qty',
                width: 25,
                align: "right",
                renderer: Ext.util.Format.numberRenderer("0,0")
            },
            {
                header: 'Beauty',
                // sortable: true,
                resizable: true,
                dataIndex: 'beauty_id',
                renderer: jun.renderBeautyEditService,
                editor: new Ext.form.ComboBox({
                    //typeAhead: true,
                    fieldLabel: 'Beauty',
                    ref: '../beauty1',
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    store: jun.BeautystoreComboCmp,
                    id: '1beauty_id',
                    hiddenName: 'beauty1_id',
                    valueField: 'beauty_id',
                    displayField: 'nama_beauty',
                    allowBlank: true,
                    anchor: '100%'
                })
            },
            {
                header: 'Dokter KMR',
                // sortable: true,
                resizable: true,
                dataIndex: 'kmr',
                renderer: jun.renderDokterEditService,
                editor : new Ext.form.ComboBox({
                    // xtype: 'combo',
                    ref: '../dokterKMR',
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    store: jun.DokterstoreComboKMRCmp,
                    hiddenName: 'dokter_kmr',
                    valueField: 'dokter_id',
                    displayField: 'nama_dokter',
                    allowBlank: true,
                    anchor: '100%'
                })
            },
            {
                header: 'Dokter KMT',
                // sortable: true,
                resizable: true,
                dataIndex: 'kmt',
                renderer: jun.renderDokterEditService,
                editor : new Ext.form.ComboBox({
                    // xtype: 'combo',
                    ref: '../dokterKMT',
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    store: jun.DokterstoreComboKMTCmp,
                    hiddenName: 'dokter_kmt',
                    valueField: 'dokter_id',
                    displayField: 'nama_dokter',
                    allowBlank: true,
                    anchor: '100%'
                })
            },
            {
                header: 'Active',
                // sortable: true,
                resizable: true,
                dataIndex: 'active',
                width: 100
            },
            {
                header: 'Branch',
                // sortable: true,
                resizable: true,
                dataIndex: 'store',
                width: 100
            }
        ],
    initComponent: function () {
        jun.rztBeutyCombinationtrans.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglbeutygridid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    //b.params.tgl = Ext.getCmp('tglbeutygridid').getValue();
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztBeutyCombinationtrans;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'tbseparator',
                },
                {
                    xtype: 'button',
                    text: 'Post / Final',
                    ref: '../btnFinal'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglbeutygridid'
                },
                {
                    xtype: 'form',
                    ref: '../formhidden'
                    //RUMUS form hidden buat penampungan sementara
                    //jasa beauty dan dokter
                }
            ]
        };
        jun.allbeautytransGrid.superclass.initComponent.call(this);
        this.btnFinal.on('Click', this.onbtnFinalclick, this);
        this.tgl.on('select', this.refreshTgl, this);
        // Ext.getCmp("1beauty_id").on('select', this.beautyOnSelect, this);
        // Ext.getCmp("dokteredit_id").on('select', this.beautyOnSelect, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    onbtnFinalclick: function () {
        var tgl = this.tgl.getValue();
        if (tgl == '' || tgl == undefined) {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Date must selected!',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            return;
        }
        Ext.MessageBox.confirm('Confirmation',
            'Are you really sure POST / FINAL data service on ' + tgl.format('F j, Y') + '?', function (btn) {
                if (btn == 'no') {
                    return;
                }
                Ext.Ajax.request({
                    url: 'SalestransDetails/final/',
                    method: 'POST',
                    scope: this,
                    params: {
                        tgl: tgl
                    },
                    success: function (f, a) {
                        jun.rztBeutytrans.reload();
                        var response = Ext.decode(f.responseText);
                        Ext.MessageBox.show({
                            title: 'Info',
                            msg: response.msg,
                            buttons: Ext.MessageBox.OK,
                            icon: Ext.MessageBox.INFO
                        });
                    },
                    failure: function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            }, this);
    },
    refreshTgl: function () {
        this.store.reload();
    },
    beautyOnSelect: function (c, r, i) { //RUMUS fungsi form hidden semetara
        var urlz = 'SalestransDetails/update/';
//        console.log(this.record);
//        console.log(c.getId() == "2beauty_id");
        this.formhidden.getForm().submit({
            scope: this,
            url: urlz,
            params: {
                salestrans_details: this.record.get("salestrans_details"),
                beauty_id: c.getId() == "1beauty_id" ? c.getValue() : this.record.get("beauty_id"),
                dokter_id: c.getId() == "dokteredit_id" ? c.getValue() : this.record.get("dokter_id"),
                jasa_dokter: this.record.get("jasa_dokter")
            },
            success: function (f, a) {
                jun.rztBeutytrans.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    }
});

jun.BeautyServicesOpenGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Perawatan Open",
    id: 'docs-jun.BeautyServicesOpenGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    selectedRecord: null,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_antrian',
            width: 80
        },
        {
            header: 'No. Pasien',
            sortable: false,
            resizable: true,
            dataIndex: 'nomor_pasien',
            width: 100
        },
        // {
        //     header: 'Name',
        //     sortable: false,
        //     resizable: true,
        //     dataIndex: 'nama_customer',
        //     width: 150
        // },
        {
            header: 'No. Receipt',
            sortable: false,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 150
        },
        {
            header: 'Note',
            sortable: false,
            resizable: true,
            dataIndex: 'note',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
        if (jun.rztBeautyLib.getTotalCount() === 0) {
            jun.rztBeautyLib.load();
        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
        // if (jun.rztBeauty1Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty1Cmp.load();
        // }
        // if (jun.rztBeauty2Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty2Cmp.load();
        // }
        // if (jun.rztBeauty3Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty3Cmp.load();
        // }
        // if (jun.rztBeauty4Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty4Cmp.load();
        // }
        // if (jun.rztBeauty5Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty5Cmp.load();
        // }
        this.store = jun.rztAntrianBeautyTrans;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    scale: 'small',
                    style: 'margin:5px',
                    text: 'A N T R I A N'
                },
                '->',
                {
                    xtype: 'button',
                    text: 'P E N D I N G >>>',
                    ref: '../btnPending'
                            // hidden: true
                }
            ]
        };
        this.bbar = {
            items: [
                // {
                //     xtype: 'button',
                //     text: 'Mulai Perawatan',
                //     ref: '../btnAdd'
                //     // hidden: true
                // },
                // {
                //     xtype: 'tbseparator'
                // },
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 100
                }]
        };
        jun.BeautyServicesOpenGrid.superclass.initComponent.call(this);
        // this.btnAdd.on('Click', this.loadForm, this);
        this.store.on('load', this.onStoreLoad, this);
        this.btnPending.on('Click', this.onPendingClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    onStoreLoad: function (a, b) {
        if (this.selectedRecords != null) {
            this.sm.selectRow(a.indexOf(this.selectedRecords), true);
            this.selectedRecords = null;
        }
    },
    onPendingClick: function (t, e) {
        t.setDisabled(true);
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih antrian perawatan.");
            t.setDisabled(false);
            return;
        }
        if (selectedz.data.salestrans_id == null) {
            Ext.MessageBox.alert("Warning", "Antrian belum transaksi.");
            t.setDisabled(false);
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                mode: 'pending',
                bagian: 'perawatan',
                id_antrian: selectedz.data.id_antrian
            },
            success: function (f, a) {
                jun.reloadAllStorePerawatan();
                t.setDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                t.setDisabled(false);
            }
        });
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        this.selectedRecords = this.sm.getSelected();
    },
    loadForm: function (t, e) {
        t.setDisabled(true);
        Ext.Ajax.request({
            url: 'BeautyServices/PanggilAntrian/',
            method: 'POST',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (parseInt(response.total) > 0) {
                    var rec = new Ext.data.Record(response.results);
                    var idz = rec.data.salestrans_id;
                    var dt = new Date;
                    rec.data.start_at = dt;
                    rec.data.end_estimate = dt.add(Date.SECOND, parseInt(rec.data.total_durasi));
                    var form = Ext.getCmp('docs-jun.BeautyPerawatanWin');
                    if (form == undefined || form == null) {
                        form = new jun.BeautyPerawatanWin();
                    }
                    form.modez = 0;
                    form.formz.getForm().loadRecord(rec);
                    form.show(this);
                    jun.rztBeautyPerawatan.load({
                        params: {
                            salestrans_id: idz
                        },
                        scope: this
                    });
                } else {
                    Ext.Msg.alert('Info', 'Tidak ada antrian perawatan.');
                }
                t.setDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                t.setDisabled(false);
            }
        });
    }
});
jun.BeautyServicesPendingGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Perawatan Open",
    id: 'docs-jun.BeautyServicesPendingGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    selectedRecord: null,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'nomor_antrian',
            width: 80
        },
        {
            header: 'No. Pasien',
            sortable: false,
            resizable: true,
            dataIndex: 'no_customer',
            width: 100
        },
        // {
        //     header: 'Customers Name',
        //     sortable: false,
        //     resizable: true,
        //     dataIndex: 'nama_customer',
        //     width: 200
        // },
        {
            header: 'No. Receipt',
            sortable: false,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 150
        },
        {
            header: 'Note',
            sortable: false,
            resizable: true,
            dataIndex: 'note',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
        if (jun.rztBeautyLib.getTotalCount() === 0) {
            jun.rztBeautyLib.load();
        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
        // if (jun.rztBeauty1Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty1Cmp.load();
        // }
        // if (jun.rztBeauty2Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty2Cmp.load();
        // }
        // if (jun.rztBeauty3Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty3Cmp.load();
        // }
        // if (jun.rztBeauty4Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty4Cmp.load();
        // }
        // if (jun.rztBeauty5Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty5Cmp.load();
        // }
        this.store = jun.rztAntrianPendingBeautyTrans;
        // this.bbar = {
        //     items: [
        //         {
        //             xtype: 'button',
        //             text: '<<< A N T R I A N',
        //             ref: '../btnAdd'
        //             // hidden: true
        //         },
        //         '->',
        //         {
        //             xtype: 'paging',
        //             store: this.store,
        //             displayInfo: true,
        //             pageSize: 20
        //         }]
        // };
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    scale: 'small',
                    text: '<<< A N T R I A N',
                    ref: '../btnAdd'
                },
                '->',
                {
                    xtype: 'label',
                    scale: 'small',
                    style: 'margin:5px',
                    text: 'P E N D I N G A N'
                }
            ]
        };
        jun.BeautyServicesPendingGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.store.on('load', this.onStoreLoad, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    onStoreLoad: function (a, b) {
        if (this.selectedRecords != null) {
            this.sm.selectRow(a.indexOf(this.selectedRecords), true);
            this.selectedRecords = null;
        }
    },
    loadForm: function (t, e) {
        t.setDisabled(true);
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih antrian pending perawatan.");
            t.setDisabled(false);
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                mode: 'unpending',
                bagian: 'perawatan',
                id_antrian: selectedz.data.id_antrian
            },
            success: function (f, a) {
                jun.reloadAllStorePerawatan();
                t.setDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                t.setDisabled(false);
            }
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        this.selectedRecords = this.sm.getSelected();
    }
});
jun.BeautyServicesProsesGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Proses Perawatan",
    id: 'docs-jun.BeautyServicesProsesGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    selectedRecord: null,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'No. Receipt',
            sortable: false,
            resizable: true,
            dataIndex: 'doc_ref'
        },
        {
            header: 'No. Pasien',
            sortable: false,
            resizable: true,
            dataIndex: 'no_customer'
        },
        {
            header: 'Nama Pasien',
            sortable: false,
            resizable: true,
            dataIndex: 'nama_customer'
        },
        {
            header: 'Mulai',
            sortable: false,
            resizable: true,
            dataIndex: 'start_at'
        },
        {
            header: 'Extend',
            sortable: false,
            resizable: true,
            dataIndex: 'add_durasi'
        },
        {
            header: 'Est. Selesai',
            sortable: false,
            resizable: true,
            dataIndex: 'end_estimate'
        },
        {
            header: 'Remaining',
            sortable: false,
            resizable: true,
            dataIndex: 'up'
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
        if (jun.rztBeautyLib.getTotalCount() === 0) {
            jun.rztBeautyLib.load();
        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBarangNoJasaMedis.getTotalCount() === 0) {
            jun.rztBarangNoJasaMedis.load();
        }
        if (jun.rztBeauty1Cmp.getTotalCount() === 0) {
            jun.rztBeauty1Cmp.load();
        }
        // if (jun.rztBeauty2Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty2Cmp.load();
        // }
        // if (jun.rztBeauty3Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty3Cmp.load();
        // }
        // if (jun.rztBeauty4Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty4Cmp.load();
        // }
        // if (jun.rztBeauty5Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty5Cmp.load();
        // }
        this.store = jun.rztProsesBeautyTrans;
        this.bbar = {
            items: [
                {
                    xtype: 'button',
                    text: 'Stop Perawatan',
                    ref: '../btnAdd',
                    margins: 5
                            // hidden: true
                },
                {
                    xtype: 'button',
                    text: 'Print Beauty',
                    ref: '../btnPrint',
                    margins: 5
                            // hidden: true
                },
                {
                    xtype: 'button',
                    text: 'Print Rekap',
                    ref: '../btnPrintRekap',
                    margins: 5
                            // hidden: true
                },
                {
                    xtype: 'button',
                    text: 'Add Perawatan Beauty',
                    ref: '../btnRawatBeauty',
                    margins: 5
                            // hidden: true
                },
                {
                    xtype: 'button',
                    text: 'Add Perawatan Dokter',
                    ref: '../btnRawatDokter',
                    margins: 5
                            // hidden: true
                },
                '->',
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20,
                    margins: 5
                }
            ]
        };
        jun.BeautyServicesProsesGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnPrint.on('Click', this.printBeauty2, this);
        this.btnPrintRekap.on('Click', this.printRekap, this);
        this.btnRawatBeauty.on('Click', this.btnRawatBeautyClick, this);
        this.btnRawatDokter.on('Click', this.btnRawatDokterClick, this);
        this.store.on('load', this.onStoreLoad, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    onStoreLoad: function (a, b) {
        if (this.selectedRecords != null) {
            this.sm.selectRow(a.indexOf(this.selectedRecords), true);
            this.selectedRecords = null;
        }
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        this.selectedRecords = this.sm.getSelected();
    },
    btnRawatBeautyClick: function () {
        jun.rztBeautyOffduty1Cmp.reload();
        this.btnRawatBeauty.setDisabled(true);
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            this.btnRawatBeauty.setDisabled(false);
            return;
        }
        var idz = selectedz.json.salestrans_id;
        Ext.Ajax.request({
            url: 'Konsul/GetKonsul/',
            method: 'POST',
            scope: this,
            params: {
                type_: 1,
                ref_id: idz
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                var form = new jun.KonsulTambahanWin({
                    typez_: 1
                });
                form.ref_id.setValue(idz);
                form.customer_id.setValue(selectedz.json.customer_id);
                if (parseInt(response.total) > 0) {
                    // jun.rztKonsulDetilTambahan.proxy.url = 'Konsul/GetKonsul';
                    jun.rztKonsulDetilTambahan.load({
                        params: {
                            konsul_id: response.results[0].konsul_id
                        }
                    });
                    form.beauty.setValue(response.results[0].beauty_id);
                }
                form.show(this);
                this.btnRawatBeauty.setDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnRawatBeauty.setDisabled(false);
            }
        });
    },
    btnRawatDokterClick: function () {
        jun.rztBeautyOffduty1Cmp.reload();
        this.btnRawatDokter.setDisabled(true);
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            this.btnRawatDokter.setDisabled(false);
            return;
        }
        var idz = selectedz.json.salestrans_id;
        Ext.Ajax.request({
            url: 'Konsul/GetKonsul/',
            method: 'POST',
            scope: this,
            params: {
                type_: 2,
                ref_id: idz
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                var form = new jun.KonsulTambahanWin({
                    typez_: 2
                });
                form.ref_id.setValue(idz);
                form.customer_id.setValue(selectedz.json.customer_id);
                if (parseInt(response.total) > 0) {
                    // jun.rztKonsulDetilTambahan.proxy.url = 'Konsul/GetKonsul';
                    jun.rztKonsulDetilTambahan.load({
                        params: {
                            konsul_id: response.results[0].konsul_id
                        }
                    });
                    form.beauty.setValue(response.results[0].beauty_id);
                }
                form.show(this);
                this.btnRawatBeauty.setDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnRawatBeauty.setDisabled(false);
            }
        });
        this.btnRawatDokter.setDisabled(false);
    },
    loadForm: function () {
        this.btnPrint.setDisabled(true);
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        //Ext.getCmp("beauty2wkid").setValue(Ext.getCmp('beauty2_id'));
        var idz = selectedz.json.salestrans_id;
        // var dt = new Date;
        // this.record.data.start_at = dt;
        // this.record.data.end_estimate = dt.add(Date.SECOND, parseInt(this.record.data.total_durasi));
        this.record.data.end_at = new Date;
        var form = Ext.getCmp('docs-jun.BeautyPerawatanWin');
        if (form == undefined || form == null) {
            form = new jun.BeautyPerawatanWin({modez : 1});
        }
        form.modez = 1;

        form.btnStop.setVisible(true);
        form.btnStop.setDisabled(false);
        form.btnPanggil.setVisible(false);
        form.btnUlang.setVisible(false);
        form.btnStart.setVisible(false);
        form.formz.getForm().loadRecord(this.record);
        form.show(this);
        jun.rztBeautyPerawatan.load({
            params: {
                salestrans_id: idz
            },
            scope: this
        });
        this.btnPrint.setDisabled(false);
    },
    printBeauty2: function () {
        jun.indexPrint = 0;
        // console.log('ayam');
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        // jun.rztSalestransDetails.removeAll();
        jun.rztSalestransDetails.load({
            callback: function () {
                jun.indexMax = jun.rztSalestransDetails.getTotalCount();
                this.printBeauty();
            },
            scope: this,
            params: {
                salestrans_id: record.json.salestrans_id
            }
        });
    },
    cetakBeauty: function (response, callback) {
        if (response.success !== false) {
            var msg = [{type: 'raw', data: response.msg}];
            var printData = __printDataBeauty.concat(msg, __feedPaper, __cutPaper);
            print(PRINTER_BEAUTY, printData);
        }
        callback();
    },
    printBeauty: function () {
        var b = jun.rztSalestransDetails.getAt(jun.indexPrint);
        if (!b) {
            return;
        }
        if (jun.indexMax <= jun.indexPrint) {
            return;
        }
        console.log(jun.indexPrint);
        console.log(b);
        var salestrans_details = b.get('salestrans_details');
        console.log(salestrans_details);
        Ext.Ajax.request({
            url: 'Salestrans/PrintBeauty',
            method: 'POST',
            scope: this,
            params: {
                id: salestrans_details,
                index: jun.indexPrint
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    return;
                }
                jun.indexPrint++;
                Ext.getCmp('docs-jun.BeautyServicesProsesGrid').cetakBeauty(response, Ext.getCmp('docs-jun.BeautyServicesProsesGrid').printBeauty);
                // print(PRINTER_STOCKER, printData);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    printRekap: function () {
        this.btnPrintRekap.setDisabled(true);
        Ext.Ajax.request({
            url: 'Salestrans/PrintRekapBeauty',
            method: 'POST',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    return;
                }
                if (response.success !== false) {
                    var msg = [{type: 'raw', data: response.msg}];
                    var feedPaper = [{
                            type: 'raw',
                            data: chr(27) + chr(100) + chr(8),
                            options: {language: 'ESCP', dotDensity: 'double'}
                        }];
                    var printData = __printDataBeauty.concat(msg, feedPaper, __cutPaper);
                    print(PRINTER_BEAUTY, printData);
                }
                this.btnPrintRekap.setDisabled(false);
            },
            failure: function (f, a) {
                this.btnPrintRekap.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.BeautyMostReadyGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Beauty Ready",
    id: 'docs-jun.BeautyMostReadyGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    selectedRecord: null,
    viewConfig: {
        forceFit: true
    },
    columns: [
        {
            header: 'Nama Beauty',
            sortable: false,
            resizable: true,
            dataIndex: 'nama_beauty',
            width: 200
        },
        {
            header: 'Keterangan',
            sortable: false,
            resizable: true,
            dataIndex: 'note_'
        },
        {
            header: 'Mulai',
            sortable: false,
            resizable: true,
            dataIndex: 'time_start'
        },
        {
            header: 'Est. Selesai',
            sortable: false,
            resizable: true,
            dataIndex: 'estimate_end'
        },
        {
            header: 'Time Diff',
            sortable: false,
            resizable: true,
            dataIndex: 'diff_'
        },
        {
            header: 'Total',
            sortable: false,
            resizable: true,
            dataIndex: 'total'
        }
    ],
    initComponent: function () {
        this.store = jun.rztBeautyOndutyStatus;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                },
                {
                    xtype: 'button',
                    style: {
                        marginTop: '2px',
                        marginLeft: '5px'
                    },
                    width: 100,
                    text: 'TOGGLE ISHOMA',
                    ref: '../btnEdit'
                }
            ]
        };
        jun.BeautyMostReadyGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.store.on('load', this.onStoreLoad, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        this.selectedRecords = this.sm.getSelected();
    },
    onStoreLoad: function (a, b) {
        if (this.selectedRecords != null) {
            this.sm.selectRow(a.indexOf(this.selectedRecords), true);
            this.selectedRecords = null;
        }
    },
    loadEditForm: function (t, e) {
        t.setDisabled(true);
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih beauty");
            t.setDisabled(false);
            return;
        }
        if (selectedz.json.note_ != 'ISHOMA' && selectedz.json.note_ != 'KOSONG') {
            Ext.MessageBox.alert("Warning", "Beauty tidak dalam status KOSONG atau ISHOMA");
            t.setDisabled(false);
            return;
        }
        var beauty_onduty_id = selectedz.json.beauty_onduty_id;
        Ext.Ajax.request({
            url: 'BeautyOnduty/update/',
            method: 'POST',
            params: {
                beauty_onduty_id: beauty_onduty_id
            },
            success: function (f, a) {
                t.setDisabled(false);
                jun.rztBeautyOndutyStatus.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                t.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
// jun.RowEditor = new Ext.ux.grid.RowEditor({saveText: 'Update'});
jun.BeautyPerawatanGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Service",
    id: 'docs-jun.BeautyPerawatanGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    // sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    // plugins: [new Ext.ux.grid.RowEditor({saveText: 'Update'})],
    plugins: [{
            ptype: 'roweditor',
            clicksToEdit: 1,
            listeners: {
                afteredit: function () {
                    this.grid.store.commitChanges();
                }
            }
        }],
    clicksToEdit: 1,
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'Items Code',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_barang'
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 25,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Beauty',
            sortable: true,
            resizable: true,
            dataIndex: 'beauty_id',
            renderer: jun.renderBeauty,
            editor: {
                xtype: 'combo',
                //typeAhead: true,
                fieldLabel: 'Beauty 1',
                ref: '../../beauty1',
                triggerAction: 'all',
                editable: false,
                lazyRender: true,
                mode: 'local',
                store: jun.rztBeautyOffduty1Cmp,
                valueField: 'beauty_id',
                displayField: 'nama_beauty',
                allowBlank: true,
                anchor: '100%',
                listWidth: 400,
                lastQuery: ''
            }
        },
        // {
        //     header: 'Beauty 2',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'beauty2_id',
        //     renderer: jun.renderBeauty,
        //     editor: {
        //         xtype: 'combo',
        //         //typeAhead: true,
        //         fieldLabel: 'Beauty 2',
        //         ref: '../beauty2',
        //         // triggerAction: 'all',
        //         editable: false,
        //         lazyRender: true,
        //         mode: 'local',
        //         store: jun.rztBeautyOffduty2Cmp,
        //         valueField: 'beauty_id',
        //         displayField: 'nama_beauty',
        //         allowBlank: true,
        //         anchor: '100%',
        //         listWidth: 400,
        //         lastQuery: ''
        //     }
        // },
        // {
        //     header: 'Beauty 3',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'beauty3_id',
        //     renderer: jun.renderBeauty,
        //     editor: {
        //         xtype: 'combo',
        //         //typeAhead: true,
        //         fieldLabel: 'Beauty 3',
        //         ref: '../beauty3',
        //         // triggerAction: 'all',
        //         editable: false,
        //         lazyRender: true,
        //         mode: 'local',
        //         store: jun.rztBeautyOffduty3Cmp,
        //         valueField: 'beauty_id',
        //         displayField: 'nama_beauty',
        //         allowBlank: true,
        //         anchor: '100%',
        //         listWidth: 400,
        //         lastQuery: ''
        //     }
        // },
        // {
        //     header: 'Beauty 4',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'beauty4_id',
        //     renderer: jun.renderBeauty,
        //     editor: {
        //         xtype: 'combo',
        //         //typeAhead: true,
        //         fieldLabel: 'Beauty 4',
        //         ref: '../beauty4',
        //         // triggerAction: 'all',
        //         editable: false,
        //         lazyRender: true,
        //         mode: 'local',
        //         store: jun.rztBeautyOffduty4Cmp,
        //         valueField: 'beauty_id',
        //         displayField: 'nama_beauty',
        //         allowBlank: true,
        //         anchor: '100%',
        //         listWidth: 400,
        //         lastQuery: ''
        //     }
        // },
        // {
        //     header: 'Beauty 5',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'beauty5_id',
        //     renderer: jun.renderBeauty,
        //     editor: {
        //         xtype: 'combo',
        //         //typeAhead: true,
        //         fieldLabel: 'Beauty 5',
        //         ref: '../beauty5',
        //         // triggerAction: 'all',
        //         editable: false,
        //         lazyRender: true,
        //         mode: 'local',
        //         store: jun.rztBeautyOffduty5Cmp,
        //         valueField: 'beauty_id',
        //         displayField: 'nama_beauty',
        //         allowBlank: true,
        //         anchor: '100%',
        //         listWidth: 400,
        //         lastQuery: ''
        //     }
        // },
        {
            header: 'Dokter',
            sortable: true,
            resizable: true,
            dataIndex: 'dokter_id',
            renderer: jun.renderDokter,
            editor: {
                xtype: 'combo',
                //typeAhead: true,
                fieldLabel: 'Doctor',
                ref: '../dokter',
                // triggerAction: 'all',
                editable: false,
                lazyRender: true,
                mode: 'local',
                // id: 'dokteredit_id',
                store: jun.rztDokterStatus,
                // hiddenName: 'dokter_id',
                valueField: 'dokter_id',
                displayField: 'nama_dokter',
                allowBlank: true,
                anchor: '100%',
                listWidth: 400,
                lastQuery: ''
            }
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
        if (jun.rztBeautyLib.getTotalCount() === 0) {
            jun.rztBeautyLib.load();
        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
        if (jun.rztBeautyOffduty1Cmp.getTotalCount() === 0) {
            jun.rztBeautyOffduty1Cmp.load();
        }
        // if (jun.rztBeautyOffduty2Cmp.getTotalCount() === 0) {
        //     jun.rztBeautyOffduty2Cmp.load();
        // }
        // if (jun.rztBeautyOffduty3Cmp.getTotalCount() === 0) {
        //     jun.rztBeautyOffduty3Cmp.load();
        // }
        // if (jun.rztBeautyOffduty4Cmp.getTotalCount() === 0) {
        //     jun.rztBeautyOffduty4Cmp.load();
        // }
        // if (jun.rztBeautyOffduty5Cmp.getTotalCount() === 0) {
        //     jun.rztBeautyOffduty5Cmp.load();
        // }
        this.store = jun.rztBeautyPerawatan;
        jun.BeautyPerawatanGrid.superclass.initComponent.call(this);
        this.getColumnModel().getColumnAt(3).editor.on('expand', this.beautyExpand_, this);
        this.getColumnModel().getColumnAt(3).editor.on('collapse', this.beautyCollapse_, this);
        // this.getColumnModel().getColumnAt(4).editor.on('expand', this.beautyExpand_, this);
        // this.getColumnModel().getColumnAt(4).editor.on('collapse', this.beautyCollapse_, this);
        // this.getColumnModel().getColumnAt(5).editor.on('expand', this.beautyExpand_, this);
        // this.getColumnModel().getColumnAt(5).editor.on('collapse', this.beautyCollapse_, this);
        // this.getColumnModel().getColumnAt(6).editor.on('expand', this.beautyExpand_, this);
        // this.getColumnModel().getColumnAt(6).editor.on('collapse', this.beautyCollapse_, this);
        // this.getColumnModel().getColumnAt(7).editor.on('expand', this.beautyExpand_, this);
        // this.getColumnModel().getColumnAt(7).editor.on('collapse', this.beautyCollapse_, this);
        this.getColumnModel().getColumnAt(4).editor.on('expand', this.dokterCollapse_, this);
        this.getColumnModel().getColumnAt(4).editor.on('collapse', this.dokterExpand_, this);
    },
    beautyCollapse_: function (c) {
        c.getStore().clearFilter();
    },
    beautyExpand_: function (c) {
        c.getStore().filter([
            {
                property: 'active',
                value: '1',
                anyMatch: false,
                caseSensitive: false
            },
            {
                property: 'off_',
                value: '1',
                anyMatch: false,
                caseSensitive: false
            }
        ]);
    },
    dokterCollapse_: function (c) {
        c.getStore().clearFilter();
    },
    dokterExpand_: function (c) {
        c.getStore().filter('active', '1', false, false);
    }
});
jun.AntrianPerawatWin = Ext.extend(Ext.Panel, {
    title: "Antrian",
    id: 'docs-jun.AntrianPerawatWin',
    anchor: '100%',
    layout: 'hbox',
    layoutConfig: {
        // padding: 5,
        align: 'stretch'
    },
    autoScroll: true,
    initComponent: function () {
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'PANGGIL',
                            width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnPanggil',
                            hidden: true//NATASHA_CUSTOM
                        },
                        {
                            xtype: 'button',
                            text: 'ISI DATA',
                            width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnIsidata',
                             hidden: false//!NATASHA_CUSTOM
                        },
                        {
                            xtype: 'button',
                            text: 'ULANGI',
                            width: 75,
                            disabled: true,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnUlang'
                        },
                        {
                            xtype: 'button',
                            text: 'PENDING',
                            width: 75,
                            disabled: true,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnPending'
                        },
                        {
                            xtype: 'button',
                            text: 'START',
                            width: 75,
                            disabled: true,
                            hidden:true,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnSpesial'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    ref: '../grplabel',
                    defaults: {
                        scale: 'large'
                    },
                    width: 350,
                    items: [
                        {
                            xtype: 'label',
                            style: {
                                padding: '1px',
                                font: 'bold 35px sans-serif'
                            },
                            ref: '../../lblAntrian'
                        },
                        {
                            xtype: 'label',
                            style: {
                                font: '35px courier'
                            },
                            text: '{'
                        },
                        {
                            xtype: 'label',
                            style: {
                                padding: '1px',
                                font: 'bold 12px sans-serif'
                            },
                            ref: '../../lblBagian'
                        },
                        {
                            xtype: 'label',
                            style: {
                                font: '35px courier'
                            },
                            text: '}'
                        }
                    ]
                }
            ]
        };
        this.items = [
            new jun.BeautyServicesOpenGrid({
                frameHeader: !1,
                margins: '0 1 0 0',
                header: !1,
                flex: 5
            }),
            new jun.BeautyServicesPendingGrid({
                frameHeader: !1,
                header: !1,
                flex: 5
            })
        ];
        jun.AntrianPerawatWin.superclass.initComponent.call(this);
        this.btnPanggil.on('click', this.onPanggilClick, this);
        this.btnUlang.on('click', this.onUlangClick, this);
        this.btnIsidata.on('click', this.onIsidataClick, this);
        this.btnPending.on('click', this.onPendingClick, this);
        this.btnSpesial.on('click', this.onSpesialClick, this);
        this.updateLabel();
        // this.on("afterrender", this.onafterrender, this);
    },
    onIsidataClick: function (t, e) {
        this.btnPanggil.setDisabled(true);
        var selectedz = Ext.getCmp('docs-jun.BeautyServicesOpenGrid').sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih antrian perawatan.");
            this.updateLabel();
            return;
        }
        if (selectedz.data.salestrans_id == null) {
            Ext.MessageBox.alert("Warning", "Antrian belum transaksi.");
            this.updateLabel();
            return;
        }
        if (selectedz.data.end_ == '0') {
            Ext.MessageBox.alert("Warning", "Antrian belum selesai transaksi.");
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/PanggilCounter/',
            method: 'POST',
            scope: this,
            params: {
                bagian: 'perawatan',
                id_antrian: selectedz.data.id_antrian,
                isi_data:'true'
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success) {
                    if (response.msg != "Tidak ada antrian.") {
                        var person = {
                            nomor: response.msg[0],
                            bagian: response.msg[1],
                            counter: response.msg[2],
                            no_base: response.msg[3],
                            id_antrian: response.msg[4],
                            customer_id: response.msg[5],
                            nama_customer: response.msg[6]
                        };
                        Locstor.set('AntrianPerawatan', person);
                        this.updateLabel();
                        this.onSpesialClick();
                        return;
                    } else {
                        Locstor.remove("AntrianPerawatan");
                        Ext.Msg.alert('Information', response.msg);
                    }
                } else {
                    Ext.Msg.alert('Information', response.msg);
                }
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onUlangClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianPerawatan');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/UlangiCounter/',
            method: 'POST',
            scope: this,
            params: {
                nomor: storedNames.nomor,
                bagian: 'perawatan',
                no_base: storedNames.no_base,
                id_antrian: storedNames.id_antrian,
                customer_id: storedNames.customer_id,
                nama_customer: storedNames.nama_customer
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (!response.success) {
                    Ext.Msg.alert('Information', response.msg);
                }
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onPanggilClick: function () {
        this.btnPanggil.setDisabled(true);
        var selectedz = Ext.getCmp('docs-jun.BeautyServicesOpenGrid').sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih antrian perawatan.");
            this.updateLabel();
            return;
        }
        if (selectedz.data.salestrans_id == null) {
            Ext.MessageBox.alert("Warning", "Antrian belum transaksi.");
            this.updateLabel();
            return;
        }
        if (selectedz.data.end_ == '0') {
            Ext.MessageBox.alert("Warning", "Antrian belum selesai transaksi.");
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/PanggilCounter/',
            method: 'POST',
            scope: this,
            params: {
                bagian: 'perawatan',
                id_antrian: selectedz.data.id_antrian
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success) {
                    if (response.msg != "Tidak ada antrian.") {
                        var person = {
                            nomor: response.msg[0],
                            bagian: response.msg[1],
                            counter: response.msg[2],
                            no_base: response.msg[3],
                            id_antrian: response.msg[4],
                            customer_id: response.msg[5],
                            nama_customer: response.msg[6]
                        };
                        Locstor.set('AntrianPerawatan', person);
                        this.updateLabel();
                        return;
                    } else {
                        Locstor.remove("AntrianPerawatan");
                        Ext.Msg.alert('Information', response.msg);
                    }
                } else {
                    Ext.Msg.alert('Information', response.msg);
                }
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    updateLabel: function () {
        var storedNames = Locstor.get('AntrianPerawatan');
        if (storedNames !== null) {
            this.btnPanggil.setDisabled(true);
            var v = Ext.getCmp('docs-jun.PerawatTambahanGrid');
            if (v != undefined) {
                v.btnAdd.setDisabled(true);
            }
            this.btnPending.setDisabled(false);
            this.btnUlang.setDisabled(false);
            this.btnSpesial.setDisabled(false);
            this.lblAntrian.setText(storedNames.nomor);
            this.lblBagian.setText(storedNames.no_base + '<br/>' + storedNames.nama_customer, false);
        } else {
            this.btnPanggil.setDisabled(false);
            var v = Ext.getCmp('docs-jun.PerawatTambahanGrid');
            if (v != undefined) {
                v.btnAdd.setDisabled(false);
            }
            this.btnPending.setDisabled(true);
            this.btnUlang.setDisabled(true);
            this.btnSpesial.setDisabled(true);
            this.lblAntrian.setText('');
            this.lblBagian.setText('TIDAK ADA ANTRIAN');
        }
        this.grplabel.doLayout();
    },
    onPendingClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianPerawatan');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                mode: 'pending',
                bagian: 'perawatan',
                nomor: storedNames.nomor,
                no_base: storedNames.no_base,
                id_antrian: storedNames.id_antrian,
                customer_id: storedNames.customer_id,
                nama_customer: storedNames.nama_customer
            },
            success: function (f, a) {
                // this.refreshStoreAntrian();
                Locstor.remove("AntrianPerawatan");
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onSpesialClick: function () {
        this.btnSpesial.setDisabled(true);
        var storedNames = Locstor.get('AntrianPerawatan');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'BeautyServices/PanggilAntrian/',
            method: 'POST',
            scope: this,
            params: {
                nomor: storedNames.nomor,
                no_base: storedNames.no_base,
                id_antrian: storedNames.id_antrian,
                customer_id: storedNames.customer_id,
                nama_customer: storedNames.nama_customer
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (parseInt(response.total) > 0) {
                    var rec = new Ext.data.Record(response.results);
                    var idz = rec.data.salestrans_id;
                    var dt = new Date;
                    rec.data.start_at = dt;
                    rec.data.end_estimate = dt.add(Date.SECOND, parseInt(rec.data.total_durasi));
                    var form = Ext.getCmp('docs-jun.BeautyPerawatanWin');
                    if (form == undefined || form == null) {
                        form = new jun.BeautyPerawatanWin({modez:0});
                    }
                    form.modez = 0;

                    form.btnStart.setDisabled(true);
                    form.btnStart.setVisible(true);
                    form.btnPanggil.setVisible(true);
                    form.btnUlang.setVisible(true);
                    form.btnUlang.setDisabled(true);
                    form.formz.getForm().loadRecord(rec);
                    form.show(this);
                    jun.rztBeautyPerawatan.load({
                        params: {
                            salestrans_id: idz
                        },
                        scope: this
                    });
                } else {
                    Ext.Msg.alert('Info', 'Tidak ada antrian perawatan.');
                }
                this.updateLabel();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.updateLabel();
            }
        });
    }
});
jun.PerawatTambahanGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Perawatan Tambahan",
    id: 'docs-jun.PerawatTambahanGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
//        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'No. Receipt',
            sortable: false,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 150
        },
        {
            header: 'No. Pasien',
            sortable: false,
            resizable: true,
            dataIndex: 'no_customer',
            width: 75
        },
        {
            header: 'Name',
            sortable: false,
            resizable: true,
            dataIndex: 'nama_customer',
            width: 150
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
        if (jun.rztBeautyLib.getTotalCount() === 0) {
            jun.rztBeautyLib.load();
        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
        if (jun.rztBeauty1Cmp.getTotalCount() === 0) {
            jun.rztBeauty1Cmp.load();
        }
        // if (jun.rztBeauty2Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty2Cmp.load();
        // }
        // if (jun.rztBeauty3Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty3Cmp.load();
        // }
        // if (jun.rztBeauty4Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty4Cmp.load();
        // }
        // if (jun.rztBeauty5Cmp.getTotalCount() === 0) {
        //     jun.rztBeauty5Cmp.load();
        // }
        this.store = jun.rztTambahanBeautyTrans;
        this.bbar = {
            items: [
                {
                    xtype: 'button',
                    text: 'Edit Service',
                    ref: '../btnAdd',
                    hidden: true
                },
                {
                    xtype: 'button',
                    text: 'Start VIP',
                    ref: '../btnStart'
                },
                '->',
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        jun.PerawatTambahanGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnStart.on('Click', this.startOnClick, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianPerawatan');
        if (storedNames != null) {
            Ext.Msg.alert('Info', 'Perawatan antrian harus diselesaikan dulu.');
            t.setDisabled(false);
            return;
        }
        Ext.Ajax.request({
            url: 'BeautyServices/PanggilAntrian/',
            method: 'POST',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (parseInt(response.total) > 0) {
                    var rec = new Ext.data.Record(response.results);
                    var idz = rec.data.salestrans_id;
                    var dt = new Date;
                    rec.data.start_at = dt;
                    rec.data.end_estimate = dt.add(Date.SECOND, parseInt(rec.data.total_durasi));
                    var form = Ext.getCmp('docs-jun.BeautyPerawatanWin');
                    if (form == undefined || form == null) {
                        form = new jun.BeautyPerawatanWin();
                    }
                    form.modez = 0;
                    form.formz.getForm().loadRecord(rec);
                    form.show(this);
                    jun.rztBeautyPerawatan.load({
                        params: {
                            salestrans_id: idz
                        },
                        scope: this
                    });
                } else {
                    Ext.Msg.alert('Info', 'Tidak ada antrian perawatan.');
                }
                t.setDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                t.setDisabled(false);
            }
        });
    },
    startOnClick: function () {
//        this.btnSpesial.setDisabled(true);
//        var storedNames = Locstor.get('AntrianPerawatan');
//        if (storedNames == null) {
//            Ext.Msg.alert('Failure', 'Local storage failed');
//            this.updateLabel();
//            return;
//        }        
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var no_base = this.record.get('no_customer');
        var idz = this.record.get('salestrans_id');
        
        Ext.Ajax.request({
            url: 'BeautyServices/PanggilAntrian/',
            method: 'POST',
            scope: this,
            params: {
//                nomor: storedNames.nomor,
                no_base: no_base,
                id_antrian: 'vip',
                salestrans_id: idz
//                nama_customer: storedNames.nama_customer
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (parseInt(response.total) > 0) {
                    var rec = new Ext.data.Record(response.results);
//                    var idz = rec.data.salestrans_id;
//                    var idz = this.record.get('salestrans_id');
                    var dt = new Date;
                    rec.data.start_at = dt;
                    rec.data.end_estimate = dt.add(Date.SECOND, parseInt(rec.data.total_durasi));
                    var form = Ext.getCmp('docs-jun.BeautyPerawatanWin');
                    if (form == undefined || form == null) {
                        form = new jun.BeautyPerawatanWin();
                    }
                    form.btnStart.setVisible(true);
                    form.btnStart.setDisabled(false);
                    form.btnPanggil.setVisible(false);
                    form.btnUlang.setVisible(false);
                    form.modez = 0;
                    form.formz.getForm().loadRecord(rec);
                    form.show(this);
                    jun.rztBeautyPerawatan.load({
                        params: {
                            salestrans_id: idz
                        },
                        scope: this
                    });
                } else {
                    Ext.Msg.alert('Info', 'Tidak ada antrian perawatan.');
                }
                this.updateLabel();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.updateLabel();
            }
        });
    }});
jun.indexPrint = 0;
jun.indexMax = 0;