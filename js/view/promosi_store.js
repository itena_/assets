jun.Promosistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Promosistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PromosiStoreId',
            url: 'Promosi',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'promosi_id'},
                {name: 'nama_promosi'},
                {name: 'awal'},
                {name: 'akhir'},
                {name: 'time_awal'},
                {name: 'time_akhir'},
                {name: 'note_'},
                {name: 'kode_promosi'}
            ]
        }, cfg));
    }
});
jun.rztPromosi = new jun.Promosistore();
jun.rztPromosiLib = new jun.Promosistore();
jun.rztPromosiCmp = new jun.Promosistore();
jun.rztPromosiLib.load();
