jun.Salestransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Salestransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SalestransStoreId',
            url: 'Salestrans',
            root: 'results',
            idProperty: 'salestrans_id',
            totalProperty: 'total',
            fields: [
                {name: 'tgl'},
                {name: 'salestrans_id'},
                {name: 'doc_ref'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'bruto'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'totalpot'},
                {name: 'total'},
                {name: 'ketdisc'},
                {name: 'vat'},
                {name: 'customer_id'},
                {name: 'doc_ref_sales'},
                {name: 'audit'},
                {name: 'store'},
                {name: 'printed'},
                {name: 'override'},
                {name: 'bayar'},
                {name: 'kembali'},
                {name: 'dokter_id'},
                {name: 'log'},
                {name: 'up'},
                {name: 'total_discrp1'},
                {name: 'type_'},
                {name: 'nama_customer'},
                {name: 'no_customer'},
                {name: 'rounding'},
                {name: 'total_durasi'},
                {name: 'end_estimate'},
                {name: 'konsul_id'},
                {name: 'pending'},
                {name: 'end_at'},
                {name: 'start_at'},
                {name: 'kmr_total'},
                {name: 'kmr_persen'},
                {name: 'parent_id'},
                {name: 'retur_note'},
                {name: 'total_durasi'},
                {name: 'add_durasi'},
                {name: 'add_jasa'},
                {name: 'counter'},
                {name: 'id_antrian'},
                {name: 'beauty_id'},
                {name: 'stocker_id'},
                {name: 'apoteker_id'},
                {name: 'notebeauty'}
            ]
        }, cfg));
    }
});
jun.rztSalestrans = new jun.Salestransstore();
jun.rztSalestransCounter = new jun.Salestransstore();
jun.rztSalestransLib = new jun.Salestransstore();
jun.rztSalestransMgMCmp = new jun.Salestransstore();
jun.rztSalestransMsDCmp = new jun.Salestransstore();
jun.rztSalestransULPTCmp = new jun.Salestransstore();
jun.rztSalestransRPGCmp = new jun.Salestransstore();
jun.rztHistory = new jun.Salestransstore({url: 'salestrans/History'});
jun.rztProsesBeautyTrans = new jun.Salestransstore({
    url: 'BeautyServices/Proses'
});
jun.rztTambahanBeautyTrans = new jun.Salestransstore({
    url: 'BeautyServices/Tambahan'
});
jun.rztSalestransList = new jun.Salestransstore({
    url: 'salestrans/salesList'
});