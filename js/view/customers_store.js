jun.Customersstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Customersstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CustomersStoreId',
            url: 'Customers',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'customer_id'},
                {name: 'nama_customer'},
                {name: 'no_customer'},
                {name: 'tempat_lahir'},
                {name: 'tgl_lahir', type: 'date'},
                {name: 'email'},
                {name: 'telp'},
                {name: 'alamat'},
                {name: 'kota'},
                {name: 'negara'},
                {name: 'awal'},
                {name: 'akhir'},
                {name: 'store'},
                {name: 'negara_id'},
                {name: 'provinsi_id'},
                {name: 'kota_id'},
                {name: 'kecamatan_id'},
                {name: 'status_cust_id'},
                {name: 'sex'},
                {name: 'kerja'},
                {name: 'log'},
                {name: 'up'},
                {name: 'store'},
                {name: 'cardtype'},
                {name: 'validcard'},
                {name: 'printcarddate'},
                {name: 'locprintcard'},
                {name: 'friend_id'},
                {name: 'info_id'}
            ]
        }, cfg));
    }
});
jun.rztCustomersCmp = new jun.Customersstore();
jun.rztCustomersMsDCmp = new jun.Customersstore();
jun.rztCustomersSalesCmp = new jun.Customersstore();
jun.rztCustomersDokterCmp = new jun.Customersstore();
jun.rztCustomersAntrianFOCmp = new jun.Customersstore();
jun.rztCustomersSalesCounterCmp = new jun.Customersstore();
jun.rztCustomersImportSalesCmp = new jun.Customersstore();
jun.rztCustomersReturSalesCmp = new jun.Customersstore();
jun.rztCustomersRefCmp = new jun.Customersstore();
jun.rztCustomersMgMCmp = new jun.Customersstore();
jun.rztCustomersPointInCmp = new jun.Customersstore();
jun.rztCustomersPointOutCmp = new jun.Customersstore();
jun.rztCustomers = new jun.Customersstore();
jun.rztCustomersDetailsCmp = new jun.Customersstore();
jun.rztCustomersMasterCmp = new jun.Customersstore();
jun.rztCustomersPiutangCmp = new jun.Customersstore();
//jun.rztCustomersCmp.load();
