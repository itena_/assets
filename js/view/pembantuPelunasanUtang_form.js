jun.PembantuPelunasanUtangUpdateTgl = Ext.extend(Ext.Window, {
    title: "Update TGL",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 115,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-PembantuPelunasanUtangUpdateTgl",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Tgl',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl",
                        ref: '../tglfrom',
                        anchor: "100%"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Update TGL",
                    ref: "../btnSave"
                }
            ]
        };
        jun.PembantuPelunasanUtangUpdateTgl.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        this.btnSave.setDisabled(true);
        var urlz = 'pembantuPelunasanUtang/updateTgl/';
        Ext.getCmp('form-PembantuPelunasanUtangUpdateTgl').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                id: this.id
            },
            success: function (f, a) {
                jun.rztPembantuPelunasanUtang.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnSave.setDisabled(false);
            }
        });
    }
});

jun.PembantuPelunasanUtangWin = Ext.extend(Ext.Window, {
    title: 'Pembantu Pelunasan Hutang',
    modez: 1,
    width: 790,
    height: 530,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-PembantuPelunasanUtang',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        id: 'tglid',
                        name: 'tgl',
                        fieldLabel: 'tgl',
                        format: 'd M Y',
//                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        width: 175,
                        x: 375,
                        y: 2
                    },
//                    {
//                        xtype: "label",
//                        text: "Suplier:",
//                        x: 295,
//                        y: 35
//                    },
//                    {
//                        xtype: 'combo',
//                        //typeAhead: true,
//                        ref: '../supplier',
//                        triggerAction: 'all',
//                        lazyRender: true,
//                        mode: 'local',
//                        store: jun.rztSupplierCmp,
//                        emptyText: "Please select supplier",
//                        hiddenName: 'supplier_id',
//                        valueField: 'supplier_id',
//                        displayField: 'supplier_name',
//                        allowBlank: true,
//                        listWidth: 350,
//                        matchFieldWidth: !1,
//                        width: 175,
//                        x: 375,
//                        y: 32
//                    },
                    {
                        xtype: "label",
                        text: "Payment:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Payment Method',
                        store: jun.rztBankCmpPusat,
                        ref: '../id_bank',
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        width: 175,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Store:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        valueField: 'store_kode',
                        value: STORE,
                        displayField: 'store_kode',
                        allowBlank: true,
                        readOnly: true,
                        listWidth: 350,
//                        matchFieldWidth: !1,
                        width: 175,
                        x: 375,
                        y: 32
                    },
                    new jun.PembantuPelunasanUtangDetilGrid({
                        x: 5,
                        y: 65,
                        height: 305,
                        frameHeader: !1,
                        header: !1,
                        ref: '../griddetils'
                    }),
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 295 + 200,
                        y: 425 - 50
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'total',
                        hideLabel: false,
                        width: 175,
                        name: 'total',
                        id: 'totalPelunasanUtangid',
                        ref: '../total',
                        maxLength: 30,
                        value: 0,
                        x: 375 + 200,
                        y: 422 - 52
                    },
                    {
                        xtype: "label",
                        text: "Rounding:",
                        x: 295 + 200,
                        y: 425 - 20
                    },
                    {
                        xtype: 'numericfield',
                        //typeAhead: true,
                        value: 0,
                        name: 'rounding',
                        ref: '../rounding',
                        width: 175,
                        x: 375 + 200,
                        y: 422 - 22
                    },
                    {
                        xtype: "label",
                        text: "Amount Bank:",
                        x: 295 + 200,
                        y: 425 - 20 +30
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'amount_bank',
                        hideLabel: false,
                        width: 175,
                        name: 'amount_bank',
//                        id: 'totalPelunasanUtangid',
                        ref: '../amount_bank',
                        maxLength: 30,
                        value: 0,
                        x: 375 + 200,
                        y: 422 - 22 + 30
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PembantuPelunasanUtangWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
//        this.supplier.on('select', this.onSupplierChange, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }

    },
    onWinClose: function () {
        jun.rztPembantuPelunasanUtangDetil.removeAll();
        jun.rztPembantuFakturUtang.removeAll();
    },
//    onSupplierChange: function () {
////        this.id_bank.reset();
//        this.griddetils.jual.reset();
//        this.griddetils.kas.reset();
//        this.griddetils.type_.reset();
//        var supplier_id = this.supplier.getValue();
//        jun.rztPembantuPelunasanUtangDetil.removeAll();
//        
//        jun.rztPembantuFakturUtang.baseParams = {
//            supplier_id: supplier_id
//        };
//        jun.rztPembantuFakturUtang.load();
//        jun.rztPembantuFakturUtang.baseParams = {};
//    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'PembantuPelunasanUtang/create';
        Ext.getCmp('form-PembantuPelunasanUtang').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                        jun.rztPembantuPelunasanUtangDetil.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztPembantuPelunasanUtang.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PembantuPelunasanUtang').getForm().reset();
                    this.onWinClose();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert(a.response.statusText, a.response.responseText);
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});
