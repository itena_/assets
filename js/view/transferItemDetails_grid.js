jun.ReturnTransferItemDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "TransferItemDetails",
    id: 'docs-jun.ReturnTransferItemDetailsGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Item Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Item Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderBarang
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        if(!this.readOnly){
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 6,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Item :'
                            },
                            {
                                xtype: 'combo',
                                //typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
    //                            style: 'margin-bottom:2px',
                                forceSelection: true,
                                store: jun.rztBarangPurchasable,
                                hiddenName: 'barang_id',
                                valueField: 'barang_id',
                                ref: '../../barang',
                                displayField: 'kode_barang'
    //                            colspan: 3
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty :'
                            },
                            {
                                xtype: 'numericfield',
                                id: 'qtyid',
                                ref: '../../qty',
                                width: 50,
                                value: 1,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: '\xA0Unit:'
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                ref: '../../sat',
                                text: '\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0\xA0'
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        defaults: {
                            scale: 'small',
                            width: 40
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Add',
                                ref: '../../btnAdd'
                            },
                            {
                                xtype: 'button',
                                text: 'Edit',
                                ref: '../../btnEdit'
                            },
                            {
                                xtype: 'button',
                                text: 'Del',
                                ref: '../../btnDelete'
                            }
                        ]
                    }
                ]
            };
        }
        jun.ReturnTransferItemDetailsGrid.superclass.initComponent.call(this);
        if(!this.readOnly){
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.loadEditForm, this);
            this.btnDelete.on('Click', this.deleteRec, this);
            this.barang.on('Change', this.onItemChange, this);
        }
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onItemChange: function () {
        var barang_id = this.barang.getValue();
        var barang = jun.getBarang(barang_id);
        this.sat.setText(barang.data.sat);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "You have not selected a item");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
        if (this.btnEdit.text != 'Save') {
            var a = jun.rztTransferItemDetails.findExact("barang_id", barang_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Item already inputted");
                return
            }
        }
        var qty = parseFloat(this.qty.getValue());
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.commit();
        } else {
            var c = jun.rztTransferItemDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty
                });
            jun.rztTransferItemDetails.add(d);
        }
        this.barang.reset();
        this.qty.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            this.qty.setValue(record.data.qty);
            btn.setText("Save");
            this.btnDisable(true);
            this.onItemChange();
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }
});
jun.TransferItemDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "TransferItemDetails",
    id: 'docs-jun.TransferItemDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'Kode Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Nama Barang',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarang
        },
        {
            header: 'Charge',
            sortable: true,
            resizable: true,
            dataIndex: 'charge',
            width: 100
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 75,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Price',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Disc',
            sortable: true,
            resizable: true,
            dataIndex: 'disc',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'PPN',
            sortable: true,
            resizable: true,
            dataIndex: 'ppn',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'PPH',
            sortable: true,
            resizable: true,
            dataIndex: 'pph_id',
            width: 100,
            align: "center",
            renderer: function(value, metaData, record, rowIndex){
                switch(value){
                    case 0 : return '-';
                    case 21 : return 'PPH 21';
                    case 22 : return 'PPH 22';
                    case 23 : return 'PPH 23';
                }
            }
        },
        {
            header: 'PPH',
            sortable: true,
            resizable: true,
            dataIndex: 'pph',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        if(!this.readOnly){
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 14,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Barang :'
                            },
                            {
                                xtype: 'combo',
                                style: 'margin-bottom:2px',
                                //typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                forceSelection: true,
                                store: jun.rztBarangPurchasable,
                                //hiddenName: 'barang_id',
                                readOnly: true,
                                valueField: 'barang_id',
                                ref: '../../barang',
                                displayField: 'kode_barang',
                                width: 175
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Charge :'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                ref: '../../charge',
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                store: jun.rztStoreCmp,
                                valueField: 'store_kode',
                                displayField: 'store_kode',
                                width: 100
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty :'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../qty',
                                style: 'margin-bottom:2px',
                                readOnly: true,
                                width: 50,
                                value: 1,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Price :'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../price',
                                width: 75,
                                value: 0,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Disc :'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../disc',
                                width: 40,
                                value: 0,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'PPN :'
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../ppn',
                                width: 40,
                                value: 0,
                                minValue: 0
                            },
                            {
                                xtype: "combo",
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender:true,
                                editable:false,
                                mode: 'local',
                                store: new Ext.data.ArrayStore({
                                    id: 0,
                                    fields: ['pph_id','pph_name'],
                                    data: [[0, 'NO PPH'], [21, 'PPH 21'], [22, 'PPH 22'],  [23, 'PPH 23']]
                                }),
                                value: 0,
                                valueField: 'pph_id',
                                displayField: 'pph_name',
                                ref: '../../pph_id',
                                style: { marginLeft: '3px' },
                                width: 75
                            },
                            {
                                xtype: 'numericfield',
                                ref: '../../pph',
                                width: 40,
                                value: 0,
                                minValue: 0
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 2,
                        defaults: {
                            width: 40
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Add',
                                ref: '../../btnAdd'
                            },
                            {
                                xtype: 'button',
                                text: 'Edit',
                                ref: '../../btnEdit'
                            }
                        ]
                    }
                ]
            };
        }

        this.contextMenu = new Ext.menu.Menu({
            plain: true,
            items: {
                xtype: 'panel',
                layout: 'vbox',
                height: 180,
                width: 250,
                items: [
                    {
                        height: 25,
                        width: '100%',
                        layout: 'form',
                        bodyStyle: "background-color: #E4E4E4;",
                        labelWidth: 40,
                        border: false,
                        defaults: {
                            xtype: 'displayfield',
                            style: {
                                fontWeight: 'bold',
                                align: 'center'
                            },
                            anchor: '100%'
                        },
                        items: [
                            {
                                fieldLabel: 'Item',
                                ref: '../../item_code'
                            }
                        ]
                    },
                    {
                        xtype: 'tabpanel',
                        ref: '../tabpanel',
                        flex: 1,
                        width: '100%',
                        activeTab: 0,
                        border: false,
                        items: [
                            {
                                title: 'Split',
                                layout: 'form',
                                labelWidth: 40,
                                bodyStyle: "background-color: #E4E4E4;padding: 5px;",
                                items: [
                                    {
                                        xtype: 'combo',
                                        fieldLabel: 'Charge',
                                        ref: '../../../charge',
                                        store: jun.rztStoreCmp,
                                        valueField: 'store_kode',
                                        displayField: 'store_kode',
                                        typeAhead: true,
                                        mode: 'local',
                                        triggerAction: 'all',
                                        emptyText: 'Select branch...',
                                        selectOnFocus: true,
                                        anchor: '100%',
                                        getListParent: function() {
                                            return this.el.up('.x-menu');
                                        }
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Qty',
                                        ref: '../../../qty',
                                        minValue: 0,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'button',
                                        ref: '../../../btnSplit',
                                        text: 'Split',
                                        anchor: '100%',
                                        height: 26
                                    }
                                ]
                            },
                            {
                                title: 'Merge',
                                layout: 'fit',
                                labelWidth: 40,
                                bodyStyle: "background-color: #E4E4E4;",
                                items: new jun.TransferItemDetailsMergeGrid({
                                    ref: '../../../mergeGrid',
                                    parent: this
                                })
                            }
                        ]
                    }
                ]
            }
        });

        jun.TransferItemDetailsGrid.superclass.initComponent.call(this);
        if(!this.readOnly){
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.onClickbtnEdit, this);
            this.on('rowcontextmenu', this.showContextMenu, this);
            this.on('sortchange', this.onShortChange, this);

            this.contextMenu.on('hide', this.contextMenuOnHide, this);
            this.contextMenu.btnSplit.on('click', this.splitItem, this);
            this.getSelectionModel().on('rowselect', this.getrow, this);
        }
    },
    onStoreChange: function (s, b, d) {
        s.refreshData();
    },
    onShortChange: function (g, i) {
        this.store.refreshSortData();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    contextMenuOnHide: function(c){
        this.contextMenu.item_code.reset();

        this.contextMenu.charge.reset();
        this.contextMenu.qty.reset();

        this.contextMenu.tabpanel.setActiveTab(0);

        this.contextMenu.mergeGrid.store.removeAll();
    },
    showContextMenu: function (c, idx, e) {
        e.stopEvent();

        this.sm.selectRow(idx);
        if (this.btnEdit.text == 'Edit'){
            this.contextMenu.item_code.setValue(jun.renderKodeBarang(this.record.get('barang_id')));

            this.contextMenu.qty.setMaxValue(this.record.get('qty'));

            this.contextMenu.idx = idx;
            this.contextMenu.showAt(e.xy);

            var item_id = this.record.get('item_id');
            this.store.each(function(r){
                if(r.get('item_id') == item_id && r.id != this.record.id )
                    this.contextMenu.mergeGrid.store.add(r.copy());
            }, this);
        }
    },
    splitItem: function () {
        var qtyBefore = this.record.get('qty'),
            qty = this.contextMenu.qty.getValue(),
            charge = this.contextMenu.charge.getValue(),
            qtyAfter = qtyBefore - qty;

        if(!qty){
            Ext.MessageBox.alert("Warning", "Qty split tidak boleh kosong.");
            this.contextMenu.hide();
            return;
        }
        if(qtyAfter <= 0){
            Ext.MessageBox.alert("Warning", "Qty split tidak boleh lebih besar dari quantity item.");
            this.contextMenu.hide();
            return;
        }

        //update
        this.record.set('qty', qtyAfter);
        this.record.commit();

        //insert
        var rec = new this.store.recordType({
                barang_id: this.record.get('barang_id'),
                item_id: this.record.get('item_id'),
                charge: charge,
                qty: qty,
                price: this.record.get('price'),
                disc: this.record.get('disc'),
                ppn: this.record.get('ppn'),
                pph_id: this.record.get('pph_id'),
                pph: this.record.get('pph')
            });
        this.store.insert(this.contextMenu.idx+1, rec);

        this.store.calculateItem();
        this.contextMenu.hide();
    },
    mergeItem: function (selectedRec) {
        //update
        var qtyAfter = selectedRec.get('qty') + this.record.get('qty'),
            idx = this.store.findBy(function(r, id){ return selectedRec.id == id; }, this),
            targetRec = this.store.getAt(idx);
        targetRec.set('qty', qtyAfter);
        targetRec.commit();

        //remove
        this.store.remove(this.record);

        this.store.calculateItem();
        this.contextMenu.hide();
    },
    resetForm: function(){
        this.barang.reset();
        this.charge.reset();
        //this.note.reset();
        this.disc.reset();
        this.qty.reset();
        this.ppn.reset();
        this.pph_id.reset();
        this.pph.reset();
        this.price.reset();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        var charge = this.charge.getValue();
        //var note = this.note.getValue();
        var disc = parseFloat(this.disc.getValue());
        var qty = parseFloat(this.qty.getValue());
        var price = parseFloat(this.price.getValue());
        var ppn = parseFloat(this.ppn.getValue());
        var pph_id = this.pph_id.getValue();
        var pph = parseFloat(this.pph.getValue());

        var result = jun.calculateItemInvoice(qty, price, disc, ppn, pph_id, pph);
        var sub_total = result['bruto'];
        var disc_rp = result['disc_rp'];
        var total_dpp = result['dpp'];
        var ppn_rp = result['ppn_rp'];
        var pph_rp = result['pph_rp'];
        var total = result['total'];

        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            //record.set('description', note);
            record.set('charge', charge);
            record.set('qty', qty);
            record.set('price', price);
            record.set('sub_total', sub_total);
            record.set('total_dpp', total_dpp);
            record.set('disc', disc);
            record.set('disc_rp', disc_rp);
            record.set('ppn', ppn);
            record.set('ppn_rp', ppn_rp);
            record.set('pph_id', pph_id);
            record.set('pph', pph);
            record.set('pph_rp', pph_rp);
            record.set('total', total);
            record.commit();
        } else {
            var c = this.store.recordType,
                d = new c({
                    barang_id: barang_id,
                    //description: note,
                    charge: charge,
                    qty: qty,
                    price: price,
                    sub_total: sub_total,
                    total_dpp: total_dpp,
                    disc: disc,
                    disc_rp: disc_rp,
                    ppn: ppn,
                    ppn_rp: ppn_rp,
                    pph_id: pph_id,
                    pph: pph,
                    pph_rp: pph_rp,
                    total: total
                });
            this.store.add(d);
        }
        
        this.resetForm();
        return true;
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            this.qty.setValue(record.data.qty);
            this.price.setValue(record.data.price);
            this.disc.setValue(record.data.disc);
            //this.note.setValue(record.data.description);
            this.charge.setValue(record.data.charge);
            this.ppn.setValue(record.data.ppn);
            this.pph_id.setValue(record.data.pph_id);
            this.pph.setValue(record.data.pph);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});

jun.TransferItemDetailsMergeGrid = Ext.extend(Ext.grid.GridPanel, {
    border: false,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        new Ext.grid.RowNumberer(),
        {
            header: 'Charge',
            sortable: true,
            resizable: true,
            dataIndex: 'charge',
            width: 100
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 75,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = new jun.TransferItemDetailsstore();
        jun.TransferItemDetailsMergeGrid.superclass.initComponent.call(this);
        this.on('rowdblclick', this.mergeItem, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    mergeItem: function (c, idx, e) {
        this.parent.mergeItem(this.record);
    }
});
