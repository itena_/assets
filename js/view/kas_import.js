////TIDAK DIPAKAI LAGI
////RACHMAD .A
jun.ImportKasOLD = Ext.extend(Ext.Window, {
    title: "Import Kas/Bank Keluar (HO)",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 260,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "panel",
                bodyStyle: "background-color: #E4E4E4;padding: 5px; margin-bottom: 5px;",
                html: '<span style="color:red;font-weight:bold">PERHATIAN !!!!</span><br><span style="color:red;font-weight:bold">Konsultasi dengan IT jika akan melakukan import !!!!!</span>'
            },
            {
                html: '<input type="file" name="xlfile" id="inputFile" />',
                name: 'file',
                xtype: "panel",
                listeners: {
                    render: function (c) {
                        new Ext.ToolTip({
                            target: c.getEl(),
                            html: 'Format file : Excel (.xls)'
                        });
                    },
                    afterrender: function () {
                        itemFile = document.getElementById("inputFile");
                        itemFile.addEventListener('change', readFileExcel, false);
                    }
                }
            },
            {
                xtype: "panel",
                bodyStyle: "margin-top: 5px;",
                height: 100,
                layout: 'fit',
                items: {
                    xtype: "textarea",
                    readOnly: true,
                    ref: '../log_msg'
                }
            },
            {
                xtype: "form",
                frame: !1,
                id: "form-ImportKas",
                border: !1
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Import",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ImportKas.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnCheckclick: function () {
        if (jun.dataStockOpname == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih file importKas.");
            return;
        }
        jun.importKasStore.loadData(jun.dataStockOpname[jun.namasheet]);
        var win = new jun.CheckImportKasWin({generateQuery: 0});
        win.show(this);
    },
    onbtnSaveclick: function () {
        if (document.getElementById("inputFile").value == '') return;
        this.btnSave.setDisabled(true);
        this.log_msg.setValue('');
        this.writeLog('\n----- MULAI IMPORT KAS -----');
        jun.submitKasCounter = 0;
        this.loopPost();
    },
    loopPost: function () {
        var arrData = jun.dataStockOpname[jun.namasheet];
        if (jun.submitKasCounter >= arrData.length) {
            /*
             * loop berakhir
             */
            this.writeLog('\n----- IMPORT KAS SELESAI -----\n'
                + 'Jumlah data : ' + (jun.submitKasCounter));
            document.getElementById("inputFile").value = '';
            this.btnSave.setDisabled(false);
            jun.dataStockOpname = null;
            return;
        }
        var d = arrData[jun.submitKasCounter];
        //Payment method
        var bank = jun.rztBankTransCmpPusat.find('nama_bank', d.payment);
        if (bank < 0) {
            this.loopLog('GAGAL\nBank tidak ditemukan.');
            this.nextPost();
            return;
        }
        var bank_id = jun.rztBankTransCmpPusat.getAt(bank).get('bank_id');
        //Jenis Kas
        var jenis = jun.jenis_kas.find('desc', d.type);
        if (jenis < 0) {
            this.loopLog('GAGAL\nJenis Kas tidak ditemukan.');
            this.nextPost();
            return;
        }
        var jenis_kas = jun.jenis_kas.getAt(jenis).get('code');
        //COA
        var coa = jun.rztChartMasterCmp.find('account_name', d.d_account_code);
        if (coa < 0) {
            this.loopLog('GAGAL\nAccount code tidak ditemukan.');
            this.nextPost();
            return;
        }
        var account_code = jun.rztChartMasterCmp.getAt(coa).get('account_code');
        //Data detail
        var detail = '[{"debit":' + d.d_debit + ',"kredit":' + d.d_kredit + ',"item_name":"' + d.d_item_name + '","total":' + d.d_total + ',"account_code":"' + account_code + '","store":"' + d.d_store + '"}]';
        Ext.getCmp('form-ImportKas').getForm().submit({
            url: 'Kas/Create',
            timeOut: 1000,
            scope: this,
            params: {
                mode: 0,
                tgl: d.tgl,
                bank_id: bank_id,
                amount: d.amount,
                no_kwitansi: d.no_kwitansi ? d.no_kwitansi : '',
                store: d.store,
                type_: jenis_kas,
                keperluan: d.note ? d.note : '',
                total_debit: d.amount,
                total_kredit: d.amount,
                arus: -1,
                detil: detail
            },
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                this.loopLog(response.msg);
                this.nextPost();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Connection failure has occured.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    nextPost: function () {
        jun.submitKasCounter++;
        this.loopPost();
    },
    loopLog: function (msg) {
        this.writeLog('\n----- ' + (jun.submitKasCounter + 1) + ' dari ' + jun.dataStockOpname[jun.namasheet].length + ' --------------\n' + msg);
    },
    writeLog: function (msg) {
        this.log_msg.setValue(msg + '\n' + this.log_msg.getValue());
    }
});////RACHMAD .A
////RACHMAD .A
////TIDAK DIPAKAI LAGI
jun.submitKasCounter = 0;
jun.importKasStore = new Ext.data.JsonStore({
    storeId: 'jun.importKasStore',
    idProperty: 'tgl',
    fields: [
        'tgl',
        'payment',
        'amount',
        'no_kwitansi',
        'store',
        'type',
        'note',
        'd_item_name',
        'd_debit',
        'd_kredit',
        'd_total',
        'd_account_code',
        'd_store'
    ]
});
jun.CheckImportKasWin = Ext.extend(Ext.Window, {
    title: "Data Stock Opname",
    width: 400,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importKasStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'kode_barang', dataIndex: 'kode_barang', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            if (jun.rztBarangLib.findExact('kode_barang', value) == -1)
                                metaData.style = "background-color: #FF7F7F;";
                            return value;
                        }
                    },
                    {
                        header: 'qty', dataIndex: 'qty', width: 60, align: "right",
                        renderer: Ext.util.Format.numberRenderer("0,0")
                    }
                ]
            }
        ];
        jun.CheckImportKasWin.superclass.initComponent.call(this);
    }
});
// jun.ImportKasMasuk = Ext.extend(Ext.Window, {
//     title: "Import Kas Masuk",
//     iconCls: "silk13-report",
//     modez: 1,
//     width: 500,
//     height: 400,
//     layout: "form",
//     modal: !0,
//     padding: 5,
//     closeForm: !1,
//     resizable: !1,
//     iswin: !0,
//     initComponent: function () {
//         if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
//             jun.rztBankTransCmpPusat.load();
//         }
//         if (jun.rztChartMasterCmp.getTotalCount() === 0) {
//             jun.rztChartMasterCmp.load();
//         }
//         this.items = [
//             {
//                 xtype: "panel",
//                 bodyStyle: "background-color: #E4E4E4;padding: 5px; margin-bottom: 5px;",
//                 html: '<span style="color:red;font-weight:bold">PERHATIAN !!!!</span><br><span style="color:red;font-weight:bold">Konsultasi dengan IT jika akan melakukan import !!!!!</span>'
//             },
//             {
//                 html: '<input type="file" name="xlfile" id="inputFile" />',
//                 name: 'file',
//                 xtype: "panel",
//                 listeners: {
//                     render: function (c) {
//                         new Ext.ToolTip({
//                             target: c.getEl(),
//                             html: 'Format file : Excel (.xls)'
//                         });
//                     },
//                     afterrender: function () {
//                         itemFile = document.getElementById("inputFile");
//                         itemFile.addEventListener('change', readFileExcel, false);
//                     }
//                 }
//             },
//             {
//                 xtype: "panel",
//                 bodyStyle: "margin-top: 5px;",
//                 height: 250,
//                 layout: 'fit',
//                 items: {
//                     xtype: "textarea",
//                     readOnly: true,
//                     ref: '../log_msg',
//                     style: {
//                         'fontFamily': 'courier new',
//                         'fontSize': '12px'
//                     }
//                 }
//             },
//             {
//                 xtype: "form",
//                 frame: !1,
//                 id: "form-ImportKasMasuk",
//                 border: !1
//             }
//         ];
//         this.fbar = {
//             xtype: "toolbar",
//             items: [
//                 {
//                     xtype: "button",
//                     text: "Import",
//                     ref: "../btnSave"
//                 }
//             ]
//         };
//         jun.ImportKasMasuk.superclass.initComponent.call(this);
//         this.btnSave.on("click", this.onbtnSaveclick, this);
//     },
//     onbtnCheckclick: function () {
//         if (jun.dataStockOpname == "") {
//             Ext.MessageBox.alert("Error", "Anda belum memilih file importKas.");
//             return;
//         }
//         jun.importKasStore.loadData(jun.dataStockOpname[jun.namasheet]);
//         var win = new jun.CheckImportInvoiceJournalWin({generateQuery: 0});
//         win.show(this);
//     },
//     onbtnSaveclick: function () {
//         if (document.getElementById("inputFile").value == '') return;
//         this.btnSave.setDisabled(true);
//         this.log_msg.setValue('');
//         this.writeLog('\n----- MULAI IMPORT KAS MASUK -----');
//         this.rowLoopCounter = 0;
//         this.headerCounter = 0;
//         this.dataSubmit = null;
//         this.arrData = jun.dataStockOpname[jun.namasheet];
//         this.loopPost();
//     },
//     headerCounter: 0,
//     dataSubmit: null,
//     arrData: null,
//     loopPost: function () {
//         if (this.rowLoopCounter >= this.arrData.length) {
//             /*
//              * loop berakhir
//              */
//             this.writeLog('\n----- IMPORT KAS MASUK PUSAT SELESAI -----'
//                 + '\nJumlah Baris : ' + this.rowLoopCounter
//                 + '\nJumlah data : ' + this.headerCounter
//                 + '\n-------------------------------------------\n');
//             document.getElementById("inputFile").value = '';
//             this.btnSave.setDisabled(false);
//             this.arrData = null;
//             jun.dataStockOpname = null;
//             return;
//         }
//         var d = this.arrData[this.rowLoopCounter];
//         if (d.jenis_baris == 'H') {
//             this.headerCounter++;
//             this.dataSubmit = {};
//             this.writeLog('\n============== Data ke- ' + this.headerCounter + ' ==============\n');
//             this.loopLog('Data Header nomor : ' + this.headerCounter);
//             var bank = jun.rztBankTransCmpPusat.find('nama_bank', d.bank);
//             if (bank < 0) {
//                 this.writeLog('\n***** GAGAL ****\nBank tidak ditemukan.');
//                 this.btnSave.setDisabled(false);
//                 //this.nextRow();
//                 return;
//             }
//             var bank_id = jun.rztBankTransCmpPusat.getAt(bank).get('bank_id');
//             this.dataSubmit['no_kwitansi'] = d.no_kwitansi;
//             this.dataSubmit['keperluan'] = d.keperluan;
//             this.dataSubmit['total'] = d.total;
//             this.dataSubmit['bank_id'] = bank_id;
//             this.dataSubmit['amount'] = d.total;
//             this.dataSubmit['tgl'] = d.tanggal;
//             this.dataSubmit['type_'] = 0;
//             this.dataSubmit['store'] = d.cabang;
//             this.dataSubmit['up'] = 0;
//             this.dataSubmit['mode'] = 0;
//             this.dataSubmit['arus'] = 1;
//             this.dataSubmit['visible'] = 1;
//             this.dataSubmit['all_store'] = 0;
//             this.dataSubmit['jml_cabang'] = 0;
//             this.dataSubmit['total_debit'] = d.total_debit;
//             this.dataSubmit['total_kredit'] = d.total_kredit;
//             this.dataSubmit['detil'] = [];
//         }
//         else {
//             /*
//              * Detail
//              */
//             if (!this.dataSubmit) {
//                 this.nextRow();
//                 return;
//             }
//             var acc_code = jun.rztChartMasterCmp.find('account_code', d.account_code);
//             if (acc_code < 0) {
//                 this.writeLog('\n***** GAGAL ****\nCOA tidak ditemukan.');
//                 this.btnSave.setDisabled(false);
//                 //this.nextRow();
//                 return;
//             }
// //            var account_code = jun.rztChartMasterCmp.getAt(coa).get('account_code');
//             //Data detail
//             this.dataSubmit['detil'].push('{"item_name":"' + d.nama_item + '","debit":"' + d.debit + '","kredit":"' + d.kredit + '","account_code":"' + d.account_code + '","store":"' + d.cabang + '"}');
//             this.loopLog('Data detail dari Header nomor : ' + this.headerCounter);
//         }
//         var nextRound = this.arrData[this.rowLoopCounter + 1];
//         if (!nextRound || nextRound.jenis_baris == 'H') {
//             this.writeLog('>>> Submit : Data Header nomor : ' + this.headerCounter);
//             if (this.dataSubmit['detil'].length > 0) {
//                 this.dataSubmit['detil'] = '[' + this.dataSubmit['detil'].join(',') + ']';
//             }
//             Ext.getCmp('form-ImportKasMasuk').getForm().submit({
//                 url: 'Kas/Create',
//                 timeOut: 1000,
//                 scope: this,
//                 params: this.dataSubmit,
//                 success: function (f, a) {
//                     var response = Ext.decode(a.response.responseText);
//                     var msg = '';
//                     if (response.success) {
//                         msg = response.msg;
//                     } else {
//                         msg = '***** GAGAL ****\n' + response.msg;
//                         this.btnSave.setDisabled(false);
//                     }
//                     this.writeLog('<<< Result : dari Header nomor : ' + this.headerCounter + '\n' + msg);
//                     if (response.success) this.nextRow();
//                 },
//                 failure: function (f, a) {
//                     switch (a.failureType) {
//                         case Ext.form.Action.CLIENT_INVALID:
//                             Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
//                             break;
//                         case Ext.form.Action.CONNECT_FAILURE:
//                             Ext.Msg.alert('Failure', 'Connection failure has occured.');
//                             break;
//                         case Ext.form.Action.SERVER_INVALID:
//                             Ext.Msg.alert('Failure', a.result.msg);
//                     }
//                 }
//             });
//         } else {
//             this.nextRow();
//         }
//     },
//     nextRow: function () {
//         this.rowLoopCounter++;
//         this.loopPost();
//     },
//     loopLog: function (msg) {
//         this.writeLog('Baris ke- ' + (this.rowLoopCounter + 1) + ' dari ' + jun.dataStockOpname[jun.namasheet].length + ' --> ' + msg);
//     },
//     writeLog: function (msg) {
//         this.log_msg.setValue(msg + '\n' + this.log_msg.getValue());
//     }
// });
jun.ImportKas = Ext.extend(Ext.Window, {
    title: "Import Kas Keluar",
    iconCls: "silk13-report",
    modez: 1,
    width: 500,
    height: 400,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    group: null,
    store: null,
    arus: 1,
    initComponent: function () {
        this.store = new Ext.data.ArrayStore({
            // store configs
            autoDestroy: true,
            // storeId: 'myStore',
            // reader configs
            idIndex: 0,
            fields: [
                {name: "debit", type: "float"},
                {name: "kredit", type: "float"},
                {name: 'item_name'},
                {name: 'total'},
                {name: 'account_code'},
                {name: 'store'}
            ]
        });
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        this.items = [
            {
                xtype: "panel",
                bodyStyle: "background-color: #E4E4E4;padding: 5px; margin-bottom: 5px;",
                html: '<span style="color:red;font-weight:bold">PERHATIAN !!!!</span><br><span style="color:red;font-weight:bold">Konsultasi dengan IT jika akan melakukan import !!!!!</span>'
            },
            {
                html: '<input type="file" name="xlfile" id="inputFile" />',
                name: 'file',
                xtype: "panel",
                listeners: {
                    render: function (c) {
                        new Ext.ToolTip({
                            target: c.getEl(),
                            html: 'Format file : Excel (.xlsx)'
                        });
                    },
                    afterrender: function () {
                        itemFile = document.getElementById("inputFile");
                        itemFile.addEventListener('change', readFileExcel, false);
                    }
                }
            },
            {
                xtype: "panel",
                bodyStyle: "margin-top: 5px;",
                height: 250,
                layout: 'fit',
                items: {
                    xtype: "textarea",
                    readOnly: true,
                    ref: '../log_msg',
                    style: {
                        'fontFamily': 'courier new',
                        'fontSize': '12px'
                    }
                }
            }//,
            // {
            //     xtype: "form",
            //     frame: !1,
            //     id: "form-ImportKasMasuk",
            //     border: !1
            // }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Import",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ImportKas.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.title = (this.arus === 1) ? 'Import Kas Masuk' : 'Import Kas Keluar';
    },
    onbtnCheckclick: function () {
        if (jun.dataStockOpname == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih file importKas.");
            return;
        }
        jun.importKasStore.loadData(jun.dataStockOpname[jun.namasheet]);
        var win = new jun.CheckImportInvoiceJournalWin({generateQuery: 0});
        win.show(this);
    },
    onbtnSaveclick: function () {
        if (document.getElementById("inputFile").value == '') return;
        this.btnSave.setDisabled(true);
        this.log_msg.setValue('');
        this.writeLog('\n----- MULAI ' + this.title.toUpperCase() + ' -----');
        this.rowLoopCounter = 0;
        this.headerCounter = 0;
        this.dataSubmit = {};
        this.dataSubmit['detil'] = [];
        this.store.removeAll();
        this.arrData = jun.dataStockOpname[jun.namasheet];
        this.loopPost();
    },
    headerCounter: 0,
    dataSubmit: null,
    arrData: null,
    loopPost: function () {
        if (this.rowLoopCounter >= this.arrData.length) {
            /*
             * loop berakhir
             */
            this.writeLog('\n----- ' + this.title.toUpperCase() + ' SELESAI -----'
                + '\nJumlah Baris : ' + this.rowLoopCounter
                + '\nJumlah data : ' + this.headerCounter
                + '\n-------------------------------------------\n');
            document.getElementById("inputFile").value = '';
            this.btnSave.setDisabled(false);
            this.arrData = null;
            jun.dataStockOpname = null;
            return;
        }
        var d = this.arrData[this.rowLoopCounter];
        if (this.group !== d.GRUP) {
            this.headerCounter++;
            this.dataSubmit = {};
            this.writeLog('\n============== Data ke- ' + this.headerCounter + ' ==============\n');
            this.loopLog('Data Header nomor : ' + this.headerCounter);
            var bank = jun.rztBankLib.find('nama_bank', d.bank);
            if (bank < 0) {
                this.writeLog('\n***** GAGAL ****\nBank tidak ditemukan.');
                this.btnSave.setDisabled(false);
                //this.nextRow();
                return;
            }
            this.group = d.GRUP;
            var bank_id = jun.rztBankLib.getAt(bank).get('bank_id');
            this.dataSubmit['no_kwitansi'] = d.no_kwitansi;
            this.dataSubmit['keperluan'] = d.keperluan;
            this.dataSubmit['total'] = parseFloat(d.amount);
            this.dataSubmit['bank_id'] = bank_id;
            this.dataSubmit['amount'] = parseFloat(d.amount);
            this.dataSubmit['tgl'] = d.tgl;
            this.dataSubmit['type_'] = 0;
            this.dataSubmit['store'] = STORE;
            this.dataSubmit['up'] = 0;
            this.dataSubmit['mode'] = 0;
            this.dataSubmit['arus'] = this.arus;
            this.dataSubmit['visible'] = 1;
            this.dataSubmit['all_store'] = 0;
            this.dataSubmit['jml_cabang'] = 0;
            // this.dataSubmit['total_debit'] = d.total_debit;
            // this.dataSubmit['total_kredit'] = d.total_kredit;
            this.dataSubmit['detil'] = [];
            this.store.removeAll();
        }
        /*
            * Detail
            */
        if (!this.dataSubmit) {
            this.nextRow();
            return;
        }
        var acc_code = jun.rztChartMasterCmp.find('account_code', d.account_code);
        if (acc_code < 0) {
            this.writeLog('\n***** GAGAL ****\nCOA tidak ditemukan.');
            this.btnSave.setDisabled(false);
            //this.nextRow();
            return;
        }
//            var account_code = jun.rztChartMasterCmp.getAt(coa).get('account_code');
        //Data detail
        // this.dataSubmit['detil'].push('{"item_name":"' + d.note + '","debit":"' + d.debit + '","kredit":"' + d.kredit + '","account_code":"' + d.account_code + '","store":"' + d.cabang + '"}');
        var c = this.store.recordType,
            d = new c({
                account_code: d.account_code,
                item_name: d.note,
                store: d.cabang,
                debit: parseFloat(d.debit),
                kredit: parseFloat(d.kredit)
            });
        this.store.add(d);
        this.loopLog('Data detail dari Header nomor : ' + this.headerCounter);
        var nextRound = this.arrData[this.rowLoopCounter + 1];
        if (!nextRound || nextRound.GRUP !== this.group) {
            this.writeLog('>>> Submit : Data Header nomor : ' + this.headerCounter);
            // if (this.dataSubmit['detil'].length > 0) {
            //     this.dataSubmit['detil'] = '[' + this.dataSubmit['detil'].join(',') + ']';
            // }
            this.dataSubmit['detil'] = Ext.encode(Ext.pluck(
                this.store.data.items, "data"));
            if (this.arus == 1) {
                this.dataSubmit['total_debit'] = (parseFloat(this.store.sum("debit")) + parseFloat(this.dataSubmit['amount']));
                this.dataSubmit['total_kredit'] = (parseFloat(this.store.sum("kredit")));
            } else {
                this.dataSubmit['total_debit'] = this.store.sum("debit");
                this.dataSubmit['total_kredit'] = (parseFloat(this.store.sum("kredit")) + parseFloat(this.dataSubmit['amount']));
            }
            // this.writeLog(JSON.stringify(this.dataSubmit));
            Ext.Ajax.request({
                url: 'Kas/Create',
                method: 'POST',
                scope: this,
                params: this.dataSubmit,
                success: function (f, a) {
                    if (this.arus == 1) {
                        jun.rztKasPusatKeluar.reload();
                    }else{
                        jun.rztKasPusat.reload();
                    }
                    var response = Ext.decode(f.responseText);
                    var msg = '';
                    if (response.success) {
                        msg = response.msg;
                    } else {
                        msg = '***** GAGAL ****\n' + response.msg;
                        this.btnSave.setDisabled(false);
                    }
                    this.writeLog('<<< Result : dari Header nomor : ' + this.headerCounter + '\n' + msg);
                    if (response.success) this.nextRow();
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
            // Ext.getCmp('form-ImportKasMasuk').getForm().submit({
            //     url: 'Kas/Create',
            //     timeOut: 1000,
            //     scope: this,
            //     params: this.dataSubmit,
            //     success: function (f, a) {
            //         var response = Ext.decode(a.response.responseText);
            //         var msg = '';
            //         if (response.success) {
            //             msg = response.msg;
            //         } else {
            //             msg = '***** GAGAL ****\n' + response.msg;
            //             this.btnSave.setDisabled(false);
            //         }
            //         this.writeLog('<<< Result : dari Header nomor : ' + this.headerCounter + '\n' + msg);
            //         if (response.success) this.nextRow();
            //     },
            //     failure: function (f, a) {
            //         switch (a.failureType) {
            //             case Ext.form.Action.CLIENT_INVALID:
            //                 Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            //                 break;
            //             case Ext.form.Action.CONNECT_FAILURE:
            //                 Ext.Msg.alert('Failure', 'Connection failure has occured.');
            //                 break;
            //             case Ext.form.Action.SERVER_INVALID:
            //                 Ext.Msg.alert('Failure', a.result.msg);
            //         }
            //     }
            // });
        } else {
            this.nextRow();
        }
    },
    nextRow: function () {
        this.rowLoopCounter++;
        this.loopPost();
    },
    loopLog: function (msg) {
        this.writeLog('Baris ke- ' + (this.rowLoopCounter + 1) + ' dari ' + jun.dataStockOpname[jun.namasheet].length + ' --> ' + msg);
    },
    writeLog: function (msg) {
        this.log_msg.setValue(msg + '\n' + this.log_msg.getValue());
    }
});