jun.Belistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Belistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BeliStoreId',
            url: 'Beli',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'beli_id'},
                {name: 'price'},
                {name: 'tax'},
                {name: 'store'},
                {name: 'up'},
                {name: 'barang_id'},

            ]
        }, cfg));
    }
});
jun.rztBeli = new jun.Belistore();
//jun.rztBeli.load();
