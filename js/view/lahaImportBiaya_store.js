jun.LahaImportBiayastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.LahaImportBiayastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'LahaImportBiayaStoreId',
            url: 'LahaImportBiaya',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'laha_import_biaya_id'},
                {name: 'amount', type: 'float'},
                {name: 'laha_import_id'},
                {name: 'account_code'},
                {name: 'note_'}
            ]
        }, cfg));
    }
});
var renderLahaImportBiaya = new Ext.data.JsonReader({
    idProperty: 'laha_import_biaya_id',
    root: 'results',
    totalProperty: 'total',
    fields: [
        {name: 'laha_import_biaya_id'},
        {name: 'amount', type: 'float'},
        {name: 'laha_import_id'},
        {name: 'account_code'},
        {name: 'store'},
        {name: 'note_'}
    ]
});
jun.rztLahaImportBiaya = new Ext.data.GroupingStore({
    reader: renderLahaImportBiaya,
    url: 'LahaImportBiaya',
    // sortInfo: {field: 'kode_material', direction: 'ASC'},
    groupField: 'laha_import_biaya_id'
});
// jun.rztLahaImportBiaya = new jun.LahaImportBiayastore();
//jun.rztLahaImportBiaya.load();
