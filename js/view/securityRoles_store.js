jun.SecurityRolesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.SecurityRolesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'SecurityRolesStoreId',
            url: 'SecurityRoles',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'security_roles_id'},
                {name: 'role'},
                {name: 'ket'},
                {name: 'sections'},
                {name: '000'},
                {name: '001'},
                {name: '100'},
                {name: '101'},
                {name: '102'},
                {name: '103'},
                {name: '104'},
                {name: '105'},
                {name: '106'},
                {name: '107'},
                {name: '108'},
                {name: '109'},
                {name: '110'},
                {name: '111'},
                {name: '112'},
                {name: '113'},
                {name: '114'},
                {name: '115'},
                {name: '116'},
                {name: '117'},
                {name: '118'},
                {name: '119'},
                {name: '120'},
                {name: '121'},
                {name: '122'},
                {name: '123'},
                {name: '124'},
                {name: '125'},
                {name: '126'},
                {name: '127'},
                {name: '128'},
                {name: '129'},
                {name: '130'},
                {name: '131'},
                {name: '132'},
                {name: '133'},
                {name: '134'},
                {name: '135'},
                {name: '136'},
                {name: '137'}, //store area
                {name: '138'}, //Bonus name
                {name: '139'}, //store area
                {name: '141'}, //Tag
                {name: '142'}, //Tag Barang
                {name: '143'}, //Assign Employee
                {name: '144'}, //Racikan
                {name: '145'}, //Bonus Template
                {name: '148'}, //Bonus Template
                {name: '200'},
                {name: '201'},
                {name: '202'},
                {name: '203'},
                {name: '204'},
                {name: '205'}, //Purchase Item
                {name: '2051'}, //Purchase Item set LUNAS
                {name: '206'}, //Return Supplier Item
                {name: '207'},
                {name: '208'},
                {name: '209'},
                {name: '210'},
                {name: '211'},
                {name: '212'},
                {name: '213'},
                {name: '214'},
                {name: '215'},
                {name: '216'},
                {name: '217'},
                {name: '218'},
                {name: '219'},
                {name: '220'},
                {name: '221'},
                {name: '222'},
                {name: '223'},
                {name: '224'},
                {name: '225'},
                {name: '226'},
                {name: '227'},
                {name: '228'},
                {name: '229'},
                {name: '230'},
                {name: '231'},
                {name: '232'},
                {name: '240'},
                {name: '241'},
                {name: '242'},
                {name: '243'},
                {name: '244'},
                {name: '245'},
                {name: '246'},
                {name: '247'},
                {name: '248'},
                {name: '249'}, //Purchase Order
                {name: '250'}, //Penerimaan Barang
                {name: '251'}, //Dropping/Transfer Request
                {name: '252'}, //Dropping/Transfer
                {name: '253'}, //Dropping/Transfer Receive
                {name: '254'},
                {name: '255'},
                {name: '256'},
                {name: '257'}, //Dropping/Transfer Approval
                {name: '260'}, //Input Penerimaan Barang
                {name: '261'}, //Import Laha natasha
                {name: '262'}, //Retur Produksi
                {name: '263'}, //All Tipe Barang
                {name: '264'}, //Dropping/Transfer Recall
                {name: '300'},
                {name: '301'},
                {name: '301'},
                {name: '302'},
                {name: '303'},
                {name: '304'},
                {name: '305'},
                {name: '306'},
                {name: '307'},
                {name: '308'},
                {name: '309'},
                {name: '310'},
                {name: '311'},
                {name: '312'},
                {name: '313'},
                {name: '314'},
                {name: '315'},
                {name: '316'},
                {name: '317'}, //Customer Details
                {name: '318'},
                {name: '319'},
                {name: '320'},
                {name: '321'},
                {name: '322'},
                {name: '323'},
                {name: '324'},
                {name: '325'},
                {name: '326'},
                {name: '327'},
                {name: '328'},
                {name: '329'},
                {name: '330'},
                {name: '331'},
                {name: '332'},
                {name: '333'},
                {name: '334'},
                {name: '335'},
                {name: '336'},
                {name: '337'},
                {name: '338'},
                {name: '339'},
                {name: '340'},
                {name: '341'},
                {name: '342'},
                {name: '343'},
                {name: '344'}, //Rekap Order Dropping
                {name: '345'}, //Buku Hutang
                {name: '346'}, //Report Supplier Invoice
                {name: '347'}, //Report Bonus
                {name: '348'}, //Report Cash Flow
                {name: '349'}, //Cashier Turnover
                {name: '350'}, //Inventory Card Perlengkapan
                {name: '351'}, //Show Cost Price on Inventory Card
                {name: '352'}, //Report Produksi
                {name: '353'}, //Report Omset Per Periode
                {name: '356'},
                {name: '357'},
                {name: '358'},
                {name: '359'},
                {name: '400'},
                {name: '401'},
                {name: '402'},
                {name: '403'},
                {name: '404'},
                {name: '405'},
                {name: '406'},
                {name: '9996'},
                {name: '9997'},
                {name: '9998'},
                {name: '9999'},
                {name: '501'},/**restriction date input */
                {name: '503'}, //Upload History Manual
                {name: '504'}, //Asset Class
                {name: '505'}, //Asset
                {name: '506'}, //Asset Detail
                {name: '507'}, //Show Asset
                {name: '508'}, //Sync
                {name: '509'}, //Asset Categories
                {name: '600'},
                {name: '601'},
                {name: '602'},
                {name: '603'}, //Report Asset
                {name: '604'}, //HistoryNars
                {name: '605'}, //Asset Barang
                {name: '116'}, //Region/area

                {name: '700'},
                {name: '701'},
                {name: '702'},
                {name: '703'},
                {name: '704'},
                {name: '800'}, //Business Unit
                {name: '817'}, //Import

            ]
        }, cfg));
    }
});
jun.rztSecurityRoles = new jun.SecurityRolesstore();
//jun.rztSecurityRoles.load();
// jun.SettingClientsstore = Ext.extend(Ext.data.JsonStore, {
//     constructor: function (cfg) {
//         cfg = cfg || {};
//         jun.SettingClientsstore.superclass.constructor.call(this, Ext.apply({
//             storeId: 'SettingClientsStoreId',
//             // url: 'SettingClients',
//             // autoLoad: true,
//             // root: 'results',
//             // totalProperty: 'total',
//             fields: [
//                 {name: 'name_'},
//                 {name: 'value_'}
//             ]
//         }, cfg));
//     }
// });
jun.rztSettingClients = new Ext.data.JsonStore({
    root: 'data',
    fields: [
        {name: 'name_'},
        {name: 'value_'}
    ]
});
// jun.rztSettingClients = new jun.SettingClientsstore();