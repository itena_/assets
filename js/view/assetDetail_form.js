jun.AssetDetailWin = Ext.extend(Ext.Window, {
    title: 'Asset Detail',
    modez: 1,
    width: 945,
    height: 595,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    iswin: true,
    initComponent: function() {
       /* if (jun.rztStore.getTotalCount() === 0) {
            jun.rztStore.load();
        }*/
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-AssetDetail',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    /*{
                        xtype: "label",
                        text: "Docref Detail:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'docref',
                        id: 'docrefid',
                        ref: '../docref',
                        readOnly: true,
                        width: 175,
                        x: 85,
                        y: 2
                    },*/
                    {
                        xtype: "label",
                        text: "No. Activa:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'ati',
                        id: 'atiid',
                        ref: '../ati',
                        readOnly: true,
                        width: 175,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Docref Asset:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'docref_other',
                        id: 'docref_otherid',
                        ref: '../docref_other',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Asset Name:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'asset_trans_name',
                        id: 'asset_trans_nameid',
                        ref: '../asset_trans_name',
                        width: 175,
                        readOnly: true,
                        x: 85,
                        y: 62
                    },

                    {
                        xtype: "label",
                        text: "Asset Code:",
                        x: 290,
                        y: 5
                    },

                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'kode_barang',
                        id: 'kode_barangid',
                        ref: '../kode_barang',
                        width: 175,
                        readOnly: true,
                        x: 370,
                        y: 2,
                    },

                    {
                        xtype: "label",
                        text: "Price:",
                        x: 290,
                        y: 35
                    },

                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        name: 'asset_trans_price',
                        id: 'asset_trans_priceid',
                        ref: '../asset_trans_price',
                        width: 175,
                        readOnly: true,
                        x: 370,
                        y: 32
                    },

                    {
                        xtype: "label",
                        text: "Date:",
                        x: 290,
                        y: 65
                    },
                    {
                        xtype: "xdatefield",
                        ref: "../asset_trans_date",
                        name: "asset_trans_date",
                        id: "asset_trans_dateid",
                        readOnly: true,
                        allowBlank: true,
                        //value: DATE_NOW,
                        format: "d M Y",
                        width: 175,
                        x: 370,
                        y: 62,
                        //minValue: RES_DATE
                    },

                    /*{
                        xtype: "label",
                        text: "Branch:",
                        x: 615,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'asset_trans_branch',
                        id: 'asset_trans_branchid',
                        ref: '../asset_trans_branch',
                        value: STORE,
                        readOnly: true,
                        maxLength: 20,
                        width: 175,
                        x: 715,
                        y: 2
                    },*/

                    {
                        xtype: "label",
                        text: "Branch:",
                        x: 615,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'asset_trans_branch',
                        id: 'asset_trans_branchid',
                        ref: '../asset_trans_branch',
                        //value: STORE,
                        readOnly: true,
                        maxLength: 20,
                        width: 175,
                        x: 715,
                        y: 2
                    },

                    {
                        xtype: "label",
                        text: "Description:",
                        x: 615,
                        y: 35
                    },
                    {
                        xtype: 'textarea',
                        hideLabel: false,
                        name: 'description',
                        id: 'descriptionid',
                        ref: '../description',
                        //value: STORE,
                        //readOnly: true,
                        //maxLength: 20,
                        width: 175,
                        height: 50,
                        x: 715,
                        y: 32
                    },

                    new jun.AssetPeriodeGrid({
                        height: 405 - 30,
                        frameHeader: !1,
                        header: !1,
                        focusRow: 3,
                        x: 5,
                        y: 65 + 30
                    }),

                    /*{
                        xtype: "label",
                        text: "Total Depreciation:",
                        x: 615,
                        y: 482
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        ref: "../penyusutanpertahun",
                        name: "penyusutanpertahun",
                        value: 0,
                        readOnly: true,
                        width: 175,
                        x: 715,
                        y: 480
                    },*/
                    {
                        xtype: "label",
                        text: "Accumulation (Now):",
                        x: 590,
                        y: 482
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        ref: "../accumulationdep",
                        name: "accumulationdep",
                        id: "accumulationdep",
                        value: 0,
                        readOnly: true,
                        width: 175,
                        x: 715,
                        y: 480
                    },

                    {
                        xtype: "label",
                        text: "Nilai Buku (Now):",
                        x: 290,
                        y: 482
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        ref: "../nilaibukudep",
                        id: "nilaibukudep",
                        name: "nilaibukudep",
                        value: 0,
                        readOnly: true,
                        width: 175,
                        x: 390,
                        y: 480
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                /*{
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },*/
                {
                    xtype: 'button',
                    text: 'Close',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetDetailWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        //this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        //this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'asset/AssetDetail/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'asset/AssetDetail/create/';
                }
             
            Ext.getCmp('form-AssetDetail').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztAssetDetail.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-AssetDetail').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    /*onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },*/
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});

jun.AssetStatusWin = Ext.extend(Ext.Window, {
    title: 'Asset Status',
    modez:1,
    width: 400,
    height: 414,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        if (jun.rztBusinessunitCmp.getTotalCount() === 0) {
            jun.rztBusinessunitCmp.load();
        }
        /*if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }*/
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetStatus',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        hidden: true,
                        fieldLabel: 'Id',
                        name: 'asset_trans_id',
                        id: 'asset_trans_id',
                        ref: '../asset_trans_id',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'No. Activa',
                        name: 'ati',
                        id: 'atiid',
                        ref: '../ati',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'DocRef',
                        name: 'docref_other',
                        id: 'docref_otherid',
                        ref: '../docref_other',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'Asset Name',
                        name: 'asset_trans_name',
                        id: 'asset_trans_nameid',
                        ref: '../asset_trans_name',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        fieldLabel: 'Asset Price',
                        name: 'asset_trans_price',
                        id: 'asset_trans_priceid',
                        ref: '../asset_trans_price',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        hidden:false,
                        fieldLabel: 'Nilai Buku',
                        name: 'assetvalue',
                        id: 'assetvalue',
                        ref: '../assetvalue',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'

                    },
                    /*{
                        xtype: 'textfield',
                        fieldLabel: 'Asset',
                        hideLabel:false,
                        //hidden:true,
                        name:'nama_barang',
                        id:'nama_barangid',
                        ref:'../nama_barang',
                        maxLength: 500,
                        //allowBlank: 1,
                        anchor: '100%'
                    },*/
                    {
                        xtype: 'combo',
                        fieldLabel: 'Status',
                        name:'status',
                        id:'statusasset',
                        ref:'../statusasset',
                        hiddenName: 'statusasset',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['0', 'NON ACTIVE'],
                                ['1', 'ACTIVE'],
                                ['2', 'LEND'],
                                ['3', 'SELL'],
                                ['4', 'RENT'],
                                ['5', 'BROKEN'],
                                ['6', 'LOST']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        allowBlank: false,
                        emptyText: "Status",
                        mode : 'local',
                        anchor: "100%"
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        hidden:true,
                        fieldLabel: 'Amount',
                        name: 'amountstatus',
                        id: 'amountstatus',
                        ref: '../amountstatus',
                        width: 175,
                        readOnly: false,
                        anchor: '100%',
                        listeners: {
                            ppn : 'ppnstatuschange'
                        },

                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        hidden:true,
                        fieldLabel: 'PPN',
                        name: 'ppnstatus',
                        id: 'ppnstatus',
                        ref: '../ppnstatus',
                        width: 175,
                        readOnly: false,
                        anchor: '100%',
                        /*listeners: {
                            ppn : 'ppnstatuschange'
                            /!*ppnstatus: function () {
                                //field.setValue(newValue.toUpperCase());
                                this.ppnstatus.setValue(newValue);
                            }*!/
                        },*/
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: "To",
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztBusinessunitCmp,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{businessunit_code} - {businessunit_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'businessunit_id',
                        //name: 'store',
                        valueField: 'businessunit_id',
                        displayField: 'businessunit_name',
                        allowBlank: false,
                        hidden:true,
                        //readOnly: !HEADOFFICE,
                        ref: '../businessunit',
                        id:'businessunitstatus',
                        emptyText: "Business Unit",
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        hidden:true,
                        ref: '../branch',
                        id:'branchstatus',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "Branch",
                        allowBlank: false,
                        //value: STORE,
                        //emptyText: "Asset Name",
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Note',
                        hideLabel:false,
                        //hidden:true,
                        name:'ket',
                        id:'ketid',
                        ref:'../ket',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetStatusWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.businessunit.on('Select', this.businessunitonselect, this);
        this.statusasset.on('Select', this.statusassetonselect, this);
        this.amountstatus.on('change', this.ppnstatuschange, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    ppnstatuschange: function(c,r,i)
    {
        var amountstatus = this.amountstatus.getValue();
        var ppn = amountstatus * PPNASSET/100;
        this.ppnstatus.setValue(ppn);
    },

    statusassetonselect: function(c,r,i)
    {
        var status = Ext.getCmp('statusasset').getValue();

        if(status == '2' || status == '4')
        {
            this.businessunit.setVisible(true);
            this.branch.setVisible(true);
            this.amountstatus.setVisible(true);

            this.businessunit.allowBlank = false;
            this.branch.allowBlank = false;
            this.amountstatus.allowBlank = false;
        }
        else if(status == '3')
        {
            this.businessunit.setVisible(true);
            this.branch.setVisible(true);
            this.amountstatus.setVisible(true);
            this.ppnstatus.setVisible(true);

            this.businessunit.allowBlank = false;
            this.branch.allowBlank = false;
            this.amountstatus.allowBlank = false;
            this.ppnstatus.allowBlank = false;
        }
        else {
            this.businessunit.setVisible(false);
            this.ppnstatus.setVisible(false);
            this.amountstatus.setVisible(false);

            this.businessunit.allowBlank = true;
            this.branch.allowBlank = true;
            this.amountstatus.allowBlank = true;
            this.ppnstatus.allowBlank = true;
        }


    },

    businessunitonselect: function(c,r,i)
    {
        var tobu = Ext.getCmp('businessunitstatus').getRawValue();

        if(tobu == 'OTHER')
        {
            this.branch.setVisible(false);
            this.branch.allowBlank = true;
            this.branch.allowBlank = true;
        }
        else {
            var buid =r.get('businessunit_id');
            this.branch.store.reload({params:{businessunit_id:buid}});
            this.branch.setVisible(true);
            this.branch.allowBlank = false;
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm : function()
    {
        this.btnDisabled(true);
        var urlz= 'asset/AssetDetail/status/id/';


        Ext.getCmp('form-AssetStatus').getForm().submit({
            url:urlz,
            timeOut: 1000,
            method: 'POST',
            scope: this,

            success: function(f,a){
                jun.rztAssetDetail.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
                if(this.modez == 0){
                    Ext.getCmp('form-AssetStatus').getForm().reset();
                    this.btnDisabled(false);
                }
                if(this.closeForm){
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }

});

/*jun.AssetEditWin = Ext.extend(Ext.Window, {
    title: 'Edit Asset',
    modez:1,
    width: 400,
    height: 381,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        if (jun.rztBusinessunit.getTotalCount() === 0) {
            jun.rztBusinessunit.load();
        }
        /!*if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }*!/
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetEdit',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        hidden: true,
                        fieldLabel: 'Id',
                        name: 'asset_trans_id',
                        id: 'asset_trans_id',
                        ref: '../asset_trans_id',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'No. Activa',
                        name: 'ati',
                        id: 'atiid',
                        ref: '../ati',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'DocRef',
                        name: 'docref_other',
                        id: 'docref_otherid',
                        ref: '../docref_other',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        fieldLabel: 'Asset Name',
                        name: 'asset_trans_name',
                        id: 'asset_trans_nameid',
                        ref: '../asset_trans_name',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        fieldLabel: 'Asset Price',
                        name: 'asset_trans_price',
                        id: 'asset_trans_priceid',
                        ref: '../asset_trans_price',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        hidden:false,
                        fieldLabel: 'Depreciation Price',
                        name: 'assetvalue',
                        id: 'assetvalue',
                        ref: '../assetvalue',
                        width: 175,
                        readOnly: true,
                        anchor: '100%'

                    },
                    /!*{
                        xtype: 'textfield',
                        fieldLabel: 'Asset',
                        hideLabel:false,
                        //hidden:true,
                        name:'nama_barang',
                        id:'nama_barangid',
                        ref:'../nama_barang',
                        maxLength: 500,
                        //allowBlank: 1,
                        anchor: '100%'
                    },*!/
                    {
                        xtype: 'combo',
                        fieldLabel: 'Status',
                        name:'status',
                        id:'statusasset',
                        ref:'../statusasset',
                        hiddenName: 'statusasset',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['0', 'NON ACTIVE'],
                                ['1', 'ACTIVE'],
                                ['2', 'LEND'],
                                ['3', 'SELL'],
                                ['4', 'RENT'],
                                ['5', 'BROKEN'],
                                ['6', 'LOST'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        allowBlank: false,
                        emptyText: "Status",
                        mode : 'local',
                        anchor: "100%",
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        hidden:true,
                        fieldLabel: 'Amount',
                        name: 'amountstatus',
                        id: 'amountstatus',
                        ref: '../amountstatus',
                        width: 175,
                        readOnly: false,
                        anchor: '100%'

                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: "To",
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztBusinessunitCmp,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{businessunit_code} - {businessunit_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'businessunit_id',
                        //name: 'store',
                        valueField: 'businessunit_id',
                        displayField: 'businessunit_name',
                        allowBlank: false,
                        hidden:true,
                        //readOnly: !HEADOFFICE,
                        ref: '../businessunit',
                        id:'businessunitstatus',
                        emptyText: "Business Unit",
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        hidden:true,
                        ref: '../branch',
                        id:'branchstatus',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "Branch",
                        allowBlank: false,
                        //value: STORE,
                        //emptyText: "Asset Name",
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Descripton',
                        hideLabel:false,
                        //hidden:true,
                        name:'ket',
                        id:'ketid',
                        ref:'../ket',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetEditWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.businessunit.on('Select', this.businessunitonselect, this);
        this.statusasset.on('Select', this.statusassetonselect, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    statusassetonselect: function(c,r,i)
    {
        var status = Ext.getCmp('statusasset').getValue();
        if(status == '2' || status == '4')
        {
            this.businessunit.setVisible(true);
            this.branch.setVisible(true);
            this.amountstatus.setVisible(false);

            this.businessunit.allowBlank = false;
            this.branch.allowBlank = false;
            this.amountstatus.allowBlank = false;
        }
        else if(status == '3')
        {
            this.businessunit.setVisible(true);
            this.branch.setVisible(true);
            this.amountstatus.setVisible(true);

            this.businessunit.allowBlank = false;
            this.branch.allowBlank = false;
            this.amountstatus.allowBlank = false;
        }
        else {
            this.businessunit.setVisible(false);
            this.branch.setVisible(false);
            this.amountstatus.setVisible(false);

            this.businessunit.allowBlank = true;
            this.branch.allowBlank = true;
            this.amountstatus.allowBlank = true;
        }
    },

    businessunitonselect: function(c,r,i)
    {
        var buid =r.get('businessunit_id');
        this.branch.store.reload({params:{businessunit_id:buid}});
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm : function()
    {
        this.btnDisabled(true);
        var urlz= 'asset/AssetDetail/status/id/';


        Ext.getCmp('form-AssetEdit').getForm().submit({
            url:urlz,
            timeOut: 1000,
            method: 'POST',
            scope: this,

            success: function(f,a){
                jun.rztAssetDetail.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
                if(this.modez == 0){
                    Ext.getCmp('form-AssetStatus').getForm().reset();
                    this.btnDisabled(false);
                }
                if(this.closeForm){
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }

});*/

jun.AssetEditDetailWin = Ext.extend(Ext.Window, {
    title: 'Asset Edit',
    modez:1,
    width: 523,
    height: 290,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        if (jun.rztAssetGroup.getTotalCount() === 0) {
            jun.rztAssetGroup.load();
        }
        if (jun.rztAssetBarang.getTotalCount() === 0) {
            jun.rztAssetBarang.load();
        }
        /*if (jun.rztBarangAsst.getTotalCount() === 0) {
            jun.rztBarangAsst.load();
        }*/
        if (jun.rztStoreTrans.getTotalCount() === 0) {
            jun.rztStoreTrans.load();
        }
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }

        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-Asset',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
/*                    {
                        xtype: 'textfield',
                        fieldLabel: 'Doc Ref',
                        hideLabel:false,
                        //hidden:true,
                        name:'doc_ref',
                        id:'doc_refid',
                        ref:'../doc_ref',
                        maxLength: 100,
                        //allowBlank: ,
                        readOnly:true,
                        anchor: '100%'
                    },*/
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Activa',
                        hideLabel:false,
                        //hidden:true,
                        name:'ati',
                        id:'atiid',
                        ref:'../ati',
                        maxLength: 100,
                        //allowBlank: ,
                        readOnly:true,
                        anchor: '100%'
                    },
                    /*{
                        xtype: 'textfield',
                        fieldLabel: 'Activa Old',
                        hideLabel:false,
                        //hidden:true,
                        name:'ati_old',
                        id:'ati_oldid',
                        ref:'../ati_old',
                        maxLength: 100,
                        //allowBlank: ,
                        readOnly:true,
                        anchor: '100%'
                    },*/
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Price',
                        hideLabel:false,
                        name:'asset_trans_price',
                        id:'asset_trans_priceid',
                        ref:'../asset_trans_price',
                        maxLength: 100,
                        allowBlank: false,
                        readOnly: false,
                        anchor: '100%',
                        listeners: {
                            ppn : 'ppnasseteditchange'
                            /*ppnstatus: function () {
                                //field.setValue(newValue.toUpperCase());
                                this.ppnstatus.setValue(newValue);
                            }*/
                        }
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'PPN',
                        hideLabel:false,
                        //hidden:true,
                        name:'ppnassetedit',
                        id:'ppnasseteditid',
                        ref:'../ppnassetedit',
                        maxLength: 30,
                        hidden:false,
                        allowBlank: false,
                        readOnly: false,
                        anchor: '100%'
                    },
                    /*{
                        xtype: 'textfield',
                        fieldLabel: 'No. Activa',
                        hideLabel:false,
                        //hidden:true,
                        name:'ati',
                        id:'atiid',
                        ref:'../ati',
                        maxLength: 100,
                        //allowBlank: ,
                        readOnly:false,
                        emptyText: "No Activa",
                        anchor: '100%'
                    },*/
                    /*{
                        xtype: "radiogroup",
                        fieldLabel: "Asset Source",
                        id: "showsource",
                        ref: '../showsource',
                        defaults: {xtype: "radio", name: "showsource"},
                        items: [
                            {
                                boxLabel: "Barang Perlengkapan",
                                inputValue: "P",
                                checked: true
                            },
                            {
                                boxLabel: "Barang Asset",
                                inputValue: "A"
                            }
                        ]
                    },*/
                    /*{
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Asset Item',
                        ref: '../barang_id',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztBarangAsst,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kode_barang} </br> {nama_barang} </br> {ket} </span></h3>',
                            "</div></tpl>"),
                        listWidth: 450,
                        lastQuery: "",
                        pageSize: 20,
                        forceSelection: true,
                        autoSelect: false,
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        hiddenName: 'barang_id',
                        name: 'barang_id',
                        valueField: 'barang_id',
                        displayField: 'kode_barang',
                        emptyText: "Barang Perlengkapan",

                        //value: STORE,
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },*/
                    /*{
                        xtype: 'combo',
                        hidden:false,
                        typeAhead: true,
                        fieldLabel: 'Asset Name',
                        ref: '../asset_trans_name',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetBarang,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kode_barang} </br> {nama_barang} </br> {ket} </span></h3>',
                            "</div></tpl>"),
                        listWidth: 450,
                        lastQuery: "",
                        pageSize: 20,
                        forceSelection: true,
                        autoSelect: false,
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        hiddenName: 'asset_trans_name',
                        name: 'asset_barang_id',
                        valueField: 'barang_id',
                        displayField: 'nama_barang',
                        emptyText: "Barang Asset",

                        //value: STORE,
                        //readOnly: !HEADOFFICE,
                        readOnly: true,
                        anchor: '100%'
                    },*/
                    /*{
                        xtype: 'textfield',
                        fieldLabel: 'Asset Name',
                        hideLabel:false,
                        //hidden:true,
                        name:'asset_name',
                        id:'asset_nameid',
                        ref:'../asset_name',
                        maxLength: 150,
                        //allowBlank: ,
                        anchor: '100%'
                    },*/
                    /*{
                        xtype: 'numericfield',
                        fieldLabel: 'Qty',
                        hideLabel:false,
                        //hidden:true,
                        name:'qty',
                        id:'qtyid',
                        ref:'../qty',
                        maxLength: 10,
                        emptyText: "Qty",
                        //allowBlank: ,
                        anchor: '100%'
                    },*/

                    /*{
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../asset_trans_branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreTrans,
                        hiddenName: 'asset_trans_branch',
                        name: 'asset_trans_branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        //readOnly: !HEADOFFICE,
                        readOnly: true,
                        anchor: '100%'
                    },*/
                    /*{
                        xtype: 'xdatefield',
                        ref:'../asset_trans_date',
                        fieldLabel: 'Date Acquisition',
                        name:'asset_trans_date',
                        id:'asset_trans_dateid',
                        format: 'd M Y',
                        value: DATE_NOW,
                        readOnly: true,
                        //allowBlank: ,
                        anchor: '100%'
                    },*/
                   /* {
                        xtype: 'numericfield',
                        fieldLabel: 'Price Acquisition',
                        hideLabel:false,
                        //hidden:true,
                        name:'asset_trans_price',
                        id:'asset_trans_priceid',
                        ref:'../asset_trans_price',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%',
                        listeners: {
                            ppn : 'ppnassetchange'
                            /!*ppnstatus: function () {
                                //field.setValue(newValue.toUpperCase());
                                this.ppnstatus.setValue(newValue);
                            }*!/
                        },
                        readOnly: true,
                    },

                    {
                        xtype: 'numericfield',
                        fieldLabel: 'New Price Acquisition',
                        hideLabel:false,
                        //hidden:true,
                        name:'new_price_acquisition',
                        id:'new_price_acquisitionid',
                        ref:'../new_price_acquisition',
                        maxLength: 30,
                        hidden:true,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Class',
                        store: jun.rztAssetGroup,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{golongan} - {desc} - period ({period})</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'class',
                        valueField: 'asset_group_id',
                        emptyText: "All Class",
                        displayField: 'golongan',
                        readOnly: true,
                        anchor: '100%'
                    },*/
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Category',
                        ref: '../category',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztAssetCategory,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{category_code} - {category_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName: 'category_id',
                        name: 'category_id',
                        valueField: 'category_id',
                        displayField: 'category_name',
                        emptyText: "All Category",
                        allowBlank: false,
                        anchor: '100%'
                    },

                    {
                        xtype: 'textarea',
                        fieldLabel: 'Description',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style : {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'description',
                        id: 'descriptionid',
                        ref: '../description',
                        maxLength: 255,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Active',
                        name:'active',
                        id:'activeasset',
                        ref:'../activeasset',
                        hiddenName: 'activeasset',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['0', 'NON ACTIVE'],
                                ['1', 'ACTIVE']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        allowBlank: false,
                        emptyText: "",
                        mode : 'local',
                        anchor: "100%"
                    },

                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.asset_trans_price.on('change', this.ppnasseteditchange, this);
        //this.showsource.on('change', this.actionshowsource, this);

        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    ppnasseteditchange: function(c,r,i)
    {
        var price = this.asset_trans_price.getValue();
        var ppn = price * PPNASSET/100;
        this.ppnassetedit.setValue(ppn);
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    actionshowsource:function(a,r){

        var apa = r.inputValue == "P";


        this.asset_barang_id.setVisible(!apa);
        this.barang_id.setVisible(apa);
    },


    saveForm : function()
    {
        this.btnDisabled(true);
        /*var urlz;
        if(this.modez == 1 || this.modez== 2) {

                urlz= 'asset/Asset/update/id/' + this.id;

            } else {

                urlz= 'asset/Asset/create/';
            }
         */
        var urlz = 'asset/AssetDetail/create/';
        Ext.getCmp('form-Asset').getForm().submit({
            url:urlz,
            params: {
                id: this.id,
                mode: 1
            },
            timeOut: 1000,
            scope: this,
            success: function(f,a){
                //jun.rztAsset.reload();
                jun.rztAssetDetail.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
                if(this.modez == 0){
                    Ext.getCmp('form-Asset').getForm().reset();
                    this.btnDisabled(false);
                }
                if(this.closeForm){
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }

});