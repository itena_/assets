jun.ReportNewCustomers = Ext.extend(Ext.Window, {
    title: "New Customers",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportNewCustomers",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportNewCustomers.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportNewCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportNewCustomers").getForm().url = "Report/NewCustomers";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportNewCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/NewCustomers";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportNewCustomers").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportNewCustomers").getForm().url = url;
            var form = Ext.getCmp('form-ReportNewCustomers').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportNewCustomers").getForm().getValues());
        }
    }
});
jun.ReportRealCustomers = Ext.extend(Ext.Window, {
    title: "Real Customers",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRealCustomers",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRealCustomers.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportRealCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRealCustomers").getForm().url = "Report/RealCustomers";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRealCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/RealCustomers";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportRealCustomers").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportRealCustomers").getForm().url = url;
            var form = Ext.getCmp('form-ReportRealCustomers').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportRealCustomers").getForm().getValues());
        }
    }
});
jun.ReportBirthDayCustomers = Ext.extend(Ext.Window, {
    title: "BirthDay Customers",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBirthDayCustomers",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportBirthDayCustomers.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBirthDayCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBirthDayCustomers").getForm().url = "Report/BirthDayCustomers";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBirthDayCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/BirthDayCustomers";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportBirthDayCustomers").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportBirthDayCustomers").getForm().url = url;
            var form = Ext.getCmp('form-ReportBirthDayCustomers').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportBirthDayCustomers").getForm().getValues());
        }
    }
});
jun.ReportCategoryCustomers = Ext.extend(Ext.Window, {
    title: "Category Patient",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportCategoryCustomers",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Category',
                        ref: '../kategori',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: new Ext.data.ArrayStore({
                            fields: ["id", "name"],
                            data: [
                                [1, "Most Active"],
                                [2, "very Active"],
                                [3, "Active Patient"],
                                [4, "Passive Patient"]
                            ]
                        }),
                        hiddenName: 'kategori',
                        name: 'kategori',
                        valueField: 'id',
                        displayField: 'name',
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportCategoryCustomers.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportCategoryCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportCategoryCustomers").getForm().url = "Report/CategoryCustomers";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportCategoryCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/CategoryCustomers";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportCategoryCustomers").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportCategoryCustomers").getForm().url = url;
            var form = Ext.getCmp('form-ReportCategoryCustomers').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportCategoryCustomers").getForm().getValues());
        }
    }
});
jun.ReportBiaya = Ext.extend(Ext.Window, {
    title: "Cash Out",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBiaya",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportBiaya.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBiaya").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBiaya").getForm().url = "Report/CashOut";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBiaya').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/CashOut";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportBiaya").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportBiaya").getForm().url = url;
            var form = Ext.getCmp('form-ReportBiaya').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportBiaya").getForm().getValues());
        }
    }
});
jun.ReportInventoryMovements = Ext.extend(Ext.Window, {
    title: "Inventory Movements",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 225,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztTag.getTotalCount() === 0) {
            jun.rztTag.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInventoryMovements",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Tag',
                        allowBlank: true,
                        emptyText: 'ALL Tag',
                        store: jun.rztTag,
                        hiddenName: 'tag_id',
                        valueField: 'tag_id',
                        displayField: 'tag_name',
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryMovements.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryMovements").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryMovements").getForm().url = "Report/InventoryMovements";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryMovements').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/InventoryMovements";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportInventoryMovements").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInventoryMovements").getForm().url = url;
            var form = Ext.getCmp('form-ReportInventoryMovements').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportInventoryMovements").getForm().getValues());
        }
    }
});
jun.ReportInventoryMovementsPerlengkapan = Ext.extend(Ext.Window, {
    title: "Inventory Movements Perlengkapan",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 225,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztTag.getTotalCount() === 0) {
            jun.rztTag.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-InventoryMovementsPerlengkapan",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Tag',
                        allowBlank: true,
                        emptyText: 'ALL Tag',
                        store: jun.rztTag,
                        hiddenName: 'tag_id',
                        valueField: 'tag_id',
                        displayField: 'tag_name',
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryMovements.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-InventoryMovementsPerlengkapan").getForm().standardSubmit = !0;
        Ext.getCmp("form-InventoryMovementsPerlengkapan").getForm().url = "Report/InventoryMovementsPerlengkapan";
        this.format.setValue("html");
        var form = Ext.getCmp('form-InventoryMovementsPerlengkapan').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/InventoryMovementsPerlengkapan";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-InventoryMovementsPerlengkapan").getForm().standardSubmit = !0;
            Ext.getCmp("form-InventoryMovementsPerlengkapan").getForm().url = url;
            var form = Ext.getCmp('form-InventoryMovementsPerlengkapan').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-InventoryMovementsPerlengkapan").getForm().getValues());
        }
    }
});
jun.ReportRekapPerawatan = Ext.extend(Ext.Window, {
    title: "Rekap Penjualan",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 215,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekapPerawatan",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekapPerawatan.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportRekapPerawatan").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapPerawatan").getForm().url = "Report/RekapPerawatan";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRekapPerawatan').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/RekapPerawatan";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportRekapPerawatan").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportRekapPerawatan").getForm().url = url;
            var form = Ext.getCmp('form-ReportRekapPerawatan').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportRekapPerawatan").getForm().getValues());
        }
    }
});
jun.ReportInventoryMovementsClinical = Ext.extend(Ext.Window, {
    title: "Inventory Movements Clinical",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 205,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztKategoriClinicalCmp.getTotalCount() === 0) {
            jun.rztKategoriClinicalCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInventoryMovementsClinical",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        emptyText: 'ALL Clinical Category',
                        fieldLabel: 'Clinical Category',
                        store: jun.rztKategoriClinicalCmp,
                        hiddenName: 'kategori_clinical_id',
                        valueField: 'kategori_clinical_id',
                        displayField: 'nama_kategori',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryMovementsClinical.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryMovementsClinical").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryMovementsClinical").getForm().url = "Report/InventoryMovementsClinical";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryMovementsClinical').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/InventoryMovementsClinical";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportInventoryMovementsClinical").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInventoryMovementsClinical").getForm().url = url;
            var form = Ext.getCmp('form-ReportInventoryMovementsClinical').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportInventoryMovementsClinical").getForm().getValues());
        }
    }
});
jun.ReportInventoryCard = Ext.extend(Ext.Window, {
    title: "Inventory Card",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztBarangNonJasa.getTotalCount() === 0) {
            jun.rztBarangNonJasa.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInventoryCard",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    // {
                    //     xtype: 'combo',
                    //     typeAhead: true,
                    //     fieldLabel: 'Item',
                    //     triggerAction: 'all',
                    //     lazyRender: true,
                    //     mode: 'local',
                    //     forceSelection: true,
                    //     store: jun.rztBarangNonJasaAll,
                    //     hiddenName: 'barang_id',
                    //     valueField: 'barang_id',
                    //     ref: '../barang',
                    //     displayField: 'kode_barang',
                    //     anchor: '100%'
                    // },
                    {
                        fieldLabel: 'Item',
                        xtype: 'mfcombobox',
                        style: 'margin-bottom:2px',
                        searchFields: [
                            'kode_barang',
                            'nama_barang'
                        ],
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<span style="font-weight:bold;">{kode_barang}</span><br><span>{nama_barang}</span>',
                                "</div></tpl>"),
                        forceSelection: true,
                        store: jun.rztBarangNonJasaAll,
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        ref: '../barang',
                        displayField: 'kode_barang',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        forceSelection: true,
                        allowBlank: false,
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryCard.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryCard").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryCard").getForm().url = "Report/InventoryCard";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryCard').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/InventoryCard";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportInventoryCard").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInventoryCard").getForm().url = url;
            var form = Ext.getCmp('form-ReportInventoryCard').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportInventoryCard").getForm().getValues());
        }
    }
});
jun.ReportInventoryCardPerlengkapan = Ext.extend(Ext.Window, {
    title: "Inventory Card Perlengkapan",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztBarangNonJasa.getTotalCount() === 0) {
            jun.rztBarangNonJasa.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInventoryCard",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    // {
                    //     xtype: 'combo',
                    //     typeAhead: true,
                    //     fieldLabel: 'Item',
                    //     triggerAction: 'all',
                    //     lazyRender: true,
                    //     mode: 'local',
                    //     forceSelection: true,
                    //     store: jun.rztBarangNonJasaAll,
                    //     hiddenName: 'barang_id',
                    //     valueField: 'barang_id',
                    //     ref: '../barang',
                    //     displayField: 'kode_barang',
                    //     anchor: '100%'
                    // },
                    {
                        fieldLabel: 'Item',
                        xtype: 'mfcombobox',
                        style: 'margin-bottom:2px',
                        searchFields: [
                            'kode_barang',
                            'nama_barang'
                        ],
                        typeAhead: false,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<span style="font-weight:bold;">{kode_barang}</span><br><span>{nama_barang}</span>',
                                "</div></tpl>"),
                        forceSelection: true,
                        store: jun.rztBarangNonJasaAll,
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        ref: '../barang',
                        displayField: 'kode_barang',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        forceSelection: true,
                        allowBlank: false,
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInventoryCard.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInventoryCard").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInventoryCard").getForm().url = "Report/InventoryCardPerlengkapan";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInventoryCard').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/InventoryCardPerlengkapan";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportInventoryCard").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInventoryCard").getForm().url = url;
            var form = Ext.getCmp('form-ReportInventoryCard').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportInventoryCard").getForm().getValues());
        }
    }
});
jun.ReportBeautySummary = Ext.extend(Ext.Window, {
    title: "Beautician Services Summary",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBeautySummary",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportBeautySummary.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBeautySummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBeautySummary").getForm().url = "Report/BeautySummary";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBeautySummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.Ajax.request({
            url: 'BeautyServices/CheckFinal',
            method: 'POST',
            scope: this,
            params: {
                tglfrom: this.tglfrom.getValue(),
                tglto: this.tglto.getValue(),
                store: this.store.getValue()
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success == false) {
                    Ext.MessageBox.show({
                        title: 'Fatal Error',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return;
                }
                var url = "Report/BeautySummary";
                this.format.setValue("excel");
                if (!is_enable_tools()) {
                    Ext.getCmp("form-ReportBeautySummary").getForm().standardSubmit = !0;
                    Ext.getCmp("form-ReportBeautySummary").getForm().url = url;
                    var form = Ext.getCmp('form-ReportBeautySummary').getForm();
                    var el = form.getEl().dom;
                    var target = document.createAttribute("target");
                    target.nodeValue = "myFrame";
                    el.setAttributeNode(target);
                    el.action = form.url;
                    el.submit();
                } else {
                    DownloadOpen(url, 'POST',
                            '.xls', Ext.getCmp("form-ReportBeautySummary").getForm().getValues());
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReportBeautyDetails = Ext.extend(Ext.Window, {
    title: "Beautician Services Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBeautyDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportBeautyDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBeautyDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBeautyDetails").getForm().url = "Report/BeautyDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBeautyDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.Ajax.request({
            url: 'BeautyServices/CheckFinal',
            method: 'POST',
            scope: this,
            params: {
                tglfrom: this.tglfrom.getValue(),
                tglto: this.tglto.getValue(),
                store: this.store.getValue()
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success == false) {
                    Ext.MessageBox.show({
                        title: 'Fatal Error',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return;
                }
                var url = "Report/BeautyDetails";
                this.format.setValue("excel");
                if (!is_enable_tools()) {
                    Ext.getCmp("form-ReportBeautyDetails").getForm().standardSubmit = !0;
                    Ext.getCmp("form-ReportBeautyDetails").getForm().url = url;
                    var form = Ext.getCmp('form-ReportBeautyDetails').getForm();
                    var el = form.getEl().dom;
                    var target = document.createAttribute("target");
                    target.nodeValue = "myFrame";
                    el.setAttributeNode(target);
                    el.action = form.url;
                    el.submit();
                } else {
                    DownloadOpen(url, 'POST',
                            '.xls', Ext.getCmp("form-ReportBeautyDetails").getForm().getValues());
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});

jun.ReportTransaksi = Ext.extend(Ext.Window, {
    title: "Report Transaksi",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportTransaksi",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportTransaksi.superclass.initComponent.call(this);
        // this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportTransaksi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTransaksi").getForm().url = "Report/ReportTransaksi";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportTransaksi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
}
);

jun.ReportJasaBeauty = Ext.extend(Ext.Window, {
    title: "Report Jasa Beauty, KMR, dan KMT",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportJasaBeauty",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                // {
                //     xtype: "button",
                //     iconCls: "silk13-page_white_excel",
                //     text: "Save to Excel",
                //     ref: "../btnSave"
                // },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportJasaBeauty.superclass.initComponent.call(this);
        // this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportJasaBeauty").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportJasaBeauty").getForm().url = "Report/ReportJasaBeauty";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportJasaBeauty').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.Ajax.request({
            url: 'BeautyServices/CheckFinal',
            method: 'POST',
            scope: this,
            params: {
                tglfrom: this.tglfrom.getValue(),
                tglto: this.tglto.getValue(),
                store: this.store.getValue()
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success == false) {
                    Ext.MessageBox.show({
                        title: 'Fatal Error',
                        msg: response.msg,
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.ERROR
                    });
                    return;
                }
                var url = "Report/BeautyDetails";
                this.format.setValue("excel");
                if (!is_enable_tools()) {
                    Ext.getCmp("form-ReportBeautyDetails").getForm().standardSubmit = !0;
                    Ext.getCmp("form-ReportBeautyDetails").getForm().url = url;
                    var form = Ext.getCmp('form-ReportBeautyDetails').getForm();
                    var el = form.getEl().dom;
                    var target = document.createAttribute("target");
                    target.nodeValue = "myFrame";
                    el.setAttributeNode(target);
                    el.action = form.url;
                    el.submit();
                } else {
                    DownloadOpen(url, 'POST',
                            '.xls', Ext.getCmp("form-ReportBeautyDetails").getForm().getValues());
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});

jun.ReportSalesSummary = Ext.extend(Ext.Window, {
    title: "Sales Summary",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesSummary",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesSummary.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesSummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesSummary").getForm().url = "Report/SalesSummary";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesSummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/SalesSummary";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportSalesSummary").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesSummary").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesSummary').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            //target.nodeValue = "_blank";
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen('Report/SalesSummary', 'POST',
                    '.xls', Ext.getCmp("form-ReportSalesSummary").getForm().getValues());
        }
    }
});
jun.ReportSalesDetails = Ext.extend(Ext.Window, {
    title: "Sales Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 225,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: 'Real Sales',
                        boxLabel: "",
                        value: 0,
                        inputValue: 1,
                        uncheckedValue: 0,
                        name: "real"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesDetails").getForm().url = "Report/SalesDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/SalesDetails";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportSalesDetails").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesDetails").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesDetails').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportSalesDetails").getForm().getValues());
        }
    }
});
jun.ReportSalesItemDetails = Ext.extend(Ext.Window, {
    title: "Sales Item Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesItemDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        fieldLabel: 'Product/Treatment',
                        mode: 'local',
                        //forceSelection: true,
                        store: jun.rztBarangCmp,
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        ref: '../barang',
                        displayField: 'kode_barang',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesItemDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesItemDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesItemDetails").getForm().url = "Report/SalesItemDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesItemDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/SalesItemDetails";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportSalesItemDetails").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesItemDetails").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesItemDetails').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportSalesItemDetails").getForm().getValues());
        }
    }
});
jun.ReportReturSalesSummary = Ext.extend(Ext.Window, {
    title: "Return Sales Summary",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportReturSalesSummary",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportReturSalesSummary.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportReturSalesSummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportReturSalesSummary").getForm().url = "Report/ReturSalesSummary";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportReturSalesSummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/ReturSalesSummary";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportReturSalesSummary").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportReturSalesSummary").getForm().url = url;
            var form = Ext.getCmp('form-ReportReturSalesSummary').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportReturSalesSummary").getForm().getValues());
        }
    }
});
jun.ReportReturSalesDetails = Ext.extend(Ext.Window, {
    title: "Return Sales Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportReturSalesDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportReturSalesDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportReturSalesDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportReturSalesDetails").getForm().url = "Report/ReturSalesDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportReturSalesDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/ReturSalesDetails";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportReturSalesDetails").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportReturSalesDetails").getForm().url = url;
            var form = Ext.getCmp('form-ReportReturSalesDetails').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportReturSalesDetails").getForm().getValues());
        }
    }
});
jun.ReportLaha = Ext.extend(Ext.Window, {
    title: "Daily Report",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportLaha",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        forceSelection: true,
                        allowBlank: false,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
//                {
//                    xtype: "button",
//                    iconCls: "silk13-html",
//                    text: "Show HTML",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportLaha.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportLaha").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLaha").getForm().url = "Report/Laha";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportLaha').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportLaha").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLaha").getForm().url = "Report/Laha";
        this.format.setValue("openOffice");
        var form = Ext.getCmp('form-ReportLaha').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportTender = Ext.extend(Ext.Window, {
    title: "Tenders",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 145,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportTender",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        forceSelection: true,
                        allowBlank: false,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_excel",
//                    text: "Save to Excel",
//                    ref: "../btnSave"
//                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-html",
//                    text: "Show HTML",
//                    ref: "../btnPdf"
//                },
                {
                    xtype: "button",
                    iconCls: "silk13-printer",
                    text: "Print",
                    ref: "../btnPrint"
                }
            ]
        };
        jun.ReportTender.superclass.initComponent.call(this);
//        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnPrint.on("click", this.onbtnPrintclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportTender").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTender").getForm().url = "Report/tenders";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportTender').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/tenders";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportTender").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportTender").getForm().url = url;
            var form = Ext.getCmp('form-ReportTender').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportTender").getForm().getValues());
        }
    },
    onbtnPrintclick: function () {
        Ext.Ajax.request({
            url: 'Tender/print',
            method: 'POST',
            scope: this,
            params: {
                tglfrom: this.tglfrom.hiddenField.dom.value,
                store: this.store.getValue()
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findDefaultPrinter();
                if (notReady()) {
                    return;
                }
//                qz.append(chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r");
//                qz.print();
//                qz.appendImage(getPath() + "images/img_receipt.png", "ESCP");
//                while (!qz.isDoneAppending()) {}
//                qz.append("\x1B\x40"); // 1
                //qz.append("\x1B\x21\x08"); // 2
//                qz.append("\x1B\x21\x01"); // 3
//                qz.append(response.msg);
//                qz.append("\x1D\x56\x41"); // 4
//                qz.append("\x1B\x40"); // 5
//                qz.print();
//                qz.appendHTML('<html><pre>' + response.msg + '</pre></html>');
//                qz.printHTML();
//                 printHTML('default', response.msg);
//                 print(PRINTER_RECEIPT, __openCashDrawer);
                var msg = [{type: 'raw', data: response.msg}];
                var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                print('default', printData);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReportSalesSummaryReceipt = Ext.extend(Ext.Window, {
    title: "Sales Summary Receipt",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesSummaryReceipt",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesSummaryReceipt.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().url = "Report/SalesSummaryReceipt";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesSummaryReceipt').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/SalesSummaryReceipt";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesSummaryReceipt').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportSalesSummaryReceipt").getForm().getValues());
        }
    }
});
jun.ReportSalesSummaryReceiptDetails = Ext.extend(Ext.Window, {
    title: "Sales Summary Receipt Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesSummaryReceiptDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }                   
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesSummaryReceiptDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().url = "Report/SalesSummaryReceiptDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesSummaryReceiptDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/SalesSummaryReceiptDetails";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesSummaryReceiptDetails').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportSalesSummaryReceiptDetails").getForm().getValues());
        }
    }
});
jun.ReportSalesNReturnDetails = Ext.extend(Ext.Window, {
    title: "Sales and Return Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesNReturnDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Print",
                    ref: "../btnPrint"
                }
            ]
        };
        jun.ReportSalesNReturnDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesNReturnDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesNReturnDetails").getForm().url = "Report/SalesnReturDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesNReturnDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/SalesnReturDetails";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportSalesNReturnDetails").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesNReturnDetails").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesNReturnDetails').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportSalesNReturnDetails").getForm().getValues());
        }
    }
});
jun.ReportDokterSummary = Ext.extend(Ext.Window, {
    title: "Doctor Summary",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportDokterSummary",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportDokterSummary.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportDokterSummary").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportDokterSummary").getForm().url = "Report/DokterSummary";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportDokterSummary').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/DokterSummary";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportDokterSummary").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportDokterSummary").getForm().url = url;
            var form = Ext.getCmp('form-ReportDokterSummary').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportDokterSummary").getForm().getValues());
        }
    }
});
jun.ReportDokterDetails = Ext.extend(Ext.Window, {
    title: "Doctor Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportDokterDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportDokterDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportDokterDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportDokterDetails").getForm().url = "Report/DokterDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportDokterDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/DokterDetails";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportDokterDetails").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportDokterDetails").getForm().url = url;
            var form = Ext.getCmp('form-ReportDokterDetails').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportDokterDetails").getForm().getValues());
        }
    }
});
jun.ReportGeneralLedger = Ext.extend(Ext.Window, {
    title: "General Ledger",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportGeneralLedger",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        fieldLabel: 'COA',
                        store: jun.rztChartMasterLib,
                        hiddenName: 'account_code',
                        valueField: 'account_code',
                        forceSelection: true,
                        matchFieldWidth: !1,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                                "</div></tpl>"),
                        listWidth: 500,
                        displayField: 'account_code',
                        ref: '../cmbkode',
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportGeneralLedger.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportGeneralLedger").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralLedger").getForm().url = "Report/GeneralLedger";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportGeneralLedger').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/GeneralLedger";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportGeneralLedger").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportGeneralLedger").getForm().url = url;
            var form = Ext.getCmp('form-ReportGeneralLedger').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportGeneralLedger").getForm().getValues());
        }
    }
});
jun.ReportGeneralJournal = Ext.extend(Ext.Window, {
    title: "General Journal",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportGeneralJournal",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportGeneralJournal.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportGeneralJournal").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportGeneralJournal").getForm().url = "Report/GeneralJournal";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportGeneralJournal').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/GeneralJournal";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportGeneralJournal").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportGeneralJournal").getForm().url = url;
            var form = Ext.getCmp('form-ReportGeneralJournal').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportGeneralJournal").getForm().getValues());
        }
    }
});
jun.ReportLabaRugi = Ext.extend(Ext.Window, {
    title: "Profit and Loss Report",
    iconCls: "silk13-report",
    modez: 1,
    width: 305,
    height: 390,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportLabaRugi",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: 'button',
                        text: 'Check All',
                        hidden: false,
                        ref: '../btnAll'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    new jun.StoreReportGrid({
                        height: 240,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    }),
                    {
                        xtype: "hidden",
                        name: "store",
                        ref: "../store"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportLabaRugi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnAll.on('click', this.onbtnCheckAll, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnCheckAll: function (b, e)
    {
        if (b.getText() == 'Check All') {
            this.griddetils.store.checkAll();
            b.setText('Uncheck All');
        } else if (b.getText() == 'Uncheck All') {
            this.griddetils.store.uncheckAll();
            b.setText('Check All');
        }

    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportLabaRugi").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportLabaRugi").getForm().url = "Report/LabaRugi";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportLabaRugi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/LabaRugiKonsolidasi";
        jun.StoreReport.commitChanges();
        this.format.setValue("excel");
        this.store.setValue(Ext.encode(Ext.pluck(
                jun.StoreReport.data.items, "data")));
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportLabaRugi").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportLabaRugi").getForm().url = url;
            var form = Ext.getCmp('form-ReportLabaRugi').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "_blank";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportLabaRugi").getForm().getValues());
        }
    }
});
jun.ReportBalanceSheet = Ext.extend(Ext.Window, {
    title: "Balance Sheet Report",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBalanceSheet",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'Sampai Tanggal',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportBalanceSheet.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBalanceSheet").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBalanceSheet").getForm().url = "Report/BalanceSheet";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportBalanceSheet').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/BalanceSheet";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportBalanceSheet").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportBalanceSheet").getForm().url = url;
            var form = Ext.getCmp('form-ReportBalanceSheet').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "_blank";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportBalanceSheet").getForm().getValues());
        }
    }
});
jun.ReportPayments = Ext.extend(Ext.Window, {
    title: "Payments Report",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportPayments",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Payment Method',
                        store: jun.rztBankCmp,
                        ref: '../bank',
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportPayments.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportPayments").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPayments").getForm().url = "Report/Payments";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportPayments').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/Payments";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportPayments").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportPayments").getForm().url = url;
            var form = Ext.getCmp('form-ReportPayments').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportPayments").getForm().getValues());
        }
    }
});
jun.GenerateLabaRugi = Ext.extend(Ext.Window, {
    title: "Generate Profit Lost",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 170,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-GenerateLabaRugi",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    new jun.comboBulan({
                        hiddenName: 'month',
                        fieldLabel: 'Month',
                        displayField: "namaBulan",
                        valueField: "noBulan",
                        anchor: '100%'
                    }),
                    {
                        xtype: "spinnerfield",
                        fieldLabel: "Year",
                        name: "year",
                        ref: "../year",
                        maxLength: 4,
                        minValue: 2012,
                        maxValue: 9999,
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        forceSelection: true,
                        emptyText: "All Branch",
                        allowBlank: true,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
            ]
        };
        jun.GenerateLabaRugi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        var dt = new Date();
        this.year.setValue(dt.format('Y'));
    },
    onbtnSaveclick: function () {
        Ext.getCmp('form-GenerateLabaRugi').getForm().submit({
            url: 'Report/GenerateLabaRugi',
            scope: this,
            timeout: 300000,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReportNeraca = Ext.extend(Ext.Window, {
    title: "Neraca Report",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 120,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportNeraca",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to_date",
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportNeraca.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportNeraca").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportNeraca").getForm().url = "Report/Neraca";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportNeraca').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportNeraca").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportNeraca").getForm().url = "Report/Neraca";
        this.format.setValue("openOffice");
        var form = Ext.getCmp('form-ReportNeraca').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

jun.ReportSalesPrice = Ext.extend(Ext.Window, {
    title: "Sales Price",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 120,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesPrice",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        forceSelection: true,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportSalesPrice.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesPrice").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesPrice").getForm().url = "Report/DaftarHargaJual";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportSalesPrice').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/DaftarHargaJual";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportSalesPrice").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesPrice").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesPrice').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit()
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportSalesPrice").getForm().getValues());
        }
    }
});
jun.ReportPurchasePrice = Ext.extend(Ext.Window, {
    title: "Purchase Price",
    iconCls: "silk13-report",
    modez: 1,
    width: 325,
    height: 125,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportPurchasePrice",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        forceSelection: true,
                        allowBlank: false,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ], this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
//                {
//                    xtype: "button",
//                    iconCls: "silk13-page_white_acrobat",
//                    text: "Save to PDF",
//                    ref: "../btnPdf"
//                }
            ]
        };
        jun.ReportPurchasePrice.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
//        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportPurchasePrice").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPurchasePrice").getForm().url = "Report/DaftarHargaBeli";
        this.format.setValue("pdf");
        var form = Ext.getCmp('form-ReportPurchasePrice').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ReportPurchasePrice").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPurchasePrice").getForm().url = "Report/DaftarHargaBeli";
        this.format.setValue("openOffice");
        var form = Ext.getCmp('form-ReportPurchasePrice').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ClosingStore = Ext.extend(Ext.Window, {
    title: "Yearly Closing",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 155,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ClosingStore",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "spinnerfield",
                        fieldLabel: "Year",
                        name: "year",
                        ref: "../year",
                        maxLength: 4,
                        minValue: 2012,
                        maxValue: 9999,
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        forceSelection: true,
                        allowBlank: false,
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    //iconCls: "silk13-page_white_excel",
                    text: "CLOSING",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ClosingStore.superclass.initComponent.call(this);
        this.btnSave.on("click", this.deleteRec, this);
        var dt = new Date();
        this.year.setValue(dt.format('Y'));
    },
    deleteRec: function () {
        Ext.MessageBox.confirm("Confirmation", "Are you sure want to closing yearly? <br>" +
                "This will clear all transaction and can't undo. Please backup first!", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function (a) {
        if (a == "no")
            return;
        Ext.getCmp('form-ClosingStore').getForm().submit({
            url: 'site/Closing',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReportRekeningKoran = Ext.extend(Ext.Window, {
    title: "Rekening Koran",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekeningKoran",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Bank',
                        store: jun.rztBankTransCmpPusat,
                        ref: '../bank',
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekeningKoran.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportRekeningKoran").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekeningKoran").getForm().url = "Report/RekeningKoran";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRekeningKoran').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/RekeningKoran";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportRekeningKoran").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportRekeningKoran").getForm().url = url;
            var form = Ext.getCmp('form-ReportRekeningKoran').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportRekeningKoran").getForm().getValues());
        }
    }
});
jun.R_ReportHutang = Ext.extend(Ext.Window, {
    title: "Report Hutang",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        // if (jun.rztSupplierCmp.getTotalCount() === 0) {
        //     jun.rztSupplierCmp.load();
        // }
        // if (jun.rztStoreCmp.getTotalCount() === 0) {
        //     jun.rztStoreCmp.load();
        // }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-R_ReportHutang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                // {
                //     xtype: "button",
                //     iconCls: "silk13-page_white_excel",
                //     text: "Save to Excel",
                //     ref: "../btnSave"
                // },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.R_ReportHutang.superclass.initComponent.call(this);
        // this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-R_ReportHutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-R_ReportHutang").getForm().url = "Report/R_ReportHutang";
        this.format.setValue("html");
        var form = Ext.getCmp('form-R_ReportHutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
    // onbtnSaveclick: function () {
    //     var url = "Report/KartuHutang";
    //     this.format.setValue("excel");
    //     if (!is_enable_tools()) {
    //         Ext.getCmp("form-R_ReportHutang").getForm().standardSubmit = !0;
    //         Ext.getCmp("form-R_ReportHutang").getForm().url = url;
    //         var form = Ext.getCmp('form-R_ReportHutang').getForm();
    //         var el = form.getEl().dom;
    //         var target = document.createAttribute("target");
    //         target.nodeValue = "myFrame";
    //         el.setAttributeNode(target);
    //         el.action = form.url;
    //         el.submit();
    //     } else {
    //         DownloadOpen(url, 'POST',
    //             '.xls', Ext.getCmp("form-R_ReportHutang").getForm().getValues());
    //     }
    // }
});

jun.R_ReportPiutang = Ext.extend(Ext.Window, {
    title: "Report Piutang",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        // if (jun.rztSupplierCmp.getTotalCount() === 0) {
        //     jun.rztSupplierCmp.load();
        // }
        // if (jun.rztStoreCmp.getTotalCount() === 0) {
        //     jun.rztStoreCmp.load();
        // }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-R_ReportPiutang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                // {
                //     xtype: "button",
                //     iconCls: "silk13-page_white_excel",
                //     text: "Save to Excel",
                //     ref: "../btnSave"
                // },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.R_ReportPiutang.superclass.initComponent.call(this);
        // this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-R_ReportPiutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-R_ReportPiutang").getForm().url = "Report/R_ReportPiutang";
        this.format.setValue("html");
        var form = Ext.getCmp('form-R_ReportPiutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
    // onbtnSaveclick: function () {
    //     var url = "Report/KartuHutang";
    //     this.format.setValue("excel");
    //     if (!is_enable_tools()) {
    //         Ext.getCmp("form-R_ReportHutang").getForm().standardSubmit = !0;
    //         Ext.getCmp("form-R_ReportHutang").getForm().url = url;
    //         var form = Ext.getCmp('form-R_ReportHutang').getForm();
    //         var el = form.getEl().dom;
    //         var target = document.createAttribute("target");
    //         target.nodeValue = "myFrame";
    //         el.setAttributeNode(target);
    //         el.action = form.url;
    //         el.submit();
    //     } else {
    //         DownloadOpen(url, 'POST',
    //             '.xls', Ext.getCmp("form-R_ReportHutang").getForm().getValues());
    //     }
    // }
});
jun.ReportKartuPiutang = Ext.extend(Ext.Window, {
    title: "Kartu Piutang",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {

        jun.rztCustomersPiutangCmp.baseParams = {
            customer_id: POSCUSTOMERDEFAULT
        };
        jun.rztCustomersPiutangCmp.load();
        jun.rztCustomersPiutangCmp.baseParams = {};

        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportKartuPiutang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        ref: '../customer',
                        xtype: 'combo',
                        fieldLabel: 'Customer',
//                        //typeAhead: true,
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztCustomersPiutangCmp,
                        id: "customer_id",
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<h3><span>{telp} <br /> {tgl_lahir:date("M j, Y")}</span>{no_customer} | {nama_customer}</h3>',
                                '{alamat}',
                                "</div></tpl>"),
                        allowBlank: false,
                        listWidth: 350,
                        width: 175
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportKartuHutang.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportKartuPiutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportKartuPiutang").getForm().url = "Report/KartuPiutang";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportKartuPiutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/KartuPiutang";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportKartuPiutang").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportKartuPiutang").getForm().url = url;
            var form = Ext.getCmp('form-ReportKartuPiutang').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportKartuPiutang").getForm().getValues());
        }
    }
});
jun.ReportKartuHutang = Ext.extend(Ext.Window, {
    title: "Debt Card",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportKartuHutang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Supplier',
                        typeAhead: true,
                        triggerAction: 'all',
                        id: 'supplierid',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        ref: '../supplier',
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        matchFieldWidth: !1,
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportKartuHutang.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportKartuHutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportKartuHutang").getForm().url = "Report/KartuHutang";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportKartuHutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/KartuHutang";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportKartuHutang").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportKartuHutang").getForm().url = url;
            var form = Ext.getCmp('form-ReportKartuHutang').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportKartuHutang").getForm().getValues());
        }
    }
});
jun.ReportBukuPiutang = Ext.extend(Ext.Window, {
    title: "Buku Piutang",
    iconCls: "silk13-report",
    modez: 1,
    width: 1,
    height: 1,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBukuPiutang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        value: 'excel',
                        ref: "../format"
                    }
                ]
            }
        ];
        jun.ReportBukuPiutang.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
    },
    onActivate: function () {
        Ext.getCmp("form-ReportBukuPiutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBukuPiutang").getForm().url = "Report/BukuPiutang";
        var form = Ext.getCmp('form-ReportBukuPiutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
        this.close();
    }
});
jun.ReportBukuHutang = Ext.extend(Ext.Window, {
    title: "Buku Hutang",
    iconCls: "silk13-report",
    modez: 1,
    width: 1,
    height: 1,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBukuHutang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        value: 'excel',
                        ref: "../format"
                    }
                ]
            }
        ];
        jun.ReportBukuHutang.superclass.initComponent.call(this);
        this.on('activate', this.onActivate, this);
    },
    onActivate: function () {
        Ext.getCmp("form-ReportBukuHutang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBukuHutang").getForm().url = "Report/BukuHutang";
        var form = Ext.getCmp('form-ReportBukuHutang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
        this.close();
    }
});
jun.CustAttstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.CustAttstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CustAttstoreId',
            url: 'Report/CustAtt',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tgl'},
                {name: 'visits'}
            ]
        }, cfg));
    }
});
jun.rztCustAttstore = new jun.CustAttstore();
jun.ReportChartCustAtt = Ext.extend(Ext.Panel, {
    title: 'Customers Attandence',
    ////renderTo: 'container',
    //width:500,
    //height:300,
    layout: 'fit',
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Branch :'
                },
                {
                    xtype: 'combo',
                    typeAhead: true,
                    fieldLabel: 'Branch',
                    ref: '../store',
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    store: jun.rztStoreCmp,
                    hiddenName: 'store',
                    name: 'store',
                    valueField: 'store_kode',
                    displayField: 'store_kode',
                    forceSelection: true,
                    allowBlank: false,
                    value: STORE,
                    readOnly: !HEADOFFICE,
                    width: 100
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'From :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglfromchartattid',
                    ref: '../fromtgl'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'To :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tgltochartattid',
                    ref: '../totgl'
                },
                {
                    xtype: 'button',
                    text: 'Refresh',
                    ref: '../btnRefresh'
                }
            ]
        };
        this.items = {
            xtype: 'linechart',
            store: jun.rztCustAttstore,
            xField: 'tgl',
            yField: 'visits',
            background: '#FDDFFE',
            yAxis: new Ext.chart.NumericAxis({
                title: 'Visits',
                displayName: 'Visits',
                labelRenderer: Ext.util.Format.numberRenderer('0,0'),
                label: {
                    display: 'inside', // or 'rotate'
                    field: 'visits',
                    'text-anchor': 'start'
                }
            }),
            xAxis: new Ext.chart.CategoryAxis({
                title: 'Date'
            }),
            extraStyle: {
                font: {
                    color: '#8B1D51'
                },
                background: {
                    color: '#FDDFFE',
                    alpha: .9
                },
                xAxis: {
                    color: '#8B1D51',
                    labelRotation: -90
                },
                yAxis: {
                    color: '#8B1D51',
                    titleRotation: -90
                }
            },
            listeners: {
                itemclick: function (o) {
                    var rec = store.getAt(o.index);
                    Ext.example.msg('Item Selected', 'You chose {0}.', rec.get('name'));
                }
            }
        }
        jun.ReportChartCustAtt.superclass.initComponent.call(this);
        this.btnRefresh.on('Click', this.loadRecord, this);
    },
    loadRecord: function () {
        var fromtgl = this.fromtgl.getValue();
        var totgl = this.totgl.getValue();
        var store = this.store.getValue();
        if (fromtgl == '' || fromtgl == undefined) {
            Ext.example.msg('From Date', 'From Date must selected');
            return;
        }
        if (totgl == '' || totgl == undefined) {
            Ext.example.msg('To Date', 'To Date must selected');
            return;
        }
        jun.rztCustAttstore.baseParams = {
            from: fromtgl,
            to: totgl,
            store: store
        };
        jun.rztCustAttstore.load();
        jun.rztCustAttstore.baseParams = {};
    }
});
jun.ChartNewCustomers = Ext.extend(Ext.Window, {
    title: "Chart New Customers",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ChartNewCustomers",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-chart_line",
                    text: "Show Chart",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ChartNewCustomers.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ChartNewCustomers").getForm().standardSubmit = !0;
        Ext.getCmp("form-ChartNewCustomers").getForm().url = "Chart/NewCust";
        var form = Ext.getCmp('form-ChartNewCustomers').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ChartCustAtt = Ext.extend(Ext.Window, {
    title: "Chart Customers Attendance",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ChartCustAtt",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-chart_line",
                    text: "Show Chart",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ChartCustAtt.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ChartCustAtt").getForm().standardSubmit = !0;
        Ext.getCmp("form-ChartCustAtt").getForm().url = "Chart/custatt";
        var form = Ext.getCmp('form-ChartCustAtt').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ChartSalesGrup = Ext.extend(Ext.Window, {
    title: "Chart Sales Group",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ChartSalesGrup",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-chart_pie",
                    text: "Show Chart",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ChartSalesGrup.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ChartSalesGrup").getForm().standardSubmit = !0;
        Ext.getCmp("form-ChartSalesGrup").getForm().url = "Chart/SalesGrup";
        var form = Ext.getCmp('form-ChartSalesGrup').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ChartTopCust = Ext.extend(Ext.Window, {
    title: "Chart Top Customers",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ChartTopCust",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'Limit',
                        xtype: 'numericfield',
                        name: "limit",
                        ref: '../limit',
                        value: 10,
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ChartTopCust.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ChartTopCust").getForm().standardSubmit = !0;
        Ext.getCmp("form-ChartTopCust").getForm().url = "Chart/TopCust";
        var form = Ext.getCmp('form-ChartTopCust').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ChartTopSalesGrup = Ext.extend(Ext.Window, {
    title: "Chart Top Sales Group",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 230,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ChartTopSalesGrup",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        allowBlank: true,
                        emptyText: 'ALL Group',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "from",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "to",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'Limit',
                        xtype: 'numericfield',
                        name: "limit",
                        ref: '../limit',
                        value: 10,
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ChartTopSalesGrup.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-ChartTopSalesGrup").getForm().standardSubmit = !0;
        Ext.getCmp("form-ChartTopSalesGrup").getForm().url = "Chart/TopSalesGrup";
        var form = Ext.getCmp('form-ChartTopSalesGrup').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.InputStockOpname = Ext.extend(Ext.Window, {
    title: "Input Stock Opname",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 380,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-InputStockOpname",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl",
                        ref: '../tgl',
                        value: new Date(),
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "Select a branch",
                        anchor: '100%'
                    },
//                    {
//                        xtype: 'fileuploadfield',
//                        fieldLabel: 'File',
//                        emptyText: 'Select file Stock Opname',
//                        fieldLabel: 'File Stock Opname',
//                        ref: '../file',
//                        name: 'file',
//                        buttonText: 'Browse',
//                        anchor: '100%'
//                    },
                    {
                        html: '<input type="file" name="xlfile" id="inputFile" />',
                        name: 'file',
                        xtype: "panel",
                        listeners: {
                            render: function (c) {
                                new Ext.ToolTip({
                                    target: c.getEl(),
                                    html: 'Format file : Excel (.xls)<br>Only first sheet will be saved<br>Two acceptable column headers are <b>kode_barang</b> and <b>qty</b>'
                                });
                            },
                            afterrender: function () {
                                itemFile = document.getElementById("inputFile");
                                itemFile.addEventListener('change', readFileExcel, false);
                            }
                        }
                    },
                    {
                        xtype: "panel",
                        bodyStyle: "background-color: #E4E4E4;padding: 5px",
                        html: 'Format file : Excel (.xls)<br>Pastikan hanya ada 1 sheet didalam file (nama sheet apa aja boleh)' +
                                '<br>kolom header : <b>kode_barang</b> dan <b>qty</b><br>' +
                                '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATcAAACsCAIAAACRlnxRAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABCVSURBVHhe7Z09ryRHFYYvvwWHrITtbH+EZSfsan/AFYlliWCT1Q0ILYO0EqG1uwQOFgnIuAiJay0Cpw5xsPhLQogAi5zEnFOfpz66u3pmumbq9PuodG91VXVNd5/zTM90z9x79Z/v/ouCgnLJ5eozAMBlc/W/baCpXQ1sxg4P8t522e4vLB0YWKoeWDo8sFQ9sDTh9bNHb7/96NlrtzgEvQ+yOUYpN3eurxNnyKtkr8+zv7DUQqF4dHPzaCxNz2FpcoRMAndN3N67fHcjn7vPtb+w1EDBoFhwSHo/WR7D2S01LYotLROiPATbAksjVtJaVC6as1tKDT1Tlui6y92fg0pgacBLehFxWcE5LM3RbGnMi7MBSx2Jmbxw7sg0cw5L04PT/cUHLD0l41haOUGMoun5Le2eyF13OXn+Pg+w1FBEopKKlwos3ZiKpp1fPcBSppZlnQNxOGe3tJLFG9N7lzkX4i7z/p7jWWnnltZPBaNoeg5LU7pmLHOGvEr2undawNLh2eFB3tsuw9LhgaXqgaXDA0vV4yylXwCAS2bDc+n3YGO++OILV9sNe9tlu7+wdGBgqXpg6fDAUvUcYGn97mIVYent9dXV1f2nr91iO6+f3qc1r2/dYjsHP+JgwFL1rLWUb/W338aGpR1oS9nba0WHApZOYj99wX/LAJZeEg0pq+1QwNJJXt/dsZss61GWWu18zph2i0yj0Hz9NLHUrmxYEtc94tMwUxgvHjNvvX99HbZubtjstG6SpQ08EUnKhm2Ix00csqu33nuLfvoNO/gJ8NzA0iWOtNSmkcsM38h1mTKi3aWdaRdDTFVqXWLXtGPqa8qHTMc3DhPbKet2SFhjY0TKTm1D3m5b0/0aCVi6xDGWWlxaJxnDhEWRVDKXkqwSFk0wMYtFbI5vTMY7FoaJWWcfbVNiyk5ug+wwdW7uupGnBZYucbyllRw3+FxK2+OSrSXM5ViSs3HJzRKrfo50fNOw2JOOSZc2JqTs9DYku2YWrm+7buOJgaVLHGVpmhwuX0w/ERaTpIorrMyr+ixyknTCiUedGyZ6JtfenJiyk9uQdLil+x038dTA0iVOdPWoSO2JrDJV1y6GNHhQX9PWzNxygB/vU7ltmNwK0Z6tsTEiZae2wWyn37XQ120LTw4sXeJoSyt1S0wjn//E9DVeObyGfRR7uVUOj1PYTt8ht4poGWbH+G0Lu5Jt88YkKTtx3GKzWXZb2mkDTw8sPSXC0l1hpIjCb8tEyqbPICnW0mElhaUnZUeWurOVOFf1kmC9paNLCktPSg9Lw4u5lP45mGxIrxMpsc5S/7J8YEd3aynptBH0AACA48Er3oGh+LnabtjbLtv9haUDA0vVA0uHB5aqZ42lfKfUsv5+qbmWkVxS4RbZ4C9rGOavbZSTBWa6tDJsylLEZaTs5S4mtMYmJiYFLJ1C/Kn35r/6nlma2MctPhymN/ZliznUzR80qLk406WWMVPWPinHQPGyXRKJQY3VNIClEyR/R6X1j6qklprP8ISjHoMhwhLInmcltoujWkRwpksvw6Usx5uU5GwIMa7GmwbWkwCWtnCgpUYgf9yDm1xZIZWfouLiTJdixrP09pZTICSAq5cR43iaLwUkZ10Cli7DL3jb3pkWlkaTKi1NxNFFZGe6NDNqyoYEIEzobqnB4EInBnBVRBSWLmAuIR129cgecW9QaEktpSWPaPX4lcNCHDPTpRslloZ412PHI0J4YekcqxQlapaaOlVDC1fi06TD9/JvB40RSx4xqWsJ+C7laLE0eYqtJgQsXWatokTdUnv6jBcP0j5Lra0MXxw106UdDZYm4StiyXDKhHDC0gnWfK00MGWpe4mTBEkGxiwXkiWBcrgn4Zku9WiwVAY3xE0EkKsiwLC0Dl8xSmm5YTptqRVRtlgzHclQR4yjxITy59Nd+jXVYalMgBA0kRNJIGHpKYmWgs3YW8oSsPSUwNIOwFL1wNLhgaXqcZaSThtBDwAAOB6cSweG4udqu2Fvu2z3F5YODCxVDywdHliqnjWWxnumh32ON72nmd8u47ubgfptTnH7zOMHznQxDZMPy3gpG4MVEyANYAhRbSgsnYQ/e+Q/yMC6nuNb4HIVQxxY77ItrGjsm5p8WEZLWfFhk7RaBiVGLo0vLG3hwO+XHvst8HIgt5gZJ+bgzjDGUzSMzWApm0Q2LFBMynhXk4CBpcuYj90fcC6lIy6Oe/CqXZqwiifGtuhinKbul1ZGTtmQD1zJv/A9nRiwdBYj6FHfLw1xKVsW4VVy3KpVS0OY2VOHPl3HTVkOS5YMLsg+auVXww2wdJkjzqWuxkc8tKSWCqFEq0XE0sGjTfjKLsI/VCBMnrQOzqApy8EpQ8b4oJpouRFpfGFpCwe+L3VHmepUDS1cKazxvfzbYf9DcQyVgSIZJsm6YmeGmVOPpyOmrAlBLTYMd3pLQ5h8owGWtnCcpVafA78FnjXOWeqiXLrKg2Hp2eDDXwY3ImMKS5lmS8Xdl/YvhE9Zag2KoTJxE96Y5TKQ+SQidNUu12ceS8zu7NXCYClbRIoRIeGq76e6q6Yxg6WTuCtHTNObUmLaUmuRbDENniKMhmSIIYwruxIRGyYflrFSlnVLsXESEaoHTrbC0lMSLQWbsbeUJWDpKYGlHYCl6oGlwwNL1eMsJZ02gh4AAHA8OJcODMXP1XbD3nbZ7i8sHRhYqh5YOjywVD3rLW3/TIO01Nz3Km6YyobkRpq8ORZJ7npawkCzenEntHxYfQybsvEzC0yMf2xMAx6TApYuwJ9BOtTSxD5pqemNfdliQK5iEANtkDMhTX/eqI0xUzaLFy/aQMrgU2slDWDpAvwB3keHnku3/Ba4WYHmT7o5yNe1iVQxXMpy0EjQ+EFuKaldsO00sB47WDoDOXpzd/grXjriQr6gXDRtibCKR4TRzHybhJUlvRWPqJTxLC3/F7jQVDRz7PC/wIkVlpKebOdRlqZPlFnLIrxKjl/VzkIjwlzUQnFvn31URk1ZoaOBIpVEVA4wkY9P5bB0AufoMVeP7BHnGh/v0JJ65GNFiFaLCJuDR9vguVloiBtBDdyTzq4RFZZyIGPghJCepBWW1jEvdk3tWEtNnaqhhStFVHwv/3bMfgs8VGiM+U3LZtIwQC0aLOV6yIFESE8yApZWMVd2Mxq+vla31Kpz2m+BxwoNogot2piGdrWoszRZ8CRxhKVLHH8uZeigEzJIMjBmubQrn0TGM0aRGvmaQ9GuFQ2WygVODRO8ULFVEUZYusRpLDUNSYtp8CRDA8kQQxwnbDTDvPOw9FLJU4JNzIIqAu7jaYClpyRaCjZjbylLwNJTAks7AEvVA0uHB5aqx1lKOm0EPQAA4HhwLh0Yip+r7Ya97bLdX1g6MLBUPbB0eGCpelZYyjdKIwf8N6f0xuXk7TImuTsWELfPPH5gpUv5bdKAkpSN8ZeBi3GVrbB0kvhR3mYySxP7pKWmN/ZliwG5iiEOnOnSjoaU5Wj5YLGurs5VG9U0vrB0CjqVtn7kKJBauuW3wCtziGirRl3KhthXk4CBpVO4P9NgOPgTguK4B6+CaYsUKlKDWy66ZmKsDGUpy5GMz7z1xIClE4iP75o3qAe8LzXOBHfKlkVMADPcqmE2T9GgFj0p6+Lrw2YSA/8LnGi2NIG/yNaiacVSU+MjHlpSS2nJU0hWmsejw/NuRv1pWB/KUtZE0sTOpIKLdxp6WNoCn00PtdTUqRpauFIY5Xv5t2P9/wLfB+pS1sc0PAUzSZbA0hb4PWrLW9O6pTYOp/0WeHX8PlBrKccUljLNlooXuVxtu340Zal9noyvabk3xsAtl9blk4jQFV37QUPKitOmjKT3NRlAwNJJxKcaWu+bTltqGpIW0+CpC5cMMYRxxfz7QUfKitiKp2vRLFth6SmJloLN2FvKErD0lMDSDsBS9cDS4YGl6nGWkk4bQQ8AADieq79/99kWhbBPBmA7KH6uthv2tst2f2HpwMBS9cDS4YGl6lll6csPn129+dEPqLz36cuit1JgaQdgqXpWWPrJ767efPbBHdW//OC9j+59+GU+oCywtAOwVD3tlv7y/TYzZYGlHYCl6mm2lM+f73ySNS4VWNoBWKqeZks/f4de7n7y6T37vvT9z4sBtQJLOwBL1bPG0nDRCO9LLwlYqp5VloZXvHyxt+V0Cks7AEvV02xp8r4Ull4QsFQ9zZaaOzHuFW9yXp0rsLQDsFQ9KyyVn2rA1aPLAZaqZ5Wlqwss7QAsVQ8sHR5Yqh5YOjywVD3OUvZpG+gBAADHg7+oMjAUP1fbDXvbZbu/sHRgYKl6YOnwwFL1NFvKf9A+peFPZ8PSDsBS9TRbKhH/JXEeWNoBWKqeAyxtdhSWdgGWqme9pa3/u5SBpR2ApepZa+mKEykBSzsAS9Wz0tJ1ksLSHsBS9ay0tPXfCztgaQdgqXrWWbrmPSkDSzsAS9WzylJ+vbtGUljaA1iqnlWWrj2VwtIewFL1rLJ0NbC0A7BUPbB0eGCpemDp8MBS9ThLSaeNoAcAABwPzqUDQ/Fztd2wt122+wtLBwaWqgeWDg8sVc8aS+MXwVs/JQhLOwBL1dNuafxIA38Eqe3TDbC0A7BUPc2WJp87ooWm0yks7QAsVU+zpdm5tO01LyztACxVT7ulhH9j2vzlNVjaAViqnnZLWVGnZ/Lqdw5Y2gFYqh5n6e2f/qK72L3dIdlxQBm3XP3zX/9WXGgPXc7uD9r37GigDFpgqVpgqZoCS9UCS9UUWKoWWKqm5Ja+/O0fFltmyvMXv85aqqVxWCgvf//HrKWxwNITlp/+7BdUskaUDmXBUlpst5Tca7d0lahkablhcnGq6LH06xfvPnjxlVtoApaqKXOWWkUbfbDirbK0cTAVa2m2baE+U5osJQHu/fiNJ6/c4vff3z358RtTSrTbUo40D/RDU959/o1rOXi2JWCpmjJpqbXCltA7VYJ1jeKtHR8sDRvTslVUWi19cPP4wcOPvw6LDyeVWG+L59Xje/4hqG6fFGApSkOpWxqUsEUOKItUrtG6tatIS+32LG6VLc2Wvrh7/tCe3+hE+vi5UUKK4erffPwgngy/ev7Q1t/w+sWWbHWGLL25c3VLMhs/hJstDKNV3IDHv/GzvboJDzdPaelPfvUjKvP1rFgzp0o2GGWjgnNpMNBYZOvyZzJm4pxG5tC5kbq8YHdP/FRm0UL+s4TipbWYjYV8bHpIdTOGHbYtjB0pHmIRWKqm4H1pVMVaxGIEJYJmoZ41+nOdeR/LpsUzoRwpeXUTXZ2ctpCcG+VpdpnS0iMLzDxXmbPULjb6QGWtpVnjTLGWJi0bWOr0CC3SEzkmDpZvZYNR3tWkMcOct+l3Oa2tVy01L8uTU/EssFRNWbC02jJT2i3NWuYLWZq1NJZ1lgZsi7XFNPCr0KBTJljo/frFY3vlll+sPvz4b4VmQbCwbnyItle85oTvrg8vAUvVlNxSZeUoS8XVoHef3IQx9oUxqWIropdNcy32alAybbxWJC//hNmMsbbXPTWEFirx6pF5lBZRYamaAkvVcnJLUc5VYKlaYKmaAkvVAkvVFP2W2puBOyywVE2BpWoLLFVTrv786V9RUFAuuVz948tvUVBQLrnAUhSUSy+wFAXl0gssRUG57PLlt/8HQHElZRMHMoAAAAAASUVORK5CYII=">'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Check input",
                    ref: "../btnCheck"
                },
                {
                    xtype: "button",
                    text: "Generate Query",
                    ref: "../btnQuery"
                },
                {
                    xtype: "button",
                    text: "Save",
                    ref: "../btnSave"
                }
            ]
        };
        jun.InputStockOpname.superclass.initComponent.call(this);
        this.btnCheck.on("click", this.onbtnCheckclick, this);
        this.btnQuery.on("click", this.onbtnQueryclick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnCheckclick: function () {
        if (jun.dataStockOpname == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih file stockopname.");
            return;
        }
        jun.stockOpnameStore.loadData(jun.dataStockOpname[jun.namasheet]);
        var win = new jun.CheckInputStockOpnameWin({generateQuery: 0});
        win.show(this);
    },
    onbtnQueryclick: function () {
        console.log(jun.namasheet);
        if (jun.dataStockOpname == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih file stockopname.");
            return;
        }
        jun.stockOpnameStore.loadData(jun.dataStockOpname[jun.namasheet]);
        var win = new jun.QueryStockOpnameWin({
            tgl: this.tgl.getValue(),
            store: this.store.getValue()
        });
        win.show(this);
    },
    onbtnSaveclick: function () { //console.log(JSON.stringify(jun.dataStockOpname));
        Ext.getCmp('form-InputStockOpname').getForm().submit({
            url: 'StockMoves/StockOpname',
            timeOut: 1000,
            scope: this,
            params: {
                data: Ext.encode(jun.dataStockOpname)
            },
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Connection failure has occured.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.stockOpnameStore = new Ext.data.JsonStore({
    storeId: 'jun.stockOpnameStore',
    idProperty: 'kode_barang',
    fields: ['kode_barang', 'qty']
});
jun.CheckInputStockOpnameWin = Ext.extend(Ext.Window, {
    title: "Data Stock Opname",
    width: 400,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.stockOpnameStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'kode_barang', dataIndex: 'kode_barang', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            if (jun.rztBarangLib.findExact('kode_barang', value) == -1)
                                metaData.style = "background-color: #FF7F7F;";
                            return value;
                        }
                    },
                    {
                        header: 'qty', dataIndex: 'qty', width: 60, align: "right",
                        renderer: Ext.util.Format.numberRenderer("0,0")
                    }
                ]
            }
        ];
        jun.CheckInputStockOpnameWin.superclass.initComponent.call(this);
    }
});
jun.QueryStockOpnameWin = Ext.extend(Ext.Window, {
    title: "Query Input Stock Opname",
    width: 600,
    height: 500,
    layout: "absolute",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'textarea',
                ref: "txtQuery",
                anchor: '100% 100%'
            }
        ];
        jun.QueryStockOpnameWin.superclass.initComponent.call(this);
        var query = "INSERT INTO posng.nscc_stock_moves(stock_moves_id,type_no,trans_no,tran_date,price,reference,qty,barang_id,store,tdate,id_user,up) VALUES ";
        for (i = 0; i < jun.stockOpnameStore.getCount(); i++) {
            var r = jun.stockOpnameStore.getAt(i);
            var idx = jun.rztBarangLib.find('kode_barang', r.get('kode_barang'));
            var rb = jun.rztBarangLib.getAt(idx);
            query += "\n(UUID(),-1,trans_no,'" + this.tgl.format('Y-m-d') + "',0,'SALDO AWAL'," + r.get('qty') + ",'" + rb.get('barang_id') + "','" + this.store + "',NOW(),'fff40883-0858-11e6-8e69-00ff55a5a602',0),";
        }
        this.txtQuery.setValue(query);
    }
});




jun.ReportCustomerSummaryDetails = Ext.extend(Ext.Window, {
    title: "Customer Summary Details",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCustomersCmp.getTotalCount() === 0) {
            jun.rztCustomersCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportCustomerSummaryDetails",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        ref: '../customer',
                        fieldLabel: 'Customer',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztCustomersDetailsCmp,
                        hiddenName: 'customer',
                        valueField: 'no_customer',
                        displayField: 'nama_customer',
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: 'div.search-item',
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">',
                                '<h3><span>{tgl_lahir:date("j M Y")}<br />{telp}</span>{nama_customer} ({no_customer})</h3>',
                                '{alamat}',
                                '</div></tpl>'
                                ),
                        //allowBlank: false,
                        listWidth: 450,
                        lastQuery: "",
                        emptyText: "All Customer",
                        anchor: "100%"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportCustomerSummaryDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportCustomerSummaryDetails").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportCustomerSummaryDetails").getForm().url = "Report/CustomerSummaryDetails";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportCustomerSummaryDetails').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/CustomerSummaryDetails";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportCustomerSummaryDetails").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportCustomerSummaryDetails").getForm().url = url;
            var form = Ext.getCmp('form-ReportCustomerSummaryDetails').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportCustomerSummaryDetails").getForm().getValues());
        }
    }
});
jun.ReportFeeCard = Ext.extend(Ext.Window, {
    title: "Fee Card",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportFeeCard",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportFeeCard.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportFeeCard").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportFeeCard").getForm().url = "Report/FeeCard";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportFeeCard').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/FeeCard";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportFeeCard").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportFeeCard").getForm().url = url;
            var form = Ext.getCmp('form-ReportFeeCard').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportFeeCard").getForm().getValues());
        }
    }
});
jun.ReportInfoEfektivitas = Ext.extend(Ext.Window, {
    title: "Info Efektivitas",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInfoEfektivitas",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportInfoEfektivitas.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInfoEfektivitas").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInfoEfektivitas").getForm().url = "Report/InfoEfektivitas";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInfoEfektivitas').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/InfoEfektivitas";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportInfoEfektivitas").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInfoEfektivitas").getForm().url = url;
            var form = Ext.getCmp('form-ReportInfoEfektivitas').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportInfoEfektivitas").getForm().getValues());
        }
    }
});
jun.ReportTerimaBarang = Ext.extend(Ext.Window, {
    title: "Transfer Barang Masuk",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportTerimaBarang",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportTerimaBarang.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportTerimaBarang").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTerimaBarang").getForm().url = "Report/TerimaBarang";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportTerimaBarang').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/TerimaBarang";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportTerimaBarang").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportTerimaBarang").getForm().url = url;
            var form = Ext.getCmp('form-ReportTerimaBarang').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportTerimaBarang").getForm().getValues());
        }
    }
});
jun.ReportEfektivitas = Ext.extend(Ext.Window, {
    title: "Efektivitas Report",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportEfektivitas",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ReportEfektivitas.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        var url = "Report/Efektivitas";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportEfektivitas").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportEfektivitas").getForm().url = url;
            var form = Ext.getCmp('form-ReportEfektivitas').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "_blank";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportEfektivitas").getForm().getValues());
        }
    }
});
jun.ReportPasienBaruRiilFaktur = Ext.extend(Ext.Window, {
    title: "Pasien Baru, Riil, Faktur",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportPasienBaruRiilFaktur",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ReportPasienBaruRiilFaktur.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        var url = "Report/PasienBaruRiilFaktur";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportPasienBaruRiilFaktur").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportPasienBaruRiilFaktur").getForm().url = url;
            var form = Ext.getCmp('form-ReportPasienBaruRiilFaktur').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "_blank";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportPasienBaruRiilFaktur").getForm().getValues());
        }
    }
});
jun.ReportStockInTransit = Ext.extend(Ext.Window, {
    title: "Stock In Transit",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 120,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportStockInTransit",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Tgl',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl",
                        value: new Date(),
                        readOnly: true,
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportStockInTransit.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        this.formz.getForm().standardSubmit = !0;
        this.formz.getForm().url = "Report/StockInTransit";
        this.format.setValue("html");
        var form = this.formz.getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/StockInTransit";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            this.formz.getForm().standardSubmit = !0;
            this.formz.getForm().url = url;
            var form = this.formz.getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', this.formz.getForm().getValues());
        }
    }
});
jun.ReportPurchaseOrder = Ext.extend(Ext.Window, {
    title: "Rekap Purchase Order",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 240,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztBarangNonJasaAll.getTotalCount() === 0)
            jun.rztBarangNonJasaAll.load();
        if (jun.rztSupplierLib.getTotalCount() === 0)
            jun.rztSupplierLib.load();
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background: none; padding: 10px",
                id: "form-ReportPurchaseOrder",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Supplier',
                        emptyText: 'All',
                        store: jun.rztSupplierLib,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Status',
                        emptyText: 'All',
                        hiddenName: 'status',
                        store: new Ext.data.ArrayStore({
                            id: 0,
                            fields: ['val', 'displayText'],
                            data: [['all', 'ALL'], [0, 'OPEN'], [1, 'PARTIAL RCVD'], [2, 'RECEIVED'], [3, 'CLOSED']]
                        }),
                        valueField: 'val',
                        displayField: 'displayText',
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan Detail',
                        name: 'show_detail',
                        ref: '../show_detail'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Product',
                        emptyText: 'All',
                        store: jun.rztBarangNonJasaAll,
                        hiddenName: 'barang_id',
                        valueField: 'barang_id',
                        displayField: 'kode_barang',
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<span style="width:260px;display:inline-block;"><b>{kode_barang}</b><br><i>{nama_barang}</i></span>',
                                "</div></tpl>"),
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportPurchaseOrder.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportPurchaseOrder").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportPurchaseOrder").getForm().url = "Report/RekapPurchaseOrder";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportPurchaseOrder').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportRekapPurchaseOrder = Ext.extend(Ext.Window, {
    title: "Rekap Purchase Order",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekapPurchaseOrder",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        value: "excel",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Supplier',
                        store: jun.rztSupplierCmp,
                        allowBlank: true,
                        emptyText: 'ALL Supplier',
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ReportRekapPurchaseOrder.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        var url = "Report/PrintRekapPurchaseOrder";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportRekapPurchaseOrder").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportRekapPurchaseOrder").getForm().url = url;
            var form = Ext.getCmp('form-ReportRekapPurchaseOrder').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportRekapPurchaseOrder").getForm().getValues());
        }
    }
});
jun.ReportRekapOrderDropping = Ext.extend(Ext.Window, {
    title: "Rekap Transfer Request",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 205,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRekapOrderDropping",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        value: "excel",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Pengirim',
                        ref: '../store_pengirim',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: STORE,
                        //value: STORE,
                        //readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan Rata-rata (per bulan)',
                        name: 'show_ratarata'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRekapOrderDropping.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportRekapOrderDropping").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRekapOrderDropping").getForm().url = "Report/RekapOrderDropping";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportRekapOrderDropping').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportSupplierInvoice = Ext.extend(Ext.Window, {
    title: "Rekap Invoice Supplier",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 270,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztSupplierLib.getTotalCount() === 0)
            jun.rztSupplierLib.load();
        if (jun.rztStoreCmp.getTotalCount() === 0)
            jun.rztStoreCmp.load();

        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background: none; padding: 10px",
                id: "form-ReportSupplierInvoice",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Supplier',
                        emptyText: 'All',
                        store: jun.rztSupplierLib,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Tipe',
                        emptyText: 'All',
                        hiddenName: 'type_',
                        store: new Ext.data.ArrayStore({
                            id: 0,
                            fields: ['val', 'displayText'],
                            data: [[0, 'INVOICE'], [1, 'RETUR']]
                        }),
                        valueField: 'val',
                        displayField: 'displayText',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Status',
                        emptyText: 'All',
                        hiddenName: 'status',
                        store: new Ext.data.ArrayStore({
                            id: 0,
                            fields: ['val', 'displayText'],
                            data: [[0, 'OPEN'], [1, 'LUNAS']]
                        }),
                        valueField: 'val',
                        displayField: 'displayText',
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Tampilkan Detail',
                        name: 'show_detail',
                        ref: '../show_detail'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Charge',
                        emptyText: 'All',
                        store: jun.rztStoreCmp,
                        hiddenName: 'charge',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        ref: '../charge',
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSupplierInvoice.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.show_detail.on("check", this.onShowDetailcheck, this);
    },
    onbtnPdfclick: function () {
        this.requestReport("html", "_blank");
    },
    onbtnSaveclick: function () {
        this.requestReport("excel", "myFrame");
    },
    onShowDetailcheck: function (c, checked) {
        this.charge.reset();
        this.charge.setReadOnly(!checked);
    },
    requestReport: function (format, t) {
        Ext.getCmp("form-ReportSupplierInvoice").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSupplierInvoice").getForm().url = "Report/ReportSupplierInvoice";
        this.format.setValue(format);
        var form = Ext.getCmp('form-ReportSupplierInvoice').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = t;
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportBonus = Ext.extend(Ext.Window, {
    title: "Report Bonus",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 205,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-Bonus",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Show details',
                        name: 'show_detail',
                        ref: '../show_detail'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportBeautySummary.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-Bonus").getForm().standardSubmit = !0;
        Ext.getCmp("form-Bonus").getForm().url = "Report/Bonus";
        this.format.setValue("html");
        var form = Ext.getCmp('form-Bonus').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-Bonus").getForm().standardSubmit = !0;
        Ext.getCmp("form-Bonus").getForm().url = "Report/Bonus";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-Bonus').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportProduksi = Ext.extend(Ext.Window, {
    title: "Report Produksi",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 240,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-Produksi",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Show details',
                        name: 'show_detail',
                        ref: '../show_detail'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Show return produksi',
                        name: 'show_retur',
                        ref: '../show_detail'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Show return only',
                        name: 'show_retur_only',
                        ref: '../show_retur_only'
                    },
                    {
                        xtype: 'checkbox',
                        fieldLabel: "",
                        boxLabel: 'Show total',
                        name: 'show_total',
                        ref: '../show_total'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportProduksi.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-Produksi").getForm().standardSubmit = !0;
        Ext.getCmp("form-Produksi").getForm().url = "Report/Produksi";
        this.format.setValue("html");
        var form = Ext.getCmp('form-Produksi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        Ext.getCmp("form-Produksi").getForm().standardSubmit = !0;
        Ext.getCmp("form-Produksi").getForm().url = "Report/Produksi";
        this.format.setValue("excel");
        var form = Ext.getCmp('form-Produksi').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

jun.SysPrefEmployeeWin = Ext.extend(Ext.Window, {
    title: "Employee Assignment",
    iconCls: "silk13-user",
    id: 'docs-jun.SysPrefEmployeeWin',
    modez: 1,
    width: 400,
    height: 205,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: 1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztEmployeeCmp.getTotalCount() === 0) {
            jun.rztEmployeeCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'PC',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztEmployeeCmp,
                        hiddenName: 'pengelola_cabang',
                        name: 'pengelola_cabang',
                        valueField: 'employee_id',
                        displayField: 'nama_employee',
                        emptyText: "Select employee",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Admin',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztEmployeeCmp,
                        hiddenName: 'admin_cabang',
                        name: 'admin_cabang',
                        valueField: 'employee_id',
                        displayField: 'nama_employee',
                        emptyText: "Select employee",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Apoteker',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztEmployeeCmp,
                        hiddenName: 'apoteker',
                        name: 'apoteker',
                        valueField: 'employee_id',
                        displayField: 'nama_employee',
                        emptyText: "Select employee",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Stocker',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztEmployeeCmp,
                        hiddenName: 'stocker',
                        name: 'stocker',
                        valueField: 'employee_id',
                        displayField: 'nama_employee',
                        emptyText: "Select employee",
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ReportBeautySummary.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.loadData();
    },
    loadData: function () {
        this.btnDisabled(true);
        Ext.Ajax.request({
            url: 'SysPrefs/Employee',
            success: function (response, opts) {
                var response = Ext.decode(response.responseText);
                var form = Ext.getCmp('docs-jun.SysPrefEmployeeWin').formz.getForm();
                for (i = 0; i < response.total; i++) {
                    form.findField(response.results[i]['name_']).setValue(response.results[i]['value_']);
                }
                Ext.getCmp('docs-jun.SysPrefEmployeeWin').btnDisabled(false);
            },
            failure: function (response, opts) {
                switch (response.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', response.result.msg);
                }
            }
        });
    },
    btnDisabled: function (v) {
        this.btnSaveClose.setDisabled(v);
    },
    onbtnSaveCloseClick: function () {
        this.btnDisabled(true);
        this.formz.getForm().submit({
            url: 'SysPrefs/EmployeeUpdate',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });

                if (response.success)
                    this.close();
                else
                    this.btnDisabled(false);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.ReportCashFlow = Ext.extend(Ext.Window, {
    title: "Report Cash Flow",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportCashFlow",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    new jun.comboBulan({
                        hiddenName: 'month',
                        fieldLabel: 'Month',
                        displayField: "namaBulan",
                        valueField: "noBulan",
                        anchor: '100%'
                    }),
                    {
                        xtype: "spinnerfield",
                        fieldLabel: "Year",
                        name: "year",
                        ref: "../year",
                        maxLength: 4,
                        minValue: 2012,
                        maxValue: 9999,
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        forceSelection: true,
                        emptyText: "All Branch",
                        allowBlank: true,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ReportCashFlow.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnSaveclick: function () {
        var url = "Report/CashFlow";
        this.format.setValue("excel");
        Ext.getCmp("form-ReportCashFlow").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportCashFlow").getForm().url = url;
        var form = Ext.getCmp('form-ReportCashFlow').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "blank_";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});
jun.ReportOmsetKasir = Ext.extend(Ext.Window, {
    title: "Cashier Turnover",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportOmsetKasir",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Cashier',
                        store: [[1, 'A'],
                            [2, 'B'],
                            [3, 'C'],
                            [4, 'D'],
                            [5, 'E']
                        ],
//                        store: [[1, 'LK'],
//                            [2, 'Sakit'],
//                            [3, 'Lupa Absen'],
//                            [4, 'Cuti Tahunan'],
//                            [5, 'Ganti Off']
//                        ],
                        ref: '../cashier',
                        name: 'cashier',
                        allowBlank: true,
                        emptyText: 'ALL Cashier',
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Print",
                    ref: "../btnPrint",
                    hidden: true
                }
            ]
        };
        jun.ReportOmsetKasir.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportOmsetKasir").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportOmsetKasir").getForm().url = "Report/OmsetKasir";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportOmsetKasir').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/OmsetKasir";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportOmsetKasir").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportOmsetKasir").getForm().url = url;
            var form = Ext.getCmp('form-ReportOmsetKasir').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportOmsetKasir").getForm().getValues());
        }
    }
});
jun.ReportOmsetGroup = Ext.extend(Ext.Window, {
    title: "Omset Per Group",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztGrupCmp.getTotalCount() === 0) {
            jun.rztGrupCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportOmsetGroup",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
//                    {
//                        xtype: 'combo',
//                        typeAhead: true,
//                        triggerAction: 'all',
//                        lazyRender: true,
//                        mode: 'local',
//                        forceSelection: true,
//                        fieldLabel: 'Group',
//                        store: jun.rztGrupCmp,
//                        ref: '../grup',
//                        name: 'grup_id',
//                        valueField: 'nama_grup',
//                        displayField: 'nama_grup',
//                        allowBlank: true,
//                        emptyText: 'ALL Group',
//                        anchor: '100%'
//                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Print",
                    ref: "../btnPrint",
                    hidden: true
                }
            ]
        };
        jun.ReportOmsetGroup.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportOmsetGroup").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportOmsetGroup").getForm().url = "Report/OmsetGroup";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportOmsetGroup').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/OmsetGroup";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportOmsetGroup").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportOmsetGroup").getForm().url = url;
            var form = Ext.getCmp('form-ReportOmsetGroup').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportOmsetGroup").getForm().getValues());
        }
    }
});

jun.ReportSalesSummaryReceiptDetailsCus = Ext.extend(Ext.Window, {
    title: "Sales Summary Receipt Details test",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportSalesSummaryReceiptDetailsCus",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportSalesSummaryReceiptDetails.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportSalesSummaryReceiptDetailsCus").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportSalesSummaryReceiptDetailsCus").getForm().url = "Report/SalesSummaryReceiptDetailsCus";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportSalesSummaryReceiptDetailsCus').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/SalesSummaryReceiptDetailsCus";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportSalesSummaryReceiptDetailsCus").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportSalesSummaryReceiptDetailsCus").getForm().url = url;
            var form = Ext.getCmp('form-ReportSalesSummaryReceiptDetailsCus').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportSalesSummaryReceiptDetailsCus").getForm().getValues());
        }
    }
});

jun.ReportFeeReferral = Ext.extend(Ext.Window, {
    title: "ReportFeeReferral",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportFeeReferral",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportFeeReferral.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportFeeReferral").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportFeeReferral").getForm().url = "Report/FeeReferral";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportFeeReferral').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/FeeReferral";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportFeeReferral").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportFeeReferral").getForm().url = url;
            var form = Ext.getCmp('form-ReportFeeReferral').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportFeeReferral").getForm().getValues());
        }
    }
});

jun.FeeReferral = Ext.extend(Ext.Window, {
    title: "Fee Referral",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 125,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                 xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-FeeReferral",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'numberfield',
                        fieldLabel: 'Fee',
//                        store: jun.rztSysPrefsReferral,
                        ref: '../referral_fee',
                        name: 'referral_fee',
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    }

                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.FeeReferral.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
        
        Ext.Ajax.request({
            url: 'SysPrefs/referral',
            method: 'POST',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                console.log(response);

//                this.store.setValue(response.store);
                this.referral_fee.setValue(response.value_);
                this.store.setValue(response.store);
                this.id = response.sys_prefs_id;
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
        
    },

    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm: function ()
    {
        this.btnDisabled(true);
        var urlz;
           urlz = 'SysPrefs/UpdateReferral/id/' + this.id;
  
        Ext.getCmp('form-FeeReferral').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztReferral.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-FeeReferral').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },
    onbtnSaveCloseClick: function ()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function ()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});
jun.ReportEfektivitasDokter = Ext.extend(Ext.Window, {
    title: "Efektivitas Doctor",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportEfektivitasDokter",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        forceSelection: true,
                        fieldLabel: 'Doctor',
                        ref: '../dokter',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztDokterStatus,
                        hiddenName: 'dokter_id',
                        name: 'dokter_id',
                        valueField: 'dokter_id',
                        displayField: 'nama_dokter',
                        allowBlank: false,       
                        anchor: '100%'
                    }                     
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportEfektivitasDokter.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportEfektivitasDokter").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportEfektivitasDokter").getForm().url = "Report/EfektivitasDokter";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportEfektivitasDokter').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/EfektivitasDokter";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportEfektivitasDokter").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportEfektivitasDokter").getForm().url = url;
            var form = Ext.getCmp('form-ReportEfektivitasDokter').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportEfektivitasDokter").getForm().getValues());
        }
    }
});
jun.ReportEfektivitasDoctorService = Ext.extend(Ext.Window, {
    title: "Efektivitas Doctor Service",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportEfektivitasDoctorService",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        forceSelection: true,
                        fieldLabel: 'Doctor',
                        ref: '../dokter',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztDokterStatus,
                        hiddenName: 'dokter_id',
                        name: 'dokter_id',
                        valueField: 'dokter_id',
                        displayField: 'nama_dokter',
                        allowBlank: false,       
                        anchor: '100%'
                    }                     
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportEfektivitasDoctorService.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportEfektivitasDoctorService").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportEfektivitasDoctorService").getForm().url = "Report/EfektivitasDoctorService";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportEfektivitasDoctorService').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/EfektivitasDoctorService";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportEfektivitasDoctorService").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportEfektivitasDoctorService").getForm().url = url;
            var form = Ext.getCmp('form-ReportEfektivitasDoctorService').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                    '.xls', Ext.getCmp("form-ReportEfektivitasDoctorService").getForm().getValues());
        }
    }


});

jun.ReportAssetDepriciation = Ext.extend(Ext.Window, {
    title: "Show Report Depriciation",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAssetDepriciation",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_trans_idid',
                        name: "asset_trans_id",
                        ref: '../asset_trans_id',
                        hidden:true,
                        //valueField: 'asset_trans_id',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'No.Activa',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'atiid',
                        name: "ati",
                        ref: '../ati',
                        //valueField: 'asset_trans_id',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'Acquisition Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "asset_trans_date",
                        ref: '../asset_trans_date',
                        readOnly: true,
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportAssetDepriciation.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportAssetDepriciation").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAssetDepriciation").getForm().url = "Report/AssetDepriciation";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAssetDepriciation').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/AssetDepriciation";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportAssetDepriciation").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportAssetDepriciation").getForm().url = url;
            var form = Ext.getCmp('form-ReportAssetDepriciation').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportAssetDepriciation").getForm().getValues());
        }
    }
});

jun.ReportAssetTotal = Ext.extend(Ext.Window, {
    title: "Report Asset",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 220,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztStoreTrans.getTotalCount() === 0) {
            jun.rztStoreTrans.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAssetTotal",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_trans_idid',
                        name: "asset_trans_id",
                        ref: '../asset_trans_id',
                        hidden:true,
                        //valueField: 'asset_trans_id',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category_id',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%",
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreTrans,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showreportasset",
                        ref: '../showreportasset',
                        defaults: {xtype: "radio",name: "showreportasset"},
                        items: [
                            {
                                boxLabel: "Total",
                                inputValue: "T"
                            },
                            {
                                boxLabel: "Detail",
                                inputValue: "D"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportAssetTotal.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        var url = "";
        var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotal";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }

        Ext.getCmp("form-ReportAssetTotal").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAssetTotal").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAssetTotal').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {

        var url = "";
        var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotal";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }

        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportAssetTotal").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportAssetTotal").getForm().url = url;
            var form = Ext.getCmp('form-ReportAssetTotal').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportAssetTotal").getForm().getValues());
        }
    }
});

jun.ReportAssetDeprisiasiAktif = Ext.extend(Ext.Window, {
    title: "Report Asset Depreciation",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 206,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztStoreTrans.getTotalCount() === 0) {
            jun.rztStoreTrans.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAssetDeprisiasiAktif",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    /*{
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_trans_idid',
                        name: "asset_trans_id",
                        ref: '../asset_trans_id',
                        hidden:true,
                        //valueField: 'asset_trans_id',
                        anchor: "100%"
                    },*/
                    /*{
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category_id',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%",
                    },*/
                    {
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category_id',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%",
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreTrans,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    /*{
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showreportasset",
                        ref: '../showreportasset',
                        defaults: {xtype: "radio",name: "showreportasset"},
                        items: [
                            {
                                boxLabel: "Total",
                                inputValue: "T"
                            },
                            {
                                boxLabel: "Detail",
                                inputValue: "D"
                            }
                        ]
                    }*/
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportAssetDeprisiasiAktif.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        var url = "Report/DeprisiasiAktif";
        /*var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotal";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }*/

        Ext.getCmp("form-ReportAssetDeprisiasiAktif").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAssetDeprisiasiAktif").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAssetDeprisiasiAktif').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {

        var url = "Report/DeprisiasiAktif";
        /*        var selectedz = this.showreportasset.getValue();
                if(selectedz && selectedz.inputValue == 'T')
                {
                    url = "Report/AssetTotal";
                    //url = "Report/bep";
                }
                else
                {
                    url = "Report/AssetTotalDetail";
                }*/

        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportAssetDeprisiasiAktif").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportAssetDeprisiasiAktif").getForm().url = url;
            var form = Ext.getCmp('form-ReportAssetDeprisiasiAktif').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportAssetDeprisiasiAktif").getForm().getValues());
        }
    }
});


jun.ReportAssetTotalNonActive = Ext.extend(Ext.Window, {
    title: "Report Asset (NonActive)",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 220,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztStoreTrans.getTotalCount() === 0) {
            jun.rztStoreTrans.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAssetTotalNonActive",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_trans_idid',
                        name: "asset_trans_id",
                        ref: '../asset_trans_id',
                        hidden:true,
                        //valueField: 'asset_trans_id',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category_id',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%",
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreTrans,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showreportasset",
                        ref: '../showreportasset',
                        defaults: {xtype: "radio",name: "showreportasset"},
                        items: [
                            {
                                boxLabel: "Total",
                                inputValue: "T"
                            },
                            {
                                boxLabel: "Detail",
                                inputValue: "D"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportAssetTotalNonActive.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        var url = "";
        var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotalNonActive";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetailNonActive";
        }

        Ext.getCmp("form-ReportAssetTotalNonActive").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAssetTotalNonActive").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAssetTotalNonActive').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {

        var url = "";
        var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotalNonActive";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetailNonActive";
        }

        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportAssetTotalNonActive").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportAssetTotalNonActive").getForm().url = url;
            var form = Ext.getCmp('form-ReportAssetTotalNonActive').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportAssetTotalNonActive").getForm().getValues());
        }
    }
});

jun.ReportRentAsset = Ext.extend(Ext.Window, {
    title: "Report Rent Asset",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        if (jun.rztStoreTrans.getTotalCount() === 0) {
            jun.rztStoreTrans.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRentAsset",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_trans_idid',
                        name: "asset_trans_id",
                        ref: '../asset_trans_id',
                        hidden:true,
                        //valueField: 'asset_trans_id',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category_id',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%",
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreTrans,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        anchor: '100%'
                    },
                    /*{
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showreportasset",
                        ref: '../showreportasset',
                        defaults: {xtype: "radio",name: "showreportasset"},
                        items: [
                            {
                                boxLabel: "Total",
                                inputValue: "T"
                            },
                            {
                                boxLabel: "Detail",
                                inputValue: "D"
                            }
                        ]
                    }*/
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportRentAsset.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        var url = "Report/RentAsset";
        /*var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotal";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }*/

        Ext.getCmp("form-ReportRentAsset").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRentAsset").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRentAsset').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {

        var url = "Report/RentAsset";
        /*var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/RentAsset";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }*/

        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportRentAsset").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportRentAsset").getForm().url = url;
            var form = Ext.getCmp('form-ReportRentAsset').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportRentAsset").getForm().getValues());
        }
    }
});

jun.ReportJurnalAsset = Ext.extend(Ext.Window, {
    title: "Report Jurnal Asset",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 171,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreTrans.getTotalCount() === 0) {
            jun.rztStoreTrans.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportJurnalAsset",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    /*{
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_trans_idid',
                        name: "asset_trans_id",
                        ref: '../asset_trans_id',
                        hidden:true,
                        //valueField: 'asset_trans_id',
                        anchor: "100%"
                    },*/
                    /*{
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category_id',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%",
                    },*/
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreTrans,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    /*{
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showreportasset",
                        ref: '../showreportasset',
                        defaults: {xtype: "radio",name: "showreportasset"},
                        items: [
                            {
                                boxLabel: "Total",
                                inputValue: "T"
                            },
                            {
                                boxLabel: "Detail",
                                inputValue: "D"
                            }
                        ]
                    }*/
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportJurnalAsset.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        var url = "Report/JurnalAsset";
        /*var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotal";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }*/

        Ext.getCmp("form-ReportJurnalAsset").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportJurnalAsset").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportJurnalAsset').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {

        var url = "Report/JurnalAsset";
/*        var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotal";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }*/

        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportJurnalAsset").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportJurnalAsset").getForm().url = url;
            var form = Ext.getCmp('form-ReportJurnalAsset').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportJurnalAsset").getForm().getValues());
        }
    }
});


//CABANG
jun.ReportShowAsset = Ext.extend(Ext.Window, {
    title: "Export Assets",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 208,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztStoreTrans.getTotalCount() === 0) {
            jun.rztStoreTrans.load();
        }
        if (jun.rztAssetCategory.getTotalCount() === 0) {
            jun.rztAssetCategory.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportShowAsset",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    /*{
                        fieldLabel: 'Id',
                        xtype: 'textfield',
                        format: 'd M Y',
                        id: 'asset_trans_idid',
                        name: "asset_trans_id",
                        ref: '../asset_trans_id',
                        hidden:true,
                        //valueField: 'asset_trans_id',
                        anchor: "100%"
                    },*/
                    {
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category',
                        store: jun.rztAssetCategory,
                        valueField: 'category_id',
                        displayField: 'category_name',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Asset Category",
                        mode : 'local',
                        anchor: "100%",
                    },
                    {
                        fieldLabel: 'From Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To Date',
                        xtype: 'xdatefield',
                        name: "tglto",
                        format: 'd M Y',
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../branch',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreTrans,
                        hiddenName: 'branch',
                        name: 'branch',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        //value: STORE,
                        //emptyText: "Asset Name",
                        anchor: '100%'
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                }
            ]
        };
        jun.ReportShowAsset.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this)
    },
    onbtnPdfclick: function () {
        var url = "Report/ShowAsset";
        /*var selectedz = this.showreportasset.getValue();
        if(selectedz && selectedz.inputValue == 'T')
        {
            url = "Report/AssetTotal";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/AssetTotalDetail";
        }*/

        Ext.getCmp("form-ReportShowAsset").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportShowAsset").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportShowAsset').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {

        var url = "Report/ShowAsset";
        /*        var selectedz = this.showreportasset.getValue();
                if(selectedz && selectedz.inputValue == 'T')
                {
                    url = "Report/AssetTotal";
                    //url = "Report/bep";
                }
                else
                {
                    url = "Report/AssetTotalDetail";
                }*/

        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportShowAsset").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportShowAsset").getForm().url = url;
            var form = Ext.getCmp('form-ReportShowAsset').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportShowAsset").getForm().getValues());
        }
    }
});