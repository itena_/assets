jun.LahaImportBiayaGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "LahaImportBiaya",
    id: 'docs-jun.LahaImportBiayaGrid',
    // iconCls: "silk-grid",
    // viewConfig: {
    //     forceFit: true
    // },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    view: new Ext.grid.GroupingView({
        forceFit: true,
        groupTextTpl: '({[values.rs.length]} COA)',
        showGroupName: false,
        enableNoGroups: false,
        enableGroupingMenu: false,
        hideGroupedColumn: true
    }),
    plugins: new Ext.ux.grid.GroupSummary(this),
    columns: [
        {
            header: 'laha_import_biaya_id',
            sortable: true,
            resizable: true,
            dataIndex: 'laha_import_biaya_id'
        },
        {
            header: 'COA',
            sortable: true,
            resizable: true,
            dataIndex: 'account_code',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note_',
            width: 200
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store'
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            summaryType: 'sum'
        }
    ],
    initComponent: function () {
        this.store = jun.rztLahaImportBiaya;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'COA :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            forceSelection: true,
                            fieldLabel: 'COA',
                            //store: this.mode,
                            store: jun.rztChartMasterCmp,
                            ref: '../../account_code',
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                                "</div></tpl>"),
                            listWidth: 500,
                            // hiddenName: 'account_code',
                            valueField: 'account_code',
                            displayField: 'account_code',
                            anchor: '100%'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Total :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../amount',
                            width: 100,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'uctextfield',
                            ref: '../../note_',
                            width: 200,
                            // colspan: 3,
                            maxLength: 100
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Branch :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            fieldLabel: 'Branch',
                            ref: '../../store_',
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            store: jun.rztStoreCmp,
                            valueField: 'store_kode',
                            displayField: 'store_kode',
                            emptyText: "Branch",
                            //value: STORE,
                            readOnly: !HEADOFFICE,
                            width: 100
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.LahaImportBiayaGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        jun.rztLahaImportBiaya.on('add', this.refreshData, this);
        jun.rztLahaImportBiaya.on('update', this.refreshData, this);
        jun.rztLahaImportBiaya.on('remove', this.refreshData, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    refreshData: function (t, r) {
        var total = t.sum("amount");
        Ext.getCmp('b_d_total_biaya_id').setValue(total);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var note_ = this.note_.getValue();
        var account_code = this.account_code.getValue();
        var store = this.store_.getValue();
        var amount = parseFloat(this.amount.getValue());
        if (account_code == "" || account_code == undefined) {
            Ext.MessageBox.alert("Error", "COA must selected.");
            return;
        }
        if (amount < 0) {
            Ext.MessageBox.alert("Error", "Total must greater than 0.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('account_code', account_code);
            record.set('note_', note_);
            record.set('store', store);
            record.set('amount', amount);
            record.commit();
        } else {
            var c = jun.rztLahaImportBiaya.recordType,
                d = new c({
                    account_code: account_code,
                    note_: note_,
                    store: store,
                    amount: amount
                });
            jun.rztLahaImportBiaya.add(d);
        }
        this.account_code.reset();
        this.note_.reset();
        this.store_.reset();
        this.amount.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.account_code.setValue(record.data.account_code);
            this.note_.setValue(record.data.note_);
            this.store_.setValue(record.data.store);
            this.amount.setValue(record.data.amount);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }
})
