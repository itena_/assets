jun.BarangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Master Product/Treatment",
    id: 'docs-jun.BarangGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: true
    }),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Barcode',
            sortable: true,
            resizable: true,
            dataIndex: 'barcode',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Item Code',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_barang',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Item Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_barang',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Item Description',
            sortable: true,
            resizable: true,
            dataIndex: 'ket',
            width: 100,
            filter: {xtype: "textfield"}
        }
    ],
    initComponent: function () {
        this.store = jun.rztBarang;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Product/Treatment',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Product/Treatment',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                }
            ]
        };
        if (jun.rztTipeBarang.getTotalCount() === 0) {
            jun.rztTipeBarang.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        this.store.baseParams = {
            mode: "grid"
        };
        this.store.reload();
        this.store.baseParams = {};
        jun.BarangGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.BarangWin({
            modez: 0
        });
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        var idz = selectedz.json.barang_id;
        var form = new jun.BarangWin({
            modez: 1,
            id: idz
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        Ext.Ajax.request({
            url: 'Barang/delete/id/' + record.json.barang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztBarang.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})