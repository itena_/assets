jun.TipeBarangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TipeBarangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TipeBarangStoreId',
            url: 'TipeBarang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tipe_barang_id'},
                {name: 'nama_tipe'},
                {name: 'coa'}
            ]
        }, cfg));
    }
});
jun.rztTipeBarang = new jun.TipeBarangstore();
//jun.rztTipeBarang.load();
