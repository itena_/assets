jun.TransferBarangAssetstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferBarangAssetstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferBarangAssetStoreId',
            url: 'TransferBarangAsset',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'transfer_asset_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tdate'},
                {name: 'doc_ref_other'},
                {name: 'user_id'},
                {name: 'type_'},
                {name: 'store'},
                {name: 'supplier_id'},
                {name: 'total'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'bruto'},
                {name: 'vat'},
                {name: 'up'},
                {name: 'total_pot'},
                {name: 'total_discrp1'},
                {name: 'tgl_jatuh_tempo'},
                {name: 'lunas'},
            ]
        }, cfg));
    }
});
jun.rztTransferBarangAsset = new jun.TransferBarangAssetstore({url: 'TransferBarangAsset/IndexIn'});
jun.rztReturnTransferBarangAsset = new jun.TransferBarangAssetstore({url: 'TransferBarangAsset/IndexOut'});
//jun.rztTransferBarangAsset.load();
