jun.PelunasanPiutangDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PelunasanPiutangDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PelunasanPiutangDetilStoreId',
            url: 'PelunasanPiutangDetil',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pelunasan_piutang_detil_id'},
                {name: 'kas_dibayar'},
                {name: 'no_faktur'},
                {name: 'sisa'},
                {name: 'pelunasan_piutang_id'},
                {name: 'transfer_item_id'},

            ]
        }, cfg));
    },
    refreshData: function (a) {
        var subtotal = this.sum("kas_dibayar");
        Ext.getCmp("totalPelunasanUtangid").setValue(subtotal);
    }
});
jun.rztPelunasanPiutangDetil = new jun.PelunasanPiutangDetilstore();
//jun.rztPelunasanPiutangDetil.load();

jun.FakturPiutangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.FakturPiutangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'FakturPiutangStoreId',
            url: 'PembantuPelunasanPiutangDetil/faktur',
            autoLoad: !1,
            autoSave: !1,
            root: 'results',
            idProperty: 'row',
            totalProperty: 'total',
            fields: [
                {name: 'transfer_item_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'no_faktur'},
                {name: 'nilai', type: 'float'},
                {name: 'sisa', type: 'float'}
            ]
        }, cfg));
    }
});
jun.rztFakturPiutang = new jun.FakturPiutangstore();