jun.TipeEmployeestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TipeEmployeestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TipeEmployeeStoreId',
            url: 'TipeEmployee',
            root: 'results',
            totalProperty: 'total',
            autoLoad: true,
            fields: [
                {name: 'tipe_employee_id'},
                {name: 'kode'},
                {name: 'nama_'}
            ]
        }, cfg));
    }
});
jun.rztTipeEmployee = new jun.TipeEmployeestore();
jun.rztTipeEmployeeAssign = new jun.TipeEmployeestore({baseParams:{assign : 1}});
//jun.rztTipeEmployee.load();
