jun.Groupstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Groupstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GroupStoreId',
            url: 'projection/Group',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'group_id'},
                {name:'group_code'},
                {name:'group_name'},
                {name:'description'},
                {name:'category_id'},
                {name:'businessunit_id'},
                {name:'uploadfile'},
                {name:'uploaddate'},
                {name:'category_code'},
                {name:'category_name'}
            ]
        }, cfg));
    }
});
jun.rztGroup = new jun.Groupstore();
jun.rztGroupLib = new jun.Groupstore();
jun.rztGroupLib.load();
