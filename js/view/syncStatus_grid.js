jun.SyncStatusGrid=Ext.extend(Ext.grid.GridPanel ,{
    title:"Sync Status",
    id:'docs-jun.SyncStatusGrid',
    iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
    plugins:[new Ext.ux.grid.GridHeaderFilters],
    columns:[

        {
            header:'Store',
            sortable:true,
            resizable:true,
            dataIndex:'store_code',
            width:30,
            filter: {xtype: "textfield"}
        },

        /*                        {
			header:'GLT Pusat',
			sortable:true,
			resizable:true,                        
            dataIndex:'glt_pusat',
			width:100
		},
        {
            header:'GLT Cabang',
            sortable:true,
            resizable:true,
            dataIndex:'glt_cabang',
            width:100
        },
                                {
			header:'BTR Pusat',
			sortable:true,
			resizable:true,                        
            dataIndex:'btr_pusat',
			width:100
		},
                                {
			header:'BTR Cabang',
			sortable:true,
			resizable:true,                        
            dataIndex:'btr_cabang',
			width:100
		},
                                {
			header:'Diff GLT',
			sortable:true,
			resizable:true,                        
            dataIndex:'diff_glt',
			width:50
		},
        {
            header:'Diff BTR',
            sortable:true,
            resizable:true,
            dataIndex:'diff_btr',
            width:50
        },*/
        {
            header:'Scan Date',
            sortable:true,
            resizable:true,
            dataIndex:'updated_date',
            width:50,
            renderer: Ext.util.Format.dateRenderer('d/M/Y H:i:s')
        },
        {
            header:'Start Scan',
            sortable:true,
            resizable:true,
            dataIndex:'startscan',
            width:35,
            renderer: Ext.util.Format.dateRenderer('d/M/Y')

        },
        {
            header:'End Scan',
            sortable:true,
            resizable:true,
            dataIndex:'endscan',
            width:35,
            renderer: Ext.util.Format.dateRenderer('d/M/Y')

        },
        {
            header: 'Scan Status',
            sortable: true,
            resizable: true,
            dataIndex: 'status_scan',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case SCAN_OK :
                        metaData.style += "background-color: #33ff33;";
                        metaData.style += 'border-right: solid Black 5px';
                        return 'OK';
                    case SCAN_PR :
                        metaData.style += "background-color: #ffff4d;";
                        metaData.style += 'border-right: solid Black 5px';
                        return 'PROCESS';
                    case SCAN_MK :
                        metaData.style += "background-color: #66ccff;";
                        metaData.style += 'border-right: solid Black 5px';
                        return 'MARKED';
                    case SCAN_NO :
                        metaData.style += "background-color: #ff0066;";
                        metaData.style += 'border-right: solid Black 5px';
                        return 'NO';
                    case SCAN_FL :
                        metaData.style += "background-color: #ff1a1a;";
                        metaData.style += 'border-right: solid Black 5px';
                        return 'FAILED';
                }
            },
        },

        {
            header:'Sync Date',
            sortable:true,
            resizable:true,
            dataIndex:'sync_date',
            width:50,
            renderer: Ext.util.Format.dateRenderer('d/M/Y H:i:s'),

        },
        {
            header:'Start Sync',
            sortable:true,
            resizable:true,
            dataIndex:'startsync',
            width:35,
            renderer: Ext.util.Format.dateRenderer('d/M/Y')

        },
        {
            header:'End Sync',
            sortable:true,
            resizable:true,
            dataIndex:'endsync',
            width:35,
            renderer: Ext.util.Format.dateRenderer('d/M/Y')

        },


        {
            header: 'Sync Status ',
            sortable: true,
            resizable: true,
            dataIndex: 'status_sync',
            width: 50,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case SYNC_OK :
                        metaData.style += "background-color: #33ff33;";
                        metaData.style += 'border-right: solid Black 5px';
                        return 'OK';
                    case SYNC_MK :
                        metaData.style += "background-color: #66ccff;";
                        metaData.style += 'border-right: solid Black 5px';
                        return 'MARKED';
                    case SYNC_PR :
                        metaData.style += "background-color: #ffff4d;";
                        metaData.style += 'border-right: solid Black 5px';
                        return 'PROCESS';
                    case SYNC_NO :
                        metaData.style += "background-color: #ff0066;";
                        metaData.style += 'border-right: solid Black 5px';
                        return 'NO';
                    case SYNC_FL :
                        metaData.style += "background-color: #ff1a1a;";
                        metaData.style += 'border-right: solid Black 5px';
                        return 'FAILED';
                }

            },


            /*filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [1, 'OK'], [2, 'PROCESS'],[0, 'NO']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }*/
        },
        {
            header:'Note',
            sortable:true,
            resizable:true,
            dataIndex:'note',
            width:100,
            field: {
                xtype : 'textfield'
            }
        },

    ],
    initComponent: function(){
        this.store = jun.rztSyncStatus;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd',
                    iconCls: 'silk13-add'
                },
                /*{
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },*/
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Delete',
                    ref: '../btnDelete',
                    iconCls: 'silk13-delete'
                },
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Scan',
                    ref: '../btnScan',
                    iconCls: "silk13-arrow_refresh_small",
                },
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Sync',
                    ref: '../btnSync',
                    iconCls: "silk13-arrow_refresh",
                },
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Mark',
                    ref: '../btnMark',
                    iconCls: 'silk13-accept'
                },
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'History',
                    ref: '../btnHistory',
                    iconCls: 'silk13-clock'
                },
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel',
                    iconCls: 'silk13-cancel'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.SyncStatusGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        //this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.btnScan.on('Click', this.Scan, this);
        this.btnSync.on('Click', this.Sync, this);
        this.btnMark.on('Click', this.Mark, this);
        this.btnCancel.on('Click', this.Cancel, this);
        this.btnHistory.on('Click', this.History, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on('activate', this.onActivate, this);
        this.on('close', this.onClose, this);
        this.autoreloadstore = setInterval(function(){jun.rztSyncStatus.reload();}, 30000);
    },

    onActivate:function()
    {
        this.btnCancel.setDisabled(false);
    },


    onClose:function()
    {
        clearInterval(this.autoreloadstore);
    },


    getrow: function (sm, idx, r)
    {
        this.record = r;
        var selectedz = this.sm.getSelections();

        var filter = r.get('status_scan') == SCAN_MK;
        this.btnMark.setText(filter ?"Unmark":"Mark");

        var filterSync = r.get('status_sync') == SYNC_PR;
        //this.btnCancel.setDisabled(filterSync ?"true":"false");
        this.btnCancel.setDisabled(!filterSync);
        //this.btnPrintOrderDropping.setDisabled((r.get('store_pengirim') != STORE || r.get('lunas') == PR_DRAFT));
    },

    History:function()
    {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih store");
            return;
        }

        var store = selectedz.json.store_code;

        var form = new jun.SyncHistoryWin({
            modez: 0,
            storecode: {store :store},
            timeOut: 1000,
            title: 'Sync History'
        });
        form.show();
    },

    Cancel:function()
    {
        var record = this.sm.getSelected();

        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Store");
            return;
        }

        Ext.Ajax.request({
            url: 'SyncStatus/cancel/id/' + record.json.sync_id,
            method: 'POST',
            success:function (f, a) {
                jun.rztSyncStatus.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },

    Mark:function()
    {
        var record = this.sm.getSelected();
        var urlz = "";


        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Store");
            return;
        }

        if(this.btnMark.text == "Mark")
        {
            urlz = 'SyncStatus/mark/id/' + record.json.sync_id;
        }
        else
        {
            urlz = 'SyncStatus/unmark/id/' + record.json.sync_id;
        }

        Ext.Ajax.request({
            url: urlz,
            method: 'POST',
            success:function (f, a) {
                jun.rztSyncStatus.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },

    Scan: function()
    {
        var selectedz = this.sm.getSelected();

        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a store");
            return;
        }

        var store = selectedz.json.store_code;
        var status_scan = selectedz.json.status_scan;
        //var status_sync = selectedz.json.status_sync;

        if (status_scan == 2 ) {
            Ext.MessageBox.alert("Warning", "Store ini sedang proses scan.");
            return;
        }
        else if(status_scan == 3)
        {
            Ext.MessageBox.alert("Warning", "Store yang ditandai tidak dapat di scan.");
            return;
        }

        var record = this.sm.getSelected();
        record.set('branch', store);
        record.commit();

        var form = new jun.ScanWin({modez:0});
        form.formz.getForm().loadRecord(this.record);
        form.show();
    },

    Sync: function(){

        var selectedz = this.sm.getSelected();

        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a store");
            return;
        }

        var store = selectedz.json.store_code;
        var status_scan = selectedz.json.status_scan;
        var status_sync = selectedz.json.status_sync;

        if (status_sync == 2) {
            Ext.MessageBox.alert("Warning", "Store ini sedang proses sinkron.");
            return;
        }
        else if(status_scan == 3)
        {
            Ext.MessageBox.alert("Warning", "Store yang ditandai tidak dapat di sync.");
            return;
        }

        var record = this.sm.getSelected();
        record.set('branch', store);
        record.commit();

        var form = new jun.SyncWin({modez:0});
        form.formz.getForm().loadRecord(this.record);
        form.show();
    },

    loadForm: function(){
        var form = new jun.SyncStatusWin({modez:0});
        form.show();
    },

    /*loadEditForm: function(){

        var selectedz = this.sm.getSelected();

        //var dodol = this.store.getAt(0);
         if(selectedz == undefined){
             Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
             return;
         }
        var idz = selectedz.json.sync_id;
        var form = new jun.SyncStatusWin({modez:1, id:idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },*/

    deleteRec : function(){
        Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },

    deleteRecYes : function(btn){

        if (btn == 'no') {
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
            return;
        }

        Ext.Ajax.request({
            url: 'SyncStatus/delete/id/' + record.json.sync_id,
            method: 'POST',
            success:function (f, a) {
                jun.rztSyncStatus.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
})
