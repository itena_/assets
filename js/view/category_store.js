jun.Categorystore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Categorystore.superclass.constructor.call(this, Ext.apply({
            storeId: 'CategoryStoreId',
            url: 'projection/Category',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'category_id'},
                {name:'category_code'},
                {name:'category_name'},
                {name:'description'},
                {name:'businessunit_id'},
                {name:'uploadfile'},
                {name:'uploaddate'},
                
            ]
        }, cfg));
    }
});
jun.rztCategory = new jun.Categorystore();
jun.rztCategoryLib = new jun.Categorystore();
jun.rztCategoryLib.load();
