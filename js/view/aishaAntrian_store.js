jun.AishaAntrianstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.AishaAntrianstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AishaAntrianStoreId',
            url: 'AishaAntrian',
            root: 'results',
            idProperty: 'id_antrian',
            totalProperty: 'total',
            fields: [
                {name: 'id_antrian'},
                {name: 'nomor_pasien'},
                {name: 'nomor_antrian'},
                {name: 'bagian'},
                {name: 'counter'},
                {name: 'tanggal'},
                {name: 'timestamp'},
                {name: 'pending'},
                {name: 'salestrans_id'},
                {name: 'doc_ref'},
                {name: 'tgl'},
                {name: 'nama_customer'},
                {name: 'no_customer'},
                {name: 'tdate'},
                {name: 'end_at'},
                {name: 'end_'},
                {name: 'note'},
                {name: 'counter_asal'},
                {name: 'counter_cashier'}

            ]
        }, cfg));
    }
});
jun.rztAishaAntrian = new jun.AishaAntrianstore();
jun.rztAntrianFO = new jun.AishaAntrianstore({
    url: 'AishaAntrian/AntrianCounter',
    // baseParams: {bagian: "counter", f: "cmp"},
    method: 'POST'
});
jun.rztAntrianFOPending = new jun.AishaAntrianstore({
    url: 'AishaAntrian/AntrianPendingCounter',
    baseParams: {bagian: "counter", f: "cmp"},
    method: 'POST'
});
jun.rztAntrianKasir = new jun.AishaAntrianstore({
    url: 'AishaAntrian/AntrianCounter',
    baseParams: {bagian: "kasir", f: "cmp"},
    method: 'POST'
});
jun.rztAntrianKasirPending = new jun.AishaAntrianstore({
    url: 'AishaAntrian/AntrianPendingCounter',
    baseParams: {bagian: "kasir", f: "cmp"},
    method: 'POST'
});
jun.rztAntrianDokter = new jun.AishaAntrianstore({
    url: 'AishaAntrian/AntrianCounter',
    baseParams: {bagian: "medis", f: "cmp"},
    method: 'POST'
});
//jun.rztAntrianDokterPending = new jun.AishaAntrianstore({
//    url: 'AishaAntrian/AntrianPendingDokter',
//    baseParams: {bagian: "medis", f: "cmp"},
//    method: 'POST'
//});
jun.MonitorAntrianstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.MonitorAntrianstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'MonitorAntrianStoreId',
            url: 'AishaAntrian/MonitorAntrian',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'id_antrian'},
                {name: 'nomor_pasien'},
                {name: 'nomor_antrian'},
                {name: 'tanggal'},
                {name: 'counter_'},
                {name: 'counter'},
                {name: 'daftar'},
                {name: 'konsul'},
                {name: 'kasir'},
                {name: 'alasan'},
                {name: 'note'},
                {name: 'counter_asal'}
            ]
        }, cfg));
    }
});
jun.rztAntrianBeautyTrans = new jun.AishaAntrianstore({
    url: 'BeautyServices/Antrian'
});
jun.rztAntrianPendingBeautyTrans = new jun.AishaAntrianstore({
    url: 'BeautyServices/AntrianPending'
});