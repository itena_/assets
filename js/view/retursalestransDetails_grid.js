jun.ReturSalestransDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    //title: "SalestransDetails",
    id: 'docs-jun.ReturSalestransDetailsGrid',
    //iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'paket_trans_id',
            sortable: false,
            resizable: false,
            dataIndex: 'paket_trans_id',
            width:40,
            hidden: true
        },
        {
            header: 'Pkg',
            sortable: false,
            resizable: false,
            dataIndex: 'paket_trans_id',
            width:30,
            align: "left",
            renderer: jun.renderPackageIco
        },
        {
            header: 'Items Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Items Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderBarang
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 25,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Price',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 75,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc',
            sortable: true,
            resizable: true,
            dataIndex: 'disc',
            width: 45,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'discrp',
            width: 75,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztReturSalestransDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            //forceSelection: true,
                            store: jun.rztBarangCmp,
                            hiddenName: 'barang_id',
                            valueField: 'barang_id',
                            ref: '../../barang',
                            displayField: 'kode_barang',
                            anchor: '100%'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            width: 50,
                            enableKeyEvents: true,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Price :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'priceid',
                            ref: '../../price',
                            width: 55,
                            readOnly: true,
                            enableKeyEvents: true,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'discdetilid',
                            ref: '../../disc',
                            width: 50,
                            readOnly: true,
                            enableKeyEvents: true,
                            maxLength: 5,
                            value: 0,
                            minValue: 0,
                            maxValue: 100
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc Amount :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'discadetilid',
                            ref: '../../disca',
                            width: 55,
                            readOnly: true,
                            enableKeyEvents: true,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            id: 'ketpotid',
                            ref: '../../ketpot',
                            width: 475,
                            colspan: 7,
                            maxLength: 255
                        },
                        {
                            xtype: 'hidden',
                            ref: '../../disc_name'
                        }
                    ]},
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.ReturSalestransDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.price.on('keyup', this.onDiscChange, this);
        this.qty.on('keyup', this.onDiscChange, this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.disca.on('keyup', this.onDiscChange, this);
        this.barang.on('select', this.onChangeBarang, this);
        if (SALES_OVERRIDE == "1") {
            this.disc.setReadOnly(false);
            this.disca.setReadOnly(false);
            this.price.setReadOnly(false);
        } else {
            this.disc.setReadOnly(true);
            this.disca.setReadOnly(true);
            this.price.setReadOnly(true);
            Ext.getCmp('priceid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discdetilid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discadetilid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
        }
    },
    onDiscChange: function (a,e) {

        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var bruto = round(price * qty,2);
        if (a.id == "discdetilid") {
            this.disca.setValue(round(bruto * (disc / 100),2));
        }
        else {
            var key = e.getKey();
            if(key == Ext.EventObject.TAB){
                return;
            }
            if (a.id == "discadetilid") {
                this.disc.setValue(0);
            }
        }
    },
    onStoreChange: function () {
        jun.rztReturSalestransDetails.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Item must selected");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztReturSalestransDetails.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Item already entered");
//                return
//            }
//        }
        var barang = jun.getBarang(barang_id);
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var disc1 = parseFloat(Ext.getCmp('discid').getValue());
        var ketpot = this.ketpot.getValue();
        var disc_name = this.disc_name.getValue();
        var bruto = round(price * qty,2);
        var vat = jun.getTax(barang_id);
        var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal,2);
        var totalpot = disca + discrp1;
        var total_with_disc = bruto - totalpot;
        var total = bruto - disca;
        var vatrp = round(total_with_disc * vat,2);
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.set('price', price);
            record.set('disc', disc);
            record.set('discrp', disca);
            record.set('ketpot', ketpot);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc_name', disc_name);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        } else {
            var c = jun.rztReturSalestransDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty,
                    price: price,
                    disc: disc,
                    discrp: disca,
                    ketpot: ketpot,
                    vat: vat,
                    vatrp: vatrp,
                    total_pot: totalpot,
                    bruto: bruto,
                    disc_name: disc_name,
                    total: total,
                    disc1: disc1,
                    discrp1: discrp1
                });
            jun.rztReturSalestransDetails.add(d);
        }
//        this.store.refreshData();
        this.barang.reset();
        this.qty.reset();
        this.price.reset();
        this.ketpot.reset();
        this.disc.reset();
        this.disca.reset();
    },
    onChangeBarang: function () {
        var barang_id = this.barang.getValue();
        var customer_id = Ext.getCmp("customersales_id").getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Item must selected.");
            return
        }
        if (customer_id == "" || customer_id == undefined) {
            Ext.MessageBox.alert("Error", "Customer must selected.");
            this.barang.reset();
            return
        }
        var barang = jun.getBarang(barang_id);
        //this.price.setValue(barang.data.price);
        Ext.Ajax.request({
            url: 'SalestransDetails/GetDiskon',
            method: 'POST',
            scope: this,
            params: {
                customer_id: customer_id,
                barang_id: barang_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.price.setValue(response.msg.price);
                this.disc.setValue(response.msg.value);
                this.disc_name.setValue(response.msg.nama_status);
                this.onDiscChange(this.disc);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
//            this.onChangeBarang();
            this.qty.setValue(record.data.qty);
            this.disc.setValue(record.data.disc);
            this.disca.setValue(record.data.discrp);
            this.ketpot.setValue(record.data.ketpot);
            this.price.setValue(record.data.price);
            this.disc_name.setValue(record.data.disc_name);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        var idz = selectedz.json.salestrans_details;
        var form = new jun.SalestransDetailsWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        this.store.remove(record);
    }
})
