jun.ChartMasterstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.ChartMasterstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ChartMasterStoreId',
            url: 'ChartMaster',
            autoLoad: true,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'account_code'},
                {name: 'account_name'},
                {name: 'status'},
                {name: 'description'},
                {name: 'kategori'},
                {name: 'header'}
            ]
        }, cfg));
    },
    FilterData: function () {
        this.filter([
            {
                property: "status",
                value: "1"
            }
        ])
    },
    FilterDataHeader: function () {
        this.filter([
            {
                property: "header",
                value: "1"
            }
        ])
    },
    FilterDataNotHeader: function () {
        this.filter([
            {
                property: "header",
                value: "0"
            }
        ])
    }
});
jun.rztChartMaster = new jun.ChartMasterstore();
jun.rztChartMasterHeader = new jun.ChartMasterstore({baseParams: {mode: "header"}});
jun.rztChartMasterLib = new jun.ChartMasterstore();
jun.rztChartMasterCmp = new jun.ChartMasterstore({baseParams: {mode: "detil"}});
jun.rztChartMasterCmpHutang = new jun.ChartMasterstore({baseParams: {mode: "hutang"}});
jun.rztChartMasterCmpBank = new jun.ChartMasterstore({baseParams: {mode: "bank"}});
jun.rztChartMasterCmpBiaya = new jun.ChartMasterstore({baseParams: {mode: "biaya"}});
jun.rztChartMasterCmpPendapatan = new jun.ChartMasterstore({baseParams: {mode: "pendapatan"}});
jun.rztChartMasterCmpSales = new jun.ChartMasterstore({baseParams: {mode: "coa_sales_grup"}});
jun.rztChartMasterCmpPurchase = new jun.ChartMasterstore({baseParams: {mode: "coa_purchase_grup"}});
jun.rztChartMasterCmpHPP = new jun.ChartMasterstore({baseParams: {mode: "coa_hpp_grup"}});
jun.rztChartMasterLib.load();
