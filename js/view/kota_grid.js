jun.KotaGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "City",
    id: 'docs-jun.KotaGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'City Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_kota',
            width: 100,
			filter: {xtype: "textfield"}
        },
        {
            header: 'Province Name',
            sortable: true,
            resizable: true,
            dataIndex: 'provinsi_id',
            renderer: jun.renderProvinsi,
            width: 100
        }
    ],
    initComponent: function () {
        jun.rztProvinsiLib.on({
            scope: this,
            load: {
                fn: function (a, b, c) {
                    if (jun.rztProvinsiCmp.getTotalCount() === 0) {
                        jun.rztProvinsiCmp.add(b);
                    }
                    this.store.baseParams = {mode: "grid"};
                    this.store.reload();
                    this.store.baseParams = {};
                }
            }
        });
        if (jun.rztProvinsiLib.getTotalCount() === 0) {
            jun.rztProvinsiLib.load();
        }
        this.store = jun.rztKota;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }
            ]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add City',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit City',
                    ref: '../btnEdit'
                }
            ]
        };
        jun.KotaGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KotaWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a city");
            return;
        }
        var idz = selectedz.json.kota_id;
        var form = new jun.KotaWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Kota/delete/id/' + record.json.kota_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztKota.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
