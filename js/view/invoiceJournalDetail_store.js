jun.InvoiceJournalDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.InvoiceJournalDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'InvoiceJournalDetailStoreId',
            url: 'InvoiceJournalDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'invoice_journal_detail_id'},
                {name: 'invoice_journal_id'},
                {name: 'store_kode'},
                {name: 'account_code'},
                {name: 'debit', type:'float'},
                {name: 'kredit', type:'float'},
//                {name: 'grup'},
                {name: 'memo_'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        var total = this.sum("debit") - this.sum("kredit");
//        console.log(total);
        Ext.getCmp("tot_debit_id").setValue(this.sum("debit"));
        Ext.getCmp("tot_kredit_id").setValue(this.sum("kredit"));
        Ext.getCmp("hutangBalance").setValue(total);
    }
});
jun.rztInvoiceJournalDetail = new jun.InvoiceJournalDetailstore();
//jun.rztInvoiceJournalDetail.load();
