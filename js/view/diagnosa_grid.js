jun.DiagnosaGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Diagnosa",
    id: 'docs-jun.DiagnosaGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Diagnosa',
            dataIndex: 'diag',
            width: 100
        },
        {
            header: 'Note',
            dataIndex: 'note_',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztDiagnosa;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 2,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Diagnosa :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../diag',
                            style: 'margin-bottom:2px',
                            width: 300,
                            maxLength: 600
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../note_',
                            width: 300,
                            maxLength: 600
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            width: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            width: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            width: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.DiagnosaGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var diag = this.diag.getValue();
        var note_ = this.note_.getValue();
        if (diag == "" || note_ == undefined) {
            Ext.MessageBox.alert("Error", "Diagnosa dan keterangan harus diisi.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('diag', diag);
            record.set('note_', note_);
            record.commit();
        } else {
            var c = jun.rztDiagnosa.recordType,
                d = new c({
                    diag: diag,
                    note_: note_
                });
            jun.rztDiagnosa.add(d);
        }
        this.diag.reset();
        this.note_.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.diag.setValue(record.data.diag);
            this.note_.setValue(record.data.note_);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});
