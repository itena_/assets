jun.AssetPeriodeWin = Ext.extend(Ext.Window, {
    title: 'AssetPeriode',
    modez:1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetPeriode',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'asset_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'asset_id',
                                    id:'asset_idid',
                                    ref:'../asset_id',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'asset_group_id',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'asset_group_id',
                                    id:'asset_group_idid',
                                    ref:'../asset_group_id',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'docref',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'docref',
                                    id:'docrefid',
                                    ref:'../docref',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'docref_other',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'docref_other',
                                    id:'docref_otherid',
                                    ref:'../docref_other',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'asset_trans_name',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'asset_trans_name',
                                    id:'asset_trans_nameid',
                                    ref:'../asset_trans_name',
                                    maxLength: 100,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'asset_trans_branch',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'asset_trans_branch',
                                    id:'asset_trans_branchid',
                                    ref:'../asset_trans_branch',
                                    maxLength: 100,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'asset_trans_price',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'asset_trans_price',
                                    id:'asset_trans_priceid',
                                    ref:'../asset_trans_price',
                                    maxLength: 100,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'asset_trans_new_price',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'asset_trans_new_price',
                                    id:'asset_trans_new_priceid',
                                    ref:'../asset_trans_new_price',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'asset_trans_date',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'asset_trans_date',
                                    id:'asset_trans_dateid',
                                    ref:'../asset_trans_date',
                                    maxLength: 100,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                            xtype: 'textfield',
                            fieldLabel: 'description',
                            hideLabel:false,
                            //hidden:true,
                            name:'description',
                            id:'descriptionid',
                            ref:'../description',
                            anchor: '100%'
                            //allowBlank: 1
                        }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'class',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'class',
                                    id:'classid',
                                    ref:'../class',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'period',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'period',
                                    id:'periodid',
                                    ref:'../period',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'tariff',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'tariff',
                                    id:'tariffid',
                                    ref:'../tariff',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'tglpenyusutan',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'tglpenyusutan',
                                    id:'tglpenyusutanid',
                                    ref:'../tglpenyusutan',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'penyusutanperbulan',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'penyusutanperbulan',
                                    id:'penyusutanperbulanid',
                                    ref:'../penyusutanperbulan',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'penyusutanpertahun',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'penyusutanpertahun',
                                    id:'penyusutanpertahunid',
                                    ref:'../penyusutanpertahun',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'status',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'status',
                                    id:'statusid',
                                    ref:'../status',
                                    maxLength: 1,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetPeriodeWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'asset/AssetPeriode/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'asset/AssetPeriode/create/';
                }
             
            Ext.getCmp('form-AssetPeriode').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztAssetPeriode.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-AssetPeriode').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});