jun.TransferBarangPerlengkapanDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "TransferBarangPerlengkapanDetails",
    id: 'docs-jun.TransferBarangPerlengkapanDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Barang Perlengkapan Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_perlengkapan_id',
            width: 150,
            renderer: jun.renderKodeBarangPerlengkapan
        },
        {
            header: 'Barang Perlengkapan Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_perlengkapan_id',
            width: 270,
            renderer: jun.renderBarangPerlengkapan
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 60,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Price',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 150,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Vat (%)',
            sortable: true,
            resizable: true,
            dataIndex: 'vat',
            width: 60,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'COA',
            sortable: true,
            resizable: true,
            dataIndex: 'account_code',
            width: 60,
            align: "right",
            //renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztTransferBarangPerlengkapanDetails;

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
//                            style: 'margin-bottom:2px',
                            forceSelection: true,
                            store: jun.rztBarangPerlengkapanLib,
                            hiddenName: 'barang_perlengkapan_id',
                            valueField: 'barang_perlengkapan_id',
                            ref: '../../barangPerlengkapan',
                            displayField: 'kode_brg_perlengkapan',
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">',
                                '<span style=" font-weight: bold">{kode_brg_perlengkapan}</span><br />{nama_brg_perlengkapan}',
                                "</div></tpl>"
                            )
//                            colspan: 3
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0\xA0\xA0Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0\xA0\xA0Price (Rp) :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'priceid',
                            ref: '../../price',
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0\xA0\xA0Vat (%) :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'vatid',
                            ref: '../../vat',
                            width: 50,
                            minValue: 0,
                            //readOnly:true,
                            value:0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: '\xA0\xA0COA :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            forceSelection: true,
                            fieldLabel: 'COA',
                            //store: this.mode,
                            store: jun.rztChartMasterCmp,
                            ref: '../../account_code',
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                                "</div></tpl>"),
                            listWidth: 500,
                            hiddenName: 'account_code',
                            valueField: 'account_code',
                            displayField: 'account_code',
                            width: 100
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 5,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            ref: '../../btnEdit'
                        }
//                        {
//                            xtype: 'tbseparator'
//                        }
                        //{
                        //    xtype: 'button',
                        //    text: 'Hapus',
                        //    ref: '../../btnDelete'
                        //}
                    ]
                }
            ]
        };

        jun.TransferBarangPerlengkapanDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        //this.barangPerlengkapan.on('Change', this.onItemChange, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);

        this.store.on('add', this.onStoreChange, this);
        this.store.on('update', this.onStoreChange, this);
        this.store.on('remove', this.onStoreChange, this);
    },
    // onItemChange: function () {
    //     var barang_perlengkapan_id = this.barangPerlengkapan.getValue();
    //     var barang = jun.getBarangPerlengkapan(barang_perlengkapan_id);
    //     this.sat.setText(barang.data.sat);
    // },

    onStoreChange: function () {
        jun.rztTransferBarangPerlengkapanDetails.refreshTotal();
    },

    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_perlengkapan_id = this.barangPerlengkapan.getValue();
        if (barang_perlengkapan_id == "") {
            Ext.MessageBox.alert("Error", "You have not selected an item");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
        if (this.btnEdit.text != 'Save') {
            var a = jun.rztTransferBarangPerlengkapanDetails.findExact("barang_perlengkapan_id", barang_perlengkapan_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Item already added");
                return
            }
        }
        var qty = parseFloat(this.qty.getValue());
        var price = parseFloat(this.price.getValue());
        var vat = parseFloat(this.vat.getValue());
        var bruto = price*qty;
        var vatrp = vat/100*price*qty;
        var total = bruto+vatrp;
        account_code=this.account_code.getValue();

        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_perlengkapan_id', barang_perlengkapan_id);
            record.set('qty', qty);
            record.set('price', price);
            record.set('vat', vat);
            record.set('account_code',account_code);
            record.set('bruto', bruto);
            record.set('vatrp', vatrp);
            record.set('total', total);
            record.commit();
        } else {
            var c = jun.rztTransferBarangPerlengkapanDetails.recordType,
                d = new c({
                    barang_perlengkapan_id: barang_perlengkapan_id,
                    qty: qty,
                    price: price,
                    vat: vat,
                    account_code:account_code,
                    bruto: bruto,
                    vatrp: vatrp,
                    total: total
                });
            jun.rztTransferBarangPerlengkapanDetails.add(d);
        }

        this.barangPerlengkapan.reset();
        this.qty.reset();
        this.price.reset();
        this.vat.reset();
        this.account_code.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected an item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barangPerlengkapan.setValue(record.data.barang_perlengkapan_id);
            this.qty.setValue(record.data.qty);
            this.price.setValue(record.data.price);
            this.vat.setValue(record.data.vat);
            this.account_code.setValue(record.data.account_code);

            btn.setText("Save");
            this.btnDisable(true);
            //this.onItemChange();
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected an item");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected an item");
            return;
        }
        this.store.remove(record);
    }
})

jun.TransferBarangPerlengkapanOutDetailsGrid = Ext.extend(Ext.grid.GridPanel,{
    title: "Transfer Out Barang Perlengkapan Details",
    id: 'docs-jun.TransferBarangPerlengkapanOutDetailsGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Barang Perlengkapan Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_perlengkapan_id',
            width: 160,
            renderer: jun.renderKodeBarangPerlengkapan
        },
        {
            header: 'Barang Perlengkapan Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_perlengkapan_id',
            width: 270,
            renderer: jun.renderBarangPerlengkapan
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 70,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztTransferOutBarangPerlengkapanDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Barang :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
//                            style: 'margin-bottom:2px',
                            forceSelection: true,
                            store: jun.rztBarangPerlengkapanLib,
                            hiddenName: 'barang_perlengkapan_id',
                            valueField: 'barang_perlengkapan_id',
                            ref: '../../barang',
                            displayField: 'kode_brg_perlengkapan',
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">',
                                '<span style=" font-weight: bold">{kode_brg_perlengkapan}</span><br />{nama_brg_perlengkapan}',
                                "</div></tpl>"
                            )
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            width: 50,
                            value: 1,
                            minValue: 0
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 5,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
//                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
//                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'tbseparator'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
//                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.TransferBarangPerlengkapanOutDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.barang.on('Change', this.onItemChange, this);
//        this.disc.on('keyup', this.onDiscChange, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onItemChange: function () {
        var barang_perlengkapan_id = this.barang.getValue();
        var barang = jun.getBarang(barang_perlengkapan_id);
//        this.sat.setText(barang.data.sat);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_perlengkapan_id = this.barang.getValue();
        if (barang_perlengkapan_id == "") {
            Ext.MessageBox.alert("Error", "You have not selected a item");
            return
        }
//      kunci barang sama tidak boleh masuk dua kali
        if (this.btnEdit.text != 'Save') {
            var a = jun.rztTransferOutBarangPerlengkapanDetails.findExact("barang_perlengkapan_id", barang_perlengkapan_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Item already inputted");
                return
            }
        }
        var qty = parseFloat(this.qty.getValue());

        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_perlengkapan_id', barang_perlengkapan_id);
            record.set('qty', qty);

            record.commit();
        } else {
            var c = jun.rztTransferOutBarangPerlengkapanDetails.recordType,
                d = new c({
                    barang_perlengkapan_id: barang_perlengkapan_id,
                    qty: qty

                });
            jun.rztTransferOutBarangPerlengkapanDetails.add(d);
        }
       // this.store.reset();
        this.barang.reset();
        this.qty.reset();

    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_perlengkapan_id);
            this.qty.setValue(record.data.qty);
            btn.setText("Save");
            this.btnDisable(true);
            this.onItemChange();
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }

});