jun.AssetBarangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetBarangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetBarangStoreId',
            url: 'asset/AssetBarang',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'barang_id'},
{name:'kode_barang'},
{name:'nama_barang'},
{name:'ket'},
{name:'grup_id'},
{name:'active'},
{name:'sat'},
{name:'up'},
{name:'tipe_barang_id'},
{name:'barcode'},
                
            ]
        }, cfg));
    }
});
jun.rztAssetBarang = new jun.AssetBarangstore();
//jun.rztAssetBarang.load();
jun.rztAssetBarangLib = new jun.AssetBarangstore();
jun.rztAssetBarangLib.load();