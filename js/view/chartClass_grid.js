jun.ChartClassGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Chart Class",
    id: 'docs-jun.ChartClassGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Class Name',
            sortable: true,
            resizable: true,
            dataIndex: 'class_name',
            width: 100
        },
        {
            header: 'Class Type',
            sortable: true,
            resizable: true,
            dataIndex: 'ctype',
            width: 100,
            renderer: jun.renderChartClassType
        }//,
        //{
        //    header: 'inactive',
        //    sortable: true,
        //    resizable: true,
        //    dataIndex: 'inactive',
        //    width: 100
        //},
    ],
    initComponent: function () {
        this.store = jun.rztChartClass;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                //{
                //    xtype: 'button',
                //    text: 'Tambah',
                //    ref: '../btnAdd'
                //},
                //{
                //    xtype: 'tbseparator'
                //},
                {
                    xtype: 'button',
                    text: 'Edit Chart Class',
                    ref: '../btnEdit'
                }
                //{
                //    xtype: 'tbseparator'
                //},
                //{
                //    xtype: 'button',
                //    text: 'Hapus',
                //    ref: '../btnDelete'
                //}
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.ChartClassGrid.superclass.initComponent.call(this);
        //this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.ChartClassWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.cid;
        var form = new jun.ChartClassWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'ChartClass/delete/id/' + record.json.cid,
            method: 'POST',
            success: function (f, a) {
                jun.rztChartClass.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.DesignLabaRugiGrid = Ext.extend(Ext.ux.tree.TreeGrid, {
    title: "Chart of Account",
    enableDD: false,
    enableSort: false,
    id: 'docs-jun.DesignLabaRugiGrid',
    iconCls: "silk-grid",
    // dataUrl: '',
    loader: new Ext.tree.TreeLoader({dataUrl: 'ChartClass/LabaRugi/'}),
    columns: [
        {
            header: 'Account Name',
            dataIndex: 'account_name',
            width: 400
        },
        {
            header: 'Account Code',
            dataIndex: 'account_code',
            width: 100
        },
        {
            header: 'Hidden',
            dataIndex: 'hide',
            width: 100
        },
        {
            header: 'Tipe',
            dataIndex: 'node_name',
            width: 100
        },
        {
            header: 'Saldo Normal',
            dataIndex: 'saldo_normal',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztChartTypesCmp.getTotalCount() === 0) {
            jun.rztChartTypesCmp.load();
        }
        if (jun.rztChartClassCmp.getTotalCount() === 0) {
            jun.rztChartClassCmp.load();
        }
        if (jun.rztChartClassLib.getTotalCount() === 0) {
            jun.rztChartClassLib.load();
        }
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Chart Type',
                    ref: '../btnAddT'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Add COA',
                    ref: '../btnAddM'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Chart',
                    ref: '../btnEditC'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Delete Chart',
                    ref: '../btnDelete'
                },
                {
                    xtype: 'tbseparator'
                },
                new Ext.form.ComboBox({
                    store: new Ext.data.ArrayStore({
                        fields: ["code", "label"],
                        data: [
                            ['L', 'Laba Rugi'],
                            ['A', 'Activa'],
                            ['P', 'Pasiva']
                        ]
                    }),
                    ref: '../cmbTipe',
                    editable: false,
                    id: 'cmdgridcoa',
                    typeAhead: false,
                    valueField: 'code',
                    displayField: 'label',
                    mode: 'local',
                    value: 'L',
                    triggerAction: 'all',
                    selectOnFocus: true,
                    width: 135
                })
            ]
        };
        jun.DesignLabaRugiGrid.superclass.initComponent.call(this);
        this.getLoader().on('load', this.restoreScroll, this);
        this.btnEditC.on('Click', this.loadEditFormC, this);
        this.btnAddT.on('Click', this.loadFormT, this);
        this.btnAddM.on('Click', this.loadFormM, this);
        this.cmbTipe.on('select', this.refreshCmbTipe, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getLoader().on({
            scope: this,
            beforeload: {
                fn: function (t, n, c) {
                    t.baseParams = {
                        'tipe': Ext.getCmp('cmdgridcoa').getValue()
                    }
                }
            }
        });
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var node = this.getSelectionModel().getSelectedNode();
        if (node.attributes.node_type == 'C') {
            Ext.MessageBox.alert("Warning", "Chart Class tidak bisa di delete.");
            return;
        }
        if (node.attributes.node_type == 'T' && node.hasChildNodes()) {
            Ext.MessageBox.alert("Warning", "Chart type tidak bisa di hapus karena masih punya child.");
            return;
        }
        var urlz;
        if (node.attributes.node_type == 'T') {
            urlz = 'ChartTypes/delete/id/' + node.attributes.id;
        } else {
            urlz = 'ChartMaster/delete/id/' + node.attributes.id;
        }
        Ext.Ajax.request({
            url: urlz,
            method: 'POST',
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (!response.success) {
                    Ext.Msg.alert('Failure', response.msg);
                    return;
                }
                jun.rztChartTypes.reload();
                jun.rztChartTypesCmp.reload();
                jun.rztChartTypesLib.reload();
                jun.rztChartMaster.reload();
                jun.rztChartMasterHeader.reload();
                jun.rztChartMasterLib.reload();
                jun.rztChartMasterCmp.reload();
                jun.rztChartMasterCmpHutang.reload();
                jun.rztChartMasterCmpBank.reload();
                jun.rztChartMasterCmpBiaya.reload();
                jun.rztChartMasterCmpPendapatan.reload();
                Ext.getCmp('docs-jun.DesignLabaRugiGrid').getRootNode().reload();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    deleteRec: function () {
        var node = this.getSelectionModel().getSelectedNode();
        if (node == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Chart");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus chart ini?', this.deleteRecYes, this);
    },
    loadFormT: function () {
        var node = this.getSelectionModel().getSelectedNode();
        if (node == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Chart");
            return;
        }
        if (node.attributes.node_type == 'M') {
            Ext.MessageBox.alert("Warning", "Silahkan pilih terlebih dulu chart type atau chart class.");
            return;
        }
        var form = new jun.ChartTypesWin({modez: 0});
        form.show(this);
        form.class_id.setValue(node.attributes.class_id);
        if (node.attributes.node_type == 'T') {
            form.parent.setValue(node.attributes.id);
        }
    },
    loadFormM: function () {
        var node = this.getSelectionModel().getSelectedNode();
        if (node == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Chart");
            return;
        }
        if (node.attributes.node_type != 'T') {
            Ext.MessageBox.alert("Warning", "Silahkan pilih terlebih dulu chart type.");
            return;
        }
        var form = new jun.ChartMasterWin({modez: 0});
        form.show(this);
        form.kategori.setValue(node.attributes.id);
        if (Ext.getCmp('cmdgridcoa').getValue() != 'L') {
            form.tipe.setValue('N');
        } else {
            form.tipe.setValue('L');
        }
    },
    refreshCmbTipe: function (c, r, i) {
        this.getRootNode().reload();
    },
    loadEditFormC: function () {
        var node = this.getSelectionModel().getSelectedNode();
        if (node == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Chart");
            return;
        }
        console.log(node.attributes.node_type);
        var idz = node.attributes.id;
        switch (node.attributes.node_type) {
            case 'C':
                var form = new jun.ChartClassWin({modez: 1, id: idz});
                form.show(this);
                form.class_name.setValue(node.attributes.account_name);
                break;
            case 'T':
                var form = new jun.ChartTypesWin({modez: 1, id: idz});
                form.show(this);
                form.name.setValue(node.attributes.account_name);
                form.class_id.setValue(node.attributes.class_id);
                form.parent.setValue(node.attributes.parent);
                form.hide_.setValue(node.attributes.hide);
                form.seq.setValue(node.attributes.seq);
                break;
            case 'M':
                var form = new jun.ChartMasterWin({modez: 1, id: idz});
                form.show(this);
                form.account_code.setValue(node.attributes.account_code);
                form.account_name.setValue(node.attributes.account_name);
                form.kategori.setValue(node.attributes.kategori);
                if (Ext.getCmp('cmdgridcoa').getValue() != 'L') {
                    form.tipe.setValue('N');
                } else {
                    form.tipe.setValue('L');
                }
                form.saldo_normal.setValue(node.attributes.saldo_normal);
                break;
        }
    }
});
// jun.DesignAktivaGrid = Ext.extend(Ext.ux.tree.TreeGrid, {
//     title: "Desain Neraca Aktiva",
//     enableDD: true,
//     enableSort: false,
//     id: 'docs-jun.DesignAktivaGrid',
//     iconCls: "silk-grid",
//     dataUrl: 'ChartClass/NeracaActiva/',
//     columns: [
//         {
//             header: 'Account Name',
//             dataIndex: 'account_name',
//             width: 230
//         },
//         {
//             header: 'Account Code',
//             dataIndex: 'account_code',
//             width: 230
//         }
//     ],
//     initComponent: function () {
//         jun.DesignAktivaGrid.superclass.initComponent.call(this);
//         this.getLoader().on('load', this.restoreScroll, this);
//     }
// });
// jun.DesignPasivaGrid = Ext.extend(Ext.ux.tree.TreeGrid, {
//     title: "Desain Neraca Pasiva",
//     enableDD: true,
//     enableSort: false,
//     id: 'docs-jun.DesignPasivaGrid',
//     iconCls: "silk-grid",
//     dataUrl: 'ChartClass/NeracaPasiva/',
//     columns: [
//         {
//             header: 'Account Name',
//             dataIndex: 'account_name',
//             width: 230
//         },
//         {
//             header: 'Account Code',
//             dataIndex: 'account_code',
//             width: 230
//         }
//     ],
//     initComponent: function () {
//         jun.DesignPasivaGrid.superclass.initComponent.call(this);
//         this.getLoader().on('load', this.restoreScroll, this);
//     }
// });