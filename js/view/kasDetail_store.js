jun.KasDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.KasDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'KasDetailStoreId',
            url: 'KasDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [
                { name: 'kas_detail_id'},
                { name: 'kas_id'},
                { name: "debit", type: "float" },
                { name: "kredit", type: "float" },
                { name: 'item_name' },
                { name: 'total' },
                { name: 'account_code' },
                { name: 'store' }
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        var amount = 0;
        if(Ext.getCmp('amount_kaspusatid')){
            //HO
            amount = Ext.getCmp('amount_kaspusatid').getValue();
        }else{
            //Cabang
            amount = Ext.getCmp('arus_kasid').getValue()==1? this.sum("kredit"):this.sum("debit");
            Ext.getCmp('amount_kasid').setValue(amount);
        }
        
        if(Ext.getCmp('arus_kasid').getValue()==1){
            /*
             * Masuk
             */
            Ext.getCmp("total_debit_kasid").setValue(this.sum("debit")+amount);
            Ext.getCmp("total_kredit_kasid").setValue(this.sum("kredit"));
        }else{
            /*
             * keluar
             */
            Ext.getCmp("total_debit_kasid").setValue(this.sum("debit"));
            Ext.getCmp("total_kredit_kasid").setValue(this.sum("kredit")+amount);
        }
    }
});

jun.rztKasDetail = new jun.KasDetailstore();