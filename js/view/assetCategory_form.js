jun.AssetCategoryWin = Ext.extend(Ext.Window, {
    title: 'Asset Categories',
    modez:1,
    width: 400,
    height: 205,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetCategory',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'Category Code',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'category_code',
                                    id:'category_codeid',
                                    ref:'../category_code',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'Category Name',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'category_name',
                                    id:'category_nameid',
                                    ref:'../category_name',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textarea',
                                    fieldLabel: 'Description',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'category_desc',
                                    id:'category_descid',
                                    ref:'../category_desc',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                 /*                                    {
                                    xtype: 'textfield',
                                    fieldLabel: 'timestamp',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'timestamp',
                                    id:'timestampid',
                                    ref:'../timestamp',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, */
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetCategoryWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz = 'asset/AssetCategory/create/';
            /*if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'asset/AssetCategory/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'asset/AssetCategory/create/';
                }
             */
            Ext.getCmp('form-AssetCategory').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztAssetCategory.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-AssetCategory').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});