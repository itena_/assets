jun.BeautyServicesstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BeautyServicesstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BeautyServicesStoreId',
            url: 'BeautyServices',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'nscc_beauty_services_id'},
                {name: 'total'},
                {name: 'beauty_id'},
                {name: 'salestrans_details'},
                {name: 'final'}
            ]
        }, cfg));
    }
});
jun.rztBeautyServices = new jun.BeautyServicesstore();
//jun.rztBeautyServices.load();
jun.beautytransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.beautytransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'beautytransStoreId',
            url: 'Salestrans/BeautyService',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'salestrans_id'},
                {name: 'doc_ref'},
                {name: 'salestrans_details'},
                {name: 'barang_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'qty'},
                {name: 'beauty_id'},
                {name: 'beauty2_id'},
                {name: 'beauty3_id'},
                {name: 'beauty4_id'},
                {name: 'beauty5_id'},
                {name: 'dokter_id'},
                {name: 'jasa_dokter'},
                {name: 'nama_customer'},
                {name: 'no_customer'},
                {name: 'final'},
                {name: 'store'},
                {name: 'active'}
            ]
        }, cfg));
    }
});
jun.rztBeutytrans = new jun.beautytransstore();
jun.rztBeutytransStatus = new jun.beautytransstore({baseParams: {active: 1}});
jun.rztBeautyPerawatan = new jun.beautytransstore({
    url: 'BeautyServices'
});

jun.beautytranscombinationstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.beautytranscombinationstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'beautytransStoreId',
            url: 'Salestrans/BeautyServiceCombination',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'salestrans_id'},
                {name: 'doc_ref'},
                {name: 'salestrans_details'},
                {name: 'barang_id'},
                {name: 'kode_barang'},
                {name: 'nama_barang'},
                {name: 'qty'},
                {name: 'beauty_id'},
                {name: 'beauty2_id'},
                {name: 'beauty3_id'},
                {name: 'beauty4_id'},
                {name: 'beauty5_id'},
                {name: 'kmr'},
                {name: 'kmt'},
                {name: 'jasa_dokter'},
                {name: 'nama_customer'},
                {name: 'no_customer'},
                {name: 'final'},
                {name: 'store'},
                {name: 'active'}
            ]
        }, cfg));
    }
});
jun.rztBeutyCombinationtrans = new jun.beautytranscombinationstore();