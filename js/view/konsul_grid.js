jun.KonsulGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Konsul",
    id: 'docs-jun.KonsulGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Konsul',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref'
        },
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'no_antrian'
        },
        {
            header: 'No. Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'tdate'
        },
        {
            header: 'Nama Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'user_id'
        }
    ],
    initComponent: function () {
        this.store = jun.rztKonsul;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        // this.tbar = {
        //     xtype: 'toolbar',
        //     items: [
        //         {
        //             xtype: 'button',
        //             text: 'Tambah',
        //             ref: '../btnAdd'
        //         },
        //         {
        //             xtype: 'tbseparator'
        //         },
        //         {
        //             xtype: 'button',
        //             text: 'Ubah',
        //             ref: '../btnEdit'
        //         },
        //         {
        //             xtype: 'tbseparator'
        //         },
        //         {
        //             xtype: 'button',
        //             text: 'Hapus',
        //             ref: '../btnDelete'
        //         }
        //     ]
        // };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.KonsulGrid.superclass.initComponent.call(this);
        // this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.KonsulWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.konsul_id;
        var form = new jun.KonsulWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Konsul/delete/id/' + record.json.konsul_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztKonsul.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.KonsulCounterGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "KonsulCounter",
    id: 'docs-jun.KonsulCounterGrid',
    iconCls: "silk-grid",
    // viewConfig: {
    //     forceFit: true
    // },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Konsul',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref'
        },
        {
            header: 'No. Antrian',
            sortable: true,
            resizable: true,
            dataIndex: 'no_antrian'
        },
        {
            header: 'No. Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'tdate'
        },
        {
            header: 'Nama Pasien',
            sortable: true,
            resizable: true,
            dataIndex: 'user_id'
        }
    ],
    initComponent: function () {
        this.store = jun.rztKonsulCounter;
        jun.rztKonsulCounter.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.counter = jun.Counter;
                    if (this.sm.isLocked()) {
                        return false;
                    }
                    var pnl = Ext.getCmp('docs-jun.KonsulDetilCounterGrid');
                    if (pnl) {
                        pnl.disable();
                    }
                }
            }
        });
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Use Suspend Items',
                    ref: '../btnUse',
                    enableToggle: true
                }
            ]
        };
        jun.KonsulCounterGrid.superclass.initComponent.call(this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.btnUse.on('toggle', this.onbtnUseClick, this);
    },
    selectSuspendItems: function (id_antrian) {
        Ext.getCmp('docs-SalestransCounterWin').resetAll_();
        this.sm.unlock();
        var i = this.store.findExact('id_antrian', id_antrian);
        if (i > -1) {
            this.sm.selectRow(i);
            // this.useSuspendItems();
            this.btnUse.toggle(true);
            this.btnUse.setDisabled(true);
            jun.rztKonsulDetilCounter.load({
                params: {
                    konsul_id: this.record.data.konsul_id
                },
                callback: function () {
                    Ext.getCmp('docs-jun.KonsulDetilCounterGrid').addAllItems();
                    Ext.getCmp('docs-jun.KonsulDetilCounterGrid').setDisabled(false);
                }
            }, this);
        }
    },
    useSuspendItems: function () {
        this.sm.lock();
        var sales = Ext.getCmp('docs-SalestransCounterWin');
        sales.dokter.setValue(this.record.data.dokter_id);
        sales.dokter.setReadOnly(true);
        sales.konsul_id.setValue(this.record.data.konsul_id);
        if (this.record.data.type_ == 1) {
            sales.beauty.setValue(this.record.data.beauty_id);
        }
        sales.customer.lastQuery = null;
        jun.rztCustomersSalesCounterCmp.load({
            params: {
                customer_id: this.record.data.customer_id
            },
            callback: function (r, o, s) {
                sales.customer.setValue(this.record.data.customer_id);
                sales.customer.setReadOnly(true);
            },
            scope: this
        });
    },
    resetSuspendList: function () {
        if (this.btnUse.pressed) {
            this.btnUse.toggle(false);
        }
        this.btnUse.setDisabled(false);
        Ext.getCmp('docs-SalestransCounterWin').resetAll_();
        this.sm.unlock();
        jun.rztKonsulCounter.reload();
    },
    onbtnUseClick: function (t, p) {
        if (p) {
            var selectedz = this.sm.getSelected();
            if (selectedz == undefined) {
                t.toggle(false);
                Ext.MessageBox.alert("Warning", "Anda belum memilih suspend items");
                return;
            }
            // if (this.record.data.customer_id != '') {
            //     t.toggle(false);
            //     Ext.MessageBox.alert("Warning", "Suspend item hanya bisa digunakan saat panggil antrian kasir.");
            //     return;
            // }
            this.useSuspendItems();
            Ext.getCmp('docs-jun.KonsulDetilCounterGrid').addAllItems();
            Ext.getCmp('docs-jun.KonsulDetilCounterGrid').setDisabled(false);
        } else {
            this.resetSuspendList();
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        jun.rztKonsulDetilCounter.load({
            params: {
                konsul_id: this.record.data.konsul_id
            }
        });
    }
})
