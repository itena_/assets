jun.SalestransGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Sales Transaction",
    id: 'docs-jun.SalestransGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    height: 1024,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            filter: {xtype: "textfield", filterName: "doc_ref"}
        },
        {
            header: 'No. Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'no_customer',
            filter: {xtype: "textfield", filterName: "no_customer"}
        },
        {
            header: 'Customers Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            filter: {xtype: "textfield", filterName: "nama_customer"}
        },
        //{
        //    header: 'Date',
        //    sortable: true,
        //    resizable: true,
        //    dataIndex: 'tgl'
        //},
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            filter: {xtype: "numericfield", filterName: "total"}
        },
        {
            header: 'Store',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100,
            filter: {xtype: "textfield", filterName: "store"}
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBankLib.getTotalCount() === 0) {
            jun.rztBankLib.load();
        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztBeautyLib.getTotalCount() === 0) {
            jun.rztBeautyLib.load();
        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztCardLib.getTotalCount() === 0) {
            jun.rztCardLib.load();
        }
        if (jun.rztCardCmp.getTotalCount() === 0) {
            jun.rztCardCmp.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztPaketCmp.getTotalCount() === 0) {
            jun.rztPaketCmp.load();
        }
        if (jun.rztPaketLib.getTotalCount() === 0) {
            jun.rztPaketLib.load();
        }
        if (REFERRAL && jun.rztReferral.getTotalCount() === 0) {
            jun.rztReferral.load();
        }
        
        jun.rztSalestrans.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglsalesgridid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    //var tgl = Ext.getCmp('tglsalesgridid').getValue();
                    //b.params.tgl = tgl;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztSalestrans;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Sales',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Sales',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Comment',
                    ref: '../btnComment'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'RETURN SALES ALL ITEM',
                    ref: '../btnRetur'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Sales',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Resep',
                    ref: '../btnResep'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Stoker',
                    ref: '../btnPrintStocker'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Beauty',
                    ref: '../btnPrintBeauty'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglsalesgridid',
                    ref: '../tgl'
                }
            ]
        };
        jun.SalestransGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printSales, this);
        this.btnResep.on('Click', this.printResep, this);
        this.btnPrintStocker.on('Click', this.printStoker, this);
        this.btnPrintBeauty.on('Click', this.printBeauty2, this);
        this.btnRetur.on('Click', this.returAllRec, this);
        this.btnComment.on('Click', this.editComment, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.on("activate", this.onActive, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    onActive: function (t) {
        t.getKeyMap().addBinding({
            key: [Ext.EventObject.F2],
            //ctrl: true,
            handler: function (key, e) {
                switch (key) {
                    case Ext.EventObject.F2:
                        this.editComment();
                        break;
                }
            },
            stopEvent: true,
            scope: this
        });
    },
    editComment: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.salestrans_id;
        var form = new jun.KetDiscWin({id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    returAllRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var form = new jun.ReturnNote({modez: 0, salestrans_id: record.data.salestrans_id});
        form.show();
        //Ext.MessageBox.confirm('Question', 'Are you sure want return all item this sales?', this.returAll, this);
    },
    returAll: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var form = new jun.ReturnNote({modez: 0, salestrans_id: record.data.salestrans_id});
        form.show();
        //Ext.Ajax.request({
        //    url: 'Salestrans/returall',
        //    method: 'POST',
        //    scope: this,
        //    params: {
        //        id: record.json.salestrans_id
        //    },
        //    success: function (f, a) {
        //        var response = Ext.decode(f.responseText);
        //        if (notReady()) {
        //            return;
        //        }
        //        opencashdrawer();
        //        printHTML(PRINTER_RECEIPT, '<html><pre>' + response.msg + '</pre></html>');
        //        printHTML(PRINTER_RECEIPT, '<html><pre>' + response.msg + '</pre></html>');
        //    },
        //    failure: function (f, a) {
        //        switch (a.failureType) {
        //            case Ext.form.Action.CLIENT_INVALID:
        //                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                break;
        //            case Ext.form.Action.CONNECT_FAILURE:
        //                Ext.Msg.alert('Failure', 'Ajax communication failed');
        //                break;
        //            case Ext.form.Action.SERVER_INVALID:
        //                Ext.Msg.alert('Failure', a.result.msg);
        //        }
        //    }
        //});
    },
    printSales: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        Ext.Ajax.request({
            url: 'Salestrans/Print',
            method: 'POST',
            scope: this,
            params: {
                id: record.json.salestrans_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    return;
                }
//                 printHTML(PRINTER_RECEIPT, '<html><pre>' + response.msg + '</pre></html>');
//                 var printData = [
//                     {
//                         type: 'raw', data: chr(27) + chr(64) +
//                     chr(27) + chr(51) + chr(40) +
//                     chr(27) + chr(77) + chr(49),
//                         options: {language: 'ESCP', dotDensity: 'double'}
//                     },
//                     {type: 'raw', data: response.msg},
//                     {type: 'raw', data: chr(27) + chr(100) + chr(5), options: {language: 'ESCP', dotDensity: 'double'}},
//                     {type: 'raw', data: chr(27) + chr(105), options: {language: 'ESCP', dotDensity: 'double'}}
//                 ];
//                 print(PRINTER_RECEIPT, __openCashDrawer);
                var msg = [{type: 'raw', data: response.msg}];
                var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                print(PRINTER_RECEIPT, printData);
                // print(PRINTER_STOCKER, printData);
                // print(PRINTER_RECEIPT, printData);
                // print(PRINTER_STOCKER, printData);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    printResep: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        Ext.Ajax.request({
            url: 'Salestrans/PrintResep',
            method: 'POST',
            scope: this,
            params: {
                id: record.json.salestrans_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    return;
                }
                if (response.success !== false) {
                    var msg = [{type: 'raw', data: response.msg}];
                    var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                    print(PRINTER_STOCKER, printData);
                } else {
                    Ext.Msg.alert('Info', "Tidak ada resep untuk transaksi ini.");
                }
                // print(PRINTER_STOCKER, printData);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    printStoker: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        Ext.Ajax.request({
            url: 'Salestrans/PrintStoker',
            method: 'POST',
            scope: this,
            params: {
                id: record.json.salestrans_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (response.success !== false) {
                    if (notReady()) {
                        return;
                    }
                    var msg = [{type: 'raw', data: response.msg}];
                    var printDataResep = __printDataStocker.concat(msg, __feedPaper, __cutPaper);
                    if (PRINTER_STOCKER != 'default') {
                        print(PRINTER_STOCKER, printDataResep);
                    }
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    printBeauty2: function () {
        jun.indexPrint = 0;
        // console.log('ayam');
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        // jun.rztSalestransDetails.removeAll();
        jun.rztSalestransDetails.load({
            callback: function () {
                jun.indexMax = jun.rztSalestransDetails.getTotalCount();
                this.printBeauty();
            },
            scope: this,
            params: {
                salestrans_id: record.json.salestrans_id
            }
        });
    },
    cetakBeauty: function (response, callback) {
        if (response.success !== false) {
            var msg = [{type: 'raw', data: response.msg}];
            var printData = __printDataBeauty.concat('.', __feedReversePaper);
            print(PRINTER_BEAUTY, printData);
            var printData = __printDataBeauty.concat(msg, __feedPaper, __cutPaper);
            print(PRINTER_BEAUTY, printData);
        }
        callback();
    },
    printBeauty: function () {
        var b = jun.rztSalestransDetails.getAt(jun.indexPrint);
        if (!b) {
            return;
        }
        if (jun.indexMax <= jun.indexPrint) {
            return;
        }
        console.log(jun.indexPrint);
        console.log(b);
        var salestrans_details = b.get('salestrans_details');
        console.log(salestrans_details);
        Ext.Ajax.request({
            url: 'Salestrans/PrintBeauty',
            method: 'POST',
            scope: this,
            params: {
                id: salestrans_details,
                index: jun.indexPrint
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    return;
                }
                jun.indexPrint++;
                Ext.getCmp('docs-jun.SalestransGrid').cetakBeauty(response, Ext.getCmp('docs-jun.SalestransGrid').printBeauty);
                // print(PRINTER_STOCKER, printData);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (selectedz == undefined) {
            return;
        }
        jun.rztCustomersSalesCmp.baseParams = {
            customer_id: this.record.data.customer_id
        };
        jun.rztCustomersSalesCmp.load();
        jun.rztCustomersSalesCmp.baseParams = {};
    },
    //editForm: function () {
    //    var selectedz = this.sm.getSelected();
    //    if (selectedz == undefined) {
    //        Ext.MessageBox.alert("Warning", "You have not selected a transaction");
    //        return;
    //    }
    //    var idz = selectedz.json.salestrans_id;
    //    var form = new jun.SalestransWin({modez: 1, id: idz});
    //    form.show(this);
    //    form.formz.getForm().loadRecord(this.record);
    //    //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
    //    //jun.rztCustomersCmp.un('load', this.editForm, this);
    //    jun.rztSalestransDetails.baseParams = {
    //        salestrans_id: idz
    //    };
    //    jun.rztSalestransDetails.load();
    //    jun.rztSalestransDetails.baseParams = {};
    //    jun.rztPayment.baseParams = {
    //        salestrans_id: idz
    //    };
    //    jun.rztPayment.load();
    //    jun.rztPayment.baseParams = {};
    //},
    loadForm: function () {
        if (POSCUSTOMERREADONLY) {
            jun.rztCustomersSalesCmp.baseParams = {
                customer_id: POSCUSTOMERDEFAULT
            };
            jun.rztCustomersSalesCmp.load();
            jun.rztCustomersSalesCmp.baseParams = {};
        }
        //jun.rztCustomersCmp.un('load', this.editForm, this);
        var form = new jun.SalestransWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.salestrans_id;
        var form = new jun.SalestransWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        //jun.rztCustomersCmp.un('load', this.editForm, this);
        jun.rztSalestransDetails.baseParams = {
            salestrans_id: idz
        };
        jun.rztSalestransDetails.load();
        jun.rztSalestransDetails.baseParams = {};
        jun.rztPayment.baseParams = {
            salestrans_id: idz
        };
        jun.rztPayment.load();
        jun.rztPayment.baseParams = {};
        jun.rztPaketTrans.baseParams = {
            salestrans_id: idz
        };
        jun.rztPaketTrans.load();
        jun.rztPaketTrans.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'Salestrans/delete/id/' + record.json.salestrans_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztSalestrans.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.HistoryGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "History",
    id: 'docs-jun.SalestransGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            filter: {xtype: "textfield", filterName: "doc_ref"}
        },
        {
            header: 'No. Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'no_customer',
            filter: {xtype: "textfield", filterName: "no_customer"}
        },
        {
            header: 'Customers Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            filter: {xtype: "textfield", filterName: "nama_customer"}
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            filter: {xtype: "textfield", filterName: "total"}
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBankTransCmp.getTotalCount() === 0) {
            jun.rztBankTransCmp.load();
        }
        if (jun.rztBeautyLib.getTotalCount() === 0) {
            jun.rztBeautyLib.load();
        }
        if (jun.rztBeautyCmp.getTotalCount() === 0) {
            jun.rztBeautyCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        jun.rztHistory.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    //b.params.tgl = Ext.getCmp('tglhistorygridid').getValue();
                    var tgl = Ext.getCmp('tglhistorygridid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztHistory;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add History',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View History',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    id: 'tglhistorygridid',
                    ref: '../tgl'
                }
            ]
        };
        jun.HistoryGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    editForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.salestrans_id;
        var form = new jun.HistoryWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztCustomersCmp.un('load', this.editForm, this);
    },
    loadForm: function () {
        jun.rztCustomersCmp.un('load', this.editForm, this);
        var form = new jun.HistoryWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.salestrans_id;
        jun.rztCustomersCmp.on('load', this.editForm, this);
        jun.rztCustomersCmp.baseParams = {
            customer_id: selectedz.json.customer_id
        };
        jun.rztCustomersCmp.reload();
        jun.rztCustomersCmp.baseParams = {};
        jun.rztSalestransDetails.baseParams = {
            salestrans_id: idz
        };
        jun.rztSalestransDetails.load();
        jun.rztSalestransDetails.baseParams = {};
    }
});
jun.ReturnNote = new Ext.extend(Ext.Window, {
    width: 390,
    height: 200,
    layout: "form",
    modal: !0,
    closable: !1,
    resizable: !1,
    plain: !0,
    border: !1,
    salestrans_id: null,
    title: "Retur Note",
    padding: 5,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReturnNote",
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                plain: !0,
                items: [
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Alasan Retur',
                        hideLabel: false,
                        //hidden:true,
                        name: 'retur_note',
                        id: 'retur_noteid',
                        ref: '../retur_note',
                        enableKeyEvents: true,
                        maxLength: 600,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        anchor: '100%',
                        allowBlank: false
                    },
                    {
                        xtype: "textfield",
                        hideLabel: !1,
                        id: "usernameid",
                        ref: "../username",
                        maxLength: 128,
                        anchor: "100%",
                        fieldLabel: "Username",
                        name: "loginUsername",
                        allowBlank: !1
                    },
                    {
                        xtype: "textfield",
                        hideLabel: !1,
                        id: "passwordid",
                        ref: "../password",
                        maxLength: 128,
                        anchor: "100%",
                        fieldLabel: "Password",
                        name: "loginPassword",
                        inputType: "password",
                        allowBlank: !1
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Return Sales All Item",
                    hidden: !1,
                    ref: "../btnReturnNote"
                },
                {
                    xtype: "button",
                    text: "Cancel",
                    hidden: !1,
                    ref: "../btnExit"
                }
            ]
        };
        jun.ReturnNote.superclass.initComponent.call(this);
        this.btnReturnNote.on("click", this.onbtnReturnNoteClick, this);
        this.btnExit.on("click", this.onbtnExitClick, this);
    },
    btnDisabled: function (status) {
        this.btnReturnNote.setDisabled(status);
    },
    onbtnExitClick: function () {
        this.close();
    },
    onbtnReturnNoteClick: function () {
        this.btnDisabled(true);
        var username = this.username.getValue();
        var password = this.password.getValue();
        if (username.trim() == "" || password.trim() == "") {
            Ext.Msg.alert("Warning!", "Username dan password tidak boleh kosong!!!");
            return;
        }
        var a = Ext.getCmp("passwordid").getValue();
        a = jun.EncryptPass(a);
        Ext.getCmp("passwordid").setValue(a);
        Ext.Ajax.request({
            url: 'Salestrans/returall',
            method: 'POST',
            scope: this,
            params: {
                salestrans_id: this.salestrans_id,
                username: username,
                password: a
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success == false) {
                    Ext.Msg.alert('Failure', response.msg);
                } else {
                    if (notReady()) {
                        this.close();
                    }
                    // opencashdrawer();
                    // printHTML(PRINTER_RECEIPT, '<html><pre>' + response.msg + '</pre></html>');
                    // printHTML(PRINTER_RECEIPT, '<html><pre>' + response.msg + '</pre></html>');
                    var msg = [{type: 'raw', data: response.msg}];
                    var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                    print(PRINTER_RECEIPT, printData);
                    print(PRINTER_RECEIPT, printData);
                    this.close();
                }
                this.btnDisabled(false);
            },
            failure: function (f, a) {
                this.btnDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.SalestransCounterGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Sales Transaction",
    id: 'docs-jun.SalestransCounterGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    height: 1024,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'No. Receipt',
            sortable: true,
            resizable: true,
            dataIndex: 'salestrans_id',
            filter: {xtype: "textfield", filterName: "doc_ref"},
            renderer: function (v, m, r) {                
                var docref = r.get('doc_ref');
                if (r.get('parent_id')) {
                    m.style = "background-color: #f44268; border: solid 1px #A5A5A5";
                } else {
                    m.style = "background-color: #99ff99; border: solid 1px #A5A5A5";
                }
                return docref;
            }
        },
        {
            header: 'No. Customers',
            sortable: true,
            resizable: true,
            dataIndex: 'no_customer',
            filter: {xtype: "textfield", filterName: "no_customer"}
        },
        {
            header: 'Customers Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_customer',
            filter: {xtype: "textfield", filterName: "nama_customer"}
        },
        //{
        //    header: 'Date',
        //    sortable: true,
        //    resizable: true,
        //    dataIndex: 'tgl'
        //},
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            filter: {xtype: "numericfield", filterName: "total"}
        },
        {
            header: 'Store',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100,
            filter: {xtype: "textfield", filterName: "store"}
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztBankLib.getTotalCount() === 0) {
            jun.rztBankLib.load();
        }
        if (jun.rztBankCmp.getTotalCount() === 0) {
            jun.rztBankCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztCardLib.getTotalCount() === 0) {
            jun.rztCardLib.load();
        }
        if (jun.rztCardCmp.getTotalCount() === 0) {
            jun.rztCardCmp.load();
        }
        if (jun.rztDokterCmp.getTotalCount() === 0) {
            jun.rztDokterCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztPaketCmp.getTotalCount() === 0) {
            jun.rztPaketCmp.load();
        }
        if (jun.rztPaketLib.getTotalCount() === 0) {
            jun.rztPaketLib.load();
        }
//        if (jun.rztSalestransList.getTotalCount() === 0) {
//            jun.rztSalestransList.load();
//        }
        jun.rztSalestransList.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.counter = jun.Counter;
                    //var tgl = Ext.getCmp('tglsalesgridid').getValue();
                    //b.params.tgl = tgl;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztSalestransList;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Print Sales',
                    ref: '../btnPrint'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Stoker',
                    ref: '../btnPrintStocker'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Print Beauty',
                    ref: '../btnPrintBeauty'
                }
            ]
        };
        jun.SalestransCounterGrid.superclass.initComponent.call(this);
        // this.btnAdd.on('Click', this.loadForm, this);
        // this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnPrint.on('Click', this.printSales, this);
        this.btnPrintStocker.on('Click', this.printStoker, this);
        this.btnPrintBeauty.on('Click', this.printBeauty2, this);
        // this.btnRetur.on('Click', this.returAllRec, this);
        // this.btnComment.on('Click', this.editComment, this);
        // this.tgl.on('select', this.refreshTgl, this);
        this.on("activate", this.onActive, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    printSales: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        Ext.Ajax.request({
            url: 'Salestrans/Print',
            method: 'POST',
            scope: this,
            params: {
                id: record.json.salestrans_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    return;
                }
                var msg = [{type: 'raw', data: response.msg}];
                var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                print(PRINTER_RECEIPT, printData);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    printStoker: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        Ext.Ajax.request({
            url: 'Salestrans/PrintStoker',
            method: 'POST',
            scope: this,
            params: {
                id: record.json.salestrans_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    return;
                }
                var msg = [{type: 'raw', data: response.msg}];
                var printData = __printDataStocker.concat(msg, __feedPaper, __cutPaper);
                print(PRINTER_STOCKER, printData);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    printBeauty2: function () {
        jun.indexPrint = 0;
        console.log('ayam');
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        // jun.rztSalestransDetails.removeAll();
        jun.rztSalestransDetails.load({
            callback: function () {
                jun.indexMax = jun.rztSalestransDetails.getTotalCount();
                this.printBeauty();
            },
            scope: this,
            params: {
                salestrans_id: record.json.salestrans_id
            }
        });
    },
    cetakBeauty: function (response, callback) {
        if (response.success !== false) {
            var msg = [{type: 'raw', data: response.msg}];
            var printData = __printDataBeauty.concat(msg, __feedPaper, __cutPaper);
            print(PRINTER_BEAUTY, printData);
        }
        callback();
    },
    printBeauty: function () {
        var b = jun.rztSalestransDetails.getAt(jun.indexPrint);
        if (!b) {
            return;
        }
        if (jun.indexMax <= jun.indexPrint) {
            return;
        }
        console.log(jun.indexPrint);
        console.log(b);
        var salestrans_details = b.get('salestrans_details');
        console.log(salestrans_details);
        Ext.Ajax.request({
            url: 'Salestrans/PrintBeauty',
            method: 'POST',
            scope: this,
            params: {
                id: salestrans_details,
                index: jun.indexPrint
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    return;
                }
                jun.indexPrint++;
                Ext.getCmp('docs-jun.SalestransCounterGrid').cetakBeauty(response, Ext.getCmp('docs-jun.SalestransCounterGrid').printBeauty);
                // print(PRINTER_STOCKER, printData);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (selectedz == undefined) {
            return;
        }
        jun.rztCustomersSalesCmp.baseParams = {
            customer_id: this.record.data.customer_id
        };
        jun.rztCustomersSalesCmp.load();
        jun.rztCustomersSalesCmp.baseParams = {};
    },
    loadForm: function () {
        if (POSCUSTOMERREADONLY) {
            jun.rztCustomersSalesCmp.baseParams = {
                customer_id: POSCUSTOMERDEFAULT
            };
            jun.rztCustomersSalesCmp.load();
            jun.rztCustomersSalesCmp.baseParams = {};
        }
        //jun.rztCustomersCmp.un('load', this.editForm, this);
        var form = new jun.SalestransWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.salestrans_id;
        var form = new jun.SalestransWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        //jun.rztCustomersCmp.un('load', this.editForm, this);
        jun.rztSalestransDetails.baseParams = {
            salestrans_id: idz
        };
        jun.rztSalestransDetails.load();
        jun.rztSalestransDetails.baseParams = {};
        jun.rztPayment.baseParams = {
            salestrans_id: idz
        };
        jun.rztPayment.load();
        jun.rztPayment.baseParams = {};
        jun.rztPaketTrans.baseParams = {
            salestrans_id: idz
        };
        jun.rztPaketTrans.load();
        jun.rztPaketTrans.baseParams = {};
    }
});
jun.SalestransCounterGridWin = Ext.extend(Ext.Window, {
    title: 'Sales List',
    modez: 1,
    width: 1000,
    height: 500,
    layout: 'fit',
    modal: true,
    // padding: 5,
    closeForm: true,
    initComponent: function () {
        this.items = [
            new jun.SalestransCounterGrid({
                frameHeader: !1,
                header: !1
            })
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Load',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SalestransCounterGridWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
    },
    onbtnSaveclick: function () {
        var selectedz = Ext.getCmp('docs-jun.SalestransCounterGrid').sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        jun.rztCustomersSalesCounterCmp.load({
            params: {
                customer_id: selectedz.data.customer_id
            },
            callback: function () {
                Ext.getCmp('docs-SalestransCounterWin').loadSalestrans(selectedz);
            }
        });
        this.close();
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.indexPrint = 0;
jun.indexMax = 0;