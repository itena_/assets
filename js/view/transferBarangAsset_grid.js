jun.TransferBarangAssetGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Purchase Barang Asset",
    id: 'docs-jun.TransferBarangAssetGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 60
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 70
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 160
        },
        {
            header:'Branch',
            sortable:true,
            resizable:true,
            dataIndex:'store',
            width:100,
            renderer: jun.renderStore
        },
    ],
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) jun.rztStoreCmp.load();
        if (jun.rztBarangAssetLib.getTotalCount() === 0) jun.rztBarangAssetLib.load();
        if (jun.rztSupplierCmp.getTotalCount() === 0) jun.rztSupplierCmp.load();
        if (jun.rztStoreLib.getTotalCount() === 0) jun.rztStoreLib.load();
        
        jun.rztTransferBarangAsset.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    //b.params.tgl = Ext.getCmp('tgltransferBarangAssetgrid').getValue();
                    var tgl = Ext.getCmp('tgltransferbarangassetgrid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztTransferBarangAsset;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Purchase',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Purchase',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tgltransferbarangassetgrid'
                }
            ]
        };
        jun.TransferBarangAssetGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },

    loadForm: function () {
        var form = new jun.TransferBarangAssetWin({modez: 0});
        form.show();
    },

    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected any Purchase Data");
            return;
        }
        var idz = selectedz.json.transfer_asset_id;
        var form = new jun.TransferBarangAssetWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztTransferBarangAssetDetails.proxy.setUrl('TransferBarangAssetDetails/IndexIn');
        jun.rztTransferBarangAssetDetails.baseParams = {
            transfer_asset_id: idz
        };
        jun.rztTransferBarangAssetDetails.load();
        jun.rztTransferBarangAssetDetails.baseParams = {};
    }
})
