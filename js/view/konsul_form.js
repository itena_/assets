jun.KonsulGridWin = Ext.extend(Ext.Panel, {
    title: "Konsul",
    id: 'docs-jun.KonsulGridWin',
    layout: 'border',
    autoScroll: true,
    initComponent: function () {
        // this.tbar = {
        //     xtype: 'toolbar',
        //     items: []
        // };
        this.items = [
            {
                region: 'west',
                id: 'west-panel',
                title: 'West',
                split: true,
                width: 200,
                minSize: 175,
                maxSize: 400,
                collapsible: true,
                margins: '35 0 5 5',
                cmargins: '35 5 5 5',
                layout: 'accordion',
                layoutConfig: {
                    animate: true
                },
                items: [{
                    html: 'test',
                    title: 'Navigation',
                    autoScroll: true,
                    border: false,
                    iconCls: 'nav'
                }, {
                    title: 'Settings',
                    html: 'test',
                    border: false,
                    autoScroll: true,
                    iconCls: 'settings'
                }]
            },
            {
                region: 'center',
                margins: '35 5 5 0',
                layout: 'column',
                autoScroll: true,
                items: [
                    {
                        columnWidth: 1,
                        baseCls: 'x-plain',
                        bodyStyle: 'padding:5px 0 5px 5px',
                        items: [{
                            title: 'A Panel',
                            html: 'test'
                        }]
                    },
                    {
                        columnWidth: 1,
                        baseCls: 'x-plain',
                        bodyStyle: 'padding:5px 0 5px 5px',
                        items: [{
                            title: 'A Panel',
                            html: 'test'
                        }]
                    },
                    {
                        columnWidth: 1,
                        baseCls: 'x-plain',
                        bodyStyle: 'padding:5px',
                        items: [{
                            title: 'A Panel',
                            html: 'test'
                        }]
                    }]
            }
        ];
        jun.KonsulGridWin.superclass.initComponent.call(this);
    }
});
jun.KonsulTambahanWin = Ext.extend(Ext.Window, {
    title: 'Perawatan Tambahan',
    modez: 1,
    width: 450,
    height: 510,
    layout: 'fit',
    id: 'docs-jun.KonsulTambahanWin',
    modal: true,
    typez_: 2,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-KonsulTambahan',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Beauty',
                        ref: '../beauty',
                        triggerAction: 'all',
                        editable: false,
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztBeautyOffduty1Cmp,
                        hiddenName: 'beauty_id',
                        valueField: 'beauty_id',
                        displayField: 'nama_beauty',
                        allowBlank: true,
                        anchor: '100%',
                        listWidth: 400,
                        lastQuery: ''
                    },
                    new jun.KonsulDetilTambahanGrid({
                        storeDetils: this.typez_ == 1 ? jun.rztBarangNoJasaMedis : jun.rztBarangCmp,
                        height: 330,
                        // frameHeader: !1,
                        margins: '0 0 5px 0',
                        header: !1,
                        ref: '../grid'
                    }),
                    {
                        xtype: 'menuseparator'
                    },
                    {
                        xtype: "textfield",
                        hideLabel: !1,
                        id: "usernameid",
                        ref: "../username",
                        margins: '5px 0 0 0',
                        maxLength: 128,
                        anchor: "100%",
                        fieldLabel: "Username",
                        name: "loginUsername",
                        allowBlank: !1
                    },
                    {
                        xtype: "textfield",
                        hideLabel: !1,
                        id: "passwordid",
                        ref: "../password",
                        maxLength: 128,
                        anchor: "100%",
                        fieldLabel: "Password",
                        name: "loginPassword",
                        inputType: "password",
                        allowBlank: !1
                    },
                    {
                        xtype: 'hidden',
                        name: 'ref_id',
                        ref: '../ref_id'
                    },
                    {
                        xtype: 'hidden',
                        name: 'type_',
                        ref: '../type_',
                        value: 1
                    },
                    {
                        xtype: 'hidden',
                        name: 'customer_id',
                        ref: '../customer_id'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.KonsulTambahanWin.superclass.initComponent.call(this);
        jun.rztKonsulDetilTambahan.removeAll();
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on('show', this.onShow, this);
    },
    onShow: function () {
        // console.log(this.typez_);
        this.type_.setValue(this.typez_);
        if (this.typez_ == 1) {
            this.username.setVisible(false);
            this.password.setVisible(false);
            this.setHeight(450);
        } else {
            this.username.getEl().set({autocomplete: "new-password"});
            this.password.getEl().set({autocomplete: "new-password"});
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
    },
    onbtnSaveclick: function (t, e) {
        this.btnDisabled(true);
        if (this.typez_ == 2) {
            var a = this.password.getValue();
            a = jun.EncryptPass(a);
            this.password.setValue(a);
        }
        var urlz = 'Konsul/Tambahan/';
        Ext.getCmp('form-KonsulTambahan').getForm().submit({
            scope: this,
            url: urlz,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztKonsulDetilTambahan.data.items, "data"))
            },
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                // this.btnDisabled(false);
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnCancelclick: function () {
        this.close();
    }
});