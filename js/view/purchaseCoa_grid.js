jun.PurchaseCoaGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PurchaseCoa",
    id: 'docs-jun.PurchaseCoaGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Supplier',
            sortable: true,
            resizable: true,
            dataIndex: 'supplier_id',
            width: 100,
            renderer: jun.renderSupplier
        },
        {
            header: 'Keterangan',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        },
        {
            header: 'Invoice',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_other',
            width: 100
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztSupplierCmp.getTotalCount() === 0)
            jun.rztSupplierCmp.load();
        if (jun.rztStoreCmp.getTotalCount() === 0)
            jun.rztStoreCmp.load();
        this.store = jun.rztPurchaseCoa;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Hapus',
                    ref: '../btnDelete'
                }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.PurchaseCoaGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PurchaseCoaWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.purchase_coa_id;
        var find = jun.rztPurchaseCoa.getAt(jun.rztPurchaseCoa.findExact('purchase_coa_id', idz));
        var posting = find.get('p');
        
        var form = new jun.PurchaseCoaWin({modez: posting == 0 ? 1 : 2, id: idz});        
        
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztPurchaseCoaDetail.baseParams = {
            purchase_coa_id: idz
        };
        jun.rztPurchaseCoaDetail.load();
        jun.rztPurchaseCoaDetail.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'PurchaseCoa/delete/id/' + record.json.purchase_coa_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztPurchaseCoa.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
