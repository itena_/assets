jun.AssetPeriodeGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"Asset Periode",
    id:'docs-jun.AssetPeriodeGrid',
    stripeRows: true,
    viewConfig: {
        forceFit: !0
    },
    sm: new Ext.grid.RowSelectionModel({
        singleSelect: !0
    }),
	columns:[
        /*{
            header:'Asset Trans',
            sortable:true,
            resizable:true,
            dataIndex:'asset_trans_id',
            width:100
        },*/
        /*{
            header:'Docref',
            sortable:true,
            resizable:true,
            dataIndex:'docref',
            width:100
        },*/
        {
            header:'Class',
            sortable:true,
            resizable:true,
            dataIndex:'class',
            width:50
        },
        {
            header:'Period',
            sortable:true,
            resizable:true,
            dataIndex:'period',
            width:50
        },
        {
            header:'Tariff(%)',
            sortable:true,
            resizable:true,
            dataIndex:'tariff',
            width:50
        },
        {
            header:'Price',
            sortable:true,
            resizable:true,
            dataIndex:'asset_trans_price',
            width:100
        },
        /*{
            header:'New Price',
            sortable:true,
            resizable:true,
            dataIndex:'asset_trans_new_price',
            width:100
        },*/

        {
            header:'Depreciation PerMonth',
            sortable:true,
            resizable:true,
            dataIndex:'penyusutanperbulan',
            width:100
        },
        {
            header:'Accumulation',
            sortable:true,
            resizable:true,
            dataIndex:'akumulasipenyusutan',
            width:100
        },
        {
            header:'Balance',
            sortable:true,
            resizable:true,
            dataIndex:'balance',
            width:100
        },
/*        {
            header:'Depreciation PerYear',
            sortable:true,
            resizable:true,
            dataIndex:'penyusutanpertahun',
            width:100
        },*/
        {
            header:'Depreciation Date',
            sortable:true,
            resizable:true,
            dataIndex:'tglpenyusutan',
            width:100
        },

		
	],
	initComponent: function(){
	this.store = jun.rztAssetPeriode;
        /*this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };*/
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'label',
                        text: 'Depreciation',
                        //ref: '../btnAdd'
                    },
                    /*{
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Ubah',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Hapus',
                        ref: '../btnDelete'
                    }*/
                ]
            };
           /*     this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};*/
		jun.AssetPeriodeGrid.superclass.initComponent.call(this);
	        //this.btnAdd.on('Click', this.loadForm, this);
                //this.btnEdit.on('Click', this.loadEditForm, this);
                //this.btnDelete.on('Click', this.deleteRec, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},
        
        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
        },
        
        loadForm: function(){
            var form = new jun.AssetPeriodeWin({modez:0});
            form.show();
        },
        
        loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();
            
            //var dodol = this.store.getAt(0);
             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.asset_periode_id;
            var form = new jun.AssetPeriodeWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },

        showData : function(docref){
            var selectedz = this.sm.getSelected();

            //var dodol = this.store.getAt(0);
            if(selectedz == undefined){
                Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                return;
            }
            var idz = selectedz.json.asset_periode_id;
            var form = new jun.AssetPeriodeWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
            /*
            var ss = new jun.rztAssetPeriode.findBy(docref);
            ss.record.refresh();
            //jun.rztAssetPeriode.findBy(docref);

            alert(docref);
            //jun.rztAssetPeriode.reload(docref)
            /!*Ext.Ajax.request({
                url: 'asset/AssetPeriode/show/docref/' + docref,
                method: 'POST',
                success:function (f, a) {
                    jun.rztAssetPeriode.findBy(docref);
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                        title:'Info',
                        msg:response.msg,
                        buttons:Ext.MessageBox.OK,
                        icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });*!/*/
        },


})
