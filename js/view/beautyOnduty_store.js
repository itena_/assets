jun.BeautyOndutystore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BeautyOndutystore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BeautyOndutyStoreId',
            url: 'BeautyOnduty',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'beauty_onduty_id'},
                {name: 'beauty_id'},
                {name: 'note_'},
                {name: 'tgl'},
                {name: 'time_start'},
                {name: 'estimate_end'}
            ]
        }, cfg));
    }
});
jun.rztBeautyOnduty = new jun.BeautyOndutystore();
//jun.rztBeautyOnduty.load();
