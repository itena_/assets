jun.TerimaBarangDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TerimaBarangDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TerimaBarangDetailsStoreId',
            url: 'TerimaBarangDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'terima_barang_details_id'},
                {name: 'terima_barang_id'},
                {
                    name: 'kode_barang',
                    mapping: 0,
                    convert: function (value, data) {
                        var jb = jun.getBarang(data['barang_id']);
                        if (jb == null) {
                            return '';
                        }
                        return jb.data.kode_barang;
                    }
                },
                {
                    name: 'nama_barang',
                    mapping: 0,
                    convert: function (value, data) {
                        var jb = jun.getBarang(data['barang_id']);
                        if (jb == null) {
                            return '';
                        }
                        return jb.data.nama_barang;
                    }
                },
                {name: 'seq', type: 'float'},
                {name: 'description'},
                {name: 'barang_id'},
                {name: 'qty'},
                {name: 'visible'},
                {name: 'item_id'}
            ]
        }, cfg));
        
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function (t, rec, idx) {
        if (rec != undefined) {
            var arrayLength = rec.length;
            for (var i = 0; i < arrayLength; i++) {
                //alert(rec[i]);
                var a = rec[i].get('barang_id');
                var jb = jun.getBarang(a);
                if (jb != null) {
                    if (rec[i].get('kode') == undefined) {
                        rec[i].data.kode = jb.data.kode_barang;
                    }
                    if (rec[i].get('nama') == undefined) {
                        rec[i].data.nama = jb.data.nama_barang;
                    }
                }
            }
        }
        this.refreshSortData();
    },
    refreshSortData: function () {
        this.each(function (item, index, totalItems) {
            item.data.seq = index;
        },this);
    }
});
jun.rztTerimaBarangDetails = new jun.TerimaBarangDetailsstore();

jun.rztInvoiceTerimaBarangDetails = new jun.TerimaBarangDetailsstore();
    jun.rztInvoiceTerimaBarangDetails.on('add', jun.rztInvoiceTerimaBarangDetails.refreshData, this);
    jun.rztInvoiceTerimaBarangDetails.on('update', jun.rztInvoiceTerimaBarangDetails.refreshData, this);
    jun.rztInvoiceTerimaBarangDetails.on('remove', jun.rztInvoiceTerimaBarangDetails.refreshData, this);
