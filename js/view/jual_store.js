jun.Jualstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Jualstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'JualStoreId',
            url: 'Jual',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'jual_id'},
                {name: 'price'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'store'},
                {name: 'up'},
                {name: 'barang_id'},
                {name: 'duration'}
            ]
        }, cfg));
    }
});
jun.rztJual = new jun.Jualstore({baseParams: {f: "cmp"}});
jun.rztJualLib = new jun.Jualstore({baseParams: {f: "cmp"}});
jun.rztJualCmp = new jun.Jualstore({baseParams: {f: "cmp"}});
//jun.rztJual.load();
