jun.BusinessunitWin = Ext.extend(Ext.Window, {
    title: 'Business Unit',
    modez:1,
    width: 438,
    height: 250,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-Businessunit',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'Business Unit Code',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'businessunit_code',
                                    id:'businessunit_codeid',
                                    ref:'../businessunit_code',
                                    maxLength: 20,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'Business Unit Name',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'businessunit_name',
                                    id:'businessunit_nameid',
                                    ref:'../businessunit_name',
                                    maxLength: 100,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                 /*                                    {
                                    xtype: 'textfield',
                                    fieldLabel: 'type',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'type',
                                    id:'typeid',
                                    ref:'../type',
                                    maxLength: 50,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                },*/

                    {
                        xtype: 'combo',
                        fieldLabel: 'Type',
                        name:'type',
                        id:'typeid',
                        ref:'../type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['PT', 'PT'],
                                ['CV', 'CV'],
                                ['Pte.Ltd', 'Pte.Ltd'],
                                ['Yayasan', 'Yayasan'],
                                ['Sdn.Bhd', 'Sdn.Bhd'],
                                ['Firma', 'Firma'],
                                ['Co.Ltd', 'Co.Ltd'],
                                ['KSO', 'KSO'],
                                ['OTHER', 'OTHER'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "type",
                        mode : 'local',
                        anchor: "100%",
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Description',
                        hideLabel:false,
                        //hidden:true,
                        name:'description',
                        id:'descriptionid',
                        ref:'../description',
                        maxLength: 200,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                                /*                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'created_at',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'created_at',
                                    id:'created_atid',
                                    ref:'../created_at',
                                    maxLength: 6,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'uploadfile',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'uploadfile',
                                    id:'uploadfileid',
                                    ref:'../uploadfile',
                                    maxLength: 200,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'uploaddate',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'uploaddate',
                                    id:'uploaddateid',
                                    ref:'../uploaddate',
                                    maxLength: 6,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, */
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.BusinessunitWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz = "projection/Businessunit/create/";
            /*if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'projection/Businessunit/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'projection/Businessunit/create/';
                }*/
             
            Ext.getCmp('form-Businessunit').getForm().submit({
                url:urlz,
                params: {
                    id: this.id,
                    mode: this.modez
                },
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztBusinessunit.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-Businessunit').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});