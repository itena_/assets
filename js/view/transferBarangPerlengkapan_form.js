jun.TransferBarangPerlengkapanWin = Ext.extend(Ext.Window, {
    title: 'Purchase Barang Perlengkapan',
    modez: 1,
    width: 920,
    height: 515,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-TransferBarangPerlengkapan',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                ref: 'formz',
                anchor: "100% 100%",
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        allowBlank: false,
                        value : DATE_NOW,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Due Date:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_jatuh_tempo',
                        fieldLabel: 'tgl',
                        name: 'tgl_jatuh_tempo',
                        id: 'tgl_jatuh_tempoid',
                        format: 'd M Y',
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Invoice No:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref_other',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref_other',
                        id: 'doc_ref_otherid',
                        ref: '../doc_ref_other',
                        style : {textTransform: "uppercase"},
                        maxLength: 50,
                        width: 170,
                        x: 400,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Supplier:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        ref: '../supplier',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztSupplierCmp,
                        emptyText: "Please select supplier",
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        allowBlank: true,
                        width: 170,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'note',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style : {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        width: 170,
                        x: 400,
                        y: 62,
                        height: 22
                    },
                    {
                        xtype: "label",
                        text: "Store/Cabang:",
                        x: 295+310,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        emptyText: "Please select store",
                        hiddenName: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        allowBlank: true,
                        width: 170,
                        x: 400+300,
                        y: 2
                    },


                    new jun.TransferBarangPerlengkapanDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 260,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: 'hidden',
                        name: 'type_',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        id: 'totalpotid',
                        ref: '../totalpot',
                        name: 'totalpot'
                    },
                    {
                        xtype: "label",
                        text: "Bruto (Rp) :",
                        x: 670,
                        y: 365
                    },
                    {
                        xtype: 'numericfield',
                        id: 'TransferBarangPerlengkapan_totalbrutoid',
                        ref: '../../totalprice',
                        name: 'bruto',
                        readOnly: true,
                        width: 145,
                        x: 670+70,
                        y: 365-3
                    },
                    {
                        xtype: "label",
                        text: "Pajak (Rp) :",
                        x: 670,
                        y: 365+25
                    },
                    {
                        xtype: 'numericfield',
                        id: 'TransferBarangPerlengkapan_totalvatid',
                        ref: '../../totalvat',
                        name: 'vat',
                        readOnly: true,
                        width: 145,
                        x: 670+70,
                        y: 365+25-3
                    },
                    {
                        xtype: "label",
                        text: "Total (Rp) :",
                        x: 670,
                        y: 365+50
                    },
                    {
                        xtype: 'numericfield',
                        id: 'TransferBarangPerlengkapan_totalpriceid',
                        ref: '../../totalvat',
                        name: 'total',
                        readOnly: true,
                        width: 145,
                        x: 670+70,
                        y: 365+50-3
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TransferBarangPerlengkapanWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }
        this.on("close", this.onWinClose, this);
    },
    onWinClose: function () {
        jun.rztTransferBarangPerlengkapanDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztTransferBarangPerlengkapanDetails.data.length == 0) {
            Ext.Msg.alert('Error', "Barang Perlengkapan details must set");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'TransferBarangPerlengkapan/createin/';
        Ext.getCmp('form-TransferBarangPerlengkapan').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztTransferBarangPerlengkapanDetails.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztTransferBarangPerlengkapan.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TransferBarangPerlengkapan').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});

jun.TransferBarangPerlengkapanOutWin = Ext.extend(Ext.Window,{
    title:"Transfer Out Barang Perlengkapan",
    modez: 1,
    width: 605,
    height: 445,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-TransferBarangPerlengkapanOut',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Invoice No:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref_other',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref_other',
                        id: 'doc_ref_otherid',
                        ref: '../doc_ref_other',
                        style: {textTransform: "uppercase"},
                        maxLength: 50,
                        width: 170,
                        x: 400,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Cabang :",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        emptyText: "Pilih Cabang",
                        hiddenName: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        allowBlank: true,
                        width: 170,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'note',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        width: 170,
                        x: 400,
                        y: 62,
                        height: 22
                    },
                    new jun.TransferBarangPerlengkapanOutDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 260,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: 'hidden',
                        name: 'type_',
                        value: 1
                    },
                    {
                        xtype: 'hidden',
                        id: 'totalpotid',
                        ref: '../totalpot',
                        name: 'totalpot'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TransferBarangPerlengkapanOutWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztTransferOutBarangPerlengkapanDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztTransferOutBarangPerlengkapanDetails.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'TransferBarangPerlengkapan/createout/';
        Ext.getCmp('form-TransferBarangPerlengkapanOut').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztTransferOutBarangPerlengkapanDetails.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztTransferBarangPerlengkapanOut.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TransferBarangPerlengkapanOut').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});