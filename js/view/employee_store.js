jun.Employeestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Employeestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'EmployeeStoreId',
            url: 'Employee',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'employee_id'},
                {name: 'nama_employee'},                
                {name: 'gol_id'},
                {name: 'kode_employee'},
                {name: 'active'},
                {name: 'up'},
                {name: 'tipe'},
                {name: 'nickname'},
                {name: 'store'},
                {name: 'no_ijin'},
                
                {name: 'admin_id'},
                {name: 'stocker_id'},
                {name: 'pc_id'},
                {name: 'fo_id'},
                {name: 'nama_admin'},
                {name: 'nama_stocker'},
                {name: 'nama_pc'},
                {name: 'nama_fo'}
            ]
        }, cfg));
    }
});
jun.rztEmployee = new jun.Employeestore();
jun.rztEmployeeCmp = new jun.Employeestore();
jun.rztEmployeeLib = new jun.Employeestore({autoLoad : true});
jun.rztEmployeeFoCmp = new jun.Employeestore({url : 'Employee/Fo', autoLoad : true});
jun.rztEmployeeStockerCmp = new jun.Employeestore({url : 'Employee/Stocker', autoLoad : true});
jun.rztEmployeeAdminCmp = new jun.Employeestore({url : 'Employee/Admin', autoLoad : true});
jun.rztEmployeePcCmp = new jun.Employeestore({url : 'Employee/Pc', autoLoad : true});
//jun.rztEmployee.load();
