jun.ClinicalTransDetailGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "ClinicalTransDetail",
    id: 'docs-jun.ClinicalTransDetailGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Item Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_clinical',
            width: 50,
            renderer: jun.renderKodeBarangClinical
        },
        {
            header: 'Item Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_clinical',
            width: 100,
            renderer: jun.renderBarangClinical
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 50
        },
        {
            header: 'Unit',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_clinical',
            width: 100,
            renderer: jun.renderUnitBarangClinical
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztClinicalTransDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 6,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztBarangClinicalCmp,
                            //hiddenName: 'barang_clinical',
                            valueField: 'barang_clinical',
                            ref: '../../barang',
                            displayField: 'kode_barang',
                            anchor: '100%'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Unit :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../sat',
                            width: 75,
                            readOnly: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../qty',
                            width: 50,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'uctextfield',
                            ref: '../../note',
                            width: 405,
                            colspan: 5,
                            maxLength: 100
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.ClinicalTransDetailGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.barang.on('select', this.onSelectBarang, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onSelectBarang: function(combo, record, index){
        this.sat.setValue(record.data.sat);
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_clinical);
            var barang = getBarangClinical(record.data.barang_clinical);
            this.qty.setValue(record.data.qty);
            this.note.setValue(record.data.note);
            this.sat.setValue(barang.data.sat);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadForm: function () {
        var barang = this.barang.getValue();
        var note = this.note.getValue();
        var qty = parseFloat(this.qty.getValue());
        if (barang == "" || barang == undefined) {
            Ext.MessageBox.alert("Error", "Clinical item must selected.");
            return;
        }
        if (qty === 0) {
            Ext.MessageBox.alert("Error", "Qty must greater than 0.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_clinical', barang);
            record.set('note', note);
            record.set('qty', qty);
            record.commit();
        } else {
            var c = jun.rztClinicalTransDetail.recordType,
                d = new c({
                    barang_clinical: barang,
                    note: note,
                    qty: qty
                });
            jun.rztClinicalTransDetail.add(d);

        }
        this.barang.reset();
        this.note.reset();
        this.qty.reset();
        this.sat.reset();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});

jun.ClinicalTransDetailOutGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "ClinicalTransDetailOut",
    id: 'docs-jun.ClinicalTransDetailOutGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Item Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_clinical',
            width: 50,
            renderer: jun.renderKodeBarangClinical
        },
        {
            header: 'Item Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_clinical',
            width: 100,
            renderer: jun.renderBarangClinical
        },
        {
            header: 'Balance',
            sortable: true,
            resizable: true,
            dataIndex: 'sisa',
            width: 50
        },
        {
            header: 'Unit',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_clinical',
            width: 100,
            renderer: jun.renderUnitBarangClinical
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztClinicalTransDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 6,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztBarangClinicalCmp,
                            //hiddenName: 'barang_clinical',
                            valueField: 'barang_clinical',
                            ref: '../../barang',
                            displayField: 'kode_barang',
                            anchor: '100%'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Unit :'
                        },
                        {
                            xtype: 'textfield',
                            ref: '../../sat',
                            width: 75,
                            readOnly: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Balance :'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../bal',
                            width: 50,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'uctextfield',
                            ref: '../../note',
                            width: 405,
                            colspan: 5,
                            maxLength: 100
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.ClinicalTransDetailOutGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.barang.on('select', this.onSelectBarang, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onSelectBarang: function(combo, record, index){
        this.sat.setValue(record.data.sat);
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_clinical);
            var barang = getBarangClinical(record.data.barang_clinical);
            this.note.setValue(record.data.note);
            this.bal.setValue(barang.data.sisa);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadForm: function () {
        var barang = this.barang.getValue();
        var note = this.note.getValue();
        var sisa = parseFloat(this.bal.getValue());
        if (barang == "" || barang == undefined) {
            Ext.MessageBox.alert("Error", "Clinical item must selected.");
            return;
        }
        //if (sisa === 0) {
        //    Ext.MessageBox.alert("Error", "Qty must greater than 0.");
        //    return;
        //}
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_clinical', barang);
            record.set('note', note);
            record.set('sisa', sisa);
            record.commit();
        } else {
            var c = jun.rztClinicalTransDetail.recordType,
                d = new c({
                    barang_clinical: barang,
                    note: note,
                    sisa: sisa
                });
            jun.rztClinicalTransDetail.add(d);

        }
        this.barang.reset();
        this.note.reset();
        this.bal.reset();
        this.sat.reset();
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        this.store.remove(record);
    }
});
