jun.PembantuPelunasanPiutangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PembantuPelunasanPiutangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PembantuPelunasanPiutangStoreId',
            url: 'PembantuPelunasanPiutang',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pembantu_pelunasan_piutang_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'total'},
                {name: 'store'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'up'},
                {name: 'bank_id'},
                {name: 'amount_bank'},
                {name: 'rounding'},

            ]
        }, cfg));
    }
});
jun.rztPembantuPelunasanPiutang = new jun.PembantuPelunasanPiutangstore();
//jun.rztPembantuPelunasanPiutang.load();
