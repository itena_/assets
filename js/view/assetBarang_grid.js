jun.AssetBarangGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"Asset Items",
        id:'docs-jun.AssetBarangGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
         /*               {
			header:'barang_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'barang_id',
			width:100
		},*/
                                {
			header:'Code',
			sortable:true,
			resizable:true,                        
            dataIndex:'kode_barang',
			width:100
		},
                                {
			header:'Name',
			sortable:true,
			resizable:true,                        
            dataIndex:'nama_barang',
			width:100
		},
                                {
			header:'Description',
			sortable:true,
			resizable:true,                        
            dataIndex:'ket',
			width:100
		},
        /*                        {
			header:'grup_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'grup_id',
			width:100
		},*/
         /*                       {
			header:'active',
			sortable:true,
			resizable:true,                        
            dataIndex:'active',
			width:100
		},*/
                		/*
                {
			header:'sat',
			sortable:true,
			resizable:true,                        
            dataIndex:'sat',
			width:100
		},
                                {
			header:'up',
			sortable:true,
			resizable:true,                        
            dataIndex:'up',
			width:100
		},
                                {
			header:'tipe_barang_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'tipe_barang_id',
			width:100
		},
                                {
			header:'barcode',
			sortable:true,
			resizable:true,                        
            dataIndex:'barcode',
			width:100
		},
                		*/
		
	],
	initComponent: function(){
	this.store = jun.rztAssetBarang;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Edit',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Delete',
                        ref: '../btnDelete'
                    }
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		jun.AssetBarangGrid.superclass.initComponent.call(this);
	        this.btnAdd.on('Click', this.loadForm, this);
                this.btnEdit.on('Click', this.loadEditForm, this);
                this.btnDelete.on('Click', this.deleteRec, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},
        
        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
        },
        
        loadForm: function(){
            var form = new jun.AssetBarangWin({modez:0});
            form.show();
        },
        
        loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();
            
            //var dodol = this.store.getAt(0);
             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.barang_id;
            var form = new jun.AssetBarangWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'asset/AssetBarang/delete/id/' + record.json.barang_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztAssetBarang.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });
        
        }
})
