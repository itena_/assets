jun.BarangWin = Ext.extend(Ext.Window, {
    title: 'Form Product/Treatment',
    modez: 1,
    width: 500,
    height: 315,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Barang',
                labelWidth: 150,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Code Product/Treatment',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kode_barang',
                        id: 'kode_barangid',
                        ref: '../kode_barang',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Barcode',
                        hideLabel: false,
                        //hidden:true,
                        name: 'barcode',
                        id: 'barcodeid',
                        ref: '../barcode',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Product/Treatment Name',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_barang',
                        id: 'nama_barangid',
                        ref: '../nama_barang',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Description',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'ket',
                        id: 'ketid',
                        ref: '../ket',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group',
                        store: jun.rztGrup,
                        ref: '../grup',
                        hiddenName: 'grup_id',
                        valueField: 'grup_id',
                        displayField: 'nama_grup',
                        anchor: '100%'
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Unit',
                        hideLabel: false,
                        //hidden:true,
                        name: 'sat',
                        id: 'satid',
                        ref: '../sat',
                        maxLength: 10,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Item Type',
                        store: jun.rztTipeBarang,
                        ref: '../tipe',
                        hiddenName: 'tipe_barang_id',
                        valueField: 'tipe_barang_id',
                        displayField: 'nama_tipe',
                        anchor: '100%'
                    }
//                    {
//                        xtype: 'numericfield',
//                        fieldLabel: 'Cost',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'cost',
//                        id: 'costid',
//                        ref: '../cost',
//                        maxLength: 30,
//                        //allowBlank: ,
//                        anchor: '100%'
//                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BarangWin.superclass.initComponent.call(this);
        //        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    onGrupChange: function () {
        var grup_id = this.grup.getValue();
        if (grup_id == "") return;
        var grup = jun.getGrup(grup_id);
        this.setDisablePrice(grup.data.kategori_id == 1);
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Barang/update/id/' + this.id;
        } else {
            urlz = 'Barang/create/';
        }
        Ext.getCmp('form-Barang').getForm().submit({
            url: urlz,
            scope: this,
            success: function (f, a) {
                jun.rztBarang.reload();
                jun.rztBarangLib.reload();
                jun.rztBarangJasa.reload();
                jun.rztBarangNonJasa.reload();
                jun.rztBarangCmp.reload();
                jun.rztBarangNonJasaAll.load();
                jun.rztBarangNonJasaTransfer.reload();
                jun.rztBarangPurchasable.load();
                

                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Barang').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.ExportImportHargaJualWin = new Ext.extend(Ext.Window, {
    width: 550,
    height: 125,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Export / Import Harga Jual",
    padding: 5,
    iswin: true,
    initComponent: function () {
        this.items = new Ext.FormPanel({
            frame: !1,
            labelWidth: 100,
            fileUpload: true,
            bodyStyle: "background-color: #E4E4E4;padding: 10px",
            id: "form-ExportImportHargaJualWin",
            labelAlign: "left",
            layout: "form",
            ref: "formz",
            border: !1,
            plain: !0,
            defaults: {
                allowBlank: false,
                msgTarget: 'side'
            },
            items: [
                {
                    xtype: "fileuploadfield",
                    hideLabel: !1,
                    fieldLabel: "File Name",
                    emptyText: 'Select an file import (*.hjn)',
                    id: "filename",
                    ref: "../filename",
                    name: "filename",
                    anchor: "95%"
                }
            ]
        });
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Export Harga Jual",
                    hidden: !1,
                    ref: "../btnBackup"
                },
                {
                    xtype: "button",
                    text: "Import Harga Jual",
                    hidden: !1,
                    ref: "../btnRestore"
                }
            ]
        };
        jun.ExportImportHargaJualWin.superclass.initComponent.call(this);
        this.btnBackup.on("click", this.onbtnBackupClick, this);
        this.btnRestore.on("click", this.btnRestoreClick, this);
    },
    onbtnBackupClick: function () {
        Ext.getCmp("form-ExportImportHargaJualWin").getForm().reset();
        Ext.getCmp("form-ExportImportHargaJualWin").getForm().standardSubmit = !0;
        Ext.getCmp("form-ExportImportHargaJualWin").getForm().url = "jual/export";
        var form = Ext.getCmp('form-ExportImportHargaJualWin').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    btnRestoreClick: function () {
        Ext.getCmp("form-ExportImportHargaJualWin").getForm().standardSubmit = false;
        Ext.getCmp("form-ExportImportHargaJualWin").getForm().url = "jual/import";
        if (Ext.getCmp("form-ExportImportHargaJualWin").getForm().isValid()) {
            Ext.getCmp("form-ExportImportHargaJualWin").getForm().submit({
                url: 'site/RestoreAll',
                waitMsg: 'Uploading your import...',
                success: function (f, a) {
                    var response = Ext.decode(a.response.responseText);
                    Ext.Msg.alert('Successfully', response.msg);
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        }
    }
});