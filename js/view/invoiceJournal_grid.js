jun.InvoiceJournalGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Invoice Journal",
    id: 'docs-jun.InvoiceJournalGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100,
            filter: {xtype: "textfield", filterName: "doc_ref"}
        },
        {
            header: 'No. Invoice',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref_other',
            width: 100,
            filter: {xtype: "textfield", filterName: "doc_ref_other"}
        },
        {
            header: 'Tgl',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            filter: {
                xtype: "xdatefield",
                format: 'd/m/Y'
            }
        },
        {
            header: 'Tgl Jatuh Tempo',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_jatuh_tempo',
            width: 100
//            filter: {xtype: "textfield", filterName: "tgl_jatuh_tempo"}
        },
        {
            header: 'Total Hutang',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100
        },
        {
            header: 'Posting',
            sortable: true,
            resizable: true,
            dataIndex: 'p',
            width: 100,
            renderer: function (v, m, r) {
                if (r.get("p") == 0) {
                    return 'BELUM DIPOSTING';
                }
                else {
                    return 'SUDAH DIPOSTING';
                }
            }
        }
    ],
    initComponent: function () {
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
//        jun.rztInvoiceJournal.on({
//            scope: this,
//            beforeload: {
//                fn: function (a, b) {
//                    //b.params.tgl = Ext.getCmp('tgljurnalumumgridid').getValue();
//                    var tgl = Ext.getCmp('tglinvoicejournalgridid');
//                    b.params.tgl = tgl.hiddenField.dom.value;
//                    b.params.mode = "grid";
//                }
//            }
//        });
        this.store = jun.rztInvoiceJournal;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Invoice Journal',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Invoice Journal',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                    // hidden: true
                },
                // {
                //     xtype: 'button',
                //     text: 'Unposting Invoice Journal',
                //     ref: '../btnDel'
                //     // hidden: true
                // },
                {
                    xtype: 'button',
                    text: 'Unposting Invoice Journal',
                    ref: '../btnUnPosting'
                },
//                {
//                    xtype: 'tbseparator'
//                },
//                {
//                    xtype: 'label',
//                    style: 'margin:5px',
//                    text: 'Date :'
//                },
//                {
//                    xtype: 'xdatefield',
//                    ref: '../tgl',
//                    id: 'tglinvoicejournalgridid'
//                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Form Import',
                    ref: '../btnImport',
                    hidden: false
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Import IT',
                    ref: '../btnImportIT',
                    hidden: !(UID == '7b9053c1-36c4-11e6-85f5-00ff55a5a602' || UID == '0b7169e1-d373-11e5-8a67-001e5839d09a')
                }
            ]
        };
        jun.InvoiceJournalGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnUnPosting.on('Click', this.unPostingRec, this);
//        this.tgl.on('select', this.refreshTgl, this);
        this.btnImport.on('Click', this.importData, this);
        this.btnImportIT.on('Click', this.importDataIT, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.InvoiceJournalWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a doctor");
            return;
        }
        var idz = selectedz.json.invoice_journal_id;
        var pos = selectedz.json.p;
        var form = new jun.InvoiceJournalWin({modez: 1, id: idz, pos: pos});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztInvoiceJournalDetail.baseParams = {
            invoice_journal_id: idz
        };
        jun.rztInvoiceJournalDetail.load();
        jun.rztInvoiceJournalDetail.baseParams = {};
    },
    importData: function () {
        var form = new jun.ImportInvoiceJournal();
        form.show();
    },
    importDataIT: function () {
        Ext.Ajax.request({
            url: 'InvoiceJournal/Import',
            method: 'POST',
            success: function (f, a) {
                jun.rztInvoiceJournal.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    unPostingRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus unposting data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        if (record.data.p != '1') {
            Ext.MessageBox.alert("Warning", "Data tidak bisa di UnPosting karena belum diposting.");
            return;
        }
        Ext.Ajax.request({
            url: 'InvoiceJournal/unposting/id/' + record.json.invoice_journal_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztInvoiceJournal.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});