jun.GroupAssetstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.GroupAssetstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GroupAssetStoreId',
            url: 'GroupAsset',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'group_asset_id'},
                {name: 'nama_group_asset'},
                {name: 'account_code'},
                {name: 'up'},

            ]
        }, cfg));
    }
});
jun.rztGroupAsset = new jun.GroupAssetstore();
//jun.rztGroupAsset.load();
