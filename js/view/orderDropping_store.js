jun.OrderDroppingstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.OrderDroppingstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'OrderDroppingStoreId',
            url: 'OrderDropping',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'order_dropping_id'},
                {name: 'tgl', type: 'date'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tdate', type: 'date'},
                {name: 'user_id'},
                {name: 'type_', type: 'int'},
                {name: 'store'},
                {name: 'store_pengirim'},
                {name: 'approved', type: 'int'},
                {name: 'approved_user_id'},
                {name: 'approved_date', type: 'date'},
                {name: 'lunas', type: 'int'},
                {name: 'up'}
            ]
        }, cfg));
    }
});
jun.rztOrderDropping = new jun.OrderDroppingstore();
jun.rztOrderDroppingCmp = new jun.OrderDroppingstore();
//jun.rztOrderDropping.load();
