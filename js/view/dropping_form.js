jun.DroppingWin = Ext.extend(Ext.Window, {
    title: 'Transfer',
    modez: 1,
    width: 900,
    height: 420,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 5px',
                id: 'form-Dropping',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        readOnly: true,
                        name: 'doc_ref',
                        maxLength: 50,
                        width: 200,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Tgl:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        name: 'tgl',
                        format: 'd M Y',
                        value: DATE_NOW,
                        readOnly: !EDIT_TGL,
                        width: 200,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "No. Order:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        ref: '../order_dropping',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztOrderDroppingCmp,
                        hiddenName: 'order_dropping_id',
                        valueField: 'order_dropping_id',
                        displayField: 'doc_ref',
                        hideTrigger: true,
                        minChars: 1,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item-table",
                        tpl: new Ext.XTemplate('<div class="container">', '<tpl for="."><div class="search-item-table">',
                            '<div class="cell4" style="width: 180px;">{doc_ref}</div>',
                            '<div class="cell4" style="width: 80px;">{tgl:date("M j, Y")}</div>',
                            '<div class="cell4" style="width: 80px;">{store}</div>',
                            "</div></tpl>", '</div>'),
                        //allowBlank: false,
                        listWidth: 360,
                        lastQuery: "",
                        width: 200,
                        x: 85,
                        y: 62,
                        listeners:{
                            select:function (ele, rec, idx) {
                                if (this.refOwner.modez == 3)
                                {
                                    jun.rztDroppingTerima.baseParams = {
                                        id_terima: this.refOwner.id,
                                        id_tr: this.refOwner.order_dropping.getValue()
                                    };
                                    jun.rztDroppingTerima.load();
                                }
                            }
                        }
                    },
                    {
                        xtype: 'label',
                        text: 'No. SJ:',
                        ref: '../txt_sj',
                        name: 'txt_sj',
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'textfield',
                        readOnly: true,
                        ref: '../no_sj',
                        name: 'no_sj',
                        maxLength: 50,
                        width: 200,
                        x: 85,
                        y: 92
                    },
                    {
                        xtype: "label",
                        text: "Pengirim:",
                        x: 340,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        //readOnly: !HEADOFFICE,
                        readOnly: true,
                        allowBlank: false,
                        width: 200,
                        x: 420,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Penerima:",
                        x: 340,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        ref: '../store_penerima',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store_penerima',
                        name: 'store_penerima',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "Select Branch",
                        //readOnly: true,
                        allowBlank: false,
                        width: 200,
                        x: 420,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 340,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        name: 'note',
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        maxLength: 600,
                        width: 300,
                        height: 50,
                        x: 420,
                        y: 62
                    },
                    new jun.DroppingDetailsGrid({
                        x: 5,
                        y: 125,
                        anchor: '100% 100%',
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils",
                        readOnly: (!(this.modez < 2)?true:false)
                    }),
                    new jun.droppingTerimaGrid({
                        x: 5,
                        y: 125,
                        anchor: '100% 100%',
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetilspenerimaanbarang"
                    })
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Approve',
                    ref: '../btnApprove',
                    hidden: true,
                    iconCls: 'silk13-tick'
                },
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.DroppingWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.order_dropping.on('select', this.onOrderDroppingSelect, this);
        this.order_dropping.on('change', this.onOrderDroppingChange, this);
        this.btnApprove.on('click', this.onbtnApproveclick, this);
        this.on("close", this.onWinClose, this);
        this.tgl.setReadOnly(!EDIT_TGL);

        this.griddetilspenerimaanbarang.setVisible(false);
        this.no_sj.setVisible(false);
        this.txt_sj.setVisible(false);
        switch(this.modez){
            case 0:
                this.btnSave.setVisible(true);
                break;
            case 1 :
                this.btnSave.setVisible(false);
                break;
            case 3 :
                this.griddetils.setVisible(false);
                this.griddetilspenerimaanbarang.setVisible(true);
                this.no_sj.setVisible(true);
                this.txt_sj.setVisible(true);
                this.no_sj.setValue(this.sj);
                break;
            default :
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(false);
                this.formz.getForm().applyToFields({ readOnly: true });
                break;
        }
        
        if(this.approval){
            this.btnApprove.setVisible(true);
        }
    },
    onOrderDroppingSelect: function (c, r, i) {
        this.store_penerima.setValue(r.get('store'));
        this.store_penerima.setReadOnly(true);
        this.griddetils.store.baseParams = {order_dropping_id: r.data.order_dropping_id};
        this.griddetils.store.reload();
        this.griddetils.store.baseParams = {};
    },
    onOrderDroppingChange: function (c, n, o) {
        if(!c.getValue() && this.modez < 2){
            this.store_penerima.setReadOnly(false);
            this.store_penerima.reset();
            c.reset();
        }
    },
    onWinClose: function () {
        this.griddetils.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Dropping/create/';
        //this.modez = (this.modez==3) ? 0 : this.modez;

        this.formz.getForm().submit({
            url: urlz,
            //timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                detil: (this.modez==3) ?
                    Ext.encode(Ext.pluck(this.griddetilspenerimaanbarang.store.data.items, "data")) : Ext.encode(Ext.pluck(this.griddetils.store.data.items, "data"))
            },
            success: function (f, a) {
                if(Ext.getCmp('docs-jun.DroppingGrid') !== undefined) Ext.getCmp('docs-jun.DroppingGrid').store.reload();

                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    this.formz.getForm().reset();
                    this.griddetils.store.removeAll();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnApproveclick: function () {
        this.btnDisabled(true);
        this.formz.getForm().submit({
            url: 'Dropping/Approve/',
            timeOut: 1000,
            scope: this,
            params: {
                id: this.id
            },
            success: function (f, a) {
                jun.rztDroppingApproval.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});