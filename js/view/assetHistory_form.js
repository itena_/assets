jun.AssetHistoryWin = Ext.extend(Ext.Window, {
    title: 'Asset History',
    modez:1,
    width: 1302,
    height: 469,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetHistory',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    new jun.AssetHistoryGrid({
                        height: 405 - 30,
                        frameHeader: !1,
                        header: !1,
                        ref: "../gridDetail",
                        x: 5,
                        y: 65 + 30
                    }),
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Export',
                    hidden: false,
                    ref:'../btnExport'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref:'../btnClose'
                }
            ]
        };

        jun.AssetHistoryWin.superclass.initComponent.call(this);
        this.gridDetail.store.baseParams=this.historyid;
        this.gridDetail.paging.doRefresh();
        this.btnClose.on('click', this.onbtnCloseclick, this);

    },


    onbtnCloseclick: function(){
        this.close();
    }
   
});