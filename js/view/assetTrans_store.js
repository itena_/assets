jun.AssetTransstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetTransstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetTransStoreId',
            url: 'AssetTrans',           
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'trans_id'},
{name:'asset_id'},
{name:'businessunit_id'},
                
            ]
        }, cfg));
    }
});
jun.rztAssetTrans = new jun.AssetTransstore();
//jun.rztAssetTrans.load();
