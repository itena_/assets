jun.PoInWin = Ext.extend(Ext.Window, {
    title: 'Purchase Order',
    modez: 1,
    pr_id: '',
    width: 1015,
    height: 600,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;',
                id: 'form-PoIn',
                labelWidth: 100,
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'tabpanel',
                        height: 175,
                        x: 5,
                        y: 5,
                        activeTab: 0,
                        resizeTabs: true, // turn on tab resizing
                        minTabWidth: 50,
                        tabWidth: 75,
                        items: [
                            {
                                title: 'Header',
                                frame: false,
                                layout: 'column',
                                bodyStyle: "background-color: #E4E4E4;",
                                border: false,
                                defaults: {
                                    border: false
                                },
                                items: [
                                    {
                                        xtype: "panel",
                                        columnWidth: .45,
                                        frame: false,
                                        layout: 'form',
                                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'No. PO',
                                                hideLabel: false,
                                                name: 'doc_ref',
                                                ref: '../../../../doc_ref',
                                                maxLength: 50,
                                                readOnly: true,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                hideLabel: false,
                                                fieldLabel: 'No. PR',
                                                name: 'doc_ref_pr',
                                                ref: '../../../../doc_ref_pr',
                                                maxLength: 50,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                hideLabel: false,
                                                fieldLabel: 'Divisi',
                                                name: 'divisi',
                                                ref: '../../../../divisi',
                                                maxLength: 100,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textarea',
                                                fieldLabel: 'Acc. Charge',
                                                name: 'acc_charge',
                                                ref: '../../../../acc_charge',
                                                enableKeyEvents: true,
                                                style: {textTransform: "uppercase"},
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        field.setValue(newValue.toUpperCase());
                                                    }
                                                },
                                                maxLength: 600,
                                                anchor: '100%',
                                                height: 50
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "panel",
                                        columnWidth: .1,
                                        frame: false,
                                        layout: 'form',
                                        bodyStyle: "background-color: #E4E4E4;padding: 10px;"
                                    },
                                    {
                                        xtype: "panel",
                                        columnWidth: .45,
                                        frame: false,
                                        layout: 'form',
                                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                        items: [
                                            {
                                                xtype: 'xdatefield',
                                                ref: '../../../../tgl',
                                                fieldLabel: 'Tgl',
                                                name: 'tgl',
                                                format: 'd M Y',
                                                allowBlank: false,
                                                value: DATE_NOW,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'xdatefield',
                                                fieldLabel: 'Tgl Kirim',
                                                ref: '../../../../tgl_delivery',
                                                name: 'tgl_delivery',
                                                format: 'd M Y',
                                                allowBlank: false,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'combo',
                                                //typeAhead: true,
                                                fieldLabel: 'Branch',
                                                ref: '../store',
                                                triggerAction: 'all',
                                                lazyRender: true,
                                                mode: 'local',
                                                store: jun.rztStoreCmp,
                                                hiddenName: 'store',
                                                name: 'store',
                                                valueField: 'store_kode',
                                                displayField: 'store_kode',
                                                emptyText: "All Branch",
                                                value: STORE,
                                                readOnly: !HEADOFFICE,
                                                anchor: '100%'
                                            },
                                            // {
                                            //     xtype: 'checkbox',
                                            //     fieldLabel: "PPN",
                                            //     boxLabel: "",
                                            //     value: 'Y',
                                            //     inputValue: 'Y',
                                            //     uncheckedValue: 'N',
                                            //     name: "ppn"
                                            // },
                                            {
                                                xtype: 'textarea',
                                                fieldLabel: 'Note',
                                                name: 'note',
                                                ref: '../../../../note',
                                                enableKeyEvents: true,
                                                style: {textTransform: "uppercase"},
                                                listeners: {
                                                    change: function (field, newValue, oldValue) {
                                                        field.setValue(newValue.toUpperCase());
                                                    }
                                                },
                                                maxLength: 600,
                                                anchor: '100%',
                                                height: 50
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                title: 'Ship To',
                                frame: false,
                                layout: 'column',
                                bodyStyle: "background-color: #E4E4E4;",
                                border: false,
                                defaults: {
                                    border: false
                                },
                                items: [
                                    {
                                        xtype: "panel",
                                        columnWidth: .45,
                                        frame: false,
                                        layout: 'form',
                                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Attn. Name',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'nama',
                                                id: 'namaid',
                                                ref: '../../../../nama',
                                                maxLength: 100,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Company Name',
                                                hideLabel: false,
                                                name: 'ship_to_company',
                                                ref: '../../../../ship_to_company',
                                                maxLength: 100,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Address',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'ship_to_address',
                                                id: 'ship_to_addressid',
                                                ref: '../../../../ship_to_address',
                                                maxLength: 100,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Company City',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'ship_to_city',
                                                id: 'ship_to_cityid',
                                                ref: '../../../../ship_to_city',
                                                maxLength: 100,
                                                anchor: '100%'
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "panel",
                                        columnWidth: .1,
                                        frame: false,
                                        layout: 'form',
                                        bodyStyle: "background-color: #E4E4E4;padding: 10px;"
                                    },
                                    {
                                        xtype: "panel",
                                        columnWidth: .45,
                                        frame: false,
                                        layout: 'form',
                                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Company Country',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'ship_to_country',
                                                id: 'ship_to_countryid',
                                                ref: '../../../../ship_to_country',
                                                maxLength: 100,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Company Phone',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'ship_to_phone',
                                                id: 'ship_to_phoneid',
                                                ref: '../../../../ship_to_phone',
                                                maxLength: 100,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Company Fax',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'ship_to_fax',
                                                ref: '../../../../ship_to_fax',
                                                maxLength: 100,
                                                anchor: '100%'
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                title: 'Supplier',
                                frame: false,
                                layout: 'column',
                                bodyStyle: "background-color: #E4E4E4;",
                                border: false,
                                defaults: {
                                    border: false
                                },
                                items: [
                                    {
                                        xtype: "panel",
                                        columnWidth: .45,
                                        frame: false,
                                        layout: 'form',
                                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                        items: [
                                            {
                                                xtype: 'combo',
                                                //typeAhead: true,
                                                fieldLabel: 'Supplier',
                                                ref: '../../../../supplier',
                                                triggerAction: 'all',
                                                lazyRender: true,
                                                mode: 'local',
                                                store: jun.rztSupplierCmp,
                                                emptyText: "Please select supplier",
                                                hiddenName: 'supplier_id',
                                                valueField: 'supplier_id',
                                                displayField: 'supplier_name',
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Name',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'supp_nama',
                                                ref: '../../../../supp_nama',
                                                maxLength: 100,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Company Name',
                                                hideLabel: false,
                                                name: 'supp_company',
                                                ref: '../../../../supp_company',
                                                maxLength: 100,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Address',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'supp_address',
                                                ref: '../../../../supp_address',
                                                maxLength: 100,
                                                anchor: '100%'
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "panel",
                                        columnWidth: .1,
                                        frame: false,
                                        layout: 'form',
                                        bodyStyle: "background-color: #E4E4E4;padding: 10px;"
                                    },
                                    {
                                        xtype: "panel",
                                        columnWidth: .45,
                                        frame: false,
                                        layout: 'form',
                                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'City',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'supp_city',
                                                ref: '../../../../supp_city',
                                                maxLength: 100,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Country',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'supp_country',
                                                ref: '../../../../supp_country',
                                                maxLength: 100,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Phone',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'supp_phone',
                                                ref: '../../../../supp_phone',
                                                maxLength: 100,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Fax',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'supp_fax',
                                                ref: '../../../../supp_fax',
                                                maxLength: 100,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                fieldLabel: 'Email',
                                                hideLabel: false,
                                                //hidden:true,
                                                name: 'supp_email',
                                                ref: '../../../../supp_email',
                                                maxLength: 100,
                                                anchor: '100%'
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                title: 'T.O.P',
                                frame: false,
                                layout: 'column',
                                bodyStyle: "background-color: #E4E4E4;",
                                border: false,
                                defaults: {
                                    border: false
                                },
                                items: [
                                    {
                                        xtype: "panel",
                                        columnWidth: .45,
                                        frame: false,
                                        layout: 'form',
                                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                        items: [
                                            {
                                                xtype: 'textfield',
                                                hideLabel: false,
                                                fieldLabel: 'Nama Rekening',
                                                //hidden:true,
                                                name: 'nama_rek',
                                                ref: '../../../../nama_rek',
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                hideLabel: false,
                                                fieldLabel: 'No Rekening',
                                                //hidden:true,
                                                name: 'no_rek',
                                                ref: '../../../../no_rek',
                                                maxLength: 100,
                                                anchor: '100%'
                                            },
                                            {
                                                xtype: 'textfield',
                                                hideLabel: false,
                                                fieldLabel: 'Rekening Bank',
                                                name: 'bank_rek',
                                                ref: '../../../../bank_rek',
                                                maxLength: 100,
                                                anchor: '100%'
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "panel",
                                        columnWidth: .1,
                                        frame: false,
                                        layout: 'form',
                                        bodyStyle: "background-color: #E4E4E4;padding: 10px;"
                                    },
                                    {
                                        xtype: "panel",
                                        columnWidth: .45,
                                        frame: false,
                                        layout: 'form',
                                        bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                        items: [
                                            {
                                                xtype: 'numericfield',
                                                hideLabel: false,
                                                fieldLabel: 'T.O.P',
                                                //hidden:true,
                                                name: 'termofpayment',
                                                ref: '../../../../termofpayment',
                                                value: 0,
                                                minValue: 0,
                                                anchor: '100%'
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    new jun.PoInDetailsGrid({
                        x: 5,
                        y: 185,
                        height: 250,
                        frameHeader: !1,
                        header: !1,
                        ref: "../griddetils"
                    }),
                    {
                        xtype: 'hidden',
                        name: "total_dpp",
                        id: 'total_dppid'
                    },
                    {
                        frame: false,
                        x: 5,
                        y: 438,
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;",
                        //border: false,
                        defaults: {
                            border: false
                        },
                        items: [
                            {
                                xtype: "panel",
                                columnWidth: .33,
                                frame: false,
                                layout: 'form',
                                bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                items: [
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Sub Total',
                                        name: 'sub_total',
                                        id: 'sub_totalid',
                                        ref: '../../../sub_total',
                                        anchor: '100%',
                                        value: 0,
                                        readOnly: true
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Disc (%)',
                                        name: 'disc',
                                        id: 'discid',
                                        ref: '../../../disc',
                                        anchor: '100%',
                                        value: 0
                                    }
                                ]
                            },
                            {
                                xtype: "panel",
                                columnWidth: .33,
                                frame: false,
                                layout: 'form',
                                bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                items: [
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Total Disc',
                                        name: 'total_disc_rp',
                                        id: 'total_disc_rpid',
                                        ref: '../../../total_disc_rp',
                                        anchor: '100%',
                                        value: 0,
                                        readOnly: true
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Total PPN',
                                        name: 'tax_rp',
                                        id: 'tax_rpid',
                                        ref: '../../../tax_rp',
                                        anchor: '100%',
                                        value: 0,
                                        readOnly: true
                                    }
                                ]
                            },
                            {
                                xtype: "panel",
                                columnWidth: .33,
                                frame: false,
                                layout: 'form',
                                bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                items: [
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Total PPH',
                                        name: 'total_pph_rp',
                                        id: 'total_pph_rpid',
                                        ref: '../../../total_pph_rp',
                                        anchor: '100%',
                                        value: 0,
                                        readOnly: true
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Total',
                                        name: 'total',
                                        id: 'totalprid',
                                        ref: '../../../total',
                                        anchor: '100%',
                                        value: 0,
                                        readOnly: true
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PoInWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.supplier.on('select', this.onSuppSelect, this);
        this.disc.on('change', this.onDiscChanged, this);
        this.on("close", this.onWinClose, this);
        switch(this.modez) {
            case 0:
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(true);
                break;
            case 1 :
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(true);
                break;
            case 2 :
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(false);
                break;
            default :
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(true);
                break;
        }
    },
    onDiscChanged: function(t, newValue, oldValue){
        jun.rztPoInDetails.each(function (record) {
            var disc = parseFloat(newValue);
            var sub_total = round(record.data.qty * record.data.price,2);
            var disc_rp = round(sub_total * (disc / 100),2);
            var total_disc = disc_rp;
            var total_dpp = sub_total - total_disc;
            var ppn_rp = round(total_dpp * (record.data.ppn / 100),2);
            var pph_rp = round(total_dpp * (record.data.pph / 100),2);
            var total = total_dpp + ppn_rp - pph_rp;
            record.set('sub_total', sub_total);
            record.set('total_dpp', total_dpp);
            record.set('disc', disc);
            record.set('disc_rp', disc_rp);
            record.set('ppn_rp', ppn_rp);
            record.set('pph_rp', pph_rp);
            record.set('total', total);
            record.commit();
        });
        jun.rztPoInDetails.refreshData();
    },
    onSuppSelect: function (c, r, i) {
        this.supp_nama.setValue(r.data.name_cp);
        this.supp_address.setValue(r.data.supp_address);
        this.supp_company.setValue(r.data.supp_company);
        this.supp_city.setValue(r.data.supp_city);
        this.supp_country.setValue(r.data.supp_country);
        this.supp_phone.setValue(r.data.supp_phone);
        this.supp_fax.setValue(r.data.supp_fax);
        this.supp_email.setValue(r.data.supp_email);
        this.nama_rek.setValue(r.data.nama_rek);
        this.no_rek.setValue(r.data.no_rek);
        this.bank_rek.setValue(r.data.bank_rek);
        this.termofpayment.setValue(r.data.termofpayment);
    },
    onWinClose: function () {
        jun.rztPoInDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if(this.tgl_delivery.getValue() == ""){
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            this.btnDisabled(false);
            return;
        }
        var urlz = 'PoIn/create/';
        Ext.getCmp('form-PoIn').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
                detil: Ext.encode(Ext.pluck(
                    jun.rztPoInDetails.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztPoIn.reload();
                jun.rztBarangCmp.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PoIn').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});