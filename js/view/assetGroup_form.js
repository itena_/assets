jun.AssetGroupWin = Ext.extend(Ext.Window, {
    title: 'Asset Class',
    modez:1,
    width: 400,
    height: 200,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,    
    initComponent: function() {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-AssetGroup',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                                                                                     {
                                    xtype: 'numericfield',
                                    fieldLabel: 'Tariff',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'tariff',
                                    id:'tariffid',
                                    ref:'../tariff',
                                    maxLength: 50,
                                    //allowBlank: ,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'numericfield',
                                    fieldLabel: 'Period',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'period',
                                    id:'periodid',
                                    ref:'../period',
                                    maxLength: 10,
                                    //allowBlank: ,
                                    anchor: '100%'
                                },
                                {
                                    xtype: 'textfield',
                                    fieldLabel: 'Class',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'golongan',
                                    id:'golonganid',
                                    ref:'../golongan',
                                    maxLength: 10,
                                    //allowBlank: ,
                                    anchor: '100%'
                                },
                                /*new jun.cmbGolongan({
                                    fieldLabel: 'Golongan',
                                    value: '1',
                                    anchor: '100%',
                                    ref:'../golongan',
                                    name:'golongan',
                                    id:'golonganid',
                                }),*/
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'Description',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'desc',
                                    id:'descid',
                                    ref:'../desc',
                                    maxLength: 100,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                   
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.AssetGroupWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz;
            if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'asset/AssetGroup/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'asset/AssetGroup/create/';
                }
             
            Ext.getCmp('form-AssetGroup').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                success: function(f,a){
                    jun.rztAssetGroup.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-AssetGroup').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});