jun.TransaksiOverrideGrid=Ext.extend(Ext.grid.GridPanel ,{
    title:"Sales Override",
    id:'docs-jun.TransaksiOverrideGrid',
    iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
    plugins:[new Ext.ux.grid.GridHeaderFilters],
    columns:[

        {
            header:'Doc.Ref',
            sortable:true,
            resizable:true,
            dataIndex:'docref',
            width:100
        },
        {
            header:'Transaction Name',
            sortable:true,
            resizable:true,
            dataIndex:'name',
            width:100
        },
        {
            header:'Amount',
            sortable:true,
            resizable:true,
            dataIndex:'amount',
            width:100
        },
        {
            header:'Description',
            sortable:true,
            resizable:true,
            dataIndex:'description',
            width:100
        },
        {
            header:'Date',
            sortable:true,
            resizable:true,
            dataIndex:'tdate',
            width:100
        },

    ],
    initComponent: function(){
        this.store = jun.rztTransaksiOverride;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Delete',
                    ref: '../btnDelete'
                },
                {
                    xtype:'tbseparator'
                }/*,
                {
                    xtype: 'button',
                    text: 'Show Details',
                    ref: '../btnShow'
                }*/
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.TransaksiOverrideGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        //this.btnShow.on('Click', this.showDetail, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },

    getrow: function(sm, idx, r){
        this.record = r;
        var selectedz = this.sm.getSelections();
    },

    /*showDetail: function(){
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
            return;
        }
        var idz = selectedz.json.transaksi_id;
        //var filter = this.record.get('lunas') == PR_DRAFT && this.record.get('store') == STORE;
        var form = new jun.TransaksiWin({
            modez: 3,
            id: idz,
            title: ("Show Sales")
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztTransaksiDetail.baseParams = {
            transaksi_id: idz
        };
        jun.rztTransaksiDetail.load();
        jun.rztTransaksiDetail.baseParams = {};
    },*/

    loadForm: function(){
        var form = new jun.TransaksiOverrideWin({modez:0});
        form.show();
    },

    loadEditForm: function(){

        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
            return;
        }
        var idz = selectedz.json.transaksi_id;
        //var filter = this.record.get('lunas') == PR_DRAFT && this.record.get('store') == STORE;
        var form = new jun.TransaksiOverrideWin({
            modez: 1,
            id: idz,
            title: ("Edit Sales")
        });
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        /*        jun.rztTransaksiDetail.baseParams = {
                    transaksi_id: idz
                };
                jun.rztTransaksiDetail.load();
                jun.rztTransaksiDetail.baseParams = {};*/
    },

    deleteRec : function(){
        Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },

    deleteRecYes : function(btn){

        if (btn == 'no') {
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Data.");
            return;
        }

        Ext.Ajax.request({
            url: 'projection/TransaksiOverride/delete/id/' + record.json.transaksi_id,
            method: 'POST',
            success:function (f, a) {
                jun.rztTransaksi.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
})
