jun.BarangAssetWin = Ext.extend(Ext.Window, {
    title: 'BarangAsset',
    modez: 1,
    width: 500,
    height: 250,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        if (jun.rztGroupAsset.getTotalCount() === 0) {
            jun.rztGroupAsset.load();
        }

        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-BarangAsset',
                labelWidth: 150,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Kode Barang Asset',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kode_barang_asset',
                        id: 'kode_barang_assetid',
                        ref: '../kode_barang_asset',
                        maxLength: 20,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Nama Barang Asset',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nama_barang_asset',
                        id: 'nama_barang_assetid',
                        ref: '../nama_barang_asset',
                        maxLength: 100,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Keterangan',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ket_asset',
                        id: 'ket_assetid',
                        ref: '../ket_asset',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    //{
                    //    xtype: 'textfield',
                    //    fieldLabel: 'satuan',
                    //    hideLabel: false,
                    //    //hidden:true,
                    //    name: 'satuan',
                    //    id: 'satuanid',
                    //    ref: '../satuan',
                    //    maxLength: 10,
                    //    //allowBlank: ,
                    //    anchor: '100%'
                    //},
                    //{
                    //    xtype: 'textfield',
                    //    fieldLabel: 'up',
                    //    hideLabel: false,
                    //    //hidden:true,
                    //    name: 'up',
                    //    id: 'upid',
                    //    ref: '../up',
                    //    maxLength: 3,
                    //    //allowBlank: ,
                    //    anchor: '100%'
                    //},
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Group Asset ',
                        store: jun.rztGroupAsset,
                        hiddenName: 'group_asset_id',
                        valueField: 'group_asset_id',
                        displayField: 'nama_group_asset',
                        anchor: '100%'
                    },

                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.BarangAssetWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {

            urlz = 'BarangAsset/update/id/' + this.id;

        } else {

            urlz = 'BarangAsset/create/';
        }

        Ext.getCmp('form-BarangAsset').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztBarangAsset.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-BarangAsset').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Semua Field Harus diisi!');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });

    },

    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});