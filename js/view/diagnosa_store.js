jun.Diagnosastore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Diagnosastore.superclass.constructor.call(this, Ext.apply({
            storeId: 'DiagnosaStoreId',
            url: 'Diagnosa',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'diagnosa_id'},
                {name: 'diag'},
                {name: 'note_'},
                {name: 'konsul_id'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function (t, rec, idx) {
        var grid = Ext.getCmp('docs-jun.SalestransQtyDetailsGrid');
        if (t.getCount() == 0) {
            jun.rztSalestransQtyDetails.removeAll();
            jun.rztSalestransQtyDetails.refreshData();
            grid.setDisabled(true);
        } else {
            grid.setDisabled(false);
        }
    }
});
jun.rztDiagnosa = new jun.Diagnosastore();
//jun.rztDiagnosa.load();
