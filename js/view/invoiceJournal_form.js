jun.InvoiceJournalWin = Ext.extend(Ext.Window, {
    title: 'Invoice Journal',
    modez: 1,
    width: 945,
    height: 595,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    iswin: true,
    initComponent: function () {
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
        if (jun.rztChartMasterLib.getTotalCount() === 0) {
            jun.rztChartMasterLib.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-InvoiceJournal',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    /*
                     * Kolom 1
                     */
                    {
                        xtype: "label",
                        text: "Doc ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        readOnly: true,
                        width: 175,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "No. Invoice:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'doc_ref_other',
                        id: 'doc_ref_otherid',
                        ref: '../doc_ref_other',
                        width: 175,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Supplier:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    /*
                     * Kolom 2
                     */         {
                        xtype: "label",
                        text: "Date:",
                        x: 290,
                        y: 5
                    },
                    {
                        xtype: "xdatefield",
                        ref: "../tran_date",
                        name: "tgl",
                        id: "tglid",
                        //readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        format: "d M Y",
                        width: 175,
                        x: 370,
                        y: 2,
                        minValue: RES_DATE
                    },
                    {
                        xtype: "label",
                        text: "Jatuh tempo:",
                        x: 290,
                        y: 35
                    },
                    {
                        xtype: "xdatefield",
                        ref: "../tgl_jatuh_tempo",
                        name: "tgl_jatuh_tempo",
                        id: "tgl_jatuh_tempoid",
                        //readOnly: true,
                        allowBlank: false,
                        format: "d M Y",
                        width: 175,
                        x: 370,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Store:",
                        x: 290,
                        y: 65
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'store_kode',
                        id: 'store_kodeid',
                        ref: '../store',
                        value: STORE,
                        readOnly: true,
                        maxLength: 20,
                        width: 175,
                        x: 370,
                        y: 62
                    },
                    /*
                     * Kolom 3
                     */
                    {
                        xtype: "label",
                        text: "Total Debit:",
                        x: 615,
                        y: 5
                    },
                    {
                        xtype: "numericfield",
                        hideLabel: !1,
                        name: "tot_debit",
                        id: "tot_debit_id",
                        readOnly: !0,
                        ref: "../TotDebit",
                        value: 0,
                        width: 175,
                        x: 715,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Total Credit:",
                        x: 615,
                        y: 35
                    },
                    {
                        xtype: "numericfield",
                        hideLabel: !1,
                        name: "tot_kredit",
                        id: "tot_kredit_id",
                        readOnly: !0,
                        ref: "../TotKredit",
                        value: 0,
                        width: 175,
                        x: 715,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Balance:",
                        x: 615,
                        y: 65
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        readOnly: true,
                        ref: "../Balance",
                        id: "hutangBalance",
                        width: 175,
                        x: 715,
                        y: 62
                    },
                    {
                        xtype: 'hidden',
                        name: 'p',
                        ref: '../p'
                    },
                    {
                        xtype: 'hidden',
                        name: 'editable',
                        ref: '../editable'
                    },
                    new jun.InvoiceJournalDetailGrid({
                        height: 405 - 30,
                        frameHeader: !1,
                        header: !1,
                        x: 5,
                        y: 65 + 30
                    }),
                    {
                        xtype: "label",
                        text: "Total Hutang:",
                        x: 615,
                        y: 482
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        ref: "../total",
                        name: "total",
                        value: 0,
                        width: 175,
                        x: 715,
                        y: 480
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'P O S T I N G',
                    ref: '../btnPosting',
                    hidden: true
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.GlTransWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        //this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnPosting.on('click', this.onbtnPostingclick, this);
        this.on('activate', this.onActive, this);
//        if (this.modez == 1 || this.modez == 2) {
//            this.btnPosting.setVisible(true);
//        } else {
//            this.btnPosting.setVisible(false);
//        }
//        this.setDateTime();
    },
    onActive: function () {
        if (this.modez == 0) {
            this.btnPosting.setVisible(true);
            this.btnSaveClose.setVisible(true);
        } else {
            this.btnPosting.setVisible(this.pos == 0);
            this.btnSaveClose.setVisible(this.pos == 0);
        }
    },
    btnDisabled: function (status) {
        //this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (this.TotDebit.value != this.TotKredit.value) {
            Ext.MessageBox.alert("Error", "Total debit and credit must equal");
            this.btnDisabled(!1);
            return;
        }
        if (parseFloat(this.TotDebit.value) === 0 || parseFloat(this.TotKredit.value) === 0) {
            Ext.MessageBox.alert("Error", "Total debit or total credit must greater than 0.");
            this.btnDisabled(!1);
            return;
        }
        if (this.editable.getValue() != 0) {
            Ext.MessageBox.alert("Error", "Data tidak bisa diedit lagi!");
            return;
        }
        if (this.total.getValue() == 0) {
            Ext.MessageBox.alert("Error", "Hutang tidak boleh 0!");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'InvoiceJournal/create/';
        Ext.getCmp('form-InvoiceJournal').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztInvoiceJournalDetail.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                jun.rztInvoiceJournal.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-InvoiceJournal').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnPostingclick: function () {
        this.btnDisabled(true);
//        if (this.p.getValue() != 0) {
//            Ext.Msg.alert('Failure', 'Data ini sudah pernah di posting!');
//            return;
//        }
        if (this.TotDebit.value != this.TotKredit.value) {
            Ext.MessageBox.alert("Error", "Total debit and credit must equal");
            this.btnDisabled(!1);
            return;
        }
        if (parseFloat(this.TotDebit.value) === 0 || parseFloat(this.TotKredit.value) === 0) {
            Ext.MessageBox.alert("Error", "Total debit or total credit must greater than 0.");
            this.btnDisabled(!1);
            return;
        }
        if (this.editable.getValue() != 0) {
            Ext.MessageBox.alert("Error", "Data tidak bisa diedit lagi!");
            return;
        }
        if (this.p.getValue() != 0) {
            Ext.MessageBox.alert("Error", "Data sudah diposting!");
            return;
        }
        if (this.total.getValue() == 0) {
            Ext.MessageBox.alert("Error", "Hutang tidak boleh 0!");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'InvoiceJournal/Create';
        Ext.getCmp('form-InvoiceJournal').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                    jun.rztInvoiceJournalDetail.data.items, "data")),
                id: this.id,
                mode: this.modez,
                posting: true
            },
            success: function (f, a) {
                jun.rztInvoiceJournal.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-InvoiceJournal').getForm().reset();
                    this.btnDisabled(false);
                }
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});