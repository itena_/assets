jun.BeautyOndutyStatusstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.BeautyOndutyStatusstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'BeautyOndutyStatusStoreId',
            url: 'BeautyServices/BeautyOnduty',
            root: 'results',
            idProperty: 'beauty_onduty_id',
            totalProperty: 'total',
            fields: [
                {name: 'diff_'},
                {name: 'beauty_onduty_id'},
                {name: 'beauty_id'},
                {name: 'note_'},
                {name: 'tgl'},
                {name: 'time_start'},
                {name: 'estimate_end'},
                {name: 'nama_beauty'},
                {name: 'gol_id'},
                {name: 'kode_beauty'},
                {name: 'urut'},
                {name: 'total'}
            ]
        }, cfg));
    }
});
jun.rztBeautyOndutyStatus = new jun.BeautyOndutyStatusstore();
//jun.rztBeautyOndutyStatus.load();
