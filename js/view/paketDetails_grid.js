jun.PaketDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PaketDetails",
    modez: 1,
    id: 'docs-jun.PaketDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Category',
            sortable: true,
            resizable: true,
            dataIndex: 'kategori_name',
            width: 100
        },
        {
            header: 'Max Regular Price',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztPaketDetails;
        this.tbar = {
            xtype: 'toolbar',
            hidden: this.modez !== 0,
            items: [
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Category :'
                },
                {
                    xtype: 'uctextfield',
                    ref: '../category',
                    width: 150,
                    maxLength: 100
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Max Regular Price :'
                },
                {
                    xtype: 'numericfield',
                    ref: '../price',
                    width: 75,
                    value: 0,
                    minValue: 0
                },
                {
                    xtype: 'button',
                    text: 'Add',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Del',
                    ref: '../btnDelete'
                }
            ]
        };
        jun.PaketDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        jun.rztBarangPaketCmp.on('load',this.barangPaketLoad,this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on("close", this.onWinClose, this);
    },
    onWinClose: function(){
        jun.rztBarangPaketCmp.un('load',this.barangPaketLoad,this);
    },
    barangPaketLoad: function(){
        this.sm.unlock();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (selectedz == undefined) {
            return;
        }
        Ext.getCmp('paket_details_id').setValue(this.record.data.paket_details_id);
        jun.rztBarangPaketCmp.baseParams = {
            price: this.record.data.price
        };
        jun.rztBarangPaketCmp.load();
        jun.rztBarangPaketCmp.baseParams = {};
        jun.rztPaketBarang.baseParams = {
            paket_details_id: this.record.data.paket_details_id
        };
        jun.rztPaketBarang.load();
        this.sm.lock();
    },
    loadForm: function () {
        var kategori_name = this.category.getValue();
        var price = parseFloat(this.price.getValue());
        if (kategori_name == "" || kategori_name == undefined) {
            Ext.MessageBox.alert("Error", "Category name must not empty.");
            return;
        }
        if (price === 0) {
            Ext.MessageBox.alert("Error", "Price must greater than 0.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('kategori_name', kategori_name);
            record.set('price', price);
            record.commit();
        } else {
            var c = jun.rztPaketDetails.recordType,
                d = new c({
                    kategori_name: kategori_name,
                    price: price
                });
            jun.rztPaketDetails.add(d);

        }
        this.category.reset();
        this.price.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.category.setValue(record.data.kategori_name);
            this.price.setValue(record.data.price);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.paket_details_id;
        var form = new jun.PaketDetailsWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == "") {
            Ext.MessageBox.alert("Warning", "Item must selected.");
            return;
        }
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        this.store.remove(record);
    }
});
jun.PaketDetailsTransGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PaketDetailsTrans",
    id: 'docs-jun.PaketDetailsTransGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Category',
            sortable: true,
            resizable: true,
            dataIndex: 'kategori_name',
            width: 100
        },
        {
            header: 'Item Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 75,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Item Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarang
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ketpot',
            width: 100
        }
    ],
    initComponent: function () {
        this.store = jun.rztTransPaketDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    text: '\xA0Item :\xA0'
                },
                {
                    xtype: 'combo',
                    //typeAhead: true,
                    triggerAction: 'all',
                    lazyRender: true,
                    mode: 'local',
                    forceSelection: true,
                    store: jun.rztBarangPaketTransCmp,
                    ref: '../barang',
                    valueField: 'barang_id',
                    displayField: 'kode_barang'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Note :'
                },
                {
                    xtype: 'textfield',
                    id: 'ketpotpaketid',
                    ref: '../ketpot',
                    width: 200,
                    maxLength: 255
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                }
            ]
        };
        jun.PaketDetailsTransGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        jun.rztBarangPaketTransCmp.on('load',this.barangPaketLoad,this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.on("close", this.onWinClose, this);
    },
    onWinClose: function(){
        jun.rztBarangPaketTransCmp.un('load',this.barangPaketLoad,this);
    },
    barangPaketLoad: function(){
        this.sm.unlock();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
        if (selectedz == undefined) {
            return;
        }
        jun.rztBarangPaketTransCmp.baseParams = {
            paket_details_id: this.record.data.paket_details_id
        };
        jun.rztBarangPaketTransCmp.load();
        jun.rztBarangPaketTransCmp.baseParams = {};
        this.sm.lock();
        this.barang.reset();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        var ketpot = this.ketpot.getValue();
        if (barang_id == "" || barang_id == undefined) {
            Ext.MessageBox.alert("Error", "Product/Treatment name must not empty.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('ketpot', ketpot);
            record.commit();
        }
        this.barang.reset();
        this.ketpot.reset();
    },
    btnDisable: function (s) {
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
            this.ketpot.setValue(record.data.ketpot);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    }
});