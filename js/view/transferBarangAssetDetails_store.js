jun.TransferBarangAssetDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferBarangAssetDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferBarangAssetDetailsStoreId',
            url: 'TransferBarangAssetDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'transfer_asset_details_id'},
                {name: 'qty'},
                {name: 'barang_asset_id'},
                {name: 'transfer_asset_id'},
                {name: 'price'},
                {name: 'total'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'bruto'},
                {name: 'vat'},
                {name: 'vatrp'},
                {name: 'disc1'},
                {name: 'discrp1'},
                {name: 'total_pot'},
            ]
        }, cfg));
    },
    refreshTotal: function () {
//        var totalBruto=0,totalVat=0;
//
//        this.each(function(r){
//            qty = Number(r.get('qty'));
//            price = Number(r.get('price'));
//            vat = Number(r.get('vat'));
//
//            totalBruto += (qty * price);
//            totalVat += (qty * price * vat/100);
//        });
        Ext.getCmp('TransferBarangAsset_totalbrutoid').setValue(this.sum('bruto'));
        Ext.getCmp('TransferBarangAsset_totalvatid').setValue(this.sum('vatrp'));
        Ext.getCmp('TransferBarangAsset_totalpriceid').setValue(this.sum('total'));
    }
});
jun.rztTransferBarangAssetDetails = new jun.TransferBarangAssetDetailsstore();
//jun.rztTransferBarangAssetDetails.load();
