jun.Realizationstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Realizationstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'RealizationStoreId',
            url: 'projection/Realization',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'realization_id'},
                {name:'account_id'},
                {name:'account_code'},
                {name:'account_name'},
                {name:'amount'},
                {name:'description'},
                {name:'tdate'},
                {name:'tdatef'},
                {name:'created_at'},
                {name:'businessunit_id'},
                {name:'uploadfile'},
                {name:'uploaddate'},
                
            ]
        }, cfg));
    }
});
jun.rztRealization = new jun.Realizationstore();
//jun.rztRealization.load();
