jun.Tagstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Tagstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TagStoreId',
            url: 'Tag',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'tag_id'},
                {name: 'tag_name'},
                {name: 'security_roles_id'}

            ]
        }, cfg));
    }
});
jun.rztTag = new jun.Tagstore();
jun.rztTagLib = new jun.Tagstore();
//jun.rztTag.load();
