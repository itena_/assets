jun.JualGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Selling Price",
    id: 'docs-jun.JualGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Item Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderKodeBarang,
            filter: {xtype: "textfield", filterName: "kode_barang"}
        },
        {
            header: 'Item Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarang,
            filter: {xtype: "textfield", filterName: "nama_barang"}
        },
        {
            header: 'Price',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            filter: {xtype: "numericfield"}
        },
        {
            header: 'Disc (%)',
            sortable: true,
            resizable: true,
            dataIndex: 'disc',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            filter: {xtype: "numericfield"}
        },
        {
            header: 'Disc (Rp)',
            sortable: true,
            resizable: true,
            dataIndex: 'discrp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            filter: {xtype: "numericfield"}
        },
        {
            header: 'Unit',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 100,
            renderer: jun.renderBarangSat,
            filter: {xtype: "textfield", filterName: "sat"}
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100,
            filter: {xtype: "textfield"}
        }
    ],
    initComponent: function () {
        this.store = jun.rztJual;
        jun.rztBarangLib.on({
            scope: this,
            load: {
                fn: function (a, b, c) {
                    this.store.baseParams = {mode: "grid"};
                    this.store.reload();
                    this.store.baseParams = {};
                }
            }
        });
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Selling Price',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Selling Price',
                    ref: '../btnEdit'
                }
            ]
        };
        jun.JualGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.JualWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.jual_id;
        var form = new jun.JualWin({modez: 1, jual_id: this.record.data.jual_id});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    }
});
