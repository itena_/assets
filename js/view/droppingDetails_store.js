jun.DroppingDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.DroppingDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'DroppingDetailsStoreId',
            url: 'DroppingDetails',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'dropping_detail_id'},
                {name: 'qty'},
                {name: 'barang_id'},
                {name: 'visible'},
                {name: 'dropping_id'},
                {name: 'price'}
            ]
        }, cfg));
    }
});
jun.rztDroppingDetails = new jun.DroppingDetailsstore();
//jun.rztDroppingDetails.load();
