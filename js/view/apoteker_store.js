jun.Apotekerstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Apotekerstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'ApotekerStoreId',
            url: 'Apoteker',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'apoteker_id'},
                {name: 'nama_apoteker'},
                {name: 'gol_id'},
                {name: 'kode_apoteker'},
                {name: 'active'},
                {name: 'store'},
                {name: 'up'},
                {name: 'tipe'}
            ]
        }, cfg));
    }
});
jun.rztApoteker = new jun.Apotekerstore();
jun.rztApotekerLib = new jun.Apotekerstore();
jun.rztApotekerCmp = new jun.Apotekerstore();
//jun.rztApoteker.load();
