jun.TransaksiDetailGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"TransaksiDetail",
        id:'docs-jun.TransaksiDetailGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
        /*                {
			header:'transaksi_detail_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'transaksi_detail_id',
			width:100
		},
                                {
			header:'transaksi_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'transaksi_id',
			width:100
		},
                                {
			header:'outlet_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'outlet_id',
			width:100
		},
                                {
			header:'produk_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'produk_id',
			width:100
		},
                                {
			header:'groupproduk_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'groupproduk_id',
			width:100
		},
                                {
			header:'unit_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'unit_id',
			width:100
		},*/
         /*
                {
			header:'businessunit_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'businessunit_id',
			width:100
		},*/
         /*                       {
			header:'namaoutlet',
			sortable:true,
			resizable:true,                        
            dataIndex:'namaoutlet',
			width:100
		},*/

        {
			header:'Product',
			sortable:true,
			resizable:true,                        
            dataIndex:'kodeproduk',
			width:100
		},
                                {
			header:'Product Group',
			sortable:true,
			resizable:true,                        
            dataIndex:'kodegroupproduk',
			width:100
		},

        /*                        {
			header:'hargabeli',
			sortable:true,
			resizable:true,                        
            dataIndex:'hargabeli',
			width:100
		},*/
        {
            header:'Outlet',
            sortable:true,
            resizable:true,
            dataIndex:'kodeoutlet',
            width:100
        },
                                {
			header:'Num Of Outlet',
			sortable:true,
			resizable:true,                        
            dataIndex:'jmloutlet',
			width:100
		},
                                {
			header:'%',
			sortable:true,
			resizable:true,                        
            dataIndex:'persentase',
			width:100
		},
        {
            header:'Price',
            sortable:true,
            resizable:true,
            dataIndex:'harga',
            width:100
        },
                                {
			header:'Qty',
			sortable:true,
			resizable:true,                        
            dataIndex:'qty',
			width:100
		},
        /*                        {
			header:'satuan',
			sortable:true,
			resizable:true,                        
            dataIndex:'satuan',
			width:100
		},*/
                                {
			header:'Sales Qty',
			sortable:true,
			resizable:true,                        
            dataIndex:'salesqty',
			width:100
		},
                                {
			header:'Sales Rp',
			sortable:true,
			resizable:true,                        
            dataIndex:'salesrp',
			width:100
		}
         /*                       {
			header:'salesrpbeli',
			sortable:true,
			resizable:true,                        
            dataIndex:'salesrpbeli',
			width:100
		},*/
/*                                {
			header:'tdate',
			sortable:true,
			resizable:true,                        
            dataIndex:'tdate',
			width:100
		},
                                {
			header:'created_at',
			sortable:true,
			resizable:true,                        
            dataIndex:'created_at',
			width:100
		},*/
         /*                       {
			header:'uploadfile',
			sortable:true,
			resizable:true,                        
            dataIndex:'uploadfile',
			width:100
		},
                                {
			header:'uploaddate',
			sortable:true,
			resizable:true,                        
            dataIndex:'uploaddate',
			width:100
		},*/

		
	],
	initComponent: function(){

	this.store = jun.rztTransaksiDetail;
        /*this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };*/
        if(!this.readOnly){
            this.tbar = {
                xtype: 'toolbar',
                id: 'toolbardetail',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 8,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Product Group :'
                            },
                            {
                                xtype: 'combo',
                                //typeAhead: true,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                store: jun.rztGroupproduk,
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<h3><span">{kode} - {nama}</span></h3>',
                                    "</div></tpl>"),
                                hiddenName: 'groupproduk_id',
                                //name: 'store',
                                valueField: 'groupproduk_id',
                                displayField: 'kode',
                                allowBlank: false,
                                readOnly: !HEADOFFICE,
                                ref: '../../groupproduk',
                                emptyText: "Product Group",
                                width: 160
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Product :'
                            },
                            {
                                xtype: 'combo',
                                typeAhead: true,
                                triggerAction: 'all',
                                lazyRender:true,
                                mode: 'local',
                                forceSelection: true,
                                //fieldLabel: 'Class',
                                store: new jun.Produkstore(),
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<h3><span">{kodeproduk} - {nama}</span></h3>',
                                    "</div></tpl>"),
                                hiddenName:'produk_id',
                                valueField: 'produk_id',
                                allowBlank: true,
                                displayField: 'kodeproduk',
                                emptyText: "Product",
                                ref: '../../produk',
                                width: 160,
                                listWidth: 300
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty :'
                            },
                            {
                                xtype: 'numericfield',
                                id: 'qtyid',
                                ref: '../../qty',
                                width: 80,
                                value: 1,
                                minValue: 0
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Percentage :'
                            },
                            {
                                xtype: 'numericfield',
                                id: 'persentaseid',
                                ref: '../../persentase',
                                width: 40,
                                value:0,
                                minValue: 0
                            },
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        defaults: {
                            scale: 'large'
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Add',
                                height: 44,
                                ref: '../../btnAdd'
                            },
                            {
                                xtype: 'button',
                                text: 'Edit',
                                height: 44,
                                ref: '../../btnEdit'
                            },
                            {
                                xtype: 'button',
                                text: 'Del',
                                height: 44,
                                ref: '../../btnDelete'
                            }
                        ]
                    }
                ]
            };
        }
		    jun.TransaksiDetailGrid.superclass.initComponent.call(this);
	        this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.onClickbtnEdit, this);
            this.btnDelete.on('Click', this.deleteRec, this);
            this.getSelectionModel().on("rowselect", this.getrow, this);
            this.store.removeAll();
            this.groupproduk.on('Select', this.groupprodukonselect, this);
	    },

    groupprodukonselect: function(c,r,i)
    {
        this.produk.store.reload({params:{groupproduk_id:r.get('groupproduk_id')}});
    },

        btnDisable: function (s) {
            this.btnAdd.setDisabled(s);
            this.btnDelete.setDisabled(s);
            if (s) {
                this.sm.lock();
            } else {
                this.sm.unlock();
            }
        },

        getrow: function (a, b, c) {
            this.record = c;
            var d = this.sm.getSelections();
        },

        onClickbtnEdit: function(btn){
            var record = this.sm.getSelected();
            if (record == undefined) {
                Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
                return;
            }
            if (btn.text == 'Edit') {
                this.groupproduk.setValue(record.data.groupproduk_id);
                this.produk.setValue(record.data.produk_id);
                this.qty.setValue(record.data.qty);
                this.persentase.setValue(record.data.persentase);
                btn.setText("Save");
                this.btnDisable(true);
            } else {
                this.loadForm();
                btn.setText("Edit");
                this.btnDisable(false);
            }
        },

        loadForm: function(){
            var produk_id = this.produk.getValue();
            var qty = parseFloat(this.qty.getValue());
            var persentase = parseFloat(this.persentase.getValue());
            var groupproduk_id = this.groupproduk.getValue();//this.parent.groupproduk.getValue();
            var outlet_id = this.parent.outlet.getValue();

            if (produk_id == "") {
                Ext.MessageBox.alert("Error", "You have not selected a item");
                return
            }

            //      kunci barang sama tidak boleh masuk dua kali
            if (this.btnEdit.text != 'Save') {
                var a = jun.rztTransaksiDetail.findExact("produk_id", produk_id);
                if (a > -1) {
                    Ext.MessageBox.alert("Error", "Item already inputted");
                    return
                }
            }

            var p = this.produk.store.find('produk_id',produk_id);
            var gp = this.groupproduk.store.find('groupproduk_id',groupproduk_id);
            var o = this.parent.outlet.store.find('outlet_id',outlet_id);
            var rproduk = this.produk.store.getAt(p);
            var rprodukgroup = this.groupproduk.store.getAt(gp);
            var routlet = this.parent.outlet.store.getAt(o);

            var harga = rproduk.get('harga');
            var hargabeli =rproduk.get('hargabeli');
            var jmloutlet = routlet.get('jumlahoutlet');

            var slsqty = ((persentase/100)*jmloutlet)*qty;
            var slsrp = harga*slsqty;
            var slsrpbeli = hargabeli*slsqty;

            if (this.btnEdit.text == 'Save')
            {
                var record = this.sm.getSelected();
                record.set('produk', produk_id);
                record.set('qty', qty);
                record.set('persentase', persentase);
                record.commit();
            } else {
                var c = jun.rztTransaksiDetail.recordType,
                    d = new c({
                        produk_id: produk_id,
                        groupproduk_id:groupproduk_id,
                        outlet_id:outlet_id,
                        qty: qty,
                        persentase:persentase,
                        kodeproduk:rproduk.get('kodeproduk'),
                        harga:harga,
                        hargabeli:hargabeli,
                        kodeoutlet:routlet.get('kodeoutlet'),
                        namaoutlet:routlet.get('namaoutlet'),
                        jmloutlet:jmloutlet,
                        kodegroupproduk:rprodukgroup.get('kode'),
                        salesqty:slsqty,
                        salesrp:slsrp,
                        salesrpbeli:slsrpbeli,
                    });
                jun.rztTransaksiDetail.add(d);
            }
            // this.store.reset();
            this.produk.reset();
            this.qty.reset();
            this.persentase.reset();
        },


        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(a){

            if (a == "no") return;
            var b = this.sm.getSelected();
            if (b == undefined) {
                Ext.MessageBox.alert("Warning", "Anda Belum Memilih Item");
                return;
            }
            this.store.remove(b);
        
        }
})
