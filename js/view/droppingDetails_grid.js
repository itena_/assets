var selectModel = new Ext.grid.CheckboxSelectionModel();

jun.DroppingDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "DroppingDetails",
    id: 'docs-jun.DroppingDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: selectModel,
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [selectModel,
        {
            header: 'Items Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 150,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Items Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 250,
            renderer: jun.renderBarang
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            align: 'right',
            width: 100,
            renderer: Ext.util.Format.numberRenderer('0,0')
        }
    ],
    initComponent: function () {
        this.store = jun.rztDroppingDetails;

        if(!this.readOnly){
            this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'buttongroup',
                        columns: 6,
                        defaults: {
                            scale: 'small'
                        },
                        items: [
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Item :'
                            },
                            {
                                xtype: 'mfcombobox',
                                searchFields: [
                                    'kode_barang',
                                    'nama_barang'
                                ],
                                typeAhead: false,
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                itemSelector: "div.search-item",
                                tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<span style="font-weight:bold;">{kode_barang}</span><br><span>{nama_barang}</span>',
                                    "</div></tpl>"),
                                forceSelection: true,
                                store: jun.rztBarangNonJasaAll,
                                hiddenName: 'barang_id',
                                valueField: 'barang_id',
                                ref: '../../barang',
                                displayField: 'kode_barang',
                                listWidth: 300
                            },
                            {
                                xtype: 'label',
                                style: 'margin:5px',
                                text: 'Qty :'
                            },
                            {
                                xtype: 'numericfield',
                                id: 'qtyid_transfer',
                                ref: '../../qty',
                                width: 50,
                                minValue: 0
                            }
                        ]
                    },
                    {
                        xtype: 'buttongroup',
                        columns: 3,
                        defaults: {
                            scale: 'small',
                            width: 40
                        },
                        items: [
                            {
                                xtype: 'button',
                                text: 'Add',
                                ref: '../../btnAdd'
                            },
                            {
                                xtype: 'button',
                                text: 'Edit',
                                ref: '../../btnEdit'
                            },
                            {
                                xtype: 'button',
                                text: 'Del',
                                ref: '../../btnDelete'
                            }
                        ]
                    }
                ]
            };
        }
        jun.DroppingDetailsGrid.superclass.initComponent.call(this);
        if(!this.readOnly){
            this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.loadEditForm, this);
            this.btnDelete.on('Click', this.deleteRec, this);
            this.getSelectionModel().on('rowselect', this.getrow, this);
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "You have not selected a item");
            return
        }
        var qty = parseFloat(!this.qty.getValue()?0:this.qty.getValue());
        if (qty <= 0) {
            Ext.MessageBox.alert("Error", "Quantity must be greater than zero.");
            return
        }

        if (this.btnEdit.text != 'Save') {
            var a = this.store.findExact("barang_id", barang_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Item already inputted");
                return
            }
        }

        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelections();
            record[0].set('barang_id',barang_id);
            record[0].set('qty',qty);
            record[0].commit();
        } else {
            var c = jun.rztDroppingDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty
                });
            jun.rztDroppingDetails.add(d);
        }
        // this.store.reset();
        this.barang.reset();
        this.qty.reset();
    },
    loadEditForm: function (btn) {
        var record = this.sm.getSelections();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record[0].data.barang_id);
            this.qty.setValue(record[0].data.qty);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }

        var gridSel = this.sm.getSelections();
        //var count = 0;
        for(var i=0; i<gridSel.length;i++){
            this.store.remove(gridSel[i]);
        }

        // var record = this.sm.getSelected();
        // // Check is list selected
        // if (record == undefined) {
        //     Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
        //     return;
        // }
        // this.store.remove(record);
    }
});
