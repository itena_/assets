jun.TransferItemWin = Ext.extend(Ext.Window, {
    title: 'Purchase Item',
    modez: 1,
    width: 1015,
    height: 600,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-TransferItem',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Due Date:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_jatuh_tempo',
                        fieldLabel: 'tgl',
                        name: 'tgl_jatuh_tempo',
                        format: 'd M Y',
                        //allowBlank: false,
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Supplier:",
                        x: 295+20,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        ref: '../supplier',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztSupplierCmp,
                        emptyText: "Please select supplier",
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        readOnly: true,
                        width: 175,
                        x: 400+20,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Invoice No:",
                        x: 295+20,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref_other',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref_other',
                        ref: '../doc_ref_other',
                        //allowBlank: false,
                        maxLength: 50,
                        width: 175,
                        x: 400+20,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "No. Faktur Pajak:",
                        x: 295+20,
                        y: 65
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        name: 'no_fakturpajak',
                        ref: '../no_fakturpajak',
                        //allowBlank: false,
                        maxLength: 50,
                        width: 175,
                        x: 400+20,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 590+60,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'note',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'note',
                        ref: '../note',
                        width: 275,
                        height: 60,
                        x: 590+60,
                        y: 22
                    },
                    new jun.TransferItemDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 340,
                        frameHeader: !1,
                        header: !1,
                        ref:"../griddetils",
                        readOnly: (this.modez < 2)?false:true,
                        store: this.storeGridDetil
                    }),
                    {
                        xtype: 'hidden',
                        name: 'store',
                        ref:"../store",
                        value: STORE
                    },
                    {
                        xtype: 'hidden',
                        name: 'type_',
                        ref:"../type_",
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        name: "total_dpp",
                        id: 'total_dpp_id'
                    },
                    {
                        frame: false,
                        x: 5,
                        y: 438,
                        layout: 'column',
                        bodyStyle: "background-color: #E4E4E4;",
                        //border: false,
                        defaults: {
                            border: false
                        },
                        items: [
                            {
                                xtype: "panel",
                                columnWidth: .33,
                                frame: false,
                                layout: 'form',
                                bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                items: [
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Sub Total',
                                        name: 'sub_total',
                                        id: 'sub_total_id',
                                        ref: '../../../sub_total',
                                        anchor: '100%',
                                        value: 0,
                                        readOnly: true
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Disc (%)',
                                        name: 'disc',
                                        id: 'disc_id',
                                        ref: '../../../disc',
                                        anchor: '100%',
                                        value: 0
                                    }
                                ]
                            },
                            {
                                xtype: "panel",
                                columnWidth: .33,
                                frame: false,
                                layout: 'form',
                                bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                items: [
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Total Disc',
                                        name: 'total_disc_rp',
                                        id: 'total_disc_rp_id',
                                        ref: '../../../total_disc_rp',
                                        anchor: '100%',
                                        value: 0,
                                        readOnly: true
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Total PPN',
                                        name: 'tax_rp',
                                        id: 'tax_rp_id',
                                        ref: '../../../tax_rp',
                                        anchor: '100%',
                                        value: 0,
                                        readOnly: true
                                    }
                                ]
                            },
                            {
                                xtype: "panel",
                                columnWidth: .33,
                                frame: false,
                                layout: 'form',
                                bodyStyle: "background-color: #E4E4E4;padding: 10px;",
                                items: [
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Total PPH',
                                        name: 'total_pph_rp',
                                        id: 'total_pph_rp_id',
                                        ref: '../../../total_pph_rp',
                                        anchor: '100%',
                                        value: 0,
                                        readOnly: true
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Total',
                                        name: 'total',
                                        id: 'totalpr_id',
                                        ref: '../../../total',
                                        anchor: '100%',
                                        value: 0,
                                        readOnly: true
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Print',
                    ref: '../btnSavePrint'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TransferItemWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnSavePrint.on('click', this.onbtnSavePrintclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
//        this.disc.on('keyup', this.onDiscChange, this);
        this.on("close", this.onWinClose, this);
        
        //this.griddetils.store.on('load', this.griddetils.store.calculateItem,  this.griddetils.store);
        this.griddetils.store.on('add', this.griddetils.store.refreshData,  this.griddetils.store);
        this.griddetils.store.on('update', this.griddetils.store.refreshData,  this.griddetils.store);
        this.griddetils.store.on('remove', this.griddetils.store.refreshData,  this.griddetils.store);
        
        this.btnCancel.setText(this.modez < 2? 'Cancel':'Close');
        switch(this.modez) {
            case 0:
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(true);
                break;
            case 1 :
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(true);
                break;
            default :
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(false);
                this.formz.getForm().applyToFields({ readOnly: true });
                break;
        }
    },
    onDiscChange: function (t, newValue, oldValue) {
        if(!this.disc.getValue()) this.disc.setValue(0);
        this.griddetils.store.each(function (record) {
            var disc = parseFloat(newValue);
            var sub_total = round(record.data.qty * record.data.price,2);
            var disc_rp = round(sub_total * (disc / 100),2);
            var total_disc = disc_rp;
            var total_dpp = sub_total - total_disc;
            var ppn_rp = round(total_dpp * (record.data.ppn / 100),2);
            var pph_rp = round(total_dpp * (record.data.pph / 100),2);
            var total = total_dpp + ppn_rp - pph_rp;
            record.set('sub_total', sub_total);
            record.set('total_dpp', total_dpp);
            record.set('disc', disc);
            record.set('disc_rp', disc_rp);
            record.set('ppn_rp', ppn_rp);
            record.set('pph_rp', pph_rp);
            record.set('total', total);
            record.commit();
        });
    },
    onWinClose: function () {
        this.griddetils.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function (print) {
        this.btnDisabled(true);
        if (this.griddetils.store.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            return;
        }
        
        Ext.getCmp('form-TransferItem').getForm().submit({
            url: (this.type_.getValue()==1?'TransferItem/CreateOut/':'TransferItem/CreateIn/'),
            scope: this,
            params: {
                mode: this.modez,
                transfer_item_id: this.transfer_item_id,
                terima_barang_id: this.terima_barang_id,
                detil: Ext.encode(Ext.pluck(
                    this.griddetils.store.data.items, "data")),
                print: print
            },
            success: function (f, a) {
                if(this.modez==0)
                    if(Ext.getCmp('docs-jun.TerimaBarangGrid') !== undefined) Ext.getCmp('docs-jun.TerimaBarangGrid').botbar.doRefresh();
                else
                    if(Ext.getCmp('docs-jun.TransferItemGrid') !== undefined) Ext.getCmp('docs-jun.TransferItemGrid').botbar.doRefresh();
                
                var response = Ext.decode(a.response.responseText);

                if(response.transfer_item_id){
                    this.parent.printInvoice(response.transfer_item_id);
                }

                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TransferItem').getForm().reset();
                    this.griddetils.store.removeAll();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(false);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnSavePrintclick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});

jun.ReturnTransferItemWin = Ext.extend(Ext.Window, {
    title: 'Return Supplier Item',
    modez: 1,
    width: 605,
    height: 445,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-ReturnTransferItem',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        width: 175,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Supplier:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        ref: '../supplier',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztSupplierCmp,
                        emptyText: "Please select supplier",
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        allowBlank: true,
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'note',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        width: 170,
                        x: 400,
                        y: 2,
                        height: 52
                    },
                    {
                        xtype: "label",
                        text: "Doc. Ref Purc:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref_other',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref_other',
                        ref: '../doc_ref_other',
                        maxLength: 50,
                        width: 170,
                        x: 400,
                        y: 62
                    },
                    new jun.ReturnTransferItemDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 260,
                        frameHeader: !1,
                        header: !1,
                        ref:"../griddetils",
                        readOnly: (this.modez < 2)?false:true,
                        store: this.storeGridDetil
                    }),
                    {
                        xtype: 'hidden',
                        name: 'store',
                        ref:"../store",
                        value: STORE
                    },
                    {
                        xtype: 'hidden',
                        name: 'type_',
                        ref:"../type_",
                        value: 1
                    },
                    {
                        xtype: 'hidden',
                        ref: '../disc',
                        name: 'disc',
                        value: 0
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ReturnTransferItemWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        
        this.btnCancel.setText(this.modez < 2? 'Cancel':'Close');
        switch(this.modez) {
            case 0:
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(true);
                break;
            case 1 :
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(true);
                break;
            default :
                this.btnSave.setVisible(false);
                this.btnSaveClose.setVisible(false);
                this.formz.getForm().applyToFields({ readOnly: true });
                break;
        }
    },
    onWinClose: function () {
        this.griddetils.store.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (this.griddetils.store.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            return;
        }
        
        Ext.getCmp('form-ReturnTransferItem').getForm().submit({
            url: 'TransferItem/CreateOut/',
            scope: this,
            params: {
                mode: this.modez,
                transfer_item_id: this.transfer_item_id,
                terima_barang_id: this.terima_barang_id,
                detil: Ext.encode(Ext.pluck(
                    this.griddetils.store.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztReturnTransferItem.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-ReturnTransferItem').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});