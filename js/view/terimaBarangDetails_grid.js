var selectModel = new Ext.grid.CheckboxSelectionModel();
jun.TerimaBarangDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "TerimaBarangDetails",
    id: 'docs-jun.TerimaBarangDetailsGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: selectModel,//new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [selectModel,
        // {
        //     header: 'Items Code',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'barang_id',
        //     width: 80,
        //     renderer: jun.renderKodeBarang
        // },
        // {
        //     header: 'Items Name',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'barang_id',
        //     width: 160,
        //     renderer: jun.renderBarang
        // },
        {
            header: 'Items Code',
            sortable: true,
            resizable: true,
            dataIndex: 'kode_barang',
            width: 80
            // renderer: jun.renderKodeBarang
        },
        {
            header: 'Items Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama_barang',
            width: 160
            // renderer: jun.renderBarang
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 40,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'description',
            width: 60
        }
    ],
    initComponent: function () {
        this.store = jun.rztTerimaBarangDetails;
        if(!this.readOnly){
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Barang :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            store: jun.rztBarangPurchasable,
                            hiddenName: 'barang_id',
                            valueField: 'barang_id',
                            displayField: 'kode_barang',
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<span style="width:100%;display:inline-block;"><b>{kode_barang}</b><br>{nama_barang}</span>',
                                "</div></tpl>"),
                            readOnly: true,
                            ref: '../../barang'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'totalid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            width: 80,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'uctextfield',
                            ref: '../../description',
                            width: 320,
                            colspan: 3,
                            maxLength: 100
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 2,
                    defaults: {
                        scale: 'large',
                        width: 40
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Edit',
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        }
        jun.TerimaBarangDetailsGrid.superclass.initComponent.call(this);
        if(!this.readOnly){
            // this.btnAdd.on('Click', this.loadForm, this);
            this.btnEdit.on('Click', this.onClickbtnEdit, this);
            this.btnDelete.on('Click', this.deleteRec, this);
        }
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadEditForm: function () {
        var qty = this.qty.getValue();
        var description = this.description.getValue();

        if (qty < 0) {
            Ext.MessageBox.alert("Error", "Qty harus minimal 0.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelections();
            record[0].set('qty', qty);
            record[0].set('description', description);
            record[0].commit();
        } else {
            var c = jun.rztTerimaBarangDetails.recordType,
                d = new c({
                    qty: qty,
                    description: description
                });
            jun.rztTerimaBarangDetails.add(d);

        }
        this.barang.reset();
        this.qty.reset();
        this.description.reset();
    },
    btnDisable: function (s) {
        // this.btnEdit.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelections();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.barang.setValue(record[0].data.barang_id);
            this.qty.setValue(record[0].data.qty);
            this.description.setValue(record[0].data.description);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadEditForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }

        var gridSel = this.sm.getSelections();
        //var count = 0;
        for(var i=0; i<gridSel.length;i++){
            this.store.remove(gridSel[i]);
        }
        // var record = this.sm.getSelected();
        // if (record == undefined) {
        //     Ext.MessageBox.alert("Warning", "Anda belum memilih data.");
        //     return;
        // }
        // this.store.remove(record);
    }
});
