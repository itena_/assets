jun.PaymentWin = Ext.extend(Ext.Window, {
    title: 'Payment',
    modez: 1,
    id: 'form-PaymentWin',
    width: 400,
    height: 200,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    //defaultButton: 'paymentbankid',
    closeAction: 'hide',
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Payment',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Payment Method',
                        store: jun.rztBankCmp,
                        ref: '../bank',
                        id: 'paymentbankid',
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Card Number',
                        hideLabel: false,
                        //hidden:true,
                        name: 'card_number',
                        id: 'card_numberid',
                        ref: '../card_number',
                        enableKeyEvents: true,
                        maxLength: 16,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Card Type',
                        store: jun.rztCardCmp,
                        ref: '../card',
                        hiddenName: 'card_id',
                        valueField: 'card_id',
                        displayField: 'card_name',
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Amount',
                        hideLabel: false,
                        //hidden:true,
                        name: 'amount',
                        id: 'amountid',
                        ref: '../amount',
                        maxLength: 30,
                        value: 0,
                        anchor: '100%'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Payment',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Close',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PaymentWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.bank.on('select', this.onBankChange, this);
        this.card_number.on("specialkey", this.onCardnumber, this);
        this.on("activate", this.onActive, this);
        this.on("hide", this.onClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSaveClose.setVisible(true);
        }
    },
    onClose: function () {
        Ext.getCmp('win-Salestrans').statusActive = 2;
    },
    onActive: function () {
        this.bank.focus(false, 50);
        this.btnDisabled(false);
    },
    onCardnumber: function (f, e) {
        if (e.getKey() == e.ENTER) {
            var val = this.card_number.getValue();
            if (val == "" || val == undefined) {
                return;
            }
            var p = new SwipeParserObj(val);
            this.card_number.setValue(p.account);
            Ext.getCmp('doc_refid').focus(false, 100);
        }
    },
    onBankChange: function () {
        var bank = this.bank.getValue();
        if (bank == "" || bank == undefined) {
            return;
        }
        if (bank == SYSTEM_BANK_CASH) {
            this.card_number.reset();
            this.card.reset();
            this.card_number.setDisabled(true);
            this.card.setDisabled(true);
        } else {
            this.card_number.setDisabled(false);
            this.card.setDisabled(false);
        }
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var total = parseFloat(Ext.getCmp("totalid").getValue());
        var bayar = parseFloat(Ext.getCmp("bayarid").getValue());
        var kembali = parseFloat(Ext.getCmp("kembaliid").getValue());
        var kembalian = 0;
        var ulpt = 0;
        var new_kembali = kembali;
        if ((bayar - total) > 0) {
            Ext.MessageBox.alert("Warning", "Tidak bisa menambahkan pembayaran karena total bayar sudah lebih dari total transaksi.");
            return;
            this.btnDisabled(false);
        }
        var bank = this.bank.getValue();
        if (bank == "" || bank == undefined) {
            return;
            this.btnDisabled(false);
        }
        var card_number = this.card_number.getValue();
        var card_id = this.card.getValue();
        var amount = this.amount.getValue();
        // if (bank == SYSTEM_BANK_CASH) {

        // new_kembali = (bayar + amount) - total;
        // kembalian = new_kembali - kembali;
        // if (kembalian < 0) {
        //     kembalian = 0;
        // }


        // } else {
        //     ulpt = (bayar + amount) - total;
        // }
        Ext.getCmp("kembaliid").setValue(new_kembali);
        var c = Ext.getCmp('docs-jun.PaymentGrid').getStore().recordType,
            d = new c({
                bank_id: bank,
                amount: amount,
                card_number: card_number,
                card_id: card_id,
                ulpt: ulpt,
                kembali: kembalian
            });
        Ext.getCmp('docs-jun.PaymentGrid').getStore().add(d);
        this.btnDisabled(false);
        Ext.getCmp('form-Payment').getForm().reset();
        //this.close();
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});