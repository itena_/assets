jun.PurchaseCoaWin = Ext.extend(Ext.Window, {
    title: 'PurchaseCoa',
    modez: 1,
    width: 1200,
    height: 450,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-PurchaseCoa',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "No.Invoice:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'doc_ref_other',
                        id: 'doc_ref_otherid',
                        ref: '../doc_ref_other',
                        x: 85,
                        y: 32,
                        maxLength: 50
                    },
                    {
                        xtype: "label",
                        text: "Tgl:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        name: 'tgl',
                        id: 'tglid',
                        x: 400,
                        y: 2,
                        format: 'd M Y'
                    },
                    {
                        xtype: "label",
                        text: "Tgl jatuh tempo:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_jatuh_tempo',
                        name: 'tgl_jatuh_tempo',
                        id: 'tgl_jatuh_tempoid',
                        x: 400,
                        y: 32,
                        format: 'd M Y'
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Store:",
                        x: 615,
                        y: 5
                    },
                    {
                        xtype: 'textfield',
                        hideLabel: false,
                        name: 'store',
                        id: 'storeid',
                        ref: '../store',
                        value: STORE,
                        readOnly: true,
                        maxLength: 20,
                        x: 715,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Supplier:",
                        x: 615,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztSupplierCmp,
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        x: 715,
                        y: 32
                    },
                    {
                        xtype: 'tabpanel',
                        bodyStyle: "background: none;",
                        activeTab: 0,
                        anchor: '100%',
                        x: 5,
                        y: 95,
                        height: 260,
                        defaults: {
                            bodyStyle: "background: none;",
                            layout: 'fit',
                            border: false,
                            frame: false
                        },
                        items: [
                            {
                                title: 'Debit',
                                items: new jun.PurchaseCoaDebitGrid({
                                    frameHeader: !1,
                                    header: !1,
                                    ref: "../../../debitGrid"
                                })
                            },
                            {
                                title: 'Kredit',
                                items: new jun.PurchaseCoaKreditGrid({
                                    frameHeader: !1,
                                    header: !1
//                                    ref: "../../../kasDetailPphGrid"
                                })
                            }]},
                    {
                        xtype: 'hidden',
                        name: 'p',
                        ref: '../p'
                    },
                    {
                        xtype: 'hidden',
                        name: 'total',
                        id: 'total_purchase_coaid',
                        ref: '../total'
                    }

                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Posting',
                    hidden: false,
                    ref: '../btnPosting'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.PurchaseCoaWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.btnPosting.on('click', this.onbtnPostingclick, this);
        if (this.modez == 1) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
            this.btnPosting.setVisible(true);
        } else if (this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.btnPosting.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnPosting.setVisible(false);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'PurchaseCoa/update/id/' + this.id;
        } else {
            urlz = 'purchase/PurchaseCoa/create/';
        }
        Ext.getCmp('form-PurchaseCoa').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
//                mode: this.modez,
//                id: this.id,
                detil: Ext.encode(Ext.pluck(
                        jun.rztPurchaseCoaDetail.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztPurchaseCoa.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PurchaseCoa').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnPostingclick: function () {
        this.btnDisabled(true);
        if (this.p.getValue() != 0) {
            Ext.Msg.alert('Failure', 'Data ini sudah pernah di posting!');
            return;
        }
        var urlz = 'purchase/PurchaseCoa/Posting';
        Ext.getCmp('form-PurchaseCoa').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            params: {
                id: this.id
            },
            success: function (f, a) {
                jun.rztPurchaseCoa.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-PurchaseCoa').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});