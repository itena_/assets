jun.PembantuPelunasanUtangDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PembantuPelunasanUtangDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PembantuPelunasanUtangDetilStoreId',
            url: 'PembantuPelunasanUtangDetil',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pembantu_pelunasan_utang_detil_id'},
                {name: 'kas_dibayar'},
                {name: 'no_faktur'},
                {name: 'account_code'},
                {name: 'sisa'},
                {name: 'pembantu_pelunasan_utang_id'},
                {name: 'invoice_journal_id'},
                {name: 'type_'},
                {name: 'supplier_id'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
        
    },
     refreshData: function () {
        var subtotal = this.sum("kas_dibayar");
        Ext.getCmp("totalPelunasanUtangid").setValue(subtotal);
    }
    
});
jun.rztPembantuPelunasanUtangDetil = new jun.PembantuPelunasanUtangDetilstore();
//jun.rztPembantuPelunasanUtangDetil.load();

jun.PembantuFakturUtangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PembantuFakturUtangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PembantuFakturUtangStoreId',
            url: 'PembantuPelunasanUtangDetil/faktur',
            autoLoad: !1,
            autoSave: !1,
            root: 'results',
            idProperty: 'row',
            totalProperty: 'total',
            fields: [
                {name: 'invoice_journal_id'},
                {name: 'supplier_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'no_faktur'},
                {name: 'nilai', type: 'float'},
                {name: 'sisa', type: 'float'}
            ]
        }, cfg));
    }
});
jun.rztPembantuFakturUtang = new jun.PembantuFakturUtangstore();