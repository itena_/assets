jun.PromoGenericstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PromoGenericstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PromoGenericStoreId',
            url: 'PromoGeneric',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'promo_generic_id'},
                {name: 'customer_id'},
                {name: 'event_id'},
                {name: 'qty'},
                {name: 'tdate'},
                {name: 'id_user'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tgl'},
                {name: 'nama_customer'},
                {name: 'store'}
            ]
        }, cfg));
    }
});
jun.rztPromoGeneric = new jun.PromoGenericstore({baseParams: {qty: '> 0'}});
jun.rztPromoGenericOut = new jun.PromoGenericstore({baseParams: {qty: '< 0'}});
//jun.rztPromoGeneric.load();
