jun.TransferBarangDetailsstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.TransferBarangDetailsstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransferBarangDetailsStoreId',
            url: 'TransferBarangDetails',
            root: 'results',
            autoLoad: !1,
            autoSave: !1,
            totalProperty: 'total',
            fields: [
                {name: 'transfer_barang_detail_id'},
                {name: 'qty', type: 'float'},
                {name: 'barang_id'},
                {name: 'transfer_item_id'},
                {name: 'total', type: 'float'},
                {name: 'disc', type: 'float'},
                {name: 'discrp', type: 'float'},
                {name: 'bruto', type: 'float'},
                {name: 'vat', type: 'float'},
                {name: 'vatrp', type: 'float'},
                {name: 'disc1', type: 'float'},
                {name: 'discrp1', type: 'float'},
                {name: 'total_pot', type: 'float'}
            ]
        }, cfg));
    },
    refreshData: function (a) {}
});
jun.rztTransferBarangDetails = new jun.TransferBarangDetailsstore();
//jun.rztTransferBarangDetails.load();
