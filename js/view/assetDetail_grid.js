jun.AssetDetailGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"Assets",
        id:'docs-jun.AssetDetailGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
        /*                {
			header:'asset_trans_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_trans_id',
			width:100
		},
                                {
			header:'asset_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_id',
			width:100
		},
                                {
			header:'asset_group_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_group_id',
			width:100
		},*/
        /*{
            header:'Doc Ref Other',
            sortable:true,
            resizable:true,
            dataIndex:'docref_other',
            width:100
        },*/
         /*                       {
			header:'Doc Ref',
			sortable:true,
			resizable:true,                        
            dataIndex:'docref_other',
			width:100,
            filter: {xtype: "textfield"}
		},*/

        {
            header:'No. Activa',
            sortable:true,
            resizable:true,
            dataIndex:'ati',
            width:90,
            filter: {xtype: "textfield"}
        },
        /*{
            header:'No. Activa Old',
            sortable:true,
            resizable:true,
            dataIndex:'ati_old',
            width:90,
            filter: {xtype: "textfield"}
        },*/

                                {
			header:'Name',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_trans_name',
			width:90,
            filter: {xtype: "textfield"}
		},
        {
            header:'Code',
            sortable:true,
            resizable:true,
            dataIndex:'kode_barang',
            width:50,
            filter: {xtype: "textfield"}
        },
        {
            header:'Description',
            sortable:true,
            resizable:true,
            dataIndex:'description',
            width:80,
            filter: {xtype: "textfield"}
        },
        {
            header:'Category',
            sortable:true,
            resizable:true,
            dataIndex:'category',
            width:60,
            filter: {xtype: "textfield"}
        },
        {
            header:'Branch',
            sortable:true,
            resizable:true,
            dataIndex:'asset_trans_branch',
            width:40,
            filter: {xtype: "textfield"}
        },
                                {
			header:'Price',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_trans_price',
			width:80,
            filter: {xtype: "textfield"}
		},
         /*                       {
			header:'New Price',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_trans_new_price',
			width:100
		},*/

        {
            header:'Class',
            sortable:true,
            resizable:true,
            dataIndex:'class',
            width:30,
            filter: {xtype: "textfield"}
        },
        {
            header:'Tariff (%)',
            sortable:true,
            resizable:true,
            dataIndex:'tariff',
            width:40,
            filter: {xtype: "textfield"}
        },
        {
            header:'Period',
            sortable:true,
            resizable:true,
            dataIndex:'period',
            width:30,
            filter: {xtype: "textfield"}
        },
        /*{
            header:'Penyusutan Perbulan',
            sortable:true,
            resizable:true,
            dataIndex:'penyusutanperbulan',
            width:100
        },
        {
            header:'Penyusutan PerTahun',
            sortable:true,
            resizable:true,
            dataIndex:'penyusutanpertahun',
            width:100
        },*/


        {
            header:'Date',
            sortable:true,
            resizable:true,
            dataIndex:'asset_trans_date',
            width:50,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            filter: {
                xtype: "xdatefield",
                format: 'd/m/Y'
            }
        },
        {
            header:'Hide',
            sortable:true,
            resizable:true,
            dataIndex:'hide',
            width:40,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case 0 :
                        metaData.style += "background-color: #9ffb8a;";
                        return 'SHOW';
                    case 1 :
                        metaData.style += "background-color: #f1f26f;";
                        return 'HIDE';

                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [1, 'HIDE'], [0, 'SHOW']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header: 'Status ',
            sortable: true,
            resizable: true,
            dataIndex: 'status',
            width: 60,
            renderer : function(value, metaData, record, rowIndex){
                switch(Number(value)){
                    case 0 :
                        metaData.style += "background-color: #ff7373;";
                        return 'NON ACTIVE';
                    case 1 :
                        metaData.style += "background-color: #9ffb8a;";
                        return 'ACTIVE';
                    case 2 :
                        metaData.style += "background-color: #f1f26f;";
                        return 'LEND';
                    case 3 :
                        metaData.style += "background-color: #ff7373;";
                        return 'SELL';
                    case 4 :
                        metaData.style += "background-color: #f1f26f;";
                        return 'RENT';
                    case 5 :
                        metaData.style += "background-color: #ff7373;";
                        return 'BROKEN';
                    case 6 :
                        metaData.style += "background-color: #ff7373;";
                        return 'LOST';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender:true,
                editable:false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NON ACTIVE'], [1, 'ACTIVE'], [2, 'LEND'], [3, 'SELL'], [4, 'RENT'], [5, 'BROKEN'], [6, 'LOST']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        },
        {
            header:'Note',
            sortable:true,
            resizable:true,
            dataIndex:'statusdesc',
            width:80,
            filter: {xtype: "textfield"}
        }


		
	],
	initComponent: function(){
	this.store = jun.rztAssetDetail;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    /*{
                        xtype: 'button',
                        text: 'Tambah',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },*/
                    {
                        xtype: 'button',
                        text: 'Add Asset',
                        ref: '../btnAdd',
                        iconCls: "silk13-add",
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Edit Asset',
                        ref: '../btnEditAsset',
                        iconCls: "silk13-application_edit",
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Show Depreciation',
                        ref: '../btnEdit',
                        iconCls: "silk13-layout"
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Show Report Depreciation',
                        ref: '../btnShow',
                        iconCls: "silk13-report"
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Show Report',
                        ref: '../btnShowAsset',
                        iconCls: "silk13-report"
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'HISTORY',
                        ref: '../btnHistory',
                        iconCls: "silk13-time",
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'HIDE',
                        ref: '../btnHide',
                        iconCls: "silk13-contrast",
                    },
                    {
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'STATUS',
                        ref: '../btnSold',
                        iconCls: "silk13-money",
                    }
                    /*
                    {
                        xtype: 'button',
                        text: 'Show Excel',
                        ref: '../btnShowExcel'
                    },*/
                    /*{
                        xtype:'tbseparator'
                    },
                    {
                        xtype: 'button',
                        text: 'Hapus',
                        ref: '../btnDelete'
                    }*/
                ]
            };
                this.store.baseParams = {mode: "grid"};
                //this.store.baseParams = {hide_value: 0};
                this.store.reload();
                //this.store.baseParams = {};
                jun.AssetDetailGrid.superclass.initComponent.call(this);
	            this.btnAdd.on('Click', this.TambahAsset, this);
                this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnEditAsset.on('Click', this.EditAssetForm, this);

                this.btnShow.on('Click', this.ShowForm, this);
                this.btnHide.on('Click', this.ShowHide, this);
                this.btnHistory.on('Click', this.ShowHistory, this);
                this.btnShowAsset.on('Click', this.ShowFormAsset, this);
        this.btnSold.on('Click', this.ActionSold, this);
                //this.btnShowExcel.on('Click', this.ShowExcel, this);
                //this.btnDelete.on('Click', this.deleteRec, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},


        EditAssetForm: function(){

        var selectedz = this.sm.getSelected();
        if(selectedz == undefined){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data");
            return;
        }

        var idz = selectedz.json.asset_trans_id;
        var form = new jun.AssetEditDetailWin({modez:1, id:idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);

    },

        TambahAsset: function(){
        var form = new jun.AssetWin({modez:0});
        form.show();
    },

        ShowHistory: function(){
        //var form = new jun.AssetHistoryWin({modez:0});
        //form.show();

        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih data");
            return;
        }

        var idz = selectedz.json.asset_trans_id;
        //Ext.MessageBox.alert("Warning", idz);

        var form = new jun.AssetHistoryWin({
            modez: 0,
            historyid: {asset_trans_id :idz},
            title: 'Asset History '
        });
        form.show();
    },

        ActionSold : function(){

        var selectedz = this.sm.getSelected();
        if(selectedz == undefined){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data");
            return;
        }

        var price = 0;
        
        var tdate = new Date(selectedz.json.asset_trans_date);
        var tdatem = tdate.getMonth()+1;
        var tdatey = tdate.getFullYear();

        var now = new Date();
        var nowm = now.getMonth()+1;
        var nowy = now.getFullYear();

       /* if(nowm == tdatem && nowy == tdatey)
        {
            price = selectedz.json.asset_trans_price;
        }
        else{*/
            var response_balance; // define here or you won't be able to access it outside of success: f()
            Ext.Ajax.request({
                url: 'asset/AssetDetail/GetBalance/id/' + selectedz.json.asset_trans_id,
                method: 'POST',
                scope: this,
                success:function (f, a) {
                    //jun.rztAssetDetail.reload();
                    response_balance = Ext.decode(f.responseText);
                    price = response_balance.success;
                    Ext.getCmp('assetvalue').setValue(response_balance.success);
                },
                failure:function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        //}

        var idz = selectedz.json.asset_trans_id;
        var form = new jun.AssetStatusWin({modez:1, id:idz, balance:response_balance});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        form.assetvalue.setValue(price);
        /*jun.rztAssetPeriode.baseParams = {
            asset_trans_id: idz
        };
        jun.rztAssetPeriode.load();
        jun.rztAssetPeriode.baseParams = {};*/

        //var form = new jun.AssetJualWin({modez:0});
        //form.show();

        //Ext.MessageBox.prompt('Confirm', 'Masukkan Keterangan Jual', this.Sold, this);
        //Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menjual Asset ini?', this.Sold, this);
    },

        Sold : function(btn, txt){
        if (btn != 'ok') {
            return;
        }//
        if (txt == '') {
            Ext.MessageBox.show({
                title: 'Info',
                msg: 'Masukkan Keterangan Jual. Misal : Telah dijual ke PT.###',
                buttons: Ext.MessageBox.OK,
                icon: Ext.MessageBox.INFO
            });
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Asset");
            return;
        }

        Ext.Ajax.request({
            url: 'asset/AssetDetail/status/id/' + record.json.asset_trans_id,
            method: 'POST',
            scope: this,
            params: {
                keterangan: txt
            },
            success:function (f, a) {
                jun.rztAssetDetail.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    },

        getrow: function (sm, idx, r){
            this.record = r;
            var selectedz = this.sm.getSelections();

            //var filter = r.get('status_scan') == SCAN_MK;
            //this.btnMark.setText(filter ?"Unmark":"Mark");

            var filter = r.get('hide') == 0;
            this.btnHide.setText(filter ?"HIDE":"SHOW");

            var filterSync = r.get('status') == '3';
            //this.btnCancel.setDisabled(filterSync ?"true":"false");
            this.btnSold.setDisabled(filterSync);
            //this.btnPrintOrderDropping.setDisabled((r.get('store_pengirim') != STORE || r.get('lunas') == PR_DRAFT));
        },
        
        loadForm: function(){
            var form = new jun.AssetDetailWin({modez:0});
            form.show();
        },

        ShowForm: function(){

            var selectedz = this.sm.getSelected();
            if(selectedz == undefined){
                Ext.MessageBox.alert("Warning","Anda belum memilih Data");
                return;
            }
            var idz = selectedz.json.asset_trans_id;
            var form = new jun.ReportAssetDepriciation({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);

        },


        ShowHide: function(){

        var record = this.sm.getSelected();

        // Check is list selected
        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
            return;
        }

        var hide = record.json.hide;
        if(hide == '0')
        {
            hide = '1';
        }
        else {
            hide = '0';
        }

        Ext.Ajax.request({
            url: 'asset/AssetDetail/showhide/id/' + record.json.asset_trans_id,
            method: 'POST',
            params: {
                id: this.id,
                mode: this.modez,
                hide: hide
            },
            timeOut: 1000,
            scope: this,
            success:function (f, a) {
                jun.rztAssetDetail.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

        },

	    ShowFormAsset: function(){
//            var selectedz = this.sm.getSelected();
//            if(selectedz == undefined){
//                Ext.MessageBox.alert("Warning","Anda belum memilih Data");
//                return;
//            }
            //var idz = selectedz.json.asset_trans_id;
            var form = new jun.ReportAssetTotal({modez:1});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);

        },
        loadEditForm: function(){
            var selectedz = this.sm.getSelected();
            if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Data");
                 return;
            }
            var price = 0;
            var accumulation = 0;

            var tdate = new Date(selectedz.json.asset_trans_date);
            var tdatem = tdate.getMonth()+1;
            var tdatey = tdate.getFullYear();

            var now = new Date();
            var nowm = now.getMonth()+1;
            var nowy = now.getFullYear();

/*            if(nowm == tdatem && nowy == tdatey)
            {
                price = selectedz.json.asset_trans_price;
            }
            else{*/
                var response_balance; // define here or you won't be able to access it outside of success: f()
                Ext.Ajax.request({
                    url: 'asset/AssetDetail/GetBalance/id/' + selectedz.json.asset_trans_id,
                    method: 'POST',
                    scope: this,
                    success:function (f, a) {
                        //jun.rztAssetDetail.reload();
                        response_balance = Ext.decode(f.responseText);
                        price = response_balance.success[0];
                        accumulation = response_balance.success[1];
                        Ext.getCmp('nilaibukudep').setValue(price);
                        Ext.getCmp('accumulationdep').setValue(accumulation);
                    },
                    failure:function (f, a) {
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            //}

            var idz = selectedz.json.asset_trans_id;
            var form = new jun.AssetDetailWin({modez:1, id:idz, balance:response_balance, accumulation: accumulation });
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
            form.nilaibukudep.setValue(price);
            form.accumulationdep.setValue(accumulation);
            jun.rztAssetPeriode.baseParams = {
                asset_trans_id: idz
            };
            jun.rztAssetPeriode.load();
            jun.rztAssetPeriode.baseParams = {};

        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'asset/AssetDetail/delete/id/' + record.json.asset_trans_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztAssetDetail.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });
        
        }
});

jun.ShowAssetDetailGrid=Ext.extend(Ext.grid.GridPanel ,{
    title:"Show Assets",
    id:'docs-jun.AssetDetailGrid',
    iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
    plugins:[new Ext.ux.grid.GridHeaderFilters],
    columns:[

        /*{
            header:'Doc Ref',
            sortable:true,
            resizable:true,
            dataIndex:'docref_other',
            width:100,
            filter: {xtype: "textfield"}
        },*/
        {
            header:'Activa',
            sortable:true,
            resizable:true,
            dataIndex:'ati',
            width:100,
            filter: {xtype: "textfield"}
        },

        {
            header:'Asset Name',
            sortable:true,
            resizable:true,
            dataIndex:'asset_trans_name',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Asset Code',
            sortable:true,
            resizable:true,
            dataIndex:'kode_barang',
            width:50,
            filter: {xtype: "textfield"}
        },
        {
            header:'Category',
            sortable:true,
            resizable:true,
            dataIndex:'category',
            width:100,
            filter: {xtype: "textfield"}
        },
/*        {
            header:'Class',
            sortable:true,
            resizable:true,
            dataIndex:'class',
            width:100,
            filter: {xtype: "textfield"}
        },*/
        {
            header:'Branch',
            sortable:true,
            resizable:true,
            dataIndex:'asset_trans_branch',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Description',
            sortable:true,
            resizable:true,
            dataIndex:'description',
            width:100,
            filter: {xtype: "textfield"}
        },
        {
            header:'Date',
            sortable:true,
            resizable:true,
            dataIndex:'asset_trans_date',
            width:100,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            filter: {
                xtype: "xdatefield",
                format: 'd/m/Y'
            }
        },
    ],
    initComponent: function(){
        this.store = jun.rztAssetDetailHide;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };

        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'label',
                    ref: '../lbldate',
                    text: 'Date',
                },
                ' ',
                {
                    xtype: "xdatefield",
                    format: 'd/m/Y',
                    ref: '../date',
                    value: DATE_NOW,
                },
                ' ',
                {
                    xtype: 'button',
                    text: 'Print',
                    ref: '../btnPrint',
                    iconCls: 'silk13-printer'
                },
                /*{
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Export Excel',
                    ref: '../btnExport',
                    iconCls: 'silk13-page_white_excel'
                },*/
                {
                    xtype:'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Export',
                    ref: '../btnExportNew',
                    iconCls: "silk13-report"
                },
                {
                    xtype: "form",
                    frame: !1,
                    border: !1,
                    ref: '../formz',
                    items: [
                        {
                            xtype: "hidden",
                            name: "asset_id",
                            ref: "../../asset_id"
                        },
                        {
                            xtype: "hidden",
                            name: "format",
                            value: "excel",
                            ref: "../../format"
                        }
                    ]
                }

            ]
        };
        this.store.baseParams = {mode: "grid"};
         this.store.baseParams = {hide_value: 0};
         this.store.reload();
        jun.AssetDetailGrid.superclass.initComponent.call(this);
        //this.btnAdd.on('Click', this.loadForm, this);
        this.btnPrint.on('Click', this.Print, this);
        //this.btnExport.on('Click', this.Export, this);
        this.btnExportNew.on('Click', this.ExportNew, this);
        //this.btnShowExcel.on('Click', this.ShowExcel, this);
        //this.btnDelete.on('Click', this.deleteRec, this);
        //this.getSelectionModel().on('rowselect', this.getrow, this);
        this.getSelectionModel().on('doubleclick', this.rowdblclick, this);
    },

    rowdblclick: function(){
        isDblClick = true;
        log.innerHTML += '<div>row double click</div>';
        Ext.MessageBox.alert("Warning","dbl");
        window.setTimeout(function(){
            isDblClick = false;
        }, 0);
    },
    btnVisibled: function (v) {
        this.lbldate.setVisible(v);
        this.date.setVisible(v);
    },
    btnDisabled: function (status) {
        /*this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);*/
    },
    getrow: function(sm, idx, r){
        this.record = r;
        var selectedz = this.sm.getSelections();

        /*this.btnVisibled(true);
        this.lbldate.setVisible(false);
        this.date.setVisible(false);*/
        //Ext.MessageBox.alert("Warning","Anda belum memilih Data");
        /*var singleClickTask = new Ext.util.DelayedTask(singleClickAction),  // our delayed task for single click
            singleClickDelay = 100; // delay in milliseconds

        function onClick() {
            singleClickTask.delay(singleClickDelay);
        }

        function onDblClick() {
            // double click detected - trigger double click action
            doubleClickAction();
            // and don't forget to cancel single click task!
            singleClickTask.cancel();
        }

        function singleClickAction() {
            Ext.MessageBox.alert("Warning","Anda belum memilih Data");
        }

        function doubleClickAction() {
            // something useful...
        }


// setting event handlers on an Element
        AssetDetailGrid.on('click', onClick);
        AssetDetailGrid.on('dblclick', onDblClick);*/


    },

    getrowdbl: function(sm, idx, r){
        this.record = r;
        var selectedz = this.sm.getSelections();

        Ext.MessageBox.alert("Warning","Anda belum memilih Data");
    },

    loadForm: function(){
        var form = new jun.AssetDetailWin({modez:0});
        form.show();
    },

    ExportNew: function(){

        /*var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data "+this.title+".");
            return;
        }
        var idz = selectedz.json.asset_id;
        //Ext.MessageBox.alert(idz);


        this.asset_id.setValue(idz);
        var form = this.formz.getForm();
        form.standardSubmit = !0;
        form.url = "Report/PrintAsset";
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();*/

        var form = new jun.ReportShowAsset({modez:1});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },

    Export: function(){

        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Data "+this.title+".");
            return;
        }
        var idz = selectedz.json.asset_id;
        //Ext.MessageBox.alert(idz);


        this.asset_id.setValue(idz);
        var form = this.formz.getForm();
        form.standardSubmit = !0;
        form.url = "Report/PrintAsset";
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },

    Print: function(){
        Ext.Ajax.request({
            url: 'asset/Asset/PrintAsset/',
            method: 'POST',
            //timeOut: 1000,
            scope: this,
            params: {
                id: this.id,
                date: this.date.getValue(),
            },
            success: function (f, a) {

                var response = Ext.decode(f.responseText);
                var printWindow = window.open('', '', 'height=0px ,width=0px');

                printWindow.document.write('<html>');
                printWindow.document.write('<head>');
                printWindow.document.write('<style>table {font-family: arial, sans-serif; border: 3px;border-collapse: collapse;width: 100%;} td, th {border: 1px solid #dddddd;text-align: center;padding: 8px;}tr:nth-child(even) {background-color: #dddddd;} </style>');
                printWindow.document.write('</head>');

                printWindow.document.write('<body>');
                printWindow.document.write('<div>');
                printWindow.document.write('<table>');
                printWindow.document.write('<tr>');
                    printWindow.document.write('<th>Name</th>');
                    printWindow.document.write('<th>No.Activa</th>');
                printWindow.document.write('</tr>');

                    for (var i = 0, len = response.data.length; i < len; i++)
                    {
                        printWindow.document.write('<tr>');
                            printWindow.document.write('<td>'+response.data[i].asset_trans_name+'</td>');
                            printWindow.document.write('<td>'+response.data[i].ati+'</td>');
                        printWindow.document.write('</tr>');
                    }

                printWindow.document.write('</table>');
                printWindow.document.write('</div>');

                printWindow.document.write('</body>');
                printWindow.document.write('</html>');

                setTimeout(function () {
                    printWindow.print();
                    printWindow.close();
                }, 500);

            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },

    deleteRec : function(){
        Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },

    deleteRecYes : function(btn){

        if (btn == 'no') {
            return;
        }

        var record = this.sm.getSelected();

        // Check is list selected
        if(record == undefined){
            Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
            return;
        }

        Ext.Ajax.request({
            url: 'asset/AssetDetail/delete/id/' + record.json.asset_trans_id,
            method: 'POST',
            success:function (f, a) {
                jun.rztAssetDetail.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

    }
})
