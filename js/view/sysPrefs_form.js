jun.SysPrefsWin = Ext.extend(Ext.Window, {
    title: 'SysPrefs',
    modez: 1,
    width: 700,
    height: 480 + 90,
    layout: 'fit',
    modal: true,
    padding: 5,
    closeForm: false,
    iswin: !0,
    initComponent: function () {
        if (jun.rztApotekerCmp.getTotalCount() === 0) {
            jun.rztApotekerCmp.load();
        }
        if (jun.rztFoCmp.getTotalCount() === 0) {
            jun.rztFoCmp.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4;',
                id: 'form-SysPrefs',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'fit',
                // height: '100%',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'tabpanel',
                        tabBarPosition: 'top',
                        activeTab: 0,
                        frame: true,
                        resizeTabs: true,
                        enableTabScroll: true,
                        tabWidth: 200,
                        items: [
                            {
                                frame: false,
                                title: "General",
                                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                                labelWidth: 100,
                                labelAlign: 'left',
                                layout: 'form',
                                border: false,
                                items: [
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Receipt Header 1',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'receipt_header1',
                                        maxLength: 48,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Receipt Header 2',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'receipt_header2',
                                        maxLength: 48,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Receipt Header 3',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'receipt_header3',
                                        maxLength: 48,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Receipt Header 4',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'receipt_header4',
                                        maxLength: 48,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Receipt Header 5',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'receipt_header5',
                                        maxLength: 48,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Receipt Header 6',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'receipt_header6',
                                        maxLength: 48,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Receipt Footer 1',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'receipt_footer1',
                                        maxLength: 48,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Receipt Footer 2',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'receipt_footer2',
                                        maxLength: 48,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Receipt Footer 3',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'receipt_footer3',
                                        maxLength: 48,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Receipt Footer 4',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'receipt_footer4',
                                        maxLength: 48,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Receipt Footer 5',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'receipt_footer5',
                                        maxLength: 48,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'textfield',
                                        fieldLabel: 'Receipt Footer 6',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'receipt_footer6',
                                        maxLength: 48,
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        typeAhead: true,
                                        fieldLabel: 'Apoteker',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: jun.rztApotekerCmp,
                                        hiddenName: 'apoteker',
                                        name: 'apoteker',
                                        valueField: 'apoteker_id',
                                        displayField: 'nama_apoteker',
                                        emptyText: "Select apoteker",
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        typeAhead: true,
                                        fieldLabel: 'Stocker',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        /*
                                         * Aishaderm
                                         * FO digilir menjadi stocker
                                         */
                                        store: jun.rztEmployeeFoCmp,
                                        hiddenName: 'stocker',
                                        name: 'stocker',
                                        valueField: 'fo_id',
                                        displayField: 'nama_fo',
                                        emptyText: "Select stocker",
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        typeAhead: true,
                                        fieldLabel: 'Admin',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: jun.rztEmployeeAdminCmp,
                                        hiddenName: 'admin_cabang',
                                        name: 'admin_cabang',
                                        valueField: 'admin_id',
                                        displayField: 'nama_admin',
                                        emptyText: "Select admin",
                                        anchor: '100%'
                                    },
                                    {
                                        xtype: 'combo',
                                        typeAhead: true,
                                        fieldLabel: 'Pengelola Cabang',
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        store: jun.rztEmployeePcCmp,
                                        hiddenName: 'pengelola_cabang',
                                        name: 'pengelola_cabang',
                                        valueField: 'pc_id',
                                        displayField: 'nama_pc',
                                        emptyText: "Select pengelola cabang",
                                        anchor: '100%'
                                    }
                                ]
                            },
                            {
                                frame: false,
                                title: "FinAcc",
                                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                                labelWidth: 100,
                                labelAlign: 'left',
                                layout: 'form',
                                border: false,
                                items: [
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'KMR (%)',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'kmr_persen',
                                        maxLength: 5,
                                        anchor: '50%'
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'KMR (Rp)',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'kmt_nominal',
                                        maxLength: 30,
                                        anchor: '50%'
                                    },
                                    {
                                        xtype: 'combo',
                                        //typeAhead: true,
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        forceSelection: true,
                                        fieldLabel: 'Bank Setor',
                                        store: jun.rztBankCmpPusat,
                                        hiddenName: 'bank_setor',
                                        valueField: 'bank_id',
                                        displayField: 'nama_bank',
                                        anchor: '50%'
                                    },
                                    {
                                        xtype: 'combo',
                                        //typeAhead: true,
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        forceSelection: true,
                                        fieldLabel: 'Kas Cabang',
                                        store: jun.rztBankCmpPusat,
                                        hiddenName: 'kas_cabang',
                                        valueField: 'bank_id',
                                        displayField: 'nama_bank',
                                        anchor: '50%'
                                    },
                                    {
                                        xtype: 'combo',
                                        //typeAhead: true,
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        forceSelection: true,
                                        fieldLabel: 'Kas Transit',
                                        store: jun.rztBankCmpPusat,
                                        hiddenName: 'kas_cabang_sementara',
                                        valueField: 'bank_id',
                                        displayField: 'nama_bank',
                                        anchor: '50%'
                                    },
                                    {
                                        xtype: 'combo',
                                        //typeAhead: true,
                                        triggerAction: 'all',
                                        lazyRender: true,
                                        mode: 'local',
                                        forceSelection: true,
                                        fieldLabel: 'Kas Kecil',
                                        store: jun.rztBankCmpPusat,
                                        hiddenName: 'petty_cash',
                                        valueField: 'bank_id',
                                        displayField: 'nama_bank',
                                        anchor: '50%'
                                    },
                                    new jun.SysPrefsGrid({
                                        height: 200,
                                        frameHeader: !1,
                                        header: !1
                                    })
                                ]
                            },
                            {
                                frame: false,
                                title: "Restriction On Sales",
                                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                                labelWidth: 100,
                                labelAlign: 'left',
                                layout: 'form',
                                border: false,
                                items: [
                                    {
                                        fieldLabel: 'Date',
                                        xtype: 'xdatefield',
                                        format: 'd M Y',
                                        name: "res_date",
                                        id: "res_date",
                                        //ref: '../res_date',
                                        //value: DATE_NOW,
                                        anchor: "50%"
                                    },
                                ]
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SysPrefsWin.superclass.initComponent.call(this);
        this.on('afterrender', this.onafterrender, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        // this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
    },
    onafterrender: function () {
        // if (this.modez == 1 || this.modez == 2) {
        //     this.btnSave.setVisible(true);
        // } else {
        //     this.btnSave.setVisible(true);
        // }
        Ext.Ajax.request({
            url: 'SysPrefs',
            method: 'POST',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                var rec = new Ext.data.Record(response.data);
                this.formz.getForm().loadRecord(rec);
                var d = Ext.decode(response.data.filter_payment);
                jun.rztBankFilterPayment.loadData(d);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnDisabled: function (status) {
        // this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'SysPrefs/create/';
        Ext.getCmp('form-SysPrefs').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                filter_payment: Ext.encode(Ext.pluck(
                        jun.rztBankFilterPayment.data.items, "data"))
            },
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});