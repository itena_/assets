jun.PembantuPelunasanPiutangDetilstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PembantuPelunasanPiutangDetilstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PembantuPelunasanPiutangDetilStoreId',
            url: 'PembantuPelunasanPiutangDetil',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'pembantu_pelunasan_piutang_detil_id'},
                {name: 'kas_dibayar'},
                {name: 'no_faktur'},
                {name: 'account_code'},
                {name: 'sisa'},
                {name: 'pembantu_pelunasan_piutang_id'},
                {name: 'payment_journal_id'},
                {name: 'type_'},
                {name: 'customer_id'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        var subtotal = this.sum("kas_dibayar");
        Ext.getCmp("totalPelunasanPiutangid").setValue(subtotal);
    }
});
jun.rztPembantuPelunasanPiutangDetil = new jun.PembantuPelunasanPiutangDetilstore();
//jun.rztPembantuPelunasanPiutangDetil.load();



jun.PembantuFakturPiutangstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PembantuFakturPiutangstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PembantuFakturPiutangStoreId',
            url: 'PembantuPelunasanPiutangDetil/faktur',
            autoLoad: !1,
            autoSave: !1,
            root: 'results',
            idProperty: 'row',
            totalProperty: 'total',
            fields: [
                {name: 'payment_journal_id'},
                {name: 'customer_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'no_faktur'},
                {name: 'nilai', type: 'float'},
                {name: 'sisa', type: 'float'}
            ]
        }, cfg));
    }
});
jun.rztPembantuFakturPiutang = new jun.PembantuFakturPiutangstore();