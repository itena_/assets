jun.ClearUlptGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Pemutihan ULPT",
    id: 'docs-jun.ClearUlptGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'clear_ulpt_id',
            sortable: true,
            resizable: true,
            dataIndex: 'clear_ulpt_id',
            width: 100
        },
        {
            header: 'tgl_dari',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_dari',
            width: 100
        },
        {
            header: 'tgl_to',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl_to',
            width: 100
        },
        {
            header: 'note_',
            sortable: true,
            resizable: true,
            dataIndex: 'note_',
            width: 100
        },
        {
            header: 'doc_ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'tdate',
            sortable: true,
            resizable: true,
            dataIndex: 'tdate',
            width: 100
        },
        /*
         {
         header:'store',
         sortable:true,
         resizable:true,
         dataIndex:'store',
         width:100
         },
         {
         header:'up',
         sortable:true,
         resizable:true,
         dataIndex:'up',
         width:100
         },
         {
         header:'user_id',
         sortable:true,
         resizable:true,
         dataIndex:'user_id',
         width:100
         },
         {
         header:'total',
         sortable:true,
         resizable:true,
         dataIndex:'total',
         width:100
         },
         */
    ],
    initComponent: function () {
        this.store = jun.rztClearUlpt;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Pemutihan ULPT',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Edit Pemituhan ULPT',
                    ref: '../btnEdit'
                }//,
                // {
                //     xtype: 'tbseparator'
                // },
                // {
                //     xtype: 'button',
                //     text: 'Hapus',
                //     ref: '../btnDelete'
                // }
            ]
        };
        this.store.baseParams = {mode: "grid"};
        this.store.reload();
        this.store.baseParams = {};
        jun.ClearUlptGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        // this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.ClearUlptWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.clear_ulpt_id;
        var form = new jun.ClearUlptWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'ClearUlpt/delete/id/' + record.json.clear_ulpt_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztClearUlpt.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
