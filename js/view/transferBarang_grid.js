jun.TransferBarangGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Transfer Barang Masuk",
    id: 'docs-jun.TransferBarangGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100,
            renderer: jun.renderStore
        }

    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangNonJasaTransfer.getTotalCount() === 0) {
            jun.rztBarangNonJasaTransfer.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        jun.rztTransferBarang.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    //b.params.tgl = Ext.getCmp('tgltransferitemgrid').getValue();
                    var tgl = Ext.getCmp('tgltransferbaranggrid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztTransferBarang;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Barang Masuk',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Tampil Barang Masuk',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tgltransferbaranggrid'
                }
            ]
        };
        jun.TransferBarangGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.TransferBarangWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.transfer_barang_id;
        var form = new jun.TransferBarangWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztTransferBarangDetails.proxy.setUrl('TransferBarangDetails/IndexIn');
        jun.rztTransferBarangDetails.baseParams = {
            transfer_barang_id: idz
        };
        jun.rztTransferBarangDetails.load();
        jun.rztTransferBarangDetails.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'TransferBarang/delete/id/' + record.json.transfer_barang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztTransferBarang.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
//jun.ReturnTransferBarangGrid = Ext.extend(Ext.grid.GridPanel, {
//    title: "Return Supplier Item",
//    id: 'docs-jun.ReturnTransferBarangGrid',
//    iconCls: "silk-grid",
//    stripeRows: true,
//    viewConfig: {
//        forceFit: true
//    },
//    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
//    columns: [
//        {
//            header: 'Date',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'tgl',
//            width: 100
//        },
//        {
//            header: 'Doc. Ref',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'doc_ref',
//            width: 100
//        },
//        {
//            header: 'Note',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'note',
//            width: 100
//        },
//        {
//            header: 'Branch',
//            sortable: true,
//            resizable: true,
//            dataIndex: 'store',
//            width: 100
//        }
//    ],
//    initComponent: function () {
//        if (jun.rztBarangLib.getTotalCount() === 0) {
//            jun.rztBarangLib.load();
//        }
//        if (jun.rztBarangNonJasa.getTotalCount() === 0) {
//            jun.rztBarangNonJasa.load();
//        }
//        if (jun.rztSupplierCmp.getTotalCount() === 0) {
//            jun.rztSupplierCmp.load();
//        }
//        if (jun.rztGrup.getTotalCount() === 0) {
//            jun.rztGrup.load();
//        }
//        jun.rztReturnTransferBarang.on({
//            scope: this,
//            beforeload: {
//                fn: function (a, b) {
//                    //b.params.tgl = Ext.getCmp('tglreturntransferitemgrid').getValue();
//                    var tgl = Ext.getCmp('tglreturntransferbaranggrid');
//                    b.params.tgl = tgl.hiddenField.dom.value;
//                    b.params.mode = "grid";
//                }
//            }
//        });
//        this.store = jun.rztReturnTransferBarang;
//        this.tbar = {
//            xtype: 'toolbar',
//            items: [
//                {
//                    xtype: 'button',
//                    text: 'Add Return Item',
//                    ref: '../btnAdd'
//                },
//                {
//                    xtype: 'tbseparator'
//                },
//                {
//                    xtype: 'button',
//                    text: 'View Return Item',
//                    ref: '../btnEdit'
//                },
//                {
//                    xtype: 'tbseparator'
//                },
//                {
//                    xtype: 'label',
//                    style: 'margin:5px',
//                    text: '\xA0Date :'
//                },
//                {
//                    xtype: 'xdatefield',
//                    ref: '../tgl',
//                    id: 'tglreturntransfergrid'
//                }
//            ]
//        };
//        jun.ReturnTransferBarangGrid.superclass.initComponent.call(this);
//        this.btnAdd.on('Click', this.loadForm, this);
//        this.btnEdit.on('Click', this.loadEditForm, this);
//        this.tgl.on('select', this.refreshTgl, this);
//        this.getSelectionModel().on('rowselect', this.getrow, this);
//        this.store.removeAll();
//    },
//    refreshTgl: function () {
//        this.store.reload();
//    },
//    getrow: function (sm, idx, r) {
//        this.record = r;
//        var selectedz = this.sm.getSelections();
//    },
//    loadForm: function () {
//        var form = new jun.ReturnTransferBarangWin({modez: 0});
//        form.show();
//    },
//    loadEditForm: function () {
//        var selectedz = this.sm.getSelected();
//        //var dodol = this.store.getAt(0);
//        if (selectedz == undefined) {
//            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
//            return;
//        }
//        var idz = selectedz.json.transfer_barang_id;
//        var form = new jun.ReturnTransferBarangWin({modez: 1, id: idz});
//        form.show(this);
//        form.formz.getForm().loadRecord(this.record);
//        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
//        jun.rztTransferBarangDetails.proxy.setUrl('TransferBarangDetails/IndexOut');
//        jun.rztTransferBarangDetails.baseParams = {
//            transfer_barang_id: idz
//        };
//        jun.rztTransferBarangDetails.load();
//        jun.rztTransferBarangDetails.baseParams = {};
//    },
//    deleteRec: function () {
//        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
//    },
//    deleteRecYes: function (btn) {
//        if (btn == 'no') {
//            return;
//        }
//        var record = this.sm.getSelected();
//        // Check is list selected
//        if (record == undefined) {
//            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
//            return;
//        }
//        Ext.Ajax.request({
//            url: 'ReturnTransferBarang/delete/id/' + record.json.transfer_barang_id,
//            method: 'POST',
//            success: function (f, a) {
//                jun.rztReturnTransferBarang.reload();
//                var response = Ext.decode(f.responseText);
//                Ext.MessageBox.show({
//                    title: 'Info',
//                    msg: response.msg,
//                    buttons: Ext.MessageBox.OK,
//                    icon: Ext.MessageBox.INFO
//                });
//            },
//            failure: function (f, a) {
//                switch (a.failureType) {
//                    case Ext.form.Action.CLIENT_INVALID:
//                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
//                        break;
//                    case Ext.form.Action.CONNECT_FAILURE:
//                        Ext.Msg.alert('Failure', 'Ajax communication failed');
//                        break;
//                    case Ext.form.Action.SERVER_INVALID:
//                        Ext.Msg.alert('Failure', a.result.msg);
//                }
//            }
//        });
//    }
//})

jun.TransferBarangOutGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Transfer Barang Keluar",
    id: 'docs-jun.TransferBarangOutGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }

    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangNonJasaTransfer.getTotalCount() === 0) {
            jun.rztBarangNonJasaTransfer.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        jun.rztTransferBarangOut.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tgl = Ext.getCmp('tglTransferBarangOutGrid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztTransferBarangOut;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Barang Keluar',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Tampil Barang Keluar',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglTransferBarangOutGrid'
                }
            ]
        };
        jun.TransferBarangOutGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.TransferBarangOutWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.transfer_barang_id;
        var form = new jun.TransferBarangOutWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztTransferBarangDetails.proxy.setUrl('TransferBarangDetails/IndexOut');            // <<<<<<indexout??
        jun.rztTransferBarangDetails.baseParams = {
            transfer_barang_id: idz
        };
        jun.rztTransferBarangDetails.load();
        jun.rztTransferBarangDetails.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'TransferBarangOut/delete/id/' + record.json.transfer_barang_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztTransferBarangOut.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
