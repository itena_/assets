jun.SalestransDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    id: 'docs-jun.SalestransDetailsGrid',
    //iconCls: "silk-grid",
    //cls: 'custom-sales-details',
    stripeRows: true,
//    viewConfig: {
//        forceFit: true
//    },
    enableKeyEvents: true,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'paket_trans_id',
            sortable: false,
            resizable: false,
            dataIndex: 'paket_trans_id',
            width: 40,
            hidden: true
        },
        {
            header: 'Pkg',
            sortable: false,
            resizable: false,
            dataIndex: 'paket_trans_id',
            width: 30,
            align: "left",
            renderer: jun.renderPackageIco
        },
        // {
        //     header: 'Items Code',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'barang_id',
        //     width: 75,
        //     renderer: jun.renderKodeBarang
        // },
        {
            header: 'Items Code',
            sortable: true,
            resizable: true,
            dataIndex: 'kode',
            width: 75
        },
        {
            header: 'Items Name',
            sortable: true,
            resizable: true,
            dataIndex: 'nama',
            width: 200
        },
        // {
        //     header: 'Items Name',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'barang_id',
        //     width: 200,
        //     renderer: jun.renderBarang
        // },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ketpot',
            width: 100
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 25,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Price',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 75,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc Code',
            sortable: true,
            resizable: true,
            dataIndex: 'disc_name',
            width: 75
        },
        {
            header: 'Disc',
            sortable: true,
            resizable: true,
            dataIndex: 'disc',
            width: 45,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'discrp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }//,
        // {
        //     header: 'Beauty 1',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'beauty_id',
        //     width: 150,
        //     renderer: jun.renderBeauty
        // },
        // {
        //     header: 'Beauty 2',
        //     sortable: true,
        //     resizable: true,
        //     dataIndex: 'beauty2_id',
        //     width: 150,
        //     renderer: jun.renderBeauty
        // }
    ],
    initComponent: function () {
        this.store = jun.rztSalestransDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            ////typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            //forceSelection: true,
                            store: jun.rztBarangCmp,
                            hiddenName: 'barang_id',
                            valueField: 'barang_id',
                            ref: '../../barang',
                            id: 'barangdetailsid',
                            displayField: 'kode_barang'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            enableKeyEvents: true,
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Price :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'priceid',
                            ref: '../../price',
                            style: 'margin-bottom:2px',
                            width: 55,
                            readOnly: true,
                            enableKeyEvents: true,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc :',
                            hidden: false
                        },
                        {
                            xtype: 'numericfield',
                            id: 'discdetilid',
                            style: 'margin-bottom:2px',
                            ref: '../../disc',
                            width: 50,
                            readOnly: true,
                            enableKeyEvents: true,
                            maxLength: 5,
                            value: 0,
                            minValue: 0,
                            maxValue: 100,
                            hidden: false
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc Amount :',
                            hidden: false
                        },
                        {
                            xtype: 'numericfield',
                            id: 'discadetilid',
                            style: 'margin-bottom:2px',
                            ref: '../../disca',
                            width: 55,
                            readOnly: true,
                            enableKeyEvents: true,
                            value: 0,
                            minValue: 0,
                            hidden: false
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            id: 'ketpotid',
                            ref: '../../ketpot',
                            width: 373,
                            colspan: 7,
                            maxLength: 255
                        },
                        {
                            xtype: 'hidden',
                            ref: '../../disc_name'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 2,
                    hidden: true,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Beauty 1:'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            ref: '../../beauty',
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            store: jun.rztBeautyCmp,
                            hiddenName: 'beauty_id',
                            valueField: 'beauty_id',
                            displayField: 'nama_beauty',
                            allowBlank: true,
                            width: 100,
                            style: 'margin-bottom:2px'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Beauty 2:'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            ref: '../../beauty2',
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            store: jun.rztBeautyCmp,
                            hiddenName: 'beauty2_id',
                            valueField: 'beauty_id',
                            displayField: 'nama_beauty',
                            allowBlank: true,
                            width: 100
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add (F1)',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit (F2)',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del (Delete)',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.SalestransDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.onAddSaveClick, this);
        this.btnEdit.on('Click', this.onAddSaveClick, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.qty.on('keyup', this.onDiscChange, this);
        this.price.on('keyup', this.onDiscChange, this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.disca.on('keyup', this.onDiscChange, this);
        this.barang.on('select', this.onChangeBarang, this);
        this.on("activate", this.onActive, this);
        this.on("keypress", this.onKeyPress, this);
        this.refreshReadOnly();
    },
    refreshReadOnly: function () {
        if (SALES_OVERRIDE == "1") {
            this.disc.setReadOnly(false);
            this.disca.setReadOnly(false);
            this.price.setReadOnly(false);
        } else {
            this.disc.setReadOnly(true);
            this.disca.setReadOnly(true);
            this.price.setReadOnly(true);
            Ext.getCmp('priceid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discdetilid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discadetilid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
        }
    },
    onKeyPress: function (e) {
        switch (e.getKey()) {
            case 46:
                if (!this.btnDelete.disabled) {
                    this.deleteRec();
                }
                break;
        }
    },
    onActive: function (t) {
        t.getKeyMap().addBinding({
            key: [Ext.EventObject.F1, Ext.EventObject.F2, Ext.EventObject.DELETE, Ext.EventObject.F3],
            handler: function (key, e) {
                switch (key) {
                    case Ext.EventObject.F1:
                        if (!this.btnAdd.disabled) {
                            this.barang.focus();
                            this.onAddSaveClick(this.btnAdd, null);
                        }
                        break;
                    case Ext.EventObject.F2:
                        if (!this.btnEdit.disabled) {
                            this.onAddSaveClick(this.btnEdit, null);
                        }
                        break;
                    case Ext.EventObject.DELETE:
                        if (!this.btnDelete.disabled) {
                            this.deleteRec();
                        }
                        break;
                    case Ext.EventObject.F3:
                        this.barang.collapse();
                        this.focusGrid();
                        break;
                }
            },
            stopEvent: true,
            scope: this
        });
    },
    onDiscChange: function (a, e) {
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var bruto = round(price * qty, 2);
        if (a.id == "discdetilid" || a.id == "priceid" || a.id == "qtyid") {
            this.disca.setValue(round(bruto * (disc / 100), 2));
        } else {
            var key = e.getKey();
            if (key == Ext.EventObject.TAB) {
                return;
            }
            if (a.id == "discadetilid") {
                this.disc.setValue(0);
            }
        }
    },
    onStoreChange: function () {
        jun.rztSalestransDetails.refreshData();
    },
    onAddSaveClick: function (b, e) {
        if (b.getText() == 'Edit (F2)') {
            this.onClickbtnEdit();
        } else {
            var price = parseFloat(this.price.getValue());
            if (price == 0) {
                Ext.MessageBox.confirm('Questions', 'Anda yakin input dengan harga nol?', function (btn, text) {
                    if (btn == 'no') {
                        return;
                    }
                    if (b.getText() == 'Save (F2)') {
                        this.onClickbtnEdit();
                    } else {
                        this.loadForm();
                    }
                }, this);
            } else {
                if (b.getText() == 'Save (F2)') {
                    this.onClickbtnEdit();
                } else {
                    this.loadForm();
                }
            }
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "" || barang_id == undefined) {
            Ext.MessageBox.alert("Error", "You have not selected a item", function () {
                this.barang.focus();
            }, this);
            return;
        }
//      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztSalestransDetails.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Item already inputted");
//                return;
//            }
//        }
        var barang = jun.getBarang(barang_id);
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        if (qty < 1) {
            Ext.MessageBox.alert("Error", "Qty minimal 1");
            return;
        }
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var disc1 = parseFloat(Ext.getCmp('discid').getValue());
        var ketpot = this.ketpot.getValue();
        var beauty = this.beauty.getValue();
        var beauty2 = this.beauty2.getValue();
        var disc_name = this.disc_name.getValue();
        var bruto = round(price * qty, 2);
        var vat = jun.getTax(barang_id);
        var subtotal = bruto - disca;
        var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
        var totalpot = disca + discrp1;
        var total_with_disc = bruto - totalpot;
        var total = bruto - disca;
        var vatrp = round(total_with_disc * vat, 2);
        if (this.btnEdit.text == 'Save (F2)') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.set('price', price);
            record.set('disc', disc);
            record.set('discrp', disca);
            record.set('ketpot', ketpot);
            record.set('beauty_id', beauty);
            record.set('beauty2_id', beauty2);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc_name', disc_name);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        } else {
            var c = jun.rztSalestransDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty,
                    price: price,
                    disc: disc,
                    discrp: disca,
                    ketpot: ketpot,
                    beauty_id: beauty,
                    beauty2_id: beauty2,
                    vat: vat,
                    vatrp: vatrp,
                    total_pot: totalpot,
                    bruto: bruto,
                    disc_name: disc_name,
                    total: total,
                    disc1: disc1,
                    discrp1: discrp1,
                    paket_trans_id: '',
                    paket_details_id: ''
                });
            jun.rztSalestransDetails.add(d);
        }
//        clearText();
        this.barang.reset();
        this.qty.reset();
        this.price.reset();
        this.ketpot.reset();
        this.disc.reset();
        this.disca.reset();
        this.beauty.reset();
        this.beauty2.reset();
        menuDetil(barang.data.kode_barang, total, 'TOTAL', parseFloat(Ext.getCmp("totalid").getValue()));
        this.barang.focus();
        this.refreshReadOnly();
    },
    onChangeBarang: function () {
        var barang_id = this.barang.getValue();
        var customer_id = Ext.getCmp("customersales_id").getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Item must selected.");
            return;
        }
        if (customer_id == "" || customer_id == undefined) {
            Ext.MessageBox.alert("Error", "Customer must selected.");
            this.barang.reset();
            return;
        }
        var barang = jun.getBarang(barang_id);
        //this.price.setValue(barang.data.price);
        if (jun.isCatJasa(barang_id)) {
            this.beauty.setDisabled(false);
            this.beauty2.setDisabled(false);
        } else {
            this.beauty.reset();
            this.beauty.setDisabled(true);
            this.beauty2.reset();
            this.beauty2.setDisabled(true);
        }
        // this.price.setValue(barang.data.price);
        // this.disc.setValue(0);
        // this.disc_name.setValue(response.msg.nama_status);
        // this.onDiscChange(this.disc);
        Ext.Ajax.request({
            url: 'SalestransDetails/GetDiskon',
            method: 'POST',
            scope: this,
            params: {
                customer_id: customer_id,
                barang_id: barang_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.price.setValue(response.msg.price);
                this.disc.setValue(response.msg.value);
                this.disc_name.setValue(response.msg.kode);
                this.onDiscChange(this.disc);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    focusGrid: function () {
        var record = this.sm.getSelected();
        var idx = 0;
        if (record == undefined) {
            var total_store = this.store.data.length;
            idx = total_store - 1;
        } else {
            idx = this.store.indexOf(record);
        }
        this.getSelectionModel().selectRow(idx);
        this.getView().focusRow(idx);
    },
    onClickbtnEdit: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            Ext.getCmp('win-Salestrans').statusActive = 1;
            return;
        }
        if (record.data.paket_trans_id.trim().length > 0) {
            Ext.MessageBox.alert("Warning", "Can't edit this product/threatment because part of packages.");
            Ext.getCmp('win-Salestrans').statusActive = 1;
            return;
        }
        if (this.btnEdit.text == 'Edit (F2)') {
            this.barang.setValue(record.data.barang_id);
//            this.onChangeBarang();
            this.qty.setValue(record.data.qty);
            this.disc.setValue(record.data.disc);
            this.disca.setValue(record.data.discrp);
            this.ketpot.setValue(record.data.ketpot);
            this.beauty.setValue(record.data.beauty_id);
            this.beauty2.setValue(record.data.beauty2_id);
            this.price.setValue(record.data.price);
            this.disc_name.setValue(record.data.disc_name);
            this.btnEdit.setText("Save (F2)");
            this.btnDisable(true);
        } else {
            this.loadForm();
            this.btnEdit.setText("Edit (F2)");
            this.btnDisable(false);
        }
        this.barang.focus();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.salestrans_details;
        var form = new jun.SalestransDetailsWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (record.data.paket_trans_id != '') {
            Ext.MessageBox.alert("Warning", "Can't edit this product/threatment because part of packages.");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        this.store.remove(record);
        var barang = jun.getBarang(record.data.barang_id);
        menuDetil(barang.data.kode_barang, "-" + record.data.total, 'TOTAL',
            parseFloat(Ext.getCmp("totalid").getValue()));
        Ext.getCmp('win-Salestrans').statusActive = 1;
    }
});
jun.SalestransQtyDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    id: 'docs-jun.SalestransQtyDetailsGrid',
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Items Code',
            dataIndex: 'barang_id',
            width: 75,
            renderer: jun.renderKodeBarang
        },
        // {
        //     header: 'Items Name',
        //     dataIndex: 'barang_id',
        //     width: 200,
        //     renderer: jun.renderBarang
        // },
        {
            header: 'Note',
            dataIndex: 'ketpot',
            width: 250
        },
        {
            header: 'Qty',
            dataIndex: 'qty',
            width: 25,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Harga',
            dataIndex: 'price',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Stock',
            dataIndex: 'stock',
            width: 50,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        if (jun.rztBarangCmp.getTotalCount() === 0) {
            jun.rztBarangCmp.load();
        }
        this.store = jun.rztSalestransQtyDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
//                        {
//                            xtype: 'combo',
//                            style: 'margin-bottom:2px',
//                            ////typeAhead: true,
//                            width: 100,
//                            triggerAction: 'all',
//                            lazyRender: true,
//                            mode: 'local',
//                            //forceSelection: true,
//                            store: jun.rztBarangCmp,
//                            hiddenName: 'barang_id',
//                            valueField: 'barang_id',
//                            ref: '../../barang',
//                            
//                            displayField: 'kode_barang'
//                        },
                        {
                            xtype: 'mfcombobox',
                            //xtype: 'combo',
                            style: 'margin-bottom:2px',
                            searchFields: [
                                'kode_barang',
                                'nama_barang'
                            ],
                            typeAhead: false,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<span style="font-weight:bold;">{kode_barang}</span><br><span>{nama_barang}</span>',
                                    "</div></tpl>"),
                            forceSelection: true,
                            store: jun.rztBarangCmp,
                            //hiddenName: 'barang_id',
                            valueField: 'barang_id',
                            ref: '../../barang',
                            id: 'barangdetailsid',
                            displayField: 'kode_barang',
                            listWidth: 300,
                            width: 175
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            enableKeyEvents: true,
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Price :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'priceid',
                            ref: '../../price',
                            style: 'margin-bottom:2px',
                            width: 55,
                            readOnly: true,
                            enableKeyEvents: true,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc :',
                            hidden: true
                        },
                        {
                            xtype: 'numericfield',
                            id: 'discdetilid',
                            style: 'margin-bottom:2px',
                            ref: '../../disc',
                            width: 50,
                            readOnly: true,
                            enableKeyEvents: true,
                            maxLength: 5,
                            value: 0,
                            minValue: 0,
                            maxValue: 100,
                            hidden: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc Amount :',
                            hidden: true
                        },
                        {
                            xtype: 'numericfield',
                            id: 'discadetilid',
                            style: 'margin-bottom:2px',
                            ref: '../../disca',
                            width: 55,
                            readOnly: true,
                            enableKeyEvents: true,
                            value: 0,
                            minValue: 0,
                            hidden: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            id: 'ketpotid',
                            ref: '../../ketpot',
                            width: 278 + 75,
                            colspan: 7,
                            maxLength: 255
                        },
                        {
                            xtype: 'hidden',
                            ref: '../../disc_name'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            width: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            width: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            width: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.SalestransQtyDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.onAddSaveClick, this);
        this.btnEdit.on('Click', this.onAddSaveClick, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.qty.on('keyup', this.onDiscChange, this);
        // this.price.on('keyup', this.onDiscChange, this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.disca.on('keyup', this.onDiscChange, this);
        this.barang.on('select', this.onChangeBarang, this);
        // this.on("activate", this.onActive, this);
        // this.on("keypress", this.onKeyPress, this);
        this.refreshReadOnly();
    },
    refreshReadOnly: function () {
        if (SALES_OVERRIDE == "1") {
            this.disc.setReadOnly(false);
            this.disca.setReadOnly(false);
            // this.price.setReadOnly(false);
        } else {
            this.disc.setReadOnly(true);
            this.disca.setReadOnly(true);
            // this.price.setReadOnly(true);
            // Ext.getCmp('priceid').on('render', function (c) {
            //     c.getEl().on('dblclick', function () {
            //         if (this.readOnly) {
            //             var login = new jun.login();
            //             login.show();
            //         }
            //     }, this);
            // });
            // Ext.getCmp('discdetilid').on('render', function (c) {
            //     c.getEl().on('dblclick', function () {
            //         if (this.readOnly) {
            //             var login = new jun.login();
            //             login.show();
            //         }
            //     }, this);
            // });
            // Ext.getCmp('discadetilid').on('render', function (c) {
            //     c.getEl().on('dblclick', function () {
            //         if (this.readOnly) {
            //             var login = new jun.login();
            //             login.show();
            //         }
            //     }, this);
            // });
        }
    },
    onKeyPress: function (e) {
        switch (e.getKey()) {
            case 46:
                if (!this.btnDelete.disabled) {
                    this.deleteRec();
                }
                break;
        }
    },
    onActive: function (t) {
        t.getKeyMap().addBinding({
            key: [Ext.EventObject.F1, Ext.EventObject.F2, Ext.EventObject.DELETE, Ext.EventObject.F3],
            handler: function (key, e) {
                switch (key) {
                    case Ext.EventObject.F1:
                        if (!this.btnAdd.disabled) {
                            this.barang.focus();
                            this.onAddSaveClick(this.btnAdd, null);
                        }
                        break;
                    case Ext.EventObject.F2:
                        if (!this.btnEdit.disabled) {
                            this.onAddSaveClick(this.btnEdit, null);
                        }
                        break;
                    case Ext.EventObject.DELETE:
                        if (!this.btnDelete.disabled) {
                            this.deleteRec();
                        }
                        break;
                    case Ext.EventObject.F3:
                        this.barang.collapse();
                        this.focusGrid();
                        break;
                }
            },
            stopEvent: true,
            scope: this
        });
    },
    onDiscChange: function (a, e) {
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var bruto = round(price * qty, 2);
        if (a.id == "discdetilid" || a.id == "priceid" || a.id == "qtyid") {
            this.disca.setValue(round(bruto * (disc / 100), 2));
        } else {
            var key = e.getKey();
            if (key == Ext.EventObject.TAB) {
                return;
            }
            if (a.id == "discadetilid") {
                this.disc.setValue(0);
            }
        }
    },
    onStoreChange: function () {

    },
    onAddSaveClick: function (b, e) {
        if (b.getText() == 'Edit') {
            this.onClickbtnEdit();
        } else {
            if (b.getText() == 'Save') {
                this.onClickbtnEdit();
            } else {
//                this.loadForm();
                this.cekStock();
            }
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    cekStock: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "" || barang_id == undefined) {
            Ext.MessageBox.alert("Error", "You have not selected a item", function () {
                this.barang.focus();
            }, this);
            return;
        }
//      kunci barang sama tidak boleh masuk dua kali
//        console.log(this.btnEdit.text);
        if (this.btnEdit.text != 'Save') {
            var a = jun.rztSalestransQtyDetails.findExact("barang_id", barang_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Item already inputted");
                return;
            }
        }
        var barang = jun.getBarang(barang_id);
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        if (qty < 1) {
            Ext.MessageBox.alert("Error", "Qty minimal 1");
            return;
        }
//      cek stok
        Ext.Ajax.request({
            url: 'StockMoves/HitungStock',
            method: 'POST',
            scope: this,
            params: {
                barang_id: barang_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.msg != '') {
                    Ext.MessageBox.alert("Error", response.msg);
                    return;
                }
                if (response.success) {
                    this.loadForm(response.sm);
                }                
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },    
    cekStockEdit: function () {
        var barang_id = this.barang.getValue();
//      cek stok
        Ext.Ajax.request({
            url: 'StockMoves/HitungStock',
            method: 'POST',
            scope: this,
            params: {
                barang_id: barang_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.msg != '') {
                    Ext.MessageBox.alert("Error", response.msg);
                    return;
                }
//                console.log(response);
                var barang_id = this.barang.getValue();
                var price = parseFloat(this.price.getValue());
                var qty = parseFloat(this.qty.getValue());
                var ketpot = this.ketpot.getValue();
                var bruto = round(price * qty, 2);
                if (response.success) {
//                    this.loadForm(response.msg);
                    var record = this.sm.getSelected();
                    record.set('barang_id', barang_id);
                    record.set('qty', qty);
                    record.set('price', price);
                    record.set('ketpot', ketpot);
                    record.set('bruto', bruto);
                    record.set('stock', response.sm);
                    record.commit();
                }         
                this.barang.reset();
                this.qty.reset();
                this.price.reset();
                this.ketpot.reset();
                this.barang.focus();
                this.refreshReadOnly();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    loadForm: function (stock) {
        var barang_id = this.barang.getValue();
//        if (barang_id == "" || barang_id == undefined) {
//            Ext.MessageBox.alert("Error", "You have not selected a item", function () {
//                this.barang.focus();
//            }, this);
//            return;
//        }
////      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztSalestransQtyDetails.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Item already inputted");
//                return;
//            }
//        }
//        var barang = jun.getBarang(barang_id);
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
//        if (qty < 1) {
//            Ext.MessageBox.alert("Error", "Qty minimal 1");
//            return;
//        }
        // var disc = parseFloat(this.disc.getValue());
        // var disca = parseFloat(this.disca.getValue());
        // var disc1 = parseFloat(Ext.getCmp('discid').getValue());
        var ketpot = this.ketpot.getValue();
        // var beauty = this.beauty.getValue();
        // var beauty2 = this.beauty2.getValue();
        // var disc_name = this.disc_name.getValue();
        var bruto = round(price * qty, 2);
        // var vat = jun.getTax(barang_id);
        // var subtotal = bruto - disca;
        // var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
        // var totalpot = disca + discrp1;
        // var total_with_disc = bruto - totalpot;
        // var total = bruto - disca;
        // var vatrp = round(total_with_disc * vat, 2);
        if (this.btnEdit.text == 'Save') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.set('price', price);
            // record.set('disc', disc);
            // record.set('discrp', disca);
            record.set('ketpot', ketpot);
            // record.set('beauty_id', beauty);
            // record.set('beauty2_id', beauty2);
            // record.set('vat', vat);
            // record.set('vatrp', vatrp);
            // record.set('total_pot', totalpot);
            // record.set('total', total);
            record.set('bruto', bruto);
            record.set('stock', stock);
            // record.set('disc_name', disc_name);
            // record.set('disc1', disc1);
            // record.set('discrp1', discrp1);
            record.commit();
        } else {
            var c = jun.rztSalestransQtyDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty,
                    price: price,
                    // disc: disc,
                    // discrp: disca,
                    ketpot: ketpot,
                    // beauty_id: beauty,
                    // beauty2_id: beauty2,
                    // vat: vat,
                    // vatrp: vatrp,
                    // total_pot: totalpot,
                    bruto: bruto,
                    stock: stock
                    // disc_name: disc_name,
                    // total: total,
                    // disc1: disc1,
                    // discrp1: discrp1,
                    // paket_trans_id: '',
                    // paket_details_id: ''
                });
            jun.rztSalestransQtyDetails.add(d);
        }
//        clearText();
        this.barang.reset();
        this.qty.reset();
        this.price.reset();
        this.ketpot.reset();
        // this.disc.reset();
        // this.disca.reset();
        // this.beauty.reset();
        // this.beauty2.reset();
        // menuDetil(barang.data.kode_barang, total, 'TOTAL', parseFloat(Ext.getCmp("totalid").getValue()));
        this.barang.focus();
        this.refreshReadOnly();
    },
    onChangeBarang: function () {
        var barang_id = this.barang.getValue();
        // var customer_id = Ext.getCmp("customersales_id").getValue();
        // if (barang_id == "") {
        //     Ext.MessageBox.alert("Error", "Item must selected.");
        //     return;
        // }
        // if (customer_id == "" || customer_id == undefined) {
        //     Ext.MessageBox.alert("Error", "Customer must selected.");
        //     this.barang.reset();
        //     return;
        // }
        var barang = jun.getBarang(barang_id);
        this.price.setValue(barang.data.price);
        // if (jun.isCatJasa(barang_id)) {
        //     this.beauty.setDisabled(false);
        //     this.beauty2.setDisabled(false);
        // } else {
        //     this.beauty.reset();
        //     this.beauty.setDisabled(true);
        //     this.beauty2.reset();
        //     this.beauty2.setDisabled(true);
        // }
        // this.price.setValue(barang.data.price);
        // this.disc.setValue(0);
        //this.disc_name.setValue(response.msg.nama_status);
        //this.onDiscChange(this.disc);
        //Ext.Ajax.request({
        //    url: 'SalestransQtyDetails/GetDiskon',
        //    method: 'POST',
        //    scope: this,
        //    params: {
        //        customer_id: customer_id,
        //        barang_id: barang_id
        //    },
        //    success: function (f, a) {
        //        var response = Ext.decode(f.responseText);
        //        this.price.setValue(response.msg.price);
        //        this.disc.setValue(response.msg.value);
        //        this.disc_name.setValue(response.msg.nama_status);
        //        this.onDiscChange(this.disc);
        //    },
        //    failure: function (f, a) {
        //        switch (a.failureType) {
        //            case Ext.form.Action.CLIENT_INVALID:
        //                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
        //                break;
        //            case Ext.form.Action.CONNECT_FAILURE:
        //                Ext.Msg.alert('Failure', 'Ajax communication failed');
        //                break;
        //            case Ext.form.Action.SERVER_INVALID:
        //                Ext.Msg.alert('Failure', a.result.msg);
        //        }
        //    }
        //});
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    focusGrid: function () {
        var record = this.sm.getSelected();
        var idx = 0;
        if (record == undefined) {
            var total_store = this.store.data.length;
            idx = total_store - 1;
        } else {
            idx = this.store.indexOf(record);
        }
        this.getSelectionModel().selectRow(idx);
        this.getView().focusRow(idx);
    },
    onClickbtnEdit: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            Ext.getCmp('win-SalestransQty').statusActive = 1;
            return;
        }
        // if (record.data.paket_trans_id.trim().length > 0) {
        //     Ext.MessageBox.alert("Warning", "Can't edit this product/threatment because part of packages.");
        //     Ext.getCmp('win-SalestransQty').statusActive = 1;
        //     return;
        // }
        if (this.btnEdit.text == 'Edit') {
            this.barang.setValue(record.data.barang_id);
//            this.onChangeBarang();
            this.qty.setValue(record.data.qty);
            // this.disc.setValue(record.data.disc);
            // this.disca.setValue(record.data.discrp);
            this.ketpot.setValue(record.data.ketpot);
            // this.beauty.setValue(record.data.beauty_id);
            // this.beauty2.setValue(record.data.beauty2_id);
            this.price.setValue(record.data.price);
            // this.disc_name.setValue(record.data.disc_name);
            this.btnEdit.setText("Save");
            this.btnDisable(true);
        } else {
            this.cekStockEdit();
            this.btnEdit.setText("Edit");
            this.btnDisable(false);
        }
        this.barang.focus();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        this.store.remove(record);
        // var barang = jun.getBarang(record.data.barang_id);
        // menuDetil(barang.data.kode_barang, "-" + record.data.total, 'TOTAL',
        //     parseFloat(Ext.getCmp("totalid").getValue()));
        // Ext.getCmp('win-SalestransQty').statusActive = 1;
    }
});
jun.SalestransNoBeautyDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    id: 'docs-jun.SalestransNoBeautyDetailsGrid',
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    enableKeyEvents: true,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Items Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Items Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderBarang
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ketpot',
            width: 100
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 25,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        }
    ],
    initComponent: function () {
        this.store = jun.rztSalestransNoBeautyDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            ////typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            //forceSelection: true,
                            store: jun.rztBarangCmp,
                            hiddenName: 'barang_id',
                            valueField: 'barang_id',
                            ref: '../../barang',
                            id: 'barangdetailsid',
                            displayField: 'kode_barang'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            enableKeyEvents: true,
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Price :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'priceid',
                            ref: '../../price',
                            style: 'margin-bottom:2px',
                            width: 55,
                            readOnly: true,
                            enableKeyEvents: true,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc :',
                            hidden: true
                        },
                        {
                            xtype: 'numericfield',
                            id: 'discdetilid',
                            style: 'margin-bottom:2px',
                            ref: '../../disc',
                            width: 50,
                            readOnly: true,
                            enableKeyEvents: true,
                            maxLength: 5,
                            value: 0,
                            minValue: 0,
                            maxValue: 100,
                            hidden: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc Amount :',
                            hidden: true
                        },
                        {
                            xtype: 'numericfield',
                            id: 'discadetilid',
                            style: 'margin-bottom:2px',
                            ref: '../../disca',
                            width: 55,
                            readOnly: true,
                            enableKeyEvents: true,
                            value: 0,
                            minValue: 0,
                            hidden: true
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            id: 'ketpotid',
                            ref: '../../ketpot',
                            width: 278,
                            colspan: 7,
                            maxLength: 255
                        },
                        {
                            xtype: 'hidden',
                            ref: '../../disc_name'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add (F1)',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit (F2)',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del (Delete)',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.SalestransNoBeautyDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.onAddSaveClick, this);
        this.btnEdit.on('Click', this.onAddSaveClick, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.qty.on('keyup', this.onDiscChange, this);
        this.price.on('keyup', this.onDiscChange, this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.disca.on('keyup', this.onDiscChange, this);
        this.barang.on('select', this.onChangeBarang, this);
        this.on("activate", this.onActive, this);
        this.on("keypress", this.onKeyPress, this);
        this.refreshReadOnly();
    },
    refreshReadOnly: function () {
        if (SALES_OVERRIDE == "1") {
            this.disc.setReadOnly(false);
            this.disca.setReadOnly(false);
            this.price.setReadOnly(false);
        } else {
            this.disc.setReadOnly(true);
            this.disca.setReadOnly(true);
            this.price.setReadOnly(true);
            Ext.getCmp('priceid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discdetilid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discadetilid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
        }
    },
    onKeyPress: function (e) {
        switch (e.getKey()) {
            case 46:
                if (!this.btnDelete.disabled) {
                    this.deleteRec();
                }
                break;
        }
    },
    onActive: function (t) {
        t.getKeyMap().addBinding({
            key: [Ext.EventObject.F1, Ext.EventObject.F2, Ext.EventObject.DELETE, Ext.EventObject.F3],
            handler: function (key, e) {
                switch (key) {
                    case Ext.EventObject.F1:
                        if (!this.btnAdd.disabled) {
                            this.barang.focus();
                            this.onAddSaveClick(this.btnAdd, null);
                        }
                        break;
                    case Ext.EventObject.F2:
                        if (!this.btnEdit.disabled) {
                            this.onAddSaveClick(this.btnEdit, null);
                        }
                        break;
                    case Ext.EventObject.DELETE:
                        if (!this.btnDelete.disabled) {
                            this.deleteRec();
                        }
                        break;
                    case Ext.EventObject.F3:
                        this.barang.collapse();
                        this.focusGrid();
                        break;
                }
            },
            stopEvent: true,
            scope: this
        });
    },
    onDiscChange: function (a, e) {
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var bruto = round(price * qty, 2);
        if (a.id == "discdetilid" || a.id == "priceid" || a.id == "qtyid") {
            this.disca.setValue(round(bruto * (disc / 100), 2));
        } else {
            var key = e.getKey();
            if (key == Ext.EventObject.TAB) {
                return;
            }
            if (a.id == "discadetilid") {
                this.disc.setValue(0);
            }
        }
    },
    onStoreChange: function () {
        jun.rztSalestransNoBeautyDetails.refreshData();
    },
    onAddSaveClick: function (b, e) {
        if (b.getText() == 'Edit (F2)') {
            this.onClickbtnEdit();
        } else {
            var price = parseFloat(this.price.getValue());
            if (price == 0) {
                Ext.MessageBox.confirm('Questions', 'Anda yakin input dengan harga nol?', function (btn, text) {
                    if (btn == 'no') {
                        return;
                    }
                    if (b.getText() == 'Save (F2)') {
                        this.onClickbtnEdit();
                    } else {
                        this.loadForm();
                    }
                }, this);
            } else {
                if (b.getText() == 'Save (F2)') {
                    this.onClickbtnEdit();
                } else {
                    this.loadForm();
                }
            }
        }
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "" || barang_id == undefined) {
            Ext.MessageBox.alert("Error", "You have not selected a item", function () {
                this.barang.focus();
            }, this);
            return;
        }
//      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztSalestransNoBeautyDetails.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Item already inputted");
//                return;
//            }
//        }
        var barang = jun.getBarang(barang_id);
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        if (qty < 1) {
            Ext.MessageBox.alert("Error", "Qty minimal 1");
            return;
        }
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var disc1 = parseFloat(Ext.getCmp('discid').getValue());
        var ketpot = this.ketpot.getValue();
        var disc_name = this.disc_name.getValue();
        var bruto = round(price * qty, 2);
        var vat = jun.getTax(barang_id);
        var subtotal = bruto - disca;
        var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
        var totalpot = disca + discrp1;
        var total_with_disc = bruto - totalpot;
        var total = bruto - disca;
        var vatrp = round(total_with_disc * vat, 2);
        if (this.btnEdit.text == 'Save (F2)') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.set('price', price);
            record.set('disc', disc);
            record.set('discrp', disca);
            record.set('ketpot', ketpot);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc_name', disc_name);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        } else {
            var c = jun.rztSalestransNoBeautyDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty,
                    price: price,
                    disc: disc,
                    discrp: disca,
                    ketpot: ketpot,
                    vat: vat,
                    vatrp: vatrp,
                    total_pot: totalpot,
                    bruto: bruto,
                    disc_name: disc_name,
                    total: total,
                    disc1: disc1,
                    discrp1: discrp1
                });
            jun.rztSalestransNoBeautyDetails.add(d);
        }
        clearText();
        this.barang.reset();
        this.qty.reset();
        this.price.reset();
        this.ketpot.reset();
        this.disc.reset();
        this.disca.reset();
        menuDetil(barang.data.kode_barang, total, 'TOTAL', parseFloat(Ext.getCmp("totalid").getValue()));
        this.barang.focus();
        this.refreshReadOnly();
    },
    onChangeBarang: function () {
        var barang_id = this.barang.getValue();
        var customer_id = Ext.getCmp("customersales_id").getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Item must selected.");
            return;
        }
        if (customer_id == "" || customer_id == undefined) {
            Ext.MessageBox.alert("Error", "Customer must selected.");
            this.barang.reset();
            return;
        }
        var barang = jun.getBarang(barang_id);
        this.price.setValue(barang.data.price);
        this.price.setValue(barang.data.price);
        this.disc.setValue(0);
        // this.disc_name.setValue(response.msg.nama_status);
        this.onDiscChange(this.disc);
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    focusGrid: function () {
        var record = this.sm.getSelected();
        var idx = 0;
        if (record == undefined) {
            var total_store = this.store.data.length;
            idx = total_store - 1;
        } else {
            idx = this.store.indexOf(record);
        }
        this.getSelectionModel().selectRow(idx);
        this.getView().focusRow(idx);
    },
    onClickbtnEdit: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            Ext.getCmp('win-SalestransNoBeauty').statusActive = 1;
            return;
        }
        // if (record.data.paket_trans_id.trim().length > 0) {
        //     Ext.MessageBox.alert("Warning", "Can't edit this product/threatment because part of packages.");
        //     Ext.getCmp('win-SalestransNoBeauty').statusActive = 1;
        //     return;
        // }
        if (this.btnEdit.text == 'Edit (F2)') {
            this.barang.setValue(record.data.barang_id);
            this.onChangeBarang();
            this.qty.setValue(record.data.qty);
            this.disc.setValue(record.data.disc);
            this.disca.setValue(record.data.discrp);
            this.ketpot.setValue(record.data.ketpot);
            this.price.setValue(record.data.price);
            this.disc_name.setValue(record.data.disc_name);
            this.btnEdit.setText("Save (F2)");
            this.btnDisable(true);
        } else {
            this.loadForm();
            this.btnEdit.setText("Edit (F2)");
            this.btnDisable(false);
        }
        this.barang.focus();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (record.data.paket_trans_id != '') {
            Ext.MessageBox.alert("Warning", "Can't edit this product/threatment because part of packages.");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        this.store.remove(record);
        var barang = jun.getBarang(record.data.barang_id);
        menuDetil(barang.data.kode_barang, "-" + record.data.total, 'TOTAL',
            parseFloat(Ext.getCmp("totalid").getValue()));
        Ext.getCmp('win-SalestransNoBeauty').statusActive = 1;
    }
});
jun.SalestransCounterDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    id: 'docs-jun.SalestransCounterDetailsGrid',
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    enableKeyEvents: true,
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'paket_trans_id',
            sortable: false,
            resizable: false,
            dataIndex: 'paket_trans_id',
            width: 40,
            hidden: true
        },
        {
            header: 'Pkg',
            sortable: false,
            resizable: false,
            dataIndex: 'paket_trans_id',
            width: 30,
            align: "left",
            renderer: jun.renderPackageIco
        },
        {
            header: 'Items Code',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 75,
            renderer: jun.renderKodeBarang
        },
        {
            header: 'Items Name',
            sortable: true,
            resizable: true,
            dataIndex: 'barang_id',
            width: 200,
            renderer: jun.renderBarang
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'ketpot',
            width: 100
        },
        {
            header: 'Qty',
            sortable: true,
            resizable: true,
            dataIndex: 'qty',
            width: 25,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0")
        },
        {
            header: 'Price',
            sortable: true,
            resizable: true,
            dataIndex: 'price',
            width: 75,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc Code',
            sortable: true,
            resizable: true,
            dataIndex: 'disc_name',
            width: 75
        },
        {
            header: 'Disc',
            sortable: true,
            resizable: true,
            dataIndex: 'disc',
            width: 45,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Disc Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'discrp',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Total',
            sortable: true,
            resizable: true,
            dataIndex: 'total',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztSalestransCounterDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Item :'
                        },
                        {
                            xtype: 'combo',
                            style: 'margin-bottom:2px',
                            ////typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            //forceSelection: true,
                            store: jun.rztBarangCmp,
                            hiddenName: 'barang_id',
                            valueField: 'barang_id',
                            ref: '../../barang',
                            id: 'barangdetailsid',
                            displayField: 'kode_barang'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Qty :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'qtyid',
                            ref: '../../qty',
                            style: 'margin-bottom:2px',
                            enableKeyEvents: true,
                            width: 50,
                            value: 1,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Price :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'priceid',
                            ref: '../../price',
                            style: 'margin-bottom:2px',
                            width: 55,
                            readOnly: true,
                            enableKeyEvents: true,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc :',
                            hidden: false
                        },
                        {
                            xtype: 'numericfield',
                            id: 'discdetilid',
                            style: 'margin-bottom:2px',
                            ref: '../../disc',
                            width: 50,
                            readOnly: true,
                            enableKeyEvents: true,
                            maxLength: 5,
                            value: 0,
                            minValue: 0,
                            maxValue: 100,
                            hidden: false
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Disc Amount :',
                            hidden: false
                        },
                        {
                            xtype: 'numericfield',
                            id: 'discadetilid',
                            style: 'margin-bottom:2px',
                            ref: '../../disca',
                            width: 55,
                            readOnly: true,
                            enableKeyEvents: true,
                            value: 0,
                            minValue: 0,
                            hidden: false
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'textfield',
                            id: 'ketpotid',
                            ref: '../../ketpot',
                            width: 594,
                            colspan: 9,
                            maxLength: 255
                        },
                        {
                            xtype: 'hidden',
                            ref: '../../disc_name'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    id: 'btnsalesdetilid',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add (F1)',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit (F2)',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del (Delete)',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.SalestransCounterDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.onAddSaveClick, this);
        this.btnEdit.on('Click', this.onAddSaveClick, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.qty.on('keyup', this.onDiscChange, this);
        this.price.on('keyup', this.onDiscChange, this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.disca.on('keyup', this.onDiscChange, this);
        this.barang.on('select', this.onChangeBarang, this);
        this.on("activate", this.onActive, this);
        this.on("keypress", this.onKeyPress, this);
        this.refreshReadOnly();
    },
    refreshReadOnly: function () {
        if (SALES_OVERRIDE == "1") {
            this.disc.setReadOnly(false);
            this.disca.setReadOnly(false);
            this.price.setReadOnly(false);
        } else {
            this.disc.setReadOnly(true);
            this.disca.setReadOnly(true);
            this.price.setReadOnly(true);
            Ext.getCmp('priceid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discdetilid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discadetilid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
        }
    },
    onKeyPress: function (e) {
        switch (e.getKey()) {
            case 46:
                if (!this.btnDelete.disabled) {
                    this.deleteRec();
                }
                break;
        }
    },
    onActive: function (t) {
        t.getKeyMap().addBinding({
            key: [Ext.EventObject.F1, Ext.EventObject.F2, Ext.EventObject.DELETE, Ext.EventObject.F3],
            handler: function (key, e) {
                switch (key) {
                    case Ext.EventObject.F1:
                        if (!this.btnAdd.disabled) {
                            this.barang.focus();
                            this.onAddSaveClick(this.btnAdd, null);
                        }
                        break;
                    case Ext.EventObject.F2:
                        if (!this.btnEdit.disabled) {
                            this.onAddSaveClick(this.btnEdit, null);
                        }
                        break;
                    case Ext.EventObject.DELETE:
                        if (!this.btnDelete.disabled) {
                            this.deleteRec();
                        }
                        break;
                    case Ext.EventObject.F3:
                        this.barang.collapse();
                        this.focusGrid();
                        break;
                }
            },
            stopEvent: true,
            scope: this
        });
    },
    onDiscChange: function (a, e) {
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var bruto = round(price * qty, 2);
        if (a.id == "discdetilid" || a.id == "priceid" || a.id == "qtyid") {
            this.disca.setValue(round(bruto * (disc / 100), 2));
        } else {
            var key = e.getKey();
            if (key == Ext.EventObject.TAB) {
                return;
            }
            if (a.id == "discadetilid") {
                this.disc.setValue(0);
            }
        }
    },
    onStoreChange: function () {
        jun.rztSalestransCounterDetails.refreshData();
    },
    onAddSaveClick: function (b, e) {
        if (b.getText() == 'Edit (F2)') {
            this.onClickbtnEdit();
        } else {
            var price = parseFloat(this.price.getValue());
            if (price == 0) {
                Ext.MessageBox.confirm('Questions', 'Anda yakin input dengan harga nol?', function (btn, text) {
                    if (btn == 'no') {
                        return;
                    }
                    if (b.getText() == 'Save (F2)') {
                        this.onClickbtnEdit();
                    } else {
//                        this.loadForm();
                        this.cekStock();
                    }
                }, this);
            } else {
                if (b.getText() == 'Save (F2)') {
                    this.onClickbtnEdit();
                } else {
//                    this.loadForm();
                    this.cekStock();
                }
            }
        }
    },
    cekStock: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "" || barang_id == undefined) {
            Ext.MessageBox.alert("Error", "You have not selected a item", function () {
                this.barang.focus();
            }, this);
            return;
        }
//      kunci barang sama tidak boleh masuk dua kali
        console.log(this.btnEdit.text);
        if (this.btnEdit.text != 'Save') {
            var a = jun.rztSalestransQtyDetails.findExact("barang_id", barang_id);
            if (a > -1) {
                Ext.MessageBox.alert("Error", "Item already inputted");
                return;
            }
        }
        var barang = jun.getBarang(barang_id);
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        if (qty < 1) {
            Ext.MessageBox.alert("Error", "Qty minimal 1");
            return;
        }
//      cek stok
        Ext.Ajax.request({
            url: 'StockMoves/HitungStock',
            method: 'POST',
            scope: this,
            params: {
                barang_id: barang_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.msg != '') {
                    Ext.MessageBox.alert("Error", response.msg);
                    return;
                }
                if (response.success) {
                    this.loadForm(response.sm);
                }                
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },    
    cekStockEdit: function () {
        var barang_id = this.barang.getValue();
//      cek stok
        Ext.Ajax.request({
            url: 'StockMoves/HitungStock',
            method: 'POST',
            scope: this,
            params: {
                barang_id: barang_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.msg != '') {
                    Ext.MessageBox.alert("Error", response.msg);
                    return;
                }
                var barang_id = Ext.getCmp('docs-jun.SalestransCounterDetailsGrid').barang.getValue();
                var barang = jun.getBarang(barang_id);                
                var price = Ext.getCmp('docs-jun.SalestransCounterDetailsGrid').price.getValue();
                var qty = Ext.getCmp('docs-jun.SalestransCounterDetailsGrid').qty.getValue();
                var disc = parseFloat(Ext.getCmp('docs-jun.SalestransCounterDetailsGrid').disc.getValue());
                var disca = parseFloat(Ext.getCmp('docs-jun.SalestransCounterDetailsGrid').disca.getValue());
                var disc1 = parseFloat(Ext.getCmp('discid').getValue());
                var ketpot = Ext.getCmp('docs-jun.SalestransCounterDetailsGrid').ketpot.getValue();
                var disc_name = Ext.getCmp('docs-jun.SalestransCounterDetailsGrid').disc_name.getValue();
               
                var bruto = round(price * qty, 2);
                var vat = jun.getTax(barang_id);
                var subtotal = bruto - disca;
                var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
                var totalpot = disca + discrp1;
                var total_with_disc = bruto - totalpot;
                var total = bruto - disca;
                var vatrp = round(total_with_disc * vat, 2);
                if (response.success) {
//                    this.loadForm(response.msg);
                    var record = this.sm.getSelected();
                    record.set('barang_id', barang_id);
                    record.set('qty', qty);
                    record.set('price', price);
                    record.set('disc', disc);
                    record.set('discrp', disca);
                    record.set('ketpot', ketpot);
                    record.set('vat', vat);
                    record.set('vatrp', vatrp);
                    record.set('total_pot', totalpot);
                    record.set('total', total);
                    record.set('bruto', bruto);
                    record.set('disc_name', disc_name);
                    record.set('disc1', disc1);
                    record.set('discrp1', discrp1);
                    record.set('stock', response.sm);
                    record.commit();
                }         
                this.barang.reset();
                this.qty.reset();
                this.price.reset();
                this.ketpot.reset();
                this.disc.reset();
                this.disca.reset();
                menuDetil(barang.data.kode_barang, total, 'TOTAL', parseFloat(Ext.getCmp("totalid").getValue()));
                this.barang.focus();
                this.refreshReadOnly();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    addItem: function (barang_id, kode_barang, price, qty, disc, disca, disc1, ketpot, disc_name) {
        if (barang_id == "" || barang_id == undefined) {
            Ext.MessageBox.alert("Error", "You have not selected a item", function () {
                this.barang.focus();
            }, this);
            return;
        }
        // this.onDiscChange(disc);
//      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztSalestransCounterDetails.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Item already inputted");
//                return;
//            }
//        }
        if (qty < 1) {
            Ext.MessageBox.alert("Error", "Qty minimal 1");
            return;
        }
        var bruto = round(price * qty, 2);
        var vat = jun.getTax(barang_id);
        var subtotal = bruto - disca;
        var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
        var totalpot = disca + discrp1;
        var total_with_disc = bruto - totalpot;
        var total = bruto - disca;
        var vatrp = round(total_with_disc * vat, 2);
        if (this.btnEdit.text == 'Save (F2)') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.set('price', price);
            record.set('disc', disc);
            record.set('discrp', disca);
            record.set('ketpot', ketpot);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc_name', disc_name);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        } else {
            var c = jun.rztSalestransCounterDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty,
                    price: price,
                    disc: disc,
                    discrp: disca,
                    ketpot: ketpot,
                    vat: vat,
                    vatrp: vatrp,
                    total_pot: totalpot,
                    bruto: bruto,
                    disc_name: disc_name,
                    total: total,
                    disc1: disc1,
                    discrp1: discrp1,
                    beauty_id: '',
                    beauty2_id: '',
                    paket_trans_id: '',
                    paket_details_id: ''
                });
            jun.rztSalestransCounterDetails.add(d);
        }
        // clearText();
        menuDetil(kode_barang, total, 'TOTAL', parseFloat(Ext.getCmp("totalid").getValue()));
        this.barang.focus();
        this.refreshReadOnly();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var barang_id = this.barang.getValue();
        if (barang_id == "" || barang_id == undefined) {
            Ext.MessageBox.alert("Error", "You have not selected a item", function () {
                this.barang.focus();
            }, this);
            return;
        }
//      kunci barang sama tidak boleh masuk dua kali
//        if (this.btnEdit.text != 'Save') {
//            var a = jun.rztSalestransCounterDetails.findExact("barang_id", barang_id);
//            if (a > -1) {
//                Ext.MessageBox.alert("Error", "Item already inputted");
//                return;
//            }
//        }        
        var barang = jun.getBarang(barang_id);
        var price = parseFloat(this.price.getValue());
        var qty = parseFloat(this.qty.getValue());
        if (qty < 1) {
            Ext.MessageBox.alert("Error", "Qty minimal 1");
            return;
        }
        var disc = parseFloat(this.disc.getValue());
        var disca = parseFloat(this.disca.getValue());
        var disc1 = parseFloat(Ext.getCmp('discid').getValue());
        var ketpot = this.ketpot.getValue();
        var disc_name = this.disc_name.getValue();
        var bruto = round(price * qty, 2);
        var vat = jun.getTax(barang_id);
        var subtotal = bruto - disca;
        var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
        var totalpot = disca + discrp1;
        var total_with_disc = bruto - totalpot;
        var total = bruto - disca;
        var vatrp = round(total_with_disc * vat, 2);
        if (this.btnEdit.text == 'Save (F2)') {
            var record = this.sm.getSelected();
            record.set('barang_id', barang_id);
            record.set('qty', qty);
            record.set('price', price);
            record.set('disc', disc);
            record.set('discrp', disca);
            record.set('ketpot', ketpot);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc_name', disc_name);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        } else {
            var c = jun.rztSalestransCounterDetails.recordType,
                d = new c({
                    barang_id: barang_id,
                    qty: qty,
                    price: price,
                    disc: disc,
                    discrp: disca,
                    ketpot: ketpot,
                    vat: vat,
                    vatrp: vatrp,
                    total_pot: totalpot,
                    bruto: bruto,
                    disc_name: disc_name,
                    total: total,
                    disc1: disc1,
                    discrp1: discrp1,
                    beauty_id: '',
                    beauty2_id: '',
                    paket_trans_id: '',
                    paket_details_id: ''
                });
            jun.rztSalestransCounterDetails.add(d);
        }
        // clearText();
        this.barang.reset();
        this.qty.reset();
        this.price.reset();
        this.ketpot.reset();
        this.disc.reset();
        this.disca.reset();
        menuDetil(barang.data.kode_barang, total, 'TOTAL', parseFloat(Ext.getCmp("totalid").getValue()));
        this.barang.focus();
        this.refreshReadOnly();
    },
    onChangeBarang: function () {
        var barang_id = this.barang.getValue();
        var customer_id = Ext.getCmp("customersales_id").getValue();
        if (barang_id == "") {
            Ext.MessageBox.alert("Error", "Item must selected.");
            return;
        }
        if (customer_id == "" || customer_id == undefined) {
            Ext.MessageBox.alert("Error", "Customer must selected.");
            this.barang.reset();
            return;
        }
        var barang = jun.getBarang(barang_id);
        // this.price.setValue(barang.data.price);
        // this.disc.setValue(0);
        // this.disc_name.setValue(response.msg.nama_status);
        // this.onDiscChange(this.disc);
        Ext.Ajax.request({
            url: 'SalestransDetails/GetDiskon',
            method: 'POST',
            scope: this,
            params: {
                customer_id: customer_id,
                barang_id: barang_id
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                this.price.setValue(response.msg.price);
                this.disc.setValue(response.msg.value);
                this.disc_name.setValue(response.msg.kode);
                this.onDiscChange(this.disc);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    focusGrid: function () {
        var record = this.sm.getSelected();
        var idx = 0;
        if (record == undefined) {
            var total_store = this.store.data.length;
            idx = total_store - 1;
        } else {
            idx = this.store.indexOf(record);
        }
        this.getSelectionModel().selectRow(idx);
        this.getView().focusRow(idx);
    },
    onClickbtnEdit: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            Ext.getCmp('win-SalestransCounter').statusActive = 1;
            return;
        }
        // if (record.data.paket_trans_id.trim().length > 0) {
        //     Ext.MessageBox.alert("Warning", "Can't edit this product/threatment because part of packages.");
        //     Ext.getCmp('win-SalestransCounter').statusActive = 1;
        //     return;
        // }
        if (this.btnEdit.text == 'Edit (F2)') {
            this.barang.setValue(record.data.barang_id);
//            this.onChangeBarang();
            this.qty.setValue(record.data.qty);
            this.disc.setValue(record.data.disc);
            this.disca.setValue(record.data.discrp);
            this.ketpot.setValue(record.data.ketpot);
            this.price.setValue(record.data.price);
            this.disc_name.setValue(record.data.disc_name);
            this.btnEdit.setText("Save (F2)");
            this.btnDisable(true);
        } else {
//            this.loadForm();
            this.cekStockEdit();
            this.btnEdit.setText("Edit (F2)");
            this.btnDisable(false);
        }
        this.barang.focus();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        if (record.data.paket_trans_id != '') {
            Ext.MessageBox.alert("Warning", "Can't edit this product/threatment because part of packages.");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this item?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        this.store.remove(record);
        var barang = jun.getBarang(record.data.barang_id);
        menuDetil(barang.data.kode_barang, "-" + record.data.total, 'TOTAL',
            parseFloat(Ext.getCmp("totalid").getValue()));
        Ext.getCmp('docs-SalestransCounterWin').statusActive = 1;
    }
});
