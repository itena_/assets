jun.TransaksiDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.TransaksiDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransaksiDetailStoreId',
            url: 'projection/TransaksiDetail',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'transaksi_detail_id'},
                {name:'transaksi_id'},
                {name:'outlet_id'},
                {name:'produk_id'},
                {name:'groupproduk_id'},
                {name:'unit_id'},
                {name:'businessunit_id'},
                {name:'doc_ref'},
                {name:'namaoutlet'},
                {name:'kodeoutlet'},
                {name:'kodeproduk'},
                {name:'kodegroupproduk'},
                {name:'harga'},
                {name:'hargabeli'},
                {name:'jmloutlet'},
                {name:'persentase'},
                {name:'qty'},
                {name:'satuan'},
                {name:'salesqty'},
                {name:'salesrp'},
                {name:'salesrpbeli'},
                {name:'tdate'},
                {name:'note'},
                {name:'created_at'},
                {name:'uploadfile'},
                {name:'uploaddate'},
                
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        //var total = this.sum("debit") - this.sum("kredit");
//        console.log(total);
        Ext.getCmp("salesqty_id").setValue(this.sum("salesqty"));
        Ext.getCmp("salesrp_id").setValue(this.sum("salesrp"));
        //Ext.getCmp("hutangBalance").setValue(total);
    }
});
jun.rztTransaksiDetail = new jun.TransaksiDetailstore();
//jun.rztTransaksiDetail.load();
