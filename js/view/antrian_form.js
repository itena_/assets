jun.AntrianWin = Ext.extend(Ext.Window, {
    title: 'Antrian',
    modez: 1,
    width: 400,
    height: 300,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Antrian',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'nomor',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nomor',
                        id: 'nomorid',
                        ref: '../nomor',
                        maxLength: 11,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'no_member',
                        hideLabel: false,
                        //hidden:true,
                        name: 'no_member',
                        id: 'no_memberid',
                        ref: '../no_member',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'kartu',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kartu',
                        id: 'kartuid',
                        ref: '../kartu',
                        maxLength: 10,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'bagian',
                        hideLabel: false,
                        //hidden:true,
                        name: 'bagian',
                        id: 'bagianid',
                        ref: '../bagian',
                        maxLength: 11,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'tujuan',
                        hideLabel: false,
                        //hidden:true,
                        name: 'tujuan',
                        id: 'tujuanid',
                        ref: '../tujuan',
                        maxLength: 10,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'picked',
                        hideLabel: false,
                        //hidden:true,
                        name: 'picked',
                        id: 'pickedid',
                        ref: '../picked',
                        maxLength: 6,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tanggal',
                        fieldLabel: 'tanggal',
                        name: 'tanggal',
                        id: 'tanggalid',
                        format: 'd M Y',
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'timestamp',
                        hideLabel: false,
                        //hidden:true,
                        name: 'timestamp',
                        id: 'timestampid',
                        ref: '../timestamp',
                        maxLength: 30,
                        //allowBlank: ,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'spesial',
                        hideLabel: false,
                        //hidden:true,
                        name: 'spesial',
                        id: 'spesialid',
                        ref: '../spesial',
                        maxLength: 1,
                        //allowBlank: ,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.AntrianWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz;
        if (this.modez == 1 || this.modez == 2) {
            urlz = 'Antrian/update/id/' + this.id;
        } else {
            urlz = 'Antrian/create/';
        }
        Ext.getCmp('form-Antrian').getForm().submit({
            url: urlz,
            timeOut: 1000,
            scope: this,
            success: function (f, a) {
                jun.rztAntrian.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-Antrian').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.AntrianGridWin = Ext.extend(Ext.Panel, {
    title: "Antrian",
    id: 'docs-jun.AntrianGridWin',
    anchor: '100%',
    layout: 'hbox',
    layoutConfig: {
        padding: 5,
        align: 'stretch'
    },
    defaults: {
        frame: false
    },
    autoScroll: true,
    initComponent: function () {
        jun.rztAntrian.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tujuan = Ext.getCmp('cmb_tujuan_pasien_id');
                    b.params.tujuan = tujuan.getValue();
                    b.params.mode = "grid";
                }
            }
        });
        jun.rztAntrianPending.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    var tujuan = Ext.getCmp('cmb_tujuan_pasien_id');
                    b.params.tujuan = tujuan.getValue();
                    b.params.mode = "grid";
                }
            }
        });
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 2,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Tujuan Pasien :'
                        },
                        new Ext.form.ComboBox({
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            style: 'margin-bottom:2px',
                            ref: '../../cmb_tujuan_pasien',
                            id: 'cmb_tujuan_pasien_id',
                            store: new Ext.data.ArrayStore({
                                id: 'cmb_tujuan_pasien',
                                fields: [
                                    'value',
                                    'label'
                                ],
                                data: [
                                    ['pembelian', 'Pembelian'],
                                    ['konsultasi', 'Konsultasi'],
                                    ['all', 'Terpadu']
                                ]
                            }),
                            width: 100,
                            value: 'all',
                            allowBlank: false,
                            valueField: 'value',
                            displayField: 'label'
                        }),
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Interval :'
                        },
                        {
                            xtype: 'spinnerfield',
                            minValue: 5,
                            maxValue: 1000,
                            value: 20,
                            id: 'interval_antrianid',
                            ref: '../../interval',
                            width: 100,
                            accelerate: true
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'P A N G G I L',
                            width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnPanggil'
                        },
                        {
                            xtype: 'button',
                            text: 'U L A N G I',
                            width: 75,
                            disabled: true,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnUlang'
                        },
                        {
                            xtype: 'button',
                            text: 'P E N D I N G',
                            width: 75,
                            disabled: true,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnPending'
                        },
                        {
                            xtype: 'button',
                            text: 'S P E S I A L',
                            width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnSpesial'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    ref: '../grplabel',
                    defaults: {
                        scale: 'large'
                    },
                    width: 350,
                    items: [
                        {
                            xtype: 'label',
                            width: '100px',
                            style: {
                                padding: '10px',
                                font: 'bold 36px courier'
                            },
                            ref: '../../lblAntrian'
                        },
                        {
                            xtype: 'label',
                            width: '250px',
                            style: {
                                padding: '10px',
                                font: 'bold 36px courier'
                            },
                            ref: '../../lblBagian'
                        }
                    ]
                }
            ]
        };
        this.items = [
            new jun.AntrianGrid({
                frameHeader: !1,
                margins: '0 5 0 0',
                header: !1,
                flex: 5
            }),
            new jun.AntrianPendingGrid({
                frameHeader: !1,
                header: !1,
                flex: 5
            })
        ];
        jun.AntrianGridWin.superclass.initComponent.call(this);
        this.btnPanggil.on('click', this.onPanggilClick, this);
        this.btnUlang.on('click', this.onUlangClick, this);
        this.btnPending.on('click', this.onPendingClick, this);
        this.btnSpesial.on('click', this.onSpesialClick, this);
        this.cmb_tujuan_pasien.on('select', this.refreshCmbTujuanPasien, this);
        this.on("close", this.onWinClose, this);
        this.on("afterrender", this.onafterrender, this);
        this.interval.on("spin", this.onSpin, this);
        this.interval.on("change", this.onSpinChange, this);
        this.taskAntrian = {
            run: this.refreshStoreAntrian,
            interval: this.interval.getValue() * 1000
        };
    },
    onUlangClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = JSON.parse(localStorage.getItem("antrian"));
        Ext.Ajax.request({
            url: 'Antrian/Ulangi/',
            method: 'POST',
            scope: this,
            params: {
                nomor: storedNames[0],
                bagian: storedNames[1],
                tujuan: storedNames[2],
                id_antrian: storedNames[3],
                spesial: storedNames[4]
            },
            success: function (f, a) {
                // var response = Ext.decode(f.responseText);
                t.setDisabled(false);
            },
            failure: function (f, a) {
                t.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onPanggilClick: function () {
        this.btnPanggil.setDisabled(true);
        var tujuan = Ext.getCmp('cmb_tujuan_pasien_id');
        Ext.Ajax.request({
            url: 'Antrian/panggil/',
            method: 'POST',
            scope: this,
            params: {
                mode: 'antrian',
                bagian: tujuan.getValue()
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success) {
                    if (response.msg != "Tidak ada antrian.") {
                        var antrian = [];
                        antrian[0] = response.msg[0];
                        antrian[1] = response.msg[1];
                        antrian[2] = response.msg[2];
                        antrian[3] = response.msg[3];
                        antrian[4] = response.msg[4];
                        localStorage.setItem("antrian", JSON.stringify(antrian));
                        this.btnPending.setDisabled(false);
                        this.btnUlang.setDisabled(false);
                        if (antrian[4] == '1') {
                            antrian[0] = antrian[0] + 'C';
                        } else {
                            if (antrian[2] == 'konsultasi') {
                                antrian[0] = antrian[0] + 'A';
                            } else {
                                antrian[0] = antrian[0] + 'B';
                            }
                        }
                        this.lblAntrian.setText(antrian[0]);
                        this.lblBagian.setText(antrian[1]);
                        this.lblAntrian.setWidth(250);
                        this.lblBagian.setWidth(100);
                        this.grplabel.setWidth(400);
                        this.refreshStoreAntrian();
                    } else {
                        localStorage.removeItem("antrian");
                        this.btnPending.setDisabled(true);
                        this.btnUlang.setDisabled(true);
                        Ext.Msg.alert('Information', response.msg);
                    }
                } else {
                    Ext.Msg.alert('Information', response.msg);
                }
                this.btnPanggil.setDisabled(false);
            },
            failure: function (f, a) {
                this.btnPanggil.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onPendingClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = JSON.parse(localStorage.getItem("antrian"));
        Ext.Ajax.request({
            url: 'Antrian/Update/',
            method: 'POST',
            scope: this,
            params: {
                id: storedNames[3],
                mode: 'pending'
            },
            success: function (f, a) {
                // var response = Ext.decode(f.responseText);
                t.setDisabled(false);
                this.btnPending.setDisabled(true);
                this.btnUlang.setDisabled(true);
                this.refreshStoreAntrian();
                localStorage.removeItem("antrian");
            },
            failure: function (f, a) {
                t.setDisabled(false);
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onSpesialClick: function (t, e) {
        t.setDisabled(true);
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin membuat antrian spesial?', this.spesialRecYes, this);
        t.setDisabled(false);
    },
    spesialRecYes: function (t, e) {
        if (t == 'no') {
            return;
        }
        Ext.Ajax.request({
            url: 'Antrian/Spesial/',
            method: 'POST',
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                Ext.Msg.alert('Informasi', response.msg);
                this.refreshStoreAntrian();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onSpinChange: function (t, n, o) {
        this.onSpin((n < 0), false);
    },
    onSpin: function (d, a) {
        if (d) {
            if (parseInt(this.interval.getValue()) < this.interval.minValue) {
                this.interval.setValue(this.interval.minValue);
            }
        }
        this.taskAntrian.interval = this.interval.getValue() * 1000;
    },
    onafterrender: function () {
        jun.runner.start(this.taskAntrian);
        if (localStorage.getItem("antrian") === null) {
            this.btnPending.setDisabled(true);
            this.btnUlang.setDisabled(true);
            this.lblAntrian.setText('');
            this.lblBagian.setText('');
        } else {
            this.btnPending.setDisabled(false);
            this.btnUlang.setDisabled(false);
            var storedNames = JSON.parse(localStorage.getItem("antrian"));
            if (storedNames[4] == '1') {
                storedNames[0] = storedNames[0] + 'C';
            } else {
                if (storedNames[2] == 'konsultasi') {
                    storedNames[0] = storedNames[0] + 'A';
                } else {
                    storedNames[0] = storedNames[0] + 'B';
                }
            }
            this.lblAntrian.setText(storedNames[0]);
            this.lblBagian.setText(storedNames[1]);
        }
    },
    onWinClose: function () {
        jun.runner.stop(this.taskAntrian);
    },
    refreshCmbTujuanPasien: function () {
        this.refreshStoreAntrian();
    },
    refreshStoreAntrian: function () {
        jun.rztAntrian.load();
        jun.rztAntrianPending.load();
    }
});
jun.AntrianFOGridWin = Ext.extend(Ext.Panel, {
    title: "AntrianFO",
    id: 'docs-jun.AntrianFOGridWin',
    anchor: '100%',
    layout: 'hbox',
    layoutConfig: {
        // padding: 5,
        align: 'stretch'
    },
    autoScroll: true,
    initComponent: function () {
        jun.rztAntrianFO.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.bagian = "counter";
                    b.params.counter = jun.Counter;
                    b.params.mode = "grid";
                }
            }
        });
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 9,
                    defaults: {
                        scale: 'medium'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'PANGGIL',
                            // width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 10px courier'
                            },
                            ref: '../../btnPanggil'
                        },
                        {
                            xtype: 'button',
                            text: 'ULANGI',
                            // width: 75,
                            disabled: true,
                            style: {
                                padding: '1px',
                                font: 'normal 10px courier'
                            },
                            ref: '../../btnUlang'
                        },
                        {
                            xtype: 'button',
                            text: 'PENDING',
                            // width: 75,
                            disabled: true,
                            style: {
                                padding: '1px',
                                font: 'normal 10px courier'
                            },
                            ref: '../../btnPending'
                        },
                        {
                            xtype: 'button',
                            text: 'NO PASIEN',
                            // width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 10px courier'
                            },
                            ref: '../../btnSpesial'
                        },
                        {
                            xtype: 'button',
                            text: 'KONSUL',
                            // width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 10px courier'
                            },
                            ref: '../../btnKonsul',
                            hidden: jun.Counter == 'E'
                        },
                        {
                            xtype: 'button',
                            text: 'REQ KONSUL',
                            // width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 10px courier'
                            },
                            ref: '../../btnReqKonsul',
                            hidden: jun.Counter == 'E'
                        },
                        {
                            xtype: 'button',

                            //text: 'NO KONSUL',
                            text: jun.Counter == 'E' ? 'KASIR':'NO KONSUL',
                            // width: 75,
                            id: 'btnnokonsulcounterantriankasir',
                            style: {
                                padding: '1px',
                                font: 'normal 10px courier'
                            },
                            ref: '../../btnEnd'
                        },
                        {
                            xtype: 'button',
                            text: 'LAINNYA',
                            // width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 10px courier'
                            },
                            ref: '../../btnOther'
                        },
                        {
                            xtype: 'button',
                            text: 'RELEASE',
                            // width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 200px courier'
                            },
                            ref: '../../btnRelease',
                            hidden: jun.Counter != 'E'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    ref: '../grplabel',
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: {
                                padding: '1px',
                                font: 'bold 35px sans-serif'
                            },
                            ref: '../../lblAntrian'
                        },
                        {
                            xtype: 'label',
                            style: {
                                font: '35px courier'
                            },
                            text: '{'
                        },
                        {
                            xtype: 'label',
                            style: {
                                padding: '1px',
                                font: 'bold 12px sans-serif'
                            },
                            ref: '../../lblBagian'
                        },
                        {
                            xtype: 'label',
                            style: {
                                font: '35px courier'
                            },
                            text: '}'
                        }
                    ]
                }
            ]
        };
        this.items = [
            new jun.AntrianFOGrid({
                frameHeader: !1,
                margins: '0 1 0 0',
                header: !1,
                flex: 5
            }),
            new jun.AntrianFOPendingGrid({
                frameHeader: !1,
                header: !1,
                flex: 5
            })
        ];
        jun.AntrianFOGridWin.superclass.initComponent.call(this);
        this.btnPanggil.on('click', this.onPanggilClick, this);
        this.btnUlang.on('click', this.onUlangClick, this);
        this.btnPending.on('click', this.onPendingClick, this);
        this.btnSpesial.on('click', this.onSpesialClick, this);
        this.btnEnd.on('click', this.onEndClick, this);
        this.btnKonsul.on('click', this.onKonsulClick, this);
        this.btnReqKonsul.on('click', this.onReqKonsulClick, this);
        this.btnOther.on('click', this.onOtherClick, this);
        this.btnRelease.on('click', this.onReleaseClick, this);
        this.on("afterrender", this.onafterrender, this);
    },
    onReleaseClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianFO');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/release/',
            method: 'POST',
            scope: this,
            params: {
                mode: 'selesai',
                // bagian: 'selesai',
                id_antrian: storedNames.id_antrian,
                no_base: storedNames.no_base
            },
            success: function (f, a) {
                Locstor.remove('AntrianFO');
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onOtherClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianFO');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Msg.show({
            title: 'Alasan',
            msg: 'Masukan alasan mengapa pasien selesai tanpa transaksi.',
            buttons: Ext.MessageBox.OKCANCEL,
            multiline: true,
            fn: function (btn, txt) {
                if (btn != 'ok') {
                    Ext.getCmp('docs-jun.AntrianFOGridWin').updateLabel();
                    return;
                }
                if (txt == '') {
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: 'Alasan harus diisi.',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                    Ext.getCmp('docs-jun.AntrianFOGridWin').updateLabel();
                    return;
                }
                Ext.Ajax.request({
                    url: 'AishaAntrian/update/',
                    method: 'POST',
                    scope: this,
                    params: {
                        mode: 'alasan',
                        bagian: 'selesai',
                        alasan: txt,
                        id_antrian: storedNames.id_antrian,
                        no_base: storedNames.no_base
                    },
                    success: function (f, a) {
                        Locstor.remove('AntrianFO');
                        Ext.getCmp('docs-jun.AntrianFOGridWin').updateLabel();
                    },
                    failure: function (f, a) {
                        Ext.getCmp('docs-jun.AntrianFOGridWin').updateLabel();
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            }
        });
    },
    onEndClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianFO');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                mode: 'perawatan',
                bagian: 'perawatan',
                id_antrian: storedNames.id_antrian,
                no_base: storedNames.no_base
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (!response.success) {
                    Ext.Msg.alert('Information', response.msg);
                } else {
                    Locstor.remove('AntrianFO');
                }
                this.updateLabel();
                Ext.getCmp('docs-jun.SalestransCounterPanelWin').show();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onKonsulClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianFO');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                mode: 'selesai',
                // bagian: 'selesai',
                id_antrian: storedNames.id_antrian,
                no_base: storedNames.no_base
            },
            success: function (f, a) {
                Locstor.remove('AntrianFO');
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onReqKonsulClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianFO');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Msg.show({
            title: 'Request Konsul',
            msg: 'Pilih tujuan Request Konsul.',
            buttons: Ext.MessageBox.OKCANCEL,
            multiline: false,
            inputField: new Ext.form.ComboBox({
                xtype: 'combo',
                //typeAhead: true,
                editable: false,
                fieldLabel: 'Ruangan Konsul',
                ref: '../req_konsul',
                triggerAction: 'all',
                lazyRender: true,
                mode: 'local',
                id: "reqkonsulvalue",
                store: [
                    ['1', 'Konsul 1'],
                    ['2', 'Konsul 2']
                ],
                hiddenName: 'req_konsul',
                valueField: 'req_konsul',
//                displayField: 'nama_dokter',
                allowBlank: true,
                width: '100%'
            }),
            fn: function (btn, txt) {
                if (btn != 'ok') {
                    Ext.getCmp('docs-jun.AntrianFOGridWin').updateLabel();
                    return;
                }
                Ext.Ajax.request({
                    url: 'AishaAntrian/update/',
                    method: 'POST',
                    scope: this,
                    params: {
                        mode: 'requestkonsul',
                        // bagian: 'selesai',
                        id_antrian: storedNames.id_antrian,
                        no_base: storedNames.no_base,
                        req_konsul: Ext.getCmp('reqkonsulvalue').getValue()
                    },
                    success: function (f, a) {
                        Locstor.remove('AntrianFO');
                        Ext.getCmp('docs-jun.AntrianFOGridWin').updateLabel();
                    },
                    failure: function (f, a) {
                        Ext.getCmp('docs-jun.AntrianFOGridWin').updateLabel();
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            }
        });
    },
    onUlangClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianFO');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/UlangiCounter/',
            method: 'POST',
            scope: this,
            params: {
                nomor: storedNames.nomor,
                bagian: 'counter',
                counter: jun.Counter,
                no_base: storedNames.no_base,
                id_antrian: storedNames.id_antrian,
                customer_id: storedNames.customer_id,
                nama_customer: storedNames.nama_customer
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (!response.success) {
                    Ext.Msg.alert('Information', response.msg);
                }
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onPanggilClick: function () {
        this.btnPanggil.setDisabled(true);
        var storedNames = Locstor.get('AntrianFO');

        if (storedNames !== null) {
            if (storedNames.no_base == '') {
                Ext.Msg.alert('Error', 'Mohon no pasien untuk antrian sebelumnya diisi.');
                this.updateLabel();
                return;
            }
        }
        if (Locstor.get('AntrianKasir') != null) {
            Ext.Msg.alert('Failure', 'Antrian Kasir harus diselesaikan dulu.');
            this.updateLabel();
            return;
        }

        Ext.Ajax.request({
            url: 'AishaAntrian/PanggilCounter/',
            method: 'POST',
            scope: this,
            params: {
                mode: 'AntrianFO',
                counter: jun.Counter,
                bagian: 'counter'
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success) {
                    if (response.msg != "Tidak ada antrian.") {
                        var person = {
                            nomor: response.msg[0],
                            bagian: response.msg[1],
                            counter: response.msg[2],
                            no_base: response.msg[3],
                            id_antrian: response.msg[4],
                            customer_id: response.msg[5],
                            nama_customer: response.msg[6]
                        };
                        Locstor.set('AntrianFO', person);
                        this.refreshStoreAntrian();
                    } else {
                        Locstor.remove('AntrianFO');
                        Ext.Msg.alert('Information', response.msg);
                    }
                } else {
                    Ext.Msg.alert('Information', response.msg);
                }
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    updateLabel: function () {
        var storedNames = Locstor.get('AntrianFO');
        if (storedNames !== null) {
            this.btnPanggil.setDisabled(true);
            this.btnPending.setDisabled(false);
            this.btnUlang.setDisabled(false);
            this.btnSpesial.setDisabled(false);
            this.lblAntrian.setText(storedNames.nomor);
            this.lblBagian.setText(storedNames.no_base + '<br/>' + storedNames.nama_customer, false);
            this.btnEnd.setDisabled(storedNames.no_base == '');
            this.btnKonsul.setDisabled(storedNames.no_base == '');
            this.btnOther.setDisabled(false);
            this.btnRelease.setDisabled(false);
            this.btnReqKonsul.setDisabled(storedNames.no_base == '');
        } else {
            this.btnPanggil.setDisabled(false);
            this.btnPending.setDisabled(true);
            this.btnUlang.setDisabled(true);
            this.btnSpesial.setDisabled(true);
            this.btnKonsul.setDisabled(true);
            this.btnEnd.setDisabled(true);
            this.btnOther.setDisabled(true);
            this.btnRelease.setDisabled(true);
            this.btnReqKonsul.setDisabled(true);
            this.lblAntrian.setText('');
            this.lblBagian.setText('TIDAK ADA ANTRIAN');
        }
        this.grplabel.doLayout();
    },
    onPendingClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianFO');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/Update/',
            method: 'POST',
            scope: this,
            params: {
                id_antrian: storedNames.id_antrian,
                mode: 'pending',
                bagian: 'counter'
            },
            success: function (f, a) {
                jun.reloadAllStoreCounter();
                Locstor.remove('AntrianFO');
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onSpesialClick: function (t, e) {
        t.setDisabled(true);
        Ext.Msg.show({
            title: 'Customers',
            msg: 'Pilih Customer, bisa dicari menggunakan no atau nama.',
            buttons: Ext.MessageBox.OKCANCEL,
            multiline: false,
            inputField: new Ext.form.ComboBox({
                ref: '../customer',
                // id: 'cmbcustomercounterfo',
                triggerAction: 'query',
                lazyRender: true,
                mode: 'remote',
                forceSelection: true,
                autoSelect: false,
                store: jun.rztCustomersSalesCounterCmp,
                valueField: 'no_customer',
                displayField: 'nama_customer',
                hideTrigger: true,
                minChars: 3,
                matchFieldWidth: !1,
                pageSize: 20,
                itemSelector: 'div.search-item',
                tpl: new Ext.XTemplate(
                    '<tpl for="."><div class="search-item">',
                    '<h3><span>{tgl_lahir:date("j M Y")}<br />{telp}</span>{nama_customer} ({no_customer})</h3>',
                    '{alamat}',
                    '</div></tpl>'
                ),
                allowBlank: false,
                listWidth: 450,
                lastQuery: ""
            }),
            fn: function (btn, txt) {
                if (btn != 'ok') {
                    Ext.getCmp('docs-jun.AntrianFOGridWin').updateLabel();
                    return;
                }
                txt = txt.toUpperCase();
                if (txt == '') {
                    Ext.MessageBox.show({
                        title: 'Info',
                        msg: 'Masukkan No Pasien atau No Base. Misal : 1JOG01',
                        buttons: Ext.MessageBox.OK,
                        icon: Ext.MessageBox.INFO
                    });
                    Ext.getCmp('docs-jun.AntrianFOGridWin').updateLabel();
                    return;
                }
                // var panelAntrian = Ext.getCmp('docs-jun.PasienHistoryWin');
                var hightPanel = Ext.getCmp('docs-jun.AntrianCounterWin').getHeight();
                Ext.getCmp('docs-jun.PasienHistoryWin').no_customer.setValue(txt);
                // Ext.getCmp('docs-jun.PasienHistoryWin').load({
                //     url: 'customers/SimpleHistory/nobase/' + txt,
                //     scripts: true,
                //     params: {
                //         nobase: txt,
                //         height: hightPanel - 100
                //     }
                // });
                // var local = localStorage.getItem("AntrianFO");
                var storedNames = Locstor.get('AntrianFO');
                if (storedNames == null) {
                    Ext.Msg.alert('Failure', 'Local storage failed');
                    Ext.getCmp('docs-jun.AntrianFOGridWin').updateLabel();
                    return;
                }
                storedNames.no_base = txt;
                var at = jun.rztCustomersSalesCounterCmp.findExact('no_customer', txt);
                var record = jun.rztCustomersSalesCounterCmp.getAt(at);
                Ext.getCmp('docs-SalestransCounterWin').customer.setValue(record.data.customer_id);
                var person = {
                    nomor: storedNames.nomor,
                    bagian: storedNames.bagian,
                    counter: storedNames.counter,
                    no_base: txt,
                    id_antrian: storedNames.id_antrian,
                    customer_id: record.data.customer_id,
                    nama_customer: record.data.nama_customer
                };
                Locstor.set('AntrianFO', person);
                Ext.Ajax.request({
                    url: 'AishaAntrian/Update/',
                    method: 'POST',
                    scope: this,
                    params: {
                        mode: 'Nobase',
                        id_antrian: storedNames.id_antrian,
                        no_base: txt,
                        customer_id: person.customer_id,
                        nama_customer: person.nama_customer
                    },
                    success: function (f, a) {
                        var response = Ext.decode(f.responseText);
                        var storedNames = Locstor.get('AntrianFO');
                        if (storedNames == null) {
                            Ext.Msg.alert('Failure', 'Local storage failed');
                            Ext.getCmp('docs-jun.AntrianFOGridWin').updateLabel();
                            return;
                        }
                        var person = {
                            nomor: storedNames.nomor,
                            bagian: storedNames.bagian,
                            counter: storedNames.counter,
                            no_base: storedNames.no_base,
                            id_antrian: storedNames.id_antrian,
                            customer_id: storedNames.customer_id,
                            nama_customer: storedNames.nama_customer
                        };
                        Locstor.set('AntrianFO', person);
                        Ext.getCmp('docs-jun.AntrianFOGridWin').updateLabel();
                    },
                    failure: function (f, a) {
                        Ext.getCmp('docs-jun.AntrianFOGridWin').updateLabel();
                        switch (a.failureType) {
                            case Ext.form.Action.CLIENT_INVALID:
                                Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                                break;
                            case Ext.form.Action.CONNECT_FAILURE:
                                Ext.Msg.alert('Failure', 'Ajax communication failed');
                                break;
                            case Ext.form.Action.SERVER_INVALID:
                                Ext.Msg.alert('Failure', a.result.msg);
                        }
                    }
                });
            }
        });
    },
    onafterrender: function () {
        this.updateLabel();
    },
    onWinClose: function () {
        jun.runner.stop(this.taskAntrian);
    },
    refreshStoreAntrian: function () {
        jun.rztAntrianFO.load();
        jun.rztAntrianFOPending.load();
    }
});
jun.AntrianKasirGridWin = Ext.extend(Ext.Panel, {
    title: "AntrianKasir",
    id: 'docs-jun.AntrianKasirGridWin',
    anchor: '100%',
    layout: 'hbox',
    layoutConfig: {
        // padding: 5,
        align: 'stretch'
    },
    autoScroll: true,
    initComponent: function () {
        jun.rztAntrianKasir.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.counter = jun.Counter;
                    b.params.mode = "grid";
                }
            }
        });
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'PANGGIL',
                            width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnPanggil'
                        },
                        {
                            xtype: 'button',
                            text: 'ULANGI',
                            width: 75,
                            disabled: true,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnUlang'
                        },
                        {
                            xtype: 'button',
                            text: 'PENDING',
                            width: 75,
                            disabled: true,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnPending'
                        },
                        {
                            xtype: 'button',
                            text: 'KONSUL',
                            width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnKonsul',
                            hidden: jun.Counter == 'E'
                        },
                        {
                            xtype: 'button',
                            text: 'SELESAI',
                            id: 'btnselesaicounterantriankasir',
                            width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnEnd'
                        }
                        //,
                        // {
                        //     xtype: 'button',
                        //     text: 'S P E S I A L',
                        //     width: 75,
                        //     style: {
                        //         padding: '1px',
                        //         font: 'normal 28px courier'
                        //     },
                        //     ref: '../../btnSpesial'
                        // }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    ref: '../grplabel',
                    defaults: {
                        scale: 'large'
                    },
                    width: 350,
                    items: [
                        {
                            xtype: 'label',
                            style: {
                                padding: '1px',
                                font: 'bold 35px sans-serif'
                            },
                            ref: '../../lblAntrian'
                        },
                        {
                            xtype: 'label',
                            style: {
                                font: '35px courier'
                            },
                            text: '{'
                        },
                        {
                            xtype: 'label',
                            style: {
                                padding: '1px',
                                font: 'bold 12px sans-serif'
                            },
                            ref: '../../lblBagian'
                        },
                        {
                            xtype: 'label',
                            style: {
                                font: '35px courier'
                            },
                            text: '}'
                        }
                    ]
                }
            ]
        };
        this.items = [
            new jun.AntrianKasirGrid({
                frameHeader: !1,
                // margins: '0 5 0 0',
                header: !1,
                flex: 5
            }),
            new jun.AntrianKasirPendingGrid({
                frameHeader: !1,
                header: !1,
                flex: 5
            })
        ];
        jun.AntrianKasirGridWin.superclass.initComponent.call(this);
        this.btnPanggil.on('click', this.onPanggilClick, this);
        this.btnUlang.on('click', this.onUlangClick, this);
        this.btnPending.on('click', this.onPendingClick, this);
        this.btnKonsul.on('click', this.onKonsulClick, this);
        this.btnEnd.on('click', this.onEndClick, this);
        this.on("afterrender", this.onafterrender, this);
    },
    onEndClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianKasir');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                mode: 'selesai',
                bagian: 'selesai',
                id_antrian: storedNames.id_antrian,
                no_base: storedNames.no_base
            },
            success: function (f, a) {
                Locstor.remove('AntrianKasir');
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onKonsulClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianKasir');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/update/',
            method: 'POST',
            scope: this,
            params: {
                mode: 'back',
                bagian: 'kasir',
                id_antrian: storedNames.id_antrian,
                no_base: storedNames.no_base
            },
            success: function (f, a) {
                Locstor.remove('AntrianKasir');
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onUlangClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianKasir');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/UlangiCounter/',
            method: 'POST',
            scope: this,
            params: {
                nomor: storedNames.nomor,
                bagian: 'kasir',
                counter: jun.Counter,
                no_base: storedNames.no_base,
                id_antrian: storedNames.id_antrian,
                customer_id: storedNames.customer_id,
                nama_customer: storedNames.nama_customer
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (!response.success) {
                    Ext.Msg.alert('Information', response.msg);
                }
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onPanggilClick: function () {
        this.btnPanggil.setDisabled(true);
        if (Locstor.get('AntrianFO') != null) {
            Ext.Msg.alert('Failure', 'Antrian FO harus diselesaikan dulu.');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/PanggilCounter/',
            method: 'POST',
            scope: this,
            params: {
                mode: 'AntrianKasir',
                counter: jun.Counter,
                bagian: 'kasir'
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success) {
                    if (response.msg != "Tidak ada antrian.") {
                        var person = {
                            nomor: response.msg[0],
                            bagian: response.msg[1],
                            counter: response.msg[2],
                            no_base: response.msg[3],
                            id_antrian: response.msg[4],
                            customer_id: response.msg[5],
                            nama_customer: response.msg[6]
                        };
                        Locstor.set('AntrianKasir', person);
                        this.updateLabel();
                        this.refreshStoreAntrian();
                        var tabpanel = this.findParentByType('tabpanel');
                        var tab = tabpanel.getComponent('docs-jun.SalestransCounterPanelWin');
                        if (tab) {
                            tabpanel.setActiveTab(tab);
                            Ext.getCmp('docs-jun.KonsulCounterGrid').selectSuspendItems(response.msg[4]);
                            jun.rztCustomersSalesCounterCmp.load({
                                params: {
                                    customer_id: response.msg[5]
                                },
                                callback: function () {
                                    Ext.getCmp('docs-SalestransCounterWin').customer.setValue(response.msg[5]);
                                },
                                scope: this
                            });
                        }
                        return;
                    } else {
                        Locstor.remove("AntrianKasir");
                        Ext.Msg.alert('Information', response.msg);
                    }
                } else {
                    Ext.Msg.alert('Information', response.msg);
                }
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    updateLabel: function () {
        var storedNames = Locstor.get('AntrianKasir');
        if (storedNames !== null) {
            this.btnPanggil.setDisabled(true);
            this.btnPending.setDisabled(false);
            this.btnUlang.setDisabled(false);
            this.btnEnd.setDisabled(false);
            this.btnKonsul.setDisabled(false);
            // this.btnSpesial.setDisabled(false);
            this.lblAntrian.setText(storedNames.nomor);
            this.lblBagian.setText(storedNames.no_base + '<br/>' + storedNames.nama_customer, false);
        } else {
            this.btnPanggil.setDisabled(false);
            this.btnPending.setDisabled(true);
            this.btnUlang.setDisabled(true);
            this.btnEnd.setDisabled(true);
            this.btnKonsul.setDisabled(true);
            this.lblAntrian.setText('');
            this.lblBagian.setText('TIDAK ADA ANTRIAN');
        }
        this.grplabel.doLayout();
    },
    onPendingClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianKasir');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/Update/',
            method: 'POST',
            scope: this,
            params: {
                id_antrian: storedNames.id_antrian,
                mode: 'pending',
                bagian: 'kasir'
            },
            success: function (f, a) {
                this.refreshStoreAntrian();
                Locstor.remove("AntrianKasir");
                this.updateLabel();
                var tabpanel = this.findParentByType('tabpanel');
                var tab = tabpanel.getComponent('docs-jun.SalestransCounterPanelWin');
                var kasir = tabpanel.getComponent('docs-jun.AntrianCounterWin');
                if (tab) {
                    tabpanel.setActiveTab(tab);
                    Ext.getCmp('docs-jun.KonsulCounterGrid').resetSuspendList();
                    tabpanel.setActiveTab(kasir);
                }
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onafterrender: function () {
        // jun.runner.start(this.taskAntrian);
        this.updateLabel();
    },
    onWinClose: function () {
        jun.runner.stop(this.taskAntrian);
    },
    refreshStoreAntrian: function () {
        jun.rztAntrianKasir.load();
        jun.rztAntrianKasirPending.load();
    }
});
jun.AntrianDokterGridWin = Ext.extend(Ext.Panel, {
    title: "Antrian Dokter",
    id: 'docs-jun.AntrianDokterGridWin',
    anchor: '100%',
    layout: 'hbox',
    layoutConfig: {
        // padding: 2,
        align: 'stretch'
    },
    defaults: {
        frame: false
    },
    autoScroll: true,
    initComponent: function () {
        jun.rztAntrianDokter.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    b.params.counter = jun.Counter;
                    b.params.mode = "grid";
                }
            }
        });
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'PANGGIL',
                            width: 75,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnPanggil'
                        },
                        {
                            xtype: 'button',
                            text: 'ULANGI',
                            width: 75,
                            disabled: true,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnUlang'
                        },
                        {
                            xtype: 'button',
                            text: 'PENDING',
                            width: 75,
                            disabled: true,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnPending'
                        },
                        {
                            xtype: 'button',
                            text: 'SELESAI',
                            width: 75,
                            disabled: true,
                            style: {
                                padding: '1px',
                                font: 'normal 28px courier'
                            },
                            ref: '../../btnSpesial'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 10,
                    ref: '../grplabel',
                    defaults: {
                        scale: 'large'
                    },
                    width: 350,
                    items: [
                        {
                            xtype: 'label',
                            style: {
                                padding: '1px',
                                font: 'bold 35px sans-serif'
                            },
                            ref: '../../lblAntrian'
                        },
                        {
                            xtype: 'label',
                            style: {
                                font: '35px courier'
                            },
                            text: '{'
                        },
                        {
                            xtype: 'label',
                            style: {
                                padding: '1px',
                                font: 'bold 12px sans-serif'
                            },
                            ref: '../../lblBagian'
                        },
                        {
                            xtype: 'label',
                            style: {
                                font: '35px courier'
                            },
                            text: '}'
                        }
                    ]
                }
            ]
        };
        this.items = [
            new jun.AntrianDokterGrid({
                frameHeader: !1,
                margins: '0 1 0 0',
                header: !1,
                flex: 5
            }),
            new jun.AntrianDokterPendingGrid({
                frameHeader: !1,
                header: !1,
                flex: 5
            })
        ];
        jun.AntrianDokterGridWin.superclass.initComponent.call(this);
        this.btnPanggil.on('click', this.onPanggilClick, this);
        this.btnUlang.on('click', this.onUlangClick, this);
        this.btnPending.on('click', this.onPendingClick, this);
        this.btnSpesial.on('click', this.onSpesialClick, this);
        this.on("afterrender", this.onafterrender, this);
    },
    onUlangClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianDokter');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/UlangiCounter/',
            method: 'POST',
            scope: this,
            params: {
                nomor: storedNames.nomor,
                bagian: 'medis',
                counter: jun.Counter,
                no_base: storedNames.no_base,
                id_antrian: storedNames.id_antrian,
                customer_id: storedNames.customer_id,
                nama_customer: storedNames.nama_customer
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (!response.success) {
                    Ext.Msg.alert('Information', response.msg);
                }
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onPanggilClick: function () {
        this.btnPanggil.setDisabled(true);
        var g = Ext.getCmp('docs-jun.AntrianDokterGrid');
        var record = g.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Antrian");
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/PanggilCounter/',
            method: 'POST',
            scope: this,
            params: {
                id_antrian: record.data.id_antrian,
                mode: 'AntrianDokter',
                counter: jun.Counter,
                bagian: 'medis'
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                if (response.success) {
                    if (response.msg != "Tidak ada antrian.") {
                        var person = {
                            nomor: response.msg[0],
                            bagian: response.msg[1],
                            counter: response.msg[2],
                            no_base: response.msg[3],
                            id_antrian: response.msg[4],
                            customer_id: response.msg[5],
                            nama_customer: response.msg[6]
                        };
                        Locstor.set('AntrianDokter', person);
                        this.refreshStoreAntrian();
                        var panel = Ext.getCmp('form-dokter-gridHistory');
                        panel.load({
                            url: 'customers/SimpleHistory/nobase/' + person.no_base,
                            scripts: true,
                            params: {
                                nobase: person.no_base,
                                customer_id: person.customer_id,
                                height: panel.getHeight() - 100
                            }
                        });
                        panel.expand();
                        var panel1 = Ext.getCmp('form-dokter-gridHistory');
                        panel1.load({
                            url: 'customers/SimpleHistory/nobase/' + person.no_base,
                            scripts: true,
                            params: {
                                nobase: person.no_base,
                                customer_id: person.customer_id,
                                height: panel1.getHeight() - 100
                            }
                        });
                        panel1.expand();
                    } else {
                        Locstor.remove("AntrianDokter");
                        Ext.Msg.alert('Information', response.msg);
                    }
                } else {
                    Ext.Msg.alert('Information', response.msg);
                }
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
        this.loadDiagnosaKonsul(record.data.id_antrian);
    },
    loadDiagnosaKonsul: function (id_antrian) {
        jun.rztDiagnosa.load({
            params: {
                id_antrian: id_antrian
            },
            callback: function (r, o, s) {
                jun.rztSalestransQtyDetails.refreshData(jun.rztSalestransQtyDetails);
                jun.rztDiagnosa.refreshData(jun.rztDiagnosa);
            },
            scope: this,
            add: false
        });
        jun.rztSalestransQtyDetails.load({
            params: {
                id_antrian: id_antrian
            },
            callback: function (r, o, s) {
                jun.rztSalestransQtyDetails.refreshData(jun.rztSalestransQtyDetails);
                jun.rztDiagnosa.refreshData(jun.rztDiagnosa);
            },
            scope: this,
            add: false
        });
    },
    updateLabel: function () {
        var storedNames = Locstor.get('AntrianDokter');
        if (storedNames !== null) {
            this.btnPanggil.setDisabled(true);
            this.btnPending.setDisabled(false);
            this.btnUlang.setDisabled(false);
            this.btnSpesial.setDisabled(false);
            this.lblAntrian.setText(storedNames.nomor);
            this.lblBagian.setText(storedNames.no_base + '<br/>' + storedNames.nama_customer, false);
            Ext.Ajax.request({
                url: 'Customers/ByNoPasien/',
                method: 'POST',
                scope: this,
                params: {
                    no_customer: storedNames.no_base
                },
                success: function (f, a) {
                    var response = Ext.decode(f.responseText);
                    if (response.total != '0') {
                        var tpl = new Ext.Template(
                            '<table cellspacing="0"><tbody>',
                            '<tr><td>Nama Pasien:</td><td>&nbsp{nama_customer}</td><td rowspan="3">Alamat: </td><td rowspan="3"><p>&nbsp{alamat}</p></td></tr>',
                            '<tr><td>No Pasien:</td><td>&nbsp{no_customer}</td></tr>',
                            '<tr><td>Telp:</td><td>&nbsp{telp}</td></tr>',
                            '<tr><td>Umur:</td><td>&nbsp{age}</td><td rowspan="3">Kerja:</td><td rowspan="3"><p>&nbsp{kerja}</p></td></tr>',
                            '</tbody></table>'
                        );
                        var p = Ext.getCmp('panel_info_pasien_id');
                        tpl.overwrite(p.body, response.data);
                        p.body.highlight('#c3daf9', {block: true});
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                        case Ext.form.Action.CLIENT_INVALID:
                            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                            break;
                        case Ext.form.Action.CONNECT_FAILURE:
                            Ext.Msg.alert('Failure', 'Ajax communication failed');
                            break;
                        case Ext.form.Action.SERVER_INVALID:
                            Ext.Msg.alert('Failure', a.result.msg);
                    }
                }
            });
        } else {
            this.btnPanggil.setDisabled(false);
            this.btnPending.setDisabled(true);
            this.btnUlang.setDisabled(true);
            this.btnSpesial.setDisabled(true);
            this.lblAntrian.setText('');
            this.lblBagian.setText('TIDAK ADA ANTRIAN');
        }
        this.grplabel.doLayout();
    },
    onSpesialClick: function (t, e) {
        t.setDisabled(true);
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menandai antrian selesai?', this.spesialRecYes, this);
    },
    spesialRecYes: function (t, e) {
        if (t == 'no') {
            return;
        }
        var storedNames = Locstor.get('AntrianDokter');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/Update/',
            method: 'POST',
            scope: this,
            params: {
                id_antrian: storedNames.id_antrian,
                mode: 'selesai',
                bagian: 'medis'
            },
            success: function (f, a) {
                // var response = Ext.decode(f.responseText);
                // Ext.Msg.alert('Informasi', response.msg);
                Locstor.remove('AntrianDokter');
                jun.rztSalestransQtyDetails.removeAll();
                jun.rztSalestransQtyDetails.refreshData(jun.rztSalestransQtyDetails);
                jun.rztDiagnosa.removeAll();
                jun.rztDiagnosa.refreshData(jun.rztDiagnosa);
                this.refreshStoreAntrian();
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onPendingClick: function (t, e) {
        t.setDisabled(true);
        var storedNames = Locstor.get('AntrianDokter');
        if (storedNames == null) {
            Ext.Msg.alert('Failure', 'Local storage failed');
            this.updateLabel();
            return;
        }
        Ext.Ajax.request({
            url: 'AishaAntrian/Update/',
            method: 'POST',
            scope: this,
            params: {
                id_antrian: storedNames.id_antrian,
                mode: 'pending',
                bagian: 'medis'
            },
            success: function (f, a) {
                this.refreshStoreAntrian();
                Locstor.remove("AntrianDokter");
                this.updateLabel();
            },
            failure: function (f, a) {
                this.updateLabel();
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    },
    onafterrender: function () {
        // jun.runner.start(this.taskAntrian);
        this.updateLabel();
        var storedNames = Locstor.get('AntrianDokter');
        if (storedNames != null) {
            this.loadDiagnosaKonsul(storedNames.id_antrian);
        }
    },
    onWinClose: function () {
        jun.runner.stop(this.taskAntrian);
    },
    refreshStoreAntrian: function () {
        jun.rztAntrianDokter.load();
        jun.rztAntrianDokterPending.load();
    }
});
jun.CustomerDokterGridWin = Ext.extend(Ext.Panel, {
    title: "Pasien",
    id: 'docs-jun.CustomerDokterGridWin',
    anchor: '100%',
    layout: 'hbox',
    layoutConfig: {
        // padding: 2,
        align: 'stretch'
    },
    defaults: {
        frame: false
    },
    autoScroll: true,
    initComponent: function () {
        this.items = [
            new jun.CustomersGrid({
                frameHeader: !1,
//                margins: '0 1 0 0',
                header: !1,
                flex: 5,
                antrianform: true                
            })
        ];
        jun.CustomerDokterGridWin.superclass.initComponent.call(this);
//        this.on("afterrender", this.onafterrender, this);
    }    
});
jun.AntrianDokterCounterGridWin = Ext.extend(Ext.Panel, {
    title: "AntrianDokter",
    id: 'docs-jun.AntrianDokterCounterGridWin',
    anchor: '100%',
    layout: 'hbox',
    layoutConfig: {
        // padding: 2,
        align: 'stretch'
    },
    defaults: {
        frame: false
    },
    autoScroll: true,
    initComponent: function () {
        this.items = [
            new jun.AntrianDokterGrid({
                frameHeader: !1,
                margins: '0 1 0 0',
                header: !1,
                flex: 5
            }),
            new jun.AntrianDokterPendingGrid({
                frameHeader: !1,
                header: !1,
                flex: 5
            })
        ];
        jun.AntrianDokterCounterGridWin.superclass.initComponent.call(this);
    }
});
jun.AntrianCounterWin = Ext.extend(Ext.Panel, {
    title: "Antrian",
    id: 'docs-jun.AntrianCounterWin',
    layout: 'border',
    layoutConfig: {
        // padding: 5,
        align: 'stretch'
    },
    initComponent: function () {
        this.items = [
            {
                region: 'center',
                anchor: '100%',
                layout: 'hbox',
                layoutConfig: {
                    padding: '0 0 1 0',
                    align: 'stretch'
                },
                items: [
                    new jun.AntrianFOGridWin({
                        title: 'Antrian Front Office',
                        margins: '0 1 0 0',
                        flex: 6
                    }),
                    new jun.AntrianKasirGridWin({
                        title: 'Antrian Kasir',
                        flex: 5
                    })
                ]
            },
            new jun.AntrianMonitorGrid({
                title: "Monitoring Antrian",
                region: 'south',
                ref: '../AntrianMonitorGrid',
                height: 300
            })
        ];
        jun.AntrianCounterWin.superclass.initComponent.call(this);
        // this.on("close", this.onWinClose, this);
        // this.on("afterrender", this.onafterrender, this);
    }
});
// jun.AntrianFOOptionWin = Ext.extend(Ext.Window, {
//     title: 'PILIH TUJUAN',
//     modez: 1,
//     width: 400,
//     height: 300,
//     layout: 'form',
//     modal: true,
//     padding: 5,
//     closeForm: false,
//     initComponent: function () {
//         this.items = [
//             ];
//         jun.AntrianFOOptionWin.superclass.initComponent.call(this);
//     }
// });
jun.AntrianFOCount = 0;
jun.AntrianKasirCount = 0;

