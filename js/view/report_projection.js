//REPORT TRANSACTION OVERRIDE
jun.ReportTransactionOverride= Ext.extend(Ext.Window, {
    title: "Report Sales Override",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 218,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportTransactionOverride",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [

/*                    {
                        xtype: 'combo',
                        fieldLabel: 'Show By',
                        hiddenName: 'showby',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['Outlet', 'Outlet'],
                                ['GroupProduct', 'GroupProduct'],
                                ['Product', 'Product'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../cmbShowBy',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: '',
                        store: jun.rztOutlet,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kodeoutlet} - {namaoutlet}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'outlet',
                        valueField: 'kodeoutlet',
                        ref:'../cmboutlet',
                        displayField: 'kodeoutlet',
                        emptyText: "All Outlet",
                        anchor: '100%',

                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: '',
                        store: jun.rztGroupproduk,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kode} - {nama}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'groupproduk',
                        valueField: 'kode',
                        ref:'../cmbgroupproduk',
                        displayField: 'kode',
                        emptyText: "All Group Products",
                        anchor: '100%',

                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: '',
                        store: jun.rztProduk,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kodeproduk} - {nama}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'produk',
                        valueField: 'kodeproduk',
                        ref:'../cmbproduk',
                        displayField: 'kodeproduk',
                        emptyText: "All Products",
                        anchor: '100%',

                    },*/
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    },
                    {
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showgroup",
                        ref: '../showdata',
                        defaults: {xtype: "radio",name: "showdata"},
                        items: [
                            {
                                boxLabel: "Header",
                                inputValue: "H"
                            },
                            {
                                boxLabel: "Detail",
                                inputValue: "D"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportTransaction.superclass.initComponent.call(this);
        //this.on("activate", this.onActivate, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
        //this.cmbShowBy.on('Select', this.ShowBy, this);
    },
    /*onActivate: function(){
        this.cmboutlet.setVisible(false);
        this.cmbgroupproduk.setVisible(false);
        this.cmbproduk.setVisible(false);
    },*/
   /* ShowBy: function(){

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == 'Outlet')
        {
            this.cmboutlet.setVisible(true);
            this.cmbgroupproduk.setVisible(false);
            this.cmbproduk.setVisible(false);

        }
        else if(selectedz == 'GroupProduct')
        {
            this.cmboutlet.setVisible(false);
            this.cmbgroupproduk.setVisible(true);
            this.cmbproduk.setVisible(false);
        }
        else if(selectedz == 'Product')
        {
            this.cmboutlet.setVisible(false);
            this.cmbgroupproduk.setVisible(false);
            this.cmbproduk.setVisible(true);
        }
    },*/

    onbtnPdfclick: function () {

        /*var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }*/

        Ext.getCmp("form-ReportTransactionOverride").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTransactionOverride").getForm().url = "Report/TransactionOverride";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportTransactionOverride').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        /*var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }*/

        var url = "Report/TransactionOverride";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportTransactionOverride").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportTransactionOverride").getForm().url = url;
            var form = Ext.getCmp('form-ReportTransactionOverride').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportTransactionOverride").getForm().getValues());
        }
    },
    Chart: function () {

        /*var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }*/

        Ext.getCmp("form-ReportTransactionOverride").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTransactionOverride").getForm().url = "Chart/TransactionOverride";
        var form = Ext.getCmp('form-ReportTransactionOverride').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
})

//REPORT TRANSACTION
jun.ReportTransaction= Ext.extend(Ext.Window, {
    title: "Report Sales",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 218,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztOutlet.getTotalCount() === 0) {
            jun.rztOutlet.load();
        }
        if (jun.rztGroupproduk.getTotalCount() === 0) {
            jun.rztGroupproduk.load();
        }
        if (jun.rztProduk.getTotalCount() === 0) {
            jun.rztProduk.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportTransaction",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [

                    {
                        xtype: 'combo',
                        fieldLabel: 'Show By',
                        hiddenName: 'showby',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['Outlet', 'Outlet'],
                                ['GroupProduct', 'GroupProduct'],
                                ['Product', 'Product'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../cmbShowBy',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: '',
                        store: jun.rztOutlet,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kodeoutlet} - {namaoutlet}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'outlet',
                        valueField: 'kodeoutlet',
                        ref:'../cmboutlet',
                        displayField: 'kodeoutlet',
                        emptyText: "All Outlet",
                        anchor: '100%',

                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: '',
                        store: jun.rztGroupproduk,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kode} - {nama}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'groupproduk',
                        valueField: 'kode',
                        ref:'../cmbgroupproduk',
                        displayField: 'kode',
                        emptyText: "All Group Products",
                        anchor: '100%',

                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: '',
                        store: jun.rztProduk,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{kodeproduk} - {nama}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'produk',
                        valueField: 'kodeproduk',
                        ref:'../cmbproduk',
                        displayField: 'kodeproduk',
                        emptyText: "All Products",
                        anchor: '100%',

                    },
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    },
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportTransaction.superclass.initComponent.call(this);
        this.on("activate", this.onActivate, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
        this.cmbShowBy.on('Select', this.ShowBy, this);
    },
    onActivate: function(){
        this.cmboutlet.setVisible(false);
        this.cmbgroupproduk.setVisible(false);
        this.cmbproduk.setVisible(false);
    },
    ShowBy: function(){

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == 'Outlet')
        {
            this.cmboutlet.setVisible(true);
            this.cmbgroupproduk.setVisible(false);
            this.cmbproduk.setVisible(false);

        }
        else if(selectedz == 'GroupProduct')
        {
            this.cmboutlet.setVisible(false);
            this.cmbgroupproduk.setVisible(true);
            this.cmbproduk.setVisible(false);
        }
        else if(selectedz == 'Product')
        {
            this.cmboutlet.setVisible(false);
            this.cmbgroupproduk.setVisible(false);
            this.cmbproduk.setVisible(true);
        }
    },

    onbtnPdfclick: function () {

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        Ext.getCmp("form-ReportTransaction").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTransaction").getForm().url = "Report/Transaction";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportTransaction').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        var url = "Report/Transaction";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportTransaction").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportTransaction").getForm().url = url;
            var form = Ext.getCmp('form-ReportTransaction').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportTransaction").getForm().getValues());
        }
    },
    Chart: function () {

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        Ext.getCmp("form-ReportTransaction").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportTransaction").getForm().url = "Chart/Transaction";
        var form = Ext.getCmp('form-ReportTransaction').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
})

//REPORT BUDGET PLANS
jun.ReportBudget = Ext.extend(Ext.Window, {
    title: "Report Budget Plans",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 214,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBudget",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../account_id',
                        displayField: 'account_code',
                        emptyText: "All Account",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    },
                    {
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showgroup",
                        ref: '../showdata',
                        defaults: {xtype: "radio",name: "showdata"},
                        items: [
                            {
                                boxLabel: "Header",
                                inputValue: "H"
                            },
                            {
                                boxLabel: "By Month",
                                inputValue: "D"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportBudget.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
    },
    onbtnPdfclick: function () {
        var url = "";
        var selectedz = this.showdata.getValue();
        if(selectedz && selectedz.inputValue == 'H')
        {
            url = "Report/Budget";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/BudgetPlanPerMonth";
        }

        Ext.getCmp("form-ReportBudget").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBudget").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBudget').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "";
        var selectedz = this.showdata.getValue();
        if(selectedz && selectedz.inputValue == 'H')
        {
            url = "Report/Budget";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/BudgetPlanPerMonth";
        }

        //var url = "Report/Budget";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportBudget").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportBudget").getForm().url = url;
            var form = Ext.getCmp('form-ReportBudget').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportBudget").getForm().getValues());
        }
    },
    Chart: function () {
        Ext.getCmp("form-ReportBudget").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBudget").getForm().url = "Chart/Budget";
        var form = Ext.getCmp('form-ReportBudget').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

//REPORT BUDGET REALIZATION
jun.ReportRealization = Ext.extend(Ext.Window, {
    title: "Report Budget Realizations",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 214,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportRealization",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../account_id',
                        displayField: 'account_code',
                        emptyText: "All Account",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    },
                    {
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showgroup",
                        ref: '../showdata',
                        defaults: {xtype: "radio",name: "showdata"},
                        items: [
                            {
                                boxLabel: "Header",
                                inputValue: "H"
                            },
                            {
                                boxLabel: "By Month",
                                inputValue: "D"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportRealization.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
    },
    onbtnPdfclick: function () {
        var url = "";
        var selectedz = this.showdata.getValue();
        if(selectedz && selectedz.inputValue == 'H')
        {
            url = "Report/Realization";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/RealizationPerMonth";
        }


        Ext.getCmp("form-ReportRealization").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRealization").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportRealization').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        //var url = "Report/Realization";
        var url = "";
        var selectedz = this.showdata.getValue();
        if(selectedz && selectedz.inputValue == 'H')
        {
            url = "Report/Realization";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/RealizationPerMonth";
        }
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportRealization").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportRealization").getForm().url = url;
            var form = Ext.getCmp('form-ReportRealization').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportRealization").getForm().getValues());
        }
    },
    Chart: function () {
        Ext.getCmp("form-ReportRealization").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportRealization").getForm().url = "Chart/Realization";
        var form = Ext.getCmp('form-ReportRealization').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }

});

//REPORT BUDGET ANALYSIS
jun.ReportAnalysis = Ext.extend(Ext.Window, {
    title: "Report Budget Analysis",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 212,//191 + 30 + 50,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportAnalysis",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Show By',
                        name: 'showby',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['Y', 'Year'],
                                ['MY', 'Month & Year'],
                                ['D', 'Date'],
                                ['A', 'Account'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../cmbShowBy',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        //fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../cmbAccount',
                        displayField: 'account_code',
                        emptyText: "All Account...",

                    },
                    {
                        xtype: 'combo',
                        //fieldLabel: 'Type',
                        name: 'month',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['01', 'January'],
                                ['02', 'February'],
                                ['03', 'March'],
                                ['04', 'April'],
                                ['05', 'May'],
                                ['06', 'June'],
                                ['07', 'July'],
                                ['08', 'August'],
                                ['09', 'September'],
                                ['10', 'Okteber'],
                                ['11', 'November'],
                                ['12', 'December'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'value',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Month...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../cmbMonth'
                    },
                    {
                        xtype: 'combo',
                        //fieldLabel: 'Type',
                        name: 'year',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['2018', '2018'],
                                ['2019', '2019'],
                                ['2020', '2020'],
                                ['2021', '2021'],
                                ['2022', '2022'],
                                ['2023', '2023'],
                                ['2024', '2024'],
                                ['2025', '2025'],
                                ['2026', '2026'],
                                ['2027', '2027'],
                                ['2028', '2028'],
                                ['2029', '2029'],
                                ['2030', '2030'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Year...",
                        mode : 'local',
                        allowBlank: true,
                        width:'60',
                        ref: '../cmbYear'
                    },
                    {
                        //fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        emptyText: "From...",
                        //anchor: "100%"
                    },
                    {
                        //fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        emptyText: "To...",
                        //anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    /*{
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../account_id',
                        displayField: 'account_code',
                        emptyText: "All Account",
                        anchor: '100%'
                    },*/
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    },
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportAnalysis.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
        this.on("activate", this.onActivate, this);
        this.cmbShowBy.on('Select', this.ShowBy, this);
    },

    onActivate: function(){
        this.cmbAccount.setVisible(false);
        this.cmbMonth.setVisible(false);
        this.cmbYear.setVisible(false);
        this.tglfrom.setVisible(false);
        this.tglto.setVisible(false);
    },

    ShowBy: function(){

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == 'Y')
        {
            this.cmbYear.setVisible(true);
            this.cmbMonth.setVisible(false);
            this.cmbAccount.setVisible(false);
            this.tglfrom.setVisible(false);
            this.tglto.setVisible(false);

        }
        else if(selectedz == 'MY')
        {
            this.cmbYear.setVisible(true);
            this.cmbMonth.setVisible(true);
            this.cmbAccount.setVisible(false);
            this.tglfrom.setVisible(false);
            this.tglto.setVisible(false);
        }
        else if(selectedz == 'A')
        {
            this.cmbYear.setVisible(false);
            this.cmbMonth.setVisible(false);
            this.cmbAccount.setVisible(true);
            this.tglfrom.setVisible(false);
            this.tglto.setVisible(false);
        }
        else if(selectedz == 'D')
        {
            this.cmbYear.setVisible(false);
            this.cmbMonth.setVisible(false);
            this.cmbAccount.setVisible(false);

            this.tglfrom.setVisible(true);
            this.tglto.setVisible(true);

            this.height.setHeight(300);
        }
    },

    onbtnPdfclick: function () {

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == undefined){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        Ext.getCmp("form-ReportAnalysis").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAnalysis").getForm().url = "Report/Analysis";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportAnalysis').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        var url = "Report/Analysis";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportAnalysis").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportAnalysis").getForm().url = url;
            var form = Ext.getCmp('form-ReportAnalysis').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportAnalysis").getForm().getValues());
        }
    },
    Chart: function () {

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        Ext.getCmp("form-ReportAnalysis").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportAnalysis").getForm().url = "Chart/Analysis";
        var form = Ext.getCmp('form-ReportAnalysis').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

//REPORT INVESTMENT PLANS
jun.ReportInvestment = Ext.extend(Ext.Window, {
    title: "Report Investment Plans",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 214,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInvestment",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../account_id',
                        displayField: 'account_code',
                        emptyText: "All Account",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    },
                    {
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showgroup",
                        ref: '../showdata',
                        defaults: {xtype: "radio",name: "showdata"},
                        items: [
                            {
                                boxLabel: "Header",
                                inputValue: "H"
                            },
                            {
                                boxLabel: "By Month",
                                inputValue: "D"
                            }
                        ]
                    }


                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportInvestment.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
    },
    onbtnPdfclick: function () {
        var url = "";
        var selectedz = this.showdata.getValue();
        if(selectedz && selectedz.inputValue == 'H')
        {
            url = "Report/Investment";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/InvestmentPlanPerMonth";
        }

        Ext.getCmp("form-ReportInvestment").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInvestment").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInvestment').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "";
        var selectedz = this.showdata.getValue();
        if(selectedz && selectedz.inputValue == 'H')
        {
            url = "Report/Investment";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/InvestmentPlanPerMonth";
        }
        //var url = "Report/Investment";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportInvestment").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInvestment").getForm().url = url;
            var form = Ext.getCmp('form-ReportInvestment').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportInvestment").getForm().getValues());
        }
    },
    Chart: function () {
        Ext.getCmp("form-ReportInvestment").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInvestment").getForm().url = "Chart/Investment";
        var form = Ext.getCmp('form-ReportInvestment').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }

});

//REPORT INVESTMENT REALIZATIONS
jun.ReportInvestmentRealization = Ext.extend(Ext.Window, {
    title: "Report Investment Realizations",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 214,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInvestmentRealization",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../account_id',
                        displayField: 'account_code',
                        emptyText: "All Account",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    },
                    {
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showgroup",
                        ref: '../showdata',
                        defaults: {xtype: "radio",name: "showdata"},
                        items: [
                            {
                                boxLabel: "Header",
                                inputValue: "H"
                            },
                            {
                                boxLabel: "By Month",
                                inputValue: "D"
                            }
                        ]
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportInvestmentRealization.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
    },
    onbtnPdfclick: function () {
        var url = "";
        var selectedz = this.showdata.getValue();
        if(selectedz && selectedz.inputValue == 'H')
        {
            url = "Report/InvestmentRealization";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/InvestmentRealizationPerMonth";
        }

        Ext.getCmp("form-ReportInvestmentRealization").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInvestmentRealization").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInvestmentRealization').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "";
        var selectedz = this.showdata.getValue();
        if(selectedz && selectedz.inputValue == 'H')
        {
            url = "Report/InvestmentRealization";
            //url = "Report/bep";
        }
        else
        {
            url = "Report/InvestmentRealizationPerMonth";
        }
        //var url = "Report/InvestmentRealization";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportInvestmentRealization").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInvestmentRealization").getForm().url = url;
            var form = Ext.getCmp('form-ReportInvestmentRealization').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportInvestmentRealization").getForm().getValues());
        }
    },
    Chart: function () {
        Ext.getCmp("form-ReportInvestmentRealization").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInvestmentRealization").getForm().url = "Chart/InvestmentRealization";
        var form = Ext.getCmp('form-ReportInvestmentRealization').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }

});

//REPORT INVESTMENT ANALYSIS
jun.ReportInvestmentAnalysis = Ext.extend(Ext.Window, {
    title: "Report Investment Analysis",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 212,//191 + 30 + 50,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInvestmentAnalysis",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Show By',
                        name: 'showby',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['Y', 'Year'],
                                ['MY', 'Month & Year'],
                                ['D', 'Date'],
                                ['A', 'Account']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../cmbShowBy',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        //fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../cmbAccount',
                        displayField: 'account_code',
                        emptyText: "All Account..."
                    },
                    {
                        xtype: 'combo',
                        //fieldLabel: 'Type',
                        name: 'month',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['01', 'January'],
                                ['02', 'February'],
                                ['03', 'March'],
                                ['04', 'April'],
                                ['05', 'May'],
                                ['06', 'June'],
                                ['07', 'July'],
                                ['08', 'August'],
                                ['09', 'September'],
                                ['10', 'Okteber'],
                                ['11', 'November'],
                                ['12', 'December']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'value',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Month...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../cmbMonth'
                    },
                    {
                        xtype: 'combo',
                        //fieldLabel: 'Type',
                        name: 'year',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['2018', '2018'],
                                ['2019', '2019'],
                                ['2020', '2020'],
                                ['2021', '2021'],
                                ['2022', '2022'],
                                ['2023', '2023'],
                                ['2024', '2024'],
                                ['2025', '2025'],
                                ['2026', '2026'],
                                ['2027', '2027'],
                                ['2028', '2028'],
                                ['2029', '2029'],
                                ['2030', '2030']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Year...",
                        mode : 'local',
                        allowBlank: true,
                        width:'60',
                        ref: '../cmbYear'
                    },
                    {
                        //fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        emptyText: "From..."
                        //anchor: "100%"
                    },
                    {
                        //fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        emptyText: "To..."
                        //anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportInvestmentAnalysis.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
        this.on("activate", this.onActivate, this);
        this.cmbShowBy.on('Select', this.ShowBy, this);
    },

    onActivate: function(){
        this.cmbAccount.setVisible(false);
        this.cmbMonth.setVisible(false);
        this.cmbYear.setVisible(false);
        this.tglfrom.setVisible(false);
        this.tglto.setVisible(false);
    },

    ShowBy: function(){

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == 'Y')
        {
            this.cmbYear.setVisible(true);
            this.cmbMonth.setVisible(false);
            this.cmbAccount.setVisible(false);
            this.tglfrom.setVisible(false);
            this.tglto.setVisible(false);

        }
        else if(selectedz == 'MY')
        {
            this.cmbYear.setVisible(true);
            this.cmbMonth.setVisible(true);
            this.cmbAccount.setVisible(false);
            this.tglfrom.setVisible(false);
            this.tglto.setVisible(false);
        }
        else if(selectedz == 'A')
        {
            this.cmbYear.setVisible(false);
            this.cmbMonth.setVisible(false);
            this.cmbAccount.setVisible(true);
            this.tglfrom.setVisible(false);
            this.tglto.setVisible(false);
        }
        else if(selectedz == 'D')
        {
            this.cmbYear.setVisible(false);
            this.cmbMonth.setVisible(false);
            this.cmbAccount.setVisible(false);

            this.tglfrom.setVisible(true);
            this.tglto.setVisible(true);

            this.height.setHeight(300);
        }
    },

    onbtnPdfclick: function () {

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == undefined){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        Ext.getCmp("form-ReportInvestmentAnalysis").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInvestmentAnalysis").getForm().url = "Report/InvestmentAnalysis";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInvestmentAnalysis').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        var url = "Report/InvestmentAnalysis";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportInvestmentAnalysis").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInvestmentAnalysis").getForm().url = url;
            var form = Ext.getCmp('form-ReportInvestmentAnalysis').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportInvestmentAnalysis").getForm().getValues());
        }
    },
    Chart: function () {

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        Ext.getCmp("form-ReportInvestmentAnalysis").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInvestmentAnalysis").getForm().url = "Chart/InvestmentAnalysis";
        var form = Ext.getCmp('form-ReportInvestmentAnalysis').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

//REPORT BEP
jun.ReportBep = Ext.extend(Ext.Window, {
    title: "Report Laba Rugi",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 175,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBep",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'HPP (%)',
                        hideLabel:false,
                        //hidden:true,
                        name:'hpp',
                        id:'hpp',
                        ref:'../hpp',
                        maxLength: 50,
                        //allowBlank: ,
                        anchor: '100%'
                    }
                    /*{
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../account_id',
                        displayField: 'account_code',
                        emptyText: "All Account",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    },
                    {
                        xtype: "radiogroup",
                        fieldLabel: "Show Data",
                        id: "showgroup",
                        ref: '../showdata',
                        defaults: {xtype: "radio",name: "showdata"},
                        items: [
                            {
                                boxLabel: "Header",
                                inputValue: "H"
                            },
                            {
                                boxLabel: "PerMonth",
                                inputValue: "D"
                            }
                        ]
                    }*/
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                /*{
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },*/
/*                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }*/
            ]
        };
        jun.ReportBep.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        //this.btnPdf.on("click", this.onbtnPdfclick, this);
        //this.btnChart.on("click", this.Chart, this);
    },
    /*onbtnPdfclick: function () {
        var url = "Report/bep";
        /!*url = "Report/bep";
        var selectedz = this.showdata.getValue();
        if(selectedz && selectedz.inputValue == 'H')
        {
            //url = "Report/Realization";

        }
        else
        {
            url = "Report/RealizationPerMonth";
        }*!/


        Ext.getCmp("form-ReportBep").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBep").getForm().url = url;
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBep').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },*/
    onbtnSaveclick: function () {
        var url = "Report/BepToExcel";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportBep").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportBep").getForm().url = url;
            var form = Ext.getCmp('form-ReportBep').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportBep").getForm().getValues());
        }
    },
    /*Chart: function () {
        Ext.getCmp("form-ReportBep").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBep").getForm().url = "Chart/Realization";
        var form = Ext.getCmp('form-ReportBep').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }*/

});



//--------------------------------------------
/////////////ALL BU

//REPORT BUDGET PLANS ALL
jun.ReportBudgetPlanAll = Ext.extend(Ext.Window, {
    title: "Report All Budget Plans",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 191,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBudgetPlanAll",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../account_id',
                        displayField: 'account_code',
                        emptyText: "All Account",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportBudgetPlanAll.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBudgetPlanAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBudgetPlanAll").getForm().url = "Report/BudgetPlanAll";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBudgetPlanAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/BudgetPlanAll";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportBudgetPlanAll").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportBudgetPlanAll").getForm().url = url;
            var form = Ext.getCmp('form-ReportBudgetPlanAll').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportBudgetPlanAll").getForm().getValues());
        }
    },
    Chart: function () {
        Ext.getCmp("form-ReportBudgetPlanAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBudgetPlanAll").getForm().url = "Chart/BudgetPlanAll";
        var form = Ext.getCmp('form-ReportBudgetPlanAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

//REPORT BUDGET REALIZATION ALL
jun.ReportBudgetRealizationAll = Ext.extend(Ext.Window, {
    title: "Report All Budget Realizations",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 191,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBudgetRealizationAll",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../account_id',
                        displayField: 'account_code',
                        emptyText: "All Account",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    },
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportBudgetRealizationAll.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportBudgetRealizationAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBudgetRealizationAll").getForm().url = "Report/BudgetRealizationAll";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBudgetRealizationAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/BudgetRealizationAll";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportBudgetRealizationAll").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportBudgetRealizationAll").getForm().url = url;
            var form = Ext.getCmp('form-ReportBudgetRealizationAll').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportBudgetRealizationAll").getForm().getValues());
        }
    },
    Chart: function () {
        Ext.getCmp("form-ReportBudgetRealizationAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBudgetRealizationAll").getForm().url = "Chart/BudgetRealizationAll";
        var form = Ext.getCmp('form-ReportBudgetRealizationAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }

});

//REPORT BUDGET ANALYSIS ALL
jun.ReportBudgetAnalysisAll = Ext.extend(Ext.Window, {
    title: "Report All Budget Analysis",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 212,//191 + 30 + 50,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportBudgetAnalysisAll",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Show By',
                        name: 'showby',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['Y', 'Year'],
                                ['MY', 'Month & Year'],
                                ['D', 'Date'],
                                ['A', 'Account']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../cmbShowBy',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        //fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../cmbAccount',
                        displayField: 'account_code',
                        emptyText: "All Account..."

                    },
                    {
                        xtype: 'combo',
                        //fieldLabel: 'Type',
                        name: 'month',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['01', 'January'],
                                ['02', 'February'],
                                ['03', 'March'],
                                ['04', 'April'],
                                ['05', 'May'],
                                ['06', 'June'],
                                ['07', 'July'],
                                ['08', 'August'],
                                ['09', 'September'],
                                ['10', 'Okteber'],
                                ['11', 'November'],
                                ['12', 'December']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'value',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Month...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../cmbMonth'
                    },
                    {
                        xtype: 'combo',
                        //fieldLabel: 'Type',
                        name: 'year',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['2018', '2018'],
                                ['2019', '2019'],
                                ['2020', '2020'],
                                ['2021', '2021'],
                                ['2022', '2022'],
                                ['2023', '2023'],
                                ['2024', '2024'],
                                ['2025', '2025'],
                                ['2026', '2026'],
                                ['2027', '2027'],
                                ['2028', '2028'],
                                ['2029', '2029'],
                                ['2030', '2030']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Year...",
                        mode : 'local',
                        allowBlank: true,
                        width:'60',
                        ref: '../cmbYear'
                    },
                    {
                        //fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        emptyText: "From..."
                        //anchor: "100%"
                    },
                    {
                        //fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        emptyText: "To..."
                        //anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    /*{
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../account_id',
                        displayField: 'account_code',
                        emptyText: "All Account",
                        anchor: '100%'
                    },*/
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportBudgetAnalysisAll.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
        this.on("activate", this.onActivate, this);
        this.cmbShowBy.on('Select', this.ShowBy, this);
    },

    onActivate: function(){
        this.cmbAccount.setVisible(false);
        this.cmbMonth.setVisible(false);
        this.cmbYear.setVisible(false);
        this.tglfrom.setVisible(false);
        this.tglto.setVisible(false);
    },

    ShowBy: function(){

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == 'Y')
        {
            this.cmbYear.setVisible(true);
            this.cmbMonth.setVisible(false);
            this.cmbAccount.setVisible(false);
            this.tglfrom.setVisible(false);
            this.tglto.setVisible(false);

        }
        else if(selectedz == 'MY')
        {
            this.cmbYear.setVisible(true);
            this.cmbMonth.setVisible(true);
            this.cmbAccount.setVisible(false);
            this.tglfrom.setVisible(false);
            this.tglto.setVisible(false);
        }
        else if(selectedz == 'A')
        {
            this.cmbYear.setVisible(false);
            this.cmbMonth.setVisible(false);
            this.cmbAccount.setVisible(true);
            this.tglfrom.setVisible(false);
            this.tglto.setVisible(false);
        }
        else if(selectedz == 'D')
        {
            this.cmbYear.setVisible(false);
            this.cmbMonth.setVisible(false);
            this.cmbAccount.setVisible(false);

            this.tglfrom.setVisible(true);
            this.tglto.setVisible(true);

            this.height.setHeight(300);
        }
    },

    onbtnPdfclick: function () {

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == undefined){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        Ext.getCmp("form-ReportBudgetAnalysisAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBudgetAnalysisAll").getForm().url = "Report/BudgetAnalysisAll";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportBudgetAnalysisAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        var url = "Report/BudgetAnalysisAll";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportBudgetAnalysisAll").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportBudgetAnalysisAll").getForm().url = url;
            var form = Ext.getCmp('form-ReportBudgetAnalysisAll').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportBudgetAnalysisAll").getForm().getValues());
        }
    },
    Chart: function () {

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        Ext.getCmp("form-ReportBudgetAnalysisAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportBudgetAnalysisAll").getForm().url = "Chart/BudgetAnalysisAll";
        var form = Ext.getCmp('form-ReportBudgetAnalysisAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});

//REPORT INVESTMENT PLANS ALL
jun.ReportInvestmentPlanAll = Ext.extend(Ext.Window, {
    title: "Report All Investment Plans",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInvestmentPlanAll",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../account_id',
                        displayField: 'account_code',
                        emptyText: "All Account",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportInvestmentPlanAll.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInvestmentPlanAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInvestmentPlanAll").getForm().url = "Report/InvestmentPlanAll";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInvestmentPlanAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/InvestmentPlanAll";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportInvestmentPlanAll").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInvestmentPlanAll").getForm().url = url;
            var form = Ext.getCmp('form-ReportInvestmentPlanAll').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportInvestmentPlanAll").getForm().getValues());
        }
    },
    Chart: function () {
        Ext.getCmp("form-ReportInvestmentPlanAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInvestmentPlanAll").getForm().url = "Chart/InvestmentPlanAll";
        var form = Ext.getCmp('form-ReportInvestmentPlanAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }

});

//REPORT INVESTMENT REALIZATIONS ALL
jun.ReportInvestmentRealizationAll = Ext.extend(Ext.Window, {
    title: "Report All Investment Realizations",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 200,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInvestmentRealizationAll",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../account_id',
                        displayField: 'account_code',
                        emptyText: "All Account",
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportInvestmentRealizationAll.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
    },
    onbtnPdfclick: function () {
        Ext.getCmp("form-ReportInvestmentRealizationAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInvestmentRealizationAll").getForm().url = "Report/InvestmentRealizationAll";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInvestmentRealizationAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var url = "Report/InvestmentRealizationAll";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportInvestmentRealizationAll").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInvestmentRealizationAll").getForm().url = url;
            var form = Ext.getCmp('form-ReportInvestmentRealizationAll').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportInvestmentRealizationAll").getForm().getValues());
        }
    },
    Chart: function () {
        Ext.getCmp("form-ReportInvestmentRealizationAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInvestmentRealizationAll").getForm().url = "Chart/InvestmentRealizationAll";
        var form = Ext.getCmp('form-ReportInvestmentRealizationAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }

});

//REPORT INVESTMENT ANALYSIS ALL
jun.ReportInvestmentAnalysisAll = Ext.extend(Ext.Window, {
    title: "Report All Investment Analysis",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 212,//191 + 30 + 50,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ReportInvestmentAnalysisAll",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        xtype: 'combo',
                        fieldLabel: 'Show By',
                        name: 'showby',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['Y', 'Year'],
                                ['MY', 'Month & Year'],
                                ['D', 'Date'],
                                ['A', 'Account'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../cmbShowBy',
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        //fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../cmbAccount',
                        displayField: 'account_code',
                        emptyText: "All Account...",

                    },
                    {
                        xtype: 'combo',
                        //fieldLabel: 'Type',
                        name: 'month',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['01', 'January'],
                                ['02', 'February'],
                                ['03', 'March'],
                                ['04', 'April'],
                                ['05', 'May'],
                                ['06', 'June'],
                                ['07', 'July'],
                                ['08', 'August'],
                                ['09', 'September'],
                                ['10', 'Okteber'],
                                ['11', 'November'],
                                ['12', 'December'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'value',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Month...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../cmbMonth'
                    },
                    {
                        xtype: 'combo',
                        //fieldLabel: 'Type',
                        name: 'year',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['2018', '2018'],
                                ['2019', '2019'],
                                ['2020', '2020'],
                                ['2021', '2021'],
                                ['2022', '2022'],
                                ['2023', '2023'],
                                ['2024', '2024'],
                                ['2025', '2025'],
                                ['2026', '2026'],
                                ['2027', '2027'],
                                ['2028', '2028'],
                                ['2029', '2029'],
                                ['2030', '2030']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: true,
                        emptyText: "Year...",
                        mode : 'local',
                        allowBlank: true,
                        width:'60',
                        ref: '../cmbYear'
                    },
                    {
                        //fieldLabel: 'From',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglfrom",
                        ref: '../tglfrom',
                        emptyText: "From..."
                        //anchor: "100%"
                    },
                    {
                        //fieldLabel: 'To',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tglto",
                        ref: '../tglto',
                        emptyText: "To..."
                        //anchor: "100%"
                    },
                    {
                        xtype: "hidden",
                        name: "format",
                        ref: "../format"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Chart Type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['column', 'Column'],
                                ['bar', 'Bar'],
                                ['area', 'Area'],
                                ['spline', 'Spline'],
                                ['pie', 'Pie'],
                                ['line', 'Line']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'100',
                        ref: '../charttype',
                        anchor: "100%"
                    },
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    iconCls: "silk13-page_white_excel",
                    text: "Save to Excel",
                    ref: "../btnSave"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-html",
                    text: "Show HTML",
                    ref: "../btnPdf"
                },
                {
                    xtype: "button",
                    iconCls: "silk13-chart_bar",
                    text: "Show Chart",
                    ref: "../btnChart"
                }
            ]
        };
        jun.ReportInvestmentAnalysisAll.superclass.initComponent.call(this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.btnPdf.on("click", this.onbtnPdfclick, this);
        this.btnChart.on("click", this.Chart, this);
        this.on("activate", this.onActivate, this);
        this.cmbShowBy.on('Select', this.ShowBy, this);
    },

    onActivate: function(){
        this.cmbAccount.setVisible(false);
        this.cmbMonth.setVisible(false);
        this.cmbYear.setVisible(false);
        this.tglfrom.setVisible(false);
        this.tglto.setVisible(false);
    },

    ShowBy: function(){

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == 'Y')
        {
            this.cmbYear.setVisible(true);
            this.cmbMonth.setVisible(false);
            this.cmbAccount.setVisible(false);
            this.tglfrom.setVisible(false);
            this.tglto.setVisible(false);

        }
        else if(selectedz == 'MY')
        {
            this.cmbYear.setVisible(true);
            this.cmbMonth.setVisible(true);
            this.cmbAccount.setVisible(false);
            this.tglfrom.setVisible(false);
            this.tglto.setVisible(false);
        }
        else if(selectedz == 'A')
        {
            this.cmbYear.setVisible(false);
            this.cmbMonth.setVisible(false);
            this.cmbAccount.setVisible(true);
            this.tglfrom.setVisible(false);
            this.tglto.setVisible(false);
        }
        else if(selectedz == 'D')
        {
            this.cmbYear.setVisible(false);
            this.cmbMonth.setVisible(false);
            this.cmbAccount.setVisible(false);

            this.tglfrom.setVisible(true);
            this.tglto.setVisible(true);

            this.height.setHeight(300);
        }
    },

    onbtnPdfclick: function () {

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == undefined){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        Ext.getCmp("form-ReportInvestmentAnalysisAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInvestmentAnalysisAll").getForm().url = "Report/InvestmentAnalysisAll";
        this.format.setValue("html");
        var form = Ext.getCmp('form-ReportInvestmentAnalysisAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    onbtnSaveclick: function () {
        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        var url = "Report/InvestmentAnalysisAll";
        this.format.setValue("excel");
        if (!is_enable_tools()) {
            Ext.getCmp("form-ReportInvestmentAnalysisAll").getForm().standardSubmit = !0;
            Ext.getCmp("form-ReportInvestmentAnalysisAll").getForm().url = url;
            var form = Ext.getCmp('form-ReportInvestmentAnalysisAll').getForm();
            var el = form.getEl().dom;
            var target = document.createAttribute("target");
            target.nodeValue = "myFrame";
            el.setAttributeNode(target);
            el.action = form.url;
            el.submit();
        } else {
            DownloadOpen(url, 'POST',
                '.xls', Ext.getCmp("form-ReportInvestmentAnalysisAll").getForm().getValues());
        }
    },
    Chart: function () {

        var selectedz = this.cmbShowBy.getValue();

        if(selectedz == ""){
            Ext.MessageBox.alert("Warning","Anda belum memilih Data yang akan di tampilkan.");
            return;
        }

        Ext.getCmp("form-ReportInvestmentAnalysisAll").getForm().standardSubmit = !0;
        Ext.getCmp("form-ReportInvestmentAnalysisAll").getForm().url = "Chart/InvestmentAnalysisAll";
        var form = Ext.getCmp('form-ReportInvestmentAnalysisAll').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "_blank";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    }
});


