jun.Racikanstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.Racikanstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'RacikanStoreId',
            url: 'Racikan',
            root: 'results',
            totalProperty: 'total',
//            autoLoad: true,
            fields: [
                {name: 'racikan_id'},
                {name: 'barang_id'},
                {name: 'qty', type: 'float'},
                {name: 'satuan'},
                {name: 'tdate'},
                {name: 'user_id'},
                {name: 'note'},
                {name: 'kode_barang'}

            ]
        }, cfg));
    }
});
jun.rztRacikan = new jun.Racikanstore();

//jun.rztRacikan.load();
