jun.TransaksiOverrideWin = Ext.extend(Ext.Window, {
    title: 'Sales Override',
    modez:1,
    width: 400,
    height: 285,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,
    initComponent: function() {
        if (jun.rztTransaksiCategory.getTotalCount() === 0) {
            jun.rztTransaksiCategory.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-TransaksiOverride',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'textfield',
                        name: 'doc_ref',
                        ref: '../doc_ref',
                        fieldLabel: 'Doc.Ref',
                        readOnly: true,
                        maxLength: 50,
                        width: 200,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        name: 'name',
                        ref: '../name',
                        fieldLabel: 'Name',
                        //readOnly: true,
                        maxLength: 50,
                        width: 200,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Sales Categories',
                        store: jun.rztTransaksiCategory,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{category_code} - {category_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'category_id',
                        valueField: 'category_id',
                        ref:'../category_id',
                        displayField: 'category_code',
                        emptyText: "Category",
                        anchor: '100%'
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Amount',
                        hideLabel:false,
                        //hidden:true,
                        name:'amount',
                        id:'amountid',
                        ref:'../amount',
                        maxLength: 50,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Description',
                        hideLabel:false,
                        //hidden:true,
                        name:'description',
                        id:'descriptionid',
                        ref:'../description',
                        anchor: '100%'
                        //allowBlank: 1
                    },
                    {
                        xtype: 'xdatefield',
                        ref:'../tdate',
                        fieldLabel: 'Date',
                        name:'tdate',
                        id:'tdateid',
                        format: 'M Y',
                        //allowBlank: 1,
                        anchor: '100%'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.TransaksiOverrideWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        //this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        }
        else if(this.modez == 3)
        {
            this.btnSaveClose.setVisible(false);
            this.btnSave.setVisible(false);
        }
        else {
            this.btnSave.setVisible(true);
        }
    },



    /*groupprodukonselect: function(c,r,i)
    {
        this.gridDetail.produk.store.reload({params:{groupproduk_id:r.get('groupproduk_id')}});
    },*/

    btnDisabled:function(status)
    {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },

    saveForm : function()
    {
        this.btnDisabled(true);
        var urlz= 'projection/TransaksiOverride/create/';

        Ext.getCmp('form-TransaksiOverride').getForm().submit({
            url:urlz,
            timeOut: 1000,
            scope: this,
            params: {
                mode: this.modez,
                id: this.id,
            },
            success: function(f,a){
                jun.rztTransaksi.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    this.formz.getForm().reset();
                    this.gridDetail.store.removeAll();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },

    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },

    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }

});