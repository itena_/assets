/*
var itemFile = null;
jun.ImportDataProjection = new Ext.extend(Ext.Window, {
    width: 500,
    height: 90,
    layout: "form",
    modal: !0,
    resizable: !1,
    plain: !0,
    border: !1,
    title: "Import Data",
    iswin: true,
    padding: 5,
    initComponent: function () {
        this.items = new Ext.FormPanel({
            frame: !1,
            labelWidth: 75,
            fileUpload: true,
            bodyStyle: "background-color: #E4E4E4;padding: 10px",
            id: "form-BackupRestoreWin",
            labelAlign: "left",
            layout: "form",
            ref: "formz",
            html: "File Type : " +
            "<select id='importType'>" +
            "<option  value='businessunit'>Business Units</option>" +
            "<option value='groupproduct'>Product Groups</option>" +
            "<option value='product'>Products</option>" +
            "<option value='outlet'>Outlets</option>" +
            "<option value='unit'>Units</option>" +
            "<option value='transaction'>Transactions</option>" +
            "<option value=''>------------------</option>" +
            "<option value='account'>Accounts</option>" +
            "<option value='accountgroup'>Account Groups</option>" +
            "<option value='accountcategory'>Account Categorys</option>" +
            "<option value='budget'>Budgets</option>" +
            "<option value='realization'>Realizations</option>" +
            "</select>" +
            "<input id='inputFile' type='file' name='uploaded'/>",
            border: !1,
            plain: !0,
            listeners: {
                afterrender: function () {
                    itemFile = document.getElementById("inputFile");
                    itemFile.addEventListener('change', EventChange, false);
                }
            }
        });
        jun.ImportDataProjection.superclass.initComponent.call(this);
    }
});
function to_json(workbook) {
    var result = {};
    workbook.SheetNames.forEach(function (sheetName) {
        var roa = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
        if (roa.length > 0) {
            result[sheetName] = roa;
        }
    });
    return result;
}
function EventChange(e) {
    var files = itemFile.files;
    var f = files[0];
    if (f.name.split('.').pop() != "xlsx") {
        Ext.Msg.alert('Failure', 'Need file *.xlsx');
        return;
    }
    var rABS = typeof FileReader !== "undefined" && typeof FileReader.prototype !== "undefined" &&
        typeof FileReader.prototype.readAsBinaryString !== "undefined";
    var reader = new FileReader();
    reader.onload = function (e) {
        if (typeof console !== 'undefined') console.log("onload", new Date());
        var data = e.target.result;
        var wb = XLSX.read(data, {type: 'binary'});
        var s = wb.Strings;
        var e = document.getElementById("importType");
        var tipe = e.value;
        if (tipe == "unit") {
            if (s[0].h.indexOf("NAME") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column NAME');
                return;
            }
            if (s[1].h.indexOf("CODE") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column CODE');
                return;
            }
            if (s[2].h.indexOf("DESCRIPTION") == -1) {
                Ext.Msg.alert('Failure', 'Not valid column DESCRIPTION');
                return;
            }
        }
        var output = to_json(wb);
        var url = "projection/import/unit";
        Ext.Ajax.request({
            method: 'POST',
            scope: this,
            url: url,
            params: {
                detil: Ext.encode(output),
                filename: files.name
            },
            success: function (f, a) {
                var response = Ext.decode(f.responseText);
                var win = new Ext.Window({
                    width: 1000,
                    height: 600,
                    modal: !0,
                    border: !1,
                    layout: 'fit',
                    autoScroll: true,
                    title: "Duplicate Data",
                    iswin: true,
                    padding: 5,
                    html: response.msg
                });
                win.show();
//                Ext.Msg.alert('Successfully', response.msg);
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
    reader.readAsBinaryString(f);
}
*/

/**
 * Created by wisnu on 21/02/2018.
 */


jun.ImportDataAsset = Ext.extend(Ext.Window, {
    title: "Import Data",
    iconCls: "silk13-report",
    modez: 1,
    width: 400,
    height: 380 + 20,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
/*        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }*/
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-ImportDataAsset",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Date',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl",
                        ref: '../tgl',
                        //readOnly: !EDIT_TGL,
                        value: new Date(),
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Import',
                        hiddenName: 'importtype',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['I', 'Item'],
                                ['CL', 'Class'],
                                ['C', 'Categories'],
                                ['B', 'Branch'],
                                ['A', 'Assets'],
                                ['AM', 'Assets Migration'],
                                ['BU','Business Units']
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Choose...",
                        mode : 'local',
                        allowBlank: true,
                        width:'120',
                        //height: '150',
                        ref: '../cmbImport'
                    },
                    /*{
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: 'Branch',
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "Select a branch",
                        anchor: '100%'
                    },*/
//                    {
//                        xtype: 'fileuploadfield',
//                        fieldLabel: 'File',
//                        emptyText: 'Select file Stock Opname',
//                        fieldLabel: 'File Stock Opname',
//                        ref: '../file',
//                        name: 'file',
//                        buttonText: 'Browse',
//                        anchor: '100%'
//                    },
                    {
                        html: '<input type="file" name="xlfile" id="inputFile" />',
                        name: 'file',
                        xtype: "panel",
                        listeners: {
                            render: function (c) {
                                new Ext.ToolTip({
                                    target: c.getEl(),
                                    html: 'Format file : Excel (.xls)<br>Hanya Sheet pertama yang akan terbaca.<br>Pastikan sudah sesuai format pada <b>TEMPLATE</b>'
                                });
                            },
                            afterrender: function () {
                                itemFile = document.getElementById("inputFile");
                                itemFile.addEventListener('change', readFileExcel, false);
                            }
                        }
                    },
                    {
                        xtype: "panel",
                        bodyStyle: "background-color: #E4E4E4;padding: 5px",
                        html: 'Format file : Excel (.xls)<br>Pastikan hanya ada 1 sheet didalam file (nama sheet apa aja boleh)' +
                        '<br>kolom header : <b>sesuai format TEMPLATE</b><br>Contoh :<br>' +
                        '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATcAAACsCAIAAACRlnxRAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABCVSURBVHhe7Z09ryRHFYYvvwWHrITtbH+EZSfsan/AFYlliWCT1Q0ILYO0EqG1uwQOFgnIuAiJay0Cpw5xsPhLQogAi5zEnFOfpz66u3pmumbq9PuodG91VXVNd5/zTM90z9x79Z/v/ouCgnLJ5eozAMBlc/W/baCpXQ1sxg4P8t522e4vLB0YWKoeWDo8sFQ9sDTh9bNHb7/96NlrtzgEvQ+yOUYpN3eurxNnyKtkr8+zv7DUQqF4dHPzaCxNz2FpcoRMAndN3N67fHcjn7vPtb+w1EDBoFhwSHo/WR7D2S01LYotLROiPATbAksjVtJaVC6as1tKDT1Tlui6y92fg0pgacBLehFxWcE5LM3RbGnMi7MBSx2Jmbxw7sg0cw5L04PT/cUHLD0l41haOUGMoun5Le2eyF13OXn+Pg+w1FBEopKKlwos3ZiKpp1fPcBSppZlnQNxOGe3tJLFG9N7lzkX4i7z/p7jWWnnltZPBaNoeg5LU7pmLHOGvEr2undawNLh2eFB3tsuw9LhgaXqgaXDA0vV4yylXwCAS2bDc+n3YGO++OILV9sNe9tlu7+wdGBgqXpg6fDAUvUcYGn97mIVYent9dXV1f2nr91iO6+f3qc1r2/dYjsHP+JgwFL1rLWUb/W338aGpR1oS9nba0WHApZOYj99wX/LAJZeEg0pq+1QwNJJXt/dsZss61GWWu18zph2i0yj0Hz9NLHUrmxYEtc94tMwUxgvHjNvvX99HbZubtjstG6SpQ08EUnKhm2Ix00csqu33nuLfvoNO/gJ8NzA0iWOtNSmkcsM38h1mTKi3aWdaRdDTFVqXWLXtGPqa8qHTMc3DhPbKet2SFhjY0TKTm1D3m5b0/0aCVi6xDGWWlxaJxnDhEWRVDKXkqwSFk0wMYtFbI5vTMY7FoaJWWcfbVNiyk5ug+wwdW7uupGnBZYucbyllRw3+FxK2+OSrSXM5ViSs3HJzRKrfo50fNOw2JOOSZc2JqTs9DYku2YWrm+7buOJgaVLHGVpmhwuX0w/ERaTpIorrMyr+ixyknTCiUedGyZ6JtfenJiyk9uQdLil+x038dTA0iVOdPWoSO2JrDJV1y6GNHhQX9PWzNxygB/vU7ltmNwK0Z6tsTEiZae2wWyn37XQ120LTw4sXeJoSyt1S0wjn//E9DVeObyGfRR7uVUOj1PYTt8ht4poGWbH+G0Lu5Jt88YkKTtx3GKzWXZb2mkDTw8sPSXC0l1hpIjCb8tEyqbPICnW0mElhaUnZUeWurOVOFf1kmC9paNLCktPSg9Lw4u5lP45mGxIrxMpsc5S/7J8YEd3aynptBH0AACA48Er3oGh+LnabtjbLtv9haUDA0vVA0uHB5aqZ42lfKfUsv5+qbmWkVxS4RbZ4C9rGOavbZSTBWa6tDJsylLEZaTs5S4mtMYmJiYFLJ1C/Kn35r/6nlma2MctPhymN/ZliznUzR80qLk406WWMVPWPinHQPGyXRKJQY3VNIClEyR/R6X1j6qklprP8ISjHoMhwhLInmcltoujWkRwpksvw6Usx5uU5GwIMa7GmwbWkwCWtnCgpUYgf9yDm1xZIZWfouLiTJdixrP09pZTICSAq5cR43iaLwUkZ10Cli7DL3jb3pkWlkaTKi1NxNFFZGe6NDNqyoYEIEzobqnB4EInBnBVRBSWLmAuIR129cgecW9QaEktpSWPaPX4lcNCHDPTpRslloZ412PHI0J4YekcqxQlapaaOlVDC1fi06TD9/JvB40RSx4xqWsJ+C7laLE0eYqtJgQsXWatokTdUnv6jBcP0j5Lra0MXxw106UdDZYm4StiyXDKhHDC0gnWfK00MGWpe4mTBEkGxiwXkiWBcrgn4Zku9WiwVAY3xE0EkKsiwLC0Dl8xSmm5YTptqRVRtlgzHclQR4yjxITy59Nd+jXVYalMgBA0kRNJIGHpKYmWgs3YW8oSsPSUwNIOwFL1wNLhgaXqcZaSThtBDwAAOB6cSweG4udqu2Fvu2z3F5YODCxVDywdHliqnjWWxnumh32ON72nmd8u47ubgfptTnH7zOMHznQxDZMPy3gpG4MVEyANYAhRbSgsnYQ/e+Q/yMC6nuNb4HIVQxxY77ItrGjsm5p8WEZLWfFhk7RaBiVGLo0vLG3hwO+XHvst8HIgt5gZJ+bgzjDGUzSMzWApm0Q2LFBMynhXk4CBpcuYj90fcC6lIy6Oe/CqXZqwiifGtuhinKbul1ZGTtmQD1zJv/A9nRiwdBYj6FHfLw1xKVsW4VVy3KpVS0OY2VOHPl3HTVkOS5YMLsg+auVXww2wdJkjzqWuxkc8tKSWCqFEq0XE0sGjTfjKLsI/VCBMnrQOzqApy8EpQ8b4oJpouRFpfGFpCwe+L3VHmepUDS1cKazxvfzbYf9DcQyVgSIZJsm6YmeGmVOPpyOmrAlBLTYMd3pLQ5h8owGWtnCcpVafA78FnjXOWeqiXLrKg2Hp2eDDXwY3ImMKS5lmS8Xdl/YvhE9Zag2KoTJxE96Y5TKQ+SQidNUu12ceS8zu7NXCYClbRIoRIeGq76e6q6Yxg6WTuCtHTNObUmLaUmuRbDENniKMhmSIIYwruxIRGyYflrFSlnVLsXESEaoHTrbC0lMSLQWbsbeUJWDpKYGlHYCl6oGlwwNL1eMsJZ02gh4AAHA8OJcODMXP1XbD3nbZ7i8sHRhYqh5YOjywVD3rLW3/TIO01Nz3Km6YyobkRpq8ORZJ7npawkCzenEntHxYfQybsvEzC0yMf2xMAx6TApYuwJ9BOtTSxD5pqemNfdliQK5iEANtkDMhTX/eqI0xUzaLFy/aQMrgU2slDWDpAvwB3keHnku3/Ba4WYHmT7o5yNe1iVQxXMpy0EjQ+EFuKaldsO00sB47WDoDOXpzd/grXjriQr6gXDRtibCKR4TRzHybhJUlvRWPqJTxLC3/F7jQVDRz7PC/wIkVlpKebOdRlqZPlFnLIrxKjl/VzkIjwlzUQnFvn31URk1ZoaOBIpVEVA4wkY9P5bB0AufoMVeP7BHnGh/v0JJ65GNFiFaLCJuDR9vguVloiBtBDdyTzq4RFZZyIGPghJCepBWW1jEvdk3tWEtNnaqhhStFVHwv/3bMfgs8VGiM+U3LZtIwQC0aLOV6yIFESE8yApZWMVd2Mxq+vla31Kpz2m+BxwoNogot2piGdrWoszRZ8CRxhKVLHH8uZeigEzJIMjBmubQrn0TGM0aRGvmaQ9GuFQ2WygVODRO8ULFVEUZYusRpLDUNSYtp8CRDA8kQQxwnbDTDvPOw9FLJU4JNzIIqAu7jaYClpyRaCjZjbylLwNJTAks7AEvVA0uHB5aqx1lKOm0EPQAA4HhwLh0Yip+r7Ya97bLdX1g6MLBUPbB0eGCpelZYyjdKIwf8N6f0xuXk7TImuTsWELfPPH5gpUv5bdKAkpSN8ZeBi3GVrbB0kvhR3mYySxP7pKWmN/ZliwG5iiEOnOnSjoaU5Wj5YLGurs5VG9U0vrB0CjqVtn7kKJBauuW3wCtziGirRl3KhthXk4CBpVO4P9NgOPgTguK4B6+CaYsUKlKDWy66ZmKsDGUpy5GMz7z1xIClE4iP75o3qAe8LzXOBHfKlkVMADPcqmE2T9GgFj0p6+Lrw2YSA/8LnGi2NIG/yNaiacVSU+MjHlpSS2nJU0hWmsejw/NuRv1pWB/KUtZE0sTOpIKLdxp6WNoCn00PtdTUqRpauFIY5Xv5t2P9/wLfB+pS1sc0PAUzSZbA0hb4PWrLW9O6pTYOp/0WeHX8PlBrKccUljLNlooXuVxtu340Zal9noyvabk3xsAtl9blk4jQFV37QUPKitOmjKT3NRlAwNJJxKcaWu+bTltqGpIW0+CpC5cMMYRxxfz7QUfKitiKp2vRLFth6SmJloLN2FvKErD0lMDSDsBS9cDS4YGl6nGWkk4bQQ8AADieq79/99kWhbBPBmA7KH6uthv2tst2f2HpwMBS9cDS4YGl6lll6csPn129+dEPqLz36cuit1JgaQdgqXpWWPrJ767efPbBHdW//OC9j+59+GU+oCywtAOwVD3tlv7y/TYzZYGlHYCl6mm2lM+f73ySNS4VWNoBWKqeZks/f4de7n7y6T37vvT9z4sBtQJLOwBL1bPG0nDRCO9LLwlYqp5VloZXvHyxt+V0Cks7AEvV02xp8r4Ull4QsFQ9zZaaOzHuFW9yXp0rsLQDsFQ9KyyVn2rA1aPLAZaqZ5Wlqwss7QAsVQ8sHR5Yqh5YOjywVD3OUvZpG+gBAADHg7+oMjAUP1fbDXvbZbu/sHRgYKl6YOnwwFL1NFvKf9A+peFPZ8PSDsBS9TRbKhH/JXEeWNoBWKqeAyxtdhSWdgGWqme9pa3/u5SBpR2ApepZa+mKEykBSzsAS9Wz0tJ1ksLSHsBS9ay0tPXfCztgaQdgqXrWWbrmPSkDSzsAS9WzylJ+vbtGUljaA1iqnlWWrj2VwtIewFL1rLJ0NbC0A7BUPbB0eGCpemDp8MBS9ThLSaeNoAcAABwPzqUDQ/Fztd2wt122+wtLBwaWqgeWDg8sVc8aS+MXwVs/JQhLOwBL1dNuafxIA38Eqe3TDbC0A7BUPc2WJp87ooWm0yks7QAsVU+zpdm5tO01LyztACxVT7ulhH9j2vzlNVjaAViqnnZLWVGnZ/Lqdw5Y2gFYqh5n6e2f/qK72L3dIdlxQBm3XP3zX/9WXGgPXc7uD9r37GigDFpgqVpgqZoCS9UCS9UUWKoWWKqm5Ja+/O0fFltmyvMXv85aqqVxWCgvf//HrKWxwNITlp/+7BdUskaUDmXBUlpst5Tca7d0lahkablhcnGq6LH06xfvPnjxlVtoApaqKXOWWkUbfbDirbK0cTAVa2m2baE+U5osJQHu/fiNJ6/c4vff3z358RtTSrTbUo40D/RDU959/o1rOXi2JWCpmjJpqbXCltA7VYJ1jeKtHR8sDRvTslVUWi19cPP4wcOPvw6LDyeVWG+L59Xje/4hqG6fFGApSkOpWxqUsEUOKItUrtG6tatIS+32LG6VLc2Wvrh7/tCe3+hE+vi5UUKK4erffPwgngy/ev7Q1t/w+sWWbHWGLL25c3VLMhs/hJstDKNV3IDHv/GzvboJDzdPaelPfvUjKvP1rFgzp0o2GGWjgnNpMNBYZOvyZzJm4pxG5tC5kbq8YHdP/FRm0UL+s4TipbWYjYV8bHpIdTOGHbYtjB0pHmIRWKqm4H1pVMVaxGIEJYJmoZ41+nOdeR/LpsUzoRwpeXUTXZ2ctpCcG+VpdpnS0iMLzDxXmbPULjb6QGWtpVnjTLGWJi0bWOr0CC3SEzkmDpZvZYNR3tWkMcOct+l3Oa2tVy01L8uTU/EssFRNWbC02jJT2i3NWuYLWZq1NJZ1lgZsi7XFNPCr0KBTJljo/frFY3vlll+sPvz4b4VmQbCwbnyItle85oTvrg8vAUvVlNxSZeUoS8XVoHef3IQx9oUxqWIropdNcy32alAybbxWJC//hNmMsbbXPTWEFirx6pF5lBZRYamaAkvVcnJLUc5VYKlaYKmaAkvVAkvVFP2W2puBOyywVE2BpWoLLFVTrv786V9RUFAuuVz948tvUVBQLrnAUhSUSy+wFAXl0gssRUG57PLlt/8HQHElZRMHMoAAAAAASUVORK5CYII=">'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Check File",
                    ref: "../btnCheck"
                },
                /*{
                    xtype: "button",
                    text: "Generate Query",
                    ref: "../btnQuery"
                },*/
                {
                    xtype: "button",
                    text: "Save",
                    ref: "../btnSave"
                }
            ]
        };
        jun.ImportDataAsset.superclass.initComponent.call(this);
        this.btnCheck.on("click", this.onbtnCheckclick, this);
        //this.btnQuery.on("click", this.onbtnQueryclick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
    },
    onbtnCheckclick: function () {
        if (jun.dataImport == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih file.");
            return;
        }

        var selectedz = this.cmbImport.getValue();

        if(selectedz == 'I')
        {
            jun.importDataItemStore.loadData(jun.dataImport[jun.namasheet]);
            var win = new jun.CheckInputItemDataWin({generateQuery: 0});
            win.show(this);
        }
        else if(selectedz == 'B')
        {
            jun.importDataBranchStore.loadData(jun.dataImport[jun.namasheet]);
            var win = new jun.CheckInputBranchDataWin({generateQuery: 0});
            win.show(this);
        }
        else if(selectedz == 'C')
        {
            jun.importDataBranchStore.loadData(jun.dataImport[jun.namasheet]);
            var win = new jun.CheckInputBranchDataWin({generateQuery: 0});
            win.show(this);
        }
        else if(selectedz == 'CL')
        {
            jun.importDataClassStore.loadData(jun.dataImport[jun.namasheet]);
            var win = new jun.CheckInputClassDataWin({generateQuery: 0});
            win.show(this);
        }
        else if(selectedz == 'A')
        {
            jun.importDataAssetStore.loadData(jun.dataImport[jun.namasheet]);
            var win = new jun.CheckInputAssetDataWin({generateQuery: 0});
            win.show(this);
        }
        else if(selectedz == 'AM')
        {
            jun.importDataAssetMigrationStore.loadData(jun.dataImport[jun.namasheet]);
            var win = new jun.CheckInputAssetMigrationDataWin({generateQuery: 0});
            win.show(this);
        }
        else if(selectedz == 'BU')
        {
            jun.importDataBUStore.loadData(jun.dataImport[jun.namasheet]);
            var win = new jun.CheckInputBUDataWin({generateQuery: 0});
            win.show(this);
        }

    },
    /*onbtnQueryclick: function () {
        console.log(jun.namasheet);
        if (jun.dataImport == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih file.");
            return;
        }
        jun.importDataUnitStore.loadData(jun.dataImport[jun.namasheet]);
        var win = new jun.QueryStockOpnameWin({
            tgl: this.tgl.getValue(),
            store: this.store.getValue()
        });
        win.show(this);
    },*/
    onbtnSaveclick: function () { //console.log(JSON.stringify(jun.dataStockOpname));
        Ext.getCmp('form-ImportDataAsset').getForm().submit({
            url: 'asset/AssetImport/Import',
            timeOut: 1000,
            scope: this,
            params: {
                data: Ext.encode(jun.dataImport)
            },
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Connection failure has occured.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});

//ITEM
jun.importDataItemStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataItemStore',
    idProperty: 'kode',
    fields: ['kode', 'nama', 'deskripsi']
});
jun.CheckInputItemDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Product / Item",
    width: 400,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataItemStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'kode', dataIndex: 'kode', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            if (jun.rztAssetBarangLib.findExact('kode_barang', value) != null)
                                metaData.style = "background-color: #FF7F7F;";
                            return value;
                        }
                    },
                    {
                        header: 'nama', dataIndex: 'nama', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },

                    {
                        header: 'deskripsi', dataIndex: 'deskripsi', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }
                ]
            }
        ];
        jun.CheckInputItemDataWin.superclass.initComponent.call(this);
    }
});

//BRANCH
jun.importDataBranchStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataBranchStore',
    idProperty: 'kode',
    fields: ['kode', 'nama', 'alamat']
});
jun.CheckInputBranchDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Branch",
    width: 400,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataBranchStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'kode', dataIndex: 'kode', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            if (jun.rztStoreLib.findExact('store_kode', value) != null)
                                metaData.style = "background-color: #FF7F7F;";
                            return value;
                        }
                    },
                    {
                        header: 'nama', dataIndex: 'nama', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },

                    {
                        header: 'alamat', dataIndex: 'alamat', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }
                ]
            }
        ];
        jun.CheckInputBranchDataWin.superclass.initComponent.call(this);
    }
});

//CATEGORY
jun.importDataCategoryStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataCategoryStore',
    idProperty: 'kode',
    fields: ['kode', 'nama', 'alamat']
});
jun.CheckInputCategoryDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Category",
    width: 400,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataCategoryStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'kode', dataIndex: 'kode', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            if (jun.rztAssetCategoryLib.findExact('category_code', value) != null)
                                metaData.style = "background-color: #FF7F7F;";
                            return value;
                        }
                    },
                    {
                        header: 'nama', dataIndex: 'nama', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },

                    {
                        header: 'alamat', dataIndex: 'keterangan', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }
                ]
            }
        ];
        jun.CheckInputCategoryDataWin.superclass.initComponent.call(this);
    }
});

//CLASS
jun.importDataClassStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataClassStore',
    idProperty: 'kode',
    fields: ['golongan', 'tariff', 'period', 'desc' ]
});
jun.CheckInputClassDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Class",
    width: 400,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataClassStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'golongan', dataIndex: 'golongan', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            //if (jun.rztAssetCategoryLib.findExact('category_code', value) != null)
                            //   metaData.style = "background-color: #FF7F7F;";
                            return value;
                        }
                    },
                    {
                        header: 'tariff', dataIndex: 'tariff', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'period', dataIndex: 'period', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'desc', dataIndex: 'desc', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }
                ]
            }
        ];
        jun.CheckInputClassDataWin.superclass.initComponent.call(this);
    }
});

//ASSETS
jun.importDataAssetStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataAssetStore',
    idProperty: '',
    fields: ['itemcode', 'qty', 'branch', 'date', 'price', 'class', 'category', 'desc', 'hide']
});
jun.CheckInputAssetDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Asset",
    width: 743,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataAssetStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'itemcode', dataIndex: 'itemcode', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'qty', dataIndex: 'qty', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'branch', dataIndex: 'branch', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'date', dataIndex: 'date', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'price', dataIndex: 'price', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'class', dataIndex: 'class', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'category', dataIndex: 'category', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'desc', dataIndex: 'desc', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'hide', dataIndex: 'hide', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }
       /*             {
                        header: 'activaold', dataIndex: 'activaold', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }*/
                ]
            }
        ];
        jun.CheckInputAssetDataWin.superclass.initComponent.call(this);
    }
});

//ASSETS MIGRATION
jun.importDataAssetMigrationStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataAssetStore',
    idProperty: 'activaold',
    fields: ['itemcode', 'qty', 'branch', 'date', 'price', 'class', 'category', 'desc', 'hide', 'activaold']
});
jun.CheckInputAssetMigrationDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Asset Migration",
    width: 743,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataAssetMigrationStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'itemcode', dataIndex: 'itemcode', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'qty', dataIndex: 'qty', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'branch', dataIndex: 'branch', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'date', dataIndex: 'date', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'price', dataIndex: 'price', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'class', dataIndex: 'class', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'category', dataIndex: 'category', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'desc', dataIndex: 'desc', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'hide', dataIndex: 'hide', width: 60, align: "right",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'activaold', dataIndex: 'activaold', width: 60, align: "right",
                        /*renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }*/
                        renderer: function (value, metaData, record, rowIndex) {
                            //if (jun.importDataAssetMigrationStore.findExact(ati_old, value) != null)
                            if (jun.rztAssetDetail.find('ati_old', value) != true)
                                metaData.style = "background-color: #FF7F7F;";
                            return value;
                        }
                    }
                ]
            }
        ];
        jun.CheckInputAssetMigrationDataWin.superclass.initComponent.call(this);
    }
});

//BUSINESS UNIT
jun.importDataBUStore = new Ext.data.JsonStore({
    storeId: 'jun.importDataBUStore',
    idProperty: 'kode',
    fields: ['kode', 'nama', 'tipe', 'deskripsi']
});
jun.CheckInputBUDataWin = Ext.extend(Ext.Window, {
    title: "Data Import Business Units",
    width: 400,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.importDataBUStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'kode', dataIndex: 'kode', width: 100,
                        renderer: function (value, metaData, record, rowIndex) {
                            if (jun.rztBusinessunitLib.find('businessunit_code', value) == true)
                                metaData.style = "background-color: #FF7F7F;";
                            return value;
                        }
                    },
                    {
                        header: 'nama', dataIndex: 'nama', width: 100, align: "left",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'tipe', dataIndex: 'tipe', width: 60, align: "left",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    },
                    {
                        header: 'deskripsi', dataIndex: 'deskripsi', width: 60, align: "left",
                        renderer: function (value, metaData, record, rowIndex) {
                            return value;
                        }
                    }

                ]
            }
        ];
        jun.CheckInputBUDataWin.superclass.initComponent.call(this);
    }
});

jun.QueryStockOpnameWin = Ext.extend(Ext.Window, {
    title: "Query Input Stock Opname",
    width: 600,
    height: 500,
    layout: "absolute",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'textarea',
                ref: "txtQuery",
                anchor: '100% 100%'
            }
        ];
        jun.QueryStockOpnameWin.superclass.initComponent.call(this);
        var query = "INSERT INTO posng.nscc_stock_moves(stock_moves_id,type_no,trans_no,tran_date,price,reference,qty,barang_id,store,tdate,id_user,up) VALUES ";
        for (i = 0; i < jun.importDataUnitStore.getCount(); i++) {
            var r = jun.importDataUnitStore.getAt(i);
            var idx = jun.rztUnit.find('code', r.get('code'));
            var rb = jun.rztUnit.getAt(idx);
            query += "\n(UUID(),-1,trans_no,'" + this.tgl.format('Y-m-d') + "',0,'SALDO AWAL'," + r.get('qty') + ",'" + rb.get('barang_id') + "','" + this.store + "',NOW(),'fff40883-0858-11e6-8e69-00ff55a5a602',0),";
        }
        this.txtQuery.setValue(query);
    }
});
