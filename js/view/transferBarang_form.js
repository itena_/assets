jun.TransferBarangWin = Ext.extend(Ext.Window, {
    title: 'Transfer Barang Masuk',
    modez: 1,
    width: 605,
    height: 445,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-TransferBarang',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Invoice No:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref_other',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref_other',
                        id: 'doc_ref_otherid',
                        ref: '../doc_ref_other',
                        style: {textTransform: "uppercase"},
                        maxLength: 50,
                        width: 170,
                        x: 400,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Cabang :",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        emptyText: "Pilih Cabang",
                        hiddenName: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        forceSelection: true,
                        width: 170,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'note',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        width: 170,
                        x: 400,
                        y: 62,
                        height: 22
                    },
                    new jun.TransferBarangDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 260,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: 'hidden',
                        name: 'type_',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        id: 'totalpotid',
                        ref: '../totalpot',
                        name: 'totalpot'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TransferBarangWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
//        this.disc.on('keyup', this.onDiscChange, this);
        this.on("close", this.onWinClose, this);
        this.tgl.setReadOnly(!EDIT_TGL);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        } else {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        }
    },
    onWinClose: function () {
        jun.rztTransferBarangDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztTransferBarangDetails.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'TransferBarang/createin/';
        Ext.getCmp('form-TransferBarang').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                id: this.id,
                mode: this.modez,
                detil: Ext.encode(Ext.pluck(
                    jun.rztTransferBarangDetails.data.items, "data"))
            },
            success: function (f, a) {
                //jun.TransferBarangstore.reload();
                jun.rztTransferBarang.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.closeForm) {
                    this.close();
                } else {
                    Ext.getCmp('form-TransferBarang').getForm().reset();
                    this.btnDisabled(false);
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
jun.TransferBarangOutWin = Ext.extend(Ext.Window, {
    title: "Transfer Barang Keluar",
    modez: 1,
    width: 605,
    height: 445,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-TransferBarangOut',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "hidden",
                        name: "format",
                        value: "excel",
                        ref: "../format"
                    },
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Invoice No:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref_other',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref_other',
                        id: 'doc_ref_otherid',
                        ref: '../doc_ref_other',
                        style: {textTransform: "uppercase"},
                        maxLength: 50,
                        width: 170,
                        x: 400,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Cabang :",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        emptyText: "Pilih Cabang",
                        hiddenName: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        value: STORE,
                        readOnly: !HEADOFFICE,
                        forceSelection: true,
                        width: 170,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'note',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        width: 170,
                        x: 400,
                        y: 62,
                        height: 22
                    },
                    new jun.TransferBarangDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 260,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: 'hidden',
                        name: 'type_',
                        value: 1
                    },
                    {
                        xtype: 'hidden',
                        id: 'totalpotid',
                        ref: '../totalpot',
                        name: 'totalpot'
                    }
                ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Simpan',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Print Bukti Kirim',
                    ref: '../btnKirim',
                    visible: !HEADOFFICE,
                    hidden : true
                },
                {
                    xtype: 'button',
                    text: 'Print Bukti Barang Keluar',
                    ref: '../btnPrintExcelBarangOut'
                },                
                {
                    xtype: 'button',
                    text: 'Simpan & Tutup',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Batal',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TransferBarangOutWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnKirim.on('click', this.buktikirim, this);
        this.btnPrintExcelBarangOut.on('click', this.printExcelBarangOut, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.on("close", this.onWinClose, this);
        this.on("activate", this.onActive, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        } else {
            this.btnKirim.setVisible(false);
            this.btnPrintExcelBarangOut.setVisible(false);
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(true);
        }
        this.tgl.setReadOnly(!EDIT_TGL);
    },
    onActive: function (t) {
        this.tgl.setReadOnly(!EDIT_TGL);
    },
    onWinClose: function () {
        jun.rztTransferBarangDetails.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    buktikirim: function () {
        Ext.getCmp("form-TransferBarangOut").getForm().standardSubmit = !0;
        Ext.getCmp("form-TransferBarangOut").getForm().url = "Report/PrintBuktiKirim";
        var form = Ext.getCmp('form-TransferBarangOut').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },
    printExcelBarangOut: function () {
        Ext.getCmp("form-TransferBarangOut").getForm().standardSubmit = !0;
        Ext.getCmp("form-TransferBarangOut").getForm().url = "Report/PrintExcelTransferBarangOut";
        var form = Ext.getCmp('form-TransferBarangOut').getForm();
        var el = form.getEl().dom;
        var target = document.createAttribute("target");
        target.nodeValue = "myFrame";
        el.setAttributeNode(target);
        el.action = form.url;
        el.submit();
    },    
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztTransferBarangDetails.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'TransferBarang/createout/';
        Ext.getCmp('form-TransferBarangOut').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                id: this.id,
                mode: this.modez,
                detil: Ext.encode(Ext.pluck(
                    jun.rztTransferBarangDetails.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztTransferBarangOut.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TransferBarangOut').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }
});
//jun.ReturnTransferBarangWin = Ext.extend(Ext.Window, {
//    title: 'Return Barang',
//    modez: 1,
//    width: 605,
//    height: 445,
//    layout: 'form',
//    modal: true,
//    padding: 5,
//    resizable: !1,
//    closeForm: false,
//    initComponent: function () {
//        this.items = [
//            {
//                xtype: 'form',
//                frame: false,
//                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
//                id: 'form-ReturnTransferBarang',
//                labelWidth: 100,
//                labelAlign: 'left',
//                layout: 'absolute',
//                anchor: "100% 100%",
//                ref: 'formz',
//                border: false,
//                items: [
//                    {
//                        xtype: "label",
//                        text: "Doc. Ref:",
//                        x: 5,
//                        y: 5
//                    },
//                    {
//                        xtype: 'uctextfield',
//                        fieldLabel: 'doc_ref',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'doc_ref',
//                        id: 'doc_refid',
//                        ref: '../doc_ref',
//                        maxLength: 50,
//                        x: 85,
//                        y: 2,
//                        height: 20,
//                        width: 175,
//                        readOnly: true
//                    },
//                    {
//                        xtype: "label",
//                        text: "Date:",
//                        x: 5,
//                        y: 35
//                    },
//                    {
//                        xtype: 'xdatefield',
//                        ref: '../tgl',
//                        fieldLabel: 'tgl',
//                        name: 'tgl',
//                        id: 'tglid',
//                        format: 'd M Y',
//                        readOnly: true,
//                        allowBlank: false,
//                        value: DATE_NOW,
//                        width: 175,
//                        x: 85,
//                        y: 32
//                    },
//                    {
//                        xtype: "label",
//                        text: "Supplier:",
//                        x: 5,
//                        y: 65
//                    },
//                    {
//                        xtype: 'combo',
//                        //typeAhead: true,
//                        ref: '../supplier',
//                        triggerAction: 'all',
//                        lazyRender: true,
//                        mode: 'local',
//                        store: jun.rztSupplierCmp,
//                        emptyText: "Please select supplier",
//                        hiddenName: 'supplier_id',
//                        valueField: 'supplier_id',
//                        displayField: 'supplier_name',
//                        allowBlank: true,
//                        width: 175,
//                        x: 85,
//                        y: 62
//                    },
//                    {
//                        xtype: "label",
//                        text: "Note:",
//                        x: 295,
//                        y: 5
//                    },
//                    {
//                        xtype: 'textarea',
//                        fieldLabel: 'note',
//                        hideLabel: false,
//                        enableKeyEvents: true,
//                        style: {textTransform: "uppercase"},
//                        listeners: {
//                            change: function (field, newValue, oldValue) {
//                                field.setValue(newValue.toUpperCase());
//                            }
//                        },
//                        name: 'note',
//                        id: 'noteid',
//                        ref: '../note',
//                        width: 170,
//                        x: 400,
//                        y: 2,
//                        height: 52
//                    },
//                    {
//                        xtype: "label",
//                        text: "Doc. Ref Purc:",
//                        x: 295,
//                        y: 65
//                    },
//                    {
//                        xtype: 'textfield',
//                        fieldLabel: 'doc_ref_other',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'doc_ref_other',
//                        id: 'doc_ref_otherid',
//                        ref: '../doc_ref_other',
//                        maxLength: 50,
//                        width: 170,
//                        x: 400,
//                        y: 62
//                    },
//                    new jun.TransferBarangDetailsGrid({
//                        x: 5,
//                        y: 95,
//                        height: 260,
//                        frameHeader: !1,
//                        header: !1
//                    }),
//                    {
//                        xtype: 'hidden',
//                        name: 'type_',
//                        value: 1
//                    },
//                    {
//                        xtype: 'hidden',
//                        id: 'totalpotid',
//                        ref: '../totalpot',
//                        name: 'totalpot'
//                    }
//                ]
//            }
//        ];
//        this.fbar = {
//            xtype: 'toolbar',
//            items: [
//                {
//                    xtype: 'button',
//                    text: 'Save',
//                    hidden: false,
//                    ref: '../btnSave'
//                },
//                {
//                    xtype: 'button',
//                    text: 'Save & Close',
//                    ref: '../btnSaveClose'
//                },
//                {
//                    xtype: 'button',
//                    text: 'Cancel',
//                    ref: '../btnCancel'
//                }
//            ]
//        };
//        jun.ReturnTransferBarangWin.superclass.initComponent.call(this);
//        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
//        this.btnSave.on('click', this.onbtnSaveclick, this);
//        this.btnCancel.on('click', this.onbtnCancelclick, this);
//        this.on("close", this.onWinClose, this);
//        if (this.modez == 1 || this.modez == 2) {
//            this.btnSave.setVisible(false);
//            this.btnSaveClose.setVisible(false);
//        } else {
//            this.btnSave.setVisible(true);
//            this.btnSaveClose.setVisible(true);
//        }
//    },
//
//    onDiscChange: function (txt, e) {
//        var disc1 = parseFloat(txt.getValue());
//        if (disc1 == 0) return;
//        jun.rztTransferBarangDetails.each(function (record) {
//            var barang = jun.getBarang(record.data.barang_id);
//            //var price = parseFloat(record.data.price);
//            var qty = parseFloat(record.data.qty);
//
//            record.commit();
//        });
//    },
//    onWinClose: function () {
//        jun.rztTransferBarangDetails.removeAll();
//        this.closeForm();
//    },
//    btnDisabled: function (status) {
//        this.btnSave.setDisabled(status);
//        this.btnSaveClose.setDisabled(status);
//    },
//    saveForm: function () {
//        this.btnDisabled(true);
//        if (jun.rztTransferBarangDetails.data.length == 0) {
//            Ext.Msg.alert('Error', "Item details must set");
//            this.btnDisabled(false);
//            return;
//        }
//        var urlz = 'TransferBarang/CreateOut/';
//        Ext.getCmp('form-ReturnTransferBarang').getForm().submit({
//            url: urlz,
//            scope: this,
//            params: {
//                detil: Ext.encode(Ext.pluck(
//                    jun.rztTransferBarangDetails.data.items, "data"))
//            },
//            success: function (f, a) {
//                jun.rztReturnTransferBarang.reload();
//                var response = Ext.decode(a.response.responseText);
//                Ext.MessageBox.show({
//                    title: 'Info',
//                    msg: response.msg,
//                    buttons: Ext.MessageBox.OK,
//                    icon: Ext.MessageBox.INFO
//                });
//                if (this.modez == 0) {
//                    Ext.getCmp('form-ReturnTransferBarang').getForm().reset();
//                    this.btnDisabled(false);
//                }
//                if (this.closeForm) {
//                    this.close();
//                }
//            },
//            failure: function (f, a) {
//                switch (a.failureType) {
//                    case Ext.form.Action.CLIENT_INVALID:
//                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
//                        break;
//                    case Ext.form.Action.CONNECT_FAILURE:
//                        Ext.Msg.alert('Failure', 'Ajax communication failed');
//                        break;
//                    case Ext.form.Action.SERVER_INVALID:
//                        Ext.Msg.alert('Failure', a.result.msg);
//                }
//                this.btnDisabled(false);
//            }
//
//        });
//    },
//    onbtnSaveCloseClick: function () {
//        this.closeForm = true;
//        this.saveForm(true);
//    },
//    onbtnSaveclick: function () {
//        this.closeForm = false;
//        this.saveForm(false);
//    },
//    onbtnCancelclick: function () {
//        this.close();
//    }
//});
