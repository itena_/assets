jun.LahaImportGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Laha Import",
    id: 'docs-jun.LahaImportGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    plugins: [new Ext.ux.grid.GridHeaderFilters],
    columns: [
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100,
            filter: {xtype: "textfield"}
        },
        {
            header: 'Tgl Pembuatan',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100,
            renderer: Ext.util.Format.dateRenderer('d/m/Y'),
            filter: {
                xtype: "xdatefield",
                format: 'd/m/Y'
            }
        },
        {
            header: 'Status',
            sortable: true,
            resizable: true,
            dataIndex: 'p',
            width: 50,
            renderer: function (value, metaData, record, rowIndex) {
                switch (Number(value)) {
                    case 0 :
                        return '';
                    case 1 :
                        metaData.style += "background-color: #B3FDB3;";
                        return 'POSTED';
                }
            },
            filter: {
                xtype: "combo",
                typeAhead: true,
                triggerAction: 'all',
                lazyRender: true,
                editable: false,
                mode: 'local',
                store: new Ext.data.ArrayStore({
                    id: 0,
                    fields: [
                        'myId',
                        'displayText'
                    ],
                    data: [['all', 'ALL'], [0, 'NOT POSTED'], [1, 'POSTED']]
                }),
                value: 'all',
                valueField: 'myId',
                displayField: 'displayText'
            }
        }
        /*  {
         header: 'p_d_kas_bank_id',
         sortable: true,
         resizable: true,
         dataIndex: 'p_d_kas_bank_id',
         width: 100
         },
         {
         header: 'p_d_kas_coa',
         sortable: true,
         resizable: true,
         dataIndex: 'p_d_kas_coa',
         width: 100
         },
         {
         header: 'p_d_piutangcard_coa',
         sortable: true,
         resizable: true,
         dataIndex: 'p_d_piutangcard_coa',
         width: 100
         },

         {
         header:'p_d_kas_n',
         sortable:true,
         resizable:true,
         dataIndex:'p_d_kas_n',
         width:100
         },
         {
         header:'p_d_piutangcard_n',
         sortable:true,
         resizable:true,
         dataIndex:'p_d_piutangcard_n',
         width:100
         },
         {
         header:'p_k_obat_coa',
         sortable:true,
         resizable:true,
         dataIndex:'p_k_obat_coa',
         width:100
         },
         {
         header:'p_k_jasa_coa',
         sortable:true,
         resizable:true,
         dataIndex:'p_k_jasa_coa',
         width:100
         },
         {
         header:'p_k_obat_n',
         sortable:true,
         resizable:true,
         dataIndex:'p_k_obat_n',
         width:100
         },
         {
         header:'p_k_jasa_n',
         sortable:true,
         resizable:true,
         dataIndex:'p_k_jasa_n',
         width:100
         },
         {
         header:'p_k_ppn_coa',
         sortable:true,
         resizable:true,
         dataIndex:'p_k_ppn_coa',
         width:100
         },
         {
         header:'p_k_ppn_n',
         sortable:true,
         resizable:true,
         dataIndex:'p_k_ppn_n',
         width:100
         },
         {
         header:'p_d_bank_bank_id',
         sortable:true,
         resizable:true,
         dataIndex:'p_d_bank_bank_id',
         width:100
         },
         {
         header:'p_d_feecard_coa',
         sortable:true,
         resizable:true,
         dataIndex:'p_d_feecard_coa',
         width:100
         },
         {
         header:'p_d_feecard_n',
         sortable:true,
         resizable:true,
         dataIndex:'p_d_feecard_n',
         width:100
         },
         {
         header:'p_k_piutangcard_coa',
         sortable:true,
         resizable:true,
         dataIndex:'p_k_piutangcard_coa',
         width:100
         },
         {
         header:'p_k_piutangcard_n',
         sortable:true,
         resizable:true,
         dataIndex:'p_k_piutangcard_n',
         width:100
         },
         {
         header:'p_d_bank_coa',
         sortable:true,
         resizable:true,
         dataIndex:'p_d_bank_coa',
         width:100
         },
         {
         header:'p_d_bank_n',
         sortable:true,
         resizable:true,
         dataIndex:'p_d_bank_n',
         width:100
         },
         {
         header:'b_d_total_biaya',
         sortable:true,
         resizable:true,
         dataIndex:'b_d_total_biaya',
         width:100
         },
         {
         header:'b_k_kas_bank_id',
         sortable:true,
         resizable:true,
         dataIndex:'b_k_kas_bank_id',
         width:100
         },
         {
         header:'b_k_pph21_coa',
         sortable:true,
         resizable:true,
         dataIndex:'b_k_pph21_coa',
         width:100
         },
         {
         header:'b_k_pph22_coa',
         sortable:true,
         resizable:true,
         dataIndex:'b_k_pph22_coa',
         width:100
         },
         {
         header:'b_k_pph23_coa',
         sortable:true,
         resizable:true,
         dataIndex:'b_k_pph23_coa',
         width:100
         },
         {
         header:'b_k_pph4_2_coa',
         sortable:true,
         resizable:true,
         dataIndex:'b_k_pph4_2_coa',
         width:100
         },
         {
         header:'b_k_kas_coa',
         sortable:true,
         resizable:true,
         dataIndex:'b_k_kas_coa',
         width:100
         },
         {
         header:'b_k_kas_n',
         sortable:true,
         resizable:true,
         dataIndex:'b_k_kas_n',
         width:100
         },
         {
         header:'b_k_pph21_n',
         sortable:true,
         resizable:true,
         dataIndex:'b_k_pph21_n',
         width:100
         },
         {
         header:'b_k_pph22_n',
         sortable:true,
         resizable:true,
         dataIndex:'b_k_pph22_n',
         width:100
         },
         {
         header:'b_k_pp23_n',
         sortable:true,
         resizable:true,
         dataIndex:'b_k_pp23_n',
         width:100
         },
         {
         header:'b_k_pph4_2_n',
         sortable:true,
         resizable:true,
         dataIndex:'b_k_pph4_2_n',
         width:100
         },
         {
         header:'s_d_bank_bank_id',
         sortable:true,
         resizable:true,
         dataIndex:'s_d_bank_bank_id',
         width:100
         },
         {
         header:'s_d_bank_coa',
         sortable:true,
         resizable:true,
         dataIndex:'s_d_bank_coa',
         width:100
         },
         {
         header:'s_d_bank_n',
         sortable:true,
         resizable:true,
         dataIndex:'s_d_bank_n',
         width:100
         },
         {
         header:'s_k_kas_bank_id',
         sortable:true,
         resizable:true,
         dataIndex:'s_k_kas_bank_id',
         width:100
         },
         {
         header:'s_k_kas_coa',
         sortable:true,
         resizable:true,
         dataIndex:'s_k_kas_coa',
         width:100
         },
         {
         header:'s_k_kas_n',
         sortable:true,
         resizable:true,
         dataIndex:'s_k_kas_n',
         width:100
         },
         */
    ],
    initComponent: function () {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        if (jun.rztBankTransCmpPusat.getTotalCount() === 0) {
            jun.rztBankTransCmpPusat.load();
        }
        if (jun.rztChartMasterCmp.getTotalCount() === 0) {
            jun.rztChartMasterCmp.load();
        }
//        jun.rztLahaImport.on({
//            scope: this,
//            beforeload: {
//                fn: function (a, b) {
//                    var tgl = Ext.getCmp('tgllahaimportgridid');
//                    b.params.tgl = tgl.hiddenField.dom.value;
//                    //var tgl = Ext.getCmp('tglsalesgridid').getValue();
//                    //b.params.tgl = tgl;
//                    b.params.mode = "grid";
//                }
//            }
//        });
        this.store = jun.rztLahaImport;
        this.bbar = {
            items: [
                {
                    xtype: 'paging',
                    store: this.store,
                    displayInfo: true,
                    pageSize: 20
                }]
        };
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Tambah Laha Import',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Ubah Laha Import',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'Unposting Laha Import',
                    ref: '../btnUnPosting'
                }
//                {
//                    xtype: 'tbseparator'
//                },
//                {
//                    xtype: 'xdatefield',
//                    id: 'tgllahaimportgridid',
//                    ref: '../tgl'
//                }
            ]
        };
//        this.store.baseParams = {mode: "grid"};
//        this.store.reload();
//        this.store.baseParams = {};
        jun.LahaImportGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.btnUnPosting.on('Click', this.unPostingRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
//        this.tgl.on('select', this.refreshTgl, this);
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.LahaImportWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih Jenis Pelayanan");
            return;
        }
        var idz = selectedz.json.laha_import_id;
        var form = new jun.LahaImportWin({modez: (selectedz.json.p == '0' ? 1 : 2), id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        jun.rztLahaImportBiaya.load({
            params: {
                laha_import_id: idz
            }
        });
        jun.rztLahaImportBankFee.load({
            params: {
                laha_import_id: idz,
                type_: 0
            }
        });
        jun.rztLahaImportBankSetor.load({
            params: {
                laha_import_id: idz,
                type_: 1
            }
        });
    },
    unPostingRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus unposting data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'LahaImport/unposting/id/' + record.json.laha_import_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztLahaImport.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.LahaImportCekBalanceGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Cek Balance",
    id: 'docs-jun.LahaImportCekBalanceGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    view: new Ext.grid.GroupingView({
        forceFit: true,
        groupTextTpl: '{text}',
        showGroupName: true,
        enableNoGroups: false,
        enableGroupingMenu: false,
        hideGroupedColumn: true
    }),
    plugins: new Ext.ux.grid.GroupSummary(this),
    columns: [
        {
            header: 'Grup',
            sortable: true,
            resizable: true,
            dataIndex: 'grup',
            width: 100
        },
        {
            header: 'COA',
            sortable: true,
            resizable: true,
            dataIndex: 'coa',
            width: 100
        },
        {
            header: 'Nama Akun',
            sortable: true,
            resizable: true,
            dataIndex: 'coa',
            renderer: jun.renderChartMaster
        },
        {
            header: 'Debet',
            sortable: true,
            resizable: true,
            dataIndex: 'd',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            summaryType: 'sum'
        },
        {
            header: 'Kredit',
            sortable: true,
            resizable: true,
            dataIndex: 'k',
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00"),
            summaryType: 'sum'
        }
    ],
    initComponent: function () {
        this.store = jun.rztCekBalance;
        jun.LahaImportCekBalanceGrid.superclass.initComponent.call(this);
    }
});
