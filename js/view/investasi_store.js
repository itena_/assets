jun.Investasistore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Investasistore.superclass.constructor.call(this, Ext.apply({
            storeId: 'InvestasiStoreId',
            url: 'projection/Investasi',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'investasi_id'},
                {name:'account_id'},
                {name:'account_code'},
                {name:'account_name'},
                {name:'name'},
                {name:'description'},
                {name:'qty'},
                {name:'amount'},
                {name:'tdate'},
                {name:'tdatef'},
                {name:'total'},
                {name:'created_at'},
                {name:'businessunit_id'},
                {name:'uploadfile'},
                {name:'uploaddate'},
            ]
        }, cfg));
    }
});
jun.rztInvestasi = new jun.Investasistore();
//jun.rztInvestasi.load();
