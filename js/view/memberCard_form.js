jun.MemberCardWin = Ext.extend(Ext.Window, {
    title: 'Print Card',
    modez:1,
    width: 438,
    height: 576,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: true,
    initComponent: function() {
        if (jun.rztStoreCmp.getTotalCount() === 0) {
            jun.rztStoreCmp.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-MemberCard',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Customer ID',
                        hideLabel:false,
                        hidden:true,
                        name:'customer_id',
                        id:'customer_idid',
                        ref:'../customer_id',
                        maxLength: 10,
                        allowBlank: false,
                        anchor: '100%',

                    },
                    {
                                    xtype: 'textfield',
                                    fieldLabel: 'No. Customer',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'no_card',
                                    id:'no_cardid',
                                    ref:'../no_card',
                                    maxLength: 10,
                                    allowBlank: false,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'Name',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'name',
                                    id:'nameid',
                                    ref:'../name',
                                    maxLength: 20,
                                    allowBlank: false,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                },
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        fieldLabel: 'Branch',
                        id:'storeid',
                        ref:'../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        emptyText: "All Branch",
                        value: STORE,
                        //emptyText: "Asset Name",
                        readOnly: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Category',
                        ref: '../category',
                        hiddenName: 'category',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['WOMAN', 'WOMAN'],
                                ['MEN', 'MEN'],
                                ['GIRL', 'GIRL'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Category",
                        mode : 'local',
                        allowBlank: true,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Type',
                        ref: '../type',
                        hiddenName: 'type',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['EC', 'EC'],
                                ['ECC', 'ECC'],
                                ['ECS', 'ECS'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Type",
                        mode : 'local',
                        allowBlank: true,
                        anchor: '100%'
                    },
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../since',
                            fieldLabel: 'Since',
                            name:'since',
                            id:'sinceid',
                            format: 'd M Y',
                            value: DATE_NOW,
                            width: 200,
                            //allowBlank: ,
                            anchor: '100%'                            
                        }, 
                                                                     {
                            xtype: 'xdatefield',
                            ref:'../valid',
                            fieldLabel: 'Valid',
                            name:'valid',
                            id:'validid',
                            format: 'd M Y',
                            value: DATE5YEAR,
                            width: 200,
                            //allowBlank: ,
                            anchor: '100%'                            
                        },
                    {
                        xtype: 'combo',
                        fieldLabel: 'Active',
                        ref: '../active',
                        hiddenName: 'active',
                        store: new Ext.data.SimpleStore({
                            data: [
                                ['Y', 'Yes'],
                                ['N', 'No'],
                            ],
                            id: 0,
                            fields: ['value', 'text']
                        }),
                        valueField: 'value',
                        displayField: 'text',
                        triggerAction: 'all',
                        editable: false,
                        emptyText: "Active",
                        mode : 'local',
                        allowBlank: false,
                        anchor: "50%",
                    },
                    {
                        width: '100%',
                        height: 245,
                        layout: 'hbox',
                        layoutConfig: {
                            pack: 'center',
                            align: 'middle'
                        },
                        defaults:{
                            margins: '0px 0px 0px 0px',
                            width: 172+50,
                            height: 212+45
                        },
                        items: [
                            {

                                width: 480,
                                height: 267,
                                html: [
                                    '<html>',
                                        '<head>',

                                            '<link rel="stylesheet" type="text/css" href="'+STYLEPREVIEW+'">',
                                            '<script type="text/javascript" src="js/barcode.js"></script>',
                                        '</head>',
                                        '<body>',
                                            '<div id="docs-jun.MemberCardWinPreview" class="MemberCardForm_PhotoPreview" >',
                                                CARDPREVIEW,
                                                '<div id="nocustomer" class="nocustomer" style="alignment: center">***NOCARD*** </div>',
                                                '<div id="nama" class="nama" style="alignment: center">***NAME***</div>',
                                                '<div id="since" class="since" style="alignment: center">****</div>',
                                                '<div id="valid" class="valid" style="alignment: center">**/**** </div>',
                                                '<div class="rectangle"></div>',
                                                '<svg xmlns="http://www.w3.org/2000/svg" class="barcode" jsbarcode-format="CODE128" jsbarcode-value="123" jsbarcode-margin="0" jsbarcode-height= "10" jsbarcode-width= "1" jsbarcode-textmargin="0" jsbarcode-displayValue="true" ></svg>',
                                            '</div>',
                                        '</body>',
                                        '<script>JsBarcode(".barcode").init();</script>',
                                    '</html>'
                                ],
                                ref: '../../cardPreview',
                            },
                        ]
                    },
                    /*{
                        xtype: 'hidden',
                        html: '<input type="file" id="docs-jun.MemberCardWinBrowse" style="display: none" accept="image/!*">'
                    }*/
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Print',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Edit',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.MemberCardWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnEdit.on('click', this.onEdit, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnEdit.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
        if (this.modez == 0) {
            this.btnEdit.setVisible(false);
        }
        if (this.modez == 3) {
            this.btnEdit.setVisible(true);
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        }

        this.no_card.on('change', this.no_cardchange, this);
        this.name.on('change', this.namechange, this);
        this.store.on('select', this.storeselect, this);
        this.since.on('select', this.sinceselect, this);
        this.valid.on('select', this.validselect, this);

        this.on("activate", this.onActivate, this);
    },
    onActivate: function ()
    {
        if(PT_CARD == "ENA")
        {
            document.getElementById("nocustomer").textContent=this.no_card.getValue();
        }
        else
        {
            document.getElementById("nocustomer").textContent=this.no_card.getValue()+this.store.getValue();
        }
        document.getElementById("nama").textContent=this.name.getValue();

        var myyear = this.since.getValue().getFullYear(); // year
        document.getElementById("since").textContent=myyear;

        var month = this.valid.getValue().getMonth(); // month (in integer 0-11)
        var year = this.valid.getValue().getFullYear(); // year
        document.getElementById("valid").textContent=month+'/'+year;
    },

    no_cardchange: function(c,r,i)
    {
        if(PT_CARD == "ENA")
        {
            document.getElementById("nocustomer").textContent=this.no_card.getValue();
        }
        else
        {
            document.getElementById("nocustomer").textContent=this.no_card.getValue()+this.store.getValue();
        }
    },
    namechange: function(c,r,i)
    {
        document.getElementById("nama").textContent=this.name.getValue();
    },
    storeselect: function(c,r,i)
    {
        if(PT_CARD == "ENA")
        {
            document.getElementById("nocustomer").textContent=this.no_card.getValue();
        }
        else
        {
            document.getElementById("nocustomer").textContent=this.no_card.getValue()+this.store.getValue();
        }
    },
    sinceselect: function(c,r,i)
    {
        var myyear = this.since.getValue().getFullYear(); // year
        document.getElementById("since").textContent=myyear;
    },
    validselect: function(c,r,i)
    {
        var month = this.valid.getValue().getMonth(); // month (in integer 0-11)
        var year = this.valid.getValue().getFullYear(); // year
        document.getElementById("valid").textContent=month+'/'+year;
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);

        Ext.Ajax.request({
            url: 'MemberCard/create/',
            method: 'POST',
            //timeOut: 1000,
            scope: this,
            params: {
                id: this.id,
                customer_id: this.customer_id.getValue(),
                mode: this.modez,
                name:this.name.getValue(),
                no_card:this.no_card.getValue(),
                store:this.store.getValue(),
                category:this.category.getValue(),
                type:this.type.getValue(),
                since:this.since.getValue(),
                valid:this.valid.getValue(),
                active:this.active.getValue(),
            },
            success: function (f, a) {

                var response = Ext.decode(f.responseText);

                if(!response.success)
                {
                    alert(response.msg);
                    if (confirm("do you want to print again ?"))
                    {

                        var printWindow = window.open('', '', 'height=0px ,width=0px');

                        printWindow.document.write('<html>');
                        printWindow.document.write('<head>');
                        printWindow.document.write('<script src="js/barcode.js"></script>');
                        //printWindow.document.write('<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/barcodes/JsBarcode.code128.min.js"></script>');

                        printWindow.document.write('<link rel="stylesheet" type="text/css" href="'+STYLECARD+'">');

                        printWindow.document.write('</head>');
                        printWindow.document.write('<body>');
                        printWindow.document.write('<div class="mycard">');

                        printWindow.document.write('<div class="nama" style="">');
                        printWindow.document.write(response.MemberCard.name);
                        printWindow.document.write('</div>');

                        //printWindow.document.write('<svg class="barcode" jsbarcode-format="code128" jsbarcode-value="123JOG02" jsbarcode-textmargin="0" jsbarcode-fontoptions="bold" jsbarcode-displayValue="false" jsbarcode-height= "30" jsbarcode-width= "2"></svg>');
                        if (PT_CARD == "ENA") {

                            printWindow.document.write('<div class="nocustomer">');
                            printWindow.document.write(response.MemberCard.no_card);
                            printWindow.document.write('</div>');

                            printWindow.document.write('<svg class="barcode" jsbarcode-format="CODE128" jsbarcode-value="' + response.MemberCard.no_card +  '"jsbarcode-margin="0" jsbarcode-height= "60" jsbarcode-width= "1" jsbarcode-textmargin="0" jsbarcode-fontoptions="" jsbarcode-displayValue="false" ></svg>');
                        }
                        else {
                            printWindow.document.write('<div class="nocustomer">');
                            printWindow.document.write(response.MemberCard.no_card + response.MemberCard.store);
                            printWindow.document.write('</div>');

                            printWindow.document.write('<div class="since">');
                            printWindow.document.write(response.yawal);
                            printWindow.document.write('</div>');

                            printWindow.document.write('<div class="valid">');
                            printWindow.document.write(response.mawal + "/" + response.yakhir);
                            printWindow.document.write('</div>');
                            printWindow.document.write('<svg class="barcode" jsbarcode-format="CODE128" jsbarcode-value="' + response.MemberCard.no_card + response.MemberCard.store + '"jsbarcode-margin="0" jsbarcode-height= "30" jsbarcode-width= "2" jsbarcode-textmargin="0" jsbarcode-fontoptions="" jsbarcode-displayValue="false" ></svg>');
                        }
                        //printWindow.document.write('<svg class="barcode" jsbarcode-format="code128" jsbarcode-value="2101100115" jsbarcode-textmargin="0" jsbarcode-margin="0" jsbarcode-height= "60" jsbarcode-width= "2" jsbarcode-fontoptions="" jsbarcode-displayValue="false" ></svg>');
                        printWindow.document.write('</div>');
                        printWindow.document.write('</body>');
                        printWindow.document.write('<script>JsBarcode(".barcode").init();</script>');
                        printWindow.document.write('</html>');

                        setTimeout(function () {
                            printWindow.print();
                            printWindow.close();
                        }, 500);
                        //return false;

                        if (this.modez == 0 || this.modez == 1 || this.modez == 2) {
                            Ext.getCmp('form-MemberCard').getForm().reset();
                            this.btnDisabled(false);
                        }
                        if (this.closeForm) {
                            this.close();
                        }


                        Ext.Ajax.request({
                            url: 'MemberCard/UpdateDataCard/',
                            method: 'POST',
                            scope: this,
                            params: {
                                id: response.MemberCard.membercard_id,
                                mode: this.modez,

                            },
                            success: function (data) {}
                        });
                    }
                }
                else {


                    if (confirm("Are you sure you want to print the card ?")) {
                        var printWindow = window.open('', '', 'height=0px ,width=0px');

                        printWindow.document.write('<html>');
                        printWindow.document.write('<head>');
                        printWindow.document.write('<script src="js/barcode.js"></script>');
                        //printWindow.document.write('<script src="https://cdn.jsdelivr.net/jsbarcode/3.6.0/barcodes/JsBarcode.code128.min.js"></script>');

                        printWindow.document.write('<link rel="stylesheet" type="text/css" href="'+STYLECARD+'">');

                        printWindow.document.write('</head>');
                        printWindow.document.write('<body>');
                        printWindow.document.write('<div class="mycard">');



                        printWindow.document.write('<div class="nama" style="">');
                        printWindow.document.write(response.MemberCard.name);
                        printWindow.document.write('</div>');

                        //printWindow.document.write('<svg class="barcode" jsbarcode-format="code128" jsbarcode-value="123JOG02" jsbarcode-textmargin="0" jsbarcode-fontoptions="bold" jsbarcode-displayValue="false" jsbarcode-height= "30" jsbarcode-width= "2"></svg>');
                        if (PT_CARD == "ENA") {
                            printWindow.document.write('<div class="nocustomer">');
                            printWindow.document.write(response.MemberCard.no_card);
                            printWindow.document.write('</div>');

                            printWindow.document.write('<svg class="barcode" jsbarcode-format="CODE128" jsbarcode-value="' + response.MemberCard.no_card +  '"jsbarcode-margin="0" jsbarcode-height= "60" jsbarcode-width= "2" jsbarcode-textmargin="0" jsbarcode-fontoptions="" jsbarcode-displayValue="false" ></svg>');
                        }
                        else {

                            printWindow.document.write('<div class="nocustomer">');
                            printWindow.document.write(response.MemberCard.no_card + response.MemberCard.store);
                            printWindow.document.write('</div>');

                            printWindow.document.write('<div class="since">');
                            printWindow.document.write(response.yawal);
                            printWindow.document.write('</div>');

                            printWindow.document.write('<div class="valid">');
                            printWindow.document.write(response.mawal + "/" + response.yakhir);
                            printWindow.document.write('</div>');
                            printWindow.document.write('<svg class="barcode" jsbarcode-format="CODE128" jsbarcode-value="' + response.MemberCard.no_card + response.MemberCard.store + '"jsbarcode-margin="0" jsbarcode-height= "30" jsbarcode-width= "2" jsbarcode-textmargin="0" jsbarcode-fontoptions="" jsbarcode-displayValue="false" ></svg>');
                        }
                        //printWindow.document.write('<svg class="barcode" jsbarcode-format="code128" jsbarcode-value="2101100115" jsbarcode-textmargin="0" jsbarcode-margin="0" jsbarcode-height= "60" jsbarcode-width= "2" jsbarcode-fontoptions="" jsbarcode-displayValue="false" ></svg>');
                        printWindow.document.write('</div>');
                        printWindow.document.write('</body>');
                        printWindow.document.write('<script>JsBarcode(".barcode").init();</script>');
                        printWindow.document.write('</html>');

                        setTimeout(function () {
                            printWindow.print();
                            printWindow.close();
                        }, 500);
                        //return false;

                        if (this.modez == 0 || this.modez == 1 || this.modez == 2) {
                            Ext.getCmp('form-MemberCard').getForm().reset();
                            this.btnDisabled(false);
                        }
                        if (this.closeForm) {
                            this.close();
                        }
                    }
                    else {
                        alert('Print Card Cancelled.', 'Print Card Cancelled.');
                    }

                    Ext.getCmp('form-MemberCard').getForm().reset();
                }

            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
        /*this.btnDisabled(true);
            var urlz= 'MemberCard/create/';
            /!*if(this.modez == 1 || this.modez== 2) {
                    
                    urlz= 'MemberCard/update/id/' + this.id;
                    
                } else {
                    
                    urlz= 'MemberCard/create/';
                }*!/
             
            Ext.getCmp('form-MemberCard').getForm().submit({
                url:urlz,
                timeOut: 1000,
                scope: this,
                params: {
                    id: this.id,
                    mode: this.modez
                },
                success: function(f,a){
                    jun.rztMemberCard.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-MemberCard').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });*/
    },

    onEdit: function()
    {
        Ext.Ajax.request({
            url: 'MemberCard/Update/id/' + this.memberid,
            method: 'POST',
            scope: this,
            params: {
                id: this.id,
                customer_id: this.customer_id.getValue(),
                mode: this.modez,
                name:this.name.getValue(),
                no_card:this.no_card.getValue(),
                store:this.store.getValue(),
                category:this.category.getValue(),
                type:this.type.getValue(),
                since:this.since.getValue(),
                valid:this.valid.getValue(),
                active:this.active.getValue(),
            },
            success:function (f, a) {
                jun.rztMemberCard.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                });
                this.closeForm = true;
            },
            failure:function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });

        //this.saveForm(true);
    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});