jun.PurchasingGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Purchasing",
    id: 'docs-jun.TransferItemGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangNonJasaAll.getTotalCount() === 0) {
            jun.rztBarangNonJasaAll.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        jun.purchaseItem.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    //b.params.tgl = Ext.getCmp('tgltransferitemgrid').getValue();
                    var tgl = Ext.getCmp('tgltransferitemgrid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.purchaseItem;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Purchasing',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Purchasing',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: 'Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tgltransferitemgrid'
                }
            ]
        };
        jun.PurchasingGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.PurchasingWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.purchase_coa_id;
        var form = new jun.PurchasingWin({modez: 1, id: idz});
        form.show(this);

        form.formz.getForm().loadRecord(this.record);
        form.nominal.setValue(this.record.data.total);

        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
//        jun.rztPurchasingCoaDetail.proxy.setUrl('TransferItemDetails/IndexIn');
        jun.rztPurchasingCoaDetail.baseParams = {
            purchase_coa_id: idz
        };
        jun.rztPurchasingCoaDetail.load();
        jun.rztPurchasingCoaDetail.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'TransferItem/delete/id/' + record.json.transfer_item_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztTransferItem.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.ReturnTransferItemGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "Return Supplier Item",
    id: 'docs-jun.ReturnTransferItemGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Date',
            sortable: true,
            resizable: true,
            dataIndex: 'tgl',
            width: 100
        },
        {
            header: 'Doc. Ref',
            sortable: true,
            resizable: true,
            dataIndex: 'doc_ref',
            width: 100
        },
        {
            header: 'Note',
            sortable: true,
            resizable: true,
            dataIndex: 'note',
            width: 100
        },
        {
            header: 'Branch',
            sortable: true,
            resizable: true,
            dataIndex: 'store',
            width: 100
        }
    ],
    initComponent: function () {
        if (jun.rztBarangLib.getTotalCount() === 0) {
            jun.rztBarangLib.load();
        }
        if (jun.rztBarangNonJasa.getTotalCount() === 0) {
            jun.rztBarangNonJasa.load();
        }
        if (jun.rztSupplierCmp.getTotalCount() === 0) {
            jun.rztSupplierCmp.load();
        }
        if (jun.rztGrup.getTotalCount() === 0) {
            jun.rztGrup.load();
        }
        jun.rztReturnTransferItem.on({
            scope: this,
            beforeload: {
                fn: function (a, b) {
                    //b.params.tgl = Ext.getCmp('tglreturntransferitemgrid').getValue();
                    var tgl = Ext.getCmp('tglreturntransferitemgrid');
                    b.params.tgl = tgl.hiddenField.dom.value;
                    b.params.mode = "grid";
                }
            }
        });
        this.store = jun.rztReturnTransferItem;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Add Return Item',
                    ref: '../btnAdd'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'button',
                    text: 'View Return Item',
                    ref: '../btnEdit'
                },
                {
                    xtype: 'tbseparator'
                },
                {
                    xtype: 'label',
                    style: 'margin:5px',
                    text: '\xA0Date :'
                },
                {
                    xtype: 'xdatefield',
                    ref: '../tgl',
                    id: 'tglreturntransferitemgrid'
                }
            ]
        };
        jun.ReturnTransferItemGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.tgl.on('select', this.refreshTgl, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.store.removeAll();
    },
    refreshTgl: function () {
        this.store.reload();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.ReturnTransferItemWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        //var dodol = this.store.getAt(0);
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a transaction");
            return;
        }
        var idz = selectedz.json.transfer_item_id;
        var form = new jun.ReturnTransferItemWin({modez: 1, id: idz});
        form.show(this);
        form.formz.getForm().loadRecord(this.record);
        //form.tgl.setValue(Date.parseDate(this.record.data.tgl, 'Y-m-d H:i:s'));
        jun.rztPurchasingCoaDetail.proxy.setUrl('TransferItemDetails/IndexOut');
        jun.rztPurchasingCoaDetail.baseParams = {
            transfer_item_id: idz
        };
        jun.rztPurchasingCoaDetail.load();
        jun.rztPurchasingCoaDetail.baseParams = {};
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'ReturnTransferItem/delete/id/' + record.json.transfer_item_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztReturnTransferItem.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.PurchasingWin = Ext.extend(Ext.Window, {
    title: 'Purchasing',
    modez: 1,
    width: 605,
    height: 510,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-TransferItem',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Due Date:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl_jatuh_tempo',
                        fieldLabel: 'tgl',
                        name: 'tgl_jatuh_tempo',
                        id: 'tgl_jatuh_tempoid',
                        format: 'd M Y',
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Store:",
                        x: 5,
                        y: 95
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        ref: '../store',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        valueField: 'store_kode',
                        value:STORE,
                        displayField: 'store_kode',
                        allowBlank: true,
                        listWidth: 350,
                        matchFieldWidth: !1,
                        width: 175,
                        x: 85,
                        y: 93
                    },
                    {
                        xtype: "label",
                        text: "Invoice No:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref_other',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref_other',
                        id: 'doc_ref_otherid',
                        ref: '../doc_ref_other',
                        maxLength: 50,
                        width: 170,
                        x: 400,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Supplier:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        ref: '../supplier',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztSupplierCmp,
                        emptyText: "Please select supplier",
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        allowBlank: true,
                        width: 170,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 65
                    },

                    {
                        xtype: 'textarea',
                        fieldLabel: 'note',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        width: 170,
                        x: 400,
                        y: 62,
                        height: 22
                    },
                    {
                        xtype: "label",
                        text: "Nominal:",
                        x: 295,
                        y: 95
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'nominal',
                        hideLabel: false,
                        //hidden:true,
                        name: 'nominal',
                        id: 'nominal',
                        ref: '../nominal',
                        maxLength: 50,
                        width: 170,
                        x: 400,
                        y: 92
                    },
                    new jun.PurchasingDetailsGrid({
                        x: 5,
                        y: 125,
                        height: 260,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 355,
                        y: 400
                    },
                    {
                        xtype: 'numericfield',
                        fieldLabel: 'Total',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total',
                        id: 'totalcoa',
                        ref: '../total',
                        maxLength: 50,
                        x: 400,
                        y: 398,
                        height: 20,
                        width: 171,
                        readOnly: true
                    },
                    {
                        xtype: 'hidden',
                        name: 'type_',
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        id: 'totalpotid',
                        ref: '../totalpot',
                        name: 'totalpot'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.TransferItemWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
//        this.disc.on('keyup', this.onDiscChange, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }
        //this.setDateTime();
    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    onDiscChange: function (txt, e) {
        var disc1 = parseFloat(txt.getValue());
        if (disc1 == 0)
            return;
        jun.rztPurchasingCoaDetail.each(function (record) {
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = round(price * qty, 2);
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
            var totalpot = discrp + discrp1;
            var total = bruto - totalpot;
            var vatrp = round(total * vat, 2);
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
    },
    onWinClose: function () {
        jun.rztPurchasingCoaDetail.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztPurchasingCoaDetail.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'Purchasing/createin/';
        Ext.getCmp('form-TransferItem').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                        jun.rztPurchasingCoaDetail.data.items, "data"))
            },
            success: function (f, a) {
//                jun.rztTransferBarangMasuk.reload();
                jun.rztBarang.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-TransferItem').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
                jun.rztPurchasingCoaDetail.removeAll();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        if (this.nominal.getValue() === this.total.getValue()) {
            this.closeForm = true;
            this.saveForm(true);
        } else
            Ext.Msg.alert('Jumlah Nominal tidak sesuai dengan Total harga COA');

    },
    onbtnSaveclick: function () {
        if (this.nominal.getValue() === this.total.getValue()) {
            this.closeForm = false;
            this.saveForm(false);
//            jun.rztPurchasingCoaDetail.removeAll();
        } else
            Ext.Msg.alert('Jumlah Nominal tidak sesuai dengan Total harga COA');


    },
    onbtnCancelclick: function () {
        this.close();
    }

});
jun.ReturnTransferItemWin = Ext.extend(Ext.Window, {
    title: 'Return Supplier Item',
    modez: 1,
    width: 605,
    height: 445,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-ReturnTransferItem',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Doc. Ref:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'doc_ref',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 50,
                        x: 85,
                        y: 2,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'tgl',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        width: 175,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Supplier:",
                        x: 5,
                        y: 65
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        ref: '../supplier',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztSupplierCmp,
                        emptyText: "Please select supplier",
                        hiddenName: 'supplier_id',
                        valueField: 'supplier_id',
                        displayField: 'supplier_name',
                        allowBlank: true,
                        width: 175,
                        x: 85,
                        y: 62
                    },
                    {
                        xtype: "label",
                        text: "Note:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'note',
                        hideLabel: false,
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'note',
                        id: 'noteid',
                        ref: '../note',
                        width: 170,
                        x: 400,
                        y: 2,
                        height: 52
                    },
                    {
                        xtype: "label",
                        text: "Doc. Ref Purc:",
                        x: 295,
                        y: 65
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'doc_ref_other',
                        hideLabel: false,
                        //hidden:true,
                        name: 'doc_ref_other',
                        id: 'doc_ref_otherid',
                        ref: '../doc_ref_other',
                        maxLength: 50,
                        width: 170,
                        x: 400,
                        y: 62
                    },
                    new jun.TransferItemDetailsGrid({
                        x: 5,
                        y: 95,
                        height: 260,
                        frameHeader: !1,
                        header: !1
                    }),
//                    {
//                        xtype: "label",
//                        text: "Disc:",
//                        x: 5,
//                        y: 365
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        enableKeyEvents: true,
//                        name: 'disc',
//                        id: 'discid',
//                        ref: '../disc',
//                        maxLength: 3,
//                        value: 0,
//                        minValue: 0,
//                        maxValue: 100,
//                        width: 175,
//                        x: 85,
//                        y: 362
//                    },
//                    {
//                        xtype: "label",
//                        text: "Disc  Amount:",
//                        x: 5,
//                        y: 395
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        readOnly: true,
//                        name: 'discrp',
//                        id: 'discrptransferouid',
//                        ref: '../discrp',
//                        maxLength: 30,
//                        value: 0,
//                        width: 175,
//                        x: 85,
//                        y: 392
//                    },
//                    {
//                        xtype: "label",
//                        text: "Sub Total:",
//                        x: 295,
//                        y: 365
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'bruto',
//                        id: 'subtotaltransferoutid',
//                        ref: '../subtotal',
//                        maxLength: 30,
//                        value: 0,
//                        readOnly: true,
//                        width: 170,
//                        x: 400,
//                        y: 362
//                    },
//                    {
//                        xtype: "label",
//                        text: "VAT:",
//                        x: 5,
//                        y: 395
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'vat',
//                        id: 'vattransferitemid',
//                        ref: '../vat',
//                        maxLength: 30,
//                        value: 0,
//                        readOnly: true,
//                        width: 175,
//                        x: 85,
//                        y: 392
//                    },
//                    {
//                        xtype: "label",
//                        text: "Total:",
//                        x: 295,
//                        y: 395
//                    },
//                    {
//                        xtype: 'numericfield',
//                        hideLabel: false,
//                        //hidden:true,
//                        name: 'total',
//                        id: 'totaltransferoutid',
//                        ref: '../total',
//                        maxLength: 30,
//                        value: 0,
//                        readOnly: true,
//                        width: 170,
//                        x: 400,
//                        y: 392
//                    },
                    {
                        xtype: 'hidden',
                        name: 'type_',
                        value: 1
                    },
                    {
                        xtype: 'hidden',
                        id: 'totalpotid',
                        ref: '../totalpot',
                        name: 'totalpot'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.ReturnTransferItemWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
//        this.disc.on('keyup', this.onDiscChange, this);
        this.on("close", this.onWinClose, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
        }
        //this.setDateTime();
    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    onDiscChange: function (txt, e) {
        var disc1 = parseFloat(txt.getValue());
        if (disc1 == 0)
            return;
        jun.rztPurchasingCoaDetail.each(function (record) {
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = round(price * qty, 2);
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
            var totalpot = discrp + discrp1;
            var total = bruto - totalpot;
            var vatrp = round(total * vat, 2);
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
    },
    onWinClose: function () {
        jun.rztPurchasingCoaDetail.removeAll();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        if (jun.rztPurchasingCoaDetail.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'TransferItem/CreateOut/';
        Ext.getCmp('form-ReturnTransferItem').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                        jun.rztTransferItemDetails.data.items, "data"))
            },
            success: function (f, a) {
                jun.rztReturnTransferItem.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.modez == 0) {
                    Ext.getCmp('form-ReturnTransferItem').getForm().reset();
                    this.btnDisabled(false);
                }
                if (this.closeForm) {
                    this.close();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }

        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        this.close();
    }

});
jun.PurchasingDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PurchasingDetailsGrid",
    id: 'docs-jun.PurchasingDetailsGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'COA',
            resizable: true,
            dataIndex: 'account_code',
            menuDisabled: true,
            width: 100
        },
        {
            header: 'Note',
            resizable: true,
            dataIndex: 'item_name',
            menuDisabled: true,
            width: 100
        },
        {
            header: 'Total',
            resizable: true,
            dataIndex: 'total',
            menuDisabled: true,
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        }
    ],
    initComponent: function () {
        this.store = jun.rztPurchasingCoaDetail;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'COA :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            forceSelection: true,
                            fieldLabel: 'COA',
                            //store: this.mode,
                            store: jun.rztChartMasterCmp,
                            ref: '../../account_code',
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                                    "</div></tpl>"),
                            listWidth: 500,
                            hiddenName: 'account_code',
                            valueField: 'account_code',
                            displayField: 'account_code',
                            anchor: '100%'
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Total :'
                        },
                        {
                            xtype: 'numericfield',
                            id: 'totalid',
                            ref: '../../total',
                            width: 75,
                            value: 0,
                            minValue: 0
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'Note :'
                        },
                        {
                            xtype: 'uctextfield',
                            ref: '../../item',
                            width: 300,
                            colspan: 3,
                            maxLength: 100
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 3,
                    defaults: {
                        scale: 'large'
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            height: 44,
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Edit',
                            height: 44,
                            ref: '../../btnEdit'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            height: 44,
                            ref: '../../btnDelete'
                        }
                    ]
                }
            ]
        };
        jun.PurchasingDetailsGrid.superclass.initComponent.call(this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnEdit.on('Click', this.onClickbtnEdit, this);
        this.btnDelete.on('Click', this.deleteRec, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    onStoreChange: function () {
        jun.rztPurchasingCoaDetail.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var item_name = this.item.getValue();
        var account_code = this.account_code.getValue();
        var total = parseFloat(this.total.getValue());
        if (account_code == "" || account_code == undefined) {
            Ext.MessageBox.alert("Error", "COA must selected.");
            return;
        }
        if (total < 0) {
            Ext.MessageBox.alert("Error", "Total must greater than 0.");
            return;
        }
        if (this.btnEdit.text == 'Save') {
            var record2 = this.sm.getSelected();
            record2.set('account_code', account_code);
            record2.set('item_name', item_name);
            record2.set('total', total);
            record2.commit();
        } else {
            var c = jun.rztPurchasingCoaDetail.recordType,
                    d = new c({
                        account_code: account_code,
                        item_name: item_name,
                        total: total
                    });
            jun.rztPurchasingCoaDetail.add(d);
        }
        this.account_code.reset();
        this.item.reset();
        this.total.reset();
    },
    btnDisable: function (s) {
        this.btnAdd.setDisabled(s);
        this.btnDelete.setDisabled(s);
        if (s) {
            this.sm.lock();
        } else {
            this.sm.unlock();
        }
    },
    onClickbtnEdit: function (btn) {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        if (btn.text == 'Edit') {
            this.account_code.setValue(record.data.account_code);
            this.item.setValue(record.data.item_name);
            this.total.setValue(record.data.total);
            btn.setText("Save");
            this.btnDisable(true);
        } else {
            this.loadForm();
            btn.setText("Edit");
            this.btnDisable(false);
        }
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
        this.store.remove(record);
    }
});
jun.PurchasingDetailstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PurchasingDetailstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'PurchasingDetailstoreId',
            url: 'PurchaseCoaDetail',
            autoLoad: !1,
            autoSave: !1,
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'purchase_coa_detail_id'},
                {name: 'item_name'},
                {name: 'total', type: 'float'},
                {name: 'purchase_coa_id'},
                {name: 'account_code'}
            ]
        }, cfg));
        this.on('add', this.refreshData, this);
        this.on('update', this.refreshData, this);
        this.on('remove', this.refreshData, this);
    },
    refreshData: function () {
        Ext.getCmp('totalcoa').setValue(this.sum("total"));

    }
});
jun.rztPurchasingCoaDetail = new jun.PurchasingDetailstore();
//jun.rztKasDetail.load();


jun.PurchasingCoa = Ext.extend(Ext.data.JsonStore, {
    constructor: function (cfg) {
        cfg = cfg || {};
        jun.PurchasingCoa.superclass.constructor.call(this, Ext.apply({
            storeId: 'PurchasingCoaId',
            url: 'Purchasing',
            root: 'results',
            totalProperty: 'total',
            fields: [
                {name: 'purchasing_coa_id'},
                {name: 'tgl'},
                {name: 'doc_ref'},
                {name: 'note'},
                {name: 'tdate'},
                {name: 'doc_ref_other'},
                {name: 'user_id'},
                {name: 'type_'},
                {name: 'store'},
                {name: 'supplier_id'},
                {name: 'total'},
                {name: 'disc'},
                {name: 'discrp'},
                {name: 'bruto'},
                {name: 'vat'},
                {name: 'tgl_jatuh_tempo'},
                {name: 'total_discrp1'},
                {name: 'total_pot'}
            ]
        }, cfg));
    }
});
jun.purchaseItem = new jun.PurchasingCoa({url: 'Purchasing/IndexIn'});
jun.rztReturnTransferItem = new jun.PurchasingCoa({url: 'TransferItem/IndexOut'});
