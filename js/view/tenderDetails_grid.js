jun.TenderDetailsGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "TenderDetails",
    id: 'docs-jun.TenderDetailsGrid',
    iconCls: "silk-grid",
    stripeRows: true,
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Pay Method',
            sortable: true,
            resizable: true,
            dataIndex: 'bank_id',
            width: 100,
            renderer: jun.renderBank
        },
        {
            header: 'Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'amount',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
    ],
    initComponent: function () {
        this.store = jun.rztTenderDetails;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Change Amount',
                    ref: '../btnEdit'
                }
            ]
        };
        jun.TenderDetailsGrid.superclass.initComponent.call(this);
        this.btnEdit.on('Click', this.loadEditForm, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    loadForm: function () {
        var form = new jun.TenderDetailsWin({modez: 0});
        form.show();
    },
    loadEditForm: function () {
        var selectedz = this.sm.getSelected();
        if (selectedz == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih pay method");
            return;
        }
        Ext.MessageBox.prompt('Pay Method Amount', 'Input amount :', function (btn, text) {
            if (text == "" || btn == 'cancel') return;
            var selectedz = this.sm.getSelected();
            var num = /^-|\d+/;
            if (num.test(text)) {
                var amount = parseFloat(text);
                selectedz.set('amount', amount);
                selectedz.commit();
            } else {
                Ext.MessageBox.show({
                    title: 'Error',
                    msg: 'Input must number.',
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.ERROR
                });
            }
        }, this);
    },
    deleteRec: function () {
        Ext.MessageBox.confirm('Pertanyaan', 'Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda Belum Memilih Data");
            return;
        }
        Ext.Ajax.request({
            url: 'TenderDetails/delete/id/' + record.json.tender_details_id,
            method: 'POST',
            success: function (f, a) {
                jun.rztTenderDetails.reload();
                var response = Ext.decode(f.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
})
