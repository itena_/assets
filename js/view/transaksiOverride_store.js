jun.TransaksiOverridestore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.TransaksiOverridestore.superclass.constructor.call(this, Ext.apply({
            storeId: 'TransaksiOverrideStoreId',
            url: 'projection/TransaksiOverride',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'transaksi_id'},
                {name:'businessunit_id'},
                {name:'docref'},
                {name:'name'},
                {name:'amount'},
                {name:'description'},
                {name:'tdate'},
                {name:'flag'},
                {name:'created_at'},
                {name:'uploadfile'},
                {name:'uploaddate'},
                
            ]
        }, cfg));
    }
});
jun.rztTransaksiOverride = new jun.TransaksiOverridestore();
//jun.rztTransaksiOverride.load();
