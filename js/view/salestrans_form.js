jun.SalestransWin = Ext.extend(Ext.Window, {
    title: 'Sales',
    modez: 1,
    width: 945,
    height: 565,
    layout: 'form',
    id: 'win-Salestrans',
    modal: true,
    padding: 5,
    resizable: !1,
    draggable: false,
    closeForm: false,
    statusActive: -1,
    onEsc: function () {
        Ext.MessageBox.confirm('Questions', 'Are you sure want exit?', function (btn) {
            if (btn == 'yes') {
                this.close();
            }
        },
                this);
    },
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-Salestrans',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        x: 85,
                        y: 2
                    },
//                    {
//                        xtype: "label",
//                        text: "Card Number:",
//                        x: 295,
//                        y: 35
//                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Receipt',
                        hideLabel: false,
                        hidden: true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 20,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
//                    {
//                        xtype: 'textfield',
//                        //hidden:true,
//                        name: 'card_number',
//                        enableKeyEvents: true,
//                        id: 'card_numberid',
//                        ref: '../card_number',
//                        maxLength: 500,
//                        width: 175,
//                        x: 400,
//                        y: 32
//                    },
                    {
                        xtype: "label",
                        text: "Customer:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        ref: '../customer',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztCustomersSalesCmp,
                        id: "customersales_id",
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        // itemSelector: "div.search-item-table",
                        // tpl: new Ext.XTemplate('<div class="container">', '<tpl for="."><div class="search-item-table">',
                        //     '<div class="cell4" style="width: 100px;text-align: right;font-size: 150%;"><h3>{no_customer}</div></h3>',
                        //     '<div class="cell4" style="width: 350px;font-size: 150%;"><h3>{nama_customer}</div></h3>',
                        //     "</div></tpl>", '</div>'),
                        itemSelector: 'div.search-item',
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">',
                                '<h3><span>{tgl_lahir:date("j M Y")}<br />{telp}</span>{nama_customer} ({no_customer})</h3>',
                                '{alamat}',
                                '</div></tpl>'
                                ),
                        allowBlank: false,
                        listWidth: 450,
                        lastQuery: "",
                        //readOnly: POSCUSTOMERREADONLY,
                        //value: POSCUSTOMERDEFAULT,
                        x: 400,
                        y: 2,
                        height: 20,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Referral:",
                        x: 295,
                        hidden: !REFERRAL,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        editable: false,
                        fieldLabel: 'Doctor',
                        ref: '../dokter',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztReferral,
                        hiddenName: 'referral_id',
                        valueField: 'referral_id',
                        hidden: !REFERRAL,
                        displayField: 'referral_nama',
                        allowBlank: true,
                        width: 175,
                        x: 400,
                        y: 35
                    },
                    {
                        xtype: "label",
                        text: "Doctor:",
//                        hidden:REFERRAL,
                        x: 610,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        editable: false,
                        fieldLabel: 'Doctor',
                        ref: '../dokter',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
//                        hidden:REFERRAL,
                        store: jun.rztDokterStatus,
                        hiddenName: 'dokter_id',
                        valueField: 'dokter_id',
                        displayField: 'nama_dokter',
                        allowBlank: true,
                        width: 200,
                        x: 715,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Comment:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        //hidden:true,
                        name: 'ketdisc',
                        id: 'ketdiscid',
                        ref: '../ketdisc',
                        enableKeyEvents: true,
                        maxLength: 255,
                        width: 175,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Beauty:",
                        x: 610,
                        y: 35
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        // editable: false,
                        // fieldLabel: 'Doctor',
                        ref: '../beauty',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztBeautyCmp,
                        hiddenName: 'beauty_id',
                        valueField: 'beauty_id',
                        displayField: 'nama_beauty',
                        allowBlank: true,
                        width: 200,
                        x: 715,
                        y: 32
                    },
//                    {
//                        xtype: "label",
//                        text: "Payment Method:",
//                        x: 295,
//                        y: 5
//                    },
//                    {
//                        xtype: 'combo',
//                        //typeAhead: true,
//                        triggerAction: 'all',
//                        lazyRender: true,
//                        ref: '../bank',
//                        mode: 'local',
//                        forceSelection: true,
//                        store: jun.rztBankCmp,
//                        hiddenName: 'bank_id',
//                        valueField: 'bank_id',
//                        displayField: 'nama_bank',
//                        width: 175,
//                        x: 400,
//                        y: 2
//                    },
//                    {
//                        xtype: "label",
//                        text: "History:",
//                        x: 610,
//                        y: 35
//                    },
                    {
                        xtype: 'hidden',
                        name: "log",
                        value: 0
                    },
                    {
                        xtype: 'tabpanel',
                        tabBarPosition: 'top',
                        x: 5,
                        y: 65,
                        activeTab: 0,
                        resizeTabs: true,
                        tabWidth: 450,
                        tabBar: {
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            defaults: {flex: 1}
                        },
                        items: [
                            new jun.SalestransDetailsGrid({
                                title: "Product/Treatment Details",
                                //x: 5,
                                //y: 65,
                                ref: '../../gridDetails',
                                height: 260,
                                frameHeader: !1,
                                header: !1
                            }),
                            new jun.PaketTransGrid({
                                title: "Package Product/Treatment",
                                arus: 1,
                                //x: 5,
                                //y: 65,
                                height: 260,
                                frameHeader: !1,
                                header: !1
                            })
                        ]
                    },
                    new jun.PaymentGrid({
                        x: 5,
                        y: 362,
                        height: 115,
                        width: 265,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "Sub Total:",
                        x: 295,
                        y: 365
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'bruto',
                        id: 'subtotalid',
                        ref: '../subtotal',
                        maxLength: 30,
                        value: 0,
                        readOnly: true,
                        width: 175,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 400,
                        y: 362
                    },
                    {
                        xtype: "label",
                        text: "VAT:",
                        x: 295,
                        y: 395
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'vat',
                        id: 'vatid',
                        ref: '../vat',
                        maxLength: 30,
                        value: 0,
                        readOnly: true,
                        width: 175,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 400,
                        y: 392
                    },
                    {
                        xtype: "label",
                        text: "Disc (%):",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'disc',
                        id: 'discid',
                        ref: '../disc',
                        maxLength: 5,
                        value: 0,
                        minValue: 0,
                        maxValue: 100,
                        width: 175,
                        readOnly: true,
                        enableKeyEvents: true,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 400,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Disc Amount:",
                        x: 295,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'discrp',
                        id: 'discrpid',
                        ref: '../discrp',
                        value: 0,
                        maxLength: 30,
                        width: 175,
                        readOnly: true,
                        enableKeyEvents: true,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 400,
                        y: 452
                    },
                    {
                        xtype: "label",
                        text: "Rounding:",
                        x: 610,
                        y: 365
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'rounding',
                        id: 'roundingid',
                        ref: '../rounding',
                        maxLength: 30,
                        value: 0,
                        readOnly: true,
                        width: 200,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 715,
                        y: 362
                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 610,
                        y: 392
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        value: 0,
                        readOnly: true,
                        maxLength: 30,
                        width: 200,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 715,
                        y: 392
                    },
                    {
                        xtype: "label",
                        text: "Collect:",
                        x: 610,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'bayar',
                        id: 'bayarid',
                        ref: '../bayar',
                        readOnly: true,
                        value: 0,
                        maxLength: 30,
                        width: 200,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 715,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Change:",
                        x: 610,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kembali',
                        id: 'kembaliid',
                        ref: '../kembali',
                        value: 0,
                        readOnly: true,
                        maxLength: 30,
                        width: 200,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 715,
                        y: 452
                    },
                    {
                        xtype: 'hidden',
                        id: 'overrideid',
                        ref: '../override',
                        name: 'override'
                    },
                    {
                        xtype: 'hidden',
                        id: 'totalpotid',
                        ref: '../totalpot',
                        name: 'totalpot'
                    },
                    {
                        xtype: 'hidden',
                        id: 'total_discrp1id',
                        ref: '../total_discrp1',
                        name: 'total_discrp1'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save (F10)',
                    hidden: false,
                    id: 'btnsave',
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    hidden: true,
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Remove Payment',
                    ref: '../btnDelPayment'
                },
                {
                    xtype: 'button',
                    text: 'Add Payment (F12)',
                    ref: '../btnPayment'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SalestransWin.superclass.initComponent.call(this);
        this.ketdisc.on('specialkey', function (t, e) {
            // e.preventDefault();
            if (e.getKey() == e.TAB) {
                var cmb = Ext.getCmp('barangdetailsid');
                if (cmb) {
                    cmb.focus();
                }
            }
        }, this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.discrp.on('change', this.onDiscrpChangeNew, this);
        this.discrp.on('keyup', this.onDiscrpChange, this);
        this.bayar.on('keyup', this.onBayarChange, this);
//        this.bank.on('select', this.onBankChange, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnPayment.on('click', this.onbtnPaymentclick, this);
        this.btnDelPayment.on('click', this.onbtnDelPaymentclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.customer.on('select', this.onCustomerSelect, this);
//        this.logCheck.on('check', this.onlogCheck, this);
        this.on("close", this.onWinClose, this);
        this.on("activate", this.onActive, this);
//        this.card_number.on("specialkey", this.onCardnumber, this);
        var obj = localStorage.getItem("sales_dokter_id");
//        var obj = jun.globalStore.readLocalStorage();
        this.dokter.setValue(obj);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.btnPayment.setVisible(false);
            this.btnDelPayment.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(false);
            this.btnPayment.setVisible(true);
            this.btnDelPayment.setVisible(true);
            //this.setDateTime();
        }
        if (SALES_OVERRIDE == "1") {
            //this.disc.setReadOnly(false);
            this.discrp.setReadOnly(false);
        } else {
            this.disc.setReadOnly(true);
            this.discrp.setReadOnly(true);
            Ext.getCmp('discid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discrpid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
        }
    },
    onActive: function (t) {
        this.tgl.setReadOnly(!EDIT_TGL);
        switch (this.statusActive) {
            case - 1:
                this.customer.focus(false, 50);
                break;
            case 0:
                this.gridDetails.barang.focus(false, 50);
                break;
            case 1:
                this.gridDetails.focusGrid();
                break;
            case 2:
                this.btnSave.focus();
                break;
        }
        this.statusActive = -1;
        t.getKeyMap().addBinding({
            key: [Ext.EventObject.F3, Ext.EventObject.F4, Ext.EventObject.F10, Ext.EventObject.F11, Ext.EventObject.F12],
            //ctrl: true,
            handler: function (key, e) {
                switch (key) {
                    case Ext.EventObject.F12:
                        if (this.btnPayment.isVisible()) {
                            var cmb = Ext.getCmp('barangdetailsid');
                            if (cmb) {
                                cmb.collapse();
                            }
                            this.onbtnPaymentclick();
                        }
                        break;
                    case Ext.EventObject.F3:
                        this.gridDetails.focusGrid();
                        break;
                    case Ext.EventObject.F10:
                        if (!this.btnSave.disabled) {
                            this.saveForm();
                        }
                        break;
                }
            },
            stopEvent: true,
            scope: this
        });
    },
    onbtnDelPaymentclick: function () {
        var record = Ext.getCmp('docs-jun.PaymentGrid').sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a payment");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this payment?', function (btn) {
            if (btn == 'no') {
                return;
            }
            jun.rztPayment.remove(record);
        }, this);
    },
    onbtnPaymentclick: function () {
        var win = Ext.getCmp('form-PaymentWin');
        if (win) {
            win.show();
        } else {
            var form = new jun.PaymentWin({modez: 0});
            form.show();
        }
        //form.bank.focus();
    },
    onlogCheck: function (c, s) {
//        this.storeCode.reset()
        this.storeCode.setDisabled(!s)
    },
    onCustomerSelect: function (combo, record, index) {
        twoRows("Selamat datang,", record.json.nama_customer);
    },
//    onBankChange: function () {
//        var bank = this.bank.getValue();
//        if (bank == "" || bank == undefined) {
//            return;
//        }
//        if (bank == SYSTEM_BANK_CASH) {
//            this.card_number.reset();
//            this.card_number.setDisabled(true);
//        } else {
//            this.card_number.setDisabled(false);
//        }
//    },
//    onCardnumber: function (f, e) {
//        if (e.getKey() == e.ENTER) {
//            var val = this.card_number.getValue();
//            if (val == "" || val == undefined) {
//                return;
//            }
//            var p = new SwipeParserObj(val);
//            this.card_number.setValue(p.account);
//            this.doc_ref.focus(false, 100);
//        }
//    },
//    setDateTime: function () {
//        Ext.Ajax.request({
//            url: 'site/GetDateTime',
//            method: 'POST',
//            scope: this,
//            success: function (f, a) {
//                var response = Ext.decode(f.responseText);
//                this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
//            },
//            failure: function (f, a) {
//                switch (a.failureType) {
//                    case Ext.form.Action.CLIENT_INVALID:
//                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
//                        break;
//                    case Ext.form.Action.CONNECT_FAILURE:
//                        Ext.Msg.alert('Failure', 'Ajax communication failed');
//                        break;
//                    case Ext.form.Action.SERVER_INVALID:
//                        Ext.Msg.alert('Failure', a.result.msg);
//                }
//            }
//        });
//    },
    onWinClose: function () {
        jun.rztSalestransDetails.removeAll();
        jun.rztPayment.removeAll();
        jun.rztPaketTrans.removeAll();
        this.customer.lastQuery = null;
    },
    onBayarChange: function () {
        jun.rztSalestransDetails.refreshData();
    },
    onDiscrpChangeNew: function (t, n, o) {
        this.onDiscrpChange(t);
    },
    onDiscChange: function (a) {
        var disc1 = parseFloat(a.getValue());
        //if (disc1 == 0) return;
        var discrpf = parseFloat(this.discrp.getValue());
        if (discrpf != 0) {
            this.discrp.setValue(0);
        }
        jun.rztSalestransDetails.each(function (record) {
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = round(price * qty, 2);
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
            var totalpot = discrp + discrp1;
            var total_with_disc = bruto - totalpot;
            var total = bruto - discrp;
            var vatrp = round(total_with_disc * vat, 2);
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
    },
    onDiscrpChange: function (a) {
        var discrpf = parseFloat(a.getValue());
        Ext.getCmp('btnsalesdetilid').setDisabled(discrpf != 0);
        var discf = parseFloat(this.disc.getValue());
        if (discf != 0) {
            this.disc.setValue(0);
            var disc1 = 0;
            var discrp1 = 0;
            jun.rztSalestransDetails.each(function (record) {
                var barang = jun.getBarang(record.data.barang_id);
                var price = parseFloat(record.data.price);
                var qty = parseFloat(record.data.qty);
                var disc = parseFloat(record.data.disc);
                var discrp = parseFloat(record.data.discrp);
                var bruto = round(price * qty, 2);
                var vat = jun.getTax(record.data.barang_id);
                discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
                var subtotal = bruto - discrp;
                var totalpot = discrp + discrp1;
                var total_with_disc = bruto - totalpot;
                var total = bruto - discrp;
                var vatrp = round(total_with_disc * vat, 2);
                record.set('discrp', discrp);
                record.set('vat', vat);
                record.set('vatrp', vatrp);
                record.set('total_pot', totalpot);
                record.set('total', total);
                record.set('bruto', bruto);
                record.set('disc1', disc1);
                record.set('discrp1', discrp1);
                record.commit();
            });
        }
        var subtotalf = jun.rztSalestransDetails.sum('total');
        if (subtotalf == 0)
            return;
        var total_discrp1 = 0;
        jun.rztSalestransDetails.each(function (record) {
            var total = parseFloat(record.data.total);
            var disc1 = round((total / subtotalf) * 100, 2);
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = round(price * qty, 2);
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * discrpf, 2);
            total_discrp1 += discrp1;
            var totalpot = discrp + discrp1;
            var total_with_disc = bruto - totalpot;
            total = bruto - discrp;
            var vatrp = round(total_with_disc * vat, 2);
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
        var sisa_discrp = discrpf - total_discrp1;
        if (sisa_discrp != 0.00) {
            var cnt = jun.rztSalestransDetails.getCount() - 1;
            var salesDetailsLastID = jun.rztSalestransDetails.getAt(cnt);
            var discrp1_sisa = parseFloat(salesDetailsLastID.data.discrp1) + sisa_discrp;
            // var discrp1 = salesDetailsLastID.discrp1;
            var total = parseFloat(salesDetailsLastID.data.total);
            // var disc1 = round((total / subtotalf) * 100, 2);
            var barang = jun.getBarang(salesDetailsLastID.data.barang_id);
            var price = parseFloat(salesDetailsLastID.data.price);
            var qty = parseFloat(salesDetailsLastID.data.qty);
            var disc = parseFloat(salesDetailsLastID.data.disc);
            var discrp = parseFloat(salesDetailsLastID.data.discrp);
            var bruto = round(price * qty, 2);
            var vat = jun.getTax(salesDetailsLastID.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
            var subtotal = bruto - discrp;
            discrp1 = discrp1_sisa;//disc1 == 0 ? 0 : round((disc1 / 100) * discrpf, 2);
            disc1 = round((discrp1 / subtotal) * 100, 2);
            var totalpot = discrp + discrp1;
            var total_with_disc = bruto - totalpot;
            total = bruto - discrp;
            var vatrp = round(total_with_disc * vat, 2);
            jun.rztSalestransDetails.getAt(cnt).set('discrp', discrp);
            jun.rztSalestransDetails.getAt(cnt).set('vat', vat);
            jun.rztSalestransDetails.getAt(cnt).set('vatrp', vatrp);
            jun.rztSalestransDetails.getAt(cnt).set('total_pot', totalpot);
            jun.rztSalestransDetails.getAt(cnt).set('total', total);
            jun.rztSalestransDetails.getAt(cnt).set('bruto', bruto);
            jun.rztSalestransDetails.getAt(cnt).set('disc1', disc1);
            jun.rztSalestransDetails.getAt(cnt).set('discrp1', discrp1);
            jun.rztSalestransDetails.getAt(cnt).commit();
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var kembali = parseFloat(this.kembali.getValue());
        var bayar = parseFloat(this.bayar.getValue());
        var total = parseFloat(this.total.getValue());
        var total_discrp1 = parseFloat(this.total_discrp1.getValue());
        if ((total - total_discrp1) > ROUNDING) {
            this.onDiscrpChange(this.discrp);
        }
        if (total > bayar) {
            Ext.Msg.alert('Error', "Masih kurang bayar " + (total - bayar));
            this.btnDisabled(false);
            this.customer.focus();
            return;
        }
        if (kembali < 0) {
            Ext.Msg.alert('Error', "Change can't less than 0");
            this.btnDisabled(false);
            this.customer.focus();
            return;
        }
        if (jun.rztSalestransDetails.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            this.customer.focus();
            return;
        }
        print(PRINTER_RECEIPT, __openCashDrawer);
        var urlz = 'Salestrans/create/';
        Ext.getCmp('form-Salestrans').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                        jun.rztSalestransDetails.data.items, "data")),
                payment: Ext.encode(Ext.pluck(
                        jun.rztPayment.data.items, "data")),
                paket: Ext.encode(Ext.pluck(
                        jun.rztPaketTrans.data.items, "data")),
                id: this.id,
                mode: this.modez,
                /*
                 * Mengisi field counter di db, kalo tidak di set di nwjs, 
                 * akan diisi default counter A
                 */
                counter: jun.Counter
            },
            success: function (f, a) {
                menuDetil("COLLECTED", parseFloat(Ext.getCmp("bayarid").getValue()), 'CHANGE', parseFloat(Ext.getCmp("kembaliid").getValue()));
//                jun.globalStore.set('sales_dokter_id', this.dokter.getValue());
                localStorage.setItem("sales_dokter_id", this.dokter.getValue());
                jun.rztSalestrans.reload();
                var response = Ext.decode(a.response.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    console.log("Printer not ready");
                } else {
                    //qz.append(chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r");
                    //qz.print();
                    // opencashdrawer();
                    //while (!qz.isDonePrinting()) {
                    //}
//                    qz.appendImage(getPath() + "images/img_receipt.png", "ESCP");
//                    while (!qz.isDoneAppending()) {
//                    }
//                    qz.append("\x1B\x40"); // 1
//                    qz.append("\x1B\x21\x08"); // 2
//                    qz.append("\x1B\x21\x01"); // 3
//                    qz.append(response.msg);
//                    qz.append("\x1D\x56\x41"); // 4
//                    qz.append("\x1B\x40"); // 5
//                    qz.print();
//                    qz.appendImage(getPath() + "images/img_receipt.png", "ESCP");
//                    while (!qz.isDoneAppending()) {
//                    }
//                    qz.append("\x1B\x40"); // 1
//                    qz.append("\x1B\x21\x08"); // 2
//                    qz.append("\x1B\x21\x01"); // 3
//                    qz.append(response.msg);
//                    qz.append("\x1D\x56\x41"); // 4
//                    qz.append("\x1B\x40"); // 5
//                    qz.print();
                    //qz.appendHTML('<html><pre>' + response.msg + '</pre></html>');
                    //qz.printHTML();
                    //qz.appendHTML('<html><pre>' + response.msg + '</pre></html>');
                    //qz.printHTML();
                    // printHTML(PRINTER_RECEIPT, '<html><pre>' + response.msg + '</pre></html>');
                    // printHTML(PRINTER_RECEIPT, '<html><pre>' + response.msg + '</pre></html>');
                    // if (response.stok !== false) {
                    //     var stok = [{type: 'raw', data: response.stok}];
                    //     var printDataStocker = __printDataStocker.concat(stok, __feedPaper, __cutPaper);
                    //     if (PRINTER_STOCKER != 'default') {
                    //         print(PRINTER_STOCKER, printDataStocker);
                    //     }
                    // }
                    if (PRINT_STOK) {
                    if (response.stok !== false) {
                        var stok = [{type: 'raw', data: response.stok}];
                        var printDataStocker = __printDataStocker.concat(stok, __feedPaper, __cutPaper);
                        if (PRINTER_STOCKER != 'default') {
                            print(PRINTER_STOCKER, printDataStocker);
                        }
                    }}
                    if (response.resep !== false) {
                        var resep = [{type: 'raw', data: response.resep}];
                        var printDataResep = __printDataStocker.concat(resep, __feedPaper, __cutPaper);
                        if (PRINTER_STOCKER != 'default') {
                            print(PRINTER_STOCKER, printDataResep);
                        }
                    }
                    var msg = [{type: 'raw', data: response.msg}];
                    var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                    print(PRINTER_RECEIPT, printData);
                    print(PRINTER_RECEIPT, printData);
                }
//                Ext.MessageBox.show({
//                    title: 'Info',
//                    msg: response.msg,
//                    buttons: Ext.MessageBox.OK,
//                    icon: Ext.MessageBox.INFO
//                });
                if (this.closeForm) {
                    this.close();
                } else {
                    this.btnDisabled(false);
                    Ext.getCmp('form-Salestrans').getForm().reset();
                    Ext.getCmp('btnsalesdetilid').setDisabled(false);
                    //this.setDateTime();
                    this.onWinClose();
                    // jun.rztSalestransDetails.refreshData();
                    if (POSCUSTOMERREADONLY) {
                        jun.rztCustomersSalesCmp.baseParams = {
                            customer_id: POSCUSTOMERDEFAULT
                        };
                        jun.rztCustomersSalesCmp.load();
                        jun.rztCustomersSalesCmp.baseParams = {};
                    }
                }
                this.customer.focus();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
                this.customer.focus();
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        Ext.MessageBox.confirm('Questions', 'Are you sure want exit?', function (btn) {
            if (btn == 'yes') {
                this.close();
            }
        },
                this);
    }
});
jun.HistoryWin = Ext.extend(Ext.Window, {
    title: 'Form History',
    modez: 1,
    width: 945,
    height: 565,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-History',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        width: 175,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Card Number:",
                        x: 295,
                        y: 35
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Receipt',
                        hideLabel: false,
                        hidden: true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 20,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: 'textfield',
                        //hidden:true,
                        name: 'card_number',
                        enableKeyEvents: true,
                        id: 'card_numberid',
                        ref: '../card_number',
                        maxLength: 500,
                        width: 175,
                        x: 400,
                        y: 32
                    },
                    {
                        xtype: "label",
                        text: "Customer:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'combo',
//                        //typeAhead: true,
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztCustomersSalesCmp,
                        id: "customersales_id",
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<h3><span>{telp} <br /> {tgl_lahir:date("M j, Y")}</span>{no_customer} | {nama_customer}</h3>',
                                '{alamat}',
                                "</div></tpl>"),
                        allowBlank: false,
                        listWidth: 750,
                        x: 85,
                        y: 32,
                        height: 20,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Payment Method:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        ref: '../bank',
                        mode: 'local',
                        forceSelection: true,
                        store: jun.rztBankCmp,
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        width: 175,
                        x: 400,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Doctor:",
                        x: 610,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        fieldLabel: 'Doctor',
                        ref: '../dokter',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztDokterCmp,
                        hiddenName: 'dokter_id',
                        valueField: 'dokter_id',
                        displayField: 'nama_dokter',
                        allowBlank: true,
                        width: 200,
                        x: 715,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "History:",
                        x: 610,
                        y: 35
                    },
                    {
                        xtype: 'hidden',
                        ref: '../log',
                        name: 'log',
                        value: 1
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        ref: '../storeCode',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztStoreCmp,
                        hiddenName: 'store',
                        name: 'store',
                        valueField: 'store_kode',
                        displayField: 'store_kode',
                        width: 200,
                        x: 715,
                        y: 32
                    },
                    new jun.SalestransDetailsGrid({
                        x: 5,
                        y: 65,
                        height: 285,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "Sub Total:",
                        x: 295,
                        y: 365
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'bruto',
                        id: 'subtotalid',
                        ref: '../subtotal',
                        maxLength: 30,
                        value: 0,
                        readOnly: true,
                        width: 175,
                        x: 400,
                        y: 362
                    },
                    {
                        xtype: "label",
                        text: "VAT:",
                        x: 295,
                        y: 395
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'vat',
                        id: 'vatid',
                        ref: '../vat',
                        maxLength: 30,
                        value: 0,
                        readOnly: true,
                        width: 175,
                        x: 400,
                        y: 392
                    },
                    {
                        xtype: "label",
                        text: "Disc (%):",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'disc',
                        id: 'discid',
                        ref: '../disc',
                        value: 0,
                        width: 175,
                        readOnly: true,
                        enableKeyEvents: true,
                        x: 400,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Disc Amount:",
                        x: 295,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'discrp',
                        id: 'discrpid',
                        ref: '../discrp',
                        value: 0,
                        maxLength: 30,
                        width: 175,
                        readOnly: true,
                        enableKeyEvents: true,
                        x: 400,
                        y: 452
                    },
                    {
                        xtype: "label",
                        text: "Comment:",
                        x: 610,
                        y: 365
                    },
                    {
                        xtype: 'textarea',
                        enableKeyEvents: true,
                        style: {textTransform: "uppercase"},
                        listeners: {
                            change: function (field, newValue, oldValue) {
                                field.setValue(newValue.toUpperCase());
                            }
                        },
                        name: 'ketdisc',
                        id: 'ketdiscid',
                        ref: '../ketdisc',
                        maxLength: 255,
                        width: 200,
                        height: 25,
                        x: 715,
                        y: 365
                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 610,
                        y: 392
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        value: 0,
                        readOnly: true,
                        maxLength: 30,
                        width: 200,
                        x: 715,
                        y: 392
                    },
                    {
                        xtype: "label",
                        text: "Collect:",
                        x: 610,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'bayar',
                        id: 'bayarid',
                        ref: '../bayar',
                        enableKeyEvents: true,
                        value: 0,
                        maxLength: 30,
                        width: 200,
                        x: 715,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Change:",
                        x: 610,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kembali',
                        id: 'kembaliid',
                        ref: '../kembali',
                        value: 0,
                        readOnly: true,
                        maxLength: 30,
                        width: 200,
                        x: 715,
                        y: 452
                    },
                    {
                        xtype: 'hidden',
                        id: 'overrideid',
                        ref: '../override',
                        name: 'override'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.HistoryWin.superclass.initComponent.call(this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.discrp.on('keyup', this.onDiscChange, this);
        this.bayar.on('keyup', this.onBayarChange, this);
        this.bank.on('select', this.onBankChange, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
//        this.logCheck.on('check', this.onlogCheck, this);
        this.on("close", this.onWinClose, this);
        this.card_number.on("specialkey", this.onCardnumber, this);
//        var obj = jun.globalStore.readLocalStorage();
//        this.dokter.setValue(obj.sales_dokter_id);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(true);
            //this.setDateTime();
        }
        if (SALES_OVERRIDE == "1") {
            this.disc.setReadOnly(false);
            this.discrp.setReadOnly(false);
        } else {
            this.disc.setReadOnly(true);
            this.discrp.setReadOnly(true);
            Ext.getCmp('discid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discrpid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
        }
    },
    onlogCheck: function (c, s) {
//        this.storeCode.reset()
        this.storeCode.setDisabled(!s)
    },
    onBankChange: function () {
        var bank = this.bank.getValue();
        if (bank == "" || bank == undefined) {
            return;
        }
        if (bank == SYSTEM_BANK_CASH) {
            this.card_number.reset();
            this.card_number.setDisabled(true);
        } else {
            this.card_number.setDisabled(false);
        }
    },
    onCardnumber: function (f, e) {
        if (e.getKey() == e.ENTER) {
            var val = this.card_number.getValue();
            if (val == "" || val == undefined) {
                return;
            }
            var p = new SwipeParserObj(val);
            this.card_number.setValue(p.account);
            this.doc_ref.focus(false, 100);
        }
    },
    //setDateTime: function () {
    //    Ext.Ajax.request({
    //        url: 'site/GetDateTime',
    //        method: 'POST',
    //        scope: this,
    //        success: function (f, a) {
    //            var response = Ext.decode(f.responseText);
    //            this.tgl.setValue(Date.parseDate(response.datetime, 'Y-m-d H:i:s'));
    //        },
    //        failure: function (f, a) {
    //            switch (a.failureType) {
    //                case Ext.form.Action.CLIENT_INVALID:
    //                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
    //                    break;
    //                case Ext.form.Action.CONNECT_FAILURE:
    //                    Ext.Msg.alert('Failure', 'Ajax communication failed');
    //                    break;
    //                case Ext.form.Action.SERVER_INVALID:
    //                    Ext.Msg.alert('Failure', a.result.msg);
    //            }
    //        }
    //    });
    //},
    onWinClose: function () {
        jun.rztSalestransDetails.removeAll();
    },
    onBayarChange: function () {
        jun.rztSalestransDetails.refreshData();
    },
    onDiscChange: function (a) {
        var subtotal = parseFloat(this.subtotal.getValue());
//        var vat = parseFloat(this.vat.getValue());
        var disc = parseFloat(this.disc.getValue());
        var discrp = parseFloat(this.discrp.getValue());
//        var bruto = subtotal - vat;
        if (a.id == "discid") {
            this.discrp.setValue(round(subtotal * (disc / 100), 2));
        } else {
            this.disc.setValue(round((discrp / subtotal) * 100, 2));
        }
        jun.rztSalestransDetails.refreshData();
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var kembali = parseFloat(this.kembali.getValue());
        if (kembali < 0) {
            Ext.Msg.alert('Error', "Change can't less than 0");
            this.btnDisabled(false);
            return;
        }
        var urlz = 'Salestrans/create/';
        Ext.getCmp('form-History').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                        jun.rztSalestransDetails.data.items, "data")),
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
//                jun.globalStore.set('sales_dokter_id', this.dokter.getValue());
                jun.rztHistory.reload();
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                if (this.closeForm) {
                    this.close();
                } else {
                    this.btnDisabled(false);
                    Ext.getCmp('form-History').getForm().reset();
                    //this.setDateTime();
                    this.onWinClose();
                    jun.rztSalestransDetails.refreshData();
                }
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        Ext.MessageBox.confirm('Questions', 'Are you sure want exit?', function (btn) {
            if (btn == 'yes') {
                this.close();
            }
        },
                this);
    }
});
jun.KetDiscWin = Ext.extend(Ext.Window, {
    title: 'Comment',
    modez: 1,
    width: 700,
    height: 115,
    layout: 'form',
    modal: true,
    padding: 5,
    resizable: !1,
    closeForm: false,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id: 'form-KetDisc',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: 'uctextfield',
                        fieldLabel: 'Comment',
                        hideLabel: false,
                        //hidden:true,
                        name: 'ketdisc',
                        ref: '../ketdisc',
                        maxLength: 100,
                        //allowBlank: ,
                        anchor: '100%'
                    }, {
                        xtype: 'hidden',
                        ref: '../salestrans_id',
                        name: 'salestrans_id'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                }
            ]
        };
        jun.KetDiscWin.superclass.initComponent.call(this);
        this.btnSaveClose.on('click', this.saveForm, this);
    },
    btnDisabled: function (status) {
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var urlz = 'Salestrans/UpdateComment/';
        Ext.getCmp('form-KetDisc').getForm().submit({
            url: urlz,
            scope: this,
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                jun.rztSalestrans.reload();
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
            }
        });
    }
});
jun.InputSales = Ext.extend(Ext.Window, {
    title: "Import Sales",
    iconCls: "silk13-report",
    modez: 1,
    width: 420,
    height: 540,
    layout: "form",
    modal: !0,
    padding: 5,
    closeForm: !1,
    resizable: !1,
    iswin: !0,
    initComponent: function () {
        if (jun.rztCardCmp.getTotalCount() === 0) {
            jun.rztCardCmp.load();
        }
        this.items = [
            {
                xtype: "form",
                frame: !1,
                bodyStyle: "background-color: #E4E4E4;padding: 10px",
                id: "form-InputSales",
                labelWidth: 100,
                labelAlign: "left",
                layout: "form",
                ref: "formz",
                border: !1,
                items: [
                    {
                        fieldLabel: 'Dari Tgl',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl_from",
                        id: "tgl_from_id",
                        ref: '../tgl_from',
                        value: DATE_NOW,
                        anchor: "100%"
                    },
                    {
                        fieldLabel: 'Sampai Tgl',
                        xtype: 'xdatefield',
                        format: 'd M Y',
                        name: "tgl_to",
                        id: "tgl_to_id",
                        ref: '../tgl_to',
                        value: DATE_NOW,
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        ref: '../customer',
                        fieldLabel: 'Customer',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztCustomersImportSalesCmp,
                        id: "customerimportsales_id",
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        itemSelector: 'div.search-item',
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">',
                                '<h3><span>{tgl_lahir:date("j M Y")}<br />{telp}</span>{nama_customer} ({no_customer})</h3>',
                                '{alamat}',
                                '</div></tpl>'
                                ),
                        allowBlank: false,
                        listWidth: 450,
                        lastQuery: "",
                        anchor: "100%"
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Payment Method',
                        store: jun.rztBankCmp,
                        ref: '../bank',
                        id: 'paymentbankid',
                        hiddenName: 'bank_id',
                        valueField: 'bank_id',
                        displayField: 'nama_bank',
                        anchor: '100%'
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'Card Number',
                        hideLabel: false,
                        //hidden:true,
                        name: 'card_number',
                        id: 'card_numberid',
                        ref: '../card_number',
                        enableKeyEvents: true,
                        maxLength: 16,
                        anchor: '100%'
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Card Type',
                        store: jun.rztCardCmp,
                        ref: '../card',
                        hiddenName: 'card_id',
                        valueField: 'card_id',
                        displayField: 'card_name',
                        anchor: '100%'
                    },
                    {
                        html: '<input type="file" name="xlfile" id="inputFile" ' +
                                'onclick="Ext.getCmp(\'save_import_sales_id\').setDisabled(true)" />',
                        name: 'file',
                        xtype: "panel",
                        listeners: {
                            render: function (c) {
                                new Ext.ToolTip({
                                    target: c.getEl(),
                                    html: 'Format file : Excel (.xlsx)<br>Only first sheet will be saved'
                                });
                            },
                            afterrender: function () {
                                itemFile = document.getElementById("inputFile");
                                itemFile.addEventListener('change', readFileExcel, false);
                            }
                        }
                    },
                    {
                        xtype: "panel",
                        bodyStyle: "background-color: #E4E4E4;padding: 5px",
                        html: 'Format file : Excel (.xls)<br>Pastikan hanya ada 1 sheet didalam file (nama sheet apa aja boleh)' +
                                '<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXkAAADlCAMAAACBDneDAAACalBMVEUAAAAAAEAAAEgAAGkAAHQAQI0ASJwAaawAdL8bXTkbXXQbc48biKYhc0Yic20ic48ij64ip8pAAABAAGlAQGlAQI1AaY1AaaxAjcpERERERGVERINEZZ5Eg7hGXTlGXVlGXXRGnLxIAABIAEhIAHRISABISEhISJxIdL9InL9InN9Vc0ZVv8pVv+VlRERlRGVlZZ5lns9nXVlnXXRnr9JpAABpAEBpAGlpQGlpaaxprOZ0AAB0AEh0AHR0SJx0dEh0dHR0v790v/9+c0Z+fn5+v+V+1f+AgICBgYGCgoKDRESDRGWDg4ODuOaFczmFwNKNQACNQECNQGmNaQCNjUCNjY2NjayNrOaNyuaOjo6Pj4+SkpKUlJSZmZmbm5ucSACcnEicnJyc37+c39+c3/+dnZ2eZUSeZWWenmWeuJ6ez+afn5+giDmgoKCg0tKij0aioqKi1f+i6v+lpaWnp6eoqKipqamqqqqrq6usaQCsrKys5uatra2urq6vr6+wsLCxsbGysrKzs7O0tLS1tbW2tra3t7e4g0S4uLi45ua5nFm50tK6urq7u7u9vb2/dAC/dEi/v3S/v7+/35y//9+////AwMDCwsLDp0bDv23Dw8PD1Y/D///ExMTFxcXGxsbHx8fJycnKjUDK5ubMzMzOzs7PnmXPnp7Pz8/P5ubSr3TSwI/S0qbS0rzS0tLT09PU1NTW1tbX19ffnEjf35zf///h4eHiv23i4uLi///j4+PmrGnmuIPmyo3mz57m5qzm5rjm5srm5s/m5ubx8fH/v3T/1Y//35z/6q7//7///8r//9///+X///81+fBZAAAMVklEQVR4AeyWz6rTUBDGp4h07x/Ii/iHPoWuzNa1CIUqtpZAMFhcxCwjBKQ2VnBlH6AEuZwXu5n1yc18MB/kLjKQrIYvv/Pr6TkjYa5palLzs/m5ZvOz+X/+ZH8EP43Mg7fi5v/62fwR/DQyD96Kmz/72fwR/DQyD96Kmz/52fwR/LRJeE6w+Uv/HP1s/gh+2iQ8R9T8pehfjRW3W1odDQD1/5nIIkXwG46wm1ciD/aEoGZwLboYFF1i8Wq+Nj7cPXlp8deI+edpuD78DXQCadAvnfTsbwhJ9eBawlVWKLrE4tV8ZXy4TdrEaKlA891TxHxFMb9LAqmqYfO6j0B0icWr+dL4075OTWElaL6FbJSULf9izzJfDpvXN4gusXg1fwij1Wvv5Y/3HMBzHjpsNM1f3WOa+YNh3kaXWLyaL6zDJph7tUDPeejGK+7Zni+GzesnQHSJxWtnbm5Vc0bIQfP62JVzJpsVy3x+h/l2iaJLLF7NZ2Gs9Boxl5FR93xG8aWjB2e2yYbNt4sURZdYvJrfAiNCOz7Sb6nzvKYRqnsEXCwgT7wWzYbRJRav5j/62fwR/DQ6j6NVYvFqfuNn80fw0+g8jlYJg7X2s/kj+Gl8HrwVNP/Oz+aP4KfxefBW0PxbP5s/gp9G53G0yp/z6dfPH3X1/dvXL3n2+dOHzfr9LXt3mxs3Ecdx3Cyh+D0q9HVoC/UBkNiGB3KC8iDEm1IEyyFWHMIg2AvkBJVMXkVRFVXWaoV8J/y3v87sSDHayrMeW/xGkIkfMl5/djrZeH7r/fXH5yrHLj19/nk1uLz0F+O3FuXx9O8qeclLXvKSl7zkJR+qSF7yTMv78mWSJOe2T95U/7xovyZ1ydjYbE1tJYveAXdPEysnm2p7atutiSFnmqddi3+1j6I+btNgmdy7jCHPafGoFo/SluPN5C/e/s6Tt2brNj+vz2h39kWKPF/3NiKf2elndzy2D1cmY0C7zzaD5WmRR1FXjxvy/Mso8uVi1UjwqGzZvnkDeex9eddGee/vhxtf3m108lV5srlbfvcU8PDyX53ZyT/8I4Y8p1XUx8YjT+2/gfJ2WotWN89M1pNnoyfvgH15npLjyJ/n9ZGLtIwgj7ZVfKm7wJ9WD5W38X1Bo/a8dvLNyM3Gw+UL+6mA8gnt1W3vzlZR5OlQTt5Q0mq4vJVi0ahbw36fZ+Ph8s3XI/T5+qA1+zT6vHX6TQB5Ts26V10yX56Nh4/zx5KvijTPqjL+OB9Qvsia39b1f9Y8bJwzG70+vz3tfW1TJOdHk9+enmziyNs/el63hZW3zr5Ytadrz68tJ4tfbl9Je+N8wuKd8rzwzdx+w+V5FPY3QlpFkq8K/l45QF5/w4Z/PPR6XT3QdRvJS36q8j3Zg5dHLirq8xptJC95yUte8pIPUiSfp97CWPKS5yKrW/ifyl/wLssA8kWS/ccBS65W2vXF5pJjykJVtPmF7alVt80Utj/XKps1Repm/a24aTIuiTZryDqQBihtkR047MmGa82lWxFO/gAI5F99ynv9hsjTe5+86D+gXdRnCiXP3DSLLWw/urRpgN3TzHZqm2HeoUL+sf1gkbowA/LWEmttmsBlIUgDkJSwRZtaaDbnqZOnhYDyQBwgz1vqh8oj0XdAJHcWGLAvyLcLXM4v285IM2xA/onpFKk3NrFj0fox99XNkZEG4Oo5P2Y/AHgnTwtB5TnOIfJX9PnjycNhAnbCTL4w2LDVAMg6ZPZUPLLxCPnMCIvUn3ekz2f42pPl5oWbNABJiXrB/0fi5Gkhgjy3YxhBvsotjHXuTrSoFVmwuu16OfLN+GBjBfJVfu8S+eJ2ZGamC1gDv5UnDUBSotz/gaxy8rQQSf6Cty8fWR4bOqi/YK6V3+dLnhfk670z5JttDX/3FN3R5+nnJCWcfEbDnTwtRJFnFnYEefI3KQsMIB28I2G0We3L2+afunF+H9JWtWv9cR55EiJunOd/fod3LcSQv/7g9Vjy1m/pbr+vGu4u1WDwDYCZuY5vnRF5W5GkhBk8edupaHOOkNo3yBduB17b8PTzSydmn79KuP3ZUHmCtdndBzQP22ggpJVr7m7BSmp71AY0w6t7J1+vSGmGBO1ekrlkuO7GbeT5TU5YYL+LF/7r/6DyDmJSf8MWqb+g6zbjyDPY+AuS17VKySt7oKLsQfyi0Ubykpe85CUvecn3hg6UPRhX3rscq+zBmju7DpYnQsABWZO5yssZUIXJHlBzFwKmTQKDh88e3Hxf4weZhyVCwAHdHDyVlzOgCpQ9sNq9i83CBrYUTZ4zGHMGnDPngNBQ+TkDqnDZA+bSaZrnLXQJnz1YL0PJm16PvJczoAqUPej6PvLMfk1ankkp4AfKc8ReeRa8lWGyB0QKbuVL8msz6PPvhEk6GU+vvJczoAqUPegiBbRHpGTq8nwKRwB5MPvkvZwBVaDsgdXes8s305a3T4C4CNDniRD0y3s5A6pA2QOrbSXg249n0ef5eKEA8kQIOKCbg6fycgZUgbIHXd9HnsBBmDL/7IHLGSh7MJp8T85A2QNdq5S8sgcqyh5otJG85CUveclLXvLKHkhe2YN+eS5XLueXPWD6g6s/3NeA2xxwI1ciCdPMHnDPieU8swdunoT7GnCbA67EEUmYbvbg+v63y1lmD2DN3WpaRt5FEqY5G3jz9Q8zzR64eym4+xosVp48kYTpyfOh6sjPLXvABKIN76zgNgcM8EQSpihPygn5uWUPGPpzdt77QAjX59kyRfmbZ4mVB7PMHtheP5/x2Q4Ua8IbbbL5Z8ymlz1wnyvFfQ24zYGTZ98Jy88ze1DxTOzd18BlzpLMRRKUPVD2QNkDXauUvOSVPVD2QH1eo43kJS95yUte8pKXvORn8ZkLybuSjyG/VJ+PI7+WfCx5PuFlZHnJc+//0eUlT+RG8hHkue/ByPKS5/OkxpeXPPc9kLz+hpW85CUft0he2QMVZQ802khe8pKXvOQlL3nJS17yV0miOakY8lewjywv+ZtvftNoE0X++v1nSbIcX17yVzX7q080Az62PMP8+sH48hpt7pv8cnx5/YZ9tnx9/Z5SHyPLEz54i2Fe8vobVvKSl3y8InllD1SUPdBoI3nJS17ykpe85CUvec1JWdHMSJw+r+vzseTXmgGPIM+EoOQjyDP/Paq85Hnv/fjykufzLiQfQ369lHwMeS9g9m979tLTRBTFAZxt1Yt0OTx8rLH4kG8AtoAgKxcFukUlBiKN0DQgD0GD7lxhVGaHKWA7dAHSEEI6piGk/U7e452b1KTXSWemM3Hy/yczZ2BY/Tg9984U8niGhTzkIe86kIf8u83NjY236+tra6urKytvlpeXlrJZxowWB2nLNwpjre4K9DzkIQ95yEMe8pCHvHeBPOT3b0SuzELef/kfT/rze1c/Qt53+YOHs3RAPpiev4Zp47s80Udo2EDeb/n9m+P5/VuYNv7Lf7jNTy/7Ie+7PPX7wYNx/+Ux5/cikUh/HvJ4hoU85CHvUSAP+VTLU3OZVDjTZrY67uXNUAbykId86ORTkG8QyEPeyCYTiWTWgLy/8qXFofS3QmEnPfT6ZzjlL3oZY/HLgetfVc7HjLXP28qfP57xUr6UmiyKq+JkqhRGeYJXyRN6XPwF3VXJn/VpWk9jefHL85H7TcsvTB3Jy6OpxeDldfUdR/JkG+X8rxTy2yT/qFada59Xyp/d5brft9Tyu13Pm5Y3hotUBgfpXBw1gpbXdSe3bOT/oHL5L4zF6IKxqFWqc4yxjpOajfy9nEB+0adx4fK01jljFfFp4PZNy2fSpiVPSWcCltf/Ka87kSdiGiVU6ELUmCh3pHyFzgp5btydI/mR7txp15b5vof+F1ahnnckn9ypl99JBiuv28jrDuSpn2kBvRwQBw2fi96Oz6Kc0LSRnwuFPMFq3J6Q+UGl/EwWx/KJQ2KnEP5hIoQ9L+yjNOercyRP4fIUS35brK9qeRrlW1J+RNO0Tlm8kh8N4ZyvcNrjevmoXHcpJE+9X7ORL0+PSXlhLYs300afCOHepsIoMSlvbTKtwuVl/8eV8qecVZBToQFPoSLkvV9hQ7Sfj9WkvPg5Xlfs5GkXo2ljppSnOdOdk2XX4d6mMPrXrrLQSg68PVA/SS2Y7gN5vD3AGzPIq2JkniaGJzLuZzzk8c0Icx3I/1fy+AZcBIE85CH/CQkmbb+QYPIb3JeYtFzgiWUAAAAASUVORK5CYII=">'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: "toolbar",
            items: [
                {
                    xtype: "button",
                    text: "Check input",
                    ref: "../btnCheck"
                },
                {
                    xtype: "button",
                    text: "Save",
                    ref: "../btnSave",
                    id: 'save_import_sales_id',
                    disabled: true
                }
            ]
        };
        jun.InputSales.superclass.initComponent.call(this);
        this.btnCheck.on("click", this.onbtnCheckclick, this);
        this.btnSave.on("click", this.onbtnSaveclick, this);
        this.bank.on('select', this.onBankChange, this);
        jun.SalesStore.on({
            scope: this,
            load: {
                fn: function (t, r, o) {
                    var valid = true;
                    if (r.length == 0) {
                        valid = false;
                    } else {
                        for (var i = 0, len = r.length; i < len; i++) {
                            if (r[i].data.ERROR != '') {
                                valid = false;
                            }
                        }
                    }
                    Ext.getCmp('save_import_sales_id').setDisabled(!valid);
                }
            }
        });
    },
    onBankChange: function () {
        var bank = this.bank.getValue();
        if (bank == "" || bank == undefined) {
            return;
        }
        if (bank == SYSTEM_BANK_CASH) {
            this.card_number.reset();
            this.card.reset();
            this.card_number.setDisabled(true);
            this.card.setDisabled(true);
        } else {
            this.card_number.setDisabled(false);
            this.card.setDisabled(false);
        }
    },
    onbtnCheckclick: function () {
        this.btnCheck.setDisabled(true);
        jun.SalesStore.removeAll();
        var bank = this.bank.getValue();
        if (jun.dataStockOpname == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih file Sales.");
            this.btnCheck.setDisabled(false);
            return;
        }
        if (Ext.getCmp('tgl_from_id').hiddenField.dom.value == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih tgl mulai.");
            this.btnCheck.setDisabled(false);
            return;
        }
        if (Ext.getCmp('tgl_to_id').hiddenField.dom.value == "") {
            Ext.MessageBox.alert("Error", "Anda belum memilih tgl selesai.");
            this.btnCheck.setDisabled(false);
            return;
        }
        if (this.customer.getValue() == '') {
            Ext.MessageBox.alert("Error", "Customer harus dipilih.");
            this.btnCheck.setDisabled(false);
            return;
        }
        if (this.bank.getValue() == '') {
            Ext.MessageBox.alert("Error", "Bank harus dipilih.");
            this.btnCheck.setDisabled(false);
            return;
        }
        if ((this.card.getValue() == '') && (bank != SYSTEM_BANK_CASH)) {
            Ext.MessageBox.alert("Error", "Card harus dipilih.");
            this.btnCheck.setDisabled(false);
            return;
        }
        jun.SalesStore.baseParams = {
            tgl_from: Ext.getCmp('tgl_from_id').hiddenField.dom.value,
            tgl_to: Ext.getCmp('tgl_to_id').hiddenField.dom.value,
            data: Ext.encode(jun.dataStockOpname[jun.namasheet])
        };
        jun.SalesStore.load();
        jun.SalesStore.baseParams = {};
        var win = new jun.CheckInputSalesWin();
        win.show(this);
        this.btnCheck.setDisabled(false);
    },
    onbtnSaveclick: function () { //console.log(JSON.stringify(jun.dataSales));
        Ext.getCmp('form-InputSales').getForm().submit({
            url: 'Salestrans/ImportSales',
            timeOut: 1000,
            scope: this,
            params: {
                data: Ext.encode(Ext.pluck(
                        jun.SalesStore.data.items, "data"))
            },
            success: function (f, a) {
                var response = Ext.decode(a.response.responseText);
                Ext.MessageBox.show({
                    title: 'Info',
                    msg: response.msg,
                    buttons: Ext.MessageBox.OK,
                    icon: Ext.MessageBox.INFO
                });
                this.close();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Connection failure has occured.');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
            }
        });
    }
});
jun.SalesStore = new Ext.data.JsonStore({
    url: 'Salestrans/CekInputImport',
    storeId: 'jun.SalesStore',
    // idProperty: 'NORECEIPT',
    fields: ['SALES ID', 'TGL', 'ITEM', 'QTY', 'ERROR']
});
jun.CheckInputSalesWin = Ext.extend(Ext.Window, {
    title: "Data Import Sales",
    width: 800,
    height: 500,
    layout: "form",
    modal: 1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'grid',
                store: jun.SalesStore,
                frame: true,
                anchor: '100% 100%',
                viewConfig: {
                    forceFit: true, autoFill: true,
                    getRowClass: function (record) {
                        return record.get('ERROR') == '' ? 'valid-row' : 'error-row';
                    }
                },
                columns: [
                    new Ext.grid.RowNumberer(),
                    {
                        header: 'SALES ID',
                        dataIndex: 'SALES ID',
                        width: 100
                                // renderer: function (value, metaData, record, rowIndex) {
                                //     if (jun.rztBarangLib.findExact('kode_barang', value) == -1)
                                //         metaData.style = "background-color: #FF7F7F;";
                                //     return value;
                                // }
                    },
                    {
                        header: 'TGL',
                        dataIndex: 'TGL',
                        width: 60
                    },
                    {
                        header: 'ITEM',
                        dataIndex: 'ITEM',
                        width: 160
                    },
                    {
                        header: 'QTY',
                        dataIndex: 'QTY',
                        width: 60,
                        align: "right",
                        renderer: Ext.util.Format.numberRenderer("0,0")
                    },
                    {
                        header: 'ERROR',
                        dataIndex: 'ERROR',
                        width: 160
                    }
                ]
            }
        ];
        jun.CheckInputSalesWin.superclass.initComponent.call(this);
    }
});
jun.SalestransNoBeautyWin = Ext.extend(Ext.Panel, {
    title: 'Sales',
    modez: 1,
    // width: 945,
    // height: 565,
    layout: 'form',
    id: 'win-SalestransNoBeauty',
    // modal: true,
    padding: 5,
    statusActive: -1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 1px',
                id: 'form-SalestransNoBeauty',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'absolute',
                anchor: "100% 100%",
                ref: 'formz',
                border: false,
                items: [
                    {
                        xtype: "label",
                        text: "Date:",
                        x: 5,
                        y: 5
                    },
                    {
                        xtype: 'xdatefield',
                        ref: '../tgl',
                        fieldLabel: 'Date',
                        name: 'tgl',
                        id: 'tglid',
                        format: 'd M Y',
                        width: 175,
                        readOnly: true,
                        allowBlank: false,
                        value: DATE_NOW,
                        x: 85,
                        y: 2
                    },
                    {
                        xtype: 'textfield',
                        fieldLabel: 'No. Receipt',
                        hideLabel: false,
                        hidden: true,
                        name: 'doc_ref',
                        id: 'doc_refid',
                        ref: '../doc_ref',
                        maxLength: 20,
                        height: 20,
                        width: 175,
                        readOnly: true
                    },
                    {
                        xtype: "label",
                        text: "Customer:",
                        x: 295,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        ref: '../customer',
                        triggerAction: 'query',
                        lazyRender: true,
                        mode: 'remote',
                        forceSelection: true,
                        autoSelect: false,
                        store: jun.rztCustomersSalesCmp,
                        id: "customersales_id",
                        hiddenName: 'customer_id',
                        valueField: 'customer_id',
                        displayField: 'nama_customer',
                        hideTrigger: true,
                        minChars: 3,
                        matchFieldWidth: !1,
                        pageSize: 20,
                        // itemSelector: "div.search-item-table",
                        // tpl: new Ext.XTemplate('<div class="container">', '<tpl for="."><div class="search-item-table">',
                        //     '<div class="cell4" style="width: 100px;text-align: right;font-size: 150%;"><h3>{no_customer}</div></h3>',
                        //     '<div class="cell4" style="width: 350px;font-size: 150%;"><h3>{nama_customer}</div></h3>',
                        //     "</div></tpl>", '</div>'),
                        itemSelector: 'div.search-item',
                        tpl: new Ext.XTemplate(
                                '<tpl for="."><div class="search-item">',
                                '<h3><span>{tgl_lahir:date("j M Y")}<br />{telp}</span>{nama_customer} ({no_customer})</h3>',
                                '{alamat}',
                                '</div></tpl>'
                                ),
                        allowBlank: false,
                        listWidth: 450,
                        lastQuery: "",
                        //readOnly: POSCUSTOMERREADONLY,
                        //value: POSCUSTOMERDEFAULT,
                        x: 400,
                        y: 2,
                        height: 20,
                        width: 175
                    },
                    {
                        xtype: "label",
                        text: "Doctor",
                        x: 610,
                        y: 5
                    },
                    {
                        xtype: 'combo',
                        //typeAhead: true,
                        editable: false,
                        fieldLabel: 'Doctor',
                        ref: '../dokter',
                        triggerAction: 'all',
                        lazyRender: true,
                        mode: 'local',
                        store: jun.rztDokterStatus,
                        hiddenName: 'dokter_id',
                        valueField: 'dokter_id',
                        displayField: 'nama_dokter',
                        allowBlank: true,
                        width: 200,
                        x: 715,
                        y: 2
                    },
                    {
                        xtype: "label",
                        text: "Comment:",
                        x: 5,
                        y: 35
                    },
                    {
                        xtype: 'uctextfield',
                        //hidden:true,
                        name: 'ketdisc',
                        id: 'ketdiscid',
                        ref: '../ketdisc',
                        enableKeyEvents: true,
                        maxLength: 255,
                        width: 830,
                        x: 85,
                        y: 32
                    },
                    {
                        xtype: 'hidden',
                        name: "log",
                        value: 0
                    },
                    new jun.SalestransNoBeautyDetailsGrid({
                        // title: "Product/Treatment Details",
                        x: 5,
                        y: 65,
                        ref: '../../gridDetails',
                        height: 260,
                        frameHeader: !1,
                        header: !1
                    }),
                    new jun.PaymentGrid({
                        x: 5,
                        y: 362,
                        height: 115,
                        width: 265,
                        frameHeader: !1,
                        header: !1
                    }),
                    {
                        xtype: "label",
                        text: "Sub Total:",
                        x: 295,
                        y: 365
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'bruto',
                        id: 'subtotalid',
                        ref: '../subtotal',
                        maxLength: 30,
                        value: 0,
                        readOnly: true,
                        width: 175,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 400,
                        y: 362
                    },
                    {
                        xtype: "label",
                        text: "VAT:",
                        x: 295,
                        y: 395
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'vat',
                        id: 'vatid',
                        ref: '../vat',
                        maxLength: 30,
                        value: 0,
                        readOnly: true,
                        width: 175,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 400,
                        y: 392
                    },
                    {
                        xtype: "label",
                        text: "Disc (%):",
                        x: 295,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'disc',
                        id: 'discid',
                        ref: '../disc',
                        maxLength: 5,
                        value: 0,
                        minValue: 0,
                        maxValue: 100,
                        width: 175,
                        readOnly: true,
                        enableKeyEvents: true,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 400,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Disc Amount:",
                        x: 295,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'discrp',
                        id: 'discrpid',
                        ref: '../discrp',
                        value: 0,
                        maxLength: 30,
                        width: 175,
                        readOnly: true,
                        enableKeyEvents: true,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 400,
                        y: 452
                    },
                    {
                        xtype: "label",
                        text: "Rounding:",
                        x: 610,
                        y: 365
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'rounding',
                        id: 'roundingid',
                        ref: '../rounding',
                        maxLength: 30,
                        value: 0,
                        readOnly: true,
                        width: 200,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 715,
                        y: 362
                    },
                    {
                        xtype: "label",
                        text: "Total:",
                        x: 610,
                        y: 392
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'total',
                        id: 'totalid',
                        ref: '../total',
                        value: 0,
                        readOnly: true,
                        maxLength: 30,
                        width: 200,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 715,
                        y: 392
                    },
                    {
                        xtype: "label",
                        text: "Collect:",
                        x: 610,
                        y: 425
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'bayar',
                        id: 'bayarid',
                        ref: '../bayar',
                        readOnly: true,
                        value: 0,
                        maxLength: 30,
                        width: 200,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 715,
                        y: 422
                    },
                    {
                        xtype: "label",
                        text: "Change:",
                        x: 610,
                        y: 455
                    },
                    {
                        xtype: 'numericfield',
                        hideLabel: false,
                        //hidden:true,
                        name: 'kembali',
                        id: 'kembaliid',
                        ref: '../kembali',
                        value: 0,
                        readOnly: true,
                        maxLength: 30,
                        width: 200,
                        alwaysDisplayDecimals: true,
                        decimalPrecision: 2,
                        x: 715,
                        y: 452
                    },
                    {
                        xtype: 'hidden',
                        id: 'overrideid',
                        ref: '../override',
                        name: 'override'
                    },
                    {
                        xtype: 'hidden',
                        id: 'totalpotid',
                        ref: '../totalpot',
                        name: 'totalpot'
                    },
                    {
                        xtype: 'hidden',
                        id: 'total_discrp1id',
                        ref: '../total_discrp1',
                        name: 'total_discrp1'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save (F10)',
                    hidden: false,
                    id: 'btnsave',
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    hidden: true,
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Remove Payment',
                    ref: '../btnDelPayment'
                },
                {
                    xtype: 'button',
                    text: 'Add Payment (F12)',
                    ref: '../btnPayment'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref: '../btnCancel'
                }
            ]
        };
        jun.SalestransNoBeautyWin.superclass.initComponent.call(this);
        this.ketdisc.on('specialkey', function (t, e) {
            // e.preventDefault();
            if (e.getKey() == e.TAB) {
                var cmb = Ext.getCmp('barangdetailsid');
                if (cmb) {
                    cmb.focus();
                }
            }
        }, this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.discrp.on('keyup', this.onDiscrpChange, this);
        this.bayar.on('keyup', this.onBayarChange, this);
//        this.bank.on('select', this.onBankChange, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnPayment.on('click', this.onbtnPaymentclick, this);
        this.btnDelPayment.on('click', this.onbtnDelPaymentclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.customer.on('select', this.onCustomerSelect, this);
//        this.logCheck.on('check', this.onlogCheck, this);
        this.on("close", this.onWinClose, this);
        this.on("activate", this.onActive, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.btnPayment.setVisible(false);
            this.btnDelPayment.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(false);
            this.btnPayment.setVisible(true);
            this.btnDelPayment.setVisible(true);
            //this.setDateTime();
        }
        if (SALES_OVERRIDE == "1") {
            //this.disc.setReadOnly(false);
            this.discrp.setReadOnly(false);
        } else {
            this.disc.setReadOnly(true);
            this.discrp.setReadOnly(true);
            Ext.getCmp('discid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discrpid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
        }
    },
    onActive: function (t) {
        this.tgl.setReadOnly(!EDIT_TGL);
        switch (this.statusActive) {
            case - 1:
                this.customer.focus(false, 50);
                break;
            case 0:
                this.gridDetails.barang.focus(false, 50);
                break;
            case 1:
                this.gridDetails.focusGrid();
                break;
            case 2:
                this.btnSave.focus();
                break;
        }
        this.statusActive = -1;
        t.getKeyMap().addBinding({
            key: [Ext.EventObject.F3, Ext.EventObject.F4, Ext.EventObject.F10, Ext.EventObject.F11, Ext.EventObject.F12],
            //ctrl: true,
            handler: function (key, e) {
                switch (key) {
                    case Ext.EventObject.F12:
                        if (this.btnPayment.isVisible()) {
                            var cmb = Ext.getCmp('barangdetailsid');
                            if (cmb) {
                                cmb.collapse();
                            }
                            this.onbtnPaymentclick();
                        }
                        break;
                    case Ext.EventObject.F3:
                        this.gridDetails.focusGrid();
                        break;
                    case Ext.EventObject.F10:
                        if (!this.btnSave.disabled) {
                            this.saveForm();
                        }
                        break;
                }
            },
            stopEvent: true,
            scope: this
        });
    },
    onbtnDelPaymentclick: function () {
        var record = Ext.getCmp('docs-jun.PaymentGrid').sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a payment");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this payment?', function (btn) {
            if (btn == 'no') {
                return;
            }
            jun.rztPayment.remove(record);
        }, this);
    },
    onbtnPaymentclick: function () {
        var win = Ext.getCmp('form-PaymentWin');
        if (win) {
            win.show();
        } else {
            var form = new jun.PaymentWin({modez: 0});
            form.show();
        }
        //form.bank.focus();
    },
    onlogCheck: function (c, s) {
//        this.storeCode.reset()
        this.storeCode.setDisabled(!s)
    },
    onCustomerSelect: function (combo, record, index) {
        twoRows("Selamat datang,", record.json.nama_customer);
    },
    onWinClose: function () {
        jun.rztSalestransNoBeautyDetails.removeAll();
        jun.rztPayment.removeAll();
        jun.rztPaketTrans.removeAll();
        this.customer.lastQuery = null;
    },
    onBayarChange: function () {
        jun.rztSalestransNoBeautyDetails.refreshData();
    },
    onDiscChange: function (a) {
        var disc1 = parseFloat(a.getValue());
        //if (disc1 == 0) return;
        var discrpf = parseFloat(this.discrp.getValue());
        if (discrpf != 0) {
            this.discrp.setValue(0);
        }
        jun.rztSalestransNoBeautyDetails.each(function (record) {
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = round(price * qty, 2);
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
            var totalpot = discrp + discrp1;
            var total_with_disc = bruto - totalpot;
            var total = bruto - discrp;
            var vatrp = round(total_with_disc * vat, 2);
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
    },
    onDiscrpChange: function (a) {
        var discrpf = parseFloat(a.getValue());
        Ext.getCmp('btnsalesdetilid').setDisabled(discrpf != 0);
        var discf = parseFloat(this.disc.getValue());
        if (discf != 0) {
            this.disc.setValue(0);
            var disc1 = 0;
            var discrp1 = 0;
            jun.rztSalestransNoBeautyDetails.each(function (record) {
                var barang = jun.getBarang(record.data.barang_id);
                var price = parseFloat(record.data.price);
                var qty = parseFloat(record.data.qty);
                var disc = parseFloat(record.data.disc);
                var discrp = parseFloat(record.data.discrp);
                var bruto = round(price * qty, 2);
                var vat = jun.getTax(record.data.barang_id);
                discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
                var subtotal = bruto - discrp;
                var totalpot = discrp + discrp1;
                var total_with_disc = bruto - totalpot;
                var total = bruto - discrp;
                var vatrp = round(total_with_disc * vat, 2);
                record.set('discrp', discrp);
                record.set('vat', vat);
                record.set('vatrp', vatrp);
                record.set('total_pot', totalpot);
                record.set('total', total);
                record.set('bruto', bruto);
                record.set('disc1', disc1);
                record.set('discrp1', discrp1);
                record.commit();
            });
        }
        var subtotalf = jun.rztSalestransNoBeautyDetails.sum('total');
        if (subtotalf == 0)
            return;
        jun.rztSalestransNoBeautyDetails.each(function (record) {
            var total = parseFloat(record.data.total);
            var disc1 = round((total / subtotalf) * 100, 2);
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = round(price * qty, 2);
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * discrpf, 2);
            var totalpot = discrp + discrp1;
            var total_with_disc = bruto - totalpot;
            total = bruto - discrp;
            var vatrp = round(total_with_disc * vat, 2);
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var kembali = parseFloat(this.kembali.getValue());
        var bayar = parseFloat(this.bayar.getValue());
        var total = parseFloat(this.total.getValue());
        if (total > bayar) {
            Ext.Msg.alert('Error', "Masih kurang bayar " + (total - bayar));
            this.btnDisabled(false);
            this.customer.focus();
            return;
        }
        if (kembali < 0) {
            Ext.Msg.alert('Error', "Change can't less than 0");
            this.btnDisabled(false);
            this.customer.focus();
            return;
        }
        if (jun.rztSalestransNoBeautyDetails.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            this.customer.focus();
            return;
        }
        print(PRINTER_RECEIPT, __openCashDrawer);
        var urlz = 'Salestrans/create/';
        Ext.getCmp('form-SalestransNoBeauty').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                        jun.rztSalestransNoBeautyDetails.data.items, "data")),
                payment: Ext.encode(Ext.pluck(
                        jun.rztPayment.data.items, "data")),
                paket: [],
                id: this.id,
                mode: this.modez
            },
            success: function (f, a) {
                menuDetil("COLLECTED", parseFloat(Ext.getCmp("bayarid").getValue()), 'CHANGE', parseFloat(Ext.getCmp("kembaliid").getValue()));
//                jun.globalStore.set('sales_dokter_id', this.dokter.getValue());
                localStorage.setItem("sales_dokter_id", this.dokter.getValue());
                // jun.rztSalestransNoBeauty.reload();
                var response = Ext.decode(a.response.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    console.log("Printer not ready");
                } else {
                    var msg = [{type: 'raw', data: response.msg}];
                    var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                    print(PRINTER_RECEIPT, printData);
                    print(PRINTER_RECEIPT, printData);
                }
                if (this.closeForm) {
                    this.close();
                } else {
                    this.btnDisabled(false);
                    Ext.getCmp('form-SalestransNoBeauty').getForm().reset();
                    //this.setDateTime();
                    this.onWinClose();
                    jun.rztSalestransNoBeautyDetails.refreshData();
                    if (POSCUSTOMERREADONLY) {
                        jun.rztCustomersSalesCmp.baseParams = {
                            customer_id: POSCUSTOMERDEFAULT
                        };
                        jun.rztCustomersSalesCmp.load();
                        jun.rztCustomersSalesCmp.baseParams = {};
                    }
                }
                this.customer.focus();
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
                this.customer.focus();
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        Ext.MessageBox.confirm('Questions', 'Are you sure want exit?', function (btn) {
            if (btn == 'yes') {
                this.close();
            }
        },
                this);
    }
});
jun.SalestransCounterWin = Ext.extend(Ext.Panel, {
    title: 'Sales',
    modez: 1,
    // height: 565,
    layout: 'fit',
    id: 'docs-SalestransCounterWin',
    statusActive: -1,
    initComponent: function () {
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 5px',
                id: 'form-SalestransCounter',
                // labelWidth: 100,
                // labelAlign: 'left',
                // layout: 'absolute',
                // anchor: "100% 100%",
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                defaults: {
                    // margins: '0 0 5 0',
                    border: false,
                    bodyStyle: 'background-color: #E4E4E4;'
                },
                ref: 'formz',
                border: false,
                items: [
                    {
                        // layout: 'column',
                        layout: 'table',
                        bodyStyle: 'background-color: #E4E4E4;padding:5px',
                        layoutConfig: {
                            tableAttrs: {
                                style: {
                                    width: '100%'
                                }
                            },
                            columns: 7
                        },
                        height: 60,
                        items: [
                            {
                                xtype: "label",
                                text: "Date:",
                                width: 104,
                                style: 'margin:4px 0 3px 0;'
                            },
                            {
                                columnWidth: 0.33,
                                xtype: 'xdatefield',
                                ref: '../../tgl',
                                fieldLabel: 'Date',
                                name: 'tgl',
                                id: 'tglid',
                                format: 'd M Y',
                                anchor: "100%",
                                readOnly: true,
                                allowBlank: false,
                                value: DATE_NOW,
                                width: 175,
                                style: 'margin:4px 0 3px 0;'
                            },
                            // {
                            //     xtype: 'textfield',
                            //     fieldLabel: 'No. Receipt',
                            //     hideLabel: false,
                            //     hidden: true,
                            //     name: 'doc_ref',
                            //     id: 'doc_refid',
                            //     ref: '../doc_ref',
                            //     maxLength: 20,
                            //     height: 20,
                            //     width: 175,
                            //     readOnly: true
                            // },
                            {
                                width: 100,
                                xtype: "label",
                                text: "Customer:",
                                style: 'margin:4px 0 0 15px;'
                            },
                            {
                                // columnWidth: 0.33,
                                xtype: 'combo',
                                ref: '../../customer',
                                triggerAction: 'query',
                                lazyRender: true,
                                mode: 'remote',
                                forceSelection: true,
                                autoSelect: false,
                                store: jun.rztCustomersSalesCounterCmp,
                                id: "customersales_id",
                                hiddenName: 'customer_id',
                                valueField: 'customer_id',
                                displayField: 'nama_customer',
                                hideTrigger: true,
                                minChars: 3,
                                matchFieldWidth: !1,
                                pageSize: 20,
                                // itemSelector: "div.search-item-table",
                                // tpl: new Ext.XTemplate('<div class="container">', '<tpl for="."><div class="search-item-table">',
                                //     '<div class="cell4" style="width: 100px;text-align: right;font-size: 150%;"><h3>{no_customer}</div></h3>',
                                //     '<div class="cell4" style="width: 350px;font-size: 150%;"><h3>{nama_customer}</div></h3>',
                                //     "</div></tpl>", '</div>'),
                                itemSelector: 'div.search-item',
                                tpl: new Ext.XTemplate(
                                        '<tpl for="."><div class="search-item">',
                                        '<h3><span>{tgl_lahir:date("j M Y")}<br />{telp}</span>{nama_customer} ({no_customer})</h3>',
                                        '{alamat}',
                                        '</div></tpl>'
                                        ),
                                allowBlank: false,
                                listWidth: 450,
                                lastQuery: "",
                                anchor: '100%',
                                width: '100%'
                            },
                            {
                                width: 10,
                                xtype: "label",
//                                text: "EC",
                                style: 'font-weight:bold;',
                                ref: "../../status"
                            },
                            {
                                width: 100,
                                xtype: "label",
                                text: "Doctor:",
                                style: 'margin:4px 0 0 15px;'
                            },
//                            {
//                                xtype: 'combo',
//                                //typeAhead: true,
//                                ref: '../../dokter',
//                                triggerAction: 'all',
//                                // editable: false,
//                                lazyRender: true,
//                                mode: 'local',
//                                store: [],
////                                hiddenName: 'dokter_id',
////                                valueField: 'dokter_id',
////                                displayField: 'nama_dokter',
//                                allowBlank: true,
//                                anchor: '100%',
//                                listWidth: 400,
//                                lastQuery: '',
//                                width: '100%'
//                            },
                            {
                                columnWidth: 0.33,
                                xtype: 'combo',
                                //typeAhead: true,
                                editable: false,
                                fieldLabel: 'Doctor',
                                ref: '../../dokter',
                                triggerAction: 'all',
                                lazyRender: true,
                                mode: 'local',
                                id: "doktersalescounter_id",
                                store: jun.rztDokterStatus,
                                hiddenName: 'dokter_id',
                                valueField: 'dokter_id',
                                displayField: 'nama_dokter',
                                allowBlank: true,
                                width: '100%'
                            },
                            {
                                xtype: "label",
                                text: "Comment:",
                                width: 104,
                                style: 'margin:4px 0 0 0;'
                            },
                            {
                                columnWidth: 0.33,
                                colspan: 4,
                                // style: 'margin-bottom:5px',
                                xtype: 'uctextfield',
                                fieldLabel: 'Comment',
                                name: 'ketdisc',
                                id: 'ketdiscid',
                                ref: '../../ketdisc',
                                enableKeyEvents: true,
                                maxLength: 255,
                                width: '100%'
                            },
                            {
                                width: 100,
                                xtype: "label",
                                text: "Beauty:",
                                style: 'margin:4px 0 0 15px;'
                            },
                            {
                                // columnWidth: 0.33,
                                xtype: 'combo',
                                //typeAhead: true,
                                ref: '../../beauty',
                                triggerAction: 'all',
                                // editable: false,
                                lazyRender: true,
                                mode: 'local',
                                store: jun.rztBeautyCmp,
                                hiddenName: 'beauty_id',
                                valueField: 'beauty_id',
                                displayField: 'nama_beauty',
                                allowBlank: true,
                                anchor: '100%',
                                listWidth: 400,
                                lastQuery: '',
                                width: '100%'
                            }
                        ]
                    },
                    // {
                    //     height: 30,
                    //     labelWidth: 100,
                    //     labelAlign: 'left',
                    //     layout: 'form',
                    //     items: [
                    //         {
                    //             style: 'margin-bottom:5px',
                    //             xtype: 'uctextfield',
                    //             fieldLabel: 'Comment',
                    //             name: 'ketdisc',
                    //             id: 'ketdiscid',
                    //             ref: '../../ketdisc',
                    //             enableKeyEvents: true,
                    //             maxLength: 255,
                    //             anchor: '100%'
                    //         }
                    //     ]
                    // },
                    {
                        flex: 1,
                        xtype: 'tabpanel',
                        // width: 932,
                        tabBarPosition: 'top',
                        // x: 5,
                        // y: 65,
                        activeTab: 0,
                        resizeTabs: true,
                        tabWidth: 450,
                        tabBar: {
                            layout: {
                                type: 'hbox',
                                align: 'stretch'
                            },
                            defaults: {flex: 1}
                        },
                        items: [
                            new jun.SalestransCounterDetailsGrid({
                                title: "Product/Treatment Details",
                                //x: 5,
                                //y: 65,
                                ref: '../../gridDetails',
                                height: 260,
                                frameHeader: !1,
                                header: !1
                            }),
                            new jun.PaketTransGrid({
                                title: "Package Product/Treatment",
                                arus: 1,
                                visible: false,
                                //x: 5,
                                //y: 65,
                                height: 260,
                                frameHeader: !1,
                                header: !1
                            })
                        ]
                    },
                    {
                        height: 120,
                        layout: 'hbox',
                        layoutConfig: {
                            padding: '5 0 1 0',
                            align: 'stretch'
                        },
                        items: [
                            new jun.PaymentGrid({
                                height: 115,
                                frameHeader: !1,
                                header: !1,
                                ref: '../../payment_grid',
                                store: jun.rztPaymentCounter,
                                flex: 1
                            }),
                            {
                                border: false,
                                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                width: 315,
                                labelWidth: 100,
                                labelAlign: 'left',
                                layout: 'form',
                                items: [
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Sub Total',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'bruto',
                                        id: 'subtotalid',
                                        ref: '../../../subtotal',
                                        maxLength: 30,
                                        value: 0,
                                        readOnly: true,
                                        width: 175,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'VAT',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'vat',
                                        id: 'vatid',
                                        ref: '../../../vat',
                                        maxLength: 30,
                                        value: 0,
                                        readOnly: true,
                                        width: 175,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Disc(%)',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'disc',
                                        id: 'discid',
                                        ref: '../../../disc',
                                        maxLength: 5,
                                        value: 0,
                                        minValue: 0,
                                        maxValue: 100,
                                        width: 175,
                                        readOnly: true,
                                        enableKeyEvents: true,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Disc Amount',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'discrp',
                                        id: 'discrpid',
                                        ref: '../../../discrp',
                                        value: 0,
                                        maxLength: 30,
                                        width: 175,
                                        readOnly: true,
                                        enableKeyEvents: true,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2
                                    }
                                ]
                            },
                            {
                                border: false,
                                bodyStyle: 'background-color: #E4E4E4;padding: 10px',
                                width: 315,
                                labelWidth: 100,
                                labelAlign: 'left',
                                layout: 'form',
                                items: [
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Rounding',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'rounding',
                                        id: 'roundingid',
                                        ref: '../../../rounding',
                                        maxLength: 30,
                                        value: 0,
                                        readOnly: true,
                                        width: 200,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Total',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'total',
                                        id: 'totalid',
                                        ref: '../../../total',
                                        value: 0,
                                        readOnly: true,
                                        maxLength: 30,
                                        width: 200,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Collect',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'bayar',
                                        id: 'bayarid',
                                        ref: '../../../bayar',
                                        readOnly: true,
                                        value: 0,
                                        maxLength: 30,
                                        width: 200,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2
                                    },
                                    {
                                        xtype: 'numericfield',
                                        fieldLabel: 'Change',
                                        hideLabel: false,
                                        //hidden:true,
                                        name: 'kembali',
                                        id: 'kembaliid',
                                        ref: '../../../kembali',
                                        value: 0,
                                        readOnly: true,
                                        maxLength: 30,
                                        width: 200,
                                        alwaysDisplayDecimals: true,
                                        decimalPrecision: 2
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        xtype: 'hidden',
                        id: 'overrideid',
                        ref: '../override',
                        name: 'override'
                    },
                    {
                        xtype: 'hidden',
                        id: 'totalpotid',
                        ref: '../totalpot',
                        name: 'totalpot'
                    },
                    {
                        xtype: 'hidden',
                        name: "log",
                        value: 0
                    },
                    {
                        xtype: 'hidden',
                        id: 'total_discrp1id',
                        ref: '../total_discrp1',
                        name: 'total_discrp1'
                    },
                    {
                        xtype: 'hidden',
                        id: 'konsul_idid',
                        ref: '../konsul_id',
                        name: 'konsul_id'
                    }
                ]
            }
        ];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                'Req Perawatan',
                {
                    columnWidth: 0.33,
                    colspan: 4,
//                    style: 'margin-right:15px',
                    style: 'margin-right:250px',
                    xtype: 'textfield',
                    fieldLabel: 'Req Perawatan',
                    name: 'notebeauty',
                    id: 'notebeautyid',
                    ref: '../notebeauty',
//                    enableKeyEvents: true,
                    flex: '5',
                    maxLength: 255
//                    , hidden: true
//                    width: '100%'
                }, 
                {
                    xtype: 'button',
                    text: 'Sales List',
                    hidden: false,
                    id: 'btnSalesList',
                    ref: '../btnSalesList'
                },
                {
                    xtype: 'button',
                    text: 'Save (F10)',
                    hidden: false,
                    id: 'btnsave',
                    ref: '../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    hidden: true,
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Remove Payment',
                    ref: '../btnDelPayment'
                },
                {
                    xtype: 'button',
                    text: 'Add Payment (F12)',
                    ref: '../btnPayment'
                },
                {
                    xtype: 'button',
                    text: 'Clear Form',
                    ref: '../btnCancel'
                }                
            ]
        };
        jun.SalestransCounterWin.superclass.initComponent.call(this);
        this.payment_grid.store = jun.rztPaymentCounter;
        this.ketdisc.on('specialkey', function (t, e) {
            // e.preventDefault();
            if (e.getKey() == e.TAB) {
                var cmb = Ext.getCmp('barangdetailsid');
                if (cmb) {
                    cmb.focus();
                }
            }
        }, this);
        this.disc.on('keyup', this.onDiscChange, this);
        this.discrp.on('keyup', this.onDiscrpChange, this);
        this.bayar.on('keyup', this.onBayarChange, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnSalesList.on('click', this.onbtnSalesListclick, this);
        this.btnPayment.on('click', this.onbtnPaymentclick, this);
        this.btnDelPayment.on('click', this.onbtnDelPaymentclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        this.customer.on('select', this.onCustomerSelect, this);
        this.on("close", this.onWinClose, this);
        this.on("activate", this.onActive, this);
//        var obj = localStorage.getItem("sales_dokter_id");
//        if (obj) {
//            this.dokter.setValue(obj);
//        }
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
            this.btnSaveClose.setVisible(false);
            this.btnPayment.setVisible(false);
            this.btnDelPayment.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
            this.btnSaveClose.setVisible(false);
            this.btnPayment.setVisible(true);
            this.btnDelPayment.setVisible(true);
        }
        if (SALES_OVERRIDE == "1") {
            //this.disc.setReadOnly(false);
            this.discrp.setReadOnly(false);
        } else {
            this.disc.setReadOnly(true);
            this.discrp.setReadOnly(true);
            Ext.getCmp('discid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
            Ext.getCmp('discrpid').on('render', function (c) {
                c.getEl().on('dblclick', function () {
                    if (this.readOnly) {
                        var login = new jun.login();
                        login.show();
                    }
                }, this);
            });
        }
    },
    onbtnSalesListclick: function (t) {
        t.setDisabled(true);
        jun.rztSalestransList.load({
            params: {
                counter: jun.Counter
            }
        });
        var grid = new jun.SalestransCounterGridWin();
        grid.show();
        t.setDisabled(false);
    },
    resetAll_: function () {
        jun.rztSalestransCounterDetails.removeAll();
        this.payment_grid.store.removeAll();
        jun.rztPaketTrans.removeAll();
        this.customer.setReadOnly(false);
        this.dokter.setReadOnly(false);
        this.beauty.reset();
        // this.dokter.setReadOnly(false);
        this.customer.lastQuery = null;
        this.customer.reset();
        this.dokter.reset();
        this.ketdisc.reset();
        this.subtotal.reset();
        this.vat.reset();
        this.disc.reset();
        this.discrp.reset();
        this.rounding.reset();
        this.total.reset();
        this.bayar.reset();
        this.kembali.reset();
        this.konsul_id.reset();
        this.btnSave.setDisabled(false);
    },
    onActive: function (t) {
        console.log('active');
        this.tgl.setReadOnly(!EDIT_TGL);
        switch (this.statusActive) {
            case - 1:
                this.customer.focus(false, 50);
                break;
            case 0:
                this.gridDetails.barang.focus(false, 50);
                break;
            case 1:
                this.gridDetails.focusGrid();
                break;
            case 2:
                this.btnSave.focus();
                break;
        }
        this.statusActive = -1;
        t.getKeyMap().addBinding({
            key: [Ext.EventObject.F3, Ext.EventObject.F4, Ext.EventObject.F10, Ext.EventObject.F11, Ext.EventObject.F12],
            //ctrl: true,
            handler: function (key, e) {
                switch (key) {
                    case Ext.EventObject.F12:
                        if (this.btnPayment.isVisible()) {
                            var cmb = Ext.getCmp('barangdetailsid');
                            if (cmb) {
                                cmb.collapse();
                            }
                            this.onbtnPaymentclick();
                        }
                        break;
                    case Ext.EventObject.F3:
                        this.gridDetails.focusGrid();
                        break;
                    case Ext.EventObject.F10:
                        if (!this.btnSave.disabled) {
                            this.saveForm();
                        }
                        break;
                }
            },
            stopEvent: true,
            scope: this
        });
    },
    onbtnDelPaymentclick: function () {
        var record = Ext.getCmp('docs-jun.PaymentGrid').sm.getSelected();
        // Check is list selected
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a payment");
            return;
        }
        Ext.MessageBox.confirm('Questions', 'Are you sure want delete this payment?', function (btn) {
            if (btn == 'no') {
                return;
            }
            this.payment_grid.store.remove(record);
        }, this);
    },
    onbtnPaymentclick: function () {
        var win = Ext.getCmp('form-PaymentWin');
        if (win) {
            win.show();
        } else {
            var form = new jun.PaymentWin({
                modez: 0,
                onClose: function () {
                    Ext.getCmp('docs-SalestransCounterWin').statusActive = 2;
                }
            });
            form.show();
        }
        //form.bank.focus();
    },
    loadSalestrans: function (record) {
        var idz = record.data.salestrans_id;
        this.formz.getForm().loadRecord(record);
        jun.rztSalestransCounterDetails.load({
            params: {
                salestrans_id: idz
            }
        });
        jun.rztPaymentCounter.load({
            params: {
                salestrans_id: idz
            }
        });
        jun.rztPaketTrans.load({
            salestrans_id: idz
        });
        this.btnSave.setDisabled(true);
    },
    onCustomerSelect: function (combo, record, index) {
        twoRows("Selamat datang,", record.json.nama_customer);
//        cek status customer
        var b = jun.rztStatusCust, c = b.findExact("status_cust_id", record.json.status_cust_id);
        var st = b.getAt(c).data.nama_status;
        this.status.setText(st);
    },
    onWinClose: function () {
        jun.rztSalestransCounterDetails.removeAll();
        this.payment_grid.store.removeAll();
        jun.rztPaketTrans.removeAll();
        this.customer.lastQuery = null;
    },
    onBayarChange: function () {
        jun.rztSalestransCounterDetails.refreshData();
    },
    onDiscChange: function (a) {
        var disc1 = parseFloat(a.getValue());
        //if (disc1 == 0) return;
        var discrpf = parseFloat(this.discrp.getValue());
        if (discrpf != 0) {
            this.discrp.setValue(0);
        }
        jun.rztSalestransCounterDetails.each(function (record) {
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = round(price * qty, 2);
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * subtotal, 2);
            var totalpot = discrp + discrp1;
            var total_with_disc = bruto - totalpot;
            var total = bruto - discrp;
            var vatrp = round(total_with_disc * vat, 2);
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
    },
    onDiscrpChange: function (a) {
        var discrpf = parseFloat(a.getValue());
        Ext.getCmp('btnsalesdetilid').setDisabled(discrpf != 0);
        var discf = parseFloat(this.disc.getValue());
        if (discf != 0) {
            this.disc.setValue(0);
            var disc1 = 0;
            var discrp1 = 0;
            jun.rztSalestransCounterDetails.each(function (record) {
                var barang = jun.getBarang(record.data.barang_id);
                var price = parseFloat(record.data.price);
                var qty = parseFloat(record.data.qty);
                var disc = parseFloat(record.data.disc);
                var discrp = parseFloat(record.data.discrp);
                var bruto = round(price * qty, 2);
                var vat = jun.getTax(record.data.barang_id);
                discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
                var subtotal = bruto - discrp;
                var totalpot = discrp + discrp1;
                var total_with_disc = bruto - totalpot;
                var total = bruto - discrp;
                var vatrp = round(total_with_disc * vat, 2);
                record.set('discrp', discrp);
                record.set('vat', vat);
                record.set('vatrp', vatrp);
                record.set('total_pot', totalpot);
                record.set('total', total);
                record.set('bruto', bruto);
                record.set('disc1', disc1);
                record.set('discrp1', discrp1);
                record.commit();
            });
        }
        var subtotalf = jun.rztSalestransCounterDetails.sum('total');
        if (subtotalf == 0)
            return;
        var total_discrp1 = 0;
        jun.rztSalestransCounterDetails.each(function (record) {
            var total = parseFloat(record.data.total);
            var disc1 = round((total / subtotalf) * 100, 2);
            var barang = jun.getBarang(record.data.barang_id);
            var price = parseFloat(record.data.price);
            var qty = parseFloat(record.data.qty);
            var disc = parseFloat(record.data.disc);
            var discrp = parseFloat(record.data.discrp);
            var bruto = round(price * qty, 2);
            var vat = jun.getTax(record.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
            var subtotal = bruto - discrp;
            var discrp1 = disc1 == 0 ? 0 : round((disc1 / 100) * discrpf, 2);
            total_discrp1 += discrp1;
            var totalpot = discrp + discrp1;
            var total_with_disc = bruto - totalpot;
            total = bruto - discrp;
            var vatrp = round(total_with_disc * vat, 2);
            record.set('discrp', discrp);
            record.set('vat', vat);
            record.set('vatrp', vatrp);
            record.set('total_pot', totalpot);
            record.set('total', total);
            record.set('bruto', bruto);
            record.set('disc1', disc1);
            record.set('discrp1', discrp1);
            record.commit();
        });
        var sisa_discrp = discrpf - total_discrp1;
        if (sisa_discrp != 0.00) {
            var cnt = jun.rztSalestransCounterDetails.getCount() - 1;
            var salesDetailsLastID = jun.rztSalestransCounterDetails.getAt(cnt);
            var discrp1_sisa = parseFloat(salesDetailsLastID.data.discrp1) + sisa_discrp;
            // var discrp1 = salesDetailsLastID.discrp1;
            var total = parseFloat(salesDetailsLastID.data.total);
            // var disc1 = round((total / subtotalf) * 100, 2);
            var barang = jun.getBarang(salesDetailsLastID.data.barang_id);
            var price = parseFloat(salesDetailsLastID.data.price);
            var qty = parseFloat(salesDetailsLastID.data.qty);
            var disc = parseFloat(salesDetailsLastID.data.disc);
            var discrp = parseFloat(salesDetailsLastID.data.discrp);
            var bruto = round(price * qty, 2);
            var vat = jun.getTax(salesDetailsLastID.data.barang_id);
            discrp = disc == 0 ? discrp : round((disc / 100) * bruto, 2);
            var subtotal = bruto - discrp;
            discrp1 = discrp1_sisa;//disc1 == 0 ? 0 : round((disc1 / 100) * discrpf, 2);
            disc1 = round((discrp1 / subtotal) * 100, 2);
            var totalpot = discrp + discrp1;
            var total_with_disc = bruto - totalpot;
            total = bruto - discrp;
            var vatrp = round(total_with_disc * vat, 2);
            jun.rztSalestransCounterDetails.getAt(cnt).set('discrp', discrp);
            jun.rztSalestransCounterDetails.getAt(cnt).set('vat', vat);
            jun.rztSalestransCounterDetails.getAt(cnt).set('vatrp', vatrp);
            jun.rztSalestransCounterDetails.getAt(cnt).set('total_pot', totalpot);
            jun.rztSalestransCounterDetails.getAt(cnt).set('total', total);
            jun.rztSalestransCounterDetails.getAt(cnt).set('bruto', bruto);
            jun.rztSalestransCounterDetails.getAt(cnt).set('disc1', disc1);
            jun.rztSalestransCounterDetails.getAt(cnt).set('discrp1', discrp1);
            jun.rztSalestransCounterDetails.getAt(cnt).commit();
        }
    },
    btnDisabled: function (status) {
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
    saveForm: function () {
        this.btnDisabled(true);
        var kembali = parseFloat(this.kembali.getValue());
        var bayar = parseFloat(this.bayar.getValue());
        var total = parseFloat(this.total.getValue());
        var total_discrp1 = parseFloat(this.total_discrp1.getValue());
        if ((total - total_discrp1) > ROUNDING) {
            this.onDiscrpChange(this.discrp);
        }
        if (total > bayar) {
            Ext.Msg.alert('Error', "Masih kurang bayar " + (total - bayar));
            this.btnDisabled(false);
            this.customer.focus();
            return;
        }
        if (kembali < 0) {
            Ext.Msg.alert('Error', "Change can't less than 0");
            this.btnDisabled(false);
            this.customer.focus();
            return;
        }
        if (jun.rztSalestransCounterDetails.data.length == 0) {
            Ext.Msg.alert('Error', "Item details must set");
            this.btnDisabled(false);
            this.customer.focus();
            return;
        }
        print(PRINTER_RECEIPT, __openCashDrawer);
        var urlz = 'Salestrans/create/';
        var id_antrian = '';
        var storedNames = Locstor.get('AntrianKasir');
        if (storedNames != null) {
            id_antrian = storedNames.id_antrian;
        }
        var storedNames = Locstor.get('AntrianFO');
        if (storedNames != null) {
            id_antrian = storedNames.id_antrian;
        }
        var note = (Ext.getCmp('notebeautyid').getValue());
        Ext.getCmp('form-SalestransCounter').getForm().submit({
            url: urlz,
            scope: this,
            params: {
                detil: Ext.encode(Ext.pluck(
                        jun.rztSalestransCounterDetails.data.items, "data")),
                payment: Ext.encode(Ext.pluck(
                        this.payment_grid.store.data.items, "data")),
                paket: Ext.encode(Ext.pluck(
                        jun.rztPaketTrans.data.items, "data")),
                id: this.id,
                counter: jun.Counter,
                id_antrian: id_antrian,
                mode: this.modez,
                note: note
            },
            success: function (f, a) {
                menuDetil("COLLECTED", parseFloat(Ext.getCmp("bayarid").getValue()), 'CHANGE', parseFloat(Ext.getCmp("kembaliid").getValue()));
//                jun.globalStore.set('sales_dokter_id', this.dokter.getValue());
//                localStorage.setItem("sales_dokter_id", this.dokter.getValue());
                jun.rztSalestransCounter.reload();
                var panKonsul = Ext.getCmp('docs-jun.KonsulCounterGrid');
                panKonsul.sm.unlock();
                panKonsul.btnUse.toggle(false);
                jun.rztKonsulCounter.reload();
                var response = Ext.decode(a.response.responseText);
                //findPrinterReceipt();
                if (notReady()) {
                    console.log("Printer not ready");
                } else {
                    if (response.stok !== false && PRINT_STOK) {
                        var stok = [{type: 'raw', data: response.stok}];
                        var printDataStocker = __printDataStocker.concat(stok, __feedPaper, __cutPaper);
                        if (PRINTER_STOCKER != 'default') {
                            print(PRINTER_STOCKER, printDataStocker);
                        }
                    }
                    if (response.resep !== false) {
                        var resep = [{type: 'raw', data: response.resep}];
                        var printDataResep = __printDataStocker.concat(resep, __feedPaper, __cutPaper);
                        if (PRINTER_STOCKER != 'default') {
                            print(PRINTER_STOCKER, printDataResep);
                        }
                    }
                    var msg = [{type: 'raw', data: response.msg}];
                    var printData = __printData.concat(msg, __feedPaper, __cutPaper);
                    print(PRINTER_RECEIPT, printData);
                    print(PRINTER_RECEIPT, printData);
                }
                if (this.closeForm) {
                    this.close();
                } else {
                    this.btnDisabled(false);
                    this.resetAll_();
                    // Ext.getCmp('form-SalestransCounter').getForm().reset();
                    //this.setDateTime();
                    this.onWinClose();
                    // jun.rztSalestransCounterDetails.refreshData();
                    if (POSCUSTOMERREADONLY) {
                        jun.rztCustomersSalesCounterCmp.baseParams = {
                            customer_id: POSCUSTOMERDEFAULT
                        };
                        jun.rztCustomersSalesCounterCmp.load();
                        jun.rztCustomersSalesCounterCmp.baseParams = {};
                    }
                }
                this.customer.focus();
                this.status.setText("");
                var tabpanel = this.findParentByType('tabpanel');
                var tab = tabpanel.getComponent('docs-jun.AntrianCounterWin');
                if (tab) {
                    tabpanel.setActiveTab(tab);
                    var storedNames = Locstor.get('AntrianKasir');
                    if (storedNames != null) {
                        Ext.getCmp('btnselesaicounterantriankasir').btnEl.dom.click();
                    }
                    var storedNames = Locstor.get('AntrianFO');
                    if (storedNames != null) {
                        var btn = Ext.getCmp('btnnokonsulcounterantriankasir');
                        if (!btn.disabled) {
                            btn.btnEl.dom.click();
                        }
                    }
                }
                Ext.getCmp('notebeautyid').setValue('');
            },
            failure: function (f, a) {
                switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                        Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                        break;
                    case Ext.form.Action.CONNECT_FAILURE:
                        Ext.Msg.alert('Failure', 'Ajax communication failed');
                        break;
                    case Ext.form.Action.SERVER_INVALID:
                        Ext.Msg.alert('Failure', a.result.msg);
                }
                this.btnDisabled(false);
                this.customer.focus();
            }
        });
    },
    onbtnSaveCloseClick: function () {
        this.closeForm = true;
        this.saveForm(true);
    },
    onbtnSaveclick: function () {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function () {
        Ext.MessageBox.confirm('Questions', 'Are you sure want clear all value?', function (btn) {
            if (btn == 'yes') {
                Ext.getCmp('docs-jun.KonsulCounterGrid').resetSuspendList();
            }
        },
                this);
    }
});