jun.InvestasiWin = Ext.extend(Ext.Window, {
    title: 'Investment Plans',
    modez:1,
    width: 400,
    height: 285,
    layout: 'form',
    modal: true,
    padding: 5,
    closeForm: false,    
    initComponent: function() {
        if (jun.rztAccount.getTotalCount() === 0) {
            jun.rztAccount.load();
        }
        this.items = [
            {
                xtype: 'form',
                frame: false,
                bodyStyle: 'background-color: #E4E4E4; padding: 10px',
                id:'form-Investasi',
                labelWidth: 100,
                labelAlign: 'left',
                layout: 'form',
                ref:'formz',
                border:false,
                items: [
                    {
                        xtype: 'combo',
                        typeAhead: true,
                        triggerAction: 'all',
                        lazyRender:true,
                        mode: 'local',
                        forceSelection: true,
                        fieldLabel: 'Account',
                        store: jun.rztAccount,
                        itemSelector: "div.search-item",
                        tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                            '<h3><span">{account_code} - {account_name}</span></h3>',
                            "</div></tpl>"),
                        hiddenName:'account_id',
                        valueField: 'account_id',
                        ref:'../account_id',
                        displayField: 'account_code',
                        emptyText: "Account",
                        anchor: '100%'
                    },
                                                                     {
                                    xtype: 'textfield',
                                    fieldLabel: 'Name',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'name',
                                    id:'nameid',
                                    ref:'../name',
                                    maxLength: 200,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 

                                                                     {
                                    xtype: 'numericfield',
                                    fieldLabel: 'Qty',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'qty',
                                    id:'qtyid',
                                    ref:'../qty',
                                    maxLength: 11,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                }, 
                                                                     {
                                    xtype: 'numericfield',
                                    fieldLabel: 'Amount',
                                    hideLabel:false,
                                    //hidden:true,
                                    name:'amount',
                                    id:'amountid',
                                    ref:'../amount',
                                    maxLength: 20,
                                    //allowBlank: 1,
                                    anchor: '100%'
                                },
                    {
                        xtype: 'textarea',
                        fieldLabel: 'Description',
                        hideLabel:false,
                        //hidden:true,
                        name:'description',
                        id:'descriptionid',
                        ref:'../description',
                        maxLength: 200,
                        //allowBlank: 1,
                        anchor: '100%'
                    },
                    {
                            xtype: 'xdatefield',
                            ref:'../tdate',
                            fieldLabel: 'Date',
                            name:'tdate',
                            id:'tdateid',
                            format: 'M Y',
                            //allowBlank: 1,
                            anchor: '100%'                            
                        },
                  ]
            }];
        this.fbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'button',
                    text: 'Save',
                    hidden: false,
                    ref:'../btnSave'
                },
                {
                    xtype: 'button',
                    text: 'Save & Close',
                    ref: '../btnSaveClose'
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    ref:'../btnCancel'
                }
            ]
        };
        jun.InvestasiWin.superclass.initComponent.call(this);
//        this.on('activate', this.onActivate, this);
        this.btnSaveClose.on('click', this.onbtnSaveCloseClick, this);
        this.btnSave.on('click', this.onbtnSaveclick, this);
        this.btnCancel.on('click', this.onbtnCancelclick, this);
        if (this.modez == 1 || this.modez == 2) {
            this.btnSave.setVisible(false);
        } else {
            this.btnSave.setVisible(true);
        }
    },

    btnDisabled:function(status){
        this.btnSave.setDisabled(status);
        this.btnSaveClose.setDisabled(status);
    },
            
    saveForm : function()
    {
        this.btnDisabled(true);
            var urlz =  'projection/Investasi/create/';
             
            Ext.getCmp('form-Investasi').getForm().submit({
                url:urlz,
                timeOut: 1000,
                params: {
                    id: this.id,
                    mode: this.modez
                },
                scope: this,
                success: function(f,a){
                    jun.rztInvestasi.reload();
                    var response = Ext.decode(a.response.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                    if(this.modez == 0){
                        Ext.getCmp('form-Investasi').getForm().reset();
                        this.btnDisabled(false);
                    }
                    if(this.closeForm){
                        this.close();
                    }
                },
                failure: function (f, a) {
                    switch (a.failureType) {
                    case Ext.form.Action.CLIENT_INVALID:
                    Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
                    break;
                    case Ext.form.Action.CONNECT_FAILURE:
                    Ext.Msg.alert('Failure', 'Ajax communication failed');
                    break;
                    case Ext.form.Action.SERVER_INVALID:
                    Ext.Msg.alert('Failure', a.result.msg);
                    }
                    this.btnDisabled(false);
                }

            });

    },
    
    onbtnSaveCloseClick: function()
    {
        this.closeForm = true;
        this.saveForm(true);
    },
    
    onbtnSaveclick: function()
    {
        this.closeForm = false;
        this.saveForm(false);
    },
    onbtnCancelclick: function(){
        this.close();
    }
   
});