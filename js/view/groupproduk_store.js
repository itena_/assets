jun.Groupprodukstore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.Groupprodukstore.superclass.constructor.call(this, Ext.apply({
            storeId: 'GroupprodukStoreId',
            url: 'projection/Groupproduk',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'groupproduk_id'},
                {name:'nama'},
                {name:'kode'},
                {name:'description'},
                {name:'created_at'},
                {name:'businessunit_id'},
                {name:'uploadfile'},
                {name:'uploaddate'},
                
            ]
        }, cfg));
    }
});
jun.rztGroupproduk = new jun.Groupprodukstore();
jun.rztGroupprodukLib = new jun.Groupprodukstore();
jun.rztGroupprodukLib.load();
