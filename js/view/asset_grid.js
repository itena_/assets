jun.AssetGrid=Ext.extend(Ext.grid.GridPanel ,{        
	title:"Asset",
        id:'docs-jun.AssetGrid',
        iconCls:"silk-grid",
    viewConfig:{
        forceFit:true
    },
        sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
        plugins:[new Ext.ux.grid.GridHeaderFilters],
	columns:[
           /*             {
			header:'asset_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_id',
			width:100
		},*/
        {
            header:'Doc Ref',
            sortable:true,
            resizable:true,
            dataIndex:'doc_ref',
            width:100
        },
        /*{
            header:'Activa',
            sortable:true,
            resizable:true,
            dataIndex:'ati',
            width:100
        },*/
                                {
			header:'Asset Name',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_name',
			width:100
		},
        {
            header:'Category',
            sortable:true,
            resizable:true,
            dataIndex:'category',
            width:100
        },
        {
            header:'Qty',
            sortable:true,
            resizable:true,
            dataIndex:'qty',
            width:40
        },
                                {
			header:'Branch',
			sortable:true,
			resizable:true,                        
            dataIndex:'branch',
			width:100
		},
                                {
			header:'Date Acquisition',
			sortable:true,
			resizable:true,                        
            dataIndex:'date_acquisition',
			width:100
		},
                                {
			header:'Price Acquisition',
			sortable:true,
			resizable:true,                        
            dataIndex:'price_acquisition',
			width:100
		},
        /*{
            header:'New Price Acquisition',
            sortable:true,
            resizable:true,
            dataIndex:'new_price_acquisition',
            width:100
        },*/
        {
            header:'Description',
            sortable:true,
            resizable:true,
            dataIndex:'description',
            width:100
        },

                		/*
                {
			header:'new_price_acquisition',
			sortable:true,
			resizable:true,                        
            dataIndex:'new_price_acquisition',
			width:100
		},
                                {
			header:'asset_group_id',
			sortable:true,
			resizable:true,                        
            dataIndex:'asset_group_id',
			width:100
		},
                		*/
		
	],
	initComponent: function(){
	this.store = jun.rztAsset;
        this.bbar = {
            items: [
           {
            xtype: 'paging',
            store: this.store,
            displayInfo: true,
            pageSize: 20
           }]
        };
            
           this.tbar = {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'button',
                        text: 'Add Asset',
                        ref: '../btnAdd'
                    },
                    {
                        xtype:'tbseparator'
                    },
                    /*{
                        xtype: 'button',
                        text: 'Edit Asset',
                        ref: '../btnEdit'
                    },
                    {
                        xtype:'tbseparator'
                    },*/
                    {
                        xtype: 'button',
                        text: 'Delete Asset',
                        ref: '../btnDelete'
                    }
                ]
            };
                this.store.baseParams = {mode: "grid"};
                this.store.reload();
                this.store.baseParams = {};
		jun.AssetGrid.superclass.initComponent.call(this);
	        this.btnAdd.on('Click', this.loadForm, this);
                //this.btnEdit.on('Click', this.loadEditForm, this);
                this.btnDelete.on('Click', this.deleteRec, this);
                this.getSelectionModel().on('rowselect', this.getrow, this);
	},
        
        getrow: function(sm, idx, r){
            this.record = r;

            var selectedz = this.sm.getSelections();
        },
        
        loadForm: function(){
            var form = new jun.AssetWin({modez:0});
            form.show();
        },
        
        loadEditForm: function(){
            
            var selectedz = this.sm.getSelected();
            
            //var dodol = this.store.getAt(0);
             if(selectedz == undefined){
                 Ext.MessageBox.alert("Warning","Anda belum memilih Jenis Pelayanan");
                 return;
             }
            var idz = selectedz.json.asset_id;
            var form = new jun.AssetWin({modez:1, id:idz});
            form.show(this);
            form.formz.getForm().loadRecord(this.record);
        },
        
        deleteRec : function(){
            Ext.MessageBox.confirm('Pertanyaan','Apakah anda yakin ingin menghapus data ini?', this.deleteRecYes, this);
        },
        
        deleteRecYes : function(btn){

            if (btn == 'no') {
            return;
            }

            var record = this.sm.getSelected();

            // Check is list selected
            if(record == undefined){
                Ext.MessageBox.alert("Warning","Anda Belum Memilih Data");
                return;
            }

            Ext.Ajax.request({
                url: 'asset/Asset/delete/id/' + record.json.asset_id,
                method: 'POST',
                success:function (f, a) {
                    jun.rztAsset.reload();
                    var response = Ext.decode(f.responseText);
                    Ext.MessageBox.show({
                    title:'Info',
                    msg:response.msg,
                    buttons:Ext.MessageBox.OK,
                    icon:Ext.MessageBox.INFO
                    });
                },
                failure:function (f, a) {
            switch (a.failureType) {
            case Ext.form.Action.CLIENT_INVALID:
            Ext.Msg.alert('Failure', 'Form fields may not be submitted with invalid values');
            break;
            case Ext.form.Action.CONNECT_FAILURE:
            Ext.Msg.alert('Failure', 'Ajax communication failed');
            break;
            case Ext.form.Action.SERVER_INVALID:
            Ext.Msg.alert('Failure', a.result.msg);
            }
                }
             });
        
        }
})

