jun.AssetHistorystore = Ext.extend(Ext.data.JsonStore, {
    constructor: function(cfg) {
        cfg = cfg || {};
        jun.AssetHistorystore.superclass.constructor.call(this, Ext.apply({
            storeId: 'AssetHistoryStoreId',
            url: 'asset/AssetHistory',
            root: 'results',
            totalProperty: 'total',
            fields: [                
                {name:'asset_history_id'},
                {name:'asset_id'},
                {name:'businessunit_id'},
                {name:'docref'},
                {name:'ati'},
                {name:'name'},
                {name:'businessunit_name'},
                {name:'branch'},
                {name:'price'},
                {name:'newprice'},
                {name:'class'},
                {name:'tariff'},
                {name:'period'},
                {name:'penyusutanperbulan'},
                {name:'penyusutanpertahun'},
                {name:'desc'},
                {name:'status'},
                {name:'tobu'},
                {name:'tobranch'},
                {name:'amount'},
                {name:'sdate'},
                {name:'created_at'},
                {name:'updated_at'}
                
            ]
        }, cfg));
    }
});
jun.rztAssetHistory = new jun.AssetHistorystore();
//jun.rztAssetHistory.load();
