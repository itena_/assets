jun.PembantuPelunasanUtangDetilGrid = Ext.extend(Ext.grid.GridPanel, {
    title: "PelunasanUtangDetil",
    id: 'docs-jun.pembantuPelunasanUtangDetilGrid',
    iconCls: "silk-grid",
    viewConfig: {
        forceFit: true
    },
    sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
    columns: [
        {
            header: 'Supplier Name',
            sortable: true,
            resizable: true,
            dataIndex: 'supplier_id',
            width: 200,
            renderer: jun.renderSupplier
        },
        {
            header: 'Invoice No.',
            sortable: true,
            resizable: true,
            dataIndex: 'no_faktur',
            width: 100
        },
        {
            header: 'Invoice Value',
            sortable: true,
            resizable: true,
            dataIndex: 'nilai',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'COA.',
            sortable: true,
            resizable: true,
            dataIndex: 'account_code',
            width: 100
        },
        {
            header: 'Amount',
            sortable: true,
            resizable: true,
            dataIndex: 'kas_dibayar',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Remains',
            sortable: true,
            resizable: true,
            dataIndex: 'sisa',
            width: 100,
            align: "right",
            renderer: Ext.util.Format.numberRenderer("0,0.00")
        },
        {
            header: 'Type',
            sortable: true,
            resizable: true,
            dataIndex: 'type_',
            width: 50,
            align: "right",
            renderer: function (v, m, r) {
                if (r.get('type_') == 0) {
                    return 'PH';
                } else {
                    return 'UM';
                }
            }}
    ],
    initComponent: function () {
        this.store = jun.rztPembantuPelunasanUtangDetil;
        this.tbar = {
            xtype: 'toolbar',
            items: [
                {
                    xtype: 'buttongroup',
                    columns: 4,
                    defaults: {
                        scale: 'small'
                    },
                    items: [
                        {
                            xtype: "label",
                            text: "Suplier:"
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
                            ref: '../../supplier',
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            store: jun.rztSupplierLib,
                            emptyText: "Please select supplier",
                            hiddenName: 'supplier_id',
                            valueField: 'supplier_id',
                            displayField: 'supplier_name',
                            allowBlank: true,
                            listWidth: 350,
                            matchFieldWidth: !1,
                            width: 175
                        },
                        {
                            xtype: 'label',
                            text: '\xA0Invoice No:'
                        },
                        {
                            xtype: "combo",
                            typeAhead: !0,
                            triggerAction: "all",
                            lazyRender: !0,
                            mode: "local",
                            store: jun.rztPembantuFakturUtang,
                            forceSelection: !0,
                            valueField: "invoice_journal_id",
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                    // '<span>{no_faktur} | Tgl: {tgl} | Nilai: {nilai:number("0,0.00")} | Sisa: {sisa:number("0,0.00")}</span>',
                                    '<span>Doc.ref: {doc_ref}</span>', '<span> | Invoice: {no_faktur}</span>', '<span> | Date: {tgl:date("M j, Y")}</span><br>',
                                    '<span>Value: {nilai:number("0,0.00")}</span>', '<span>| Remains: {sisa:number("0,0.00")}</span>',
                                    "</div></tpl>"),
                            displayField: "no_faktur",
                            listWidth: 650,
                            editable: !0,
                            ref: "../../jual",
                            lastQuery: ""
                        },
                        {
                            xtype: 'label',
                            style: 'margin:5px',
                            text: 'COA :'
                        },
                        {
                            xtype: 'combo',
                            //typeAhead: true,
//                            triggerAction: 'all',
                            lazyRender: true,
                            style: 'margin-bottom:2px',
                            mode: 'local',
                            enableKeyEvents: true,
                            forceSelection: true,
                            fieldLabel: 'COA',
                            store: jun.rztChartMasterCmp,
                            ref: '../../account_code',
                            matchFieldWidth: !1,
                            itemSelector: "div.search-item",
                            tpl: new Ext.XTemplate('<tpl for="."><div class="search-item">',
                                '<h3><span">{account_code} - {account_name}</span></h3><br />{description}',
                                "</div></tpl>"),
                            listWidth: 500,
//                            hiddenName: 'account_code',
                            valueField: 'account_code',
                            displayField: 'account_code'
                        },
                        {
                            xtype: 'label',
                            text: '\xA0Amount:'
                        },
                        {
                            xtype: 'numericfield',
                            ref: '../../kas',
                            value: 0,
                            width: 175
                        },
                        {
                            xtype: 'label',
                            text: '\xA0Uang Muka :'
                        },
                        {
                            xtype: 'combo',
                            store: [[0, 'PH'],
                                [1, 'UM']
                            ],
                            style: 'margin-bottom:2px',
                            typeAhead: true,
                            triggerAction: 'all',
                            lazyRender: true,
                            mode: 'local',
                            forceSelection: true,
                            value: 0,
                            name: "type_",
                            id: "type_id",
                            ref: '../../type_'
                        }
                    ]
                },
                {
                    xtype: 'buttongroup',
                    columns: 2,
                    defaults: {
                        scale: 'large',
                        width: 40
                    },
                    items: [
                        {
                            xtype: 'button',
                            text: 'Add',
                            ref: '../../btnAdd'
                        },
                        {
                            xtype: 'button',
                            text: 'Del',
                            ref: '../../btnDel'
                        }
                    ]
                }
            ]
        };
        jun.PembantuPelunasanUtangDetilGrid.superclass.initComponent.call(this);
//        this.getSelectionModel().on('rowselect', this.getrow, this);
        this.btnAdd.on('Click', this.loadForm, this);
        this.btnDel.on('Click', this.deleteRec, this);
        this.jual.on('select', this.selectJual, this);
        this.supplier.on('select', this.onSupplierChange, this);
        this.getSelectionModel().on('rowselect', this.getrow, this);

    },
    selectJual: function (combo, record, index) {
        this.kas.setValue(record.data.sisa);
    },
    onStoreChange: function () {
        jun.rztPembantuPelunasanUtangDetil.refreshData();
    },
    getrow: function (sm, idx, r) {
        this.record = r;
        var selectedz = this.sm.getSelections();
    },
    onSupplierChange: function () {
//        this.id_bank.reset();
        this.account_code.reset();
        this.jual.reset();
        this.kas.reset();
        this.type_.reset();
        var supplier_id = this.supplier.getValue();
//        jun.rztPembantuPelunasanUtangDetil.removeAll();

        jun.rztPembantuFakturUtang.baseParams = {
            supplier_id: supplier_id
        };
        jun.rztPembantuFakturUtang.load();
        jun.rztPembantuFakturUtang.baseParams = {};
    },
    loadForm: function () {
        var jual = this.jual.getValue();
        if (jual == "") {
            Ext.MessageBox.alert("Error", "Invoice must selected.");
            return
        }
        var kas = parseFloat(this.kas.getValue());
        if (kas < 0) {
            Ext.MessageBox.alert("Error", "Payment total must more than 0.");
            return;
        }
        var faktur_id = jun.rztPembantuFakturUtang.findExact('invoice_journal_id', jual);
        if (faktur_id == -1) {
            Ext.MessageBox.alert("Error", "Fatal Error.");
            return
        }

        var faktur = jun.rztPembantuFakturUtang.getAt(faktur_id);
        var a = this.store.findExact("invoice_journal_id", faktur.data.invoice_journal_id);
        if (a > -1) {
            Ext.MessageBox.alert("Error", "Invoice already added.");
            return;
        }
        if (kas > 0 && kas > faktur.data.sisa) {
            Ext.MessageBox.alert("Error", "Payment total can't more than invoice total.");
            return;
        }
        var sisa = faktur.data.sisa - kas;
        var type_ = this.type_.getValue();
        var supplier_id = this.supplier.getValue();
        var account_code = this.account_code.getValue();

        var c = jun.rztPembantuPelunasanUtangDetil.recordType,
                d = new c({
                    invoice_journal_id: faktur.data.invoice_journal_id,
                    kas_dibayar: kas,
                    tgl: faktur.data.tgl,
                    no_faktur: faktur.data.no_faktur,
                    nilai: faktur.data.nilai,
                    sisa: sisa,
                    type_: type_,
                    supplier_id: supplier_id,
                    account_code: account_code
                });
        jun.rztPembantuPelunasanUtangDetil.add(d);
        this.jual.reset();
        this.kas.reset();
        this.type_.reset();
        this.supplier.reset();
        this.account_code.reset();
    },
    deleteRec: function () {
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "Anda belum memilih item.");
            return;
        }
//        console.log(record);
        Ext.MessageBox.confirm('Confirmation', 'Are you sure coba?', this.deleteRecYes, this);
    },
    deleteRecYes: function (btn) {
        if (btn == 'no') {
            return;
        }
        var record = this.sm.getSelected();
        if (record == undefined) {
            Ext.MessageBox.alert("Warning", "You have not selected a item");
            return;
        }
        this.store.remove(record);
    }
});