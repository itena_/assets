<?php

class m180329_091053_bank_limit extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand('ALTER TABLE `nscc_bank`
			ADD COLUMN `minimum`  decimal(30,2) NOT NULL DEFAULT 0 AFTER `store`;')->execute();
	}

	public function down()
	{
		echo "m180329_091053_bank_limit does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}