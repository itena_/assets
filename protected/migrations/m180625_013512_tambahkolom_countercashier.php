<?php

class m180625_013512_tambahkolom_countercashier extends CDbMigration
{
	public function up()
	{
        Yii::app()->db->createCommand('
        ALTER TABLE aisha_antrian ADD counter_cashier VARCHAR(5);')->execute();
	}

	public function down()
	{
		echo "m180625_013512_tambahkolom_countercashier does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}