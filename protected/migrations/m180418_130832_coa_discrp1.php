<?php

class m180418_130832_coa_discrp1 extends CDbMigration
{
	public function up()
	{
		Yii::app()->db->createCommand('ALTER TABLE `nscc_grup_attr`
ADD COLUMN `coa_sales_discrp1`  varchar(15) NULL AFTER `coa_sales_disc`;
		
		UPDATE nscc_grup_attr SET coa_sales_discrp1=coa_sales_disc;')->execute();
	}

	public function down()
	{
		echo "m180418_130832_coa_discrp1 does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}