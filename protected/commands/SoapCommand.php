<?php
ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);
Yii::import('application.components.SoapClientYii');
class SoapCommand extends CConsoleCommand{
    
    public function actionBank($id){
        try {
            /** @var Bank $model */
            $model = Bank::model()->findByPk($id);
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveBank(json_encode($attrib));
            var_dump($result);
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

    public function actionBanktrans($id){
        try {
            /** @var BankTrans $model */
            $model = BankTrans::model()->findByPk($id);
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveBankTrans(json_encode($attrib));
            var_dump($result);
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

    public function actionGltrans($id){
        try {
            /** @var GlTrans $model */
            $model = GlTrans::model()->findByPk($id);
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveGltrans(json_encode($attrib));
            var_dump($result);
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

    public function actionRefs($id){
        try {
            /** @var Refs $model */
            $model = Refs::model()->findByPk($id);
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveRefs(json_encode($attrib));
            var_dump($result);
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

     public function actionKas($id){
        try {
            /** @var Kas $model */
            $model = Kas::model()->findByPk($id);

            /** @var Kas Detail $model */
            $criteria = new CDbCriteria;
            $criteria->compare('kas_id',$model->kas_id);
            $modelDetil = KasDetail::model()->findAll($criteria);
            
            if ($model == null) {
                throw new Exception('Data Kas kosong.');
            }

            
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveKas(json_encode($attrib));
            var_dump($result);
            
            if($modelDetil != null){
                foreach($modelDetil as $val){
                    $attribDetil = $val->getAttributes();
                    $resultDetil = $client->saveKasdetail(json_encode($attribDetil));
                    var_dump($resultDetil); 
                }
            }      
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

     public function actionKasdetail($id){
        try {
            /** @var KasDetail $model */
            $model = KasDetail::model()->findByPk($id);
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveKasdetail(json_encode($attrib));
            var_dump($result);
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

    public function actionBarang($id){
        try {
            /** @var Barang $model */
            $model = Barang::model()->findByPk($id);
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveBarang(json_encode($attrib));
            var_dump($result);
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

    public function actionBarangAll($id){
        try {
            /** @var Barang $model */
            $model = Barang::model()->findByPk($id);
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            
            //$cabang = array('JOG01','JOG02','BDG01');
             if(STOREID != 'SIN01'){
                $cabang = Store::model()->findAll(array(
                    'condition'=>'store_kode NOT IN (:s, "SIN01")',
                    'params'=>array(':s'=>STOREID),
                ));   
            }else{
                $cabang = Store::model()->findAll(array(
                    'condition'=>'store_kode != :s',
                    'params'=>array(':s'=>STOREID),
                ));
            } 
            
            foreach($cabang as $cab){
                $result = $client->saveBarangAll(json_encode($attrib),$cab);
                var_dump($result);
            }  

            
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

    public function actionStockmoves($id){
        try {
            /** @var StockMoves $model */
            $model = StockMoves::model()->findByPk($id);
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveStockMoves(json_encode($attrib));
            var_dump($result);
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

    public function actionDroppinghistory($id)
    {
        $res = "";
        try
        {
            /** @var DroppingHistory $model */
            $model = DroppingHistory::model()->findByPk($id);

            $criteria = new CDbCriteria;
            $criteria->compare('id_history',$model->id_history);

            if ($model == null) {
                throw new Exception('Data Dropping History kosong.');
            }
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            $cabang = array();
            if($model->store_pengirim != STOREID)
                $cabang[] = $model->store_pengirim;
            if($model->store != STOREID)
                $cabang[] = $model->store;
            if(!in_array(STOREID_HO, $cabang) && STOREID != STOREID_HO)
                $cabang[] = STOREID_HO;

            foreach($cabang as $cab){
                $result = $client->saveDroppingHistory(json_encode($attrib),$cab);
                var_dump($result);
                $res = $result;

            }
        }
        catch (SoapFault $e)
        {
            var_dump($e);
        }
    }

    public function actionDroppinghistoryresend($id)
    {
        $res = "";
        try
        {
            /** @var Dropping $model */
            $model = OrderDropping::model()->findByPk($id) ? OrderDropping::model()->findByPk($id) : Dropping::model()->findByPk($id);
            //$model = Dropping::model()->findByPk($id);
            //$modelhistory = DroppingHistory::model()->findByPk($id);

            if ($model == null) {
                throw new Exception('Data kosong.');
            }

            $id = $model->order_dropping_id ? $model->order_dropping_id : $model->dropping_id;

            $criteria = new CDbCriteria;
            $criteria->compare('order_dropping_id',$id);
            $modelhistory = DroppingHistory::model()->findAll($criteria);

            $attrib = $model->getAttributes();

            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            $cabang = array();

            if($model->order_dropping_id)
            {
                if($model->store_pengirim != STOREID)
                    $cabang[] = $model->store_pengirim;
                if($model->store != STOREID)
                    $cabang[] = $model->store;
                if(!in_array(STOREID_HO, $cabang) && STOREID != STOREID_HO)
                    $cabang[] = STOREID_HO;
            }
            else
            {
                if($model->store_penerima != STOREID)
                    $cabang[] = $model->store_penerima;
                if($model->store != STOREID)
                    $cabang[] = $model->store;
                if(!in_array(STOREID_HO, $cabang) && STOREID != STOREID_HO)
                    $cabang[] = STOREID_HO;
            }



            foreach($cabang as $cab){

                if($modelhistory != null){
                    foreach($modelhistory as $val){
                        $attribDetil = $val->getAttributes();
                        $result = $client->saveDroppingHistory(json_encode($attribDetil),$cab);
                        var_dump($res);
                        $res = $result;
                    }
                }
            }
        }
        catch (SoapFault $e)
        {
            var_dump($e);
        }
    }

    public function actionOrderdropping($id){
        try {
            /** @var Order Dropping $model */
            $model = OrderDropping::model()->findByPk($id);
            /** @var Order Dropping Detail $model */
            $criteria = new CDbCriteria;
            $criteria->compare('order_dropping_id',$model->order_dropping_id);
            $modelDetil = OrderDroppingDetails::model()->findAll($criteria);
            
            if ($model == null) {
                throw new Exception('Data Order Dropping kosong.');
            }
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveOrderDropping(json_encode($attrib));

            var_dump($result);
            if($modelDetil != null){
                foreach($modelDetil as $val){
                    $attribDetil = $val->getAttributes();
                    $resultDetil = $client->saveOrderDroppingDetails(json_encode($attribDetil));
                    var_dump($resultDetil); 
                }
            }    
        } catch (SoapFault $e) {
            var_dump($e->getMessage());
            //alert($e->getMessage());
            //alert("'Response :<br>', htmlentities($client->__getLastResponse()), '<br>'");
            //echo "Response :<br>", htmlentities($client->__getLastResponse()), "<br>";
        }
    }

    public function actionResendOrderdroppingAll($id)
    {
        $res = "";
        try
        {
            /** @var Order Dropping $model */
            $model = OrderDropping::model()->findByPk($id);

            /** @var Order Dropping Detail $model */
            $criteria = new CDbCriteria;
            $criteria->compare('order_dropping_id',$model->order_dropping_id);
            $modelDetil = OrderDroppingDetails::model()->findAll($criteria);

            if ($model == null) {
                throw new Exception('Data Order Dropping kosong.');
            }
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            $cabang = array();
            if($model->store_pengirim != STOREID)
                $cabang[] = $model->store_pengirim;
            if($model->store != STOREID)
                $cabang[] = $model->store;
            if(!in_array(STOREID_HO, $cabang) && STOREID != STOREID_HO)
                $cabang[] = STOREID_HO;


            foreach($cabang as $cab){
                $result = $client->CheckResendOrderdroppingAll(json_encode($attrib),$cab);
                var_dump($result);
                $res = $result;

                if($modelDetil != null){
                    foreach($modelDetil as $val){
                        $attribDetil = $val->getAttributes();
                        $resultDetil = $client->CheckResendOrderdroppingDetailsAll(json_encode($attribDetil),$cab);
                        var_dump($resultDetil);
                    }
                }
            }
        }
        catch (SoapFault $e)
        {
            var_dump($e);
            if($model->up == DR_SEND)
            {
                $model->up = DR_PENDING;
            }
            if($model->approved == 1)
            {
                if($model->up == DR_APPROVE)
                {
                    $model->up = DR_SEND;
                }
            }
            $model->save();
        }
        finally
        {
            if($res != "OK")
            {
                if($model->up == DR_SEND)
                {
                    $model->up = DR_PENDING;
                }
                if($model->approved == 1)
                {
                    if($model->up == DR_APPROVE)
                    {
                        $model->up = DR_SEND;
                    }
                }
                $model->save();
            }
        }
    }

    public function actionOrderdroppingAll($id)
    {
        $res = "";
        try {

            /** @var Order Dropping $model */

            $model = OrderDropping::model()->findByPk($id);

            $criteria = new CDbCriteria;
            $criteria->compare('order_dropping_id',$model->order_dropping_id);

            /** @var Order Dropping Detail $modelDetil */
            $modelDetil = OrderDroppingDetails::model()->findAll($criteria);
            
            if ($model == null) {
                throw new Exception('Data Order Dropping kosong.');
            }
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            $cabang = array();
            if($model->store_pengirim != STOREID)
                $cabang[] = $model->store_pengirim;
            if($model->store != STOREID)
                $cabang[] = $model->store;
            if(!in_array(STOREID_HO, $cabang) && STOREID != STOREID_HO)
                $cabang[] = STOREID_HO;


            foreach($cabang as $cab){
                $result = $client->saveOrderDroppingAll(json_encode($attrib),$cab);

                var_dump($result);
                $res = $result;

                if($modelDetil != null){
                    foreach($modelDetil as $val){
                        $attribDetil = $val->getAttributes();
                        $resultDetil = $client->saveOrderDroppingDetailsAll(json_encode($attribDetil),$cab);
                        var_dump($resultDetil);
                    }
                }
            }


        }
        catch (SoapFault $e)
        {
            var_dump($e);
            if($model->up == DR_SEND)
            {
                $model->up = DR_PENDING;
            }
            if($model->approved == 1)
            {
                if($model->up == DR_APPROVE)
                {
                    $model->up = DR_SEND;
                }
            }
            $model->save();
        }
        finally
        {
            if($res != "OK")
            {
                if($model->up == DR_SEND)
                {
                    $model->up = DR_PENDING;
                }
                if($model->approved == 1)
                {
                    if($model->up == DR_APPROVE)
                    {
                        $model->up = DR_SEND;
                    }
                }
                $model->save();
            }
        }
    }

    public function actionDropping($id){
        try {
            /** @var Dropping $model */
            $model = Dropping::model()->findByPk($id);
            /** @var Dropping Detail $model */
            $criteria = new CDbCriteria;
            $criteria->compare('dropping_id',$model->dropping_id);
            $modelDetil = DroppingDetails::model()->findAll($criteria);
            
            if ($model == null) {
                throw new Exception('Data Dropping kosong.');
            }
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveDropping(json_encode($attrib));
            var_dump($result);
            if($modelDetil != null){
                foreach($modelDetil as $val){
                    $attribDetil = $val->getAttributes();
                    $resultDetil = $client->saveDroppingDetails(json_encode($attribDetil));
                    var_dump($resultDetil); 
                }
            }    
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

    public function actionDroppingAll($id)
    {
        $res = "";
        try {
            /** @var Dropping $model */
            $model = Dropping::model()->findByPk($id);
            /** @var Dropping Detail $model */
            $criteria = new CDbCriteria;
            $criteria->compare('dropping_id',$model->dropping_id);
            $modelDetil = DroppingDetails::model()->findAll($criteria);
            
            if ($model == null) {
                throw new Exception('Data Dropping kosong.');
            }
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            $cabang = array();
            if($model->store_penerima != STOREID)
                $cabang[] = $model->store_penerima;
            if($model->store != STOREID)
                $cabang[] = $model->store;
            if(!in_array(STOREID_HO, $cabang) && STOREID != STOREID_HO)
                $cabang[] = STOREID_HO;

            foreach($cabang as $cab){
                $result = $client->saveDroppingAll(json_encode($attrib),$cab);
                var_dump($result);
                $res = $result;

                if($modelDetil != null){
                    foreach($modelDetil as $val){
                        $attribDetil = $val->getAttributes();
                        $resultDetil = $client->saveDroppingDetailsAll(json_encode($attribDetil),$cab);
                        var_dump($resultDetil); 
                    }
                }
            } 
        } catch (SoapFault $e) {
            var_dump($e);

            if($model->order_dropping_id)
            {
                if($model->approved == 1)
                {
                    if($model->up == DR_SEND)
                    {
                        $model->up = DR_PENDING;
                    }
                }
            }
            else
            {
                if($model->up == DR_SEND)
                {
                    $model->up = DR_PENDING;
                }
                if($model->up == DR_APPROVE)
                {
                    $model->up = DR_SEND;
                }
            }


            $model->save();
        }
        finally
        {
            if($res != "OK")
            {
                if($model->order_dropping_id)
                {
                    if($model->approved == 1)
                    {
                        if($model->up == DR_SEND)
                        {
                            $model->up = DR_PENDING;
                        }
                    }
                }
                else
                {
                    if($model->up == DR_SEND)
                    {
                        $model->up = DR_PENDING;
                    }
                    if($model->up == DR_APPROVE)
                    {
                        $model->up = DR_SEND;
                    }
                }

                $model->save();
            }
        }
    }

    public function actionReceivedropping($id)
    {
        $res = "";
        try {
            /** @var Receive Dropping $model */
            $model = ReceiveDropping::model()->findByPk($id);
            /** @var Receive Dropping Detail $model */
            $criteria = new CDbCriteria;
            $criteria->compare('receive_dropping_id',$model->receive_dropping_id);
            $modelDetil = ReceiveDroppingDetails::model()->findAll($criteria);
            
            if ($model == null) {
                throw new Exception('Data Receive Dropping kosong.');
            }
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveReceiveDropping(json_encode($attrib));
            var_dump($result);
            $res = $result;

            if($modelDetil != null){
                foreach($modelDetil as $val){
                    $attribDetil = $val->getAttributes();
                    $resultDetil = $client->saveReceiveDroppingDetails(json_encode($attribDetil));
                    var_dump($resultDetil); 
                }
            }    
        }
        catch (SoapFault $e)
        {
            var_dump($e);
            if($model->lunas != 2)
            {
                if($model->up == DR_SEND)
                {
                    $model->up = DR_PENDING;
                    $model->lunas = 0;
                }
            }
            else
            {
                if($model->up == DR_CLOSE)
                {
                    $model->up = DR_PENDING;
                }

            }

            $model->save();
        }
        finally
        {
            if($res != "OK")
            {
                if($model->lunas != 2)
                {
                    if($model->up == DR_SEND)
                    {
                        $model->up = DR_PENDING;
                        $model->lunas = 0;
                    }
                }
                else
                {
                    if($model->up == DR_CLOSE)
                    {
                        $model->up = DR_PENDING;
                    }

                }

                $model->save();
            }
        }
    }
    
    public function actionReceivedroppingAll($id)
    {
        $res = "";
        try {
            /** @var Order Dropping $model */
            $model = ReceiveDropping::model()->findByPk($id);
            /** @var Order Dropping Detail $model */
            $criteria = new CDbCriteria;
            $criteria->compare('receive_dropping_id',$model->receive_dropping_id);
            $modelDetil = ReceiveDroppingDetails::model()->findAll($criteria);
            
            if ($model == null) {
                throw new Exception('Data Receive Dropping kosong.');
            }
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            $cabang = array();
            if($model->store_pengirim != STOREID)
                $cabang[] = $model->store_pengirim;
            if($model->store != STOREID)
                $cabang[] = $model->store;
            if(!in_array(STOREID_HO, $cabang) && STOREID != STOREID_HO)
                $cabang[] = STOREID_HO;

            foreach($cabang as $cab){
                $result = $client->saveReceivedroppingAll(json_encode($attrib),$cab);
                var_dump($result);
                $res = $result;

                if($modelDetil != null){
                    foreach($modelDetil as $val){
                        $attribDetil = $val->getAttributes();
                        $resultDetil = $client->saveReceivedroppingDetailsAll(json_encode($attribDetil),$cab);
                        var_dump($resultDetil); 
                    }
                }
            }  
        }
        catch (SoapFault $e)
        {
            var_dump($e);
            if($model->lunas != 2)
            {
                if($model->up == DR_SEND)
                {
                    $model->up = DR_PENDING;
                    $model->lunas = 0;
                }
            }
            else
            {
                if($model->up == DR_CLOSE)
                {
                    $model->up = DR_PENDING;
                }

            }

            $model->save();
        }
        finally
        {
            if($res != "OK")
            {
                if($model->lunas != 2)
                {
                    if($model->up == DR_SEND)
                    {
                        $model->up = DR_PENDING;
                        $model->lunas = 0;
                    }
                }
                else
                {
                    if($model->up == DR_CLOSE)
                    {
                        $model->up = DR_PENDING;
                    }

                }
                $model->save();
            }
        }
    }

    public function actionDroppingRecallAll($id)
    {
        $res = "";
        try {
            /** @var Dropping $model */
            $model = DroppingRecall::model()->findByPk($id);
            /** @var Dropping Detail $model */
            $criteria = new CDbCriteria;
            $criteria->compare('dropping_recall_id',$model->dropping_recall_id);
            $modelDetil = DroppingRecallDetails::model()->findAll($criteria);

            if ($model == null) {
                throw new Exception('Data Dropping Recall kosong.');
            }
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

            foreach ($model->droppingRecallDetails as $d){
                $cabang = array();
                if($d->dropping->store_penerima != STOREID)
                    $cabang[] = $d->dropping->store_penerima;
                if($d->dropping->store != STOREID)
                    $cabang[] = $d->dropping->store;
                if(!in_array(STOREID_HO, $cabang) && STOREID != STOREID_HO)
                    $cabang[] = STOREID_HO;

                foreach($cabang as $cab){
                    $result = $client->saveDroppingRecallAll(json_encode($attrib),$cab);
                    var_dump($result);
                    $res = $result;

                    if($modelDetil != null){
                        foreach($modelDetil as $val){
                            $attribDetil = $val->getAttributes();
                            $resultDetil = $client->saveDroppingRecallDetailsAll(json_encode($attribDetil),$cab);
                            var_dump($resultDetil);
                        }
                    }
                }
            }

        } catch (SoapFault $e) {
            var_dump($e);

                if($model->up == DR_SEND)
                {
                    $model->up = DR_PENDING;
                }

            $model->save();
        }
        finally
        {
            if($res != "OK")
            {

                    if($model->up == DR_SEND)
                    {
                        $model->up = DR_PENDING;
                    }

                $model->save();
            }
        }
    }
    
    public function actionSalestrans($id){
        try {
            /** @var SalesTrans $model */
            $model = Salestrans::model()->findByPk($id);
            /** @var Dropping Detail $model */
            $criteria = new CDbCriteria;
            $criteria->compare('salestrans_id',$model->salestrans_id);
            $modelDetil = SalestransDetails::model()->findAll($criteria); 
            
            if ($model == null) {
                throw new Exception('Data Penjualan kosong.');
            }
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveSalestrans(json_encode($attrib));
            var_dump($result);
            if($modelDetil != null){
                foreach($modelDetil as $val){
                    $attribDetil = $val->getAttributes();
                    $resultDetil = $client->saveSalestransDetails(json_encode($attribDetil));
                    var_dump($resultDetil); 
                }
            }     
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

    public function actionCustomer($id){
        try {
            /** @var Customers $model */
            $model = Customers::model()->findByPk($id);
            
            if ($model == null) {
                throw new Exception('Data Customer kosong.');
            }
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveCustomers(json_encode($attrib));
            var_dump($result);  
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

    public function actionCustomerAll($id){
        try {
            /** @var Customers $model */
            $model = Customers::model()->findByPk($id);
            
            if ($model == null) {
                throw new Exception('Data Customer kosong.');
            }
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));

	        if(HISTORY_NSC) {
		        $cabang = Store::model()->findAll(array(
			        //'condition'=>'store_kode != :s',
			        'condition'=>'store_kode NOT IN ( :s, "NSC01")',
			        'params'=>array(':s'=>STOREID),
		        ));
	        }
	        else {
		        $cabang = Store::model()->findAll(array(
			        //'condition'=>'store_kode != :s',
			        'condition' => 'store_kode NOT IN ( :s, "SIN01")',
			        'params' => array(':s' => STOREID),
		        ));
	        }
            foreach($cabang as $cab){
                $result = $client->saveCustomersAll(json_encode($attrib),$cab);
                var_dump($result);
            }    
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

    public function actionStockmovesperlengkapan($id){
        try {
            /** @var StockMoves $model */
            $model = StockMovesPerlengkapan::model()->findByPk($id);
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveStockMovesPerlengkapan(json_encode($attrib));
            var_dump($result);
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

    public function actionStockmovesperlengkapanAll($id){
        try {
            /** @var StockMoves $model */
            $model = StockMovesPerlengkapan::model()->findByPk($id);
            $attrib = $model->getAttributes();
            $client = new SoapClient(SOAP_PUSH, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            if(STOREID != 'SIN01'){
                $cabang = Store::model()->findAll(array(
                    'condition'=>'store_kode NOT IN (:s, "SIN01")',
                    'params'=>array(':s'=>STOREID),
                ));   
            }else{
                $cabang = Store::model()->findAll(array(
                    'condition'=>'store_kode != :s',
                    'params'=>array(':s'=>STOREID),
                ));
            } 
            foreach($cabang as $cab){
                 $result = $client->saveStockMovesPerlengkapan(json_encode($attrib));
                var_dump($result);
            }  
        } catch (SoapFault $e) {
            var_dump($e);
        }
    }

}