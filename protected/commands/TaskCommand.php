<?php
/*
 * Untuk eksekusi tugas/task yang memerlukan PHP
 * karena tidak bisa dengan Query SQL langsung dsb.
 */
class TaskCommand extends CConsoleCommand
{
    public function actionUpdateHpp($id){
        /*
         * Update/Insert HPP
         * table 'nscc_jual'
         * field 'cost'
         */
        $res = "Update Hpp tidak dapat dilakukan.";
        if($id==date("Ymd")){
            $data = array(
                array('barang_id' => '009a4308-d1ff-11e5-850e-00ff901fd500', 'kode_barang' => 'NG-PNAA', 'cost' => 50000),
                array('barang_id' => '0753c024-70dc-11e7-9e0b-f44d3016365c', 'kode_barang' => 'NG-BAI', 'cost' => 100),
                array('barang_id' => '1f2a6652-c366-11e6-919d-201a069ec688', 'kode_barang' => 'NG-RENEWALSER', 'cost' => 9500),
                array('barang_id' => '359c7320-d1fd-11e5-850e-00ff901fd500', 'kode_barang' => 'NG-BRT2', 'cost' => 16200),
                array('barang_id' => '3d9ded0f-70dc-11e7-9e0b-f44d3016365c', 'kode_barang' => 'NG-TRICOC', 'cost' => 35000),
                array('barang_id' => '44cef497-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-AA', 'cost' => 13000),
                array('barang_id' => '44d0ff20-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-BDS', 'cost' => 14500),
                array('barang_id' => '44d10083-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-BM', 'cost' => 40000),
                array('barang_id' => '44d101d5-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-BP', 'cost' => 27000),
                array('barang_id' => '44d102fc-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-BRT1', 'cost' => 15000),
                array('barang_id' => '44d1041b-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-EDC', 'cost' => 15600),
                array('barang_id' => '44d10528-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-FW', 'cost' => 10000),
                array('barang_id' => '44d75602-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-LDC', 'cost' => 8500),
                array('barang_id' => '44d7634a-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-LM', 'cost' => 12500),
                array('barang_id' => '44d764ac-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-LP', 'cost' => 7500),
                array('barang_id' => '44d765a9-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-MC', 'cost' => 11500),
                array('barang_id' => '44d76698-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-MSC', 'cost' => 10000),
                array('barang_id' => '44d76788-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-MST', 'cost' => 11000),
                array('barang_id' => '44d76873-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-SC', 'cost' => 8500),
                array('barang_id' => '44d7695e-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-STC', 'cost' => 9500),
                array('barang_id' => '44d76a67-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-AC1', 'cost' => 17000),
                array('barang_id' => '44d76b6c-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-AC3', 'cost' => 17000),
                array('barang_id' => '44d76c64-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-AD2', 'cost' => 15000),
                array('barang_id' => '44d76d94-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-AI', 'cost' => 9000),
                array('barang_id' => '44d76e8c-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-RA0', 'cost' => 8500),
                array('barang_id' => '44d76f7f-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-RA2', 'cost' => 11500),
                array('barang_id' => '44d77178-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-W0', 'cost' => 16000),
                array('barang_id' => '44d7725f-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-W1', 'cost' => 17000),
                array('barang_id' => '44d7734a-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-W2', 'cost' => 25000),
                array('barang_id' => '44d77431-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-W3', 'cost' => 26500),
                array('barang_id' => '44d7751c-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-W4', 'cost' => 13000),
                array('barang_id' => '44d778da-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-GT', 'cost' => 15000),
                array('barang_id' => '44d77dd0-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-PNGA1', 'cost' => 142500),
                array('barang_id' => '44d77ed1-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NG-PNGA2', 'cost' => 134000),
                array('barang_id' => '44d780ab-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NGNF-AC', 'cost' => 90000),
                array('barang_id' => '44d78192-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NGNF-B', 'cost' => 178000),
                array('barang_id' => '44d78279-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NGNF-SS', 'cost' => 127000),
                array('barang_id' => '44d78369-1a3c-11e5-b6f7-00ff24e56c2c', 'kode_barang' => 'NGNF-AA', 'cost' => 120000),
                array('barang_id' => '57338d3c-177a-11e6-b062-50af736e62f0', 'kode_barang' => 'NG-CC', 'cost' => 15000),
                array('barang_id' => '5e44170a-6c30-11e7-9a1c-f44d3019e252', 'kode_barang' => 'NG-CLIN', 'cost' => 20500),
                array('barang_id' => '6a19d9b6-6067-11e6-9a2f-50af736e62f0', 'kode_barang' => 'DH', 'cost' => 26365),
                array('barang_id' => '6d209a1a-2bb5-11e6-96cb-50af736e62f0', 'kode_barang' => 'NG-AD1', 'cost' => 12000),
                array('barang_id' => '775d1c9e-d1fd-11e5-850e-00ff901fd500', 'kode_barang' => 'NG-CALM', 'cost' => 14000),
                array('barang_id' => '79ee8691-3365-11e6-96cb-50af736e62f0', 'kode_barang' => 'LKFHA', 'cost' => 6000),
                array('barang_id' => '7a9cfe55-6c30-11e7-9a1c-f44d3019e252', 'kode_barang' => 'NG-ALEN', 'cost' => 24000),
                array('barang_id' => '8000f5fc-d1ff-11e5-850e-00ff901fd500', 'kode_barang' => 'NG-PNAC2', 'cost' => 33500),
                array('barang_id' => '823665dd-d1fd-11e5-850e-00ff901fd500', 'kode_barang' => 'NG-SEBUM', 'cost' => 10500),
                array('barang_id' => '830e0405-c366-11e6-919d-201a069ec688', 'kode_barang' => 'NG-BRTNIGHT1', 'cost' => 14500),
                array('barang_id' => '8a405af0-c366-11e6-919d-201a069ec688', 'kode_barang' => 'NG-BRTNIGHT2', 'cost' => 19000),
                array('barang_id' => '8b3ed410-4d7f-11e7-84ca-54bef7092b01', 'kode_barang' => 'MRETC', 'cost' => 21500),
                array('barang_id' => '91f4bc11-c366-11e6-919d-201a069ec688', 'kode_barang' => 'NG-BRTNIGHT3', 'cost' => 24000),
                array('barang_id' => '9263b848-6c30-11e7-9a1c-f44d3019e252', 'kode_barang' => 'NG-TRICO', 'cost' => 35000),
                array('barang_id' => '968b76ff-d1fd-11e5-850e-00ff901fd500', 'kode_barang' => 'NG-SBRIGHT', 'cost' => 12500),
                array('barang_id' => '995b9984-a0d7-11e6-b889-54bef7093cb4', 'kode_barang' => 'NG-VCSER', 'cost' => 16500),
                array('barang_id' => 'a2b84bef-ab47-11e6-bd75-201a069ec688', 'kode_barang' => 'NG-BRT3', 'cost' => 11500),
                array('barang_id' => 'a367dca9-d1fd-11e5-850e-00ff901fd500', 'kode_barang' => 'NG-SMOOTDAY', 'cost' => 38000),
                array('barang_id' => 'b091ce7e-d1fd-11e5-850e-00ff901fd500', 'kode_barang' => 'NG-HERBACNE', 'cost' => 9500),
                array('barang_id' => 'da11794d-b7ce-11e6-b3d5-201a069ec688', 'kode_barang' => 'NG-BODYSCRUB', 'cost' => 12500),
                array('barang_id' => 'e4d15030-b362-11e5-96b3-00ff6236775f', 'kode_barang' => 'NG-RA1', 'cost' => 9000)
            );

            $res = "";
            $i = 1;
            foreach ($data as $d) {
                $jual = Jual::model()->find('barang_id = :barang_id AND store = :store', array(
                    ':barang_id' => $d['barang_id'],
                    ':store' => STOREID
                ));
                if ($jual) {
                    $jual->cost = $d['cost'];
                    $jual->save();

                    $res .= "UPDATE : " . $jual->barang_id . "<br>";
                    $i++;
                } else {
                    $barang = Barang::model()->findByPk($d['barang_id']);
                    if ($barang) {
                        $jual = new Jual();
                        $jual->barang_id = $d['barang_id'];
                        $jual->cost = $d['cost'];
                        $jual->store = STOREID;
                        $jual->save();

                        $res .= "INSERT : " . $jual->barang_id . "<br>";
                        $i++;
                    }
                }
            }

            $res .= "<br> Total : $i";
            $res = "Total update/insert Hpp : $i";
        }
        echo $res;
    }
}