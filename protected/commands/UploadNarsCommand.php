<?php
//ini_set('soap.wsdl_cache_enabled', 0);
//ini_set('soap.wsdl_cache_ttl', 0);
Yii::import('application.components.SoapClientYii');
class UploadNarsCommand extends CConsoleCommand
{
    public function actionPasien($id)
    {
        self::pasienSoap($id);
    }
    private function pasienSoap($id)
    {
        try {
            /** @var Customers $cust */
            $cust = Customers::model()->findByPk($id);
            if ($cust == null) {
                throw new Exception('Data Customers kosong.');
            }
            $attrib = $cust->getAttributes();
            $attrib['thumb'] = null;
            $attrib['photo'] = null;
            $attrib['kota'] = '';
//            $nama_kota = '';
            if ($cust->kota_id != null) {
                /** @var Kota $kota */
                $kota = Kota::model()->findByPk($cust->kota_id);
                if ($kota != null) {
                    $attrib['kota'] = $kota->nama_kota;
                }
            }
            $client = new SoapClient(SOAP_CUSTOMER, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->savePasien($attrib, STOREID);
            var_dump($result);
        } catch (Exception $e) {
            var_dump($e);
        }
    }
    private function historySoap($id)
    {
        try {
            /** @var NarstransView $model */
            $model = NarstransView::model()->findByAttributes(['salestrans_id' => $id]);
            if ($model == null) {
                throw new Exception('Salestrans tidak ditemukan');
            }
            $data = $model->getAttributes();


	        if(HISTORY_NSC)
	        {
		        $sales_details = Yii::app()->db->createCommand("SELECT
                nsd.salestrans_details,
                nsd.barang_id,
                nsd.salestrans_id,
                nsd.qty,
                nsd.disc,
                nsd.discrp,
                nsd.ketpot,
                nsd.vat,
                nsd.vatrp,
                nsd.bruto,
                nsd.total,
                nsd.total_pot,
                nsd.price,
                nsd.jasa_dokter,
                nsd.dokter_id,
                nsd.disc_name,
                nsd.hpp,
                nsd.cost,
                nsd.disc1,
                nsd.discrp1,
                nsd.paket_trans_id,
                nsd.paket_details_id,
                nb.kode_barang,
                nb.sat,
                ng.nama_grup as 'grup_barang'
                FROM
                nscc_salestrans_details AS nsd
                INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
                INNER JOIN nscc_grup AS ng ON ng.grup_id = nb.grup_id
                WHERE salestrans_id = :salestrans_id")
			        ->queryAll(true, array(":salestrans_id" => $model->salestrans_id));
	        }
	        else {
		        $sales_details = Yii::app()->db->createCommand("SELECT
	            nsd.salestrans_details,
	            nsd.barang_id,
	            nsd.salestrans_id,
	            nsd.qty,
	            nsd.disc,
	            nsd.discrp,
	            nsd.ketpot,
	            nsd.vat,
	            nsd.vatrp,
	            nsd.bruto,
	            nsd.total,
	            nsd.total_pot,
	            nsd.price,
	            nsd.jasa_dokter,
	            nsd.dokter_id,
	            nsd.disc_name,
	            nsd.hpp,
	            nsd.cost,
	            nsd.disc1,
	            nsd.discrp1,
	            nsd.paket_trans_id,
	            nsd.paket_details_id,
	            nb.kode_barang,
	            nb.sat
	            FROM
	            nscc_salestrans_details AS nsd
	            INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
	            WHERE salestrans_id = :salestrans_id")
				        ->queryAll(true, array(":salestrans_id" => $model->salestrans_id));
	        }
            $client = new SoapClient(SOAP_CUSTOMER, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
//            var_dump($sales_details);
            $result = $client->saveHistoryPasien($data, $sales_details, STOREID);
            var_dump($result);
        } catch (Exception $e) {
            var_dump($e);
        }
    }
    public function actionHistory($id)
    {
        self::historySoap($id);
    }
    public function actionDiagnosaPasien($id)
    {
        try {
            /** @var DiagnosaView $diag */
            $diag = DiagnosaView::model()->findByAttributes(['diagnosa_id' => $id]);
            if ($diag == null) {
                throw new Exception('Data Diagnosa kosong.');
            }
            $attrib = $diag->getAttributes();
            $client = new SoapClient(SOAP_CUSTOMER, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveDiagnosaPasien($attrib, STOREID);
            var_dump($result);
        } catch (Exception $e) {
            var_dump($e);
        }
    }
    public function actionUploadHistory($from, $to)
    {
        /** @var Salestrans[] $sales */
        $sales = Salestrans::model()->findAll('tgl >= :from AND tgl <= :to',
            [
                ':from' => $from,
                ':to' => $to
            ]);
        $counter = 0;
        $total = count($sales);
        foreach ($sales as $r) {
            $counter++;
            echo ">>> $counter/$total Proses sales docref : $r->doc_ref tgl : $r->tgl \n";
            self::pasienSoap($r->customer_id);
            self::historySoap($r->salestrans_id);
        }
    }
}