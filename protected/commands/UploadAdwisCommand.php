<?php
//ini_set('soap.wsdl_cache_enabled', 0);
//ini_set('soap.wsdl_cache_ttl', 0);
Yii::import('application.components.SoapClientYii');
class UploadAdwisCommand extends CConsoleCommand
{
    public function actionDiagnosaPasien($id)
    {
        try {
            /** @var DiagnosaView $diag */
            $diag = DiagnosaView::model()->findByAttributes(['diagnosa_id' => $id]);
            if ($diag == null) {
                throw new Exception('Data Diagnosa kosong.');
            }
            $attrib = $diag->getAttributes();
            $client = new SoapClient(SOAP_CUSTOMER, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_BOTH, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->saveDiagnosaPasien($attrib, STOREID);
            var_dump($result);
        } catch (Exception $e) {
            var_dump($e);
        }
    }
    
}