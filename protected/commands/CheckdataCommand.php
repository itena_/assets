<?php
/*
** controller for get data before send data to soap
*/
//Yii::import('application.components.ConsoleProgress');
class CheckdataCommand extends CConsoleCommand{

    public function actionBanktrans($tno){
        try {   
            $criteria = new CDbCriteria;
            $criteria->compare('trans_no',$tno);
            $model = BankTrans::model()->findAll($criteria);
            if($model != null){
                foreach($model as $val){
                    U::runCommand('soap', 'banktrans', '--id=' . $val->bank_trans_id,  'banktrans_'.$val->bank_trans_id.'.log');
                } 
            }else{
                throw new Exception('Bank Trans tidak ditemukan');
            }
        }catch (Exception $e) {
            var_dump($e);
        }
    }

    public function actionGltrans($tno){
        try {
            $criteria = new CDbCriteria;
            $criteria->compare('type_no',$tno);
            $model = GlTrans::model()->findAll($criteria);
            if($model != null){
                foreach($model as $val){
                    U::runCommand('soap', 'gltrans', '--id=' . $val->counter, 'gl_'.$val->counter.'.log');
                }
            }else{
                throw new Exception('GL Trans tidak ditemukan');
            }
        }catch (Exception $e) {
            var_dump($e);
        }

    } 
    
    public function actionRef($tno){
        try {
            $criteria = new CDbCriteria;
            $criteria->compare('type_no',$tno);
            $model = Refs::model()->find($criteria);
            if($model != null){
                U::runCommand('soap', 'refs', '--id=' . $model->refs_id, 'refs_'.$model->refs_id.'.log');
            }else{
                throw new Exception('Refs tidak ditemukan');
            }
        }catch (Exception $e) {
            var_dump($e);
        }

    }
    
    /*
     * Bonus Aishaderm
     */    
    public function actionBonusInAssign($store,$from,$to,$tipe_employee_id){
        $total = 0;        
        try{
            $where = "";
            if(isset($tipe_employee_id)){
                $where = " AND tipe_employee_id = '$tipe_employee_id'";
            }            
            $cmd = Yii::app()->db->createCommand(
            "   SELECT 
                b.bonus_id
                , bj.persen_bonus
                , b.amount
                , t2.jum count
                , TRUNCATE((b.amount * (bj.persen_bonus /100) / t2.jum),2) nilai
                FROM nscc_bonus b
                JOIN (	
                    SELECT t1.employee_id,ee.store,t1.jum, t1.tanggal
                    from nscc_employees ee
                    JOIN (
                        SELECT assign_employee_id,a.employee_id, a.tipe_employee_id, t.jum, t.tgl tanggal
                        FROM nscc_assign_employees a
                        LEFT JOIN (
                                SELECT employee_id,count(tipe_employee_id) jum,tipe_employee_id, tgl
                                from nscc_assign_employees
                                where tgl >= '$from' AND tgl <= '$to' $where
                                GROUP BY tgl,tipe_employee_id
                        ) as t on t.tipe_employee_id = a.tipe_employee_id
                        where a.tgl >= '$from' AND a.tgl <= '$to'
                    ) as t1 on t1.employee_id = ee.employee_id
                    WHERE ee.store = '$store'
                ) as t2 on t2.employee_id = b.employee_id
                LEFT JOIN nscc_bonus_jual bj on bj.bonus_jual_id = b.bonus_jual_id
                where b.employee_id = t2.employee_id AND b.tgl = t2.tanggal
                GROUP BY bonus_id"                    
            );
            $model = $cmd->queryAll();
            
            if($model != null){
                foreach($model as $k){
                    Bonus::model()->updateByPk($k['bonus_id'],['amount_bonus' => $k['nilai']]);
                    $total++;
                }
                echo $total . PHP_EOL;
                //file_put_contents('bonus.loc', $dump);
            }else{
                throw new Exception('Bonus tidak ditemukan');
            }
            echo CJSON::encode(array(
                'success' => $total,
                'bon' => 0
            ));
        } catch (Exception $ex) {
            var_dump($ex);
        }        
    }
    public function actionBonusNotInAssign($store,$from,$to){
        $total = 0;        
        try{
            $cmd = Yii::app()->db->createCommand(
            "SELECT 
                b.bonus_id
                , t1.persen_bonus
                , b.amount
                , TRUNCATE((t1.persen_bonus /100),4) decimal_persen
                , TRUNCATE((b.amount * (t1.persen_bonus /100)),2) nilai
            FROM nscc_bonus b
            JOIN
                (
                    select bonus_jual_id, persen_bonus, store 
                    from nscc_bonus_jual
                    where bonus_name_id in ('1','2','3','8','9') AND store = '$store'
                ) t1 on t1.bonus_jual_id in (b.bonus_jual_id)
            where tgl >= '$from' AND tgl <= '$to' AND b.store = '$store'
            GROUP BY bonus_id"                    
            );
            $model = $cmd->queryAll();
            
            if($model != null){
                foreach($model as $k){
                    Bonus::model()->updateByPk($k['bonus_id'],['amount_bonus' => $k['nilai']]);
                    $total++;
                }
                echo $total . PHP_EOL;
                //file_put_contents('bonus.loc', $dump);
            }else{
                throw new Exception('Bonus tidak ditemukan');
            }
            echo CJSON::encode(array(
                'success' => $total,
                'bon' => 0
            ));
        } catch (Exception $ex) {
            var_dump($ex);
        }        
    }
    
    public function actionGenerateCreateBonus($tgl){
        try {   
           $sales = Salestrans::model()->findAllByAttributes(array('tgl' => $tgl));
           $a = '';
           foreach ($sales as $sls){
              //$salesdetail = SalestransDetails::model()->findAllByAttributes(array('salestrans_id' => $sls->salestrans_id));
              $salesdetail = $sls->salestransDetails;
              foreach ($salesdetail as $slsdtl){
//                var_dump($slsdtl->barang_id);
                    Bonus::sales_bonus_create(PENJUALAN, $sls, $slsdtl, $tgl);
              }              
           }
           
        }catch (Exception $e) {
            var_dump($e);
        }
        
    }
    public function actionGenerateCreateBonusBeautyDokter($tgl){
        try {   
           $sales = Salestrans::model()->findAllByAttributes(array('tgl' => $tgl));
           $a = '';
           foreach ($sales as $sls){
              //$salesdetail = SalestransDetails::model()->findAllByAttributes(array('salestrans_id' => $sls->salestrans_id));
              $salesdetail = $sls->salestransDetails;
              foreach ($salesdetail as $slsdtl){
//                    Bonus::sales_bonus_create(PENJUALAN, $sls, $slsdtl, $tgl);
                  Bonus::deleteDataBonus($slsdtl->salestrans_details, 'service');
                  $beauty = Yii::app()->db->createCommand("SELECT beauty_id from nscc_beauty_services where salestrans_details = '$slsdtl->salestrans_details';")->queryScalar();
                  if ($beauty){
//                      var_dump($sls->dokter_id);
                      Bonus::serviceBeautyCreate(PENJUALAN, $sls, $slsdtl, $beauty);
                  }
                  if($sls->dokter_id){
                      Bonus::serviceDokterCreate(PENJUALAN, $sls, $slsdtl, $sls->dokter_id);
                  }                  
              }              
           }
           
        }catch (Exception $e) {
            var_dump($e);
        }
        
    }

    public function actionStockmoves($tno){
        try {
            $criteria = new CDbCriteria;
            $criteria->compare('trans_no',$tno);
            $model = StockMoves::model()->findAll($criteria);
            if($model != null){
                foreach($model as $val){
                    U::runCommand('soap', 'stockmoves', '--id=' . $val->stock_moves_id, 'stockmoves_'.$val->stock_moves_id.'.log');
                }
            }else{
                throw new Exception('stock moves tidak ditemukan');
            }
        }catch (Exception $e) {
            var_dump($e);
        }

    } 


    public function actionStockmovesperlengkapan($tno){
        try {
            $criteria = new CDbCriteria;
            $criteria->compare('trans_no',$tno);
            $model = StockMoves::model()->findAll($criteria);
            if($model != null){
                foreach($model as $val){
                    U::runCommand('soap', 'stockmovesperlengkapan', '--id=' . $val->stock_moves_id, 'stockmoves_'.$val->stock_moves_id.'.log');
                }
            }else{
                throw new Exception('stock moves tidak ditemukan');
            }
        }catch (Exception $e) {
            var_dump($e);
        }

    } 

}
?>