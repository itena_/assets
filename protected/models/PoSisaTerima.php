<?php
Yii::import('application.models._base.BasePoSisaTerima');

class PoSisaTerima extends BasePoSisaTerima
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->po_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->po_id = $uuid;
        }
        return parent::beforeValidate();
    }
}