<?php

Yii::import('application.models._base.BasePo');

class Po extends BasePo {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeValidate() {
        if ($this->po_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->po_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    
    static function get_po_details_to_print($po_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                0 num,
                concat_ws(' ', pbv.nama_barang, ppd.description) AS nama_barang,
                ppd.charge,
                ppd.qty,
                pbv.sat,
                ppd.price,
                ppd.disc,
                ppd.sub_total as total
            FROM nscc_po_details ppd
                INNER JOIN nscc_barang pbv ON ppd.barang_id = pbv.barang_id
            WHERE 
                ppd.po_id = :po_id
                AND ppd.visible = 1
            ORDER BY ppd.seq
        ");
        return $comm->queryAll(true, array(':po_id' => $po_id));
    }
    
    public static function get_historyHarga($barang_id)
    {
        $param = array(
            ':barang_id' => $barang_id
        );
        $comm = Yii::app()->db->createCommand("
            SELECT
                pod.price, po.tgl, s.supplier_name
            FROM nscc_po_details pod
                LEFT JOIN nscc_po po ON pod.po_id = po.po_id
                LEFT JOIN nscc_supplier s ON s.supplier_id = po.supplier_id
            WHERE
                    pod.barang_id = :barang_id
            ORDER BY po.tgl DESC
            LIMIT 10;");
        return $comm->queryAll(true, $param);
    }
}
