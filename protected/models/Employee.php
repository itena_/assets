<?php

Yii::import('application.models._base.BaseEmployee');

class Employee extends BaseEmployee {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeValidate() {
        if ($this->employee_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->employee_id = $uuid;
        }
        return parent::beforeValidate();
    }

}
