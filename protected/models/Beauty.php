<?php
Yii::import('application.models._base.BaseBeauty');
class Beauty extends BaseBeauty
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'beauty_id';
    }
    public function beforeValidate()
    {
        if ($this->beauty_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->beauty_id = $uuid;
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        return parent::beforeValidate();
    }
}