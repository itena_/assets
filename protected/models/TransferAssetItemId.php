<?php
Yii::import('application.models._base.BaseTransferAssetItemId');

class TransferAssetItemId extends BaseTransferAssetItemId
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->transfer_asset_item_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->transfer_asset_item_id = $uuid;
        }
        return parent::beforeValidate();
    }
}