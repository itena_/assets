<?php
Yii::import('application.models._base.BaseLahaImportDetails');
class LahaImportDetails extends BaseLahaImportDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->laha_import_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->laha_import_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}