<?php
Yii::import('application.models._base.BaseMemberCard');

class MemberCard extends BaseMemberCard
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->membercard_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->membercard_id = $uuid;
        }
        return parent::beforeValidate();
    }
}