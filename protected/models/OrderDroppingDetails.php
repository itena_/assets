<?php
Yii::import('application.models._base.BaseOrderDroppingDetails');

class OrderDroppingDetails extends BaseOrderDroppingDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->order_dropping_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->order_dropping_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
    static function get_details_to_print($order_dropping_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                b.kode_barang,
                b.nama_barang,
                tid.qty,
                b.sat
            FROM nscc_order_dropping_details tid
                LEFT JOIN nscc_barang b ON b.barang_id = tid.barang_id
            WHERE 
                tid.order_dropping_id = :order_dropping_id
                AND tid.visible = 1
                ORDER BY b.kode_barang ASC
        ");
        return $comm->queryAll(true, array(':order_dropping_id' => $order_dropping_id));
    }
	static function get_details_all($criteria)
	{
		$cmd = DbCmd::instance()
			->addSelect(array(
				'b.kode_barang',
				'b.nama_barang',
				'tid.*',
				'b.sat'
			))
			->addFrom('{{order_dropping_details}} as tid')
			->addLeftJoin('{{barang}} as b', 'b.barang_id = tid.barang_id')
			->addCondition('tid.order_dropping_id = :order_dropping_id')
			->addCondition('tid.visible = 1')
			->addParam(':order_dropping_id', $criteria['order_dropping_id'])
		;

		return $cmd;
	}
}