<?php
Yii::import('application.models._base.BaseTerimaBarang');
class TerimaBarang extends BaseTerimaBarang
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->terima_barang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->terima_barang_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        
        return parent::beforeValidate();
    }
    static function get_details_to_print($terima_barang_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                b.kode_barang,
                b.nama_barang,
                tid.qty,
                b.sat
            FROM nscc_terima_barang_details tid
                LEFT JOIN nscc_barang b ON b.barang_id = tid.barang_id
            WHERE 
                tid.terima_barang_id = :terima_barang_id
                AND tid.visible = 1
            ORDER BY tid.seq
        ");
        return $comm->queryAll(true, array(':terima_barang_id' => $terima_barang_id));
    }
}