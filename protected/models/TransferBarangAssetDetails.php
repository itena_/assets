<?php
Yii::import('application.models._base.BaseTransferBarangAssetDetails');

class TransferBarangAssetDetails extends BaseTransferBarangAssetDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->transfer_asset_details_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->transfer_asset_details_id = $uuid;
        }
        return parent::beforeValidate();
    }
}