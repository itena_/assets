<?php
Yii::import('application.models._base.BaseReferralTrans');

class ReferralTrans extends BaseReferralTrans
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->referral_trans == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->referral_trans = $uuid;
        }
        return parent::beforeValidate();
    }
}