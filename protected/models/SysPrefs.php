<?php

Yii::import('application.models._base.BaseSysPrefs');
class SysPrefs extends BaseSysPrefs
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_val($index,$store = STOREID)
    {
        $pref = SysPrefs::model()->find('name_ = :name_ AND store = :store',
            array(':name_' => $index, ':store' => $store));
        if($pref == null){
            return "";
        }
        return $pref->value_;
    }
    public static function save_value($name_, $value_, $store)
    {
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO nscc_sys_prefs (name_, value_, store, up)
                VALUES (:name_, :value_, :store, 0)"
        );
        return $comm->execute(array(':name_' => $name_, ':value_' => $value_, ':store' => $store));
    }
    public function beforeValidate()
    {
        if ($this->sys_prefs_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->sys_prefs_id = $uuid;
        }
        return parent::beforeValidate();
    }
}