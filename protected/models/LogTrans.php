<?php
Yii::import( 'application.models._base.BaseLogTrans' );
class LogTrans extends BaseLogTrans {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public function beforeValidate() {
		if ( $this->log_trans_id == null ) {
			$command            = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid               = $command->queryScalar();
			$this->log_trans_id = $uuid;
		}
		if ( $this->tdate == null ) {
			$this->tdate = new CDbExpression( 'NOW()' );
		}
		if ( $this->user_id == null ) {
			$this->user_id = Yii::app()->user->getId();
		}
		if ( $this->browser_id == null ) {
			$this->browser_id = Yii::app()->user->getBrowser();
		}
		if ( $this->session == null ) {
			$this->session = Yii::app()->user->getSession();
		}
		$users          = Users::model()->findByPk( $this->user_id );
		$this->username = $users->user_id;
		$this->name     = $users->name;
		return parent::beforeValidate();
	}
	public static function saveLog( $type, $type_no, $action, $note ) {
		$model          = new LogTrans();
		$model->type    = $type;
		$model->type_no = $type_no;
		$model->action  = $action;
		$model->note    = $note;
		$model->save();
		return $model;
	}
}