<?php
Yii::import('application.models._base.BaseMonitorAntrian');
class MonitorAntrian extends BaseMonitorAntrian
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'id_antrian';
    }
}