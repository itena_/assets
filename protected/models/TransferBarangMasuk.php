<?php
Yii::import('application.models._base.BaseTransferBarangMasuk');

class TransferBarangMasuk extends BaseTransferBarangMasuk
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->transfer_barang_masuk_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->transfer_barang_masuk_id = $uuid;
        }
        if ($this->tdate_trbrg == null) {
            $this->tdate_trbrg = new CDbExpression('NOW()');
        }
        if ($this->user_id == null) {
            $this->user_id = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
}