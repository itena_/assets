<?php
Yii::import('application.models._base.BaseDiskon');
class Diskon extends BaseDiskon
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function save_diskon($barang_id, $type_, $ref_id, $value, $store = STOREID)
    {
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO `{{diskon}}` (`value`, `barang_id`, `ref_id`, store, `type_`)
            VALUES (:value,:barang_id,:ref_id,:store,:type_)");
        return $comm->execute([
            ':value' => $value, 'barang_id' => $barang_id,
            ':ref_id' => $ref_id, ':store' => $store, ':type_' => $type_
        ]);
    }
    public static function save_diskon_by_grup($type_, $ref_id, $grup, $value, $store = STOREID)
    {
        $comm = Yii::app()->db->createCommand(
            "REPLACE INTO `{{diskon}}` (`ref_id`,`barang_id`,`value`, store, `type_`)
            SELECT :ref_id AS ref_id,nb.barang_id,:value AS `value`,:store AS store,:type_ AS type_ 
            FROM nscc_barang nb WHERE nb.grup_id = :grup_id"
        );
        return $comm->execute([
            ':value' => $value, ':ref_id' => $ref_id, ':store' => $store,
            ':grup_id' => $grup, ':type_' => $type_
        ]);
    }
    public function beforeValidate()
    {
        if ($this->diskon_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->diskon_id = $uuid;
        }
        return parent::beforeValidate();
    }
}