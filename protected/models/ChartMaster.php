<?php
Yii::import('application.models._base.BaseChartMaster');
class ChartMaster extends BaseChartMaster
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_child($coa, $inc_header = false)
    {
        $res = array();
        $criteria = new CDbCriteria();
        $criteria->addCondition("kategori = :account_code");
        $criteria->params = array(':account_code' => $coa);
        $chart = ChartMaster::model()->findAll($criteria);
        foreach ($chart as $c) {
            if ($c->header == 1) {
                if ($inc_header) {
                    $res[] = $c;
                }
            } else {
                $res[] = $c;
            }
            $childArr = Self::get_child($c->account_code, $inc_header);
            $res = array_merge($res, $childArr);
        }
        return $res;
    }
    public static function get_laba_rugi($from, $to, $store = null)
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND pgt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT pcm.account_code,pcm.account_name,
        IFNULL(Sum(pgt.amount),0) total FROM nscc_gl_trans AS pgt
            RIGHT JOIN nscc_chart_master pcm ON (pgt.account_code = pcm.account_code AND
        pgt.tran_date >= :from AND pgt.tran_date <= :to AND pgt.visible = 1)
        WHERE pcm.tipe = 'L' $where
        GROUP BY pcm.account_code;");
        return $comm->queryAll(true, $param);
    }
    public static function get_laba_rugi_new($from, $to, $store = null)
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND pgt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
       nct.`name` AS name_1,ncm.account_code,ncm.account_name,ncm.saldo_normal,
				IFNULL(Sum(pgt.amount),0) AS total,
        ncc.ctype,nct.id AS id_1
        FROM nscc_chart_master AS ncm
        LEFT JOIN nscc_chart_types AS nct ON ncm.kategori = nct.id
        LEFT JOIN nscc_chart_types AS nct1 ON nct.parent = nct1.id
        LEFT JOIN nscc_chart_types AS nct2 ON nct1.parent = nct2.id
        LEFT JOIN nscc_chart_class AS ncc ON COALESCE(nct2.class_id,nct1.class_id,nct.class_id) = ncc.cid
        LEFT JOIN nscc_gl_trans AS pgt ON ncm.account_code = pgt.account_code AND
        pgt.tran_date >= :from AND pgt.tran_date <= :to $where AND pgt.visible = 1
        WHERE ncc.ctype IN (4,5,6,7)
        GROUP BY ncm.account_code
        ORDER BY ncm.account_code");
        return $comm->queryAll(true, $param);
    }
    public static function get_neraca_new($to, $store = null)
    {
        $where = "";
        $param = array(':to' => $to);
        if ($store != null) {
            $where = "AND pgt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
       nct.`name` AS name_1,ncm.account_code,ncm.account_name,ncm.saldo_normal,
				IFNULL(Sum(pgt.amount),0) AS total,
        ncc.ctype,nct.id AS id_1
        FROM nscc_chart_master AS ncm
        LEFT JOIN nscc_chart_types AS nct ON ncm.kategori = nct.id
        LEFT JOIN nscc_chart_types AS nct1 ON nct.parent = nct1.id
        LEFT JOIN nscc_chart_types AS nct2 ON nct1.parent = nct2.id
        LEFT JOIN nscc_chart_class AS ncc ON COALESCE(nct2.class_id,nct1.class_id,nct.class_id) = ncc.cid
        LEFT JOIN nscc_gl_trans AS pgt ON ncm.account_code = pgt.account_code AND
        pgt.tran_date <= :to $where AND pgt.visible = 1
        WHERE ncc.ctype IN (1,2,3,8,9)
        GROUP BY ncm.account_code
        ORDER BY ncm.account_code");
        return $comm->queryAll(true, $param);
    }
    public static function get_neraca($from, $to, $store = null)
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND pgt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT pcm.account_code,pcm.account_name,pcm.header,
    SUM(IF (pgt.tran_date < :from, pgt.amount, 0)) `before`,
    SUM(IF (pgt.amount >= 0 AND pgt.tran_date >= :from
		  AND pgt.tran_date <= :to, pgt.amount, 0)) `debit`,
    SUM(IF (pgt.amount < 0 AND pgt.tran_date >= :from
		  AND pgt.tran_date <= :to, ABS(pgt.amount), 0)) `kredit`,
    SUM(IF (pgt.tran_date <= :to, pgt.amount, 0)) `after`
    FROM nscc_chart_master pcm 
    LEFT JOIN nscc_gl_trans AS pgt ON (pgt.account_code = pcm.account_code AND pgt.visible = 1)
    WHERE pcm.tipe = 'N' $where GROUP BY pcm.account_code;");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    public static function getBalanceSheet($from, $to, $store = null)
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND pgt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT nct.`name` AS name_1,
        ncc.ctype,nct.id AS id_1,pcm.account_code,pcm.account_name,pcm.header,        
         SUM(IF (pgt.tran_date < :from, IF(pcm.saldo_normal = 'D',pgt.amount,-pgt.amount), 0)) `before`,
    SUM(IF (pgt.amount >= 0 AND pgt.tran_date >= :from
		  AND pgt.tran_date <= :to, pgt.amount, 0)) `debit`,
    SUM(IF (pgt.amount < 0 AND pgt.tran_date >= :from
		  AND pgt.tran_date <= :to, ABS(pgt.amount), 0)) `kredit`,
    SUM(IF (pgt.tran_date <= :to, IF(pcm.saldo_normal = 'D',pgt.amount,-pgt.amount), 0)) `after`
    FROM nscc_chart_master pcm 
    LEFT JOIN nscc_chart_types AS nct ON pcm.kategori = nct.id
        LEFT JOIN nscc_chart_types AS nct1 ON nct.parent = nct1.id
        LEFT JOIN nscc_chart_types AS nct2 ON nct1.parent = nct2.id
	LEFT JOIN nscc_chart_class AS ncc ON COALESCE(nct2.class_id,nct1.class_id,nct.class_id) = ncc.cid
    LEFT JOIN nscc_gl_trans AS pgt ON (pgt.account_code = pcm.account_code AND pgt.visible = 1)
    WHERE ncc.ctype IN (1,2,3,8,9) $where GROUP BY pcm.account_code;");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }
    public static function get_saldo_until($tgl, $account)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        Sum(pgt.amount) FROM psn_gl_trans AS pgt
        WHERE pgt.tran_date <= :tgl AND
        pgt.account_code = :account_code");
        return $comm->queryScalar(array(':tgl' => $tgl, ':account_code' => $account));
    }
    public static function print_chart($id, &$lr, $arus = 1)
    {
        $arr = array();
        $child = ChartTypes::get_child($id);
        foreach ($child as $child_item) {
            if ($child_item['hide'] != '1') {
                $arr[] = array(
                    'account_code' => '',
                    'account_name' => $child_item['name'],
                    'total' => ''
                );
            }
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $child_item['id']) {
                    $arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $arr = array_merge($arr, self::print_chart($child_item['id'], $lr, $arus));
        }
        return $arr;
    }
    public static function printBalanceSheet($id, &$lr, $arus = 1)
    {
        $arr = array();
        $child = ChartTypes::get_child($id);
        foreach ($child as $child_item) {
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $child_item['id']) {
                    $arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'before' => $item['before'],
                        'debit' => $item['debit'],
                        'kredit' => $item['kredit'],
                        'after' => $item['after']
                    );
                    unset($lr[$key]);
                }
            }
            $arr = array_merge($arr, self::print_chart($child_item['id'], $lr, $arus));
        }
        return $arr;
    }
    public static function print_chart_konsolidasi($id, &$lr, $store = array())
    {
        $arr = array();
        $label = array();
        $child = ChartTypes::get_child($id);
        foreach ($child as $child_item) {
            if ($child_item['hide'] != '1') {
                $label[] = array(
                    'id' => '',
                    'account_code' => '',
                    'account_name' => $child_item['name']
                );
            }
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $child_item['id']) {
                    foreach ($store as $cab) {
                        $arr[$cab][$item['account_code']] =
                            (($item['saldo_normal'] == 'D' ? 1 : -1) * $item[$cab]);
                        $arr['TOTAL'][$item['account_code']] +=
                            $arr[$cab][$item['account_code']];
                    }
                    unset($lr[$key]);
                }
            }
            $result = self::print_chart_konsolidasi($child_item['id'], $lr, $store);
            $arr = array_merge($arr, $result['arr']);
            $label = array_merge($label, $result['label']);
        }
        return array('arr' => $arr, 'label' => $label);
    }
    protected function beforeDelete()
    {
        if (count($this->glTrans) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan jurnal.');
        }
        if (count($this->banks) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan bank.');
        }
        if (count($this->kasDetails) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan kas detail.');
        }
        if (count($this->suppliers) > 0) {
            throw new CDbException('Tidak bisa dihapus karena digunakan supplier.');
        }
        return parent::beforeDelete();
    }
    public static function create_laba_rugi_new($from, $to, $store = "")
    {
        $lr = ChartMaster::get_laba_rugi_new($from, $to, $store);
        $income = ChartTypes::get_chart_types_by_class(CL_INCOME);
        $income_arr = array();
        foreach ($income as $row) {
            $income_arr[] = array(
                'id' => '',
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $income_arr[] = array(
                        'id' => $item['account_code'],
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $income_arr = array_merge($income_arr, ChartMaster::print_chart($row['id'], $lr, -1));
        }
        $total_income = array_sum(array_column($income_arr, 'total'));
        $hpp = ChartTypes::get_chart_types_by_class(CL_COGS);
        $hpp_arr = array();
        foreach ($hpp as $row) {
            $hpp_arr[] = array(
                'id' => '',
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $hpp_arr[] = array(
                        'id' => $item['account_code'],
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $hpp_arr = array_merge($hpp_arr, ChartMaster::print_chart($row['id'], $lr));
        }
        $total_hpp = array_sum(array_column($hpp_arr, 'total'));
        $laba_kotor = $total_income - $total_hpp;
        $cost = ChartTypes::get_chart_types_by_class(CL_EXPENSE);
        $cost_arr = array();
        foreach ($cost as $row) {
            $cost_arr[] = array(
                'id' => '',
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $cost_arr[] = array(
                        'id' => $item['account_code'],
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $cost_arr = array_merge($cost_arr, ChartMaster::print_chart($row['id'], $lr));
        }
        $total_biaya = array_sum(array_column($cost_arr, 'total'));
        $total_laba_bersih = $laba_kotor - $total_biaya;
        $other_income = ChartTypes::get_chart_types_by_class(CL_OTHER_INCOME);
        $other_income_arr = array();
        foreach ($other_income as $row) {
            $other_income_arr[] = array(
                'id' => '',
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $other_income_arr[] = array(
                        'id' => $item['account_code'],
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $other_income_arr = array_merge($other_income_arr, ChartMaster::print_chart($row['id'], $lr));
        }
        $total_other_income = array_sum(array_column($other_income_arr, 'total'));
        $laba_bersih_sebelum_pajak = $total_laba_bersih - $total_other_income;
        if ($store == '') {
            $store = 'ALL CABANG';
        }
        return array(
            'header' => array(
                array(
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store,
                    'income' => $total_income,
                    'hpp' => $total_hpp,
                    'laba_kotor' => $laba_kotor,
                    'biaya' => $total_biaya,
                    'laba_bersih' => $total_laba_bersih,
                    'other_income' => $total_other_income,
                    'laba_bersih_sebelum_pajak' => $laba_bersih_sebelum_pajak,
                    'income_label' => 'Income',
                    'hpp_label' => 'HPP'
                )
            ),
            'income' => $income_arr,
            'hpp' => $hpp_arr,
            'biaya' => $cost_arr,
            'other_income' => $other_income_arr
        );
    }
}