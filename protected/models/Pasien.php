<?php
Yii::import('application.models._base.BasePasien');
class Pasien extends BasePasien
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public static function get_detail_pasien($nobase)
    {
        if(NATASHA_CUSTOM)
        {
            $param = array(':no_base' => $nobase);
            $comm = Yii::app()->dbnars->createCommand("SELECT T.no_faktur, I.kd_brng, 
            upper(I.satuan) AS satuan, DATE_FORMAT(T.tgl,'%d %b %Y') tgl, I.qty AS qty, 
            C.kode, C.nama, I.ketpot, T.ketdisc, T.tgl tgli, P.nobase, P.namacus, P.alamat, P.telp, P.tgllh
            FROM faktur AS T
            INNER JOIN item AS I ON I.no_faktur=T.no_faktur AND I.id_cabang = T.id_cabang AND I.tahun = T.tahun
            LEFT JOIN cabang AS C ON C.id=T.id_cabang
			INNER JOIN pasien as P on T.no_base=P.nobase
            WHERE T.no_base=:no_base
            GROUP BY T.no_faktur, T.no_base, I.kd_brng
            HAVING qty > 0
            ORDER BY T.tgl DESC, T.no_faktur ASC");
            return $comm->queryAll(true, $param);
        }
        else
        {
            $param = array(':no_base' => $nobase);
            $comm = Yii::app()->dbnars->createCommand("SELECT T.no_faktur, I.kd_brng, 
            upper(I.satuan) AS satuan, DATE_FORMAT(T.tgl,'%d %b %Y') tgl, SUM(I.qty) AS qty, 
            C.kode, C.nama, I.ketpot, T.ketdisc, T.tgl tgli
            FROM faktur AS T
            INNER JOIN item AS I ON I.no_faktur=T.no_faktur AND I.id_cabang = T.id_cabang AND I.tahun = T.tahun
            LEFT JOIN cabang AS C ON C.id=T.id_cabang
            WHERE T.no_base=:no_base
            GROUP BY T.no_faktur, T.no_base, I.kd_brng
            HAVING qty > 0
            ORDER BY T.tgl DESC, T.no_faktur ASC");
            return $comm->queryAll(true, $param);
        }
    }
    public static function get_detail_pasien_local($customer_id)
    {
        $param = array(':customer_id' => $customer_id);
        $comm = Yii::app()->db->createCommand("
            SELECT 
	doc_ref as no_faktur
        , kode_barang as kd_brng
        , sat
        , DATE_FORMAT(tgl,'%d %b %Y') tgl
        , qty
        , store as kode
        , ketpot
        , ketdisc
        , tgl tgli
        FROM
                nscc_salestrans_details_barang
        WHERE
	customer_id = :customer_id
        ORDER BY tgl DESC, doc_ref ASC");
        return $comm->queryAll(true, $param);        
    }

    public static function get_diagnosa_pasien($id)
    {
        $param = array(':id' => $id);
        $comm = Yii::app()->dbnars->createCommand("SELECT *,DATE_FORMAT(tgl,'%d %b %Y') tgld, tgl tgli
        FROM diagnosa
        WHERE customer_id = :id
        ORDER BY tgl desc");
        return $comm->queryAll(true, $param);
    }
    public static function get_diagnosa_pasien_local($id)
    {
        $param = array(':id' => $id);
        $comm = Yii::app()->db->createCommand("SELECT *,DATE_FORMAT(tgl,'%d %b %Y') tgld, tgl tgli
        FROM nscc_diagnosa_view
        WHERE customer_id = :id
        ORDER BY tgl desc");
        return $comm->queryAll(true, $param);
    }
}