<?php
Yii::import('application.models._base.BaseGroupAsset');

class GroupAsset extends BaseGroupAsset
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->group_asset_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->group_asset_id = $uuid;
        }
        return parent::beforeValidate();
    }
}