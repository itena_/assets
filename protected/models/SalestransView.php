<?php
Yii::import('application.models._base.BaseSalestransView');
class SalestransView extends BaseSalestransView
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'salestrans_id';
    }
}