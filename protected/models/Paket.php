<?php
Yii::import('application.models._base.BasePaket');

class Paket extends BasePaket
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->paket_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->paket_id = $uuid;
        }
        return parent::beforeValidate();
    }
}