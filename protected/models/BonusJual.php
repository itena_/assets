<?php
Yii::import('application.models._base.BaseBonusJual');

class BonusJual extends BaseBonusJual
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->bonus_jual_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->bonus_jual_id = $uuid;
        }
        return parent::beforeValidate();
    }
}