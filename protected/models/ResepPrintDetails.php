<?php
Yii::import('application.models._base.BaseResepPrintDetails');
class ResepPrintDetails extends BaseResepPrintDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->resep_print_details_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->resep_print_details_id = $uuid;
        }
        return parent::beforeValidate();
    }
}