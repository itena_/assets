<?php
Yii::import('application.models._base.BaseTerimaBarangDetails');
class TerimaBarangDetails extends BaseTerimaBarangDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->terima_barang_details_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->terima_barang_details_id = $uuid;
        }
        return parent::beforeValidate();
    }
    public static function get_item_detil_invoice(){
        $comm = Yii::app()->db->createCommand("
            SELECT
                tbd.barang_id,
                tbd.seq,
                tbd.qty,
                pod.charge,
                pod.price,
                pod.disc,
                pod.ppn,
                pod.pph_id,
                pod.pph,
                tbd.item_id
            FROM nscc_terima_barang_details tbd
                    LEFT JOIN nscc_terima_barang tb ON tb.terima_barang_id = tbd.terima_barang_id
                LEFT JOIN nscc_po_details pod ON tb.po_id = pod.po_id
                    AND tbd.barang_id = pod.barang_id
                    AND tbd.item_id = pod.item_id
                    AND pod.visible = 1
            WHERE
                    tbd.terima_barang_id = :terima_barang_id
                AND tbd.visible = 1
        ");
        return $comm->queryAll(true,array(':terima_barang_id'=>$_POST['terima_barang_id']));
    }
    public static function get_item_detil_return(){
        $comm = Yii::app()->db->createCommand("
            SELECT
                tbd.barang_id,
                tbd.seq,
                (tbd.qty-ifnull(t.qty,0)) qty,
                pod.charge,
                pod.price,
                pod.disc,
                pod.ppn,
                pod.pph
            FROM nscc_terima_barang_details tbd
                    LEFT JOIN nscc_terima_barang tb ON tb.terima_barang_id = tbd.terima_barang_id
                LEFT JOIN nscc_po_details pod ON tb.po_id = pod.po_id AND tbd.barang_id = pod.barang_id AND pod.visible = 1
                LEFT JOIN (
                            SELECT
                                    tid.barang_id,
                        sum(tid.qty) qty
                            FROM nscc_transfer_item_details tid
                                    LEFT JOIN nscc_transfer_item ti ON tid.transfer_item_id = ti.transfer_item_id
                            WHERE
                                    tid.visible = 1
                        AND ti.terima_barang_id = :terima_barang_id
                        AND ti.type_ = 1
                            GROUP BY tid.barang_id
                ) t ON tbd.barang_id = t.barang_id
            WHERE
                    tbd.terima_barang_id = :terima_barang_id
                AND tbd.visible = 1
        ");
        return $comm->queryAll(true,array(':terima_barang_id'=>$_POST['terima_barang_id']));
    }
}