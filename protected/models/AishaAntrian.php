<?php
Yii::import('application.models._base.BaseAishaAntrian');
class AishaAntrian extends BaseAishaAntrian
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->id_antrian == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->id_antrian = $uuid;
        }
        return parent::beforeValidate();
    }
}