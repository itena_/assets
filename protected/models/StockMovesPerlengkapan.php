<?php
Yii::import('application.models._base.BaseStockMovesPerlengkapan');

class StockMovesPerlengkapan extends BaseStockMovesPerlengkapan
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->stock_moves_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->stock_moves_id = $uuid;
        }
        if ($this->tdate == null) {
            $this->tdate = new CDbExpression('NOW()');
        }
        if ($this->store == null) {
            $this->store = STOREID;
        }
        if ($this->id_user == null) {
            $this->id_user = Yii::app()->user->getId();
        }
        return parent::beforeValidate();
    }
    
    public static function get_saldo_item($barang_id, $store)
    {
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nsm.qty),0) FROM nscc_stock_moves_perlengkapan AS nsm
        WHERE nsm.barang_id = :barang_id AND nsm.visible = 1 AND nsm.store = :store");
        return $comm->queryScalar(array(':barang_id' => $barang_id,
            ':store' => $store));
    }

    public static function push($trans_no = "")
    {
        $arrAttr = array('up' => 0);
        if($trans_no){
            $arrAttr['trans_no'] = $trans_no;
        }
        $models = StockMovesPerlengkapan::model()->findAllByAttributes($arrAttr);
        foreach ($models as $model) {
            $model->up = 1;
            if($model->save()){
                U::runCommand('soap','stockmovesperlengkapan_', '--id=' . $model->stock_moves_id,  'stockmovesperlengkapan_'.$model->stock_moves_id.'.log');
            }
        }
    }
}