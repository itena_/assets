<?php
Yii::import('application.models._base.BaseReceiveDroppingDetails');

class ReceiveDroppingDetails extends BaseReceiveDroppingDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->receive_dropping_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->receive_dropping_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}