<?php
Yii::import( 'application.models._base.BaseUsers' );
class Users extends BaseUsers {
	public static function model( $className = __CLASS__ ) {
		return parent::model( $className );
	}
	public static function is_audit() {
		$id   = Yii::app()->user->getId();
		$user = Users::model()->findByPk( $id );
		return strtolower( substr( $user->user_id, 0, 3 ) ) === strtolower( SysPrefs::get_val( 'audit' ) ) ? 1 : 0;
	}
	public static function get_override( $user, $pass ) {
		$comm = Yii::app()->db->createCommand( "SELECT ns.id FROM nscc_users AS ns
        INNER JOIN nscc_security_roles AS nsc ON ns.security_roles_id = nsc.security_roles_id
        WHERE ns.`password` = :pass AND ns.user_id = :user AND nsc.sections LIKE '%209%'" );
		return $comm->queryScalar( array( ':pass' => $pass, ':user' => $user ) );
	}
	public static function get_access( $user, $pass, $section ) {
		$comm = Yii::app()->db->createCommand( "SELECT ns.id FROM nscc_users AS ns
        INNER JOIN nscc_security_roles AS nsc ON ns.security_roles_id = nsc.security_roles_id
        WHERE ns.`password` = :pass AND ns.user_id = :user AND nsc.sections LIKE :section" );
		return $comm->queryScalar( array( ':pass' => $pass, ':user' => $user, ':section' => '%' . $section . '%' ) );
	}
	public function beforeValidate() {
		if ( $this->id == null ) {
			$command  = $this->dbConnection->createCommand( "SELECT UUID();" );
			$uuid     = $command->queryScalar();
			$this->id = $uuid;
		}
		/*if ( $this->store == null ) {
			$this->store = STOREID;
		}*/
		return parent::beforeValidate();
	}
	public function is_available_role( $role ) {
		$sections = explode( ',', $this->securityRoles->sections );
		return in_array( $role, $sections );
	}
	/**
	 * session_exists()
	 * Will check if the needed session keys exists.
	 *
	 * @return {boolean} True if keys exists, else false
	 */
	private function session_exists() {
		return isset( $_SESSION['USER_AGENT_KEY'] ) && isset( $_SESSION['INIT'] );
	}
	/**
	 * session_validate()
	 * Will check if a user has a encrypted key stored in the session array.
	 * If it returns true, user is the same as before
	 * If the method returns false, the session_id is regenerated
	 *
	 * @param {String} $email    The users email adress
	 *
	 * @return {boolean} True if valid session, else false
	 */
	public function session_validate() {
		// Encrypt information about this session
		$user_agent = $this->session_hash_string( $_SERVER['HTTP_USER_AGENT'], $this->id );
		// Check for instance of session
		if ( $this->session_exists() == false ) {
			// The session does not exist, create it
			$this->session_reset( $user_agent );
		} else {
			if ( ! $this->session_match( $user_agent ) ) {
				Yii::app()->user->logout();
//				$this->redirect( Yii::app()->homeUrl );
			}
		}
		// Match the hashed key in session against the new hashed string
		// The hashed string is different, reset session
//		$this->session_reset( $user_agent );
//		return false;
	}
	/**
	 * session_match()
	 * Compares the session secret with the current generated secret.
	 *
	 * @param {String} $user_agent The encrypted key
	 */
	private function session_match( $user_agent ) {
		// Validate the agent and initiated
		return $this->session == $user_agent && $_SESSION['USER_AGENT_KEY'] == $user_agent
		       && $_SESSION['INIT'] == true;
	}
	/**
	 * session_encrypt()
	 * Generates a unique encrypted string
	 *
	 * @param {String} $user_agent        The http_user_agent constant
	 * @param {String} $unique_string     Something unique for the user (email, etc)
	 */
	public function session_hash_string( $user_agent, $unique_string ) {
		return md5( $user_agent . $unique_string );
	}
	/**
	 * session_reset()
	 * Will regenerate the session_id (the local file) and build a new
	 * secret for the user.
	 *
	 * @param {String} $user_agent
	 */
	private function session_reset( $user_agent ) {
		// Create new id
		session_regenerate_id( true );
		$_SESSION         = array();
		$_SESSION['INIT'] = true;
		// Set hashed http user agent
		$_SESSION['USER_AGENT_KEY'] = $user_agent;
		$this->session              = $user_agent;
		$this->save();
	}
	/**
	 * Destroys the session
	 */
	private function session_destroy() {
		// Destroy session
		session_destroy();
	}
}