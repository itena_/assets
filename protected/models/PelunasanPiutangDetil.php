<?php
Yii::import('application.models._base.BasePelunasanPiutangDetil');

class PelunasanPiutangDetil extends BasePelunasanPiutangDetil
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public static function get_piutang($customer_id)
	{
		$comm = Yii::app()->db->createCommand("SELECT nti.payment_journal_id,nti.tgl,
        nti.doc_ref_other no_faktur,nti.total nilai,nti.doc_ref,
        nti.total - COALESCE(SUM(npud.kas_dibayar),0) AS sisa
        FROM nscc_payment_journal nti
        LEFT JOIN nscc_pembantu_pelunasan_piutang_detil npud ON nti.payment_journal_id = npud.payment_journal_id
        WHERE nti.customer_id = :customer_id AND nti.p = 1
        GROUP BY nti.payment_journal_id
        HAVING sisa != 0");
		return $comm->queryAll(true, array(
			':customer_id' => $customer_id
		));
	}

	public function beforeValidate(){
        if ($this->pelunasan_piutang_detil_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->pelunasan_piutang_detil_id = $uuid;
        }
        return parent::beforeValidate();
    }
}