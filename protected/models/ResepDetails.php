<?php
Yii::import('application.models._base.BaseResepDetails');
class ResepDetails extends BaseResepDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->resep_details_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->resep_details_id = $uuid;
        }
        return parent::beforeValidate();
    }
}