<?php
Yii::import('application.models._base.BaseSalestransDetails');
class SalestransDetails extends BaseSalestransDetails
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->salestrans_details == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->salestrans_details = $uuid;
        }
        return parent::beforeValidate();
    }
    public function save_beauty_tips($beauty_id, $beauty2_id, $beauty3_id, $beauty4_id, $beauty5_id)
    {
//        $delete = Yii::app()->db->createCommand("
//        DELETE FROM nscc_beauty_services WHERE salestrans_details = :salestrans_details");
//        $delete->execute(array(':salestrans_details' => $this->salestrans_details));
        BeautyServices::model()->updateAll(array('visible' => 0), 'salestrans_details = :salestrans_details',
            array(':salestrans_details' => $this->salestrans_details));
        if ($beauty_id != null) {
            BeautyServices::add_beauty_tips($this, $beauty_id, 1);
        }
        if ($beauty2_id != null) {
            BeautyServices::add_beauty_tips($this, $beauty2_id, 2);
        }
        if ($beauty3_id != null) {
            BeautyServices::add_beauty_tips($this, $beauty3_id, 3);
        }
        if ($beauty4_id != null) {
            BeautyServices::add_beauty_tips($this, $beauty4_id, 4);
        }
        if ($beauty5_id != null) {
            BeautyServices::add_beauty_tips($this, $beauty5_id, 5);
        }
    }
    public static function get_diskon($customer_id, $barang_id, $store = STOREID)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT IFNULL(nj.price,0) price,IFNULL(nj.disc,0) `value`,'REG' AS kode,'REGULAR' AS nama
        FROM nscc_barang AS nb
            LEFT JOIN nscc_jual AS nj ON nj.barang_id = nb.barang_id
        WHERE nj.store = :store AND nb.barang_id = :barang_id 
        UNION ALL
		SELECT IFNULL(nj.price,0) price,IFNULL(nd.`value`,0) `value`,np.kode_promosi AS kode,np.nama_promosi AS nama
        FROM nscc_barang AS nb
            LEFT JOIN nscc_jual AS nj ON nj.barang_id = nb.barang_id
            LEFT JOIN nscc_diskon AS nd ON nd.barang_id = nj.barang_id AND nd.store = nj.store
            LEFT JOIN nscc_promosi AS np ON nd.ref_id = np.promosi_id AND nd.type_ = 1 AND 
              DATE(NOW()) >= awal AND DATE(NOW()) <= akhir
        WHERE nj.store = :store AND nb.barang_id = :barang_id 
        UNION ALL
        SELECT IFNULL(nj.price,0) price,IFNULL(nd.`value`,0) `value`,nsc.kode_status AS kode,nsc.nama_status AS nama
        FROM nscc_barang AS nb
            LEFT JOIN nscc_jual AS nj ON nj.barang_id = nb.barang_id
            LEFT JOIN nscc_diskon AS nd ON nd.barang_id = nj.barang_id AND nd.store = nj.store
            LEFT JOIN nscc_status_cust AS nsc ON nd.ref_id = nsc.status_cust_id AND nd.type_ = 0
            LEFT JOIN nscc_customers AS ncus ON ncus.status_cust_id = nsc.status_cust_id
        WHERE nj.store = :store AND nb.barang_id = :barang_id AND 
				ncus.customer_id = :customer_id AND 
          ncus.validcard >= DATE(NOW()) 
        ORDER BY `value` DESC");
        return $comm->queryRow(true, [
            ':customer_id' => $customer_id,
            ':barang_id' => $barang_id,
            ':store' => $store
        ]);
    }
    public static function get_sales_trans_details($salestrans_id)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT nsd.salestrans_details,nsd.barang_id,nsd.salestrans_id,nsd.qty,nsd.disc,nsd.discrp,
        nsd.ketpot,nsd.vat,nsd.vatrp,nsd.bruto,nsd.total,nsd.total_pot,nsd.price,nsd.jasa_dokter,
        nsd.dokter_id,disc_name,paket_trans_id,paket_details_id,
        (SELECT nsb.beauty_id FROM nscc_beauty_services nsb
        WHERE nsb.salestrans_details = nsd.salestrans_details AND nsb.tipe = 1 AND nsb.visible = 1 LIMIT 1) beauty_id,
        (SELECT nsb.beauty_id FROM nscc_beauty_services nsb
        WHERE nsb.salestrans_details = nsd.salestrans_details AND nsb.tipe = 2 AND nsb.visible = 1 LIMIT 1) beauty2_id,
	    nb.kode_barang,nb.nama_barang
        FROM nscc_barang nb
	    INNER JOIN nscc_salestrans_details nsd
	    ON nb.barang_id = nsd.barang_id
        WHERE nsd.salestrans_id = :salestrans_id");
        return $comm->queryAll(true, array(':salestrans_id' => $salestrans_id));
    }
    public static function get_retursales_trans_details($salestrans_id)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT nsd.salestrans_details,nsd.barang_id,nsd.salestrans_id,-nsd.qty qty,-nsd.disc disc,-nsd.discrp discrp,
        -nsd.vat vat,-nsd.vatrp vatrp,-nsd.bruto bruto,-nsd.total total,-nsd.total_pot total_pot,
        nsd.ketpot,nsd.price,nsd.jasa_dokter,nsd.dokter_id,disc_name,nb.kode_barang,nb.nama_barang,
        nsd.paket_trans_id,nsd.paket_details_id
        FROM nscc_barang nb
	    INNER JOIN nscc_salestrans_details nsd
	    ON nb.barang_id = nsd.barang_id
        WHERE nsd.salestrans_id = :salestrans_id");
        return $comm->queryAll(true, array(':salestrans_id' => $salestrans_id));
    }
}