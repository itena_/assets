<?php
Yii::import('application.models._base.BaseResepPrintBahan');
class ResepPrintBahan extends BaseResepPrintBahan
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function beforeValidate()
    {
        if ($this->resep_print_bahan_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->resep_print_bahan_id = $uuid;
        }
        return parent::beforeValidate();
    }
}