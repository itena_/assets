<?php
Yii::import('application.models._base.BaseUserView');

class UserView extends BaseUserView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->user_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->user_id = $uuid;
        }
        return parent::beforeValidate();
    }
}