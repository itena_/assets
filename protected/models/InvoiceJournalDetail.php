<?php

Yii::import('application.models._base.BaseInvoiceJournalDetail');

class InvoiceJournalDetail extends BaseInvoiceJournalDetail {

    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function beforeValidate() {
        if ($this->invoice_journal_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->invoice_journal_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }

}
