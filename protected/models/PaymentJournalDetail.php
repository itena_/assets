<?php
Yii::import('application.models._base.BasePaymentJournalDetail');

class PaymentJournalDetail extends BasePaymentJournalDetail
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->payment_journal_detail_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->payment_journal_detail_id = $uuid;
        }
        return parent::beforeValidate();
    }
}