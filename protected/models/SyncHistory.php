<?php
Yii::import('application.models._base.BaseSyncHistory');

class SyncHistory extends BaseSyncHistory
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->history_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->history_id = $uuid;
        }
        return parent::beforeValidate();
    }
}