<?php
Yii::import('application.models._base.BaseReferral');

class Referral extends BaseReferral
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->referral_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->referral_id = $uuid;
        }
        return parent::beforeValidate();
    }
}