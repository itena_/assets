<?php

Yii::import('application.models._base.BasePrintzDetails');

class PrintzDetails extends BasePrintzDetails
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate()
    {
        if ($this->printz_details_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->printz_details_id = $uuid;
        }
        return parent::beforeValidate();
    }
}