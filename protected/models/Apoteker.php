<?php
Yii::import('application.models._base.BaseApoteker');
class Apoteker extends BaseApoteker
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function primaryKey()
    {
        return 'apoteker_id';
    }
}