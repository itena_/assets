<?php

/**
 * This is the model base class for the table "{{beauty}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Beauty".
 *
 * Columns in table "{{beauty}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $beauty_id
 * @property string $nama_beauty
 * @property string $gol_id
 * @property string $kode_beauty
 * @property integer $active
 * @property string $store
 * @property integer $up
 * @property string $tipe
 *
 */
abstract class BaseBeauty extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{beauty}}';
	}

	public static function representingColumn() {
        return 'beauty_id';
	}

	public function rules() {
		return array(
			array('beauty_id, nama_beauty, gol_id, kode_beauty, store', 'required'),
			array('active, up', 'numerical', 'integerOnly'=>true),
			array('beauty_id', 'length', 'max'=>50),
			array('nama_beauty', 'length', 'max'=>100),
            array('gol_id, tipe', 'length', 'max' => 36),
			array('kode_beauty, store', 'length', 'max'=>20),
            array('active, up, tipe', 'default', 'setOnEmpty' => true, 'value' => null),
            array('beauty_id, nama_beauty, gol_id, kode_beauty, active, store, up, tipe', 'safe', 'on' => 'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'beauty_id' => Yii::t('app', 'Beauty'),
			'nama_beauty' => Yii::t('app', 'Nama Beauty'),
			'gol_id' => Yii::t('app', 'Gol'),
			'kode_beauty' => Yii::t('app', 'Kode Beauty'),
			'active' => Yii::t('app', 'Active'),
			'store' => Yii::t('app', 'Store'),
			'up' => Yii::t('app', 'Up'),
            'tipe' => Yii::t('app', 'Tipe'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('beauty_id', $this->beauty_id, true);
		$criteria->compare('nama_beauty', $this->nama_beauty, true);
        $criteria->compare('gol_id', $this->gol_id, true);
		$criteria->compare('kode_beauty', $this->kode_beauty, true);
		$criteria->compare('active', $this->active);
		$criteria->compare('store', $this->store, true);
		$criteria->compare('up', $this->up);
        $criteria->compare('tipe', $this->tipe, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}