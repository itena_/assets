<?php
/**
 * This is the model base class for the table "{{laha_import_details}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "LahaImportDetails".
 *
 * Columns in table "{{laha_import_details}}" available as properties of the model,
 * followed by relations of table "{{laha_import_details}}" available as properties of the model.
 *
 * @property string $laha_import_detail_id
 * @property string $amount
 * @property string $laha_import_id
 * @property string $account_code
 *
 * @property LahaImport $lahaImport
 */
abstract class BaseLahaImportDetails extends GxActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    public function tableName()
    {
        return '{{laha_import_details}}';
    }
    public static function representingColumn()
    {
        return 'amount';
    }
    public function rules()
    {
        return array(
            array('laha_import_detail_id, laha_import_id', 'required'),
            array('laha_import_detail_id, laha_import_id', 'length', 'max' => 36),
            array('amount', 'length', 'max' => 30),
            array('account_code', 'length', 'max' => 15),
            array('amount, account_code', 'default', 'setOnEmpty' => true, 'value' => null),
            array('laha_import_detail_id, amount, laha_import_id, account_code', 'safe', 'on' => 'search'),
        );
    }
    public function relations()
    {
        return array(
            'lahaImport' => array(self::BELONGS_TO, 'LahaImport', 'laha_import_id'),
        );
    }
    public function pivotModels()
    {
        return array();
    }
    public function attributeLabels()
    {
        return array(
            'laha_import_detail_id' => Yii::t('app', 'Laha Import Detail'),
            'amount' => Yii::t('app', 'Amount'),
            'laha_import_id' => Yii::t('app', 'Laha Import'),
            'account_code' => Yii::t('app', 'Account Code'),
        );
    }
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('laha_import_detail_id', $this->laha_import_detail_id, true);
        $criteria->compare('amount', $this->amount, true);
        $criteria->compare('laha_import_id', $this->laha_import_id);
        $criteria->compare('account_code', $this->account_code, true);
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
        ));
    }
}