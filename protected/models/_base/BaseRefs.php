<?php

/**
 * This is the model base class for the table "{{refs}}".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Refs".
 *
 * Columns in table "{{refs}}" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $refs_id
 * @property string $type_no
 * @property integer $type_
 * @property string $reference
 * @property integer $up
 *
 */
abstract class BaseRefs extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '{{refs}}';
	}

	public static function representingColumn() {
		return 'type_no';
	}

	public function rules() {
		return array(
			array('refs_id, type_no', 'required'),
			array('type_, up', 'numerical', 'integerOnly'=>true),
			array('refs_id', 'length', 'max'=>36),
			array('type_no, reference', 'length', 'max'=>50),
			array('type_, reference, up', 'default', 'setOnEmpty' => true, 'value' => null),
			array('refs_id, type_no, type_, reference, up', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'refs_id' => Yii::t('app', 'Refs'),
			'type_no' => Yii::t('app', 'Type No'),
			'type_' => Yii::t('app', 'Type'),
			'reference' => Yii::t('app', 'Reference'),
			'up' => Yii::t('app', 'Up'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('refs_id', $this->refs_id, true);
		$criteria->compare('type_no', $this->type_no, true);
		$criteria->compare('type_', $this->type_);
		$criteria->compare('reference', $this->reference, true);
		$criteria->compare('up', $this->up);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}