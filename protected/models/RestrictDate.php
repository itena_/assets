<?php
Yii::import('application.models._base.BaseRestrictDate');

class RestrictDate extends BaseRestrictDate
{

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->res_date_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->res_date_id = $uuid;
        }
        $this->user_name = $this->userid->user_id;
        $this->updated = date('Y-m-d H:i:s');
        $this->updater = Yii::app()->user->id;
        if($this->isNewRecord){
            $this->created = date('Y-m-d H:i:s');
            $this->author = Yii::app()->user->id;
            $this->up = '0';
        } 
        return parent::beforeValidate();
    }

    /* get data restiction date input from preferences
    type :
    1= start
    2= end
    */
    public static function getPersonalRestDate($type)
    {
        $model = self::model()->findByAttributes(array('user_id' => Yii::app()->user->id));
        if ($model != null) {
            if($type == '1'){
                return $model->start_date;
            }elseif($type == '2'){
                return $model->end_date;
            }else{
                return false;
            }
        } else {
            return false;
        }
    }
}