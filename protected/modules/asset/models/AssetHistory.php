<?php
Yii::import('application.modules.asset.models._base.BaseAssetHistory');

class AssetHistory extends BaseAssetHistory
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->asset_history_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->asset_history_id = $uuid;
        }
        return parent::beforeValidate();
    }
}