<?php
Yii::import('application.modules.asset.models._base.BaseAssetBarang');

class AssetBarang extends BaseAssetBarang
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->barang_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->barang_id = $uuid;
        }
        return parent::beforeValidate();
    }
}