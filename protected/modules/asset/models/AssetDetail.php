<?php
Yii::import('application.modules.asset.models._base.BaseAssetDetail');

class AssetDetail extends BaseAssetDetail
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->asset_trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->asset_trans_id = $uuid;
        }
        return parent::beforeValidate();
    }

    static function get_details_to_print($asset_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                *
            FROM nscc_asset_detail ad

            WHERE 
                asset_id = :asset_id
                ORDER BY ati ASC
        ");
        return $comm->queryAll(true, array(':asset_id' => $asset_id));
    }

    static function get_details_to_printfaktur($asset_id)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT 
                *
            FROM nscc_asset_detail ad

            WHERE 
                asset_trans_id = :asset_id
                ORDER BY ati ASC
        ");
        return $comm->queryAll(true, array(':asset_id' => $asset_id));
    }
}