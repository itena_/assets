<?php
Yii::import('application.modules.asset.models._base.BaseStore');

class Store extends BaseStore
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->store_kode == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->store_kode = $uuid;
        }
        return parent::beforeValidate();
    }
}