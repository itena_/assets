<?php
Yii::import('application.modules.asset.models._base.BaseStoreArea');

class StoreArea extends BaseStoreArea
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->store_area_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->store_area_id = $uuid;
        }
        return parent::beforeValidate();
    }
}