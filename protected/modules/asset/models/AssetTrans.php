<?php
Yii::import('application.modules.asset.models._base.BaseAssetTrans');

class AssetTrans extends BaseAssetTrans
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->trans_id = $uuid;
        }
        return parent::beforeValidate();
    }
}