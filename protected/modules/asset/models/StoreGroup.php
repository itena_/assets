<?php
Yii::import('application.modules.asset.models._base.BaseStoreGroup');

class StoreGroup extends BaseStoreGroup
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->nscc_store_group_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->nscc_store_group_id = $uuid;
        }
        return parent::beforeValidate();
    }
}