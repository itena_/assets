<?php
Yii::import('application.modules.asset.models._base.BaseAssetGroup');

class AssetGroup extends BaseAssetGroup
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->asset_group_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->asset_group_id = $uuid;
        }
        return parent::beforeValidate();
    }
}