<?php
Yii::import('application.modules.asset.models._base.BaseRentTrans');

class RentTrans extends BaseRentTrans
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->rent_trans_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->rent_trans_id = $uuid;
        }
        return parent::beforeValidate();
    }
}