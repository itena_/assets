<?php

class AssetPeriodeController extends GxController {

    public function actionIndex() {
        $param = array();
        $criteria = new CDbCriteria();
        if(isset($_POST['asset_trans_id'])){
            $criteria->addCondition('asset_trans_id = :asset_trans_id');
            $param[':asset_trans_id'] = $_POST['asset_trans_id'];
        }
        $criteria->params = $param;

        $model = AssetPeriodeView::model()->findAll($criteria);
        $total = AssetPeriodeView::model()->count($criteria);
        //$model = AssetPeriode::model()->findAll($criteria);
        //$total = AssetPeriode::model()->count($criteria);

        $this->renderJson($model, $total);
    }


public function actionCreate() {
$model = new AssetPeriode;
if (!Yii::app()->request->isAjaxRequest)
return;
if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['AssetPeriode'][$k] = $v;
}
$model->attributes = $_POST['AssetPeriode'];
$msg = "Data gagal disimpan.";

    if ($model->save()) {
$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->asset_periode_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg));
Yii::app()->end();

}

}

public function actionUpdate($id) {
$model = $this->loadModel($id, 'AssetPeriode');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['AssetPeriode'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['AssetPeriode'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->asset_periode_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->asset_periode_id));
}
}
}

public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'AssetPeriode')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}


/*public function actionIndex() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}

$criteria = new CDbCriteria();
if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
(isset($_POST['limit']) && isset($_POST['start']))) {
$criteria->limit = $limit;
$criteria->offset = $start;
}
$model = AssetPeriode::model()->findAll($criteria);
$total = AssetPeriode::model()->count($criteria);

$this->renderJson($model, $total);

}*/

public function actionShow($docref) {
        if(isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if(isset($_POST['start'])){
            $start = $_POST['start'];

        } else {
            $start = 0;
        }

        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        //if (isset($_POST['doc_ref'])) {
            //$criteria->addCondition('docref LIKE :docref');
            //$criteria->compare('docref','BDG01/V/1/12/17/2');
        $criteria->compare('docref',$docref);

            //$criteria->params[':docref'] = '%' . JKT01/II/1/12/17/1 . '%';
        //}
        $criteria->order = "tglpenyusutan ASC";


    $model = AssetPeriode::model()->findAll($criteria);
    $total = AssetPeriode::model()->count($criteria);

        $this->renderJson($model, $total);

    }


}