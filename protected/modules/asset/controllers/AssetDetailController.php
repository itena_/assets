<?php

class AssetDetailController extends GxController {

    private $TBS;

    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
        return;
        if (isset($_POST) && !empty($_POST))
        {
            $is_new = $_POST['mode'] == 0;
            $as_id = $_POST['id'];

            $user_id = Yii::app()->user->getId();
            $businessunit_id = $_COOKIE['businessunitid'];
            $businessunit = $this->loadModel($businessunit_id, "Businessunit");

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();

            try
            {
                $model = $is_new ? new AssetDetail : $this->loadModel($as_id, "AssetDetail");

                foreach($_POST as $k=>$v){
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['AssetDetail'][$k] = $v;
                }


                //$branch = $_POST['AssetDetail']['asset_trans_branch'];
                //$grupid = $_POST['AssetDetail']['class'];
                $cat = $_POST['AssetDetail']['category_id'];
                $desc = $_POST['AssetDetail']['description'];
                $active = $_POST['AssetDetail']['activeasset'];
                $newprice = $_POST['AssetDetail']['asset_trans_price'];
                $ppn = $_POST['AssetDetail']['ppnassetedit'];
                //$last = $this->getLastRow($branch) + 1;

                /*docref = $model->docref;
                $cabang = $businessunit->businessunit_code.$branch;
                $store = Store::model()->findByAttributes(array('store_kode' => $branch, 'businessunit_id' => $businessunit_id));
                $area = str_pad($store->wilayah_id, 2, '0', STR_PAD_LEFT);
                $urutan = str_pad($store->id_cabang, 2, '0', STR_PAD_LEFT);

                $group = $this->getGolongan($grupid);
                $golongan = $group[0]; //$this->getGolongan($grupid);
                $tariff = $group[1];
                $period = $group[2];
                $desc = $group[3];

                $ati = $area.$cabang.$urutan.'/'.$golongan;*/
                $model->category_id = $cat;
                $model->description = $desc;
                $model->active = $active;
                $model->asset_trans_price = $newprice;
                $total = $ppn + $newprice;

                if($model->period)
                {
                    $penyusutan = $newprice / $model->period;
                    $penyusutantahun = $penyusutan * $model->period;
                }

                $model->penyusutanperbulan = $penyusutan;
                $model->penyusutanpertahun = $penyusutantahun;
                $model->updated_at = new CDbExpression('NOW()');

                //$model->attributes = $_POST['AssetDetail'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetDetail')) . CHtml::errorSummary($model));
                }

                //rubah deprisiasi
                $this->rubahDeprisiasi($model->asset_trans_id, 0);

                //ASSET HISTORY
                $history = new History();
                $history->add_history_status($businessunit_id, $model->asset_id, $model->asset_trans_id, $model->docref,
                    $model->ati,$model->asset_trans_name,$model->asset_trans_branch,
                    $model->asset_trans_price, $model->asset_trans_new_price,
                    $model->class, $model->tariff,$model->period,
                    $model->penyusutanperbulan,$model->penyusutanpertahun,
                    $model->description. " [EDITED]", $model->status);

                //NON ACTIVE GL
                if($active == '0')
                {
                    $gl = new GL();
                    $gl->setGLNonActive($model->asset_trans_id);
                }else{
                    $gl = new GL();
                    $gl->editGL($model->asset_trans_id, $model->asset_trans_price, $ppn, $model->asset_trans_price);
                }

                $transaction->commit();
                $status = true;
                $msg = "Data berhasil di edit.";
                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg));
                Yii::app()->end();
            }
            catch (Exception $ex)
            {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
                echo CJSON::encode(array(
                    'success' => 'failed',
                    'msg' => $msg
                ));
            }


        }

    }

    public function getGolongan($id)
    {
        $query = "select golongan as gol, tariff, period, nscc_asset_group.`desc` from nscc_asset_group
                     where asset_group_id = '".$id."' limit 1";

        $list= Yii::app()->db->createCommand($query)->queryAll();

        $rs=array();
        foreach($list as $item)
        {
            $rs[0]=$item['gol'];
            $rs[1]=$item['tariff'];
            $rs[2]=$item['period'];
            $rs[3]=$item['desc'];
        }
        return $rs;
    }

    public function actionStatus()
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Asset Status berhasil di ubah.';
            $status = true;
            try {

                $asset_trans_id = $_POST['asset_trans_id'];

                $amountstatus = get_number($_POST['amountstatus']);
                $ppnstatus = get_number($_POST['ppnstatus']);
                $statusasset = $_POST['statusasset'];
                $assetvalue = get_number($_POST['assetvalue']);

                $user_id = Yii::app()->user->getId();

                $ket = $_POST['ket'];
                $model = $this->loadModel($asset_trans_id, 'AssetDetail');


                $sellbranch = $_POST['branch'];
                if($sellbranch =="")
                {
                    $sellbranch = $model->asset_trans_branch;
                }

                $tobusinessunit_id = $_POST['businessunit_id'];
                if($tobusinessunit_id == "")
                {
                    $businessunit_id = $_COOKIE['businessunitid'];
                    $tobusinessunit_id =$businessunit_id;
                }

                $businessunit = $this->loadModel($tobusinessunit_id, 'Businessunit');
                $tobu = $businessunit->businessunit_code;

                if($tobu == 'OTHER')
                {
                    $tobranch = '';
                }
                else{
                    $tobranch = $sellbranch;
                }


                if($statusasset == '3')
                {
                    if($tobusinessunit_id != '')
                    {
                        $penyusutan = 0;
                        $penyusutantahun = 0;

                        //ati dan cabang lama sebelum aktiva berubah
                        $atiii = $model->ati;
                        $cablama = $model->asset_trans_branch;
                        $buidlama = $model->businessunit_id;
                        $transdatelama = $model->asset_trans_date;

                        $model->businessunit_id = $tobusinessunit_id;
                        $model->status = '1';
                        $model->asset_trans_price = $amountstatus;
                        $model->asset_trans_branch = $sellbranch;
                        $model->asset_trans_date = new CDbExpression('NOW()');

                        if($model->period)
                        {
                            $penyusutan = $model->asset_trans_price / $model->period;
                            $penyusutantahun = $penyusutan * $model->period;
                        }

                        $model->penyusutanperbulan = $penyusutan;
                        $model->penyusutanpertahun = $penyusutantahun;

                        //rubah activa
                        $cabang = $businessunit->businessunit_code.$sellbranch;
                        $store = Store::model()->findByAttributes(array('store_kode' => $sellbranch,'businessunit_id'=> $tobusinessunit_id));
                        $area = str_pad($store->wilayah_id, 2, '0', STR_PAD_LEFT);
                        $urutan = str_pad($store->id_cabang, 2, '0', STR_PAD_LEFT);
                        $golongan = $model->class;

                        $lastDetail = $this->getLastRowDetail($sellbranch) + 1;

                        $ati = $area.$cabang.$urutan.'/'.$golongan.'/'.$lastDetail;

                        $model->ati = $ati;
                    }
                    else
                    return;

                }
                else{
                    $model->status = $statusasset;
                }


                //if($ket != '')
                //{
                    //$model->description = $ket;
                //}

                if($ket != "")
                {
                    $model->statusdesc =  $ket;//$model->description;
                }
                else{
                    if($statusasset == '3')
                    {
                        $model->statusdesc =  "Di beli dari ".$cablama;
                    }
                    elseif($statusasset == '4')
                    {
                        $model->statusdesc =  "Di sewa oleh ".$tobu." - ".$tobranch;
                    }
                }

                //$model->statusdesc = $ket;
                $model->updated_at = new CDbExpression('NOW()');
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetDetail')) . CHtml::errorSummary($model));




                if($statusasset == '3')
                {


                    $total = $ppnstatus + $model->asset_trans_price;
                    //gl penjual
                    $gl = new GL();
                    $gl->add_gl_trans(ASSETJUAL, $model->asset_trans_id, $model->asset_id,
                        $buidlama, $transdatelama, $atiii,
                        "$atiii", -$model->asset_trans_price, $ppnstatus, $total, $assetvalue,
                        $user_id, 0,'jual', $cablama);

                    //reset deprisiasi
                    $this->rubahDeprisiasi($model->asset_trans_id, 0);

                    //gl pembeli
                    $gl = new GL();
                    $gl->add_gl_trans(ASSETBELI, $model->asset_trans_id, $model->asset_id,
                        $model->businessunit_id, $model->asset_trans_date, $model->ati,
                        "$model->ati", $model->asset_trans_price, $ppnstatus, $total, $model->asset_trans_price,
                        $user_id, 1,'beli', $model->asset_trans_branch);


                    //$this->PrintFakturJualAsset($model->asset_trans_id);

                }

                if($statusasset == '4')
                {
                    //$type, $trans_id, $bu, $date_, $memo_, $price, $depprice, $amount,  $person_id, $desc, $store, $tostore, $tobu
                    $gl = new GL();
                    $gl->add_rent_trans(ASSETSEWA, $model->asset_trans_id,
                        $model->businessunit_id, $model->asset_trans_date, $model->ati,
                        $model->asset_trans_name, $model->category_id,
                        $model->asset_trans_price, $assetvalue, $amountstatus, $user_id, $ket, $model->asset_trans_branch,$tobranch ? $tobranch : 'OTHER', $tobu);
                }

                //ASSET HISTORY
                $history = new AssetHistory;
                $history->businessunit_id = $model->businessunit_id;
                $history->masterassetid = $model->asset_id;
                $history->asset_id = $model->asset_trans_id;
                $history->docref = $model->docref;
                $history->ati = $model->ati;
                $history->name = $model->asset_trans_name;
                $history->branch = $model->asset_trans_branch;
                $history->price = $model->asset_trans_price;
                $history->newprice = $model->asset_trans_new_price;
                $history->class = $model->class;
                $history->tariff = $model->tariff;
                $history->period = $model->period;
                $history->penyusutanperbulan = $model->penyusutanperbulan;
                $history->penyusutanpertahun = $model->penyusutanpertahun;

                if($ket != "")
                {
                    $history->desc =  $ket;//$model->description;
                }
                else{
                    if($statusasset == '3')
                    {
                        $history->desc =  "Di beli dari ".$cablama;
                    }
                    elseif($statusasset == '4')
                    {
                        $history->desc =  "Di sewa oleh ".$tobu." - ".$tobranch;
                    }
                }

                $history->status =  $model->status;
                $history->tobu = $tobu;
                $history->tobranch = $tobranch ? $tobranch : 'OTHER';
                $history->amount = $amountstatus;
                $history->sdate = new CDbExpression('NOW()');
                $history->amount = $amountstatus ? $amountstatus : 0;
                $history->created_at = new CDbExpression('NOW()');
                $history->updated_at = new CDbExpression('NOW()');

                if (!$history->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetHistory')) . CHtml::errorSummary($history));
                }
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function rubahDeprisiasi($assedetailid, $new)
    {
        $lastdeprisiasi = 0;
        $lastbalance = 0;
        $lastaccumulated = 0;

        $modeldetail = $this->loadModel($assedetailid, "AssetDetail");
        //$modelperiode = $new ? new AssetPeriode : $this->loadModel($assedetailid, "AssetPeriode");
        AssetPeriode::model()->deleteAll("asset_trans_id = :asset_trans_id", array(':asset_trans_id' => $modeldetail->asset_trans_id));
        //$modelperiode = AssetPeriode::model()->findByAttributes(array('asset_trans_id' => $assedetailid));
        //$modelperiode = new AssetPeriode;

        for($j=0;$j<$modeldetail->period;$j++)
        {
            $modelperiode = new AssetPeriode;
            $modelperiode->docref = $modeldetail->docref;
            $modelperiode->docref_other = $modeldetail->docref_other;
            $modelperiode->ati = $modeldetail->ati;
            $modelperiode->businessunit_id = $modeldetail->businessunit_id;
            $modelperiode->asset_trans_id = $modeldetail->asset_trans_id;
            $modelperiode->asset_id = $modeldetail->asset_id;
            $modelperiode->asset_group_id = $modeldetail->asset_group_id;
            $modelperiode->barang_id = $modeldetail->barang_id;
            $modelperiode->asset_trans_date = $modeldetail->asset_trans_date;
            $modelperiode->asset_trans_price = $modeldetail->asset_trans_price;
            $modelperiode->asset_trans_new_price = $modeldetail->asset_trans_new_price;
            $modelperiode->asset_trans_name = $modeldetail->asset_trans_name;
            $modelperiode->asset_trans_branch = $modeldetail->asset_trans_branch;
            $modelperiode->description = $modeldetail->description;

            $modelperiode->period = $modeldetail->period;
            $modelperiode->class = $modeldetail->class;
            $modelperiode->tariff = $modeldetail->tariff;

            //$penyusutan = $modelperiode->asset_trans_price * ($modelperiode->tariff/100) / $modelperiode->period;
            $penyusutan = $modelperiode->asset_trans_price / $modelperiode->period;

            $modelperiode->penyusutanperbulan = $penyusutan;
            $lastdeprisiasi = $modelperiode->penyusutanperbulan;

            $penyusutantahun = $penyusutan * $modelperiode->period;
            $modelperiode->penyusutanpertahun = $penyusutantahun;

            if($lastbalance == 0  || $lastdeprisiasi ==0)
            {
                $modelperiode->balance = $modelperiode->asset_trans_price - $penyusutan;
                $lastbalance = $modelperiode->balance;

                $modelperiode->akumulasipenyusutan = $penyusutan;
                $lastaccumulated = $modelperiode->akumulasipenyusutan;
            }
            else
            {
                $modelperiode->balance = $lastbalance - $lastdeprisiasi;
                $lastbalance = $modelperiode->balance;

                $modelperiode->akumulasipenyusutan = $lastaccumulated + $penyusutan;
                $lastaccumulated = $modelperiode->akumulasipenyusutan;
            }

            //counter bulan
            $x = $j;
            $d = strtotime("$x months",strtotime($modeldetail->asset_trans_date));
            $newdate = date('Y-m-t',$d);
            /////////////////////////////////////////////

            $modelperiode->tglpenyusutan = $newdate;

            $modelperiode->startdate = $modelperiode->asset_trans_date;

            $tmp = $modelperiode->period - 1;
            $modelperiode->enddate = $newdate = date('Y-m-t',strtotime("+$tmp months",strtotime($modeldetail->asset_trans_date)));

            //$modelperiode->enddate = $newdate = date('Y-m-t',strtotime("+$modelperiode->period months",strtotime($modeldetail->asset_trans_date)));

            $modelperiode->status = $modeldetail->status;

            $modelperiode->created_at = $modeldetail->created_at;
            $modelperiode->updated_at = $modeldetail->updated_at;

            if (!$modelperiode->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'modelperiode')) . CHtml::errorSummary($modelperiode));
            }


        }
    }

    public function getLastRowDetail($store)
    {
        $businessunit_id = $_COOKIE['businessunitid'];
        $criteria = new CDbCriteria;
        $criteria->compare('active','1');
        $criteria->compare('asset_trans_branch',$store);
        $criteria->compare('businessunit_id',$businessunit_id);
        $model = AssetDetail::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

    public function actionShowHide($id)
    {
        $model = $this->loadModel($id, 'AssetDetail');


        if (isset($_POST) && !empty($_POST)) {
            foreach($_POST as $k=>$v){
                if (is_angka($v)) $v = get_number($v);
                $_POST['AssetDetail'][$k] = $v;
            }
            $msg = "Data gagal disimpan";

            $hide =$_POST['hide'];

            $model->hide = $hide;
            $model->attributes = $_POST['AssetDetail'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di ubah.";
            } else {
                $msg .= " ".implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest)
            {
                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg
                ));
                Yii::app()->end();
            } else
            {
                $this->redirect(array('view', 'id' => $model->asset_trans_id));
            }
        }
    }

    public function actionGetBalance($id)
    {
        $year = date('Y');
        $month = date('m');
        $businessunit_id = $_COOKIE['businessunitid'];

        $query = "select balance, akumulasipenyusutan from nscc_asset_periode
                     where asset_trans_id = '".$id."'
                     and businessunit_id = '".$businessunit_id."'
                     and MONTH(tglpenyusutan) = '".$month."'
                     and YEAR(tglpenyusutan) = '".$year."'";

        $list= Yii::app()->db->createCommand($query)->queryAll();

        $rs = array();//=array();
        foreach($list as $item)
        {
            $rs[0]=$item['balance'];
            $rs[1]=$item['akumulasipenyusutan'];
        }

        if (Yii::app()->request->isAjaxRequest) {
            echo CJSON::encode(array(
                'success' => $rs,
                'msg' => $rs
            ));
            Yii::app()->end();
        } else {
            //$this->redirect(array('view', 'id' => $Periode->asset_trans_id));
        }
        //return $rs;
    }

    public function ssactionGetBalance($id)
    {
        //$model = $this->loadModel($id, 'AssetPer');
        $now = date('Y-m-t');
        $Periode = AssetPeriode::model()->findByAttributes(array('asset_trans_id' => $id, 'tglpenyusutan'=>$now));

        //if (isset($_POST) && !empty($_POST)) {
/*            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['AssetPeriode'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $Periode->attributes = $_POST['AssetPeriode'];*/

            /*if ($Periode->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $Periode->asset_trans_id;
            } else {
                $msg .= " " . implode(", ", $Periode->getErrors());
                $status = false;
            }*/

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => '111',
                    'msg' => '111'
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $Periode->asset_trans_id));
            }
       // }
    }


    public function PrintFakturJualAsset($id)
    {

            $ti = AssetDetail::model()->find('asset_trans_id = :asset_id', array(':asset_id' => $id));
            $tid = AssetDetail::get_details_to_printfaktur($id);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'fakturjualasset.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $header_data = array(
                'title' => 'ASSETS',
                'doc_ref' => $ti->docref_other,
                'tgl' => date_format(date_create($ti->asset_trans_date), 'd F Y'),
                'branch' => $ti->asset_trans_branch,
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('item', $tid);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "ASSET $ti->docref_other $ti->asset_trans_branch.xls");
            } else {
                $this->TBS->Show();
            }
        //}
    }


    public function actionUpdate($id) {
$model = $this->loadModel($id, 'AssetDetail');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['AssetDetail'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['AssetDetail'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->asset_trans_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->asset_trans_id));
}
}
}

    public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'AssetDetail')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}

    public function actionIndex() {
        if(isset($_POST['limit'])) {
        $limit = $_POST['limit'];
        } else {
        $limit = 20;
        }

        if(isset($_POST['start'])){
        $start = $_POST['start'];

        } else {
        $start = 0;
        }
        $businessunit_id = $_COOKIE['businessunitid'];
        $param = array();
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
        (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if (isset($_POST['asset_trans_date'])) {
            $asset_trans_date = $_POST['asset_trans_date'];
            $criteria->addCondition('asset_trans_date = date(:asset_trans_date)');
            $param[':asset_trans_date'] = "$asset_trans_date";
        }
        if (isset($_POST['docref_other'])) {
            $docref_other = $_POST['docref_other'];
            $criteria->addCondition('docref_other like :docref_other');
            $param[':docref_other'] = "%$docref_other%";
        }
        if (isset($_POST['ati'])) {
            $ati = $_POST['ati'];
            $criteria->addCondition('ati like :ati');
            $param[':ati'] = "%$ati%";
        }
        if (isset($_POST['ati_old'])) {
            $ati_old = $_POST['ati_old'];
            $criteria->addCondition('ati_old like :ati_old');
            $param[':ati_old'] = "%$ati_old%";
        }
        if (isset($_POST['asset_trans_branch'])) {
            $asset_trans_branch = $_POST['asset_trans_branch'];
            $criteria->addCondition('asset_trans_branch like :asset_trans_branch');
            $param[':asset_trans_branch'] = "%$asset_trans_branch%";
        }
        if (isset($_POST['asset_trans_name'])) {
            $asset_trans_name = $_POST['asset_trans_name'];
            $criteria->addCondition('asset_trans_name like :asset_trans_name');
            $param[':asset_trans_name'] = "%$asset_trans_name%";
        }
        if (isset($_POST['asset_trans_price'])) {
            $asset_trans_price = $_POST['asset_trans_price'];
            $criteria->addCondition('asset_trans_price like :asset_trans_price');
            $param[':asset_trans_price'] = "%$asset_trans_price%";
        }
        if (isset($_POST['description'])) {
            $description = $_POST['description'];
            $criteria->addCondition('description like :description');
            $param[':description'] = "%$description%";
        }
        if (isset($_POST['category'])) {
            $category = $_POST['category'];
            $criteria->addCondition('category like :category');
            $param[':category'] = "%$category%";
        }
        if (isset($_POST['period'])) {
            $period = $_POST['period'];
            $criteria->addCondition('period like :period');
            $param[':period'] = "%$period%";
        }
        if (isset($_POST['tariff'])) {
            $tariff = $_POST['tariff'];
            $criteria->addCondition('tariff like :tariff');
            $param[':tariff'] = "%$tariff%";
        }
        if (isset($_POST['class'])) {
            $class = $_POST['class'];
            $criteria->addCondition('class like :class');
            $param[':class'] = "%$class%";
        }
        if (isset($_POST['hide'])&& $_POST['hide'] != 'all') {
            $hide = $_POST['hide'];
            $criteria->addCondition('hide = :hide');
            $param[':hide'] = "$hide";
        }
        if (isset($_POST['status'])&& $_POST['status'] != 'all') {
            $status = $_POST['status'];
            $criteria->addCondition('status = :status');
            $param[':status'] = "$status";
        }
        if (isset($_POST['kode_barang'])) {
            $kode_barang = $_POST['kode_barang'];
            $criteria->addCondition('kode_barang like :kode_barang');
            $param[':kode_barang'] = "%$kode_barang%";
        }

        //untuk menampilkan asset di cabang
        if (isset($_POST['hide_value'])) {
            $hide_value = $_POST['hide_value'];
            $criteria->addCondition('hide = :hide');
            $param[':hide'] = $hide_value;

            $user_id = Yii::app()->user->getId();
            $Users = Users::model()->findByPk($user_id);

            if($Users->security_roles_id == 4)
            {
                $criteria->addCondition('asset_trans_branch = :asset_trans_branch');
                $param[':asset_trans_branch'] = $Users->store;
            }
        }

        $criteria->addCondition('active = 1');
        $criteria->addCondition('businessunit_id = :businessunit_id');
        $param[':businessunit_id'] = $businessunit_id;

        $criteria->order = 'asset_trans_name, docref';
        $criteria->params = $param;
        $model = AssetDetailView::model()->findAll($criteria);
        $total = AssetDetailView::model()->count($criteria);

        $this->renderJson($model, $total);

    }
}