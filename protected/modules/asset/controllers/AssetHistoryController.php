<?php

class AssetHistoryController extends GxController {

public function actionCreate() {
$model = new AssetHistory;
if (!Yii::app()->request->isAjaxRequest)
return;
if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['AssetHistory'][$k] = $v;
}
$model->attributes = $_POST['AssetHistory'];
$msg = "Data gagal disimpan.";

    if ($model->save()) {
$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->asset_history_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg));
Yii::app()->end();

}

}

public function actionUpdate($id) {
$model = $this->loadModel($id, 'AssetHistory');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['AssetHistory'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['AssetHistory'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->asset_history_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->asset_history_id));
}
}
}

public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'AssetHistory')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}


public function actionIndex() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}

$criteria = new CDbCriteria();
if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
(isset($_POST['limit']) && isset($_POST['start']))) {
$criteria->limit = $limit;
$criteria->offset = $start;
}


    if (isset($_POST['asset_trans_id'])) {
        $asset_trans_id = $_POST['asset_trans_id'];
        $criteria->addCondition('asset_id = :asset_trans_id');
        $param[':asset_trans_id'] = "$asset_trans_id";
    }
    if (isset($_POST['status'])&& $_POST['status'] != 'all') {
        $status = $_POST['status'];
        $criteria->addCondition('status = :status');
        $param[':status'] = "$status";
    }

    /*$criteria->addCondition('businessunit_id = :businessunit_id');
    $param[':businessunit_id'] = $businessunit_id;*/

    $criteria->order = 'created_at desc';
    $criteria->params = $param;

$model = AssetHistory::model()->findAll($criteria);
$total = AssetHistory::model()->count($criteria);

$this->renderJson($model, $total);

}

}