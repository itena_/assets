<?php

class AssetBarangController extends GxController {

        public function actionCreate()
        {
            $model = new AssetBarang;
            if (!Yii::app()->request->isAjaxRequest)
            return;
            if (isset($_POST) && !empty($_POST))
            {
                foreach($_POST as $k=>$v)
                {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['AssetBarang'][$k] = $v;
                }

                //check kode barang
                $barang = AssetBarang::model()->findByAttributes([
                    'kode_barang' => $_POST['AssetBarang']['kode_barang'],
                ]);

                if($barang)
                {
                    $status = false;
                    $msg = "Kode Barang sudah ada " . $model->barang_id;
                    echo CJSON::encode(array(
                        'success'=>$status,
                        'msg'=>$msg));
                    Yii::app()->end();
                    return;
                }

                $businessunit_id = $_COOKIE['businessunitid'];
                $model->businessunit_id = $businessunit_id;
                $model->attributes = $_POST['AssetBarang'];
                $msg = "Data gagal disimpan.";

                if ($model->save()) {
                    $status = true;
                    $msg = "Data berhasil di simpan dengan id " . $model->barang_id;
                } else {
                    $msg .= " ".implode(", ", $model->getErrors());
                    $status = false;
                }

                echo CJSON::encode(array(
                'success'=>$status,
                'msg'=>$msg));
                Yii::app()->end();
            }
        }

public function actionUpdate($id) {
$model = $this->loadModel($id, 'AssetBarang');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['AssetBarang'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['AssetBarang'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->barang_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->barang_id));
}
}
}

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest)
        {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                //$this->loadModel($id, 'AssetBarang')->delete();
                $model = $this->loadModel($id, 'AssetBarang');
                $model->active= 0;
                if ($model->save()) {

                    $status = true;
                    $msg = "Data berhasil di hapus.";
                } else {
                    $msg .= " ".implode(", ", $model->getErrors());
                    $status = false;
                }
            }
            catch (Exception $ex)
            {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
            Yii::app()->end();
        }
        else
            throw new CHttpException(400,
            Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }


public function actionIndex() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}

$criteria = new CDbCriteria();
if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
(isset($_POST['limit']) && isset($_POST['start']))) {
$criteria->limit = $limit;
$criteria->offset = $start;
}

    //$businessunit_id = $_COOKIE['businessunitid'];
    //$criteria->addCondition('businessunit_id = :businessunit_id');
    //$param[':businessunit_id'] = $businessunit_id;

    //$criteria->params = $param;

    $criteria->addCondition('active = 1');

$model = AssetBarang::model()->findAll($criteria);
$total = AssetBarang::model()->count($criteria);

$this->renderJson($model, $total);

}

}