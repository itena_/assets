<?php

class AssetController extends GxController {

    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
        return;

        if (isset($_POST) && !empty($_POST))
        {

            $is_new = $_POST['mode'] == 0;
            $as_id = $_POST['id'];
            //$source = $_POST['showsource'];

            //$branch = $_POST['branch'];
            $user_id = Yii::app()->user->getId();
            $businessunit_id = $_COOKIE['businessunitid'];
            $businessunit = $this->loadModel($businessunit_id, "Businessunit");

            //$detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();

            try
            {
                $model = $is_new ? new Asset : $this->loadModel($as_id, "Asset");
                //$store = Store::model()->findByAttributes(array('store_kode' => STOREID));

                /*if ($is_new) {
                    $ref = new Reference();
                } else {
                    AssetDetail::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $as_id));
                    AssetPeriode::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $as_id));
                }*/

                foreach($_POST as $k=>$v){
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Asset'][$k] = $v;
                }

                $_POST['Asset']['status'] = 1;
                $branch = $_POST['Asset']['branch'];
                $grupid = $_POST['Asset']['asset_group_id'];
                $qty = $_POST['Asset']['qty'];
                $ppn = $_POST['Asset']['ppnasset'];
                //$cat_id = $_POST['Asset']['category_id'];

                //$cat = $this->loadModel($cat_id, "AssetCategory");
                //$catcode = $cat->category_code;

                $group = $this->getGolongan($grupid);
                $last = $this->getLastRow($branch) + 1;
                
                $hide = isset($_POST['Asset']['hide'])?1:0;
                //if(PT_NEGARA=='NGI')
                //{
                    //$cabang = 'NG'.$store->store_kode;
                //}
                //else if(PT_NEGARA=='PNG')
                //{
                    //$cabang = 'PNG'.$store->store_kode;
                //}

                $cabang = $businessunit->businessunit_code.$branch;
                //$store = Store::model()->findByAttributes(array('store_kode' => $branch));
                $store = Store::model()->findByAttributes(array('store_kode' => $branch, 'businessunit_id' => $businessunit_id));
                $area = str_pad($store->wilayah_id, 2, '0', STR_PAD_LEFT);
                $urutan = str_pad($store->id_cabang, 2, '0', STR_PAD_LEFT);

                $golongan = $group[0]; //$this->getGolongan($grupid);
                $tariff = $group[1];
                $period = $group[2];
                $desc = $group[3];

                //$docref = $branch.'/'.$golongan.'/'.$last.'/'.date('m').'/'.date('y');

                $model->created_at = new CDbExpression('NOW()');
                $model->updated_at = new CDbExpression('NOW()');
                $model->hide = $hide;
                $model->businessunit_id = $businessunit_id;
                $model->user_id = $user_id;

                $year = date('Y', strtotime($_POST['Asset']['date_acquisition']));

                if ($is_new)
                {
                    $docref = $cabang.'AST'.$year.$last;
                } else {

                    $docref = $model->doc_ref;
                    $ati = $model->ati;

                    AssetDetail::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $model->asset_id));
                    AssetPeriode::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $model->asset_id));

                }

                $barang = AssetBarang::model()->findByPk($_POST['Asset']['asset_barang_id']);

                $_POST['Asset']['doc_ref'] = $docref;
                $_POST['Asset']['barang_id'] = $barang->barang_id;
                $_POST['Asset']['asset_name'] = $barang->nama_barang;
                $_POST['Asset']['new_price_acquisition'] = $_POST['Asset']['new_price_acquisition'] ? $_POST['Asset']['new_price_acquisition'] : "0";

                $ati = $area.$cabang.$urutan.'/'.$golongan;
                $model->attributes = $_POST['Asset'];
                $msg = "Data gagal disimpan.";

                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Asset')) . CHtml::errorSummary($model));
                }
                else
                {
                    for($i=0;$i<$qty;$i++)
                    {
                        $modeldetail = new AssetDetail;
                        $penyusutan = 0;
                        $penyusutantahun = 0;

                        $lastDetail = $this->getLastRowDetail($branch) + 1;

                        $docrefdetail = $docref.'/'.$lastDetail;
                        $modeldetail->ati = $ati.'/'.$lastDetail;
                        $modeldetail->ati_old = "-";
                        $modeldetail->businessunit_id = $businessunit_id;
                        $modeldetail->user_id = $user_id;
                        $modeldetail->category_id = $model->category_id;
                        $modeldetail->hide = $model->hide;
                        $modeldetail->asset_id = $model->asset_id;
                        $modeldetail->asset_trans_branch = $branch;
                        $modeldetail->asset_group_id = $grupid;
                        $modeldetail->barang_id = $model->barang_id;
                        $modeldetail->docref_other = $model->doc_ref;
                        $modeldetail->docref = $docrefdetail;
                        $modeldetail->asset_trans_name = $model->asset_name;
                        $modeldetail->asset_trans_date = $model->date_acquisition;
                        $modeldetail->asset_trans_price = $model->price_acquisition;
                        $modeldetail->asset_trans_new_price = $model->new_price_acquisition ;
                        $modeldetail->description = $model->description;
                        $modeldetail->class = $golongan;
                        $modeldetail->tariff = $tariff;
                        $modeldetail->period = $period;
                        $modeldetail->status = 1;


                        if($period)
                        {
                            $penyusutan = $modeldetail->asset_trans_price / $period;
                            $penyusutantahun = $penyusutan * $period;
                        }


                        $modeldetail->penyusutanperbulan = $penyusutan;
                        $modeldetail->penyusutanpertahun = $penyusutantahun;

                        $modeldetail->created_at = $model->created_at;
                        $modeldetail->updated_at = $model->updated_at;

                        if (!$modeldetail->save()) {
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Asset Detail')) . CHtml::errorSummary($modeldetail));
                        }
                        else
                        {
                            $total = $ppn + $modeldetail->asset_trans_price;
                            //GL
                            $gl = new GL();
                            $atiii = $modeldetail->ati;
                            $gl->add_gl_trans(ASSETBELI, $modeldetail->asset_trans_id, $modeldetail->asset_id,
                                $modeldetail->businessunit_id, $modeldetail->asset_trans_date,
                                $atiii, "$atiii", $modeldetail->asset_trans_price, $ppn, $total, $modeldetail->asset_trans_price,
                                $user_id, 1, 'beli',
                                $modeldetail->asset_trans_branch);

                            //ASSET HISTORY
                            $history = new History();
                            $history->add_history_status($businessunit_id, $modeldetail->asset_id, $modeldetail->asset_trans_id,$docref,
                                $modeldetail->ati,$modeldetail->asset_trans_name,$modeldetail->asset_trans_branch,
                                $modeldetail->asset_trans_price, $modeldetail->asset_trans_new_price,
                                $modeldetail->class, $modeldetail->tariff,$modeldetail->period,
                                $modeldetail->penyusutanperbulan,$modeldetail->penyusutanpertahun,
                                $modeldetail->description, $modeldetail->status);

                            /*$history = new AssetHistory;
                            $history->businessunit_id = $businessunit_id;
                            $history->asset_id = $modeldetail->asset_trans_id;
                            $history->docref = $modeldetail->docref;
                            $history->ati = $modeldetail->ati;
                            $history->name = $modeldetail->asset_trans_name;
                            $history->branch = $modeldetail->asset_trans_branch;
                            $history->price = $modeldetail->asset_trans_price;
                            $history->newprice = $modeldetail->asset_trans_new_price;
                            $history->class = $modeldetail->class;
                            $history->tariff = $modeldetail->tariff;
                            $history->period = $modeldetail->period;
                            $history->penyusutanperbulan = $modeldetail->penyusutanperbulan;
                            $history->penyusutanpertahun = $modeldetail->penyusutanpertahun;
                            $history->desc =  $modeldetail->description;
                            $history->status =  $modeldetail->status;
                            $history->sdate = new CDbExpression('NOW()');
                            $history->created_at = new CDbExpression('NOW()');
                            $history->updated_at = new CDbExpression('NOW()');

                            if (!$history->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetHistory')) . CHtml::errorSummary($history));
                            }*/

                            /*if($source == 'P')
                            {
                                //mengurangi stok barang perlengkapan
                                U::add_stock_moves_all(
                                    null,
                                    ASSET, $model->asset_id,
                                    $model->date_acquisition,
                                    $model->barang_id,
                                    -1,
                                    $model->doc_ref,
                                    $model->price_acquisition,
                                    $model->branch
                                );
                            }*/



                            $lastdeprisiasi = 0;
                            $lastbalance = 0;
                            $lastaccumulated = 0;

                            for($j=0;$j<$period;$j++)
                            {
                                $modelperiode = new AssetPeriode;

                                $modelperiode->docref = $modeldetail->docref;
                                $modelperiode->docref_other = $modeldetail->docref_other;
                                $modelperiode->ati = $modeldetail->ati;
                                $modelperiode->businessunit_id = $businessunit_id;
                                $modelperiode->asset_trans_id = $modeldetail->asset_trans_id;
                                $modelperiode->asset_id = $modeldetail->asset_id;
                                $modelperiode->asset_group_id = $modeldetail->asset_group_id;
                                $modelperiode->barang_id = $modeldetail->barang_id;
                                $modelperiode->asset_trans_date = $modeldetail->asset_trans_date;
                                $modelperiode->asset_trans_price = get_number($modeldetail->asset_trans_price);
                                $modelperiode->asset_trans_new_price = $modeldetail->asset_trans_new_price;
                                $modelperiode->asset_trans_name = $modeldetail->asset_trans_name;
                                $modelperiode->asset_trans_branch = $modeldetail->asset_trans_branch;
                                $modelperiode->description = $modeldetail->description;

                                $modelperiode->period = $modeldetail->period;
                                $modelperiode->class = $modeldetail->class;
                                $modelperiode->tariff = $modeldetail->tariff;


                                //$penyusutan = $modelperiode->asset_trans_price * ($modelperiode->tariff/100) / $modelperiode->period;
                                $penyusutan = $modelperiode->asset_trans_price / $modelperiode->period;

                                $modelperiode->penyusutanperbulan = $penyusutan;
                                $lastdeprisiasi = $modelperiode->penyusutanperbulan;

                                $penyusutantahun = $penyusutan * $modelperiode->period;
                                $modelperiode->penyusutanpertahun = $penyusutantahun;

                                if($lastbalance == 0  || $lastdeprisiasi == 0)
                                {
                                    $modelperiode->balance = $modelperiode->asset_trans_price - $penyusutan;
                                    $lastbalance = $modelperiode->balance;

                                    $modelperiode->akumulasipenyusutan = $penyusutan;
                                    $lastaccumulated = $modelperiode->akumulasipenyusutan;
                                }
                                else
                                {
                                    $modelperiode->balance = $lastbalance - $lastdeprisiasi;
                                    $lastbalance = $modelperiode->balance;

                                    $modelperiode->akumulasipenyusutan = $lastaccumulated + $penyusutan;
                                    $lastaccumulated = $modelperiode->akumulasipenyusutan;

                                }

                                //counter bulan
                                $x = $j;
                                $d = strtotime("$x months",strtotime($modeldetail->asset_trans_date));
                                $newdate = date('Y-m-t',$d);
                                /////////////////////////////////////////////

                                $modelperiode->tglpenyusutan = $newdate;


                                $modelperiode->startdate = $modelperiode->asset_trans_date;

                                $tmp = $modelperiode->period - 1;
                                $modelperiode->enddate = $newdate = date('Y-m-t',strtotime("+$tmp months",strtotime($modeldetail->asset_trans_date)));

                                $modelperiode->status = $modeldetail->status;

                                $modelperiode->created_at = $modeldetail->created_at;
                                $modelperiode->updated_at = $modeldetail->updated_at;

                                if (!$modelperiode->save()) {
                                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($modelperiode));
                                }
                            }
                        }
                    }
                    $status = true;
                    $msg = "Data berhasil di simpan dengan id " . $model->asset_id;
                }
                /*else
                {
                    $msg .= " ".implode(", ", $model->getErrors());
                    $status = false;
                }*/

                $transaction->commit();
                //StockMovesPerlengkapan::push($model->asset_id);

                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg));
                Yii::app()->end();
            }
            catch (Exception $ex)
            {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
                echo CJSON::encode(array(
                    'success' => 'failed',
                    'msg' => $msg
                ));
            }
        }
    }

    public function actionDataPeriode($docref)
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('doc_ref LIKE :doc_ref');
        $criteria->params[':docref'] = '%' . $docref . '%';
        $criteria->order = "tglpenyusutan ASC";
        $model = AssetPeriode::model()->findAll($criteria);
        $total = AssetPeriode::model()->count($criteria);
        $this->renderJson($model, $total);

        /*$criteria = new CDbCriteria;
        $criteria->compare('branch',$store);
        $model = Asset::model()->findAll($criteria);
        $count = count($model);

        return $count;*/
    }

    public function actionPrintAsset()
    {
        $tdate = $_POST['date'];
        $date = date('Y-m-d', strtotime($tdate));
        $data = array();

        $user_id = Yii::app()->user->getId();
        $Users = Users::model()->findByPk($user_id);
        $businessunit_id = $_COOKIE['businessunitid'];

        $criteria = new CDbCriteria;
        $criteria->compare('asset_trans_date',$date);
        $criteria->compare('asset_trans_branch',$Users->store);
        $criteria->compare('businessunit_id',$businessunit_id);
        $criteria->order = "asset_trans_name, ati ASC";
        $model = AssetDetail::model()->findAll($criteria);

        //$model = AssetDetail::model()->findByAttributes(array('asset_trans_date' => $date, 'asset_trans_branch' => STOREID));

        if ($model != null) {

            if($model != null){
                foreach($model as $val){
                    $attribDetil = $val->getAttributes();
                    //$data[] = json_encode($attribDetil);
                    $data[] = $attribDetil;
                }
            }

            echo CJSON::encode(array(
                'success' => 'success',
                'data' => $data,
                'msg' => 'success'
            ));

        } else {
            echo CJSON::encode(array(
                'success' => false,
                'msg' => 'data tidak ditemukan'
            ));
        }
    }

    public function getLastRow($store)
    {
        $businessunit_id = $_COOKIE['businessunitid'];
        $criteria = new CDbCriteria;
        $criteria->compare('branch',$store);
        $criteria->compare('active','1');
        $criteria->compare('businessunit_id',$businessunit_id);
        $model = Asset::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

    public function getLastRowDetail($store)
    {
        $businessunit_id = $_COOKIE['businessunitid'];
        $criteria = new CDbCriteria;
        $criteria->compare('active','1');
        $criteria->compare('asset_trans_branch',$store);
        $criteria->compare('businessunit_id',$businessunit_id);
        $model = AssetDetail::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

    public function getGolongan($id)
    {
        $query = "select golongan as gol, tariff, period, nscc_asset_group.`desc` from nscc_asset_group
                     where asset_group_id = '".$id."' limit 1";

        $list= Yii::app()->db->createCommand($query)->queryAll();

        $rs=array();
        foreach($list as $item)
        {
            $rs[0]=$item['gol'];
            $rs[1]=$item['tariff'];
            $rs[2]=$item['period'];
            $rs[3]=$item['desc'];
        }
        return $rs;
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Asset');

        if (isset($_POST) && !empty($_POST))
        {
            foreach($_POST as $k=>$v){
                if (is_angka($v)) $v = get_number($v);
                $_POST['Asset'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Asset'];

            if ($model->save())
            {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->asset_id;
            }
            else
            {
                $msg .= " ".implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest)
            {
                echo CJSON::encode(array(
                'success'=>$status,
                'msg'=>$msg
                ));
                Yii::app()->end();
            }
            else
            {
                $this->redirect(array('view', 'id' => $model->asset_id));
            }
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try
            {
                $Asset = $this->loadModel($id, 'Asset');
                $Asset->active = '0';
                if (!$Asset->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Asset')) . CHtml::errorSummary($Asset));}
                AssetDetail::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $id));
                AssetPeriode::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $id));
                AssetHistory::model()->deleteAll("masterassetid = :asset_id", array(':asset_id' => $id));
                GlTrans::model()->deleteAll("masterassetid = :asset_id", array(':asset_id' => $id));

                /*$del = $this->loadModel($id, 'Asset')->delete();
                AssetDetail::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $id));
                AssetPeriode::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $id));
                AssetHistory::model()->deleteAll("masterassetid = :asset_id", array(':asset_id' => $id));
                GlTrans::model()->deleteAll("masterassetid = :asset_id", array(':asset_id' => $id));*/
            }
            catch (Exception $ex)
            {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
            Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

public function actionIndex() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}
    $businessunit_id = $_COOKIE['businessunitid'];
    $criteria = new CDbCriteria();

    if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
    (isset($_POST['limit']) && isset($_POST['start']))) {
    $criteria->limit = $limit;
    $criteria->offset = $start;
    }

    $criteria->addCondition('active = 1');
    $criteria->addCondition('businessunit_id = :businessunit_id');
    $param[':businessunit_id'] = $businessunit_id;

    $criteria->params = $param;

    $model = AssetView::model()->findAll($criteria);
    $total = AssetView::model()->count($criteria);

    $this->renderJson($model, $total);

}

}