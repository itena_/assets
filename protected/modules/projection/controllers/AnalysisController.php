<?php

/**
 * Created by PhpStorm.
 * User: wisnu
 * Date: 02/04/2018
 * Time: 11:51
 */
class AnalysisController extends GxController
{

    public function actionShowByAccount()
    {
        if(isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if(isset($_POST['start'])){
            $start = $_POST['start'];

        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }

        $criteria->addCondition('businessunit_id = :businessunit_id');
        $criteria->addCondition('AccountCode = :AccountCode');
        $param[':businessunit_id'] = BUSINESSUNITID;
        $param[':AccountCode'] = $_POST['acc'];

        $criteria->params = $param;
        $criteria->order = "AccountCode ASC";

        $model = AnalysisView::model()->findAll($criteria);
        $total = AnalysisView::model()->count($criteria);

        $this->renderJson($model, $total);
    }

    public function actionShowByMonthYear()
    {
        if(isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if(isset($_POST['start'])){
            $start = $_POST['start'];

        } else {
            $start = 0;
        }

        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;

            $criteria->addCondition('businessunit_id = :businessunit_id');
            $param[':businessunit_id'] = BUSINESSUNITID;

            $criteria->params = $param;
            $criteria->order = "AccountCode ASC";
        }
        $model = AnalysisView::model()->findAll($criteria);
        $total = AnalysisView::model()->count($criteria);

        $this->renderJson($model, $total);
    }

    public function actionShowByYear()
    {
        if(isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if(isset($_POST['start'])){
            $start = $_POST['start'];

        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;

            $criteria->addCondition('businessunit_id = :businessunit_id');
            $param[':businessunit_id'] = BUSINESSUNITID;

            $criteria->params = $param;
            $criteria->order = "AccountCode ASC";
        }
        $model = AnalysisView::model()->findAll($criteria);
        $total = AnalysisView::model()->count($criteria);

        $this->renderJson($model, $total);
    }

    public function actionIndex()
    {
        if(isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if(isset($_POST['start'])){
            $start = $_POST['start'];

        } else {
            $start = 0;
        }


        //$user_id = Yii::app()->user->getId();
        //$users = Users::model()->findByPk( $user_id );
        //$businessunit_id = $users->businessunit_id;
        $businessunit_id = $_COOKIE['businessunitid'];


        $join1 = DbCmd::instance()
            ->addSelect("g.account_id , sum(g.amount) as amount")
            ->addFrom("realization g")
            ->addCondition("g.businessunit_id = :businessunit_id")
            ->addGroup("g.account_id");

        $join2 = DbCmd::instance()
            ->addSelect("b.account_id , sum(b.amount) as amount")
            ->addFrom("budget b")
            ->addCondition("b.businessunit_id = :businessunit_id")
            ->addGroup("b.account_id");

        $cmd = DbCmd::instance()
            ->addSelect("a.businessunit_id, a.account_code as 'AccountCode', a.account_name as 'AccountName',
            IFNULL(b.amount,0) as 'AmountBudget', 
            IFNULL(gl.amount,0) as 'AmountRealization' , 
            IFNULL((b.amount - gl.amount),0) as 'Achievement',
            IFNULL(Round((gl.amount/b.amount*100),0),0) as 'AchievementPercent'")
            ->addFrom( "account a");

        if (isset($_POST['acc']) && $_POST['acc']) {
            $join1->addCondition("g.account_id = :account_id");
            $join2->addCondition("b.account_id = :account_id");
            $cmd->addCondition("a.account_id = :account_id")
            ->addParams([':account_id' => $_POST['acc']]);
        }

        if (isset($_POST['year']) && $_POST['year']) {
            $join1->addCondition("YEAR(g.tdate) = :year");
            $join2->addCondition("YEAR(b.tdate) = :year");
            $cmd->addParams([':year' => $_POST['year']]);
        }

        if (isset($_POST['month']) && $_POST['month'])
        {
            $join1->addCondition("MONTH(g.tdate) = :month");
            $join2->addCondition("MONTH(b.tdate) = :month");
            $cmd->addParams([':month' => $_POST['month']]);
        }

        if (isset($_POST['date_start']) && $_POST['date_start'])
        {
            $join1->addCondition("g.tdate >= :date_start");
            $join2->addCondition("b.tdate >= :date_start");
            $cmd->addParams([':date_start' => $_POST['date_start']]);
        }

        if (isset($_POST['date_end']) && $_POST['date_end'])
        {
            $join1->addCondition("g.tdate <= :date_end");
            $join2->addCondition("b.tdate <= :date_end");
            $cmd->addParams([':date_end' => $_POST['date_end']]);
        }

        $cmd ->addLeftJoin($join1->setAs("gl"),"a.account_id = gl.account_id")
            ->addLeftJoin($join2->setAs("b"),"a.account_id = b.account_id")
            ->addCondition("a.businessunit_id = :businessunit_id")
            ->addParams([':businessunit_id' => $businessunit_id])
            ->addOrder('a.account_code asc');


        //$cmd->setConnection(Yii::app()->dbprojection);

        $count = $cmd->queryCount();
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $cmd->setLimit(array_key_exists('limit', $_POST)? $_POST['limit'] : 20, array_key_exists('start', $_POST)? $_POST['start'] : 0);
        }
        $model = $cmd->queryAll();
        $this->renderJsonArrWithTotal($model, $count);
    }
}