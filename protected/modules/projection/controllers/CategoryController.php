<?php

class CategoryController extends GxController {

    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;

        if (isset($_POST) && !empty($_POST))
        {

            $is_new = $_POST['mode'] == 0;
            $gp_id = $_POST['id'];

            //$user_id = Yii::app()->user->getId();
            //$users = Users::model()->findByPk( $user_id );
            //$businessunit_id = $users->businessunit_id;
            $businessunit_id = $_COOKIE['businessunitid'];

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();

            try{
                $model = $is_new ? new Category : $this->loadModel($gp_id, "Category");

                foreach($_POST as $k=>$v){
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Category'][$k] = $v;
                }

                $check = Category::model()->findByAttributes(['category_code' => $_POST['Category']['category_code' ],'businessunit_id' => $businessunit_id]);
                if($is_new)
                {
                    if($check)
                    {
                        $msg = "Data sudah ada di database.";
                        $status = false;
                        return;
                    }
                }

                $model->attributes = $_POST['Category'];
                $model->businessunit_id = $businessunit_id;//$businessunit->businessunit_id;
                $msg = "Data berhasil di simpan.";
                $status = true;

                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Category')) . CHtml::errorSummary($model));

                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            }
            catch (Exception $ex)
            {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            finally
            {
                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg));
                Yii::app()->end();

            }


        }
    }

public function actionUpdate($id) {
$model = $this->loadModel($id, 'Category');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['Category'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['Category'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->category_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->category_id));
}
}
}

public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'Category')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}


public function actionIndex() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}

    //$user_id = Yii::app()->user->getId();
    //$users = Users::model()->findByPk( $user_id );
    //$businessunit_id = $users->businessunit_id;
    $businessunit_id = $_COOKIE['businessunitid'];

$criteria = new CDbCriteria();
if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
(isset($_POST['limit']) && isset($_POST['start']))) {
$criteria->limit = $limit;
$criteria->offset = $start;

    $criteria->addCondition('businessunit_id = :businessunit_id');
    $param[':businessunit_id'] = $businessunit_id;

    $criteria->params = $param;
    $criteria->order = "category_code ASC";
}
$model = Category::model()->findAll($criteria);
$total = Category::model()->count($criteria);

$this->renderJson($model, $total);

}

}