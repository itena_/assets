<?php

/**
 * Created by PhpStorm.
 * User: wisnu
 * Date: 26/02/2018
 * Time: 14:40
 */

Yii::import('application.components.U');
Yii::import("application.components.tbs_class_php5", true);
Yii::import("application.components.tbs_plugin_excel", true);
Yii::import("application.components.tbs_plugin_opentbs", true);
Yii::import("application.components.tbs_plugin_aggregate", true);
Yii::import("application.extensions.PHPExcel", true);

class ReportController extends GxController
{
    private $TBS;
    private $logo;
    private $format;
    public $is_excel;

    public function init()
    {
        parent::init();
        if (!isset($_POST) || empty($_POST)) {
            $this->redirect(url('/'));
        }
        $this->layout = "report";
        $this->logo = bu() . app()->params['url_logo'];
        $this->TBS = new clsTinyButStrong;
        $this->format = $_POST['format'];
        if ($this->format == 'excel') {
            $this->TBS->PlugIn(TBS_INSTALL, TBS_EXCEL);
            $this->is_excel = true;
        } elseif ($this->format == 'openOffice') {
            $this->TBS->PlugIn(TBS_INSTALL, OPENTBS_PLUGIN);
        }
        error_reporting(E_ALL & ~E_NOTICE);
    }

    public function actionBudget()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $accountid = $_POST['account_id'];

            $account = BudgetView::model()->findByAttributes(['account_id' => $accountid]);
            $acc = $account->account_code;
            $accname = $account->account_name;

            $budget = U::report_budget($from, $to, $accountid);
            $total = array_sum(array_column($budget, 'amount'));
            if ($accountid == "") {
                $accountid = "All Account";
            }
            $dataProvider = new CArrayDataProvider($budget, array(
                'id' => 'budget',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=budget$from-$to.xls");
                echo $this->render('Budget', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'accountcode' => $acc,
                    'accountname' => $accname,
                    'total' => $total,
                ), true);
            } else {
                $this->render('Budget', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'accountcode' => $acc,
                    'accountname' => $accname,
                    'total' => $total,
                ));
            }
        }
    }

    /*public function actionBudget()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $accountid = $_POST['account_id'];

            $account = BudgetView::model()->findByAttributes(['account_id' => $accountid]);
            $acc = $account->account_code;

            $cust = U::report_budget($from, $to, $accountid);
            if ($accountid == "") {
                $accountid = "All Account";
            }
            $dataProvider = new CArrayDataProvider($cust, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=budget$from-$to.xls");
                echo $this->render('NewCustomers', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'accountcode' => $acc
                ), true);
            } else {
                $this->render('NewCustomers', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'accountcode' => $acc
                ));
            }
        }
    }*/



}