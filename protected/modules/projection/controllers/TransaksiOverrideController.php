<?php

class TransaksiOverrideController extends GxController {

    public function actionCreate() {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));

        if (isset($_POST) && !empty($_POST))
        {

            $is_new = $_POST['mode'] == 0;
            $id = $_POST['id'];

            $user_id = Yii::app()->user->getId();
            //$users = UserView::model()->findByPk( $user_id );
            $users = UserView::model()->findByAttributes(['id' => $user_id]);
            //$businessunit_id = $users->businessunit_id;
            $businessunit_id = $_COOKIE['businessunitid'];
            $bu = Businessunit::model()->findByPk( $businessunit_id );
            $businessunitcode = $bu->businessunit_code;

            $docref="";
            //$detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();

            try{
                $model = $is_new ? new TransaksiOverride: $this->loadModel($id, "TransaksiOverride");
                //$businessunit = Businessunit::model()->findByAttributes(['businessunit_code' => BUSINESSUNIT]);

                $last = $this->getLastRow($businessunit_id) + 1;


                if ($is_new) {
                    $docref = $businessunitcode.'/'.date('m').'/'.date('y').'/'.$last;
                } else {
                    //AssetDetail::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $as_id));
                    //AssetPeriode::model()->deleteAll("asset_id = :asset_id", array(':asset_id' => $as_id));
                    //TransaksiDetail::model()->deleteAll("transaksi_id = :transaksi_id", array(':transaksi_id' => $id));
                    $docref = $model->docref;
                }

                foreach($_POST as $k=>$v){
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['TransaksiOverride'][$k] = $v;
                }
                $_POST['TransaksiOverride']['docref'] = $docref;

                $time = strtotime($_POST['TransaksiOverride']['tdate']);
                $tdate = date('Y-m-d',$time);

                $model->attributes = $_POST['TransaksiOverride'];
                $model->businessunit_id = $businessunit_id;
                $model->flag = '1';
                $model->tdate = $tdate;
                $model->created_at = new CDbExpression('NOW()');
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'TransaksiOverride')) . CHtml::errorSummary($model));

                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            }
            catch (Exception $ex)
            {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            finally
            {
                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg));
                Yii::app()->end();

            }
        }
    }

    public function getLastRow($id)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('businessunit_id',$id);
        $model = TransaksiOverride::model()->findAll($criteria);
        $count = count($model);
        return $count;
    }

public function actionUpdate($id) {
$model = $this->loadModel($id, 'TransaksiOverride');


if (isset($_POST) && !empty($_POST)) {
foreach($_POST as $k=>$v){
if (is_angka($v)) $v = get_number($v);
$_POST['TransaksiOverride'][$k] = $v;
}
$msg = "Data gagal disimpan";
$model->attributes = $_POST['TransaksiOverride'];

    if ($model->save()) {

$status = true;
$msg = "Data berhasil di simpan dengan id " . $model->transaksi_id;
} else {
$msg .= " ".implode(", ", $model->getErrors());
$status = false;
}

if (Yii::app()->request->isAjaxRequest)
{
echo CJSON::encode(array(
'success'=>$status,
'msg'=>$msg
));
Yii::app()->end();
} else
{
$this->redirect(array('view', 'id' => $model->transaksi_id));
}
}
}

public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'TransaksiOverride')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}


public function actionIndex() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}


    $businessunit_id = $_COOKIE['businessunitid'];

$criteria = new CDbCriteria();
if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
(isset($_POST['limit']) && isset($_POST['start']))) {
$criteria->limit = $limit;
$criteria->offset = $start;
}

    $criteria->addCondition('businessunit_id = :businessunit_id');
    $param[':businessunit_id'] = $businessunit_id;
    $criteria->addCondition('flag = :flag');
    $param[':flag'] = '1';

    $criteria->params = $param;
    $criteria->order = "tdate ASC";
$model = TransaksiOverride::model()->findAll($criteria);
$total = TransaksiOverride::model()->count($criteria);

$this->renderJson($model, $total);

}

}