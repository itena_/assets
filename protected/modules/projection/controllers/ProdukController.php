<?php

class ProdukController extends GxController {

    public function actionCreate()
    {
        //$model = new Businessunit();
        if (!Yii::app()->request->isAjaxRequest)
            return;

        if (isset($_POST) && !empty($_POST))
        {

            $is_new = $_POST['mode'] == 0;
            $gp_id = $_POST['id'];

            //$user_id = Yii::app()->user->getId();
            //$users = Users::model()->findByPk( $user_id );
            //$businessunit_id = $users->businessunit_id;
            $businessunit_id = $_COOKIE['businessunitid'];

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();


            try{
                $model = $is_new ? new Produk : $this->loadModel($gp_id, "Produk");

                foreach($_POST as $k=>$v){
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Produk'][$k] = $v;
                }

                $check = Produk::model()->findByAttributes(['kodeproduk' => $_POST['Produk']['kodeproduk'],'businessunit_id' => $businessunit_id]);
                if($is_new)
                {
                    if($check)
                    {
                        $msg = "Data sudah ada di database.";
                        $status = false;
                        return;
                    }
                }

                $model->attributes = $_POST['Produk'];

                $model->businessunit_id = $businessunit_id;
                $model->created_at = new CDbExpression('NOW()');

                $msg = "Data berhasil di simpan.";
                $status = true;

                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Produk')) . CHtml::errorSummary($model));

                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            }
            catch (Exception $ex)
            {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            finally
            {
                echo CJSON::encode(array(
                    'success'=>$status,
                    'msg'=>$msg));
                Yii::app()->end();

            }
        }
    }

    public function actionUpdate($id) {
    $model = $this->loadModel($id, 'Produk');


    if (isset($_POST) && !empty($_POST)) {
    foreach($_POST as $k=>$v){
    if (is_angka($v)) $v = get_number($v);
    $_POST['Produk'][$k] = $v;
    }
    $msg = "Data gagal disimpan";
    $model->attributes = $_POST['Produk'];

        if ($model->save()) {

    $status = true;
    $msg = "Data berhasil di simpan dengan id " . $model->produk_id;
    } else {
    $msg .= " ".implode(", ", $model->getErrors());
    $status = false;
    }

    if (Yii::app()->request->isAjaxRequest)
    {
    echo CJSON::encode(array(
    'success'=>$status,
    'msg'=>$msg
    ));
    Yii::app()->end();
    } else
    {
    $this->redirect(array('view', 'id' => $model->produk_id));
    }
    }
    }

    public function actionDelete($id) {
    if (Yii::app()->request->isPostRequest) {
    $msg = 'Data berhasil dihapus.';
    $status = true;
    try {
    $this->loadModel($id, 'Produk')->delete();
    } catch (Exception $ex) {
    $status = false;
    $msg = $ex;
    }
    echo CJSON::encode(array(
    'success' => $status,
    'msg' => $msg));
    Yii::app()->end();
    } else
    throw new CHttpException(400,
    Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

/*    public function actionIndex()
    {

        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $select = "select p.produk_id, p.kodeproduk, p.nama, p.harga, p.hargabeli, p.created_at, gp.kode as 'kodegroup', gp.nama as 'namagroup', u.code as 'satuan' from produk p
                    join groupproduk gp on p.groupproduk_id = gp.groupproduk_id 
                    join unit u on p.unit_id = u.unit_id";

        $param = array();
        $where = "";
        $order = " ORDER BY p.kodeproduk asc";
        if (isset($_POST['businessunit_id'])) {

            $where = " WHERE p.businessunit_id = :businessunit_id ";
            $param[':businessunit_id'] = $_POST['businessunit_id'];
        }


        $total_cmd = Yii::app()->db->createCommand($select . $where);
        $total = $total_cmd->query($param)->rowCount;
        $comm = Yii::app()->db->createCommand($select . $where . $order);
//        $comm = Yii::app()->db->createCommand($select . $where . $order);
        $row = $comm->queryAll(true, $param);
        $this->renderJsonArrWithTotal($row, $total);
    }*/

    public function actionIndex()
    {
        $businessunit_id = "";

        if(isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if(isset($_POST['start'])){
            $start = $_POST['start'];
        } else {
            $start = 0;
        }

        //$user_id = Yii::app()->user->getId();
        //$users = Users::model()->findByPk( $user_id );
        //$businessunit_id = $users->businessunit_id;
        $businessunit_id = $_COOKIE['businessunitid'];

        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
        (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }

        if(isset($_POST['groupproduk_id'])){
            $criteria->addCondition('groupproduk_id = :groupproduk_id');
            $param[':groupproduk_id'] = $_POST['groupproduk_id'];
        }

        $criteria->addCondition('businessunit_id = :businessunit_id');
        $param[':businessunit_id'] = $businessunit_id;

        $criteria->params = $param;
        $criteria->order = "kodeproduk ASC";

        $model = Produk::model()->findAll($criteria);
        $total = Produk::model()->count($criteria);

        $this->renderJson($model, $total);

    }

}