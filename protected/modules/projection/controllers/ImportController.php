<?php
/**
 * Created by PhpStorm.
 * User: wisnu
 * Date: 07/07/2018
 * Time: 10:13
 */

class ImportController extends GxController
{
    public function actionUnit1()
    {
        /*if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {

            $user_id = Yii::app()->user->getId();
            $user = Users::model()->findByPk( $user_id );
            $businessunit_id = $user->businessunit_id;

            $filename = $_POST['filename'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $stat = StatusCust::model()->find("nama_status = :nama_status", array(':nama_status' => 'REG'));
                $ref = new Reference();
                $docref = $ref->get_next_reference(CUSTOMER);
                $arr_key = array_keys($detils);
                $duplicate_no_cust = array();
                foreach ($detils[$arr_key[0]] as $row) {
                    $arr_key = array_keys($row);
                    $arr_idx = array();
                    foreach ($arr_key as $key) {
                        if (strpos($key, 'NAME') !== false) {
                            $arr_idx[0] = $key;
                        }
                        if (strpos($key, 'CODE') !== false) {
                            $arr_idx[1] = $key;
                        }
                        if (strpos($key, 'DESCRIPTION') !== false) {
                            $arr_idx[2] = $key;
                        }
                    }
                    $unit = Unit::model()->find('code = :code',
                        array(':code' => $row[$arr_key[1]]));
                    if ($unit != null) {
                        $duplicate_unit[] = array(
                            $row[$arr_idx[0]],
                            $row[$arr_idx[1]],
                            $row[$arr_idx[2]],
                        );
                        continue;
                    }
                    $model = new Unit;
                    $model->businessunit_id = $businessunit_id;
                    $model->name = $row[$arr_idx[0]];
                    $model->code = $row[$arr_idx[1]];
                    $model->description = $row[$arr_idx[2]];
                    $model->created_at = new CDbExpression('NOW()');
                    $model->uploadfile = $user->user_id.''.$filename;
                    $model->uploaddate = new CDbExpression('NOW()');

                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Customers')) . CHtml::errorSummary($model));
                    }
                    $docref = $ref->_customers($docref);
                }
//                $msg = t('save.fail', 'app');
                $ref->save(CUSTOMER, $model->customer_id, $docref);
                $transaction->commit();
                $msg = '<table cellspacing="5" style="border:1px solid #BBBBBB;border-collapse:collapse;padding:5px; font-size: 10pt;">
                        <thead>
                            <tr>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;" >No. Customers</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Customer Name</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Address</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Telp</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">BirthDate</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Sex</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Occuption</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">First</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Last</th>
                            </tr>
                        </thead><tbody >';
                foreach ($duplicate_no_cust as $r) {
                    $msg .= "<tr>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[0]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[1]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[2]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[3]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[4]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[5]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[6]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[7]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[8]</td>
                            </tr>";
                }
                $msg .= "</tbody></table>";
//                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }*/


    }

    public function getLastRow($id)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('businessunit_id',$id);
        $model = Transaksi::model()->findAll($criteria);
        $count = count($model);
        return $count;
    }

    public function actionImport()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {

            $user_id = Yii::app()->user->getId();
            //$users = UserView::model()->findByPk( $user_id );
            $users = UserView::model()->findByAttributes(['id' => $user_id]);
            $businessunit_id = $_COOKIE['businessunitid'];
            //$businessunit_id = $users->businessunit_id;
            $bu = Businessunit::model()->findByPk( $businessunit_id );


            $businessunitcode = $bu->businessunit_code;
            $username = $users->user_id;

            $sheets = CJSON::decode($_POST['data']);
            $trans_date = $_POST['tgl'];
            $importtype = $_POST['importtype'];

            foreach ($sheets as $sheet) {

                /*
                 * Header kolom kode_barang dan qty
                 * ?????? Hanya sheet pertama yang tereksekusi
                 */

                if($importtype == "U")
                {
                    U::unit_import($sheet, $trans_date, $username, $businessunit_id);
                }
                elseif ($importtype == "O")
                {
                    U::outlet_import($sheet, $trans_date, $username, $businessunit_id);
                }
                elseif ($importtype == "GP")
                {
                    U::produk_group_import($sheet, $trans_date, $username, $businessunit_id);
                }
                elseif ($importtype == "P")
                {
                    U::produk_import($sheet, $trans_date, $username, $businessunit_id);
                }
                elseif ($importtype == "AC")
                {
                    U::account_category_import($sheet, $trans_date, $username, $businessunit_id);
                }
                elseif ($importtype == "AG")
                {
                    U::account_group_import($sheet, $trans_date, $username, $businessunit_id);
                }
                elseif ($importtype == "A")
                {
                    U::account_import($sheet, $trans_date, $username, $businessunit_id);
                }
                elseif ($importtype == "BU")
                {
                    U::businessunit_import($sheet, $trans_date, $username);
                }
                elseif ($importtype == "S")
                {
                    $last = $this->getLastRow($businessunit_id) + 1;
                    U::sales_import($sheet, $trans_date, $username, $last, $businessunit_id, $businessunitcode);
                }
                elseif ($importtype == "B")
                {
                    U::budget_import($sheet, $trans_date, $username, $businessunit_id);
                }
                elseif ($importtype == "R")
                {
                    U::realization_import($sheet, $trans_date, $username, $businessunit_id);
                }
            }
        }
    }
}