<?php
Yii::import('application.modules.projection.models._base.BaseGroupView');

class GroupView extends BaseGroupView
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->group_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->group_id = $uuid;
        }
        return parent::beforeValidate();
    }
}