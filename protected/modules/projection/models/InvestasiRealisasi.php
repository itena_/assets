<?php
Yii::import('application.modules.projection.models._base.BaseInvestasiRealisasi');

class InvestasiRealisasi extends BaseInvestasiRealisasi
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->investasi_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->investasi_id = $uuid;
        }
        return parent::beforeValidate();
    }
}