<?php
Yii::import('application.modules.projection.models._base.BaseTransaksiCategory');

class TransaksiCategory extends BaseTransaksiCategory
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->category_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->category_id = $uuid;
        }
        return parent::beforeValidate();
    }
}