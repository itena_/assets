<?php
Yii::import('application.modules.projection.models._base.BaseRealization');

class Realization extends BaseRealization
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->realization_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->realization_id = $uuid;
        }
        return parent::beforeValidate();
    }
}