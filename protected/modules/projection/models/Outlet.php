<?php
Yii::import('application.modules.projection.models._base.BaseOutlet');

class Outlet extends BaseOutlet
{
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}
    public function beforeValidate(){
        if ($this->outlet_id == null) {
            $command = $this->dbConnection->createCommand("SELECT UUID();");
            $uuid = $command->queryScalar();
            $this->outlet_id = $uuid;
        }
        return parent::beforeValidate();
    }
}