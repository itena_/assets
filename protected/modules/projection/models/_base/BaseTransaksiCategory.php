<?php

/**
 * This is the model base class for the table "transaksi_category".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "TransaksiCategory".
 *
 * Columns in table "transaksi_category" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $category_id
 * @property string $category_name
 * @property string $category_code
 * @property string $description
 *
 */
abstract class BaseTransaksiCategory extends GxActiveRecord {

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'transaksi_category';
	}

	public static function representingColumn() {
		return 'category_id';
	}

	public function rules() {
		return array(
			array('category_id', 'required'),
			array('category_id, category_name, category_code, description', 'length', 'max'=>50),
			array('category_name, category_code, description', 'default', 'setOnEmpty' => true, 'value' => null),
			array('category_id, category_name, category_code, description', 'safe', 'on'=>'search'),
		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'category_id' => Yii::t('app', 'Category'),
			'category_name' => Yii::t('app', 'Category Name'),
			'category_code' => Yii::t('app', 'Category Code'),
			'description' => Yii::t('app', 'Description'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('category_id', $this->category_id, true);
		$criteria->compare('category_name', $this->category_name, true);
		$criteria->compare('category_code', $this->category_code, true);
		$criteria->compare('description', $this->description, true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
		));
	}
}