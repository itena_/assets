<?php

/**
 * This is the model class for table "{{gl_trans}}".
 *
 * The followings are the available columns in table '{{gl_trans}}':
 * @property string $counter
 * @property integer $type
 * @property string $type_no
 * @property string $tran_date
 * @property string $memo_
 * @property string $amount
 * @property string $id_user
 * @property string $account_code
 * @property string $store
 * @property string $tdate
 * @property integer $cf
 * @property integer $up
 * @property integer $visible
 *
 * The followings are the available model relations:
 * @property ChartMaster $accountCode
 */
class HGlTrans extends PusatActiveRecord
{
	public function getDbConnection()
    {
        return self::getPusatDbConnection();
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{gl_trans}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('counter, type_no, account_code, store, tdate', 'required'),
			array('type, cf, up, visible', 'numerical', 'integerOnly'=>true),
			array('counter', 'length', 'max'=>36),
			array('type_no, id_user', 'length', 'max'=>50),
			array('amount', 'length', 'max'=>30),
			array('account_code', 'length', 'max'=>15),
			array('store', 'length', 'max'=>20),
			array('tran_date, memo_', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('counter, type, type_no, tran_date, memo_, amount, id_user, account_code, store, tdate, cf, up, visible', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'accountCode' => array(self::BELONGS_TO, 'ChartMaster', 'account_code'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'counter' => 'Counter',
			'type' => 'Type',
			'type_no' => 'Type No',
			'tran_date' => 'Tran Date',
			'memo_' => 'Memo',
			'amount' => 'Amount',
			'id_user' => 'Id User',
			'account_code' => 'Account Code',
			'store' => 'Store',
			'tdate' => 'Tdate',
			'cf' => 'Cf',
			'up' => 'Up',
			'visible' => 'Visible',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('counter',$this->counter,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('type_no',$this->type_no,true);
		$criteria->compare('tran_date',$this->tran_date,true);
		$criteria->compare('memo_',$this->memo_,true);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('id_user',$this->id_user,true);
		$criteria->compare('account_code',$this->account_code,true);
		$criteria->compare('store',$this->store,true);
		$criteria->compare('tdate',$this->tdate,true);
		$criteria->compare('cf',$this->cf);
		$criteria->compare('up',$this->up);
		$criteria->compare('visible',$this->visible);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HGlTrans the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
