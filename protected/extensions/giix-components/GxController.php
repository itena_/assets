<?php
/** TODO
 *  Tidak boleh ada delete find deleteAll
 */
/**
 * GxController class file.
 *
 * @author Rodrigo Coelho <giix@rodrigocoelho.com.br>
 * @link http://rodrigocoelho.com.br/giix/
 * @copyright Copyright &copy; 2010 Rodrigo Coelho
 * @license http://rodrigocoelho.com.br/giix/license/ New BSD License
 */
/**
 * GxController is the base class for the generated controllers.
 *
 * @author Rodrigo Coelho <giix@rodrigocoelho.com.br>
 * @since 1.0
 */
abstract class GxController extends CController {
	public $layout = 'plain';
	//public $layout = '//layouts/column1';
	public $menuz = array();
	public $menuzrs = array();
	/**
	 * @var array Context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu = array();
	/**
	 * @var array The breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs = array();
	public function init() {
		register_shutdown_function( array( $this, 'onShutdownHandler' ) );
		earlyFatalErrorHandler( true ); // Unregister early hanlder
		if ( ! Yii::app()->user->isGuest ) {
			/** @var Users $user */
			$id_user = Yii::app()->user->getId();
			$users = Users::model()->findByPk($id_user);
			if($users != null) {
				$users->session_validate();
			}
		}
	}
	public function onShutdownHandler() {
		// 1. error_get_last() returns NULL if error handled via set_error_handler
		// 2. error_get_last() returns error even if error_reporting level less then error
		$error = error_get_last();
		// Fatal errors
		$errorsToHandle = E_ERROR | E_PARSE | E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_COMPILE_WARNING;
		if ( ! is_null( $error ) && ( $error['type'] & $errorsToHandle ) ) {
			// It's better to set errorAction = null to use system view "error.php" instead of
			// run another controller/action (less possibility of additional errors)
			Yii::app()->errorHandler->errorAction = null;
			$message                              = 'FATAL ERROR: ' . $error['message'];
			if ( ! empty( $error['file'] ) ) {
				$message .= ' (' . $error['file'] . ' :' . $error['line'] . ')';
			}
			// Force log & flush as logs were already processed as the error is fatal
			Yii::log( $message, CLogger::LEVEL_ERROR, 'php' );
			Yii::getLogger()->flush( true );
			// Pass the error to Yii error handler (standard or a custom handler you may be using)
			Yii::app()->handleError( $error['type'], 'Fatal error: ' . $error['message'], $error['file'], $error['line'] );
		}
	}
	public function filters() {
		return array(
			'accessControl',
		);
	}
	public function accessRules() {
		return array(
			array(
				'allow',
				'users' => array( '*' ),
				'actions' => array( 'login', 'GetDateTime', 'Sync', 'error' ),
			),
			array(
				'allow',
				'users' => array( '@' ),
			),
			array(
				'deny',
				'users' => array( '*' ),
			),
		);
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 *
	 * @param mixed $id the ID of the model to be loaded
	 * @param string $modelClass the model class name
	 *
	 * @return GxActiveRecord the loaded model
	 * @throws Exception
	 */
	public function loadModel( $id, $modelClass ) {
		$model = GxActiveRecord::model( $modelClass )->findByPk( $id );
		if ( $model === null ) {
			throw new Exception( 'Data tidak ditemukan!!!' );
		}
		return $model;
	}
	/**
	 * Performs the AJAX validation.
	 *
	 * @param CModel $model the model to be validated
	 * @param string $form the name of the form
	 */
	protected function performAjaxValidation( $model, $form ) {
		if ( Yii::app()->request->isAjaxRequest && $_POST['ajax'] == $form ) {
			echo GxActiveForm::validate( $model );
			Yii::app()->end();
		}
	}
	/**
	 * Finds the related primary keys specified in the form post.
	 * Only for HAS_MANY and MANY_MANY relations.
	 *
	 * @param array $form The post data.
	 * @param array $relations A list of model relations.
	 *
	 * @return array An array where the keys are the relation names (string) and the values arrays with the related model primary keys (int|string) or composite primary keys (array with pk name (string) => pk value (int|string)).
	 * Example of returned data:
	 * array(
	 *   'categories' => array(1, 4),
	 *   'tags' => array(array('id1' => 3, 'id2' => 7), array('id1' => 2, 'id2' => 0)) // composite pks
	 * )
	 * An empty array is returned in case there is no related pk data from the post.
	 */
	protected function getRelatedData( $form, $relations ) {
		$relatedPk = array();
		foreach ( $relations as $relationName => $relationData ) {
			if ( isset( $form[ $relationName ] ) && ( ( $relationData[0] == GxActiveRecord::HAS_MANY )
			                                          || ( $relationData[0] == GxActiveRecord::MANY_MANY ) )
			) {
				$relatedPk[ $relationName ] = $form[ $relationName ] === '' ? null
					: $form[ $relationName ];
			}
		}
		return $relatedPk;
	}
	public function renderJson( $model, $total ) {
		$argh = array();
		foreach ( $model AS $dodol ) {
			//foreach($model AS $SJP)
			$argh[] = $dodol->getAttributes();
		};
		$jsonresult = '{"total":"' . $total . '","results":' . json_encode( $argh ) . '}';
		Yii::app()->end( $jsonresult );
	}
	public function renderJsonArr( $model = array() ) {
		$jsonresult = '{"total":"' . count( $model ) . '","results":' . json_encode( $model ) . '}';
		Yii::app()->end( $jsonresult );
	}
	public function renderJsonArrWithTotal( $model = array(), $total ) {
		$jsonresult = '{"total":"' . $total . '","results":' . json_encode( $model ) . '}';
		Yii::app()->end( $jsonresult );
	}
	public function delete_gl_trans( $type, $type_no ) {
		GlTrans::model()->updateAll( array(
			'visible' => 0,
			'up'      => 0
		), 'type = :type AND type_no = :type_no', array( ':type' => $type, ':type_no' => $type_no ) );
	}
//    public function delete_refs($type, $type_no)
//    {
//        Refs::model()->deleteAll('type_ = :type AND type_no = :type_no', array(':type' => $type, ':type_no' => $type_no));
//    }
	public function delete_bank_trans( $type, $type_no ) {
		BankTrans::model()->updateAll( array(
			'visible' => 0,
			'up'      => 0
		), 'type_ = :type AND trans_no = :type_no', array( ':type' => $type, ':type_no' => $type_no ) );
	}
	public function delete_stock_moves( $type, $type_no ) {
		#StockMoves::model()->deleteAll('type_no = :type AND trans_no = :type_no', array(':type' => $type, ':type_no' => $type_no));
		StockMoves::model()->updateAll( array(
			'visible' => 0,
			'up'      => 0
		), 'type_no = :type AND trans_no = :type_no', array( ':type' => $type, ':type_no' => $type_no ) );
	}
	public function delete_stock_moves_perlengkapan( $type, $type_no ) {
		#StockMoves::model()->deleteAll('type_no = :type AND trans_no = :type_no', array(':type' => $type, ':type_no' => $type_no));
		StockMovesPerlengkapan::model()->updateAll( array(
			'visible' => 0,
			'up'      => 0
		), 'type_no = :type AND trans_no = :type_no', array( ':type' => $type, ':type_no' => $type_no ) );
	}
	public function generate_uuid() {
		$command = Yii::app()->db->createCommand( "SELECT UUID();" );
		return $command->queryScalar();
	}
}