<?php
class DbCmd {
    private  $cmd;

    private $connection;
    public  $select;
    private $arrSelect;
    public  $from;
    private  $arrFrom;
    private  $join;
    private  $arrWhere;
    public  $params;
    public  $group;
    private  $arrGroup;
    public  $order;
    private  $arrOrder;
    private  $arrHaving;
    public  $limit;
    public  $offset;
    public  $as;
    public  $isUnion;

    private static $inCounter = 0;

    private $tablePattern = "/\S+((\s+(AS|as|As|sA)\s+)|(\s+))\S+/";

    static public function instance(){
        return new self();
    }

    public function __construct($table=''){
        $this->reset();
        $table && $this->addFrom($table);
        $this->connection = Yii::app()->db;
    }

    public function reset(){
        $this->select = '';
        $this->arrSelect = array();

        $this->from = '';
        $this->arrFrom = array();

        $this->join = array();

        $this->arrWhere = array();
        $this->params = array();

        $this->group = '';
        $this->arrGroup = array();

        $this->order = '';
        $this->arrOrder = array();

        $this->arrHaving = array();

        $this->limit = false;
        $this->offset = false;

        $this->as = false;

        $this->isUnion = false;

        //self::$inCounter = 0;
    }

    public function setConnection($connection){
        $this->connection = $connection;
    }

    private function mergeParams($params = []){
        $this->params = array_merge($this->params, $params);
    }

    private function addArrFrom($table){
        if($table instanceof self){
            $this->arrFrom[] = '('.$table->getText().')'.($this->isUnion?'':' AS '.($table->as? $table->as : 't'.count($this->arrFrom)));
            $this->params = array_merge($this->params, $table->params);
        } else
            $this->arrFrom[] = $table;
    }

    public function addFrom($table){
        if(is_array($table)){
            foreach ($table as $t){
                $this->addArrFrom($t);
            }
        } else
            $this->addArrFrom($table);

        return $this;
    }

    public function clearFrom(){
        $this->arrFrom = [];

        return $this;
    }

    public function addSelect($fields){
        if(is_array($fields)) $this->arrSelect = array_merge($this->arrSelect, $fields);
        else $this->arrSelect[] = $fields;

        return $this;
    }

    public function updateSelect($index = -1, $fields){
//        if(is_array($fields))
//            foreach ($fields as $key => $value) $this->arrSelect[$key] = $value;
//        else
        $this->arrSelect[$index] = $fields;

        return $this;
    }

    public function clearSelect(){
        $this->arrSelect = [];

        return $this;
    }

    const Equal = '=';
    const NotEqual = '!=';
    public function addCompare($field1, $field2, $rule = self::Equal, $operand = 'AND'){
        if(count($this->arrWhere)) $this->arrWhere[] = $operand;
        $this->arrWhere[] = '(('.$field1.' IS NOT NULL AND '.$field2.' IS NOT NULL AND '.' '.$field1.$rule.' '.$field2.') OR ('.$field1.' IS NULL '.$rule.' '.$field2.' IS NULL))';

        return $this;
    }

    const OperatorAND = 'AND';
    const OperatorOR = 'OR';
    public function addCondition($condition, $operand = self::OperatorAND){
        if(count($this->arrWhere)) $this->arrWhere[] = $operand;
        $this->arrWhere[] = $condition;

        return $this;
    }

    public function addIsNullCondition($field, $operand = self::OperatorAND){
        $this->addCondition($field.' IS NULL', $operand);

        return $this;
    }

    public function addIsNotNullCondition($field, $operand = self::OperatorAND){
        $this->addCondition($field.' IS NOT NULL', $operand);

        return $this;
    }

    public function addInCondition($field, $values, $operand = self::OperatorAND){
        $this->addInOrNotInCondition('IN', $field, $values, $operand);

        return $this;
    }

    public function addNotInCondition($field, $values, $operand = self::OperatorAND){
        $this->addInOrNotInCondition('NOT IN', $field, $values, $operand);

        return $this;
    }

    private function addInOrNotInCondition($in, $field, $values, $operand = self::OperatorAND){
        self::$inCounter++;
        if(count($this->arrWhere)) $this->arrWhere[] = $operand;
        $condition = $field." $in ( ";

        if($values instanceof self){
            $condition .= $values->getText();
            $this->params = array_merge($this->params, $values->params);
        }else if(is_array($values)){
            for($i = 0; $i < count($values); $i++){
                if($i > 0 && $i < count($values)) $condition .= ' , ';
                $key = ":inOrNotIn".self::$inCounter.$i;
                $condition .= $key;
                $this->params[$key] = $values[$i];
            }
        }else{
            $condition .= $values;
        }

        $this->arrWhere[] = $condition.' )';
    }

    public function addParam($key, $value){
        $this->params[$key] = $value;

        return $this;
    }

    public function addParams($params=array()){
        if(is_array($params))
            $this->params = array_merge($this->params, $params);

        return $this;
    }

    const LEFT_JOIN = 'LEFT JOIN';
    const RIGHT_JOIN = 'RIGHT JOIN';
    const INNER_JOIN = 'INNER JOIN';
    public function addJoin($table, $condition, $join = self::LEFT_JOIN){
        if($table instanceof self){
            $joinTable = '('.$table->getText().') AS '.($table->as? $table->as : 'jt'.count($this->join));
            $this->params = array_merge($this->params, $table->params);
        }else{
            $joinTable = $table;
        }

        if($condition instanceof self){
            $this->mergeParams($condition->params);
            $condition = $condition->getWhere();
        }

        $this->join[] = ' ' . $join . ' ' . $joinTable . ' ON ' . $condition . ' ';

        return $this;
    }

    public function addLeftJoin($table, $condition){
        $this->addJoin($table, $condition, self::LEFT_JOIN);

        return $this;
    }

    public function addRightJoin($table, $condition){
        $this->addJoin($table, $condition, self::RIGHT_JOIN);

        return $this;
    }

    public function addInnerJoin($table, $condition){
        $this->addJoin($table, $condition, self::INNER_JOIN);

        return $this;
    }

    public function addOrder($fields){
        if(is_array($fields)) $this->arrOrder = array_merge($this->arrOrder, $fields);
        else $this->arrOrder[] = $fields;

        return $this;
    }

    public function clearOrder(){
        $this->arrOrder = [];

        return $this;
    }

    public function addGroup($fields){
        if(is_array($fields)) $this->arrGroup = array_merge($this->arrGroup, $fields);
        else $this->arrGroup[] = $fields;

        return $this;
    }

    public function clearGroup(){
        $this->arrGroup = [];

        return $this;
    }

    public function addHaving($condition, $operand = self::OperatorAND){
        if(count($this->arrHaving)) $this->arrHaving[] = $operand;
        $this->arrHaving[] = $condition;

        return $this;
    }

    public function clearHaving(){
        $this->arrHaving = [];

        return $this;
    }

    public function setLimit($limit, $offset = false){
        $this->limit = (int)$limit;
        if($offset!==false) $this->offset = (int)$offset;

        return $this;
    }

    public function union(array $tables){
        $this->reset();
        $this->isUnion = true;
        foreach ($tables as $table)
            $this->addArrFrom($table);

        return $this;
    }

    private function prepare(){
        if($this->isUnion){
            $this->cmd = $this->connection->createCommand(implode(" UNION ", $this->arrFrom));
        }else{
            $newLine = chr(13).chr(10);

            $this->cmd = $this->connection->createCommand();
            $this->cmd->select = ($this->select? $this->select.(count($this->arrSelect)?', ':''):'').implode($newLine.", ", $this->arrSelect);
            $this->cmd->from = ($this->from? $this->from.(count($this->arrFrom)?', ':''):'').implode($newLine.", ", $this->arrFrom);
            $this->cmd->join = implode(" ".$newLine, $this->join);
            $this->cmd->where = implode(" ".$newLine, $this->arrWhere);
            $this->cmd->order = ($this->order? $this->order.(count($this->arrOrder)?', ':''):'').implode($newLine.", ", $this->arrOrder);
            $this->cmd->group = ($this->group? $this->group.(count($this->arrGroup)?', ':''):'').implode($newLine.", ", $this->arrGroup);
            $this->cmd->having = implode(" ".$newLine, $this->arrHaving);
            if($this->limit !== false) $this->cmd->limit($this->limit);
            if( $this->offset !== false) $this->cmd->offset($this->offset);
        }
    }

    public function query($params = array()){
        $this->prepare();
        return $this->cmd->query(count($params)? array_merge($this->params, $params):$this->params);
    }

    public function queryAll($fetchAssociative=true, $params = array()){
        $this->prepare();
        return $this->cmd->queryAll($fetchAssociative, count($params)? array_merge($this->params, $params):$this->params);
    }

    public function queryRow($fetchAssociative=true, $params = array()){
        $this->prepare();
        return $this->cmd->queryRow($fetchAssociative, count($params)? array_merge($this->params, $params):$this->params);
    }

    public function queryScalar($params = array()){
        $this->prepare();
        return $this->cmd->queryScalar(count($params)? array_merge($this->params, $params):$this->params);
    }

    public function queryColumn($params = array()){
        $this->prepare();
        return $this->cmd->queryColumn(count($params)? array_merge($this->params, $params):$this->params);
    }

    public function queryCount($params = array()){
        $this->prepare();
        $this->cmd->select = 'COUNT(*)';
        return $this->cmd->queryScalar(count($params)? array_merge($this->params, $params):$this->params);
    }

    public function getText(){
        $this->prepare();
        return $this->cmd->getText();
    }

    /**
     * The alias name,
     * Will be used when applied as subquery
     * @param $alias
     * @return $this
     */
    public function setAs($alias){
        $this->as = $alias;

        return $this;
    }

    public function getWhere(){
        return implode(" ", $this->arrWhere);
    }
}