<?php

class U
{

    static function get_report_detil_nota($from, $to, $jnstrans, $sales_id, $supp_id, $pasar_id, $konsumen_id)
    {
        $select [] = "CONCAT(pp.doc_ref,' ') `No Faktur`";
        if ($sales_id !== null) {
            $select [] = 'ps1.salesman_name Salesman';
//            $group[] = 'ps.salesman_name';
        }
        $select [] = "pp.tgl Tanggal";
        if ($konsumen_id !== null) {
            $select [] = 'pk.konsumen_name Nama Konsumen';
        }
        $select [] = "ptk.type_konsumen_name Tipe";
        if ($pasar_id !== null) {
            $select [] = 'pp1.pasar_name Pasar';
        }
        $select [] = "pa.area_name Area";
        $select [] = "CONCAT(pb.barcode,' ') Kode";
        $select [] = "pb.barang_name Nama Barang";
        if ($supp_id !== null) {
            $select [] = 'ps.supplier_name Suplier';
        }
        $select [] = "pdp.jml Kuantitas Penjualan";
        $select [] = "pdp.sat Satuan";
        $select [] = "pdp.pcs 'Kuantitas Jual (pieces)'";
        $select [] = "IF(pb.bonus,'B','-') Bonus";
        $select [] = "CONCAT(pdp.disc1,'%') `Disc 1`";
        $select [] = "CONCAT(pdp.disc2,'%') `Disc 2`";
        $select [] = "pdp.pot Potongan";
        $select [] = "pdp.nominal Nominal";
        $select [] = "pp.total";
        $query = Yii::app()->db->createCommand();
        $query->select($select);
        $query->from = '{{penjualan}} pp';
        $query->join('{{detil_penjualan}} pdp', '( pp.penjualan_id = pdp.penjualan_id  )');
        $query->join('{{barang}} pb', '( pdp.barang_id = pb.barang_id  )  ');
        $query->join('{{supplier}} ps', '( pb.supplier_id = ps.supplier_id  ) ');
        $query->join('{{salesman}} ps1', '( pp.salesman_id = ps1.salesman_id  )');
        $query->join('{{konsumen}} pk', '( pp.konsumen_id = pk.konsumen_id  ) ');
        $query->leftJoin('{{type_konsumen}} ptk', '( pk.type_konsumen_id = ptk.type_konsumen_id  )');
        $query->leftJoin('{{pasar}} pp1', '( pk.pasar_id = pp1.pasar_id  )  ');
        $query->join('{{area}} pa', '( pk.area_id = pa.area_id  )  ');
        $query->order("pp.doc_ref ASC");
        $query->andWhere('pp.tgl >= :from', array(':from' => $from));
        $query->andWhere('pp.tgl <= :to', array(':to' => $to));
        if ($sales_id != null) {
            $query->andWhere('pp.salesman_id = :sales_id', array(':sales_id' => $sales_id));
        }
        if ($supp_id != null) {
            $query->andWhere('pb.supplier_id = :supp_id', array(':supp_id' => $supp_id));
        }
        if ($konsumen_id != null) {
            $query->andWhere('pp.konsumen_id = :konsumen_id', array(':konsumen_id' => $konsumen_id));
        }
        if ($pasar_id != null) {
            $query->andWhere('pk.pasar_id = :pasar_id', array(':pasar_id' => $pasar_id));
        }
        if ($jnstrans != null) {
            $total = $jnstrans == 'P' ? "pp.total > 0" : "pp.total < 0";
            $query->andWhere($total);
        }
        return $query->queryAll(true);
    }

    static function get_kelola_stok()
    {
        $select [] = "DISTINCT(trans_no) trans_no";
        $select [] = "tran_date";
        $select [] = "reference";
        $select [] = "memo_ note";
        $query = Yii::app()->db->createCommand();
        $query->select($select);
        $query->from = "{{stock_moves}} psm";
        $query->leftJoin("{{comments}} c", '(psm.type = c.type) AND (psm.trans_no = c.type_no) ');
        $query->andWhere('psm.type = :type', array(':type' => KELOLASTOK));
        return $query->queryAll(true);
    }

    static function get_selisih_stok()
    {
        $select [] = "DISTINCT(trans_no) trans_no";
        $select [] = "tran_date";
        $select [] = "reference";
        $select [] = "memo_ note";
        $query = Yii::app()->db->createCommand();
        $query->select($select);
        $query->from = "{{stock_moves}} psm";
        $query->leftJoin("{{comments}} c", '(psm.type = c.type) AND (psm.trans_no = c.type_no) ');
        $query->andWhere('psm.type = :type', array(':type' => SELISIHSTOK));
        return $query->queryAll(true);
    }

    static function get_report_laba_kotor($from, $to, $salesman_id)
    {
        return Yii::app()->db->createCommand("SELECT CONCAT(cr.barcode,' ') Kode, cr.barang_name `Nama Barang`,
        cr.b `Penjualan Bruto`, cr.p Potongan, cr.n `Penjualan Netto`,?  cr.h HPP, cr.n - cr.h `Laba Kotor` FROM
    (SELECT pb.barcode, pb.barang_name, SUM(pdp.bruto) b, SUM(pdp.totalpot) p, SUM(pdp.nominal) n,SUM(pdp.hpp) h
    FROM {{detil_penjualan}} pdp
?  INNER JOIN {{penjualan}} pp ON ( pdp.penjualan_id = pp.penjualan_id  )
?  INNER JOIN {{barang}} pb ON ( pdp.barang_id = pb.barang_id  )
?  WHERE pp.tgl >= :FROM AND pp.tgl <= :TO AND pp.salesman_id = :sales_id
?  GROUP BY pb.barcode, pb.barang_name) cr")->queryAll(true, array(
            ':from' => $from,
            ':to' => $to,
            ':sales_id' => $salesman_id
        ));
    }

    static function get_report_biaya_sales($from, $to, $salesman_id)
    {
        return Yii::app()->db->createCommand("SELECT pbs.tgl Tanggal, pbs.bbm BBM, pbs.makan Makan,
    pbs.parkir Parkir, IFNULL((SELECT SUM(pbsl.amount) FROM {{biaya_sales_lain}} pbsl
    WHERE pbsl.biaya_sales_id = pbs.biaya_sales_id),0) `Lain-lain`
    FROM {{biaya_sales}} pbs
    WHERE pbs.tgl >= :FROM AND pbs.tgl <= :TO AND pbs.salesman_id = :sales_id
    ORDER BY pbs.tgl")
            ->queryAll(true, array(
                ':from' => $from,
                ':to' => $to,
                ':sales_id' => $salesman_id
            ));
    }

    static function get_dos_detil($dos_id)
    {
        return Yii::app()->db->createCommand("SELECT pb.barcode, pb.barang_name, pdd.jml, pdd.sat, pdd.pcs
        FROM {{dos_detil}} pdd
?  INNER JOIN {{barang}} pb ON ( pdd.barang_id = pb.barang_id  )
        WHERE pdd.dos_id = :dos_id")->queryAll(true, array(':dos_id' => $dos_id));
    }

    static function get_botol_detil($tgl, $sales)
    {
        return Yii::app()->db->createCommand("SELECT pbd.jenis_botol_id, SUM(pbd.jml) jml_faktur,
        SUM(pbd.jml) jml_data_gudang, 0 jml_selisih, pjb.price
FROM {{botol}} pb
?  INNER JOIN {{botol_detil}} pbd ON ( pb.botol_id = pbd.botol_id  )
?  INNER JOIN {{jenis_botol}} pjb ON ( pbd.jenis_botol_id = pjb.jenis_botol_id )
WHERE pb.tgl = :tgl AND pb.salesman_id = :sales
GROUP BY pbd.jenis_botol_id, jml_selisih, pjb.price
")->queryAll(true, array(':tgl' => $tgl, ':sales' => $sales));
    }

    static function get_barang_cmp()
    {
        return Yii::app()->db->createCommand("SELECT pb.barang_id, pb.barcode, pb.barang_name, pb.status,
IFNULL((SELECT SUM(psm.qty) FROM {{stock_moves}} psm
?  WHERE psm.barang_id = pb.barang_id),0) - IFNULL((SELECT SUM(pdp.pcs) FROM {{detil_penjualan}} pdp
?  INNER JOIN {{penjualan}} pp ON ( pdp.penjualan_id = pp.penjualan_id  )
?  WHERE pp.final = 0 AND pdp.barang_id = pb.barang_id),0) sisa
FROM {{barang}} pb")->queryAll();
    }

    static function report_pengiriman($tgl, $sales)
    {
        return Yii::app()->db->createCommand("SELECT pp.tgl `Tgl`,pp.doc_ref `No. Faktur`,
        CONCAT(pk.konsumen_name,'  ') `Nama Konsumen`,CONCAT(pa.area_name,pk.address) Alamat, pp.total Netto
    FROM {{penjualan}} pp INNER JOIN {{konsumen}} pk ON ( pp.konsumen_id = pk.konsumen_id  )
?  ?  INNER JOIN {{area}} pa ON ( pk.area_id = pa.area_id  )
?  ?  WHERE pp.tgl = :tgl AND pp.salesman_id = :sales")
            ->queryAll(true, array(':tgl' => $tgl, ':sales' => $sales));
    }

    // ------------------------------------------------ DOS ----------------------------------------------------------------
    static function get_piutang($konsumen)
    {
        return Yii::app()->db->createCommand("
        SELECT * FROM (SELECT pp.penjualan_id,  pp.tgl, pp.doc_ref no_faktur, pp.total nilai,
        pp.total - (IF(SUM(pppd.kas_diterima) IS NULL, 0, SUM(pppd.kas_diterima)) + pp.uang_muka) sisa
FROM {{penjualan}} pp
?  LEFT OUTER JOIN {{pelunasan_piutang_detil}} pppd ON ( pp.penjualan_id = pppd.penjualan_id  )
WHERE `konsumen_id` = :konsumen AND NOT pp.lunas
GROUP BY pp.penjualan_id, pp.doc_ref, pp.tgl, pp.total) spp
WHERE spp.sisa != 0
")->queryAll(true, array(':konsumen' => $konsumen));
    }

    static function get_piutang_per_sales($sales, $tgl)
    {
        return Yii::app()->db->createCommand("
        SELECT * FROM (SELECT pp.`penjualan_id`,pp.tgl, pp.doc_ref no_faktur, pp.total, pk.konsumen_code, pk.konsumen_name,
(SELECT pp.total - (IF(SUM(pppd.kas_diterima) IS NULL, 0, SUM(pppd.kas_diterima)) + pp.uang_muka)
?  FROM {{pelunasan_piutang_detil}} pppd
?  INNER JOIN {{pelunasan_piutang}} ppp ON (pppd.pelunasan_piutang_id = ppp.pelunasan_piutang_id)
?  WHERE pppd.penjualan_id = pp.`penjualan_id`
?  AND ppp.tgl <= :tgl
 ) sisa
FROM {{penjualan}} pp
?  INNER JOIN {{konsumen}} pk ON ( pp.konsumen_id = pk.konsumen_id  )
WHERE NOT pp.`lunas` AND pp.salesman_id = :salesman_id) spp
WHERE spp.sisa != 0")->queryAll(true, array(':salesman_id' => $sales, ':tgl' => $tgl));
    }

    static function get_report_piutang_all($tgl)
    {
        return Yii::app()->db->createCommand("
        SELECT spp.doc_ref `No Faktur`, spp.tgl `Tgl Faktur`,spp.tempo `Tgl Jatuh Tempo`,
        spp.konsumen_code `Kode Konsumen`, spp.konsumen_name `Nama Konsumen`, spp.total `Nilai Faktur`,
        spp.sisa `Sisa Tagihan` FROM (SELECT pp.doc_ref, pp.tgl,pp.tempo,pk.konsumen_code, pk.konsumen_name, pp.total,
(SELECT pp.total - (IF(SUM(pppd.kas_diterima) IS NULL, 0, SUM(pppd.kas_diterima)) + pp.uang_muka)
?  FROM {{pelunasan_piutang_detil}} pppd
?  INNER JOIN {{pelunasan_piutang}} ppp ON (pppd.pelunasan_piutang_id = ppp.pelunasan_piutang_id)
?  WHERE pppd.penjualan_id = pp.`penjualan_id`
?  AND ppp.tgl <= :tgl) sisa
FROM {{penjualan}} pp
?  INNER JOIN {{konsumen}} pk ON ( pp.konsumen_id = pk.konsumen_id  )
WHERE NOT pp.`lunas`) spp
WHERE spp.sisa != 0")->queryAll(true, array(':tgl' => $tgl));
    }

    static function get_pelunasan($tgl, $sales, $konsumen)
    {
        return Yii::app()->db->createCommand("SELECT pp.penjualan_id,  ppp.tgl, pp.doc_ref AS no_faktur,
         pppd.sisa,
        pp.total AS nilai,pppd.kas_diterima
FROM {{pelunasan_piutang}} ppp
?  INNER JOIN {{pelunasan_piutang_detil}} pppd ON ( ppp.pelunasan_piutang_id = pppd.pelunasan_piutang_id  )
?  ?  INNER JOIN {{penjualan pp}} ON ( pppd.penjualan_id = pp.penjualan_id  )
WHERE ppp.tgl = :tgl AND
?  ppp.salesman_id = :sales AND
?  ppp.konsumen_id = :konsumen
GROUP BY pp.penjualan_id, no_faktur, nilai,pppd.kas_diterima")
            ->queryAll(true, array(':tgl' => $tgl, ':sales' => $sales, ':konsumen' => $konsumen));
    }

    static function get_retur_jual()
    {
        return Yii::app()->db->createCommand("SELECT pp.penjualan_id, pp.salesman_id, pp.konsumen_id, pp.doc_ref,
        pp.tgl, pp.tempo, -pp.sub_total sub_total, -pp.total total, -pp.uang_muka uang_muka,
        pp.no_bg_cek, -pp.sisa_tagihan sisa_tagihan, pp.lunas,
        pp.final, pp.id_user,pp.parent, -pp.bruto bruto
        FROM {{penjualan}} pp
        WHERE pp.total <= 0")
            ->queryAll();
    }

    static function get_retur_jual_detil($id)
    {
        return Yii::app()->db->createCommand("SELECT pdp.detil_penjualan, pdp.penjualan_id, pdp.barang_id, pdp.sat,
        -pdp.jml jml, pdp.price, pdp.disc1, pdp.disc2, pdp.pot, -pdp.nominal nominal, -pdp.pcs pcs
        FROM {{detil_penjualan}} pdp
        WHERE pdp.penjualan_id = :penjualan_id")
            ->queryAll(true, array(':penjualan_id' => $id));
    }

    // ------------------------------------------------ Void ----------------------------------------------------------------
    static function get_voided($type)
    {
        $void = Yii::app()->db->createCommand()->select('id')->from('mt_voided')->where('type=:type', array(
            ':type' => $type
        ))->queryColumn();
        return $void;
    }

    static function get_max_type_no($type)
    {
        $type_no = app()->db->createCommand()->select("MAX(type_no)")
            ->from("{{gl_trans}}")->where('type=:type', array(':type' => $type))->queryScalar();
        return $type_no == false ? 0 : $type_no;
    }

    static function get_max_type_no_stock($type)
    {
        $type_no = app()->db->createCommand()->select("MAX(trans_no)")
            ->from("{{stock_moves}}")->where('type=:type', array(':type' => $type))->queryScalar();
        return $type_no == false ? 0 : $type_no;
    }

    // --------------------------------------------- Bank Trans -------------------------------------------------------------
    static function get_next_trans_no_bank_trans($type, $store = STOREID)
    {
//        $db = BankTrans::model()->getDbConnection();
        $total = app()->db->createCommand(
            "SELECT MAX(trans_no)
            FROM nscc_bank_trans WHERE type_= :type AND store = :store")
            ->queryScalar(array(':type' => $type, ':store' => $store));
        if ($total === false) {
            $total = 0;
        } else {
            $total++;
        }
        return $total;
    }

    static function get_next_trans_saldo_awal()
    {
        $db = GlTrans::model()->getDbConnection();
        $total = $db->createCommand(
            "SELECT MAX(type_no)
FROM mt_gl_trans WHERE type=" . SALDO_AWAL)->queryScalar();
        return $total == null ? 0 : $total + 1;
    }

    static function get_ledger_trans($from, $to)
    {
        $rows = Yii::app()->db->createCommand(
            "SELECT
            mt_gl_trans.tran_date,
            mt_gl_trans.type,
            mt_gl_trans.type_no,
            refs.reference,
            SUM(IF(mt_gl_trans.amount>0, mt_gl_trans.amount,0)) as amount,
            users.user_id
            FROM
            mt_gl_trans
            LEFT JOIN mt_refs as refs ON
            (mt_gl_trans.type=refs.type AND mt_gl_trans.type_no=refs.type_no),users
            WHERE mt_gl_trans.tran_date BETWEEN '$from' AND '$to'
            GROUP BY mt_gl_trans.tran_date,mt_gl_trans.type,
            mt_gl_trans.type_no,mt_gl_trans.users_id
            ")->queryAll();
        return $rows;
    }

    static function get_general_ledger_trans($from, $to)
    {
        $rows = Yii::app()->db->createCommand(
            "SELECT
            mt_gl_trans.type,
            mt_gl_trans.type_no,
            mt_gl_trans.tran_date,
            CONCAT(mt_chart_master.account_code,' ',mt_chart_master.account_name) as account,
            mt_gl_trans.amount
            FROM
            mt_gl_trans
            INNER JOIN mt_chart_master ON mt_gl_trans.account = mt_chart_master.account_code
            WHERE mt_gl_trans.tran_date BETWEEN '$from' AND '$to'
            ")->queryAll();
        return $rows;
    }

    static function get_bank_trans_view()
    {
        global $systypes_array;
        $bfw = U::get_balance_before_for_bank_account($_POST['trans_date_mulai'], $_POST['bank_act']);
        $arr['data'][] = array(
            'type' => 'Saldo Awal - ' . sql2date($_POST['trans_date_mulai']),
            'ref' => '',
            'tgl' => '',
            'debit' => $bfw >= 0 ? number_format($bfw, 2) : '',
            'kredit' => $bfw < 0 ? number_format($bfw, 2) : '',
            'neraca' => '',
            'person' => ''
        );
        $credit = $debit = 0;
        $running_total = $bfw;
        if ($bfw > 0) {
            $debit += $bfw;
        } else {
            $credit += $bfw;
        }
        $result = U::get_bank_trans_for_bank_account($_POST['bank_act'], $_POST['trans_date_mulai'], $_POST['trans_date_sampai']);
        foreach ($result as $myrow) {
            $running_total += $myrow->amount;
            $jemaat = get_jemaat_from_user_id($myrow->users_id);
            $arr['data'][] = array(
                'type' => $systypes_array[$myrow->type],
                'ref' => $myrow->ref,
                'tgl' => sql2date($myrow->trans_date),
                'debit' => $myrow->amount >= 0 ? number_format($myrow->amount, 2) : '',
                'kredit' => $myrow->amount < 0 ? number_format(-$myrow->amount, 2) : '',
                'neraca' => number_format($running_total, 2),
                'person' => $jemaat->real_name
            );
            if ($myrow->amount > 0) {
                $debit += $myrow->amount;
            } else {
                $credit += $myrow->amount;
            }
        }
        $arr['data'][] = array(
            'type' => 'Saldo Akhir - ' . sql2date($_POST['trans_date_sampai']),
            'ref' => '',
            'tgl' => '',
            'debit' => $running_total >= 0 ? number_format($running_total, 2) : '',
            'kredit' => $running_total < 0 ? number_format(-$running_total, 2) : '',
            'neraca' => '', // number_format($debit + $credit, 2),
            'person' => ''
        );
        return $arr;
    }

    static function get_balance_before_for_bank_account($from, $bank_account, $store = "")
    {
        $where = '';
        $param = array(':tgl' => $from, ':bank_id' => $bank_account);
        if ($store != null) {
            $where = "AND nbt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT
        IFNULL(Sum(nbt.amount),0) AS total
        FROM nscc_bank_trans nbt
        WHERE nbt.visible =1 AND DATE(nbt.tgl) < :tgl AND nbt.bank_id = :bank_id $where");
        return $comm->queryScalar($param);
    }

    static function get_bank_trans_for_bank_account($bank_account, $from, $to)
    {
        $criteria = new CDbCriteria();
        if ($bank_account != null) {
            $criteria->addCondition("bank_act =" . $bank_account);
        }
        $criteria->addBetweenCondition("trans_date", $from, $to);
        $criteria->order = "trans_date, id";
        return BankTrans::model()->findAll($criteria);
    }

    static function get_prefs($name)
    {
        $criteria = new CDbCriteria();
        if ($name != null) {
            $criteria->addCondition("name ='$name'");
        } else {
            return null;
        }
        $prefs = SysPrefs::model()->find($criteria);
        return $prefs->value;
    }

    static function get_act_code_from_bank_act($bank_act)
    {
        $bank = Bank::model()->findByPk($bank_act);
        if ($bank != null) {
            return $bank->account_code;
        } else {
            return false;
        }
    }

    static function get_sql_for_journal_inquiry($from, $to)
    {
        $rows = Yii::app()->db->createCommand()->select(
            "gl_trans.tran_date,gl_trans.type,refs.reference,Sum(IF(amount>0, amount,0)) AS amount,
    comments.memo_,gl_trans.person_id,gl_trans.type_no")->from('gl_trans')->join(
            'comments', 'gl_trans.type = comments.type AND gl_trans.type_no = comments.type_no')->Join(
            'refs', 'gl_trans.type = refs.type AND gl_trans.type_no = refs.type_no')->where(
            "gl_trans.amount!=0 and gl_trans.tran_date >= '$from'
?  ?          AND gl_trans.tran_date <= '$to'")->group('gl_trans.type, gl_trans.type_no')->order(
            'tran_date desc')->queryAll();
        return $rows;
    }

    static function add_gl(
        $type, $trans_id, $date_, $ref, $account, $memo_, $comment_, $amount, $cf, $store = null
    )
    {
        $person_id = Yii::app()->user->getId();
        $is_bank_to = self::is_bank_account($account);
        self::add_gl_trans($type, $trans_id, $date_, $account, $memo_, $amount, $person_id, $cf, $store);
        if ($is_bank_to) {
            $bank = Bank::model()->find("account_code = :account_code", array(":account_code" => $account));
            if ($bank == null) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bank transaction')));
            }
            self::add_bank_trans($type, $trans_id, $bank->bank_id, $ref, $date_, $amount, $person_id, $store);
        }
        if (strlen($comment_) > 0) {
            self::add_comments($type, $trans_id, $date_, $comment_);
        }
        // return $trans_id;
    }

    // --------------------------------------------- Gl Trans ---------------------------------------------------------------
    static function is_bank_account($account_code)
    {
//        $criteria = new CDbCriteria();
//        $criteria->addCondition("account_code = :account_code");
//        $criteria->addCondition("kategori = :kas");
//        $criteria->addCondition("kategori = :bank", 'OR');
//        $criteria->params = array(
//            ":account_code" => $account_code,
//            ':kas' => SysPrefs::get_val('coa_grup_kas'),
//            ':bank' => SysPrefs::get_val('coa_grup_bank')
//        );
        $comm = Yii::app()->db->createCommand("SELECT account_code FROM nscc_chart_master WHERE
        account_code = :account_code AND (kategori = :kas OR kategori = :bank)");
        $bank_act = $comm->queryAll(true, array(
            ":account_code" => $account_code,
            ':kas' => COA_GRUP_KAS,
            ':bank' => COA_GRUP_BANK
        ));
        return count($bank_act) > 0;
    }

    static function add_gl_trans($type, $trans_id, $date_, $account, $memo_, $amount, $person_id, $cf, $store = null)
    {
        $gl_trans = new GlTrans();
        $gl_trans->type = $type;
        $gl_trans->type_no = $trans_id;
        $gl_trans->tran_date = $date_;
        $gl_trans->account_code = $account;
        $gl_trans->memo_ = $memo_;
        $gl_trans->id_user = $person_id;
        $gl_trans->amount = $amount;
        $gl_trans->cf = $cf;
        $gl_trans->store = $store;
        if (!$gl_trans->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'General Ledger')) . CHtml::errorSummary($gl_trans));
        }/* else{
          if(PUSH_PUSAT){
          U::runCommand('gltrans', '--id=' . $gl_trans->counter, 'gl_'.$gl_trans->counter.'.log');
          } else {
          if (PUSH_PUSAT) {
          U::runCommand('gltrans', '--id=' . $gl_trans->counter, 'gl_' . $gl_trans->counter . '.log');
          }
          } */
    }

    static function add_bank_trans(
        $type, $trans_no, $bank_act, $ref, $date_, $amount, $person_id, $store = null
    )
    {
        $bank_trans = new BankTrans;
        $bank_trans->type_ = $type;
        $bank_trans->trans_no = $trans_no;
        $bank_trans->bank_id = $bank_act;
        $bank_trans->ref = $ref;
        $bank_trans->tgl = $date_;
        $bank_trans->amount = $amount;
        $bank_trans->id_user = $person_id;
        $bank_trans->store = $store;
        if (!$bank_trans->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bank transaction')) . CHtml::errorSummary($bank_trans));
        }/* else{
          if(PUSH_PUSAT){
          U::runCommand('banktrans', '--id=' . $bank_trans->bank_trans_id,  'banktrans_'.$bank_trans->bank_trans_id.'.log');
          }
          } */
    }

    static function add_comments($type, $type_no, $date_, $memo_)
    {
        if ($memo_ != null && $memo_ != "") {
            $comment = new Comments();
            $comment->type = $type;
            $comment->type_no = $type_no;
            $comment->date_ = $date_;
            $comment->memo_ = $memo_;
            if (!$comment->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Comments')) . CHtml::errorSummary($comment));
            }
        }
    }

    static function add_stock_moves(
        $type, $trans_no, $tran_date, $barang_id, $qty, $reference, $price, $store
    )
    {
        $move = new StockMoves;
        $move->type_no = $type;
        $move->trans_no = $trans_no;
        $move->tran_date = $tran_date;
        $move->price = $price;
        $move->reference = $reference;
        $move->qty = $qty;
//        $move->discount_percent = $discount_percent;
        $move->store = $store;
        $move->barang_id = $barang_id;
        $move->visible = 1;
//        $move->gudang_id = $gudang_id;
        if (!$move->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Stock moves')) . CHtml::errorSummary($move));
        }
        return $move->stock_moves_id;
    }

    static function add_stock_moves_perlengkapan(
        $type, $trans_no, $tran_date, $barang_id, $qty, $reference, $price, $store
    )
    {
        $move = new StockMovesPerlengkapan;
        $move->type_no = $type;
        $move->trans_no = $trans_no;
        $move->tran_date = $tran_date;
        $move->price = $price;
        $move->reference = $reference;
        $move->qty = $qty;
//        $move->discount_percent = $discount_percent;
        $move->store = $store;
        $move->barang_id = $barang_id;
//        $move->gudang_id = $gudang_id;
        if (!$move->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Stock moves')) . CHtml::errorSummary($move));
        }
    }

    static function add_stock_moves_all(
        $stock_moves_id, $type, $trans_no, $tran_date, $barang_id, $qty, $reference, $price = 0, $store = STOREID
    )
    {
        $barang = Barang::model()->findByPk($barang_id);
        $move = null;
        switch ($barang->tipe_barang_id) {
            case TIPE_FINISH_GOODS:
            case TIPE_RAW_MATERIAL:
                $move = new StockMoves;
                break;
            case TIPE_PERLENGKAPAN:
                $move = new StockMovesPerlengkapan;
                break;
        }
        $move->stock_moves_id = $stock_moves_id ? $stock_moves_id : null;
        $move->type_no = $type;
        $move->trans_no = $trans_no;
        $move->tran_date = $tran_date;
        $move->price = $price;
        $move->reference = $reference;
        $move->qty = $qty;
        $move->store = $store;
        $move->barang_id = $barang_id;
        $move->visible = 1;
        if (!$move->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Stock moves')) . CHtml::errorSummary($move));
        }
        return $move->stock_moves_id;
    }

    static function delete_stock_moves_all($type, $type_no)
    {
        StockMoves::model()
            ->updateAll(array('visible' => 0, 'up' => 0), 'type_no = :type AND trans_no = :type_no', array(':type' => $type, ':type_no' => $type_no));
        StockMovesPerlengkapan::model()
            ->updateAll(array('visible' => 0, 'up' => 0), 'type_no = :type AND trans_no = :type_no', array(':type' => $type, ':type_no' => $type_no));
    }

    // --------------------------------------------- Comments ---------------------------------------------------------------
    static function get_comments($type, $type_no)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type=" . $type);
        $criteria->addCondition("id=" . $type_no);
        return Comments::model()->find($criteria);
    }

    static function update_comments($type, $id, $date_, $memo_)
    {
        if ($date_ == null) {
            U::delete_comments($type, $id);
            U::add_comments($type, $id, Yii::app()->dateFormatter->format('yyyy-MM-dd', time()), $memo_);
        } else {
            $criteria = new CDbCriteria();
            $criteria->addCondition("type=" . $type);
            $criteria->addCondition("id=" . $id);
            $criteria->addCondition("date_=" . $date_);
            $comment = Comments::model()->find($criteria);
            $comment->memo_ = $memo_;
            $comment->save();
        }
    }

    static function delete_comments($type, $type_no)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type=" . $type);
        $criteria->addCondition("id=" . $type_no);
        $comment = Comments::model()->find($criteria);
        $comment->delete();
    }

    // ---------------------------------------------- Report ----------------------------------------------------------------
    static function get_beban()
    {
        $rows = app()->db->createCommand("SELECT account_code FROM mt_chart_master WHERE account_code REGEXP '^5[1-9]'")->queryAll();
        return $rows;
    }

    static function get_daftar_master_konsumen($code, $nama, $phone, $hp, $hp2, $tempo, $status)
    {
        $query = app()->db->createCommand();
        $query->select("CONCAT(pk.konsumen_code,' ') `Kode Konsumen`, pk.konsumen_name `Nama Konsumen`,
        pk.phone Phone, pk.hp HP, pk.hp2 `HP 2`, pk.tempo Tempo, pk.address Alamat, pk.status `Status`");
        $query->from("{{konsumen}} pk");
        if ($code !== "") {
            $query->andWhere("konsumen_code like :konsumen_code", array(":konsumen_code" => $code . "%"));
        }
        if ($nama !== "") {
            $query->andWhere("konsumen_name like :konsumen_name", array(":konsumen_name" => "%" . $nama . "%"));
        }
        if ($phone !== "") {
            $query->andWhere("phone like :phone", array(":phone" => "%" . $phone . "%"));
        }
        if ($hp !== "") {
            $query->andWhere("hp like :hp", array(":hp" => "%" . $hp . "%"));
        }
        if ($hp2 !== "") {
            $query->andWhere("hp2 like :hp2", array(":hp2" => "%" . $hp2 . "%"));
        }
        if ($tempo !== "") {
            $query->andWhere("tempo like :tempo", array(":tempo" => "%" . $tempo . "%"));
        }
        if ($status !== "") {
            $query->andWhere("status like :status", array(":status" => "%" . $status . "%"));
        }
        return $query->queryAll(true);
    }

    static function get_arr_kode_rekening_pengeluaran($code = "")
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_type='" . Prefs::TypeCostAct() . "'");
        if ($code != "account_code" && $code != "") {
            $criteria->addCondition("account_code='$code'");
        }
        $model = ChartMaster::model()->findAll($criteria);
        $daftar = array();
        foreach ($model as $coderek) {
            $daftar[$coderek['account_code']] = $coderek['account_name'];
        }
        return $daftar;
    }

    static function get_pengeluaran_detil_kode_rekening(
        $start_date, $end_date, $code
    )
    {
        $rows = Yii::app()->db->createCommand()->select(
            "a.tran_date,a.memo_,IF(a.amount > 0,a.amount,'') as debit,IF(a.amount < 0,-a.amount,'') as kredit")->from(
            "mt_gl_trans a")->rightJoin("mt_chart_master b", "a.account=b.account_code
    AND a.tran_date between :start and :end", array(
            ':start' => $start_date,
            ':end' => $end_date
        ))->leftJoin('mt_voided c', "a.type_no=c.id AND c.type=a.type")->where(
            "b.account_code=:code and a.type != :type and ISNULL(c.date_)", array(
            'code' => $code,
            'type' => VOID
        ))->order("a.tran_date")->queryAll();
        // ->where("b.account_code=:code",array('code'=>$code))
        return $rows;
    }

    static function get_pengeluaran_per_kode_rekening($start_date, $end_date)
    {
        $rows = Yii::app()->db->createCommand()->select(
            "b.account_code,b.account_name as nama_rekening,IFNULL(sum(a.amount),0) as total_beban")->from(
            "mt_gl_trans a")->rightJoin("mt_chart_master b", "a.account=b.account_code
    AND a.tran_date between :start and :end", array(
            ':start' => $start_date,
            ':end' => $end_date
        ))->where("b.account_type=:type and !b.inactive", array(
            ':type' => Prefs::TypeCostAct()
        ))->group("b.account_name")->order("b.account_code")->queryAll();
        return $rows;
    }

    static function get_total_pengeluaran($start_date, $end_date, $code = "")
    {
        $kode = $code == "" ? "" : "and b.account_code = '$code'";
        $rows = Yii::app()->db->createCommand()->select("sum(a.amount) as total_beban")->from(
            "mt_gl_trans a")->join("mt_chart_master b", "a.account=b.account_code")->where(
            "a.tran_date between :start and :end and b.account_type=:type $kode", array(
            ':start' => $start_date,
            ':end' => $end_date,
            ':type' => Prefs::TypeCostAct()
        ))->queryScalar();
        return $rows == null ? 0 : $rows;
    }

    static function get_detil_pendapatan($start_date, $end_date)
    {
        $rows = Yii::app()->db->createCommand()->select(
            "b.account_name as nama_rekening,IFNULL(-sum(a.amount),0) as total_pendapatan")->from(
            "mt_gl_trans a")->rightJoin("mt_chart_master b", "a.account=b.account_code and
        a.tran_date between :start and :end", array(
            ':start' => $start_date,
            ':end' => $end_date
        ))->where("b.account_type=:type and !b.inactive", array(
            ':type' => Prefs::TypePendapatanAct()
        ))->group("b.account_name")->order("b.account_code")->queryAll();
        return $rows;
    }

    static function get_total_pendapatan($start_date, $end_date)
    {
        $rows = Yii::app()->db->createCommand()->select("-sum(a.amount) as total_pendapatan")->from(
            "mt_gl_trans a")->join("mt_chart_master b", "a.account=b.account_code")->where(
            "a.tran_date between :start and :end and b.account_type=:type", array(
            ':start' => $start_date,
            ':end' => $end_date,
            ':type' => Prefs::TypePendapatanAct()
        ))->order("b.account_code")->queryScalar();
        return $rows == null ? 0 : $rows;
    }

    static function get_chart_master_beban()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_type = " . Prefs::TypeCostAct());
        return ChartMaster::model()->findAll($criteria);
    }

    static function account_in_gl_trans($account)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_code = :account_code");
        $criteria->params = array(':account_code' => $account);
        $count = GlTrans::model()->count($criteria);
        return $count > 0;
    }

    static function account_used_bank($account)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_code = :account_code");
        $criteria->params = array(':account_code' => $account);
        $count = Bank::model()->count($criteria);
        return $count > 0;
    }

    static function account_used_supplier($account)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("account_code = :account_code");
        $criteria->params = array(':account_code' => $account);
        $count = Supplier::model()->count($criteria);
        return $count > 0;
    }

    static function report_new_customers($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND nc.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nc.nama_customer,nc.no_customer,nc.tempat_lahir,nc.tgl_lahir,nc.email,nc.telp,
        nc.alamat,nc.awal,nc.akhir,nc.store,nc.customer_id,nk.nama_kecamatan,
        nko.nama_kota,np.nama_provinsi,nn.nama_negara,nsc.nama_status,ni.info_name
        FROM nscc_customers AS nc
        LEFT JOIN nscc_kecamatan AS nk ON nc.kecamatan_id = nk.kecamatan_id
        LEFT JOIN nscc_kota AS nko ON nk.kota_id = nko.kota_id
        LEFT JOIN nscc_provinsi AS np ON nko.provinsi_id = np.provinsi_id
        LEFT JOIN nscc_negara AS nn ON np.negara_id = nn.negara_id
        LEFT JOIN nscc_status_cust AS nsc ON nc.status_cust_id = nsc.status_cust_id
        LEFT JOIN nscc_info AS ni ON ni.info_id = nc.info_id
        WHERE DATE(nc.awal) >= :from AND DATE(nc.awal) <= :to $where
        ORDER BY nc.awal");
        return $comm->queryAll(true, $param);
    }

    static function report_real_customers($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nc.customer_id,nc.nama_customer,nc.no_customer,nc.tempat_lahir,nc.tgl_lahir,nc.email,nc.telp,
        nc.alamat,nc.awal,nc.akhir,nc.store,nc.customer_id,nk.nama_kecamatan,
        nko.nama_kota,np.nama_provinsi,nn.nama_negara,nsc.nama_status
        FROM nscc_customers AS nc
        INNER JOIN nscc_salestrans AS ns ON ns.customer_id = nc.customer_id
        LEFT JOIN nscc_kecamatan AS nk ON nc.kecamatan_id = nk.kecamatan_id
        LEFT JOIN nscc_kota AS nko ON nk.kota_id = nko.kota_id
        LEFT JOIN nscc_provinsi AS np ON nko.provinsi_id = np.provinsi_id
        LEFT JOIN nscc_negara AS nn ON np.negara_id = nn.negara_id
        LEFT JOIN nscc_status_cust AS nsc ON nc.status_cust_id = nsc.status_cust_id
        WHERE ns.tgl >= :from AND ns.tgl <= :to $where
         GROUP BY nc.customer_id, ns.tgl
        ORDER BY nc.awal");
        return $comm->queryAll(true, $param);
    }

    static function report_kategori_customer($kategori, $store = "")
    {
        $on = $having = $where = "";
        if ($store != null) {
            $on = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        switch ($kategori) {
            case 1 :
                $where = "WHERE a.jml_trans > 0";
                $having = "HAVING jml_bulan = 3";
                break;
            case 2 :
                $where = "WHERE a.jml_trans > 0";
                $having = "HAVING jml_bulan = 2";
                break;
            case 3 :
                $where = "WHERE a.jml_trans > 0";
                $having = "HAVING jml_bulan = 1";
                break;
            case 4 :
                $where = "WHERE a.jml_trans = 0";
                break;
        }
        //HAVING jml_bulan = 2
        $comm = Yii::app()->db->createCommand("
        SELECT a.*,COUNT(a.no_customer) jml_bulan FROM (SELECT
        nc.nama_customer,nc.no_customer,nc.tempat_lahir,nc.tgl_lahir,nc.email,nc.telp,
        nc.alamat,nc.awal,nc.akhir,nc.store,nc.customer_id,COUNT(ns.tgl) jml_trans
        FROM nscc_salestrans AS ns
        RIGHT JOIN nscc_customers AS nc ON ns.customer_id = nc.customer_id AND
	      ns.tgl >= (CURDATE() - INTERVAL 3 MONTH) AND ns.type_ = 1 $on
        GROUP BY nc.customer_id,YEAR(ns.tgl), MONTH(ns.tgl)) a
        $where
        GROUP BY a.no_customer $having");
        return $comm->queryAll(true, $param);
    }

    static function report_birthday_customers($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND nc.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nc.nama_customer,nc.no_customer,nc.tempat_lahir,nc.tgl_lahir,nc.email,nc.telp,
        nc.alamat,nc.awal,nc.akhir,nc.store,nc.customer_id,nk.nama_kecamatan,
        nko.nama_kota,np.nama_provinsi,nn.nama_negara,nsc.nama_status
        FROM nscc_customers AS nc
        LEFT JOIN nscc_kecamatan AS nk ON nc.kecamatan_id = nk.kecamatan_id
        LEFT JOIN nscc_kota AS nko ON nk.kota_id = nko.kota_id
        LEFT JOIN nscc_provinsi AS np ON nko.provinsi_id = np.provinsi_id
        LEFT JOIN nscc_negara AS nn ON np.negara_id = nn.negara_id
        LEFT JOIN nscc_status_cust AS nsc ON nc.status_cust_id = nsc.status_cust_id
        WHERE DAYOFYEAR(:from) <= DAYOFYEAR(nc.tgl_lahir) AND DAYOFYEAR(:to) >= DAYOFYEAR(nc.tgl_lahir) $where
        ORDER BY DAYOFYEAR(nc.tgl_lahir)");
        return $comm->queryAll(true, $param);
    }

    static function report_biaya($from, $to)
    {
        $comm = Yii::app()->db->createCommand("SELECT nk.tgl, nk.doc_ref, nk.no_kwitansi,
            nk.keperluan,-nk.total total
            FROM nscc_kas AS nk
            WHERE nk.total < 0 AND DATE(tgl) >= :FROM AND DATE(tgl) <= :TO");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }

    static function report_kartu_stok($kode_barang, $from, $to, $store = "")
    {
        $where = "";
        $param = array(
            ':kode_barang' => $kode_barang,
            ':from' => $from,
            ':to' => $to
        );
        if ($store != null) {
            $where = "AND nsm.store = :store";
            $param[':store'] = $store;
        }

        $sort = "ORDER BY nsm.tdate";
        if (NATASHA_CUSTOM)
            $sort = "ORDER BY nsm.tran_date ASC";

        $comm = Yii::app()->db->createCommand("SELECT DATE(nsm.tran_date) tgl,nsm.reference doc_ref,
            0 `before`,IF(nsm.qty >0,nsm.qty,0) `in`,IF(nsm.qty <0,-nsm.qty,0) `out`,
            0 `after`,nsm.qty,nsm.price
            FROM {{stock_moves}} AS nsm
            INNER JOIN nscc_barang nb ON nsm.barang_id = nb.barang_id
            WHERE DATE(nsm.tran_date) >= :from AND DATE(nsm.tran_date) <= :to
            AND nb.kode_barang = :kode_barang AND nsm.visible = 1 $where
            $sort");
        return $comm->queryAll(true, $param);
    }

    static function report_kartu_stok_perlengkapan($kode_barang, $from, $to, $store = "")
    {
        $where = "";
        $param = array(
            ':kode_barang' => $kode_barang,
            ':from' => $from,
            ':to' => $to
        );
        if ($store != null) {
            $where = "AND nsm.store = :store";
            $param[':store'] = $store;
        }

        $sort = "ORDER BY nsm.tdate";
        if (NATASHA_CUSTOM)
            $sort = "ORDER BY nsm.tran_date ASC";

        $comm = Yii::app()->db->createCommand("SELECT DATE(nsm.tran_date) tgl,nsm.reference doc_ref,
            0 `before`,IF(nsm.qty >0,nsm.qty,0) `in`,IF(nsm.qty <0,-nsm.qty,0) `out`,
            0 `after`,nsm.qty,nsm.price
            FROM {{stock_moves_perlengkapan}} AS nsm
            INNER JOIN nscc_barang nb ON nsm.barang_id = nb.barang_id
            WHERE DATE(nsm.tran_date) >= :from AND DATE(nsm.tran_date) <= :to
            AND nb.kode_barang = :kode_barang AND nsm.visible = 1 $where
            $sort");
        return $comm->queryAll(true, $param);
    }

    static function report_mutasi_stok($from, $to, $store = "", $grup_id = "", $tag_id = "")
    {
        $on = "";
        $where = "";
        $harga_beli_select = " 0 AS price,";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $on .= " AND nsm.store = :store";
            $param[':store'] = $store;
            $harga_beli_select = " (SELECT nbl.price FROM nscc_beli AS nbl WHERE nbl.barang_id = nb.barang_id  AND nbl.store = 'KDR01') AS price,";
        }
        if ($grup_id) {
            $where .= " AND nb.grup_id = :grup_id";
            $param[':grup_id'] = $grup_id;
        }
        if ($tag_id) {
            $where .= " AND tb.tag_id = :tag_id";
            $param[':tag_id'] = $tag_id;
        }
        $sorting = "ORDER BY ng.nama_grup, nb.kode_barang";
        if (NATASHA_CUSTOM)
            $sorting = "ORDER BY nb.kode_barang";
        $id = Yii::app()->user->getId();
        $user = Users::model()->findByPk($id);
        $show_hpp_join = "";
        $show_hpp_select = "price,";
        if ($user->is_available_role(351)) {
            $show_hpp_select = " max(if(t2.tdate = nsm.tdate, nsm.price, 0)) price, ";
            $show_hpp_join = "
            left join (
                SELECT barang_id, max(tdate) tdate
                FROM nscc_stock_moves
                WHERE
                    store = :store
		            AND DATE(tran_date) <= :to
		        GROUP BY barang_id
            ) t2 on t2.barang_id = nsm.barang_id ";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT 
            $harga_beli_select
            nb.kode_barang,
            nb.nama_barang,
            $show_hpp_select
            SUM(IF (DATE(nsm.tran_date) < :from, nsm.qty, 0)) `before`,
            SUM(IF (nsm.qty > 0 AND nsm.type_no = " . SUPPIN . " AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) Purchase,
            SUM(IF (nsm.qty > 0 AND nsm.type_no = " . RETURJUAL . " AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) ReturnSales,
            SUM(IF (nsm.qty < 0 AND nsm.type_no = " . SUPPOUT . " AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) ReturnPurchase,
            SUM(IF (nsm.qty < 0 AND nsm.type_no = " . PENJUALAN . " AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) Sales,
		    ABS(SUM(IF (nsm.qty != 0 AND (nsm.type_no = " . PENJUALAN . " OR nsm.type_no = " . RETURJUAL . ") 
		    AND DATE(nsm.tran_date) >= :from AND DATE(nsm.tran_date) <= :to, nsm.qty, 0))) RealSales,
            SUM(IF (nsm.qty > 0 AND nsm.type_no IN (" . ITEM_IN . "," . RECEIVE_DROPPING . "," . DROPPING_RECALL . "," . PRODUKSI . "," . RETURN_PRODUKSI . ") AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) BarangMasuk,
            SUM(IF (nsm.qty < 0 AND nsm.type_no IN (" . ITEM_OUT . "," . DROPPING . "," . PRODUKSI . "," . RETURN_PRODUKSI . ") AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) BarangKeluar,
            SUM(IF (DATE(nsm.tran_date) <= :to, nsm.qty, 0)) `after`
        FROM nscc_barang AS nb 
            LEFT  JOIN nscc_stock_moves AS nsm ON (nsm.barang_id = nb.barang_id  $on)
            $show_hpp_join
            LEFT JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
            LEFT JOIN nscc_kategori AS nk ON ng.kategori_id = nk.kategori_id
            LEFT JOIN nscc_tag_barang AS tb ON tb.barang_id = nb.barang_id
        WHERE nk.have_stock != 0 AND nsm.visible = 1 $where
        GROUP BY nb.kode_barang, nb.nama_barang
        $sorting");
        return $comm->queryAll(true, $param);
    }

    static function report_mutasi_stok_perlengkapan($from, $to, $store = "", $grup_id = "", $tag_id = "")
    {
        $on = "";
        $where = "";
        $harga_beli_select = " 0 AS price,";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $on .= " AND nsm.store = :store";
            $param[':store'] = $store;
            $harga_beli_select = " (SELECT nbl.price FROM nscc_beli AS nbl WHERE nbl.barang_id = nb.barang_id  AND nbl.store = 'KDR01') AS price,";
        }
        if ($grup_id) {
            $where .= " AND nb.grup_id = :grup_id";
            $param[':grup_id'] = $grup_id;
        }
        if ($tag_id) {
            $where .= " AND tb.tag_id = :tag_id";
            $param[':tag_id'] = $tag_id;
        }
        $sorting = "ORDER BY ng.nama_grup, nb.kode_barang";
        if (NATASHA_CUSTOM)
            $sorting = "ORDER BY nb.kode_barang";
        $id = Yii::app()->user->getId();
        $user = Users::model()->findByPk($id);
        $show_hpp_join = "";
        $show_hpp_select = "price,";
        if ($user->is_available_role(351)) {
            $show_hpp_select = " max(if(t2.tdate = nsm.tdate, nsm.price, 0)) price, ";
            $show_hpp_join = "
            left join (
                SELECT barang_id, max(tdate) tdate
                FROM nscc_stock_moves_perlengkapan
                WHERE
                    store = :store
		            AND DATE(tran_date) <= :to
		        GROUP BY barang_id
            ) t2 on t2.barang_id = nsm.barang_id ";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT 
            $harga_beli_select
            nb.kode_barang,
            nb.nama_barang,
            $show_hpp_select
            SUM(IF (DATE(nsm.tran_date) < :from, nsm.qty, 0)) `before`,
            SUM(IF (nsm.qty > 0 AND nsm.type_no = " . SUPPIN . " AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) Purchase,
            SUM(IF (nsm.qty > 0 AND nsm.type_no = " . RETURJUAL . " AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) ReturnSales,
            SUM(IF (nsm.qty < 0 AND nsm.type_no = " . SUPPOUT . " AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) ReturnPurchase,
            SUM(IF (nsm.qty < 0 AND nsm.type_no = " . PENJUALAN . " AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) Sales,
            SUM(IF (nsm.qty > 0 AND nsm.type_no IN (" . ITEM_IN . "," . RECEIVE_DROPPING . ") AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) BarangMasuk,
            SUM(IF (nsm.qty < 0 AND nsm.type_no IN (" . ITEM_OUT . "," . DROPPING . ") AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) BarangKeluar,
            SUM(IF (DATE(nsm.tran_date) <= :to, nsm.qty, 0)) `after`
        FROM nscc_barang AS nb 
            LEFT  JOIN nscc_stock_moves_perlengkapan AS nsm ON (nsm.barang_id = nb.barang_id  $on)
            $show_hpp_join
            LEFT JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
            LEFT JOIN nscc_kategori AS nk ON ng.kategori_id = nk.kategori_id
            LEFT JOIN nscc_tag_barang AS tb ON tb.barang_id = nb.barang_id
        WHERE nk.have_stock != 0 AND nsm.visible = 1 $where
        GROUP BY nb.kode_barang, nb.nama_barang
        $sorting");
        return $comm->queryAll(true, $param);
    }

    static function report_rekap_perawatan($from, $to, $grup_id = null, $store = "")
    {
        $where = "";
        $on = "";
        $param = array(':from_' => $from, ':to_' => $to);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        if ($grup_id != null) {
            $on .= "WHERE ng.grup_id = :grup_id";
            $param[':grup_id'] = $grup_id;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nb.kode_barang,nb.nama_barang,
        SUM(IF (ns.tgl >= :from_ AND ns.tgl <= :to_, nsd.qty, 0)) AS qty,
        SUM(IF (ns.tgl >= :from_ AND ns.tgl <= :to_, nsd.total, 0)) AS total
        FROM nscc_barang AS nb
        LEFT JOIN nscc_salestrans_details AS nsd ON nsd.barang_id = nb.barang_id
        LEFT JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id $where
        LEFT JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
        $on
        GROUP BY nsd.barang_id,nb.kode_barang,nb.nama_barang
        ORDER BY ng.nama_grup,nb.kode_barang");
        return $comm->queryAll(true, $param);
    }

    static function report_mutasi_stok_clinical($from, $to, $kategori_clinical_id, $store = "")
    {
        $where = "";
        $group = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND nsm.store = :store";
            $param[':store'] = $store;
        }
        if ($kategori_clinical_id != null) {
            $group = "WHERE nb.kategori_clinical_id = :kategori_clinical_id";
            $param[':kategori_clinical_id'] = $kategori_clinical_id;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nb.kode_barang, nb.nama_barang,
		        SUM(IF (DATE(nsm.tran_date) < :from, nsm.qty, 0)) `BEFORE`,
        SUM(IF (nsm.qty > 0 AND nsm.tipe_clinical_id = 'b388ae3c-d1c9-11e4-96ad-00ffe52bbadb' AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) WH,
        SUM(IF (nsm.qty > 0 AND nsm.tipe_clinical_id = 'bc29d264-d1c9-11e4-96ad-00ffe52bbadb' AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, nsm.qty, 0)) RELOKASI,
		SUM(IF (nsm.qty < 0 AND nsm.tipe_clinical_id = '17eaafe7-d1d7-11e4-96ad-00ffe52bbadb' AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) PENJUALAN,
		SUM(IF (nsm.qty < 0 AND nsm.tipe_clinical_id = '2b9fdd0a-d1d7-11e4-96ad-00ffe52bbadb' AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) RELOKASIOUT,
		SUM(IF (nsm.qty < 0 AND nsm.tipe_clinical_id = '32711c0d-d1d7-11e4-96ad-00ffe52bbadb' AND DATE(nsm.tran_date) >= :from
		  AND DATE(nsm.tran_date) <= :to, ABS(nsm.qty), 0)) RETUR,
        SUM(IF (DATE(nsm.tran_date) <= :to, nsm.qty, 0)) `AFTER`
        FROM nscc_barang_clinical AS nb INNER JOIN nscc_stock_moves_clinical AS nsm ON (nsm.barang_clinical = nb.barang_clinical $where)
        $group
        GROUP BY nb.kode_barang, nb.nama_barang ORDER BY nb.barang_clinical");
        return $comm->queryAll(true, $param);
    }

    static function report_beauty_summary($from, $to, $store = "")
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $param = array(':from' => $from, ':to' => $to, ':kategori_id' => $kategori_id);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT Sum(nsd.qty) AS qty,Sum(nbs.total) AS tip,nbe.kode_beauty,nbe.nama_beauty
        FROM nscc_salestrans_details AS nsd
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_beauty_services AS nbs ON (nbs.salestrans_details = nsd.salestrans_details AND nbs.visible = 1) 
        INNER JOIN nscc_beauty AS nbe ON nbs.beauty_id = nbe.beauty_id
        WHERE ng.kategori_id = :kategori_id AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to
        AND	ns.doc_ref NOT IN (SELECT nsr.doc_ref_sales FROM nscc_salestrans AS nsr WHERE nsr.type_ = -1) and ns.type_ = 1 $where
        GROUP BY nbe.nama_beauty
        ORDER BY nbe.nama_beauty");
        return $comm->queryAll(true, $param);
    }

    static function report_beauty_details($from, $to, $store = "")
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $param = array(':from' => $from, ':to' => $to, ':kategori_id' => $kategori_id);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nb.kode_barang,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,ns.doc_ref,nc.nama_customer,nsd.qty,nbs.total beauty_tip,
        nbe.nama_beauty,nbe.kode_beauty
        FROM nscc_salestrans_details AS nsd
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_customers AS nc ON ns.customer_id = nc.customer_id
        INNER JOIN nscc_beauty_services AS nbs ON (nbs.salestrans_details = nsd.salestrans_details AND nbs.visible = 1)
        INNER JOIN nscc_beauty AS nbe ON nbs.beauty_id = nbe.beauty_id
        WHERE ng.kategori_id = :kategori_id AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to
        AND	ns.doc_ref NOT IN (SELECT nsr.doc_ref_sales FROM nscc_salestrans AS nsr WHERE nsr.type_ = -1) and ns.type_ = 1 $where
        ORDER BY nbe.nama_beauty , ns.doc_ref ");
        return $comm->queryAll(true, $param);
    }

    static function report_sales_summary($nama_grup, $from, $to, $store = "")
    {
        $grup = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($nama_grup != null) {
            $grup = "AND ng.nama_grup = :nama_grup";
            $param[':nama_grup'] = $nama_grup;
        }
        $where = '';
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT ns.doc_ref, SUM(nsd.total + nsd.vatrp) AS total,
  nb.kode_barang, SUM(nsd.qty) AS qty, DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl
FROM nscc_salestrans ns
  INNER JOIN nscc_salestrans_details nsd
    ON nsd.salestrans_id = ns.salestrans_id
  INNER JOIN nscc_barang nb
    ON nsd.barang_id = nb.barang_id
    INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
WHERE nsd.total >= 0 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup $where
GROUP BY ns.doc_ref, nb.kode_barang, ns.tgl");
        return $comm->queryAll(true, $param);
    }

    static function report_sales_summary_receipt($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
  ns.doc_ref,ns.ketdisc, nc.no_customer, nc.nama_customer, ns.total, nb.nama_bank, SUM(np.amount) amount,np.kembali
FROM nscc_salestrans ns
  INNER JOIN nscc_customers nc
    ON ns.customer_id = nc.customer_id
  LEFT JOIN nscc_payment np
    ON np.salestrans_id = ns.salestrans_id
  LEFT JOIN nscc_bank nb
    ON np.bank_id = nb.bank_id
WHERE DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to AND ns.type_ = 1 $where
GROUP BY ns.doc_ref, nb.nama_bank, np.amount");
        return $comm->queryAll(true, $param);
    }

    static function report_sales_summary_receipt_amount_total($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(SUM(ns.total),0) AS total
FROM nscc_salestrans ns
WHERE DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to AND ns.type_ = 1 $where");
        $comm->setFetchMode(PDO::FETCH_OBJ);
        return $comm->queryRow(true, $param);
    }

    static function report_sales_summary_receipt_details($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT DATE_FORMAT(ns.tgl,'%d %b %Y') tgl,
         ns.doc_ref, ns.ketdisc,nc.no_customer, nc.nama_customer, nb.kode_barang, nsd.qty, nsd.price,nsd.bruto,
          nsd.discrp,nsd.vatrp, (nsd.total+nsd.vatrp) total,ns.total total_faktur, nst.nama_status,
          CAST(REPLACE(LEFT(ns.ketdisc,(LOCATE('/',ns.ketdisc)-1)),ns.store,'')as UNSIGNED) seq
FROM nscc_salestrans_details nsd
  LEFT JOIN nscc_salestrans ns
    ON nsd.salestrans_id = ns.salestrans_id
  LEFT JOIN nscc_customers nc
    ON ns.customer_id = nc.customer_id
  LEFT JOIN nscc_status_cust nst
    ON nc.status_cust_id = nst.status_cust_id
  LEFT JOIN nscc_barang nb
    ON nsd.barang_id = nb.barang_id
    WHERE DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to
  AND ns.type_ = 1 $where
  ORDER BY seq,ns.tdate");
        return $comm->queryAll(true, $param);
    }

    static function report_Fee_Referral($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND nrt.store = :store";
            $param[':store'] = $store;
        }

        $comm = Yii::app()->db->createCommand("SELECT nrt.referral_trans, nr.referral_nama, nc.nama_customer , nrt.tdate, ns.doc_ref, nrt.bonus
FROM nscc_referral_trans nrt
LEFT JOIN nscc_customers nc 
ON  nrt.id_user = nc.customer_id 
LEFT JOIN nscc_referral nr
ON nrt.referral_id = nr.referral_id
LEFT JOIN nscc_salestrans ns
ON nrt.sales_id = ns.salestrans_id
WHERE DATE(nrt.tdate) >= :from AND DATE(nrt.tdate) <= :to $where
    ");
        return $comm->queryAll(true, $param);
    }

    static function report_sales_n_return_details($grup_id, $from, $to, $store = "")
    {
        $grup = "";
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($grup_id != null) {
            $grup = "AND nb.grup_id = :grup_id";
            $param[':grup_id'] = $grup_id;
        }
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT DATE_FORMAT(ns.tgl,'%d %b %Y') tgl,
         ns.doc_ref, nc.no_customer, nc.nama_customer, nb.kode_barang, nsd.qty, nsd.price,nsd.bruto,
          nsd.discrp,nsd.vatrp, (nsd.total+nsd.vatrp) total,ns.total total_faktur
FROM nscc_salestrans_details nsd
  LEFT JOIN nscc_salestrans ns
    ON nsd.salestrans_id = ns.salestrans_id
  LEFT JOIN nscc_customers nc
    ON ns.customer_id = nc.customer_id
  LEFT JOIN nscc_barang nb
    ON nsd.barang_id = nb.barang_id
    WHERE DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where $grup
    ");
        return $comm->queryAll(true, $param);
    }

    static function report_omset_kasir($kasir, $from, $to, $store = "")
    {
        $cashier = "";
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($kasir != "ALL Cashier") {
            $cashier = "AND ns.counter= :kasir";
            $param[':kasir'] = $kasir;
        } else {
            $cashier = "";
        }
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT DATE_FORMAT(ns.tgl,'%d %b %Y') tgl,
         ns.doc_ref, nc.no_customer, nc.nama_customer, nb.kode_barang, nsd.qty, nsd.price,nsd.bruto,
          nsd.discrp,nsd.vatrp, (nsd.total+nsd.vatrp) total,ns.total total_faktur
FROM nscc_salestrans_details nsd
  LEFT JOIN nscc_salestrans ns
    ON nsd.salestrans_id = ns.salestrans_id
  LEFT JOIN nscc_customers nc
    ON ns.customer_id = nc.customer_id
  LEFT JOIN nscc_barang nb
    ON nsd.barang_id = nb.barang_id
    WHERE DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where $cashier
    ");
        return $comm->queryAll(true, $param);
    }

    static function report_omset_group($from, $to, $store = "")
    {
        $cmd = new DbCmd('{{salestrans_details}} nsd');
        if ($store != null) {
            $cmd->addCondition("ns.store = '$store'");
        }
        $cmd->addSelect("DATE_FORMAT(ns.tgl,'%d %b %Y') tgl, ng.nama_grup, sum(nsd.qty) qty,sum(nsd.bruto) bruto,
          sum(nsd.discrp) discrp,sum(nsd.vatrp) vatrp, sum(nsd.total+nsd.vatrp) total,sum(nsd.total_pot) total_pot,
          sum(nsd.discrp1) discrp1");
        $cmd->addLeftJoin('{{salestrans}} ns', ' ns.salestrans_id = nsd.salestrans_id ');
        $cmd->addLeftJoin('{{barang}} b', ' b.barang_id = nsd.barang_id ');
        $cmd->addLeftJoin('{{grup}} ng', ' ng.grup_id = b.grup_id ');
        $cmd->addCondition("DATE(ns.tgl) >= '$from' AND DATE(ns.tgl) <= '$to'");
        $cmd->addGroup('ng.grup_id');
        return $cmd->queryAll();
    }

    static function report_transaksi($from, $to, $store = "")
    {
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND s.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand(
            "select s.doc_ref, s.tgl, CONCAT(c.nama_customer,' ', c.no_customer) as pasien ,  b.kode_barang, d.qty from nscc_salestrans as s
				inner join nscc_salestrans_details as d on d.salestrans_id=s.salestrans_id
				left join nscc_salestrans as rt on rt.doc_ref_sales=s.doc_ref
				inner join nscc_customers as c on c.customer_id=s.customer_id
				inner join nscc_barang as b on b.barang_id=d.barang_id
				where s.tgl >= :from and s.tgl <= :to and rt.doc_ref is null and s.type_=1 $where
				order by s.doc_ref, s.tgl");
        return $comm->queryAll(true, $param);
    }

    static function report_total_item($from, $to, $cream = false, $store = "")
    {
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND s.store = :store";
            $param[':store'] = $store;
        }
        $perawatan_id = SysPrefs::get_val('kategori_threatment_id');
        $benang_id = SysPrefs::get_val('kategori_benang_id');
        $where_cream = "AND g.kategori_id NOT IN ($perawatan_id, $benang_id)";
        if ($cream)
            $where_cream = "AND g.kategori_id IN ($perawatan_id, $benang_id)";
        $comm = Yii::app()->db->createCommand("
	    select b.kode_barang, sum(d.qty) as total_qty, sum(d.total) as total_harga   from nscc_salestrans as s
			inner join nscc_salestrans_details as d on s.salestrans_id=d.salestrans_id
			inner join nscc_barang as b on b.barang_id=d.barang_id
			inner join nscc_grup as g on g.grup_id=b.grup_id
			left join nscc_salestrans as rt on rt.doc_ref_sales=s.doc_ref
				where s.tgl >= :from and s.tgl <= :to and rt.doc_ref is null and s.type_=1 $where_cream $where
			group by b.barang_id
			order by b.kode_barang");
        return $comm->queryAll(true, $param);
    }

    static function report_total_transaksi($from, $to, $store = "")
    {
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND s.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand(
            "select s.doc_ref, s.tgl, CONCAT(c.nama_customer,' ', c.no_customer) as pasien, b.nama_bank, ifnull((p.amount-p.kembali),0) as amount from nscc_salestrans as s
				inner join nscc_customers as c on c.customer_id=s.customer_id
				LEFT join nscc_payment as p on p.salestrans_id=s.salestrans_id
				left join nscc_bank as b on b.bank_id=p.bank_id
				where s.tgl >= :from and s.tgl <= :to $where
				group by p.payment_id
				order by s.doc_ref, s.tgl, b.nama_bank");
        return $comm->queryAll(true, $param);
    }

    static function report_detail_transaksi($from, $to, $store = "")
    {
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND s.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand(
            "select s.doc_ref, s.tgl, CONCAT(c.nama_customer,' ', c.no_customer) as pasien, b.kode_barang, d.qty, d.bruto, d.total_pot, (d.bruto-d.total_pot) as net, d.vatrp, (d.vatrp+(d.bruto-d.total_pot)) as total, s.rounding from nscc_salestrans as s
				inner join nscc_salestrans_details as d on d.salestrans_id=s.salestrans_id
				inner join nscc_customers as c on c.customer_id=s.customer_id
				inner join nscc_barang as b on b.barang_id=d.barang_id
				where s.tgl >= :from and s.tgl <= :to $where
				order by s.tdate,s.doc_ref");
        return $comm->queryAll(true, $param);
    }

    static function report_beauty_detail($from, $to, $store = "")
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND bc.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
			select bc.*, concat(e.kode_employee,'-',e.nama_employee) as beauty
			, concat(dkmr.kode_employee,'-',dkmr.nama_employee) as dokter_kmr
			, concat(dkmt.kode_employee,'-',dkmt.nama_employee) as dokter_kmt
			, bc.nama_customer
			from nscc_beauty_combination as bc
			left join nscc_employees as e on e.employee_id=bc.beauty_id
			left join nscc_employees as dkmr on dkmr.employee_id=bc.kmr
			left join nscc_employees as dkmt on dkmt.employee_id=bc.kmt
			left join nscc_salestrans as rt on rt.doc_ref_sales=bc.doc_ref
			left join nscc_beauty_services as bs on bc.beauty_id=bs.beauty_id and bc.salestrans_details=bs.salestrans_details and bs.visible=1
			WHERE bc.tgl >= :from and bc.tgl <= :to and rt.doc_ref is null $where
			ORDER by bc.doc_ref, bc.qty");
        return $comm->queryAll(true, $param);
    }

    static function report_beauty_totalitem($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND s.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
		select concat(e.kode_employee,'-',e.nama_employee) as beauty, i.kode_barang, count(i.kode_barang) as qty, sum(b.total) as total from nscc_beauty_services as b
			inner join nscc_employees as e on e.employee_id=b.beauty_id
			inner join nscc_salestrans_details as d on d.salestrans_details=b.salestrans_details
			inner join nscc_salestrans as s on s.salestrans_id=d.salestrans_id
			inner join nscc_barang as i on i.barang_id=d.barang_id
			left join nscc_salestrans as rt on rt.doc_ref_sales=s.doc_ref
			where s.tgl >= :from and s.tgl <= :to and s.type_ = 1 and b.visible=1 and rt.doc_ref is null $where
			group by i.kode_barang, e.kode_employee
			order by e.kode_employee, i.kode_barang");
        return $comm->queryAll(true, $param);
    }

    static function report_beauty_totalall($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND s.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
		select concat(e.kode_employee,'-',e.nama_employee) as beauty, sum(b.total) as total from nscc_beauty_services as b
			inner join nscc_employees as e on e.employee_id=b.beauty_id
			inner join nscc_salestrans_details as d on d.salestrans_details=b.salestrans_details
			inner join nscc_salestrans as s on s.salestrans_id=d.salestrans_id
			inner join nscc_barang as i on i.barang_id=d.barang_id
			left join nscc_salestrans as rt on rt.doc_ref_sales=s.doc_ref
			where s.tgl >= :from and s.tgl <= :to and s.type_ = 1 and b.visible=1 and rt.doc_ref is null $where
			group by beauty
			order by beauty");
        return $comm->queryAll(true, $param);
    }

    static function report_kmr_total($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND s.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
		select concat(e.kode_employee,'-',e.nama_employee) as kmr, sum(s.bruto - s.discrp - s.rounding) as total from nscc_salestrans as s
			inner join nscc_employees as e on e.employee_id=s.dokter_id
			left join nscc_salestrans as rt on rt.doc_ref_sales=s.doc_ref
			where s.tgl >= :from and s.tgl <= :to and s.type_ = 1 and rt.doc_ref is null $where
			group by kode_employee
			order by kode_employee");
        return $comm->queryAll(true, $param);
    }

    static function report_kmritem($from, $to, $store = "")
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $param = array(':from' => $from, ':to' => $to, ':kategori_id' => $kategori_id);
        if ($store != null) {
            $where = "AND s.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
		select concat(e.kode_employee,'-',e.nama_employee) as kmr, i.kode_barang, count(i.kode_barang) as qty from nscc_salestrans as s
			inner join nscc_employees as e on e.employee_id=s.dokter_id
			inner join nscc_salestrans_details as d on d.salestrans_id=s.salestrans_id
			inner join nscc_barang as i on i.barang_id=d.barang_id
			inner join nscc_grup as g on g.grup_id=i.grup_id
			left join nscc_salestrans as rt on rt.doc_ref_sales=s.doc_ref
			where s.tgl >= :from and s.tgl <= :to and s.type_ = 1 and g.kategori_id = :kategori_id and rt.doc_ref is null $where
			group by i.kode_barang, e.kode_employee
			order by e.kode_employee, i.kode_barang");
        return $comm->queryAll(true, $param);
    }

    static function report_kmt($from, $to, $store = "")
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $param = array(':from' => $from, ':to' => $to, ':kategori_id' => $kategori_id);
        if ($store != null) {
            $where = "AND s.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
		select concat(e.kode_employee,'-',e.nama_employee) as kmt, i.kode_barang, sum(d.qty) as qty from nscc_salestrans as s
			inner join nscc_salestrans_details as d on d.salestrans_id=s.salestrans_id
			inner join nscc_employees as e on e.employee_id=d.dokter_id
			inner join nscc_barang as i on i.barang_id=d.barang_id
			inner join nscc_grup as g on g.grup_id=i.grup_id
			left join nscc_salestrans as rt on rt.doc_ref_sales=s.doc_ref
			where s.tgl >= :from and s.tgl <= :to and s.type_ = 1 and g.kategori_id = :kategori_id and rt.doc_ref is null $where
			group by i.kode_barang, e.kode_employee
			order by e.kode_employee, i.kode_barang");
        return $comm->queryAll(true, $param);
    }

    static function report_dokter_summary($from, $to, $store = "")
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $param = array(':from' => $from, ':to' => $to, ':kategori_id' => $kategori_id);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        if (!NATASHA_CUSTOM)
            $comm = Yii::app()->db->createCommand("SELECT nd.nama_dokter, nb.kode_barang, nb.nama_barang,
	        SUM(nsd.qty) AS qty, SUM(nsd.jasa_dokter) AS tip, DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl
	        FROM nscc_salestrans_details nsd
	        INNER JOIN nscc_barang nb ON nsd.barang_id = nb.barang_id
	        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
	        INNER JOIN nscc_salestrans ns ON nsd.salestrans_id = ns.salestrans_id
	        INNER JOIN nscc_dokter nd ON nsd.dokter_id = nd.dokter_id
	        WHERE ng.kategori_id = :kategori_id AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where
	        GROUP BY nd.nama_dokter,ns.tgl,nb.kode_barang
	        ORDER BY nd.nama_dokter,nb.kode_barang");
        else
            $comm = Yii::app()->db->createCommand("SELECT nd.nama_dokter, nb.kode_barang, nb.nama_barang,
	        SUM(nsd.qty) AS qty, '0' AS tip, DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl
	        FROM nscc_salestrans_details nsd
	        INNER JOIN nscc_barang nb ON nsd.barang_id = nb.barang_id
	        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
	        INNER JOIN nscc_salestrans ns ON nsd.salestrans_id = ns.salestrans_id
	        INNER JOIN nscc_dokter nd ON nsd.dokter_id = nd.dokter_id
	        WHERE ng.kategori_id = :kategori_id AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where
	        GROUP BY nd.nama_dokter,ns.tgl,nb.kode_barang
	        ORDER BY nd.nama_dokter,nb.kode_barang");
        return $comm->queryAll(true, $param);
    }

    static function report_efektivitas_doctor_service($from, $to, $store = "", $dokterId = "") {
        
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $dokter = "";
        $param = array(':from' => $from, ':to' => $to, ':kategori_id' => $kategori_id);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        if ($dokterId != null) {
            $dokter = "AND nsd.dokter_id = :dokter_id";
            $param[':dokter_id'] = $dokterId;
        }
        $comm = Yii::app()->db->createCommand("SELECT a.nama_dokter, a.kode_barang,a.nama_barang,a.qty,a.tgl, a.price, a.qty*a.price as total_item_rp, a.total FROM
(SELECT nd.nama_dokter, nb.kode_barang, nb.nama_barang,
	        SUM(nsd.qty) AS qty, '0' AS tip, DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl, nsd.price, SUM(nsd.total) as total
	        FROM nscc_salestrans_details nsd
	        INNER JOIN nscc_barang nb ON nsd.barang_id = nb.barang_id
	        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
	        INNER JOIN nscc_salestrans ns ON nsd.salestrans_id = ns.salestrans_id
	        INNER JOIN nscc_dokter nd ON nsd.dokter_id = nd.dokter_id
	        WHERE ng.kategori_id = :kategori_id AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where $dokter
	        GROUP BY nd.nama_dokter,ns.tgl,nb.kode_barang
	        ORDER BY nd.nama_dokter,nb.kode_barang) as a");
        return $comm->queryAll(true, $param);
    }

    static function report_dokter_details($from, $to, $store = "")
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $param = array(':from' => $from, ':to' => $to, ':kategori_id' => $kategori_id);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        if (!NATASHA_CUSTOM)
            $comm = Yii::app()->db->createCommand("SELECT DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,nd.nama_dokter,nc.nama_customer,
	        nb.kode_barang,nb.nama_barang,SUM(nsd.qty) AS qty,SUM(nsd.jasa_dokter) AS tip,ns.doc_ref
	        FROM nscc_salestrans_details nsd
	        INNER JOIN nscc_barang nb ON nsd.barang_id = nb.barang_id
	        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
	        INNER JOIN nscc_salestrans ns ON nsd.salestrans_id = ns.salestrans_id
	        INNER JOIN nscc_dokter nd ON nsd.dokter_id = nd.dokter_id
	        INNER JOIN nscc_customers nc ON ns.customer_id = nc.customer_id
	        WHERE ng.kategori_id = :kategori_id AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where
	        GROUP BY nd.nama_dokter,ns.tgl,nc.nama_customer,ns.doc_ref,nb.kode_barang
	        ORDER BY ns.doc_ref,nd.nama_dokter");
        else
            $comm = Yii::app()->db->createCommand("SELECT DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,nd.nama_dokter,nc.nama_customer,
	        nb.kode_barang,nb.nama_barang,SUM(nsd.qty) AS qty,'0' AS tip,ns.doc_ref
	        FROM nscc_salestrans_details nsd
	        INNER JOIN nscc_barang nb ON nsd.barang_id = nb.barang_id
	        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
	        INNER JOIN nscc_salestrans ns ON nsd.salestrans_id = ns.salestrans_id
	        INNER JOIN nscc_dokter nd ON nsd.dokter_id = nd.dokter_id
	        INNER JOIN nscc_customers nc ON ns.customer_id = nc.customer_id
	        WHERE ng.kategori_id = :kategori_id AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where
	        GROUP BY nd.nama_dokter,ns.tgl,nc.nama_customer,ns.doc_ref,nb.kode_barang
	        ORDER BY ns.doc_ref,nd.nama_dokter");
        return $comm->queryAll(true, $param);
    }

    static function report_retursales_summary($nama_grup, $from, $to, $store = "")
    {
        $grup = "";
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($nama_grup != null) {
            $grup = "AND ng.nama_grup = :nama_grup";
            $param[':nama_grup'] = $nama_grup;
        }
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT ns.doc_ref,ns.doc_ref_sales, -SUM(nsd.total + nsd.vatrp) AS total,
  nb.kode_barang, -SUM(nsd.qty) AS qty, DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,ns.retur_note
FROM nscc_salestrans ns
  LEFT JOIN nscc_salestrans_details nsd
    ON nsd.salestrans_id = ns.salestrans_id
  LEFT JOIN nscc_barang nb
    ON nsd.barang_id = nb.barang_id
    LEFT JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
WHERE nsd.total < 0 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup $where
GROUP BY ns.doc_ref, nb.kode_barang, ns.tgl");
        return $comm->queryAll(true, $param);
    }

    static function report_audit_summary($grup_id, $from, $to)
    {
        $grup = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($grup_id != null) {
            $grup = "AND nb.grup_id = :grup_id";
            $param[':grup_id'] = $grup_id;
        }
        $comm = Yii::app()->db->createCommand("SELECT ns.doc_ref, SUM(nsd.total) AS total,
  nb.kode_barang, SUM(nsd.qty) AS qty, DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl
FROM nscc_audit ns
  INNER JOIN nscc_audit_details nsd
    ON nsd.audit_details = ns.audit_id
  INNER JOIN nscc_barang nb
    ON nsd.barang_id = nb.barang_id
WHERE nsd.total >= 0 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup
GROUP BY ns.doc_ref, nb.kode_barang, ns.tgl");
        return $comm->queryAll(true, $param);
    }

    static function report_sales_details($nama_grup, $from, $to, $real = 0, $store = "")
    {
        $grup = "";
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($nama_grup != null) {
            $grup = "AND ng.nama_grup = :nama_grup";
            $param[':nama_grup'] = $nama_grup;
        }
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $sql = $real ? "SELECT ns.doc_ref,nscc_customers.no_customer,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
        nscc_customers.nama_customer,nb.kode_barang,nsd.qty,nsd.price,nsd.bruto,nsd.discrp,nsd.vatrp,(nsd.total+nsd.vatrp) total
        FROM {{salestrans_details}} AS nsd
        LEFT JOIN {{salestrans}} AS ns ON nsd.salestrans_id = ns.salestrans_id
        LEFT JOIN {{barang}} AS nb ON nsd.barang_id = nb.barang_id
        LEFT JOIN {{customers}} ON ns.customer_id = nscc_customers.customer_id
        LEFT JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
        WHERE nsd.total >= 0 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup $where
        ORDER BY ns.doc_ref" : "SELECT a.doc_ref,a.no_customer,a.tgl,
        a.nama_customer,a.kode_barang,SUM(a.qty) qty,a.price,SUM(a.bruto) bruto,SUM(a.discrp) discrp,SUM(a.vatrp) vatrp,SUM(a.total) total FROM
(SELECT ns.doc_ref,nscc_customers.no_customer,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
        nscc_customers.nama_customer,nb.kode_barang,nsd.qty,nsd.price,nsd.bruto,nsd.discrp,nsd.vatrp,(nsd.total+nsd.vatrp) total
        FROM nscc_salestrans_details AS nsd
        LEFT JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        LEFT JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        LEFT JOIN nscc_customers ON ns.customer_id = nscc_customers.customer_id
        LEFT JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
        WHERE ns.type_ = 1 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup $where
UNION ALL
SELECT ns.doc_ref_sales doc_ref,nscc_customers.no_customer,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
        nscc_customers.nama_customer,nb.kode_barang,nsd.qty,nsd.price,nsd.bruto,nsd.discrp,nsd.vatrp,(nsd.total+nsd.vatrp) total
        FROM nscc_salestrans_details AS nsd
        LEFT JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        LEFT JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        LEFT JOIN nscc_customers ON ns.customer_id = nscc_customers.customer_id
        LEFT JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
        WHERE ns.type_ = -1 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup $where) a
GROUP BY a.doc_ref,a.kode_barang
HAVING qty <> 0 ORDER BY a.doc_ref";
        $comm = Yii::app()->db->createCommand($sql);
        return $comm->queryAll(true, $param);
    }

    static function report_sales_item_details($barang_id, $from, $to, $store = "")
    {
        $barang = "";
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($barang_id != null) {
            $barang = "AND nsd.barang_id = :barang_id";
            $param[':barang_id'] = $barang_id;
        }
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $sql = "SELECT a.doc_ref,a.no_customer,a.tgl,
        a.nama_customer,a.kode_barang,SUM(a.qty) qty,a.price,SUM(a.bruto) bruto,SUM(a.discrp) discrp,SUM(a.vatrp) vatrp,SUM(a.total) total FROM
(SELECT ns.doc_ref,nscc_customers.no_customer,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
        nscc_customers.nama_customer,nb.kode_barang,nsd.qty,nsd.price,nsd.bruto,nsd.discrp,nsd.vatrp,(nsd.total+nsd.vatrp) total
        FROM nscc_salestrans_details AS nsd
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_customers ON ns.customer_id = nscc_customers.customer_id
        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
        WHERE ns.type_ = 1 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $barang $where
UNION ALL
SELECT ns.doc_ref_sales doc_ref,nscc_customers.no_customer,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
        nscc_customers.nama_customer,nb.kode_barang,nsd.qty,nsd.price,nsd.bruto,nsd.discrp,nsd.vatrp,(nsd.total+nsd.vatrp) total
        FROM nscc_salestrans_details AS nsd
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_customers ON ns.customer_id = nscc_customers.customer_id
        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
        WHERE ns.type_ = -1 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $barang $where) a
GROUP BY a.doc_ref,a.kode_barang
HAVING qty > 0 ORDER BY a.doc_ref";
        $comm = Yii::app()->db->createCommand($sql);
        return $comm->queryAll(true, $param);
    }

    static function report_retur_sales_details($nama_grup = "", $from, $to, $store = "")
    {
        $grup = "";
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($nama_grup != null) {
            $grup = "AND ng.nama_grup = :nama_grup";
            $param[':nama_grup'] = $nama_grup;
        }
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT ns.doc_ref,nscc_customers.no_customer,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
        nscc_customers.nama_customer,nb.kode_barang,-nsd.qty qty,nsd.price,-nsd.bruto bruto,-nsd.discrp discrp,-nsd.total total
        FROM {{salestrans_details}} AS nsd
        INNER JOIN {{salestrans}} AS ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN {{barang}} AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN {{customers}} ON ns.customer_id = nscc_customers.customer_id
        INNER JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
        WHERE nsd.total < 0 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup $where
        ORDER BY ns.doc_ref");
        return $comm->queryAll(true, $param);
    }

    static function report_audit_details($grup_id, $from, $to)
    {
        $grup = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($grup_id != null) {
            $grup = "AND nb.grup_id = :grup_id";
            $param[':grup_id'] = $grup_id;
        }
        $comm = Yii::app()->db->createCommand("SELECT ns.doc_ref,nscc_customers.no_customer,DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
        nscc_customers.nama_customer,nb.kode_barang,nsd.qty,nsd.price,nsd.discrp,nsd.total
        FROM nscc_audit_details AS nsd
        INNER JOIN nscc_audit AS ns ON nsd.audit_details = ns.audit_id
        INNER JOIN {{barang}} AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN {{customers}} ON ns.customer_id = nscc_customers.customer_id
        WHERE nsd.total >= 0 AND DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $grup
        ORDER BY ns.doc_ref");
        return $comm->queryAll(true, $param);
    }

    static function report_view_customer_history($customer_id, $tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT DATE(ns.tgl) tgl,ns.doc_ref,nb.kode_barang,nsd.qty,nb.sat
        FROM {{salestrans}} AS ns
        INNER JOIN {{salestrans_details}} AS nsd ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN {{barang}} AS nb ON nsd.barang_id = nb.barang_id
        WHERE ns.customer_id = :customer_id AND ns.type_ = 1 AND DATE(ns.tgl) > :tgl
        ORDER BY ns.tgl DESC");
        return $comm->queryAll(true, array(':customer_id' => $customer_id, ':tgl' => $tgl));
    }

    static function get_sales_trans_for_audit($date)
    {
        $comm = Yii::app()->db->createCommand("SELECT ns.salestrans_id,ns.doc_ref,
        nc.no_customer,nc.nama_customer,nba.nama_bank,Sum(nsd.total) AS total
        FROM nscc_salestrans AS ns
        INNER JOIN nscc_customers AS nc ON ns.customer_id = nc.customer_id
        INNER JOIN nscc_bank AS nba ON ns.bank_id = nba.bank_id
        INNER JOIN nscc_salestrans_details AS nsd ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        WHERE nb.grup_id = 1 AND nsd.total >= 0 AND ns.audit <> 1 AND DATE(ns.tgl) = :date
        GROUP BY ns.doc_ref,nc.no_customer,nc.nama_customer,nba.nama_bank");
        return $comm->queryAll(true, array(':date' => $date));
    }

    static function report_casin($tgl, $store = STOREID)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT nkd.item_name keperluan,-nkd.total total
        FROM nscc_kas_detail AS nkd
        INNER JOIN nscc_kas AS nk ON nkd.kas_id = nk.kas_id
        WHERE nk.bank_id = :bank_id AND nk.tgl = :tgl AND nk.arus = 1
        AND nk.visible = 1 AND nkd.visible = 1 AND nk.store = :store
        ORDER BY nk.doc_ref");
        return $comm->queryAll(true, array(
            ':tgl' => $tgl,
            ':store' => $store,
            ':bank_id' => SysPrefs::get_val('kas_cabang', $store)
        ));
    }

    static function report_sales_laha_rounding_total($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(SUM(ns.rounding),0) AS total
        FROM nscc_salestrans ns
        WHERE DATE(ns.tgl) = :tgl");
//        $comm->setFetchMode(PDO::FETCH_OBJ);
        return $comm->queryScalar(array(':tgl' => $tgl));
    }

    static function report_retursales_laha($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT ng.nama_grup,-Sum(nsd.total) AS total
        FROM nscc_grup AS ng
        INNER JOIN nscc_barang AS nb ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_salestrans_details AS nsd ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        WHERE DATE(ns.tgl) = :tgl AND ns.type_ = -1
        GROUP BY ng.nama_grup");
        return $comm->queryAll(true, array(':tgl' => $tgl));
    }

    static function report_cash_laha($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(Sum(nbt.amount),0)
        FROM nscc_bank_trans AS nbt
        INNER JOIN nscc_bank AS nb ON nbt.bank_id = nb.bank_id
        WHERE nb.nama_bank = 'CASH' AND DATE(nbt.tgl) = :tgl");
        return $comm->queryScalar(array(':tgl' => $tgl));
    }

    static function report_noncash_laha($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(Sum(nbt.amount),0)
        FROM nscc_bank_trans AS nbt
        INNER JOIN nscc_bank AS nb ON nbt.bank_id = nb.bank_id
        WHERE nbt.visible = 1 AND nb.bank_id NOT IN(:cash,:cash_while)
        AND DATE(nbt.tgl) = :tgl");
        return $comm->queryScalar(array(
            ':tgl' => $tgl,
            ':cash' => SysPrefs::get_val('kas_cabang'),
            ':cash_while' => SysPrefs::get_val('kas_cabang_sementara')
        ));
    }

    static function report_ulpt_laha($tgl)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT IFNULL(Sum(nbt.amount),0)
        FROM nscc_ulpt_view AS nbt
        WHERE nbt.tgl = :tgl");
        return $comm->queryScalar(
            array(
                ':tgl' => $tgl
            ));
    }

    static function report_ulpt_details_laha($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT nuv.nama_customer AS keperluan,nuv.amount AS total
        FROM nscc_ulpt_view AS nuv
        WHERE nuv.tgl = :tgl");
        return $comm->queryAll(true, array(
            ':tgl' => $tgl
        ));
    }

    static function report_rpj_details_laha($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT nuv.nama_customer AS keperluan,-nuv.amount AS total
        FROM nscc_rpg AS nuv
        WHERE nuv.bank_id = :cash AND nuv.tgl = :tgl");
        return $comm->queryAll(true, array(
            ':tgl' => $tgl,
            ':cash' => SysPrefs::get_val('kas_cabang')
        ));
    }

    static function report_rpj_laha($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(Sum(-nbt.amount),0)
        FROM nscc_bank_trans AS nbt
        INNER JOIN nscc_bank AS nb ON nbt.bank_id = nb.bank_id
        WHERE nbt.visible = 1 AND nb.bank_id = :cash AND nbt.type_ = :type
        AND DATE(nbt.tgl) = :tgl");
        return $comm->queryScalar(array(
            ':tgl' => $tgl,
            ':cash' => SysPrefs::get_val('kas_cabang'),
            ':type' => RPG
        ));
    }

    static function report_noncash_detail_laha($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT 	nb.nama_bank,	nc.card_name,	SUM(a.amount) AS amount FROM (
SELECT np.bank_id,ns.tgl,np.card_id, np.amount 
FROM nscc_payment AS np
INNER JOIN nscc_salestrans AS ns ON np.salestrans_id = ns.salestrans_id
UNION ALL
SELECT nuv.bank_id,nuv.tgl,nuv.card_id,nuv.amount
FROM nscc_ulpt_view AS nuv) AS a
INNER JOIN nscc_card AS nc ON a.card_id = nc.card_id
INNER JOIN nscc_bank AS nb ON a.bank_id = nb.bank_id
WHERE a.tgl = :tgl AND a.bank_id NOT IN (:cash)
GROUP BY	nb.bank_id,	nc.card_id
ORDER BY	nb.nama_bank,	nc.card_name");
        return $comm->queryAll(true, array(
            ':tgl' => $tgl,
            ':cash' => SysPrefs::get_val('kas_cabang')
        ));
    }

    static function report_noncash_sales($tgl)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT IFNULL(SUM(np.amount),0) AS total
        FROM nscc_payment np
  INNER JOIN nscc_salestrans ns
    ON np.salestrans_id = ns.salestrans_id
  WHERE DATE(ns.tgl) = :tgl AND
        ns.type_ = 1 AND np.bank_id <> :bank_id");
        return $comm->queryScalar(array(':tgl' => $tgl, ':bank_id' => Bank::get_bank_cash_id()));
    }

    static function report_modal($tgl)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT IFNULL(SUM(nk.total),0) AS total
        FROM nscc_kas nk
        WHERE DATE(nk.tgl) = :tgl
        AND nk.bank_id = :bank_id AND nk.total > 0 AND nk.type_ = 1");
        return $comm->queryScalar(array(':tgl' => $tgl, ':bank_id' => Bank::get_bank_cash_id()));
    }

    static function report_noncash_details_laha($tgl, $store = STOREID)
    {
//        $arr_filt = explode(',', SysPrefs::get_val('filter_payment', $store));
        $bank_id = Bank::get_bank_cash_id();
        $arr_filt[] = SysPrefs::get_val('kas_cabang', $store);
        $new_arr = array();
        foreach ($arr_filt as $bank) {
            $new_arr[] = "'$bank'";
        }
//        $bank_id = implode(',', $new_arr);
        $comm = Yii::app()->db->createCommand("SELECT nb.nama_bank,SUM(np.amount) total
        FROM nscc_payment AS np
        INNER JOIN nscc_salestrans AS ns ON np.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_bank AS nb ON np.bank_id = nb.bank_id
        WHERE nb.bank_id <> :bank_id AND DATE(ns.tgl) = :tgl AND ns.store = :store
        GROUP BY nb.nama_bank");
        return $comm->queryAll(true, array(
            ':tgl' => $tgl,
            ':bank_id' => $bank_id,
            ':store' => $store
        ));
    }

    static function report_sales_laha($tgl, $store = "")
    {
        $comm = Yii::app()->db->createCommand("SELECT ng.nama_grup,Sum(nsd.total) AS total
        FROM nscc_grup AS ng
        INNER JOIN nscc_barang AS nb ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_salestrans_details AS nsd ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        WHERE DATE(ns.tgl) = :tgl
        GROUP BY ng.nama_grup");
        return $comm->queryAll(true, array(':tgl' => $tgl));
    }

    static function report_casout($tgl, $type, $store = STOREID)
    {
        $comm = Yii::app()->db->createCommand("
        SELECT nkd.item_name keperluan,ABS(nkd.total) total
        FROM nscc_kas_detail AS nkd
        INNER JOIN nscc_kas AS nk ON nkd.kas_id = nk.kas_id
        WHERE nk.bank_id = :bank_id AND nk.tgl = :tgl AND nk.arus = -1
        AND nk.visible = 1 AND nkd.visible = 1 AND nk.type_ = :type AND nk.store = :store
        ORDER BY nk.doc_ref");
        return $comm->queryAll(true, array(
            ':tgl' => $tgl,
            ':type' => $type,
            ':store' => $store,
            ':bank_id' => SysPrefs::get_val('kas_cabang', $store),
        ));
    }

    static function report_laha_bank_transfer($tgl, $store)
    {
        $comm = Yii::app()->db->createCommand("
    SELECT IFNULL(ABS(SUM(nbt.amount)),0)
    FROM nscc_bank_trans nbt
    WHERE nbt.visible = 1 AND nbt.type_ = 11 AND nbt.tgl = :tgl AND nbt.bank_id = :bank AND nbt.store = :store");
        return $comm->queryScalar(array(
            ':tgl' => $tgl,
            ':store' => $store,
            ':bank' => SysPrefs::get_val('kas_cabang', $store)
        ));
    }

    static function report_laha_bank_transfer_setor($tgl, $store)
    {
        $comm = Yii::app()->db->createCommand("
    SELECT IFNULL(ABS(SUM(nbt.amount)),0)
    FROM nscc_bank_trans nbt
    WHERE nbt.visible = 1 AND nbt.type_ = 11 AND nbt.tgl = :tgl AND nbt.bank_id = :bank AND nbt.store = :store");
        return $comm->queryScalar(array(
            ':tgl' => $tgl,
            ':store' => $store,
            ':bank' => SysPrefs::get_val('bank_setor', $store)
        ));
    }

    static function report_vat_sales($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(Sum(ns.vat),0)
        FROM nscc_salestrans AS ns
        WHERE DATE(ns.tgl) = :tgl");
        return $comm->queryScalar(array(':tgl' => $tgl));
    }

    static function report_vat_retursales($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(-Sum(ns.vat),0)
        FROM nscc_salestrans AS ns
        WHERE DATE(ns.tgl) = :tgl");
        return $comm->queryScalar(array(':tgl' => $tgl));
    }

    static function report_discount_sales($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(Sum(ns.discrp),0)
        FROM nscc_salestrans AS ns
        WHERE DATE(ns.tgl) = :tgl");
        return $comm->queryScalar(array(':tgl' => $tgl));
    }

    static function report_discount_returnsales($tgl)
    {
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(-Sum(ns.discrp),0)
        FROM nscc_salestrans AS ns
        WHERE DATE(ns.tgl) = :tgl AND ns.type_ = -1");
        return $comm->queryScalar(array(':tgl' => $tgl));
    }

    static function report_tenders($tgl, $store = "")
    {
        $where1 = "";
        $where2 = "";
        $param = array(':tgl' => $tgl);
        if ($store != null) {
            $where1 = "AND nbt.store = :store";
            $where2 = "AND nt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT nb.nama_bank,IFNULL(a.total,0) `system`,IFNULL(b.total,0) `tenders`,
        IF((IFNULL(a.total,0)-IFNULL(b.total,0)) <= 0,'SHORT','OVER') `Status`,
        ABS(IFNULL(a.total,0)-IFNULL(b.total,0)) 'diff' FROM
        (SELECT nbt.bank_id,IFNULL(Sum(nbt.amount),0) total
        FROM nscc_bank_trans AS nbt
        WHERE DATE(nbt.tgl) = :tgl and nbt.visible = 1 $where1
        GROUP BY nbt.bank_id) a
        LEFT JOIN (
        SELECT ntd.bank_id,IFNULL(ntd.amount,0) total
        FROM nscc_tender AS nt
        INNER JOIN nscc_tender_details AS ntd ON ntd.tender_id = nt.tender_id
        Where nt.tgl = :tgl $where2
        GROUP BY ntd.bank_id) b ON (a.bank_id = b.bank_id)
        RIGHT JOIN nscc_bank nb ON a.bank_id = nb.bank_id");
        return $comm->queryAll(true, $param);
    }

    static function get_date_service_no_final($from, $to, $store = "")
    {
        $kategori_id = SysPrefs::get_val('kategori_threatment_id');
        $where = "";
        $param = array(':from' => $from, ':to' => $to, ':kategori_id' => $kategori_id);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT DATE_FORMAT(ns.tgl,'%d %b %Y') tgl
        FROM nscc_salestrans_details AS nsd
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_beauty_services nbs ON (nbs.salestrans_details = nsd.salestrans_details AND nbs.visible = 1)
        WHERE nbs.final = 0 AND ng.kategori_id = :kategori_id  AND DATE(ns.tgl) >= :from AND DATE(ns.tgl)  <= :to and ns.type_  = 1 $where
        GROUP BY DATE(ns.tgl)");
        return $comm->queryAll(true, $param);
    }

    static function get_printed_sales()
    {
        $newLine = "\n";
        $raw = U::setCenter(SysPrefs::get_val('receipt_header0'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_header1'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_header2'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_header3'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_header4'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_header5'));
        $raw .= $newLine;
        $raw .= U::fillWithChar("=");
//        $raw .= "---------------------------------------------------------------";
        $raw .= $newLine;
        $raw .= U::fillWithChar("-");
        $raw .= $newLine;
        $raw .= "Item Code        Item Name                 QTY           TOTAL ";
        $raw .= $newLine;
        $raw .= U::fillWithChar("-");
        $raw .= $newLine;
        $raw .= U::addItemCodeReceipt("TSUNCR", '1.00', '27,500.00');
        $raw .= $newLine;
        $raw .= U::addItemNameReceipt("Teen's Day Sunscreen", 46);
        $raw .= $newLine;
        $raw .= U::addItemDiscReceipt("10.00", "-2,750.00");
        $raw .= $newLine;
        $raw .= U::fillWithChar("-");
        $raw .= $newLine;
        $raw .= U::fillWithChar("=");
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_footer0'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_footer1'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_footer2'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_footer3'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_footer4'));
        $raw .= $newLine;
        $raw .= U::setCenter(SysPrefs::get_val('receipt_footer5'));
        $raw .= $newLine;
        return $raw;
    }

    static function get_gl_before($account_code, $date, $store = "")
    {
        $where = "";
        $param = array(":account_code" => $account_code, ":date" => $date);
        if ($store != null) {
            $where = "AND ngt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT IFNULL(SUM(ngt.amount),0) FROM nscc_gl_trans ngt
        WHERE ngt.account_code = :account_code AND ngt.visible = 1 AND ngt.tran_date < :date $where");
        return $comm->queryScalar($param);
    }

    static function get_gl_after($account_code, $date, $store = "")
    {
        $where = "";
        $param = array(":account_code" => $account_code, ":date" => $date);
        if ($store != null) {
            $where = "AND ngt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT IFNULL(SUM(ngt.amount),0) FROM nscc_gl_trans ngt
        WHERE ngt.account_code = :account_code AND ngt.visible = 1 AND ngt.tran_date > :date $where");
        $comm->queryScalar($param);
    }

    static function get_general_ledger($account_code, $from, $to, $store = "")
    {
        $where = "";
        $param = array(
            ':account_code' => $account_code,
            ':from' => $from,
            ':to' => $to
        );
        if ($store != null) {
            $where = "AND ngt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT DATE_FORMAT(ngt.tran_date,'%d/%m/%Y') tgl,ngt.store,
        ngt.memo_,IF(ngt.amount > 0, ngt.amount, '') AS Debit,
        IF(ngt.amount < 0, -ngt.amount, '') AS Credit,
        0 AS Balance,
        ngt.amount
        FROM nscc_gl_trans ngt
        WHERE ngt.account_code = :account_code AND ngt.visible = 1 AND
        ngt.tran_date >= :from AND ngt.tran_date <= :to $where
        ORDER BY ngt.tran_date,ngt.tdate");
        return $comm->queryAll(true, $param);
    }

    static function get_general_journal($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ngt.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
        SELECT DATE_FORMAT(ngt.tran_date,'%d/%m/%Y') tgl,ngt.store,
        ntt.tipe_name,nscc_refs.reference,
        ncm.account_code,ncm.account_name,
        ngt.memo_,IF(ngt.amount > 0, ngt.amount, '') AS Debit,
        IF(ngt.amount < 0, -ngt.amount, '') AS Credit
        FROM nscc_gl_trans ngt
        INNER JOIN nscc_chart_master ncm
            ON ngt.account_code = ncm.account_code
        LEFT JOIN nscc_trans_tipe ntt
            ON ngt.type = ntt.tipe_id
        LEFT JOIN nscc_refs
            ON ngt.type = nscc_refs.type_ AND ngt.type_no = nscc_refs.type_no
        WHERE ngt.tran_date >= :from AND ngt.visible = 1 AND ngt.tran_date <= :to $where
        ORDER BY ngt.tran_date,ngt.type_no");
        return $comm->queryAll(true, $param);
    }

    static function generate_primary_key($table)
    {
        $sys = SysTypes::model()->find("type_id = :type_id", array(':type_id' => $table));
        $newid = "";
        switch ($table) {
            case RSALESTRANS :
                $newid = STOREID . "SLST" . $sys->next_reference;
                break;
            case RSALESTRANSDETAILS :
                $newid = STOREID . "SLSTDTL" . $sys->next_reference;
                break;
            case RUSERS :
                $newid = STOREID . "USERS" . $sys->next_reference;
                break;
            case RBEAUTY :
                $newid = STOREID . "BEAUTY" . $sys->next_reference;
                break;
            case RGRUP :
                $newid = STOREID . "GRUP" . $sys->next_reference;
                break;
            case RDOKTER :
                $newid = STOREID . "DOKTER" . $sys->next_reference;
                break;
            case RBARANG :
                $newid = STOREID . "BRNG" . $sys->next_reference;
                break;
            case RBANK :
                $newid = STOREID . "BANK" . $sys->next_reference;
                break;
            case RKAS :
                $newid = STOREID . "KAS" . $sys->next_reference;
                break;
            case RTENDER :
                $newid = STOREID . "TENDER" . $sys->next_reference;
                break;
            case RTRANSFERITEM :
                $newid = STOREID . "TITEM" . $sys->next_reference;
                break;
            case RBELI :
                $newid = STOREID . "BLI" . $sys->next_reference;
                break;
            case RJURNAL_UMUM :
                $newid = STOREID . "JU" . $sys->next_reference;
                break;
            case RPRINTZ:
                $newid = STOREID . "PZ" . $sys->next_reference;
                break;
            case RBANKTRANSFER:
                $newid = STOREID . "BTR" . $sys->next_reference;
                break;
            case RPELUNASANUTANG:
                $newid = STOREID . "PELUTANG" . $sys->next_reference;
                break;
        }
        $sys->next_reference++;
        if (!$sys->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'SysTypes')) . CHtml::errorSummary($sys));
        }
        return $newid;
    }

    static function save_file($filename, $data)
    {
        if (defined('WRITE_TO_FILE') && WRITE_TO_FILE) {
            $fh = @fopen($filename, 'w');
            if ($fh === false) {
                throw new Exception('Failed create file.');
            }
            fwrite($fh, $data);
            fclose($fh);
        }
    }

    static function get_report_payment($from, $to, $bank_id, $store = '')
    {
        $where = "";
        $param = array(
            ':from' => $from,
            ':to' => $to,
            ':bank_id' => $bank_id
        );
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("SELECT ns.doc_ref,
        nc.nama_customer,nc.no_customer,nca.card_name,np.card_number,DATE_FORMAT(ns.tgl,'%d %b %Y') tgl,
        np.amount,np.kembali
        FROM nscc_payment AS np
        LEFT JOIN nscc_card AS nca ON np.card_id = nca.card_id
        INNER JOIN nscc_salestrans AS ns ON np.salestrans_id = ns.salestrans_id
        INNER JOIN nscc_customers AS nc ON ns.customer_id = nc.customer_id
        WHERE ns.tgl >= :from AND ns.tgl <= :to AND np.bank_id = :bank_id $where 
        ORDER BY ns.doc_ref, ns.tgl");
        return $comm->queryAll(true, $param);
    }

    static function get_daftar_harga_jual($store)
    {
        $comm = Yii::app()->db->createCommand("SELECT ng.nama_grup,
        nb.kode_barang,nb.nama_barang,IFNULL(nj.price,0) price
        FROM nscc_barang AS nb
        LEFT JOIN nscc_grup AS ng ON nb.grup_id = ng.grup_id
        LEFT JOIN nscc_jual AS nj ON (nj.barang_id = nb.barang_id AND nj.store = :store)");
        return $comm->queryAll(true, array(':store' => $store));
    }

    static function get_bank_trans($from, $to, $bank_id)
    {
        $comm = Yii::app()->db->createCommand("SELECT ntt.tipe_name,nbt.ref,
        DATE_FORMAT(nbt.tgl,'%d %b %Y') AS tgl,        IF(nbt.amount >= 0,nbt.amount,0) AS Debit,
        IF(nbt.amount < 0,-nbt.amount,0) AS Credit,        0 AS balance,
        nbt.amount,        nbt.store,        ngt.memo_,        '' AS comment_
        FROM nscc_bank_trans AS nbt
        LEFT JOIN nscc_trans_tipe AS ntt ON nbt.type_ = ntt.tipe_id
        -- LEFT JOIN nscc_comments AS nc ON nbt.type_ = nc.type AND nbt.trans_no = nc.type_no
        INNER JOIN nscc_bank AS nb ON nbt.bank_id = nb.bank_id
        INNER JOIN nscc_gl_trans AS ngt ON nb.account_code = ngt.account_code AND nbt.type_ = ngt.type AND 
          nbt.trans_no = ngt.type_no AND nbt.amount = ngt.amount
        WHERE nbt.visible =1 AND ngt.visible = 1 AND nbt.tgl >= :from AND nbt.tgl <= :to AND nbt.bank_id = :bank_id
        group by nbt.bank_trans_id
        ORDER BY nbt.tgl,nbt.tdate");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to, ':bank_id' => $bank_id));
    }

    static function stockOpname_input($data, $trans_date, $store)
    {
        $command = StockMoves::model()->dbConnection->createCommand("SELECT UUID();");
        $trans_no = $command->queryScalar();
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($data as $dt) {
                $barang = Barang::model()->findByAttributes(array('kode_barang' => $dt['kode_barang']));
                if (!$barang)
                    continue;
//                    throw new Exception("Data was not saved.<br>Kode Barang ".$dt['kode_barang']." has not been registered.");
                $stock_baru = array(
                    'type_no' => -1
                , 'trans_no' => $trans_no
                , 'tran_date' => $trans_date
                , 'price' => 0
                , 'reference' => 'SALDO AWAL'
                , 'qty' => get_number($dt['qty'])
                , 'barang_id' => $barang->barang_id
                , 'store' => $store
                );
                $item = new StockMoves;
                $item->attributes = $stock_baru;
                if (!$item->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Input Stock Opname')) . CHtml::errorSummary($item));
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        app()->db->autoCommit = true;
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }



    static function report_customer_summary_details($from, $to, $customer = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($customer != null) {
            $where = "AND ns.customer_id = :customer ";
            $param[':customer'] = $customer;
        }
        $comm = Yii::app()->db->createCommand("
            SELECT 
                DATE_FORMAT(ns.tgl,'%d %b %Y') tgl,
                ns.doc_ref, 
                ns.ketdisc,
                ns.store,
                nb.kode_barang, 
                nsd.qty, 
                nsd.price,
                nsd.bruto,
                nsd.discrp,
                nsd.vatrp, 
                (nsd.total+nsd.vatrp) total,
                ns.total total_faktur,
                CAST(REPLACE(LEFT(ns.ketdisc,(LOCATE('/',ns.ketdisc)-1)),ns.store,'')as UNSIGNED) seq
            FROM nscc_salestrans_details nsd
            INNER JOIN nscc_salestrans ns
                ON nsd.salestrans_id = ns.salestrans_id
            INNER JOIN nscc_barang nb
                ON nsd.barang_id = nb.barang_id
                WHERE DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to
                AND ns.type_ = 1 $where
            ORDER BY seq,ns.tdate");
        return $comm->queryAll(true, $param);
    }

    static function report_fee_card($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
            SELECT
                DATE_FORMAT(ns.tgl, '%d %b %Y') tgl,
                nb.nama_bank,
                nc.card_name,
                np.amount,
                ROUND((np.amount*nc.persen_fee/100),2) fee,
                ROUND((np.amount*(1-nc.persen_fee/100)),2) piutang
            FROM nscc_payment np
                INNER JOIN nscc_card nc ON nc.card_id = np.card_id
                INNER JOIN nscc_bank nb ON nb.bank_id = np.bank_id
                INNER JOIN nscc_salestrans ns ON ns.salestrans_id = np.salestrans_id
            WHERE
                DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to AND ns.type_ = 1 $where
            ORDER BY ns.tgl
        ");
        return $comm->queryAll(true, $param);
    }

    static function piutang_due()
    {
        $comm = Yii::app()->db->createCommand("
    SELECT 
    gh.customer_id,
    gh.nama_customer,
		SUM(gh.totl0) AS totl0,
    SUM(gh.totl1) AS totl1,
    SUM(gh.totl2) AS totl2,
    SUM(gh.totl3) AS totl3,
		SUM(gh.totalx) AS totalx,
		gh.due
FROM

(SELECT 
        hj.customer_id,
            hj.nama_customer,
            hj.doc_ref_other,
            hj.tgl_jatuh_tempo,
						IF(hj.due >= 0 AND hj.due <= 15, hj.total - hj.totalkas, 0) AS totl0,
            IF(hj.due >= 16 AND hj.due <= 30, hj.total - hj.totalkas, 0) AS totl1,
            IF(hj.due >= 30 AND hj.due <= 60,hj.total - hj.totalkas, 0) AS totl2,
						IF(hj.due > 60, hj.total - hj.totalkas, 0) AS totl3,
						hj.total - hj.totalkas AS totalx,
            hj.due
    FROM

        (SELECT 
						pc.customer_id,
            pc.nama_customer,
            pss.doc_ref_other,
            IFNULL(qw.totalkas, 0) AS totalkas,
            pss.total,
            DATEDIFF(pss.tgl_jatuh_tempo, NOW()) AS due,
            pss.tgl_jatuh_tempo
    FROM
        nscc_payment_journal AS pss
    INNER JOIN nscc_customers AS pc ON pss.customer_id = pc.customer_id
    LEFT JOIN 
  
(SELECT 
        q.payment_journal_id, 
				SUM(kas_dibayar) AS totalkas
    FROM
        nscc_pembantu_pelunasan_piutang_detil AS q
        WHERE q.type_=1
    GROUP BY payment_journal_id) AS qw ON pss.payment_journal_id = qw.payment_journal_id) AS hj

    WHERE
        hj.due > 0
) AS gh

WHERE gh.totalx !=0
GROUP BY gh.customer_id
ORDER BY gh.nama_customer ASC

    ");
        return $comm->queryAll(true);
    }

    static function hutang_due()
    {
        $comm = Yii::app()->db->createCommand("
    SELECT 
    gh.supplier_id,
    gh.supplier_name,
	SUM(gh.totl0) AS totl0,
    SUM(gh.totl1) AS totl1,
    SUM(gh.totl2) AS totl2,
    SUM(gh.totl3) AS totl3,
		SUM(gh.totalx) AS totalx,
		gh.due
FROM

(SELECT 
        hj.supplier_id,
            hj.supplier_name,
            hj.doc_ref_other,
            hj.tgl_jatuh_tempo,
						IF(hj.due >= 0 AND hj.due <= 15, hj.total - hj.totalkas, 0) AS totl0,
            IF(hj.due >= 16 AND hj.due <= 30, hj.total - hj.totalkas, 0) AS totl1,
            IF(hj.due >= 30 AND hj.due <= 60,hj.total - hj.totalkas, 0) AS totl2,
						IF(hj.due > 60, hj.total - hj.totalkas, 0) AS totl3,
						hj.total - hj.totalkas AS totalx,
            hj.due
    FROM

        (SELECT 
						pc.supplier_id,
            pc.supplier_name,
            pss.doc_ref_other,
            IFNULL(qw.totalkas, 0) AS totalkas,
            pss.total,
            DATEDIFF(pss.tgl_jatuh_tempo, NOW()) AS due,
            pss.tgl_jatuh_tempo
    FROM
        nscc_invoice_journal AS pss
    INNER JOIN nscc_supplier AS pc ON pss.supplier_id = pc.supplier_id
    LEFT JOIN 
  
(SELECT 
        q.invoice_journal_id, 
				SUM(kas_dibayar) AS totalkas
    FROM
        nscc_pembantu_pelunasan_utang_detil AS q
        WHERE q.type_=1
    GROUP BY invoice_journal_id) AS qw ON pss.invoice_journal_id = qw.invoice_journal_id) AS hj

) AS gh

WHERE gh.totalx !=0
GROUP BY gh.supplier_id
ORDER BY gh.supplier_name ASC

    ");
        return $comm->queryAll(true);
    }

    static function report_InfoEfektivitas($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND nc.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
            SELECT ni.info_name,Count(*) AS total
            FROM nscc_info AS ni
            INNER JOIN nscc_customers AS nc ON nc.info_id = ni.info_id
            WHERE date(nc.awal) >= :from AND date(nc.awal) <= :to $where
            GROUP BY ni.info_id");
        return $comm->queryAll(true, $param);
    }

    static function report_Transfer_Barang($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ntb.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
            SELECT ntb.tgl,ntb.doc_ref,ntb.note,ntb.tdate,ntb.doc_ref_other,ntb.user_id,ntb.type_,
                ntb.store,ntb.total,ntb.disc,ntb.discrp,ntb.bruto,ntb.vat,ntb.total_pot,ntb.total_discrp1,
                ntb.tgl_jatuh_tempo,ntb.lunas,ntbd.qty,ntbd.transfer_barang_id,ntbd.total,ntbd.disc,
                ntbd.discrp,ntbd.bruto,ntbd.vat,ntbd.vatrp,ntbd.disc1,ntbd.discrp1,ntbd.total_pot,
                ntbd.visible,nb.kode_barang,nb.nama_barang
            FROM nscc_transfer_barang AS ntb
            INNER JOIN nscc_transfer_barang_details AS ntbd ON ntbd.transfer_barang_id = ntb.transfer_barang_id
            INNER JOIN nscc_barang AS nb ON ntbd.barang_id = nb.barang_id
            WHERE ntb.tgl >= :from AND  ntb.tgl <= :to $where");
        return $comm->queryAll(true, $param);
    }

    static function report_efektivitas($from, $to, $store = "")
    {
        $where = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND nc.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
            SELECT date_format(a.tgl,'%d %b %Y') tgl,a.nama_customer,a.no_customer,a.info_name,
              if(cream > 0 AND perawatan = 0,1,0) cream,if(cream = 0 AND perawatan > 0,1,0) perawatan,
              if(cream > 0 AND perawatan > 0,1,0) cream_rawat,if(cream = 0 AND perawatan = 0,1,0) konsul
            FROM (SELECT date(nc.awal) tgl, nc.nama_customer,nc.no_customer,ni.info_name,sum(if(ng.kategori_id <> 1,nsd.qty,0)) cream,
              sum(if(ng.kategori_id = 1,nsd.qty,0)) perawatan
            FROM nscc_customers nc LEFT JOIN nscc_info ni ON nc.info_id = ni.info_id
              LEFT JOIN nscc_salestrans ns ON nc.customer_id = ns.customer_id
              LEFT JOIN nscc_salestrans_details nsd ON ns.salestrans_id = nsd.salestrans_id
              LEFT JOIN nscc_barang nb ON nsd.barang_id = nb.barang_id
              LEFT JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
            WHERE DATE(nc.awal) >= :from AND DATE(nc.awal) <= :to $where
            GROUP BY nc.customer_id) a
            ORDER BY a.no_customer;");
        return $comm->queryAll(true, $param);
    }

    static function report_pasien_baru_riil_faktur($from, $to, $store = "")
    {
        $where = $where1 = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $where1 = "AND nc.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
            SELECT date_format(a.tgl,'%d %b %Y') tgl,c.baru,a.receipt,a.riil,c.cream,
	        c.perawatan,c.cream_rawat,c.konsul FROM
            (SELECT ns.tgl, #COUNT(DISTINCT CASE WHEN DATE(nc.awal) = ns.tgl THEN nc.customer_id END) baru,						
						COUNT(DISTINCT(ns.salestrans_id)) receipt,COUNT(DISTINCT(ns.customer_id))	riil
            FROM nscc_customers nc
              INNER JOIN nscc_salestrans ns ON nc.customer_id = ns.customer_id AND ns.type_ <> -1
            WHERE ns.tgl >= :from AND ns.tgl <= :to $where
            GROUP BY ns.tgl) a INNER JOIN (
						SELECT b.tgl,COUNT(DISTINCT b.customer_id) baru,
              SUM(if(cream > 0 AND perawatan = 0,1,0)) cream,SUM(if(cream = 0 AND perawatan > 0,1,0)) perawatan,
              SUM(if(cream > 0 AND perawatan > 0,1,0)) cream_rawat,SUM(if(cream = 0 AND perawatan = 0,1,0)) konsul
            FROM (SELECT date(nc.awal) tgl,nc.customer_id, sum(if(ng.kategori_id <> 1,nsd.qty,0)) cream,
              sum(if(ng.kategori_id = 1,nsd.qty,0)) perawatan
            FROM nscc_customers nc
              LEFT JOIN nscc_salestrans ns ON nc.customer_id = ns.customer_id AND ns.type_ <> -1
							#INNER JOIN nscc_salestrans nsr ON ns.salestrans_id <> nsr.parent_id AND nsr.type_ = -1
              LEFT JOIN nscc_salestrans_details nsd ON ns.salestrans_id = nsd.salestrans_id
              LEFT JOIN nscc_barang nb ON nsd.barang_id = nb.barang_id
              LEFT JOIN nscc_grup ng ON nb.grup_id = ng.grup_id
            WHERE DATE(nc.awal) >= :from AND DATE(nc.awal) <= :to $where1
            GROUP BY tgl,nc.customer_id) b 
			GROUP BY b.tgl) c ON a.tgl = c.tgl ORDER BY a.tgl");
        return $comm->queryAll(true, $param);
    }

    static function report_rekap_PurchaseOrder($from, $to, $supplier_id, $status)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                po.*,
                s.supplier_name
            FROM nscc_po po
                LEFT JOIN nscc_supplier s ON s.supplier_id = po.supplier_id
            WHERE
                po.tgl >= :from AND po.tgl <= :to
                " . (strtolower($supplier_id) == 'all' ? "" : "AND po.supplier_id = '" . $supplier_id . "'") . "
                " . (strtolower($status) == 'all' ? "" : "AND po.status = " . $status) . "
                AND po.type_ = 1
            ORDER BY po.doc_ref, po.tgl
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }

    static function report_rekap_PurchaseOrder_details($from, $to, $supplier_id, $barang_id, $status)
    {
        $comm = Yii::app()->db->createCommand("
            SELECT
                po.*,
                s.supplier_name,
                b.kode_barang,
                b.nama_barang,
                pod.*,
                DATE_FORMAT(po.tgl_delivery, '%d-%b-%Y') tgl_delivery,
                t.tgl_sj,
                t.qty qty_sj,
                r.qty_returned,
                (ifnull(pod.qty,0)-ifnull(t.qty,0)) qty_outstanding,
                b.sat
            FROM nscc_po AS po
                LEFT JOIN nscc_po_details AS pod ON po.po_id = pod.po_id
                LEFT JOIN nscc_supplier AS s ON s.supplier_id = po.supplier_id
                LEFT JOIN nscc_barang AS b ON b.barang_id = pod.barang_id
                LEFT JOIN (
                        SELECT
                                tb.po_id,
                                tbd.barang_id,
                                tbd.terima_barang_id,
                                SUM(tbd.qty) qty,
                                GROUP_CONCAT(DATE_FORMAT(tb.tgl_sj, '%d-%b-%Y') ORDER BY tb.tgl_sj SEPARATOR '; ') tgl_sj
                        FROM nscc_terima_barang_details AS tbd
                                LEFT JOIN nscc_terima_barang AS tb ON tb.terima_barang_id = tbd.terima_barang_id
                        WHERE
                            tbd.visible = 1
                        GROUP BY
                                tb.po_id, tbd.barang_id
                    ) AS t ON t.po_id = pod.po_id AND t.barang_id = pod.barang_id
                LEFT JOIN (
                        SELECT
                                rb.terima_barang_id,
                                rbd.barang_id,
                                sum(abs(rbd.qty)) qty_returned
                        FROM nscc_transfer_item_details rbd
                                LEFT JOIN nscc_transfer_item rb ON rb.transfer_item_id = rbd.transfer_item_id
			WHERE
                            rb.type_ = 1
                            AND rbd.visible = 1
                        GROUP BY rb.terima_barang_id, rbd.barang_id
                    ) r ON r.terima_barang_id = t.terima_barang_id AND r.barang_id = t.barang_id
            WHERE
                po.tgl >= :from AND po.tgl <= :to
                " . (strtolower($supplier_id) == 'all' ? "" : "AND po.supplier_id = '" . $supplier_id . "'") . "
                " . (strtolower($barang_id) == 'all' ? "" : "AND pod.barang_id = '" . $barang_id . "'") . "
                " . (strtolower($status) == 'all' ? "" : "AND po.status = " . $status) . "
                AND po.type_ = 1
            ORDER BY po.doc_ref, po.tgl
        ");
        return $comm->queryAll(true, array(':from' => $from, ':to' => $to));
    }

    static function report_R_ReportPiutang($from, $to)
    {
        $supp = Yii::app()->db->createCommand("SELECT C.customer_id, C.nama_customer FROM nscc_customers AS C 
			INNER JOIN nscc_payment_journal AS PJ ON PJ.customer_id = C.customer_id
			GROUP BY C.customer_id
			ORDER BY C.nama_customer")->queryAll();
        foreach ($supp as $item) {
            $awal = Yii::app()->db->createCommand("SELECT SUM(total) AS awal FROM nscc_payment_journal
				WHERE tdate < :tgl AND customer_id = :sup AND p = 1");
            $awal->bindParam(':tgl', $from);
            $awal->bindParam(':sup', $item['customer_id']);
            $awal = $awal->queryScalar();
            $kredit = Yii::app()->db->createCommand("SELECT SUM(total) AS debet FROM nscc_payment_journal
				WHERE tdate >= :awal AND tdate <= :akhir AND customer_id = :sup AND p = 1");
            $kredit->bindParam(':awal', $from);
            $kredit->bindParam(':akhir', $to);
            $kredit->bindParam(':sup', $item['customer_id']);
            $kredit = $kredit->queryScalar();
            $debet = Yii::app()->db->createCommand("SELECT SUM(kas_dibayar) AS kredit FROM nscc_pembantu_pelunasan_piutang AS ppu
				INNER JOIN nscc_pembantu_pelunasan_piutang_detil AS ppud ON ppud.pembantu_pelunasan_piutang_id = ppu.pembantu_pelunasan_piutang_id
				WHERE tdate >= :awal AND tdate <= :akhir AND customer_id = :sup");
            $debet->bindParam(':awal', $from);
            $debet->bindParam(':akhir', $to);
            $debet->bindParam(':sup', $item['customer_id']);
            $debet = $debet->queryScalar();
            $akhir = $awal + $kredit - $debet;
            $ret[] = array(
                'customer' => $item['nama_customer'],
                'saldo_awal' => $awal,
                'debet' => $debet,
                'kredit' => $kredit,
                'saldo_akhir' => $akhir
            );
        }
        return $ret;
    }

    static function report_R_ReportHutang($from, $to)
    {
        $supp = Yii::app()->db->createCommand("SELECT supplier_id, supplier_name FROM nscc_supplier
			ORDER BY supplier_name")->queryAll();
        foreach ($supp as $item) {
            $awal = Yii::app()->db->createCommand("SELECT SUM(total) AS awal FROM nscc_invoice_journal
				WHERE tdate < :tgl AND supplier_id = :sup AND p = 1");
            $awal->bindParam(':tgl', $from);
            $awal->bindParam(':sup', $item['supplier_id']);
            $awal = $awal->queryScalar();
            $debet = Yii::app()->db->createCommand("SELECT SUM(total) AS debet FROM nscc_invoice_journal
				WHERE tdate >= :awal AND tdate <= :akhir AND supplier_id = :sup AND p = 1");
            $debet->bindParam(':awal', $from);
            $debet->bindParam(':akhir', $to);
            $debet->bindParam(':sup', $item['supplier_id']);
            $debet = $debet->queryScalar();
            $kredit = Yii::app()->db->createCommand("SELECT SUM(kas_dibayar) AS kredit FROM nscc_pembantu_pelunasan_utang AS ppu
				INNER JOIN nscc_pembantu_pelunasan_utang_detil AS ppud ON ppud.pembantu_pelunasan_utang_id = ppu.pembantu_pelunasan_utang_id
				WHERE tdate >= :awal AND tdate <= :akhir AND supplier_id = :sup");
            $kredit->bindParam(':awal', $from);
            $kredit->bindParam(':akhir', $to);
            $kredit->bindParam(':sup', $item['supplier_id']);
            $kredit = $kredit->queryScalar();
            $akhir = $awal + $debet - $kredit;
            $ret[] = array(
                'supplier' => $item['supplier_name'],
                'saldo_awal' => $awal,
                'debet' => $debet,
                'kredit' => $kredit,
                'saldo_akhir' => $akhir
            );
        }
        return $ret;
    }

    static function report_SupplierInvoice($from, $to, $charge, $type_ = "All", $status = "All", $supplier_id = "", $show_detail = false)
    {
        $cmd = DbCmd::instance()
            ->addSelect(array(
                'if(ti.type_ = 0, "Invoice", "Retur") type_',
                'ti.tgl',
                'ti.doc_ref',
                's.supplier_name',
                'ti.tgl_jatuh_tempo',
                'ti.status'
            ))
            ->addFrom('{{transfer_item}} ti')
            ->addLeftJoin('{{supplier}} s', 's.supplier_id = ti.supplier_id')
            ->addCondition('DATE(ti.tgl) >= :from AND DATE(ti.tgl) <= :to')
            ->addOrder('ti.tgl');

        if ($show_detail) {
            $cmd->addSelect(array(
                'b.kode_barang',
                'tid.charge',
                'abs(tid.qty) qty',
                'abs(tid.price) price',
                'abs(tid.sub_total) sub_total',
                'abs(tid.disc_rp) disc_rp',
                'abs(tid.ppn_rp) ppn_rp',
                'pph_id',
                'abs(tid.pph_rp) pph_rp',
                'abs(tid.total) total'
            ))
                ->addLeftJoin('{{transfer_item_details}} tid', 'tid.transfer_item_id = ti.transfer_item_id AND tid.visible = 1')
                ->addLeftJoin('{{barang}} b', 'b.barang_id = tid.barang_id');
        } else {
            $cmd->addSelect('abs(ti.total) total');
        }

        if ($charge != "" && $charge != "All") {
            $cmd->addCondition('tid.charge = :charge')
                ->addParam(':charge', $charge);
        }
        if ($supplier_id != null) {
            $cmd->addCondition('ti.supplier_id = :supplier_id')
                ->addParam(':supplier_id', $supplier_id);
        }
        if ($type_ != "" && $type_ != "All") {
            $cmd->addCondition('ti.type_ = :type_')
                ->addParam(':type_', $type_);
        }
        if ($status != "" && $status != "All") {
            $cmd->addCondition('ti.status = :status')
                ->addParam(':status', $status);
        }

        return $cmd->queryAll(true, array(
                ':from' => $from,
                ':to' => $to)
        );
    }

    static function report_bonus_summary($from, $to, $store = "")
    {
        $param = array(':from' => $from, ':to' => $to);
        $where = "";
        if ($store != null) {
            $where = "AND nbs.store = :store";
            $param[':store'] = $store;
        }
        $person = Yii::app()->user->getId();
        $comm = Yii::app()->db->createCommand("SELECT sr.role FROM nscc_security_roles as sr 
			INNER JOIN nscc_users as u ON u.security_roles_id=sr.security_roles_id
			WHERE u.id='$person'");
        $sr = $comm->queryRow(true);
        $dk = "AND te.kode <> 'DK'";
        if ($sr == "KP")
        	$dk = "";

        $comm = Yii::app()->db->createCommand("
            SELECT
                ne.kode_employee,
                ne.nama_employee,
                Sum(nbs.amount_bonus) AS amount_bonus
            FROM nscc_bonus AS nbs
                LEFT JOIN nscc_employees AS ne ON nbs.employee_id = ne.employee_id
                INNER JOIN nscc_tipe_employee AS te ON te.tipe_employee_id=ne.tipe
            WHERE
                nbs.visible = 1
                $dk
                AND DATE(nbs.tgl) >= :from
                AND DATE(nbs.tgl) <= :to
                $where
            GROUP BY ne.nama_employee
            ORDER BY ne.nama_employee
        ");
        return $comm->queryAll(true, $param);
    }


	static function report_bonus_summary_dokter($from, $to, $store = "", $employee = "")
	{
		$param = array(':from' => $from, ':to' => $to);
		$where = "";
		if ($store != null) {
			$where = "AND nbs.store = :store";
			$param[':store'] = $store;
		}

		if ($employee != "") {
			$empl = " AND nbs.employee_id = :empl";
			$param[':empl'] = $employee;
		}

		$comm = Yii::app()->db->createCommand("
            SELECT
                nc.nama_customer,
                ns.doc_ref,
                sum(nsd.bruto - nsd.total_pot) as net,
                Sum(nbs.amount_bonus) AS amount_bonus
            FROM nscc_bonus AS nbs
            	INNER JOIN nscc_salestrans_details AS nsd ON nbs.trans_no=nsd.salestrans_details
            	INNER JOIN nscc_salestrans AS ns ON ns.salestrans_id=nsd.salestrans_id
                INNER JOIN nscc_bonus_jual AS nbj ON nbj.bonus_jual_id=nbs.bonus_jual_id
                LEFT JOIN nscc_customers as nc ON nc.customer_id=ns.customer_id
                LEFT JOIN nscc_employees AS ne ON nbs.employee_id = ne.employee_id
            WHERE
                nbs.visible = 1
                AND nbj.bonus_name_id=3
                AND DATE(nbs.tgl) >= :from
                AND DATE(nbs.tgl) <= :to
                $where $empl
            GROUP BY ns.doc_ref, nbs.employee_id
            ORDER BY ns.doc_ref
        ");
		return $comm->queryAll(true, $param);
	}

	static function report_bonus_kmt_summary_dokter($from, $to, $store = "", $employee = "")
	{
		$param = array(':from' => $from, ':to' => $to);
		$where = "";
		if ($store != null) {
			$where = "AND nbs.store = :store";
			$param[':store'] = $store;
		}

		if ($employee != "") {
			$empl = " AND nbs.employee_id = :empl";
			$param[':empl'] = $employee;
		}

		$comm = Yii::app()->db->createCommand("
            SELECT
                nc.nama_customer,
                ns.doc_ref,
                b.kode_barang,
                nsd.bruto - nsd.total_pot as net,
                Sum(nbs.amount_bonus) AS amount_bonus
            FROM nscc_bonus AS nbs
            	INNER JOIN nscc_salestrans_details AS nsd ON nbs.trans_no=nsd.salestrans_details
            	INNER JOIN nscc_salestrans AS ns ON ns.salestrans_id=nsd.salestrans_id
                INNER JOIN nscc_bonus_jual AS nbj ON nbj.bonus_jual_id=nbs.bonus_jual_id
                INNER JOIN nscc_barang AS b ON b.barang_id=nsd.barang_id
                INNER JOIN nscc_grup AS g ON g.grup_id=b.grup_id
                LEFT JOIN nscc_customers as nc ON nc.customer_id=ns.customer_id
                LEFT JOIN nscc_employees AS ne ON nbs.employee_id = ne.employee_id
            WHERE
                nbs.visible = 1
                AND g.kategori_id = 1
                AND nbj.bonus_name_id=8
                AND DATE(nbs.tgl) >= :from
                AND DATE(nbs.tgl) <= :to
                $where $empl
            GROUP BY ns.doc_ref, nbs.employee_id, b.barang_id
            ORDER BY ns.doc_ref
        ");
		return $comm->queryAll(true, $param);
	}

    static function report_produksi_summary($from, $to, $store = "", $show_retur, $show_retur_only)
    {
        $cmd = new DbCmd('{{produksi}} p');
        $cmd->addSelect("
                DATE_FORMAT(p.tgl,'%d %b %Y') tgl, 
                p.doc_ref");
        if ($show_retur) {
            $cmd->addSelect('p.doc_ref_produksi');
        }
        $cmd->addSelect("
                b.kode_barang,
                b.nama_barang,
                p.qty,
                u.`name`");
        $cmd->addLeftJoin('{{barang}} b', ' b.barang_id = p.barang_id ');
        $cmd->addLeftJoin('{{users}} u', ' u.id = p.user_id ');
        $cmd->addCondition("DATE(p.tgl) >= '$from' AND DATE(p.tgl) <= '$to'");
        if (!$show_retur && !$show_retur_only) {
            $cmd->addCondition("arus = 1");
        } else if (!$show_retur && $show_retur_only) {
            $cmd->addCondition("arus = -1");
        }
        if ($show_retur_only) {
            $cmd->addCondition("arus = -1");
        }
        $cmd->addOrder('p.tgl');
        return $cmd->queryAll();
    }

    static function report_produksi_details($from, $to, $store = "", $show_retur, $show_retur_only)
    {
        $cmd = new DbCmd('{{produksi_detil}} pd');
        $cmd->addSelect("
                DATE_FORMAT(p.tgl,'%d %b %Y') tgl, 
                p.doc_ref");
        if ($show_retur) {
            $cmd->addSelect('p.doc_ref_produksi');
        }
        $cmd->addSelect("
                b.kode_barang,
                b.nama_barang,
                pd.qty");
        $cmd->addLeftJoin('{{barang}} b', 'b.barang_id = pd.barang_id');
        $cmd->addLeftJoin('{{produksi}} p', 'p.produksi_id = pd.produksi_id');
        $cmd->addCondition("DATE(p.tgl) >= '$from' AND DATE(p.tgl) <= '$to'");
        if (!$show_retur && !$show_retur_only) {
            $cmd->addCondition("arus = 1");
        } else if (!$show_retur && $show_retur_only) {
            $cmd->addCondition("arus = -1");
        }
        if ($show_retur_only) {
            $cmd->addCondition("arus = -1");
        }
        $cmd->addOrder('p.tgl');
        $cmd->getText();
        return $cmd->queryAll();
    }

    static function report_produksi_total($from, $to)
    {
        $cmd = new DbCmd('{{produksi}} np');
        $cmd->addSelect("
                b.kode_barang,b.nama_barang,
                sum(np.qty) qty, b.sat");
        $cmd->addLeftJoin('{{barang}} b', 'b.barang_id = np.barang_id');
        $cmd->addCondition("DATE(np.tgl) >= '$from' AND DATE(np.tgl) <= '$to'");
        $cmd->addGroup('b.kode_barang');
        $comm1 = $cmd->getText();

        $cmddtl = new DbCmd('{{produksi_detil}} npd');
        $cmddtl->addSelect("
                b.kode_barang,b.nama_barang,
                sum(if(np.arus = 1,-npd.qty,npd.qty)) qty, b.sat");
        $cmddtl->addLeftJoin('{{barang}} b', 'b.barang_id = npd.barang_id');
        $cmddtl->addLeftJoin('{{produksi}} np', 'np.produksi_id = npd.produksi_id');
        $cmddtl->addCondition("DATE(np.tgl) >= '$from' AND DATE(np.tgl) <= '$to'");
        $cmddtl->addGroup('b.kode_barang');
        $comm2 = $cmddtl->getText();

        $comm = Yii::app()->db->createCommand("
                select * from ($comm1 UNION $comm2) t
                ORDER BY t.kode_barang
            ");
        return $comm->queryAll();
    }

    static function arus_kas_masuk_keluar($from, $to, $mode, $store = "")
    {
        $param = array(':from' => $from, ':to' => $to);
        $where = "";
        if ($store != null) {
            $where = "AND nbs.store = :store";
            $param[':store'] = $store;
        }
        $whereMode = "";
        if ($mode == -1) {
            $whereMode = "AND ngt.amount < 0";
        } else {
            $whereMode = "AND ngt.amount >= 0";
        }
        $comm = Yii::app()->db->createCommand("
            SELECT ngt.account_code,ncm.account_name,
            ABS(SUM(IFNULL(ngt.amount,0))) amount
            FROM nscc_gl_trans AS ngt
            INNER JOIN nscc_chart_master AS ncm ON ncm.account_code = ngt.account_code
            WHERE ngt.cf = 1 AND ngt.tran_date >= :from AND ngt.tran_date <= :to 
            $whereMode $where
            GROUP BY ngt.account_code
        ");
        return $comm->queryAll(true, $param);
    }

    static function posisi_saldo_before($from, $store = "")
    {
        $param = array(':from' => $from);
        $where = "";
        if ($store != null) {
            $where = "AND nbs.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
            SELECT nb.nama_bank, ABS(SUM(nbt.amount)) amount
            FROM nscc_bank_trans AS nbt
            INNER JOIN nscc_bank AS nb ON nb.bank_id = nbt.bank_id
            WHERE nbt.tgl < :from AND nbt.visible = 1 $where
            GROUP BY nb.bank_id
        ");
        return $comm->queryAll(true, $param);
    }

    static function report_bonus_details($from, $to, $store = "")
    {
        $param = array(':from' => $from, ':to' => $to);
        $where = "";
        if ($store != null) {
            $where = "AND nbs.store = :store";
            $param[':store'] = $store;
        }
        $comm = Yii::app()->db->createCommand("
            SELECT
                nb.kode_barang,
                DATE_FORMAT(nbs.tgl, '%d %b %Y') AS tgl,
                ns.doc_ref,
                nc.nama_customer,
                nbs.amount_bonus,
                ne.kode_employee,
                ne.nama_employee,
                nbn.bonus_name,
                nj.price
            FROM nscc_bonus AS nbs
                LEFT JOIN nscc_salestrans_details AS nsd ON nbs.trans_no = nsd.salestrans_details
                LEFT JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
                LEFT JOIN nscc_barang AS nb ON nbs.barang_id = nb.barang_id
                LEFT JOIN nscc_jual AS nj ON nj.barang_id = nb.barang_id
                LEFT JOIN nscc_customers AS nc ON ns.customer_id = nc.customer_id
                LEFT JOIN nscc_employees AS ne ON nbs.employee_id = ne.employee_id
                LEFT JOIN nscc_bonus_jual AS nbj ON nbs.bonus_jual_id = nbj.bonus_jual_id
                LEFT JOIN nscc_bonus_name AS nbn ON nbj.bonus_name_id = nbn.bonus_name_id
            WHERE
                nbs.visible = 1
                AND DATE(nbs.tgl) >= :from
                AND DATE(nbs.tgl) <= :to
                $where
            ORDER BY ne.nama_employee
        ");
        return $comm->queryAll(true, $param);
    }
    static function report_sales_summary_receipt_doctor($from, $to, $store = "", $dokterId = "") {
        $where = "";
        $dokter = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        if ($dokterId != null) {
            $dokter = "AND ns.dokter_id = :dokter_id";
            $param[':dokter_id'] = $dokterId;
        }        
        $comm = Yii::app()->db->createCommand("SELECT DATE_FORMAT(ns.tgl, '%d %b %Y') AS tgl,
  ns.doc_ref,ns.ketdisc, nc.no_customer, nc.nama_customer, ns.total, nb.nama_bank, SUM(np.amount) amount,np.kembali
FROM nscc_salestrans ns
  INNER JOIN nscc_customers nc
    ON ns.customer_id = nc.customer_id
  LEFT JOIN nscc_payment np
    ON np.salestrans_id = ns.salestrans_id
  LEFT JOIN nscc_bank nb
    ON np.bank_id = nb.bank_id
WHERE DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to AND ns.type_ = 1 $where $dokter
GROUP BY ns.doc_ref, nb.nama_bank, np.amount");
        return $comm->queryAll(true, $param);
    }    
    /* get data restiction date input from preferences */
    static function report_sales_summary_receipt_amount_total_doctor($from, $to, $store = "",$dokterId = "" )
    {
        $where = "";
        $dokter = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($store != null) {
            $where = "AND ns.store = :store";
            $param[':store'] = $store;
        }
        if ($dokterId != null) {
            $dokter = "AND ns.dokter_id = :dokter_id";
            $param[':dokter_id'] = $dokterId;
        }
        $comm = Yii::app()->db->createCommand("SELECT IFNULL(SUM(ns.total),0) AS total
FROM nscc_salestrans ns
WHERE DATE(ns.tgl) >= :from AND DATE(ns.tgl) <= :to $where $dokter");
        $comm->setFetchMode(PDO::FETCH_OBJ);
        return $comm->queryRow(true, $param);
    } 
    
    public static function getResDate()
    {
        $model = $model = SysPrefs::model()->findByAttributes(array('name_' => 'res_date'));
        if ($model != null) {
            return $model->value_;
        } else {
            return false;
        }
    }

    static function runCommand($command, $action, $id, $log)
    {
        console($command, $action, $id, $log);
        /*  $yiic = dirname(__file__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'yiic';
          if (substr(php_uname(), 0, 7) == "Windows") {
          exec("start /B php $yiic soap $action --id=$id");
          } else {
          exec("php $yiic soap $action --id=$id");
          } */
    }

    static function runSync($command, $action, $cabang, $start, $end, $log)
    {
        consoleSync($command, $action, $cabang, $start, $end, $log);

        //console($command, $action, $id, $log);
    }

    static function runHistory($command, $action, $from, $to, $log)
    {
        console($command, $action, $from, $to, $log);
        /*  $yiic = dirname(__file__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'yiic';
          if (substr(php_uname(), 0, 7) == "Windows") {
          exec("start /B php $yiic soap $action --id=$id");
          } else {
          exec("php $yiic soap $action --id=$id");
          } */
    }

    static function runCommandSync($action, $id)
    {
        console('cron', $action, $id, 'syncpusat.log');
    }

    static function runCommandNars($action, $id)
    {
        console('uploadnars', $action, $id, 'nars.log');
    }

    static function runConsole($command, $action, $id)
    {
        console($command, $action, $id, 'runConsole.log');
    }

//    static function runCommandAdwis($action, $id)
//    {
//        console('uploadadwis', $action, $id, 'protected/runtime/adwis.log');
//    }
    static function createReference($type)
    {
        $systype = SysTypes::model()->find("type_id = :type_id", array(':type_id' => $type));
        if ($systype == null) {
            throw new Exception("Doc. Ref not found.");
        } else {
            if (preg_match("/(.+)\/\d{2}-\d{2}-(\d{2})\/(\d+)/", $systype->next_reference, $result) == 1) {
                list($all, $prefix, $year, $number) = $result;
                $val = intval($number + 1);
                $year_new = date("y");
                if ($year != $year_new) {
                    $val = 1;
                }
                $dig_count = strlen($number);
                $nextval = sprintf('%0' . $dig_count . 'd', $val);
                $systype->next_reference = $prefix . "/" . date("d-m-y") . "/" . $nextval;
            }
            if (!$systype->save()) {
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'System types')) .
                    CHtml::errorSummary($systype));
            } else {
                return $systype->next_reference;
            }
        }
    }

    //projection

    //------------- projection report

    //sales override
    static function report_proyeksitransaksioverride_bep($from, $to, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);

            $comm = Yii::app()->db
                ->createCommand("select tc.category_name, SUM(tro.amount) as total from transaksi_override tro 
                                join transaksi_category tc on tro.category_id = tc.category_id
                                WHERE DATE(tro.tdate) >= :from AND DATE(tro.tdate) <= :to AND tro.flag = '1' AND tro.businessunit_id = '$bu'
                                group by tc.category_name");

        /*select tc.category_name, SUM(tro.amount) as total from transaksi_override tro
                                join transaksi_category tc on tro.category_id = tc.category_id
                                WHERE DATE(tro.tdate) >= :from AND DATE(tro.tdate) <= :to AND tro.flag = '1' AND tro.businessunit_id = '$bu'
                                group by tc.category_name*/

        return $comm->queryAll(true, $param);
    }

    //sales override
    static function report_proyeksitransaksioverride($from, $to, $bu, $showdata)
    {
        $query = "";
        $where = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);


        if($showdata == 'D')
        {
            $comm = Yii::app()->db
                ->createCommand("select * from transaksi_override
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to AND flag = '1' AND businessunit_id = '$bu'
                                order by docref, tdate DESC");
        }
        else{
            $comm = Yii::app()->db
                ->createCommand("select transaksi_id, businessunit_id, docref, name, description, DATE_FORMAT(tdate, '%b %Y') as tdate, sum(amount) as amount, flag from transaksi_override
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to AND flag = '1' AND businessunit_id = '$bu'
                                group by MONTH(tdate) order by docref, tdate DESC");
        }

        /*$comm = Yii::app()->db
            ->createCommand("select * from transaksi_override
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to AND flag = '1' AND businessunit_id = '$bu'
                                order by docref, tdate DESC");*/

        /*select transaksi_id, businessunit_id, docref, name, description, DATE_FORMAT(tdate, '%b %Y') as tdate, sum(amount) as amont, flag from transaksi_override
              where businessunit_id = 'c73e49c2-1616-11e8-a32f-201a069f4b32'
                                group by MONTH(tdate)
                                order by tdate ASC */


        return $comm->queryAll(true, $param);
    }

    //sales
    static function report_proyeksitransaksi($from, $to, $outlet, $grup, $produk, $showby, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);

        if ($showby == "Outlet") {
            $where = "AND kodeoutlet = :outlet ";
            $param[':outlet'] = $outlet;
        }
        if ($showby == "GroupProduct") {
            $where = "AND kodegroup = :kodegroup ";
            $param[':kodegroup'] = $grup;
        }
        if ($showby == "Product") {
            $where = "AND kodeproduk = :kodeproduk ";
            $param[':kodeproduk'] = $produk;
        }

        $comm = Yii::app()->db
            ->createCommand("select * from transaksi_detail_view
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to AND flag = '1' $where
                                AND businessunit_id = '$bu'
                                order by docref, tdate DESC ");

        return $comm->queryAll(true, $param);
    }

    //budget plan
    static function report_budget_plan($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select * from budget_view
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
                                AND businessunit_id = '$bu'
                                ORDER BY account_code ASC");
        } else {
            $comm = Yii::app()->db->createCommand("select * from budget_view
            WHERE account_id = '$id' 
            AND businessunit_id = '$bu'
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //budget plan permonth
    static function report_budget_plan_permonth($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND a.account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.amount else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.amount else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.amount else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.amount else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.amount else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.amount else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.amount else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.amount else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.amount else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.amount else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.amount else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.amount else 0 end) december,
                                    sum(b.amount) total
                                from budget b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                where DATE(b.tdate) >= :from
                                        and DATE(b.tdate) <= :to
                                        and b.businessunit_id = '$bu' $where
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        }
        else
        {
            $comm = Yii::app()->db->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.amount else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.amount else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.amount else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.amount else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.amount else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.amount else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.amount else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.amount else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.amount else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.amount else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.amount else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.amount else 0 end) december,
                                    sum(b.amount) total
                                from budget b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE a.account_id = '$id'
                                    and b.businessunit_id = '$bu'
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        }

        return $comm->queryAll(true, $param);
    }

    //budget realization
    static function report_budget_realization($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select * from realization_view
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
                                AND businessunit_id = '$bu'
                                ORDER BY account_code ASC");
        } else {
            $comm = Yii::app()->db->createCommand("select * from realization_view
            WHERE account_id = '$id' 
            AND businessunit_id = '$bu'
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //budget realization permonth
    static function report_budget_realization_permonth($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND a.account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.amount else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.amount else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.amount else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.amount else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.amount else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.amount else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.amount else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.amount else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.amount else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.amount else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.amount else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.amount else 0 end) december,
                                    sum(b.amount) total
                                from realization b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                where DATE(b.tdate) >= :from
                                        and DATE(b.tdate) <= :to
                                        and b.businessunit_id = '$bu' $where
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        }
        else
        {
            $comm = Yii::app()->db->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.amount else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.amount else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.amount else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.amount else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.amount else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.amount else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.amount else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.amount else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.amount else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.amount else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.amount else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.amount else 0 end) december,
                                    sum(b.amount) total
                                from realization b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE a.account_id = '$id'
                                    and b.businessunit_id = '$bu'
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        }

        return $comm->queryAll(true, $param);
    }

    //budget analysis
    static function report_budget_analysis($acc, $month, $year, $from, $to ,$showby, $bu)
    {

        $join1 = DbCmd::instance()
            ->addSelect("g.account_id , sum(g.amount) as amount")
            ->addFrom("realization g")
            ->addCondition("g.businessunit_id = :businessunit_id")
            ->addGroup("g.account_id");

        $join2 = DbCmd::instance()
            ->addSelect("b.account_id , sum(b.amount) as amount")
            ->addFrom("budget b")
            ->addCondition("b.businessunit_id = :businessunit_id")
            ->addGroup("b.account_id");

        $cmd = DbCmd::instance()
            ->addSelect("a.businessunit_id, a.account_code as 'AccountCode', a.account_name as 'AccountName',
            IFNULL(b.amount,0) as 'AmountBudget', 
            IFNULL(gl.amount,0) as 'AmountRealization' , 
            IFNULL((b.amount - gl.amount),0) as 'Achievement',
            IFNULL(Round((gl.amount/b.amount*100),0),0) as 'AchievementPercent'")
            ->addFrom( "account a");


        if($showby == 'Year')
        {
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }
        }
        elseif ($showby == 'Month & Year')
        {
            if ($month != "")
            {
                $join1->addCondition("MONTH(g.tdate) = :month");
                $join2->addCondition("MONTH(b.tdate) = :month");
                $cmd->addParams([':month' => $month]);
            }
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }

        }
        elseif($showby == 'Account')
        {
            if ($acc != "") {
                $join1->addCondition("g.account_id = :account_id");
                $join2->addCondition("b.account_id = :account_id");
                $cmd->addCondition("a.account_id = :account_id")
                    ->addParams([':account_id' => $acc]);
            }
        }
        elseif($showby == 'Date')
        {
            if ($from != "")
            {
                $join1->addCondition("DATE(g.tdate) >= :date_start");
                $join2->addCondition("DATE(b.tdate) >= :date_start");
                $cmd->addParams([':date_start' => $from]);
            }

            if ($to != "")
            {
                $join1->addCondition("DATE(g.tdate) <= :date_end");
                $join2->addCondition("DATE(b.tdate) <= :date_end");
                $cmd->addParams([':date_end' => $to]);
            }
        }

        $cmd ->addLeftJoin($join1->setAs("gl"),"a.account_id = gl.account_id")
            ->addLeftJoin($join2->setAs("b"),"a.account_id = b.account_id")
            ->addCondition("a.businessunit_id = :businessunit_id")
            ->addParams([':businessunit_id' => $bu])
            ->addOrder('a.account_code asc');

        //$cmd->setConnection(Yii::app()->dbprojection);
        return $cmd->queryAll();
    }

    //investment plan
    static function report_investment_plan($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";


        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select *, (select sum(total) from investasi_view ii join account aa on aa.account_id=ii.account_id join `group` gg on gg.group_id = aa.group_id where gg.group_id = a.group_id) as alltotal 
                                from investasi_view i
                                join account a on a.account_id = i.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE DATE(i.tdate) >= :from
                                AND DATE(i.tdate) <= :to
                                AND i.businessunit_id = '$bu'
                                ORDER BY g.group_name, i.account_code ASC");



            /*Select *, (select sum(total) from investasi_view where account_code = i.account_code) as alltotal from investasi_view i
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
            AND businessunit_id = '$bu'
                                ORDER BY account_code ASC*/

        } else {
            $comm = Yii::app()->db->createCommand("select *, (select sum(total) from investasi_view where account_code = i.account_code) as alltotal from investasi_view i
            WHERE account_id = '$id' 
            AND businessunit_id = '$bu'
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //investment plan permonth
    static function report_investment_plan_permonth($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND a.account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.total else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.total else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.total else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.total else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.total else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.total else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.total else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.total else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.total else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.total else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.total else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.total else 0 end) december,
                                    sum(b.total) total
                                from investasi b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                where DATE(b.tdate) >= :from
                                        and DATE(b.tdate) <= :to
                                        and b.businessunit_id = '$bu' $where
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        }
        else
        {
            $comm = Yii::app()->db->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.total else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.total else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.total else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.total else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.total else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.total else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.total else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.total else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.total else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.total else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.total else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.total else 0 end) december,
                                    sum(b.total) total
                                from investasi b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE a.account_id = '$id'
                                    and b.businessunit_id = '$bu'
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        }

        return $comm->queryAll(true, $param);
    }

    //investment realization
    static function report_investment_realization($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";


        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select *, (select sum(total) from investasi_realisasi_view ii join account aa on aa.account_id=ii.account_id join `group` gg on gg.group_id = aa.group_id where gg.group_id = a.group_id) as alltotal 
                                from investasi_realisasi_view i
                                join account a on a.account_id = i.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE DATE(i.tdate) >= :from
                                AND DATE(i.tdate) <= :to
                                AND i.businessunit_id = '$bu'
                                ORDER BY g.group_name, i.account_code ASC");


                        /*Select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal
            from investasi_realisasi_view i
            join account a on a.account_id = i.account_id
            join `group` g on g.group_id = a.group_id
            WHERE DATE(i.tdate) >= :from
                        AND DATE(i.tdate) <= :to
                        AND i.businessunit_id = '$bu'
            ORDER BY g.group_name, i.account_code ASC*/

            /*Select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal from investasi_realisasi_view i
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
            AND businessunit_id = '$bu'
                                ORDER BY account_code ASC*/

        } else {
            $comm = Yii::app()->db->createCommand("select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal from investasi_realisasi_view i
            WHERE account_id = '$id' 
            AND businessunit_id = '$bu'
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //investment realization permonth
    static function report_investment_realization_permonth($from, $to, $id, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND a.account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.total else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.total else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.total else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.total else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.total else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.total else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.total else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.total else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.total else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.total else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.total else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.total else 0 end) december,
                                    sum(b.total) total
                                from investasi_realisasi b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                where DATE(b.tdate) >= :from
                                        and DATE(b.tdate) <= :to
                                        and b.businessunit_id = '$bu' $where
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        }
        else
        {
            $comm = Yii::app()->db->createCommand("select g.group_name, a.account_name,
                                    sum(case when month(b.tdate) = 1 then b.total else 0 end) january,
                                    sum(case when month(b.tdate) = 2 then b.total else 0 end) february,
                                    sum(case when month(b.tdate) = 3 then b.total else 0 end) march,
                                    sum(case when month(b.tdate) = 4 then b.total else 0 end) april,
                                    sum(case when month(b.tdate) = 5 then b.total else 0 end) may,
                                    sum(case when month(b.tdate) = 6 then b.total else 0 end) june,
                                    sum(case when month(b.tdate) = 7 then b.total else 0 end) july,
                                    sum(case when month(b.tdate) = 8 then b.total else 0 end) august,
                                    sum(case when month(b.tdate) = 9 then b.total else 0 end) september,
                                    sum(case when month(b.tdate) = 10 then b.total else 0 end) oktober,
                                    sum(case when month(b.tdate) = 11 then b.total else 0 end) november,
                                    sum(case when month(b.tdate) = 12 then b.total else 0 end) december,
                                    sum(b.total) total
                                from investasi_realisasi b
                                join account a on b.account_id = a.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE a.account_id = '$id'
                                    and b.businessunit_id = '$bu'
                                group by a.account_name, g.group_name
                                order by g.group_name, a.account_code asc");
        }

        return $comm->queryAll(true, $param);
    }

    //investment analysis
    static function report_investment_analysis($acc, $month, $year, $from, $to ,$showby, $bu)
    {

        $join1 = DbCmd::instance()
            ->addSelect("g.account_id , sum(g.total) as amount")
            ->addFrom("investasi_realisasi g")
            ->addCondition("g.businessunit_id = :businessunit_id")
            ->addGroup("g.account_id");

        $join2 = DbCmd::instance()
            ->addSelect("b.account_id , sum(b.total) as amount")
            ->addFrom("investasi b")
            ->addCondition("b.businessunit_id = :businessunit_id")
            ->addGroup("b.account_id");

        $cmd = DbCmd::instance()
            ->addSelect("a.businessunit_id, a.account_code as 'AccountCode', a.account_name as 'AccountName',
            IFNULL(b.amount,0) as 'AmountBudget', 
            IFNULL(gl.amount,0) as 'AmountRealization' , 
            IFNULL((b.amount - gl.amount),0) as 'Achievement',
            IFNULL(Round((gl.amount/b.amount*100),0),0) as 'AchievementPercent'")
            ->addFrom( "account a");


        if($showby == 'Year')
        {
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }
        }
        elseif ($showby == 'Month & Year')
        {
            if ($month != "")
            {
                $join1->addCondition("MONTH(g.tdate) = :month");
                $join2->addCondition("MONTH(b.tdate) = :month");
                $cmd->addParams([':month' => $month]);
            }
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }

        }
        elseif($showby == 'Account')
        {
            if ($acc != "") {
                $join1->addCondition("g.account_id = :account_id");
                $join2->addCondition("b.account_id = :account_id");
                $cmd->addCondition("a.account_id = :account_id")
                    ->addParams([':account_id' => $acc]);
            }
        }
        elseif($showby == 'Date')
        {
            if ($from != "")
            {
                $join1->addCondition("DATE(g.tdate) >= :date_start");
                $join2->addCondition("DATE(b.tdate) >= :date_start");
                $cmd->addParams([':date_start' => $from]);
            }

            if ($to != "")
            {
                $join1->addCondition("DATE(g.tdate) <= :date_end");
                $join2->addCondition("DATE(b.tdate) <= :date_end");
                $cmd->addParams([':date_end' => $to]);
            }
        }

        $cmd ->addLeftJoin($join1->setAs("gl"),"a.account_id = gl.account_id")
            ->addLeftJoin($join2->setAs("b"),"a.account_id = b.account_id")
            ->addCondition("a.businessunit_id = :businessunit_id")
            ->addParams([':businessunit_id' => $bu])
            ->addOrder('a.account_code asc');

        //$cmd->setConnection(Yii::app()->dbprojection);
        return $cmd->queryAll();
    }


    //------------------------------------------LABARUGI
    //BEP
    static function report_bep($from, $to, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);


        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("");
        }
        else
        {
            $comm = Yii::app()->db->createCommand("");
        }

        return $comm->queryAll(true, $param);
    }

    //budget realization bep
    static function report_budget_realization_bep($from, $to, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);

        $comm = Yii::app()->db
            ->createCommand("select gr.group_name,  a.account_name, SUM(r.amount) total from `group` gr
                                join account a on a.group_id = gr.group_id
                                join realization r on a.account_id = r.account_id
                                WHERE DATE(r.tdate) >= :from
                                AND DATE(r.tdate) <= :to
                                AND r.businessunit_id = '$bu'
                                group by a.account_code, gr.group_code
                                order by gr.group_name asc");


        return $comm->queryAll(true, $param);
    }

    //investment realization bep
    static function report_investment_realization_bep($from, $to, $bu)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);

        $comm = Yii::app()->db
            ->createCommand("Select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal from investasi_realisasi_view i
                                WHERE DATE(tdate) >= :from 
                                AND DATE(tdate) <= :to 
                                AND businessunit_id = '$bu'
                                ORDER BY account_code ASC");

        /*Select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal from investasi_realisasi_view i
                                WHERE YEAR(tdate) = :from
                                AND businessunit_id = '$bu'
                                ORDER BY account_code ASC*/

        return $comm->queryAll(true, $param);
    }




    //-----------------------------------------------ALL BU

    //budget plan
    static function report_budget_plan_all($from, $to, $id)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select * from budget_view
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
                                ORDER BY account_code ASC");
        } else {
            $comm = Yii::app()->db->createCommand("select * from budget_view
            WHERE account_id = '$id' 
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //budget realization
    static function report_budget_realization_all($from, $to, $id)
    {
        $query = "";
        $where = "";
        $comm = "";

        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select * from realization_view
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
                                ORDER BY account_code ASC");
        } else {
            $comm = Yii::app()->db->createCommand("select * from realization_view
            WHERE account_id = '$id' 
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //budget analysis
    static function report_budget_analysis_all($acc, $month, $year, $from, $to ,$showby)
    {
        $join1 = DbCmd::instance()
            ->addSelect("g.account_id , sum(g.amount) as amount")
            ->addFrom("realization g")
            ->addGroup("g.account_id");

        $join2 = DbCmd::instance()
            ->addSelect("b.account_id , sum(b.amount) as amount")
            ->addFrom("budget b")
            ->addGroup("b.account_id");

        $cmd = DbCmd::instance()
            ->addSelect("a.businessunit_id, a.account_code as 'AccountCode', a.account_name as 'AccountName',
            IFNULL(b.amount,0) as 'AmountBudget', 
            IFNULL(gl.amount,0) as 'AmountRealization' , 
            IFNULL((b.amount - gl.amount),0) as 'Achievement',
            IFNULL(Round((gl.amount/b.amount*100),0),0) as 'AchievementPercent'")
            ->addFrom( "account a");


        if($showby == 'Year')
        {
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }
        }
        elseif ($showby == 'Month & Year')
        {
            if ($month != "")
            {
                $join1->addCondition("MONTH(g.tdate) = :month");
                $join2->addCondition("MONTH(b.tdate) = :month");
                $cmd->addParams([':month' => $month]);
            }
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }

        }
        elseif($showby == 'Account')
        {
            if ($acc != "") {
                $join1->addCondition("g.account_id = :account_id");
                $join2->addCondition("b.account_id = :account_id");
                $cmd->addCondition("a.account_id = :account_id")
                    ->addParams([':account_id' => $acc]);
            }
        }
        elseif($showby == 'Date')
        {
            if ($from != "")
            {
                $join1->addCondition("DATE(g.tdate) >= :date_start");
                $join2->addCondition("DATE(b.tdate) >= :date_start");
                $cmd->addParams([':date_start' => $from]);
            }

            if ($to != "")
            {
                $join1->addCondition("DATE(g.tdate) <= :date_end");
                $join2->addCondition("DATE(b.tdate) <= :date_end");
                $cmd->addParams([':date_end' => $to]);
            }
        }

        $cmd ->addLeftJoin($join1->setAs("gl"),"a.account_id = gl.account_id")
            ->addLeftJoin($join2->setAs("b"),"a.account_id = b.account_id")
            ->addOrder('a.account_code asc');

        //$cmd->setConnection(Yii::app()->dbprojection);
        return $cmd->queryAll();
    }

    //investment plan
    static function report_investment_plan_all($from, $to, $id)
    {
        $query = "";
        $where = "";
        $comm = "";


        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select *, (select sum(total) from investasi_view ii join account aa on aa.account_id=ii.account_id join `group` gg on gg.group_id = aa.group_id where gg.group_id = a.group_id) as alltotal 
                                from investasi_view i
                                join account a on a.account_id = i.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE DATE(i.tdate) >= :from
                                AND DATE(i.tdate) <= :to
                                ORDER BY g.group_name, i.account_code ASC");

            /*Select *, (select sum(total) from investasi_view where account_code = i.account_code) as alltotal from investasi_view i
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
                                ORDER BY account_code ASC*/

        } else {
            $comm = Yii::app()->db->createCommand("select *, (select sum(total) from investasi_view where account_code = i.account_code) as alltotal from investasi_view i
            WHERE account_id = '$id' 
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //investment realization
    static function report_investment_realization_all($from, $to, $id)
    {
        $query = "";
        $where = "";
        $comm = "";


        $param = array(':from' => $from, ':to' => $to);
        if ($id != null) {
            $where = "AND account_id = :id ";
            $param[':id'] = $id;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db
                ->createCommand("Select *, (select sum(total) from investasi_realisasi_view ii join account aa on aa.account_id=ii.account_id join `group` gg on gg.group_id = aa.group_id where gg.group_id = a.group_id) as alltotal 
                                from investasi_realisasi_view i
                                join account a on a.account_id = i.account_id
                                join `group` g on g.group_id = a.group_id
                                WHERE DATE(i.tdate) >= :from
                                AND DATE(i.tdate) <= :to
                                ORDER BY g.group_name, i.account_code ASC");

            /*Select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal from investasi_realisasi_view i
                                WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to $where
                                ORDER BY account_code ASC*/

        } else {
            $comm = Yii::app()->db->createCommand("select *, (select sum(total) from investasi_realisasi_view where account_code = i.account_code) as alltotal from investasi_realisasi_view i
            WHERE account_id = '$id' 
            ORDER BY account_code ASC");
        }

        return $comm->queryAll(true, $param);
    }

    //investment analysis
    static function report_investment_analysis_all($acc, $month, $year, $from, $to ,$showby)
    {

        $join1 = DbCmd::instance()
            ->addSelect("g.account_id , sum(g.amount) as amount")
            ->addFrom("investasi_realisasi g")
            ->addGroup("g.account_id");

        $join2 = DbCmd::instance()
            ->addSelect("b.account_id , sum(b.amount) as amount")
            ->addFrom("investasi b")
            ->addGroup("b.account_id");

        $cmd = DbCmd::instance()
            ->addSelect("a.businessunit_id, a.account_code as 'AccountCode', a.account_name as 'AccountName',
            IFNULL(b.amount,0) as 'AmountBudget', 
            IFNULL(gl.amount,0) as 'AmountRealization' , 
            IFNULL((b.amount - gl.amount),0) as 'Achievement',
            IFNULL(Round((gl.amount/b.amount*100),0),0) as 'AchievementPercent'")
            ->addFrom( "account a");


        if($showby == 'Year')
        {
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }
        }
        elseif ($showby == 'Month & Year')
        {
            if ($month != "")
            {
                $join1->addCondition("MONTH(g.tdate) = :month");
                $join2->addCondition("MONTH(b.tdate) = :month");
                $cmd->addParams([':month' => $month]);
            }
            if ($year != "") {
                $join1->addCondition("YEAR(g.tdate) = :year");
                $join2->addCondition("YEAR(b.tdate) = :year");
                $cmd->addParams([':year' => $year]);
            }

        }
        elseif($showby == 'Account')
        {
            if ($acc != "") {
                $join1->addCondition("g.account_id = :account_id");
                $join2->addCondition("b.account_id = :account_id");
                $cmd->addCondition("a.account_id = :account_id")
                    ->addParams([':account_id' => $acc]);
            }
        }
        elseif($showby == 'Date')
        {
            if ($from != "")
            {
                $join1->addCondition("DATE(g.tdate) >= :date_start");
                $join2->addCondition("DATE(b.tdate) >= :date_start");
                $cmd->addParams([':date_start' => $from]);
            }

            if ($to != "")
            {
                $join1->addCondition("DATE(g.tdate) <= :date_end");
                $join2->addCondition("DATE(b.tdate) <= :date_end");
                $cmd->addParams([':date_end' => $to]);
            }
        }

        $cmd ->addLeftJoin($join1->setAs("gl"),"a.account_id = gl.account_id")
            ->addLeftJoin($join2->setAs("b"),"a.account_id = b.account_id")
            ->addOrder('a.account_code asc');

        //$cmd->setConnection(Yii::app()->dbprojection);
        return $cmd->queryAll();
    }



    //----------------------------------------------IMPORT DATA PROJECTION

    static function item_import($data, $trans_date, $usr, $bu)
    {
        //$command = Unit::model()->dbConnection->createCommand("SELECT UUID();");
        //$trans_no = $command->queryScalar();
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($data as $dt) {
                //$chck = AssetBarang::model()->findByAttributes(array('kode_barang' => $dt['kode'], 'businessunit_id' => $bu));
                $chck = AssetBarang::model()->findByAttributes(array('kode_barang' => $dt['kode']));

                if (!$chck)
                {
                    //continue;
                    //                    throw new Exception("Data was not saved.<br>Kode Barang ".$dt['kode_barang']." has not been registered.");
                    $item = new AssetBarang;
                    $item->kode_barang = $dt['kode'];
                    $item->nama_barang = $dt['nama'];
                    $item->ket = $dt['deskripsi'];
                    //$item->created_at = new CDbExpression('NOW()');//date('Y-m-d', strtotime($trans_date));
                    $item->businessunit_id = $bu;
                    //$unit->uploadfile = $usr;
                    //$unit->uploaddate = new CDbExpression('NOW()');

                    if (!$item->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetBarang')) . CHtml::errorSummary($item));

                }
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        finally
        {
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }


    static function branch_import($data, $trans_date, $usr, $bu)
    {
        //$command = Unit::model()->dbConnection->createCommand("SELECT UUID();");
        //$trans_no = $command->queryScalar();
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($data as $dt) {
                //$chck = AssetBarang::model()->findByAttributes(array('kode_barang' => $dt['kode'], 'businessunit_id' => $bu));
                $chck = Store::model()->findByAttributes(array('store_kode' => $dt['kode'], 'businessunit_id' => $bu));

                if (!$chck)
                {
                    //continue;
                    //                    throw new Exception("Data was not saved.<br>Kode Barang ".$dt['kode_barang']." has not been registered.");
                    $store = new Store;
                    $store->store_kode = $dt['kode'];
                    $store->nama_store = $dt['nama'];
                    $store->alamat = $dt['alamat'];
                    //$item->created_at = new CDbExpression('NOW()');//date('Y-m-d', strtotime($trans_date));
                    $store->businessunit_id = $bu;
                    //$unit->uploadfile = $usr;
                    //$unit->uploaddate = new CDbExpression('NOW()');

                    if (!$store->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Store')) . CHtml::errorSummary($store));

                }
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        finally
        {
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    static function category_import($data, $trans_date, $usr, $bu)
    {
        //$command = Unit::model()->dbConnection->createCommand("SELECT UUID();");
        //$trans_no = $command->queryScalar();
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($data as $dt) {
                //$chck = AssetBarang::model()->findByAttributes(array('kode_barang' => $dt['kode'], 'businessunit_id' => $bu));
                $chck = AssetCategory::model()->findByAttributes(array('category_code' => $dt['kode']));

                if (!$chck)
                {
                    //continue;
                    //                    throw new Exception("Data was not saved.<br>Kode Barang ".$dt['kode_barang']." has not been registered.");
                    $cat = new AssetCategory;
                    $cat->category_code = $dt['kode'];
                    $cat->category_name = $dt['nama'];
                    $cat->category_desc = $dt['deskripsi'];
                    //$item->created_at = new CDbExpression('NOW()');//date('Y-m-d', strtotime($trans_date));
                    $cat->businessunit_id = $bu;
                    //$unit->uploadfile = $usr;
                    //$unit->uploaddate = new CDbExpression('NOW()');

                    if (!$cat->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetCategory')) . CHtml::errorSummary($cat));

                }
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        finally
        {
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    static function class_import($data, $trans_date, $usr, $bu)
    {
        //$command = Unit::model()->dbConnection->createCommand("SELECT UUID();");
        //$trans_no = $command->queryScalar();
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($data as $dt) {
                //$chck = AssetBarang::model()->findByAttributes(array('kode_barang' => $dt['kode'], 'businessunit_id' => $bu));
                $chck = AssetGroup::model()->findByAttributes(array('golongan' => $dt['golongan']));

                if (!$chck)
                {
                    $gol = new AssetGroup;
                    $gol->golongan = $dt['golongan'];
                    $gol->tariff = $dt['tariff'];
                    $gol->period = $dt['period'];
                    $gol->desc = $dt['desc'];
                    $gol->businessunit_id = $bu;

                    if (!$gol->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetGroup')) . CHtml::errorSummary($gol));

                }
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        finally
        {
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    static function businessunit_import($data, $trans_date, $usr)
    {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            foreach ($data as $dt) {
                $chck = Businessunit::model()->findByAttributes(array('businessunit_code' => $dt['kode']));
                if (!$chck)
                {
                    //continue;
//                    throw new Exception("Data was not saved.<br>Kode Barang ".$dt['kode_barang']." has not been registered.");

                    $bu = new Businessunit;
                    $bu->businessunit_code = $dt['kode'];
                    $bu->businessunit_name = $dt['nama'];
                    $bu->type = $dt['tipe'];
                    $bu->description = $dt['deskripsi'];
                    $bu->created_at = new CDbExpression('NOW()');
                    $bu->uploadfile = $usr;
                    $bu->uploaddate = new CDbExpression('NOW()');
                    if (!$bu->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Businessunit')) . CHtml::errorSummary($bu));
                }
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        finally
        {
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }


    public function asset_import($data, $trans_date, $usr, $bu, $migrasi)
    {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();

        $dupArray = array();
        $temp = "";

        try {
            foreach ($data as $dt)
            {
                if($migrasi) {
                    $chck = AssetDetail::model()->findByAttributes(array('ati' => $dt['activaold'], 'businessunit_id' => $bu, 'active' => '1'));
                }
                else{
                    $chck = 0;//AssetDetail::model()->findByAttributes(array('ati_old' => $dt['activaold'], 'businessunit_id' => $bu));
                }


                if (!$chck) //cek duplikat no activa lama
                {
                    if($migrasi)
                    {
                        $qty = 1;
                        $activaold = $dt['activaold'];
                    }
                    else{
                        $qty = $dt['qty'];
                        $activaold = "-";
                    }

                    $item = $dt['itemcode'];
                    $branch = $dt['branch'];
                    $tempdate = strtotime($dt['date']);
                    $date = date('Y-m-d',$tempdate);
                    $price = $dt['price'];
                    $class = $dt['class'];
                    $category = $dt['category'];
                    $desc = $dt['desc'];
                    $hide = $dt['hide'];
                    $ppn = $price *10/100;

                    $groupclass = AssetGroup::model()->findByAttributes(array('golongan' => $class));
                    if($groupclass == "")
                    {
                        $status = false;
                        $msg = "Class '" . $class . "' tidak ada di database";
                    }
                    $barang = AssetBarang::model()->findByAttributes(array('kode_barang' => $item));
                    if($barang == "")
                    {
                        $status = false;
                        $msg = "Kode barang '" . $item . "' tidak ada di database";
                    }
                    $cat = AssetCategory::model()->findByAttributes(array('category_code' => $category));
                    if($cat == "")
                    {
                        $status = false;
                        $msg = "Category '" . $category . "'' tidak ada di database";
                    }

                    $businessunit = Businessunit::model()->findByPk($bu);
                    if($businessunit == "")
                    {
                        $status = false;
                        $msg = "Business Unit ID '" . $bu . "' tidak ada di database";
                    }

                    $model = new Asset;
                    $model->businessunit_id = $bu;
                    $model->user_id = $usr;
                    $model->asset_group_id = $groupclass->asset_group_id;
                    $model->barang_id = $barang->barang_id;
                    $model->category_id = $cat->category_id;

                    $model->asset_name = $barang->nama_barang;
                    $model->date_acquisition = $date;
                    $model->branch = $branch;
                    $model->qty = $qty;
                    $model->price_acquisition = $price;
                    $model->description = $desc;

                    $model->hide = $hide;
                    $model->created_at = new CDbExpression('NOW()');
                    $model->updated_at = new CDbExpression('NOW()');


                    $group = $this->getGolongan($groupclass->asset_group_id);
                    $last = $this->getLastRow($branch) + 1;

                    $cabang = $businessunit->businessunit_code.$branch;
                    $store = Store::model()->findByAttributes(array('store_kode' => $branch));
                    $area = str_pad($store->wilayah_id, 2, '0', STR_PAD_LEFT);
                    $urutan = str_pad($store->id_cabang, 2, '0', STR_PAD_LEFT);

                    $golongan = $group[0]; //$this->getGolongan($grupid);
                    $tariff = $group[1];
                    $period = $group[2];
                    $descs = $group[3];

                    $year = date('Y', strtotime($date));

                    $docref = $cabang.'AST'.$year.$last;

                    $ati = $area.$cabang.$urutan.'/'.$golongan;

                    $model->doc_ref = $docref;

                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Asset')) . CHtml::errorSummary($model));
                    }
                    else
                    {
                        for($i=0;$i<$qty;$i++)
                        {
                            $modeldetail = new AssetDetail;
                            $penyusutan = 0;
                            $penyusutantahun = 0;

                            $lastDetail = $this->getLastRowDetail($branch) + 1;

                            $docrefdetail = $docref.'/'.$lastDetail;
                            if($migrasi)
                            {
                                $modeldetail->ati = $activaold;//$ati.'/'.$lastDetail;
                            }
                            else{
                                $modeldetail->ati = $ati.'/'.$lastDetail;
                            }
                            //$modeldetail->ati = $activaold;//$ati.'/'.$lastDetail;
                            //$modeldetail->ati_old = $activaold;
                            $modeldetail->businessunit_id = $bu;
                            $modeldetail->user_id = $usr;
                            $modeldetail->category_id = $model->category_id;
                            $modeldetail->hide = $model->hide;
                            $modeldetail->asset_id = $model->asset_id;
                            $modeldetail->asset_trans_branch = $branch;
                            $modeldetail->asset_group_id = $groupclass->asset_group_id;
                            $modeldetail->barang_id = $model->barang_id;
                            $modeldetail->docref_other = $model->doc_ref;
                            $modeldetail->docref = $docrefdetail;
                            $modeldetail->asset_trans_name = $model->asset_name;
                            $modeldetail->asset_trans_date = $model->date_acquisition;
                            $modeldetail->asset_trans_price = $model->price_acquisition;
                            $modeldetail->asset_trans_new_price = $model->new_price_acquisition ;
                            $modeldetail->description = $model->description;
                            $modeldetail->class = $golongan;
                            $modeldetail->tariff = $tariff;
                            $modeldetail->period = $period;
                            $modeldetail->status = 1;


                            if($period)
                            {
                                $penyusutan = $modeldetail->asset_trans_price / $period;
                                $penyusutantahun = $penyusutan * $period;
                            }


                            $modeldetail->penyusutanperbulan = $penyusutan;
                            $modeldetail->penyusutanpertahun = $penyusutantahun;

                            $modeldetail->created_at = $model->created_at;
                            $modeldetail->updated_at = $model->updated_at;

                            if (!$modeldetail->save()) {
                                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Asset Detail')) . CHtml::errorSummary($modeldetail));
                            }
                            else
                            {
                                $total = $ppn + $modeldetail->asset_trans_price;
                                //GL
                                $gl = new GL();
                                $atiii = $modeldetail->ati;
                                $gl->add_gl_trans(ASSETBELI, $modeldetail->asset_trans_id, $modeldetail->asset_id,
                                    $modeldetail->businessunit_id, $modeldetail->asset_trans_date,
                                    $atiii, "$atiii", $modeldetail->asset_trans_price, $ppn, $total, $modeldetail->asset_trans_price,
                                    $usr, 1, 'beli',
                                    $modeldetail->asset_trans_branch);

                                //ASSET HISTORY
                                $history = new History();
                                $history->add_history_status($bu, $modeldetail->asset_id, $modeldetail->asset_trans_id, $docref,
                                    $modeldetail->ati,$modeldetail->asset_trans_name,$modeldetail->asset_trans_branch,
                                    $modeldetail->asset_trans_price, $modeldetail->asset_trans_new_price,
                                    $modeldetail->class, $modeldetail->tariff,$modeldetail->period,
                                    $modeldetail->penyusutanperbulan,$modeldetail->penyusutanpertahun,
                                    $modeldetail->description, $modeldetail->status);

                                $lastdeprisiasi = 0;
                                $lastbalance = 0;
                                $lastaccumulated = 0;

                                for($j=0;$j<$period;$j++)
                                {
                                    $modelperiode = new AssetPeriode;

                                    $modelperiode->docref = $modeldetail->docref;
                                    $modelperiode->docref_other = $modeldetail->docref_other;
                                    $modelperiode->ati = $modeldetail->ati;
                                    $modelperiode->businessunit_id = $bu;
                                    $modelperiode->asset_trans_id = $modeldetail->asset_trans_id;
                                    $modelperiode->asset_id = $modeldetail->asset_id;
                                    $modelperiode->asset_group_id = $modeldetail->asset_group_id;
                                    $modelperiode->barang_id = $modeldetail->barang_id;
                                    $modelperiode->asset_trans_date = $modeldetail->asset_trans_date;
                                    $modelperiode->asset_trans_price = get_number($modeldetail->asset_trans_price);
                                    $modelperiode->asset_trans_new_price = $modeldetail->asset_trans_new_price;
                                    $modelperiode->asset_trans_name = $modeldetail->asset_trans_name;
                                    $modelperiode->asset_trans_branch = $modeldetail->asset_trans_branch;
                                    $modelperiode->description = $modeldetail->description;

                                    $modelperiode->period = $modeldetail->period;
                                    $modelperiode->class = $modeldetail->class;
                                    $modelperiode->tariff = $modeldetail->tariff;


                                    //$penyusutan = $modelperiode->asset_trans_price * ($modelperiode->tariff/100) / $modelperiode->period;
                                    $penyusutan = $modelperiode->asset_trans_price / $modelperiode->period;

                                    $modelperiode->penyusutanperbulan = $penyusutan;
                                    $lastdeprisiasi = $modelperiode->penyusutanperbulan;

                                    $penyusutantahun = $penyusutan * $modelperiode->period;
                                    $modelperiode->penyusutanpertahun = $penyusutantahun;

                                    if($lastbalance == 0  || $lastdeprisiasi ==0)
                                    {
                                        $modelperiode->balance = $modelperiode->asset_trans_price - $penyusutan;
                                        $lastbalance = $modelperiode->balance;

                                        $modelperiode->akumulasipenyusutan = $penyusutan;
                                        $lastaccumulated = $modelperiode->akumulasipenyusutan;
                                    }
                                    else
                                    {
                                        $modelperiode->balance = $lastbalance - $lastdeprisiasi;
                                        $lastbalance = $modelperiode->balance;

                                        $modelperiode->akumulasipenyusutan = $lastaccumulated + $penyusutan;
                                        $lastaccumulated = $modelperiode->akumulasipenyusutan;
                                    }

                                    //counter bulan
/*                                    $x = $j+1;
                                    $d = strtotime("+$x months",strtotime($modeldetail->asset_trans_date));
                                    $newdate = date('Y-m-t',$d);*/

                                    $x = $j;
                                    $d = strtotime("$x months",strtotime($modeldetail->asset_trans_date));
                                    $newdate = date('Y-m-t',$d);
                                    /////////////////////////////////////////////

                                    $modelperiode->tglpenyusutan = $newdate;


                                    $modelperiode->startdate = $modelperiode->asset_trans_date;

                                    $tmp = $modelperiode->period - 1;
                                    $modelperiode->enddate = date('Y-m-t',strtotime("+$tmp months",strtotime($modeldetail->asset_trans_date)));

                                    $modelperiode->status = $modeldetail->status;

                                    $modelperiode->created_at = $modeldetail->created_at;
                                    $modelperiode->updated_at = $modeldetail->updated_at;

                                    if (!$modelperiode->save()) {
                                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($modelperiode));
                                    }
                                }
                            }
                        }
                        $status = true;
                        $msg = "Data berhasil di simpan dengan id " . $model->asset_id;
                        //$msg = t('save.success', 'app');
                    }
                }
                else{
                    array_push($dupArray,$dt['activaold']);//var_dump($msg);
                    $temp = "Dengan data Activa duplikat : ";
                }
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        finally
        {
            $comma_separated = implode("<br>*", $dupArray);
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg."<br>".$temp."<br>*".$comma_separated));
            Yii::app()->end();
            var_dump($dupArray);
        }
    }

    public function getLastRow($store)
    {
        $businessunit_id = $_COOKIE['businessunitid'];
        $criteria = new CDbCriteria;
        $criteria->compare('active','1');
        $criteria->compare('branch',$store);
        $criteria->compare('businessunit_id',$businessunit_id);
        $model = Asset::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

    public function getLastRowDetail($store)
    {
        $businessunit_id = $_COOKIE['businessunitid'];
        $criteria = new CDbCriteria;
        $criteria->compare('active','1');
        $criteria->compare('asset_trans_branch',$store);
        $criteria->compare('businessunit_id',$businessunit_id);
        $model = AssetDetail::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

    static function getGolongan($id)
    {
        $query = "select golongan as gol, tariff, period, nscc_asset_group.`desc` from nscc_asset_group
                     where asset_group_id = '".$id."' limit 1";

        $list= Yii::app()->db->createCommand($query)->queryAll();

        $rs=array();
        foreach($list as $item)
        {
            $rs[0]=$item['gol'];
            $rs[1]=$item['tariff'];
            $rs[2]=$item['period'];
            $rs[3]=$item['desc'];
        }
        return $rs;
    }



    static function report_asset($from, $to, $cat, $buid, $branch)
    {
        $query = "";
        $where = "";
        $wherebranch = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);
        //$param[':buid'] = $buid;

        if ($cat != null) {
            $where = "AND ad.category_id = '".$cat."' ";
            //$param[':category_id'] = $cat;
        }
        if ($branch != null) {
            $wherebranch = "and ad.asset_trans_branch = '".$branch."'";
            //$param[':category_id'] = $cat;
        }

        if ($from != undefined || $to != undefined) {
            /*$comm = Yii::app()->db->createCommand("select a.docref_other as 'docref', a.asset_trans_name as 'asset', count(a.asset_trans_price) as 'qty', sum(a.asset_trans_price) as 'total'
                                                    from nscc_asset_detail_view a
                                                    where a.`status` = 1 
                                                    and a.businessunit_id = :buid
                                                    and date(a.asset_trans_date) >= :from
                                                    and date(a.asset_trans_date) <= :to
                                                    $where
                                                    group by a.docref_other
                                                    order by a.asset_trans_name asc
                                                    ");*/

            /*select b.nama_barang as 'asset', c.category_name as 'category',  COUNT(ad.asset_trans_name) as 'qty', SUM(ad.asset_trans_price) as 'total' from nscc_asset_detail ad
                                                        join nscc_asset_category c on c.category_id = ad.category_id
                                                        join nscc_asset_barang b on b.barang_id = ad.barang_id
                                                        where ad.businessunit_id = :buid
            and DATE(ad.asset_trans_date) >= :from
            and DATE(ad.asset_trans_date) <= :to
                                                        $where
                                                        group by ad.asset_trans_name
                                                        order by ad.asset_trans_name asc*/


            $comm = Yii::app()->db->createCommand("
                    select  category, name as 'asset', kode, branch, qty,  price as 'total', ROUND(IFNULL(balance, price), 2) as 'balance' from (
                    
                        select c.category_name as 'category',  ad.asset_trans_name as 'name', b.kode_barang as 'kode',ad.description,
                        ad.asset_trans_branch as 'branch',
                        COUNT(ad.asset_trans_name) as 'qty',
                        ad.asset_trans_date as 'acquisitiondate',
                        ad.class,
                        ad.tariff,
                        ad.period, 
                        SUM(ad.asset_trans_price) as 'price',
                        SUM((select aps.balance from nscc_asset_periode aps
                        where aps.asset_trans_id  = ad.asset_trans_id
                        and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
                        and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()))) as 'balance'
                        from nscc_asset_detail ad
                        join nscc_asset_category c on c.category_id = ad.category_id
                        join nscc_asset_barang b on b.barang_id = ad.barang_id
                            where ad.businessunit_id = '".$buid."'
                            and ad.status != 0
                            and ad.active != 0
                            and DATE(ad.asset_trans_date) >= :from
                            and DATE(ad.asset_trans_date) <= :to
                            $where
                            $wherebranch
                        group by b.kode_barang
                        order by ad.asset_trans_name, ad.ati asc ) as asset");
        } else {
            $comm = Yii::app()->db->createCommand("
                    select  category, name as 'asset', kode, branch, qty,  price as 'total', ROUND(IFNULL(balance, price),2) as 'balance' from (
                    
                        select c.category_name as 'category',  ad.asset_trans_name as 'name', b.kode_barang as 'kode',ad.description,
                        ad.asset_trans_branch as 'branch',
                        COUNT(ad.asset_trans_name) as 'qty',
                        ad.asset_trans_date as 'acquisitiondate',
                        ad.class,
                        ad.tariff,
                        ad.period, 
                        SUM(ad.asset_trans_price) as 'price',
                        SUM((select aps.balance from nscc_asset_periode aps
                        where aps.asset_trans_id  = ad.asset_trans_id
                        and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
                        and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()))) as 'balance'
                        from nscc_asset_detail ad
                        join nscc_asset_category c on c.category_id = ad.category_id
                        join nscc_asset_barang b on b.barang_id = ad.barang_id
                            where ad.businessunit_id = '".$buid."'
                            and ad.status != 0
                            and ad.active != 0
                            $where
                            $wherebranch
                        group by b.kode_barang
                        order by ad.asset_trans_name, ad.ati asc ) as asset");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_asset_detail($from, $to, $cat, $buid, $branch)
    {
        $query = "";
        $where = "";
        $wherebranch = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);
        //$param[':buid'] = $buid;

        if ($cat != null) {
            $where = "AND ad.category_id = '".$cat."'";
            //$param[':category_id'] = $cat;
        }
        if ($branch != null) {
            $wherebranch = "and ad.asset_trans_branch = '".$branch."'";
            //$param[':category_id'] = $cat;
        }

        if ($from != undefined || $to != undefined) {
            /*$comm = Yii::app()->db->createCommand("select a.docref_other as 'docref', a.asset_trans_name as 'asset', count(a.asset_trans_price) as 'qty', sum(a.asset_trans_price) as 'total'
                                                    from nscc_asset_detail_view a
                                                    where a.`status` = 1
                                                    and a.businessunit_id = :buid
                                                    and date(a.asset_trans_date) >= :from
                                                    and date(a.asset_trans_date) <= :to
                                                    $where
                                                    group by a.docref_other
                                                    order by a.asset_trans_name asc
                                                    ");*/

            /*select ad.ati as 'activa', ad.asset_trans_price as 'price',
IF(ap.balance != '0',ap.balance,0) as 'depriciation price'
from nscc_asset_detail ad
join nscc_asset_periode ap on ap.asset_trans_id = ad.asset_trans_id
where MONTH(ap.tglpenyusutan) = MONTH(CURDATE())
            AND YEAR(ap.tglpenyusutan) = YEAR(CURDATE())
            AND ad.businessunit_id = 'c73e49c2-1616-11e8-a32f-201a069f4b32'
            AND ap.businessunit_id = 'c73e49c2-1616-11e8-a32f-201a069f4b32'*/

           /* select ad.ati as 'activa',  ad.asset_trans_name as 'name', ad.description,
ad.asset_trans_date as 'acquisitiondate',
ad.asset_trans_price as 'price',
IF(MONTH(ap.asset_trans_date) = MONTH(CURDATE())  and  YEAR(ap.asset_trans_date) = YEAR(CURDATE()), ap.asset_trans_price ,ap.balance) as 'depriciationprice'
from nscc_asset_detail ad
join nscc_asset_periode ap on ap.asset_trans_id = ad.asset_trans_id
where MONTH(ap.tglpenyusutan) = MONTH(CURDATE())
            AND YEAR(ap.tglpenyusutan) = YEAR(CURDATE())*/
/*            select ad.ati as 'activa', c.category_name as 'category',  ad.asset_trans_name as 'name',
ad.asset_trans_branch as 'branch',
ad.asset_trans_date as 'acquisitiondate',
ad.class,
ad.period,
ad.asset_trans_price as 'price',
(select aps.balance from nscc_asset_periode aps
where aps.asset_trans_id  = ad.asset_trans_id
            and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())
            and YEAR(aps.tglpenyusutan) = YEAR(CURDATE())) as 'balance'
from nscc_asset_detail ad
join nscc_asset_category c on c.category_id = ad.category_id
join nscc_asset_barang b on b.barang_id = ad.barang_id
order by ad.asset_trans_name, ad.ati asc*/

            $comm = Yii::app()->db->createCommand("select activa, category, name, kode, status, description,branch,acquisitiondate, class, tariff, period, ROUND(price, 2) as price, ROUND(IFNULL(balance, price))  as 'balance' from (

    select ad.ati as 'activa', c.category_name as 'category',  ad.asset_trans_name as 'name', b.kode_barang as 'kode',ad.description,
    ad.asset_trans_branch as 'branch',
    ad.asset_trans_date as 'acquisitiondate',
    ad.class,
    ad.tariff,
    ad.period, ad.status,
    ad.asset_trans_price as 'price',
    (select aps.balance from nscc_asset_periode aps
    where aps.asset_trans_id  = ad.asset_trans_id
    and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
    and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()) group by aps.asset_trans_id) as 'balance'
    from nscc_asset_detail ad
    join nscc_asset_category c on c.category_id = ad.category_id
    join nscc_asset_barang b on b.barang_id = ad.barang_id
    where ad.businessunit_id = '".$buid."'
    and ad.status != 0
                            and ad.active != 0
    
    and DATE(ad.asset_trans_date) >= :from
    and DATE(ad.asset_trans_date) <= :to
    $where
    $wherebranch
    order by ad.asset_trans_name, ad.ati asc ) as asset order by activa, name asc
    ");
        }
        else {
            $comm = Yii::app()->db->createCommand("select activa, category, name, kode, status, description,branch,acquisitiondate, class, tariff, period, ROUND(price, 2) as price, ROUND(IFNULL(balance, price)) as 'balance' from ( select ad.ati as 'activa', c.category_name as 'category',  ad.asset_trans_name as 'name', b.kode_barang as 'kode',ad.description,
ad.asset_trans_branch as 'branch',
ad.asset_trans_date as 'acquisitiondate',
ad.class,
ad.period, ad.status,
ad.tariff,
ad.asset_trans_price as 'price',
(select aps.balance from nscc_asset_periode aps
where aps.asset_trans_id  = ad.asset_trans_id
and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()) group by aps.asset_trans_id) as 'balance'
from nscc_asset_detail ad
join nscc_asset_category c on c.category_id = ad.category_id
join nscc_asset_barang b on b.barang_id = ad.barang_id
where ad.businessunit_id = '".$buid."'
and ad.status != 0
                            and ad.active != 0
$where
$wherebranch
order by ad.asset_trans_name, ad.ati asc ) as asset order by activa, name asc
");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_asset_nonactive($from, $to, $cat, $buid, $branch)
    {
        $query = "";
        $where = "";
        $wherebranch = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);
        //$param[':buid'] = $buid;

        if ($cat != null) {
            $where = "AND ad.category_id = '".$cat."' ";
            //$param[':category_id'] = $cat;
        }
        if ($branch != null) {
            $wherebranch = "and ad.asset_trans_branch = '".$branch."'";
            //$param[':category_id'] = $cat;
        }

        if ($from != undefined || $to != undefined) {
            /*$comm = Yii::app()->db->createCommand("select a.docref_other as 'docref', a.asset_trans_name as 'asset', count(a.asset_trans_price) as 'qty', sum(a.asset_trans_price) as 'total'
                                                    from nscc_asset_detail_view a
                                                    where a.`status` = 1
                                                    and a.businessunit_id = :buid
                                                    and date(a.asset_trans_date) >= :from
                                                    and date(a.asset_trans_date) <= :to
                                                    $where
                                                    group by a.docref_other
                                                    order by a.asset_trans_name asc
                                                    ");*/

            /*select b.nama_barang as 'asset', c.category_name as 'category',  COUNT(ad.asset_trans_name) as 'qty', SUM(ad.asset_trans_price) as 'total' from nscc_asset_detail ad
                                                        join nscc_asset_category c on c.category_id = ad.category_id
                                                        join nscc_asset_barang b on b.barang_id = ad.barang_id
                                                        where ad.businessunit_id = :buid
            and DATE(ad.asset_trans_date) >= :from
            and DATE(ad.asset_trans_date) <= :to
                                                        $where
                                                        group by ad.asset_trans_name
                                                        order by ad.asset_trans_name asc*/


            $comm = Yii::app()->db->createCommand("
                    select  category, name as 'asset', kode, branch, qty,  price as 'total', ROUND(IFNULL(balance, price), 2) as 'balance' from (
                    
                        select c.category_name as 'category',  ad.asset_trans_name as 'name', b.kode_barang as 'kode',ad.description,
                        ad.asset_trans_branch as 'branch',
                        COUNT(ad.asset_trans_name) as 'qty',
                        ad.asset_trans_date as 'acquisitiondate',
                        ad.class,
                        ad.tariff,
                        ad.period, 
                        SUM(ad.asset_trans_price) as 'price',
                        SUM((select aps.balance from nscc_asset_periode aps
                        where aps.asset_trans_id  = ad.asset_trans_id
                        and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
                        and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()))) as 'balance'
                        from nscc_asset_detail ad
                        join nscc_asset_category c on c.category_id = ad.category_id
                        join nscc_asset_barang b on b.barang_id = ad.barang_id
                            where ad.businessunit_id = '".$buid."'
                            and ad.active = '0'
                            and DATE(ad.asset_trans_date) >= :from
                            and DATE(ad.asset_trans_date) <= :to
                            $where
                            $wherebranch
                        group by b.kode_barang
                        order by ad.asset_trans_name, ad.ati asc ) as asset");
        } else {
            $comm = Yii::app()->db->createCommand("
                    select  category, name as 'asset', kode, branch, qty,  price as 'total', ROUND(IFNULL(balance, price),2) as 'balance' from (
                    
                        select c.category_name as 'category',  ad.asset_trans_name as 'name', b.kode_barang as 'kode',ad.description,
                        ad.asset_trans_branch as 'branch',
                        COUNT(ad.asset_trans_name) as 'qty',
                        ad.asset_trans_date as 'acquisitiondate',
                        ad.class,
                        ad.tariff,
                        ad.period, 
                        SUM(ad.asset_trans_price) as 'price',
                        SUM((select aps.balance from nscc_asset_periode aps
                        where aps.asset_trans_id  = ad.asset_trans_id
                        and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
                        and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()))) as 'balance'
                        from nscc_asset_detail ad
                        join nscc_asset_category c on c.category_id = ad.category_id
                        join nscc_asset_barang b on b.barang_id = ad.barang_id
                            where ad.businessunit_id = '".$buid."'
                            and ad.active = '0'
                            $where
                            $wherebranch
                        group by b.kode_barang
                        order by ad.asset_trans_name, ad.ati asc ) as asset");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_asset_detail_nonactive($from, $to, $cat, $buid, $branch)
    {
        $query = "";
        $where = "";
        $wherebranch = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);
        //$param[':buid'] = $buid;

        if ($cat != null) {
            $where = "AND ad.category_id = '".$cat."'";
            //$param[':category_id'] = $cat;
        }
        if ($branch != null) {
            $wherebranch = "and ad.asset_trans_branch = '".$branch."'";
            //$param[':category_id'] = $cat;
        }

        if ($from != undefined || $to != undefined) {
            /*$comm = Yii::app()->db->createCommand("select a.docref_other as 'docref', a.asset_trans_name as 'asset', count(a.asset_trans_price) as 'qty', sum(a.asset_trans_price) as 'total'
                                                    from nscc_asset_detail_view a
                                                    where a.`status` = 1
                                                    and a.businessunit_id = :buid
                                                    and date(a.asset_trans_date) >= :from
                                                    and date(a.asset_trans_date) <= :to
                                                    $where
                                                    group by a.docref_other
                                                    order by a.asset_trans_name asc
                                                    ");*/

            /*select ad.ati as 'activa', ad.asset_trans_price as 'price',
IF(ap.balance != '0',ap.balance,0) as 'depriciation price'
from nscc_asset_detail ad
join nscc_asset_periode ap on ap.asset_trans_id = ad.asset_trans_id
where MONTH(ap.tglpenyusutan) = MONTH(CURDATE())
            AND YEAR(ap.tglpenyusutan) = YEAR(CURDATE())
            AND ad.businessunit_id = 'c73e49c2-1616-11e8-a32f-201a069f4b32'
            AND ap.businessunit_id = 'c73e49c2-1616-11e8-a32f-201a069f4b32'*/

            /* select ad.ati as 'activa',  ad.asset_trans_name as 'name', ad.description,
 ad.asset_trans_date as 'acquisitiondate',
 ad.asset_trans_price as 'price',
 IF(MONTH(ap.asset_trans_date) = MONTH(CURDATE())  and  YEAR(ap.asset_trans_date) = YEAR(CURDATE()), ap.asset_trans_price ,ap.balance) as 'depriciationprice'
 from nscc_asset_detail ad
 join nscc_asset_periode ap on ap.asset_trans_id = ad.asset_trans_id
 where MONTH(ap.tglpenyusutan) = MONTH(CURDATE())
             AND YEAR(ap.tglpenyusutan) = YEAR(CURDATE())*/
            /*            select ad.ati as 'activa', c.category_name as 'category',  ad.asset_trans_name as 'name',
            ad.asset_trans_branch as 'branch',
            ad.asset_trans_date as 'acquisitiondate',
            ad.class,
            ad.period,
            ad.asset_trans_price as 'price',
            (select aps.balance from nscc_asset_periode aps
            where aps.asset_trans_id  = ad.asset_trans_id
                        and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())
                        and YEAR(aps.tglpenyusutan) = YEAR(CURDATE())) as 'balance'
            from nscc_asset_detail ad
            join nscc_asset_category c on c.category_id = ad.category_id
            join nscc_asset_barang b on b.barang_id = ad.barang_id
            order by ad.asset_trans_name, ad.ati asc*/

            $comm = Yii::app()->db->createCommand("select activa, category, name, kode, status, description,branch,acquisitiondate, class, tariff, period, ROUND(price, 2) as price, ROUND(IFNULL(balance, price))  as 'balance' from (

    select ad.ati as 'activa', c.category_name as 'category',  ad.asset_trans_name as 'name', b.kode_barang as 'kode',ad.description,
    ad.asset_trans_branch as 'branch',
    ad.asset_trans_date as 'acquisitiondate',
    ad.class,
    ad.tariff,
    ad.period, ad.status,
    ad.asset_trans_price as 'price',
    (select aps.balance from nscc_asset_periode aps
    where aps.asset_trans_id  = ad.asset_trans_id
    and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
    and YEAR(aps.tglpenyusutan) = YEAR(CURDATE())) as 'balance'
    from nscc_asset_detail ad
    join nscc_asset_category c on c.category_id = ad.category_id
    join nscc_asset_barang b on b.barang_id = ad.barang_id
    where ad.businessunit_id = '".$buid."'
                            and ad.active = '0'
    
    and DATE(ad.asset_trans_date) >= :from
    and DATE(ad.asset_trans_date) <= :to
    $where
    $wherebranch
    order by ad.asset_trans_name, ad.ati asc ) as asset order by activa, name asc
    ");
        }
        else {
            $comm = Yii::app()->db->createCommand("select activa, category, name, kode, status, description,branch,acquisitiondate, class, tariff, period, ROUND(price, 2) as price, ROUND(IFNULL(balance, price)) as 'balance' from ( select ad.ati as 'activa', c.category_name as 'category',  ad.asset_trans_name as 'name', b.kode_barang as 'kode',ad.description,
ad.asset_trans_branch as 'branch',
ad.asset_trans_date as 'acquisitiondate',
ad.class,
ad.period, ad.status,
ad.tariff,
ad.asset_trans_price as 'price',
(select aps.balance from nscc_asset_periode aps
where aps.asset_trans_id  = ad.asset_trans_id
and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
and YEAR(aps.tglpenyusutan) = YEAR(CURDATE())) as 'balance'
from nscc_asset_detail ad
join nscc_asset_category c on c.category_id = ad.category_id
join nscc_asset_barang b on b.barang_id = ad.barang_id
where ad.businessunit_id = '".$buid."'
and ad.active = '0'
$where
$wherebranch
order by ad.asset_trans_name, ad.ati asc ) as asset order by activa, name asc
");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_asset_depriciation($from, $to, $ati = "", $id)
    {
        $query = "";
        $where = "";
        $comm = "";
        $param = array(':from' => $from, ':to' => $to);
        if ($ati != null) {
            $where = "AND ap.ati = :ati ";
            $param[':ati'] = $ati;
        }

        if ($from != undefined || $to != undefined) {
            $comm = Yii::app()->db->createCommand("select docref, ati, asset_trans_name, asset_trans_name, 
asset_trans_branch,asset_trans_price, asset_trans_new_price, 
DATE_FORMAT(asset_trans_date, '%d %M %Y') as asset_trans_date, description, class,
 period, tariff, DATE_FORMAT(tglpenyusutan, '%d %M %Y') as tglpenyusutan, 
 ROUND(ap.penyusutanperbulan, 2) penyusutanperbulan, 
 ROUND(ap.akumulasipenyusutan, 2) akumulasipenyusutan, ap.penyusutanpertahun, 
 ROUND(ap.balance, 2) balance, status from nscc_asset_periode ap
            WHERE DATE(ap.tglpenyusutan) >= :from AND DATE(ap.tglpenyusutan) <= :to 
            $where
            ORDER BY ap.tglpenyusutan ASC");
        } else {
            $comm = Yii::app()->db->createCommand("select docref, ati, asset_trans_name, asset_trans_name, asset_trans_branch,asset_trans_price, asset_trans_new_price, DATE_FORMAT(asset_trans_date, '%d %M %Y') as asset_trans_date, description, class, period, tariff, DATE_FORMAT(tglpenyusutan, '%d %M %Y') as tglpenyusutan, ROUND(ap.penyusutanperbulan, 2) penyusutanperbulan, ap.penyusutanpertahun, ROUND(ap.akumulasipenyusutan, 2) akumulasipenyusutan, ROUND(ap.balance, 2) balance, status from nscc_asset_periode ap 
            WHERE ap.asset_trans_id = '$id' ORDER BY ap.tglpenyusutan ASC");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_jurnalasset($from, $to, $buid, $branch)
    {
        $query = "";
        $where = "";
        $comm = "";

        if ($branch != null) {
            $where = "and gl.store = '".$branch."'";
            //$param[':category_id'] = $cat;
        }
        $param = array(':from' => $from, ':to' => $to);
        //$param[':buid'] = $buid;

        /*if ($cat != null) {
            $where = "AND ad.category_id = :category_id ";
            $param[':category_id'] = $cat;
        }*/

        if ($from != 'undefined' || $to != 'undefined') {

            /*$comm = Yii::app()->db->createCommand("select a.docref_other as 'docref', a.asset_trans_name as 'asset', count(a.asset_trans_price) as 'qty', sum(a.asset_trans_price) as 'total'
                                                    from nscc_asset_detail_view a
                                                    where a.`status` = 1
                                                    and a.businessunit_id = :buid
                                                    and date(a.asset_trans_date) >= :from
                                                    and date(a.asset_trans_date) <= :to
                                                    $where
                                                    group by a.docref_other
                                                    order by a.asset_trans_name asc
                                                    ");*/
                        /*select
            gl.memo_, gl.tran_date,
            gl.desc as 'status' ,gl.amount as price, gl.depreciation_price as 'depreciationprice',
            if(gl.amount >= 0,gl.amount,0) debit,
            if(gl.amount < 0,-gl.amount,0) kredit

            from nscc_gl_trans gl
            where gl.businessunit_id = 'c73e49c2-1616-11e8-a32f-201a069f4b32'
            order by gl.memo_ asc*/
            /*$comm = Yii::app()->db->createCommand("select b.nama_barang as 'asset', c.category_name as 'category',  COUNT(ad.asset_trans_name) as 'qty', SUM(ad.asset_trans_price) as 'total' from nscc_asset_detail ad
                                                        join nscc_asset_category c on c.category_id = ad.category_id
                                                        join nscc_asset_barang b on b.barang_id = ad.barang_id
                                                        where ad.businessunit_id = :buid
                                                        and DATE(ad.asset_trans_date) >= :from
                                                        and DATE(ad.asset_trans_date) <= :to
                                                        $where
                                                        group by ad.asset_trans_name
                                                        order by ad.asset_trans_name asc");*/

            /*select
                                                        gl.memo_, ad.asset_trans_name, ab.kode_barang, ad.description, gl.tran_date, gl.tdate,
                                                        gl.desc as 'status' ,gl.amount as price, gl.depreciation_price as 'depreciationprice',
                                                        if(gl.amount >= 0,gl.amount,0) debit,
                                                        if(gl.amount < 0,-gl.amount,0) kredit,
                                                        gl.depreciation_price - gl.amount as balance
                                                        from nscc_gl_trans gl
                                                        join nscc_asset_detail ad on ad.asset_trans_id = gl.type_no
                                                        join nscc_asset_barang ab on ab.barang_id = ad.barang_id
                                                        where gl.businessunit_id = '".$buid."'
            and date(gl.tran_date) >= :from
            and date(gl.tran_date) <= :to
                                                        order by gl.memo_,gl.tdate asc*/

            $comm = Yii::app()->db->createCommand("select *, 
if((kredit - depreciationprice) < 0, 0, ROUND((kredit - depreciationprice),2)  ) as 'profit',
if((kredit - depreciationprice) >= 0, 0, if(kredit != 0,ROUND(-(kredit - depreciationprice),2),0)  ) as 'loss'
from (
	select  
	gl.memo_, ad.asset_trans_name, ab.kode_barang,   gl.tran_date, DATE(gl.tdate) as tdate,
	gl.desc as 'status' ,gl.amount as price, gl.ppn, gl.total, gl.depreciation_price as 'depreciationprice', 
	if(gl.amount >= 0,gl.amount + gl.ppn,0) debit,
	if(gl.amount < 0,-gl.amount,0) kredit

	from nscc_gl_trans gl
	join nscc_asset_detail ad on ad.asset_trans_id = gl.type_no
	join nscc_asset_barang ab on ab.barang_id = ad.barang_id
	where gl.businessunit_id = '".$buid."'
	$where
            and date(gl.tran_date) >= :from
            and date(gl.tran_date) <= :to
            and gl.visible != 0
            order by gl.memo_,gl.tdate asc
	) 
as jurnal");
        } else {
            $comm = Yii::app()->db->createCommand("select *, 
if((kredit - depreciationprice) < 0, 0, ROUND((kredit - depreciationprice),2)  ) as 'profit',
if((kredit - depreciationprice) >= 0, 0, if(kredit != 0,ROUND(-(kredit - depreciationprice),2),0)  ) as 'loss'
from (
	select  
	gl.memo_, ad.asset_trans_name, ab.kode_barang,  gl.tran_date, DATE(gl.tdate) as tdate,
	gl.desc as 'status' ,gl.amount as price, gl.ppn, gl.total, gl.depreciation_price as 'depreciationprice', 
	if(gl.amount >= 0,gl.amount + gl.ppn ,0) debit,
	if(gl.amount < 0,-gl.amount,0) kredit

	from nscc_gl_trans gl
	join nscc_asset_detail ad on ad.asset_trans_id = gl.type_no
	join nscc_asset_barang ab on ab.barang_id = ad.barang_id
	where gl.businessunit_id = '".$buid."'
	$where
	and gl.visible != 0
            order by gl.memo_,gl.tdate asc
	) 
as jurnal");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_rentasset($from, $to, $cat, $buid, $branch)
    {
        $query = "";
        $where = "";
        $wherebranch = "";
        $comm = "";

        //$param[':buid'] = ;
        $param = array(':from' => $from, ':to' => $to);
        if ($cat != null) {
            $where = "AND rt.category = '".$cat."'";
            //$param[':category_id'] = $cat;
        }
        if ($branch != null) {
            $wherebranch = "and rt.store = '".$branch."'";
            //$param[':category_id'] = $cat;
        }
        $assettype = ASSETSEWA;

        if ($from != 'undefined' || $to != 'undefined') {
            //$param = array(':from' => $from, ':to' => $to);

            $comm = Yii::app()->db->createCommand("select rt.*, DATE(rt.tdate) as 'ttdate' from nscc_rent_trans rt
                                                where rt.businessunit_id = '".$buid."'
                                                and rt.`type` = '".$assettype."'
                                                and date(rt.tdate) >= :from
                                                and date(rt.tdate) <= :to
                                                $where 
                                                $wherebranch
                                                ");
        } else {
            $comm = Yii::app()->db->createCommand("select rt.*, DATE(rt.tdate) as 'ttdate' from nscc_rent_trans rt
                                                where rt.businessunit_id = '".$buid."'
                                                and rt.`type` = '".$assettype."'
                                                $where
                                                $wherebranch
                                                ");
        }

        return $comm->queryAll(true, $param);
    }

    static function report_deprisiasiasset($from, $to, $buid, $branch, $cat_id)
    {
        $query = "";
        $where = "";
        $wherecat = "";
        $comm = "";

        if ($branch != null) {
            $where = "and apv.asset_trans_branch = '".$branch."'";
            //$param[':category_id'] = $cat;
        }
        if ($cat_id != null) {
            $wherecat = "and adv.category_id = '".$cat_id."'";
            //$param[':category_id'] = $cat;
        }
        $param = array(':from' => $from, ':to' => $to);

        if ($from != 'undefined' || $to != 'undefined')
        {
            $comm = Yii::app()->db->createCommand("select activa, asset, keterangan, store,period, tdate, depreciationstart, depreciationend, acquisitionprice, ROUND(IFNULL(depreciationpermonth, 0),2) as depreciationpermonth, ROUND(IFNULL(depreciationaccumulation, 0),2) as depreciationaccumulation, ROUND(IFNULL(nilaibuku, 0),2) as nilaibuku, ROUND(IFNULL(depreciationaccumulationbyparam, 0),2) as depreciationaccumulationbyparam, ROUND(IFNULL(nilaibukubyparam, 0),2) as nilaibukubyparam, ROUND(IFNULL(akumulasibyparam, 0),2) as akumulasibyparam from
(select 
 apv.ati as activa,
 apv.asset_trans_branch as store,
 apv.asset_trans_name as asset, 
 apv.period,
 adv.description as keterangan,
 date(apv.asset_trans_date) tdate, 
 date(apv.startdate) as depreciationstart,
 date(apv.enddate) as depreciationend,
 apv.asset_trans_price as acquisitionprice, 
 apv.penyusutanperbulan as depreciationpermonth, 
  (select aps.akumulasipenyusutan from nscc_asset_periode aps
    where aps.asset_trans_id  = apv.asset_trans_id
    and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
    and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()) group by aps.asset_trans_id) as 'depreciationaccumulation',
  (select aps.balance from nscc_asset_periode aps
    where aps.asset_trans_id  = apv.asset_trans_id
    and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
    and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()) group by aps.asset_trans_id) as 'nilaibuku',
sum(apv.penyusutanperbulan) as 'depreciationaccumulationbyparam',
(select aps.balance from nscc_asset_periode aps
    where aps.asset_trans_id  = apv.asset_trans_id
    and MONTH(aps.tglpenyusutan) = MONTH(:to)  
    and YEAR(aps.tglpenyusutan) = YEAR(:to) group by aps.asset_trans_id) as 'nilaibukubyparam',
        (select aps.akumulasipenyusutan from nscc_asset_periode aps
    where aps.asset_trans_id  = apv.asset_trans_id
    and MONTH(aps.tglpenyusutan) = MONTH(:to)  
    and YEAR(aps.tglpenyusutan) = YEAR(:to) group by aps.asset_trans_id) as 'akumulasibyparam'
from nscc_asset_periode apv
join nscc_asset_detail_view adv on apv.asset_trans_id = adv.asset_trans_id
where date(apv.tglpenyusutan) >= :from
and date(apv.tglpenyusutan) <= :to
and apv.businessunit_id = '".$buid."'
$where $wherecat 
group by apv.ati
order by apv.asset_trans_name, apv.asset_trans_date) as deprisiasi
ORDER BY store, CAST((SUBSTRING_INDEX(activa, '/', -1)) AS UNSIGNED), activa asc");
        } else {
            $comm = Yii::app()->db->createCommand("select activa,store , asset, keterangan, period, tdate, depreciationstart, depreciationend, acquisitionprice, ROUND(IFNULL(depreciationpermonth, 0),2) as depreciationpermonth, ROUND(IFNULL(depreciationaccumulation, 0),2) as depreciationaccumulation, ROUND(IFNULL(nilaibuku, 0),2) as nilaibuku, ROUND(IFNULL(depreciationaccumulationbyparam, 0),2) as depreciationaccumulationbyparam, ROUND(IFNULL(nilaibukubyparam, 0),2) as nilaibukubyparam, ROUND(IFNULL(akumulasibyparam, 0),2) as akumulasibyparam from
(select 
 apv.ati as activa,
 apv.asset_trans_branch as store,
 apv.asset_trans_name as asset, 
 apv.period,
 adv.description as keterangan,
 date(apv.asset_trans_date) tdate, 
 date(apv.startdate) as depreciationstart,
 date(apv.enddate) as depreciationend,
 apv.asset_trans_price as acquisitionprice, 
 apv.penyusutanperbulan as depreciationpermonth, 
  (select aps.akumulasipenyusutan from nscc_asset_periode aps
    where aps.asset_trans_id  = apv.asset_trans_id
    and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
    and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()) group by aps.asset_trans_id) as 'depreciationaccumulation',
  (select aps.balance from nscc_asset_periode aps
    where aps.asset_trans_id  = apv.asset_trans_id
    and MONTH(aps.tglpenyusutan) = MONTH(CURDATE())  
    and YEAR(aps.tglpenyusutan) = YEAR(CURDATE()) group by aps.asset_trans_id) as 'nilaibuku',
    sum(apv.penyusutanperbulan) as 'depreciationaccumulationbyparam',
    (select aps.balance from nscc_asset_periode aps
    where aps.asset_trans_id  = apv.asset_trans_id
    and MONTH(aps.tglpenyusutan) = MONTH(:to)  
    and YEAR(aps.tglpenyusutan) = YEAR(:to) group by aps.asset_trans_id) as 'nilaibukubyparam',
    (select aps.akumulasipenyusutan from nscc_asset_periode aps
    where aps.asset_trans_id  = apv.asset_trans_id
    and MONTH(aps.tglpenyusutan) = MONTH(:to)  
    and YEAR(aps.tglpenyusutan) = YEAR(:to) group by aps.asset_trans_id) as 'akumulasibyparam'
from nscc_asset_periode apv
join nscc_asset_detail_view adv on apv.asset_trans_id = adv.asset_trans_id
where date(apv.tglpenyusutan) >= :from
and date(apv.tglpenyusutan) <= :to
and apv.businessunit_id = '".$buid."'
$where $wherecat 
group by apv.ati
order by apv.asset_trans_name, apv.asset_trans_date) as deprisiasi
ORDER BY store, CAST((SUBSTRING_INDEX(activa, '/', -1)) AS UNSIGNED), activa asc");
        }

        return $comm->queryAll(true, $param);
    }

    //untuk cabang
    static function report_showasset($from, $to, $buid, $branch, $cat)
    {
        $query = "";
        $where = "";
        $wherebranch = "";
        $comm = "";

        if ($cat != null) {
            $where = "AND ad.category_id = '".$cat."'";
            //$param[':category_id'] = $cat;
        }
        if ($branch != null) {
            $wherebranch = "and ad.asset_trans_branch = '".$branch."'";
            //$param[':category_id'] = $cat;
        }
        $param = array(':from' => $from, ':to' => $to);

        if ($from != 'undefined' || $to != 'undefined')
        {
            $comm = Yii::app()->db->createCommand("select ad.ati as 'activa', ad.asset_trans_name as 'assetname', ab.kode_barang as 'assetcode' from nscc_asset_detail ad
join nscc_asset_barang ab 
on ab.barang_id = ad.barang_id
where date(ad.asset_trans_date) >= :from
and date(ad.asset_trans_date) <= :to
and ad.businessunit_id  = '".$buid."'
and ad.active = '1'
$where
$wherebranch
order by ad.asset_trans_name asc");
        } else {
            $comm = Yii::app()->db->createCommand("select ad.ati as 'activa', ad.asset_trans_name as 'assetname', ab.kode_barang as 'assetcode' from nscc_asset_detail ad
join nscc_asset_barang ab 
on ab.barang_id = ad.barang_id
where ad.businessunit_id  = '".$buid."'
and ad.active = '1'
$where
$wherebranch
order by ad.asset_trans_name asc");
        }

        return $comm->queryAll(true, $param);
    }
}



