<?php
/**
 * @property Salestrans $s
 * @property ResepPrint $resepPrint
 */
class PrintReceipt extends BasePrint
{
    protected $s;
    protected $resepPrint;
    /**
     * @return ResepPrint
     */
    public function getResepPrint()
    {
        return $this->resepPrint;
    }
    /**
     * @param ResepPrint $resepPrint
     */
    public function setResepPrint(ResepPrint $resepPrint)
    {
        $this->resepPrint = $resepPrint;
    }
    function __construct($s)
    {
//        $s = new Salestrans();
        $this->s = $s;
        $this->resepPrint = $this->createResepPrint();
    }
    protected function createResepPrint()
    {
        $resepPrint = ResepPrint::model()->findByAttributes(['salestrans_id' => $this->s->salestrans_id]);
        if ($resepPrint == null) {
            $resepPrint = new ResepPrint;
            $resepPrint->salestrans_id = $this->s->salestrans_id;
            $resepPrint->header0 = SysPrefs::get_val('receipt_header0');
            $resepPrint->header1 = SysPrefs::get_val('receipt_header1');
            $resepPrint->header2 = SysPrefs::get_val('receipt_header2');
            $resepPrint->header3 = SysPrefs::get_val('receipt_header3');
            $resepPrint->header4 = SysPrefs::get_val('receipt_header4');
            $resepPrint->header5 = SysPrefs::get_val('receipt_header5');
            $resepPrint->doc_ref_receipt = $this->s->doc_ref;
            $resepPrint->date_ = $this->s->tgl;
            $resepPrint->pasien_code = $this->s->customer->no_customer;
            $resepPrint->pasien_name = $this->s->customer->nama_customer;
            $user = Users::model()->findByPk($this->s->user_id);
            $resepPrint->kasir = $user->name . ' [ ' . strtoupper($this->s->counter) . ' ]';
            $resepPrint->alamat = $this->s->customer->alamat;
            if ($this->s->dokter_id != null) {
                /** @var Dokter $dokter */
                $dokter = Dokter::model()->findByPk($this->s->dokter_id);
                if ($dokter != null) {
                    $resepPrint->dokter = $dokter->nama_dokter;
                }
            }
            if ($this->s->apoteker_id != null) {
                /** @var Apoteker $apoteker */
                $apoteker = Apoteker::model()->findByPk($this->s->apoteker_id);
                if ($apoteker != null) {
                    $resepPrint->apoteker = $apoteker->nama_apoteker;
                }
            }
//            if ($this->s->stocker_id != null) {
//                /** @var Apoteker $fo */
//                $fo = Apoteker::model()->findByPk($this->s->stocker_id);
//                if ($fo != null) {
//                    $resepPrint->stocker = $fo->nama_apoteker;
//                }
//            }
//            stocker yg bertugas saat ini
            $stocker = AssignEmployeesView::onDuty(AssignEmployeesView::STK);
            foreach ($stocker as $stk){
                $resepPrint->stocker = $stk->nama_employee;
            }             
            
            if ($this->s->beauty_id != null) {
                /** @var Beauty $beauty */
                $beauty = Beauty::model()->findByPk($this->s->beauty_id);
                if ($beauty != null) {
                    $resepPrint->beauty = $beauty->nama_beauty;
                }
            }
            $resepPrint->subtotal = $this->s->bruto;
            $resepPrint->disc = $this->s->total_discrp1;
            $resepPrint->vat = $this->s->vat;
            $resepPrint->rounding = $this->s->rounding;
            $resepPrint->amount_due = $this->s->total;
            $resepPrint->kembalian = $this->s->kembali;
            $resepPrint->footer0 = SysPrefs::get_val('receipt_footer0');
            $resepPrint->footer1 = SysPrefs::get_val('receipt_footer1');
            $resepPrint->footer2 = SysPrefs::get_val('receipt_footer2');
            $resepPrint->footer3 = SysPrefs::get_val('receipt_footer3');
            $resepPrint->footer4 = SysPrefs::get_val('receipt_footer4');
            $resepPrint->footer5 = SysPrefs::get_val('receipt_footer5');
            if (!$resepPrint->save()) {
                throw new Exception(CHtml::errorSummary($resepPrint));
            }
            foreach ($this->s->payments as $pay) {
                $resepPayment = new ResepPayment;
                $resepPayment->nama_bank = $pay->bank->nama_bank;
                $resepPayment->amount = $pay->amount;
                $resepPayment->resep_print_id = $resepPrint->resep_print_id;
                if (!$resepPayment->save()) {
                    throw new Exception(CHtml::errorSummary($resepPayment));
                }
            }
            $urut = 1;
            $urut_medis = 1;
            $urut_back = count($this->s->salestransDetails);
            foreach ($this->s->salestransDetails as $d) {
                $resepPrintDetil = new ResepPrintDetails;
                $resepPrintDetil->kode_item = $d->barang->kode_barang;
                $resepPrintDetil->nama_item = $d->barang->nama_barang;
                $resepPrintDetil->stok = $d->barang->grup->kategori->have_stock;
                $resepPrintDetil->qty = $d->qty;
                $resepPrintDetil->sat = $d->barang->sat == null ? '. ' : $d->barang->sat;
                $resepPrintDetil->price = $d->price;
                $resepPrintDetil->total = $d->bruto;
                $resepPrintDetil->vatrp = $d->vatrp;
	            $resepPrintDetil->disc = $d->disc;
                $resepPrintDetil->discrp = $d->discrp;
                $resepPrintDetil->ketpot = $d->ketpot;
                $resepPrintDetil->resep_print_id = $resepPrint->resep_print_id;
                $resepPrintDetil->salestrans_details = $d->salestrans_details;
                if (count($d->barang->reseps) == 0) {
                    $resepPrintDetil->r_ = 0;
                    $resepPrintDetil->no_urut = $urut;
                    $urut++;
                    if (!$resepPrintDetil->save()) {
                        throw new Exception(CHtml::errorSummary($resepPrintDetil));
                    }
                } else {
                    if ($resepPrint->doc_ref_resep == null) {
                        $ref = new Reference();
                        $docref = $ref->get_next_reference(RESEP);
                        if (!$resepPrint->saveAttributes(['doc_ref_resep' => $docref])) {
                            throw new Exception(CHtml::errorSummary($resepPrintDetil));
                        }
                        $ref->save(RESEP, $resepPrint->resep_print_id, $docref);
                    }
                    $resepPrintDetil->r_ = 1;
                    $resepPrintDetil->no_urut = $urut_back;
                    $resepPrintDetil->no_resep = $urut_medis;
                    $urut_back--;
                    if (!$resepPrintDetil->save()) {
                        throw new Exception(CHtml::errorSummary($resepPrintDetil));
                    }
                    $urut_bahan = 1;
                    foreach ($d->barang->reseps[0]->resepDetails as $bhn) {
                        $resepBahan = new ResepPrintBahan;
                        $resepBahan->no_urut = $urut_bahan;
                        $resepBahan->kode_bahan = $bhn->kode_bahan;
                        $resepBahan->nama_bahan = $bhn->nama_bahan;
                        $resepBahan->qty = $bhn->qty * $resepPrintDetil->qty;
                        $resepBahan->sat = $bhn->sat == null ? ' ' : $bhn->sat;
                        $resepBahan->resep_print_details_id = $resepPrintDetil->resep_print_details_id;
                        if (!$resepBahan->save()) {
                            throw new Exception(CHtml::errorSummary($resepBahan));
                        }
                        $urut_bahan++;
                    }
                }
            }
        }
        return $resepPrint;
    }
    public function buildTxt()
    {
        $newLine = "\r\n";
        $raw = parent::setCenter($this->resepPrint->header0);
        $raw .= $newLine;
        $raw .= parent::setCenter($this->resepPrint->header1);
        $raw .= $newLine;
        $raw .= parent::setCenter($this->resepPrint->header2);
        $raw .= $newLine;
        $raw .= parent::setCenter($this->resepPrint->header3);
        $raw .= $newLine;
        $raw .= parent::setCenter($this->resepPrint->header4);
        $raw .= $newLine;
        $raw .= parent::setCenter($this->resepPrint->header5);
        $raw .= $newLine;
//        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("No. Receipt", $this->resepPrint->doc_ref_receipt);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Tanggal", sql2date($this->resepPrint->date_, "dd-MMM-yyyy"));
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Customer Code", $this->resepPrint->pasien_code);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Customer Name", $this->resepPrint->pasien_name);
        $raw .= $newLine;
//        $user = Users::model()->findByPk($this->s->user_id);
        if (PRINT_NAMA_DOKTER) {
            $raw .= parent::addHeaderSales("Doctor", $this->resepPrint->dokter);
            $raw .= $newLine;
        }
        $raw .= parent::addHeaderSales("Cashier", $this->resepPrint->kasir);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Beauty", $this->resepPrint->beauty);
//        $user = Users::model()->findByPk(Yii::app()->getUser()->id);
//        $raw .= parent::addHeaderSales("Cashier", $user->name);
        $raw .= $newLine;
        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
//        $raw .= "Item Code                             QTY                  TOTAL";
//        $raw .= $newLine;
//        $raw .= parent::fillWithChar("-");
//        $raw .= $newLine;
//        $disc = 0;
//        foreach ($this->s->salestransDetails as $d) {
//            $disc += $d->discrp1;
//            $raw .= parent::addItemCodeReceipt($d->barang->kode_barang,
//                number_format(($d->qty), 2),
//                number_format(($d->bruto), 2) . ($d->vatrp > 0 ? "*" : ""));
//            $raw .= $newLine;
//            $raw .= parent::addItemNameReceipt($d->barang->nama_barang, 46);
//            $raw .= $newLine;
//            if ($d->discrp != 0) {
//                $raw .= parent::addItemDiscReceipt($d->disc != 0 ? number_format($d->disc, 2) . ' %' : '',
//                    number_format(-($d->discrp), 2));
//                $raw .= $newLine;
//            }
//        }
        $crite = new CDbCriteria();
//        $crite->addCondition('r_ = 0');
        $crite->addCondition('resep_print_id = :resep_print_id');
        $crite->order = 'no_urut';
        $crite->params = [':resep_print_id' => $this->resepPrint->resep_print_id];
        /** @var ResepPrintDetails[] $resepPrintDetils */
        $resepPrintDetils = ResepPrintDetails::model()->findAll($crite);
        foreach ($resepPrintDetils as $rpd) {
            $raw .= parent::addItemCodeReceiptNoSat(number_format($rpd->no_urut, 0) . ".",
                $rpd->kode_item, $rpd->qty,
                $rpd->sat, number_format($rpd->total, 0, ',', '.') .
                ($rpd->vatrp > 0 ? "*" : ""));
            $raw .= $newLine;
            if (PRINT_NAMA_BARANG == TRUE){
                /*
                 * Untuk Aishaderm, nama barang tidak dilampirkan.
                 */
                $raw .= parent::addItemNameReceipt($rpd->nama_item, 37);
	            $raw .= $newLine;
	            if ($rpd->ketpot <> '-' && $rpd->ketpot <> '')
	            {
	            	$raw .= parent::addItemNameReceipt($rpd->ketpot, 37);
		            $raw .= $newLine;
	            }
            }
            if ($rpd->discrp != 0) {
            	#PERMINTAAN BU NANA DITAMBAHKAN DISKON DALAM PERSEN
                $raw .= parent::addItemDiscReceipt($rpd->disc."%",
                    number_format(-($rpd->discrp), 0, ',', '.'));
                $raw .= $newLine;
            }
        }
//        if ($this->resepPrint->doc_ref_resep != null) {
//            $raw .= $newLine;
//            $raw .= parent::fillWithChar("-");
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("No. Resep", $this->resepPrint->doc_ref_resep);
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("Apt./AA", $this->resepPrint->apoteker);
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("A/N. Resep", $this->resepPrint->pasien_name);
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("Alamat", $this->resepPrint->alamat);
//            $raw .= $newLine;
//            $raw .= parent::addHeaderSales("Dokter", $this->resepPrint->dokter);
//            $raw .= $newLine;
//        }
//        $raw .= $newLine;
//        $criteria = new CDbCriteria();
//        $criteria->addCondition('r_ = 1');
//        $criteria->addCondition('resep_print_id = :resep_print_id');
//        $criteria->order = 'no_resep';
//        $criteria->params = [':resep_print_id' => $this->resepPrint->resep_print_id];
//        /** @var ResepPrintDetails[] $resepPrintDetils */
//        $resepPrintDetils = ResepPrintDetails::model()->findAll($criteria);
//        foreach ($resepPrintDetils as $rpd) {
//            $raw .= parent::addItemCodeResepSat("R$rpd->no_resep /",
//                number_format($rpd->qty, 1, ',', '.'), $rpd->sat);
//            $raw .= $newLine;
//            $cri = new CDbCriteria();
//            $cri->addCondition('resep_print_details_id = :resep_print_details_id');
//            $cri->order = 'no_urut';
//            $cri->params = [':resep_print_details_id' => $rpd->resep_print_details_id];
//            /** @var ResepPrintBahan[] $resepPrintBahan */
//            $resepPrintBahan = ResepPrintBahan::model()->findAll($cri);
//            foreach ($resepPrintBahan as $rpb) {
//                $raw .= parent::addResepBahan("$rpb->no_urut.", $rpb->nama_bahan,
//                    number_format($rpb->qty, 1, ',', '.'), $rpb->sat);
//                $raw .= $newLine;
//            }
//            $raw .= $newLine;
//        }
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Sub Total", number_format($this->resepPrint->subtotal, 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Discount", number_format($this->resepPrint->disc, 2));
        $raw .= $newLine;
        if (GST == TRUE){
            $raw .= parent::addLeftRight("GST", number_format($this->resepPrint->vat, 2));
        } else {
            $raw .= parent::addLeftRight("VAT", number_format($this->resepPrint->vat, 2));
        }        
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Rounding", number_format($this->resepPrint->rounding, 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Amount Due", number_format($this->resepPrint->amount_due, 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("", self::fillWithChar("-", 17));
        $raw .= $newLine;
        $raw .= $newLine;
        foreach ($this->resepPrint->resepPayments as $pay) {
            $raw .= parent::addLeftRight($pay->nama_bank, number_format(($pay->amount), 2));
            $raw .= $newLine;
        }
        if ($this->resepPrint->kembalian != 0) {
            $raw .= parent::addLeftRight("CHANGE", number_format(($this->resepPrint->kembalian), 2));
            $raw .= $newLine;
        }
        $raw .= $newLine;
        if ($this->resepPrint->footer0 != '') {
            $raw .= parent::setCenter($this->resepPrint->footer0);
            $raw .= $newLine;
        }
        if ($this->resepPrint->footer1 != '') {
            $raw .= parent::setCenter($this->resepPrint->footer1);
            $raw .= $newLine;
        }
        if ($this->resepPrint->footer2 != '') {
            $raw .= parent::setCenter($this->resepPrint->footer2);
            $raw .= $newLine;
        }
        if ($this->resepPrint->footer3 != '') {
            $raw .= parent::setCenter($this->resepPrint->footer3);
            $raw .= $newLine;
        }
        if ($this->resepPrint->footer4 != '') {
            $raw .= parent::setCenter($this->resepPrint->footer4);
            $raw .= $newLine;
        }
        if ($this->resepPrint->footer5 != '') {
            $raw .= parent::setCenter($this->resepPrint->footer5);
            $raw .= $newLine;
        }
        $raw .= $newLine;
        $raw .= parent::addLeftRight("", get_date_today("hh:mm:ss/dd-MM-yyyy"));
        $raw .= $newLine;
        U::save_file(ReportPath . $this->s->doc_ref . '.txt', $raw);
        return $raw;
//        return base64_encode(chr(27) . chr(64) . parent::fillWithChar("-") . chr(27) . chr(105));
    }
}