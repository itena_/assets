<?php
/**
 * Created by PhpStorm.
 * User: MASTER
 * Date: 7/14/14
 * Time: 8:13 AM
 */
class PrintCashOut extends BasePrint
{
    /** @var  $s Kas */
    private $s;
    function __construct($s)
    {
//        $s = new Kas;
        $this->s = $s;
    }
    public function buildTxt()
    {
        $newLine = "\r\n";
        $raw = parent::setCenter(SysPrefs::get_val('receipt_header0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header5'));
        $raw .= $newLine;
//        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Doc. Ref", $this->s->doc_ref);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Tanggal", sql2date($this->s->tgl, "dd-MMM-yyyy"));
//        $raw .= $newLine;
//        $raw .= parent::addHeaderSales("To", $this->s->who);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("No. Receipt", $this->s->no_kwitansi);
        $raw .= $newLine;
//        $user = Users::model()->findByPk(Yii::app()->getUser()->id);
//        $raw .= parent::addHeaderSales("Cashier", $user->name);
        $user = Users::model()->findByPk($this->s->user_id);
        $raw .= parent::addHeaderSales("Cashier", $user->name);
        $raw .= $newLine;
        $raw .= "Note:";
        $raw .= $newLine;
        $raw .= parent::__wordwrap($this->s->keperluan, 1, 0, CHARLENGTHRECEIPT);
        $raw .= $newLine;
//        $raw .= parent::fillWithChar("-");
//        $raw .= $newLine;
//        $raw .= "COA    Note                                  TOTAL";
//        $raw .= $newLine;
        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $disc = 0;
        foreach ($this->s->kasDetails as $d) {
            if (!$d->visible) {
                continue;
            }
//            $d = new KasDetail;
            $raw .= parent::addLeftRight($d->account_code, number_format(abs($d->total), 2) .
                str_repeat(' ', 10));
//            $raw .= self::addItemCode('   ' . $d->account_code, '', '',
//                number_format(abs($d->total), 2) . str_repeat(' ', 10), CHARLENGTHRECEIPT);
            $raw .= $newLine;
//            $raw .= parent::addItemNameReceipt($d->item_name, CHARLENGTHRECEIPT, 7);
//            $raw .= parent::__wordwrap($this->s->keperluan, 1, 0, CHARLENGTHRECEIPT);
            $raw .= parent::__wordwrap($d->item_name, 1, 0, CHARLENGTHRECEIPT);
            $raw .= $newLine;
        }
        $raw .= parent::fillWithChar("-");
        $raw .= $newLine;
        $raw .= $newLine;
        $raw .= parent::addLeftRight("Total", number_format(abs($this->s->amount), 2));
        $raw .= $newLine;
        $raw .= parent::addLeftRight("", self::fillWithChar("-", 17));
        $raw .= $newLine;
        $raw .= $newLine;
//        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer5'));
        $raw .= $newLine;
        $raw .= '.';
        $raw .= $newLine;
//        U::save_file(ReportPath.$this->s->doc_ref.'.txt',$raw);
        return $raw;
    }
    function addItemCode($itemKode, $qty, $price, $subtotal, $l = CHARLENGTHRECEIPT)
    {
        $litemKode = strlen($itemKode); // max 13
        $mitemKode = 13;
        $lqty = strlen($qty); //max 6
        $mqty = 6;
        $lprice = strlen($price); //max 15
        $mprice = 15;
        $lsubtotal = strlen($subtotal); //max 16
        $msubtotal = 16;
        if ($litemKode > $mitemKode) {
            $itemKode = substr($itemKode, 0, $mitemKode);
//            $litemKode = $mitemKode;
        }
        if ($lqty > $mqty) {
            $qty = substr($qty, 0, $mitemKode);
//            $lqty = $mqty;
        }
        if ($lprice > $mprice) {
            $price = substr($price, 0, $mprice);
//            $lqty = $mqty;
        }
        if ($lsubtotal > $msubtotal) {
            $subtotal = substr($subtotal, 0, $msubtotal);
//            $lsubtotal = $msubtotal;
        }
        $msg1 = parent::addLeftRight($itemKode, $qty, $mitemKode + $mqty);
        $msg2 = parent::addLeftRight($msg1, $price, $mitemKode + $mqty + $mprice);
        return parent::addLeftRight($msg2, $subtotal, $l);
    }
} 