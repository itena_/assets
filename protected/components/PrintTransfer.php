<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 22/09/2014
 * Time: 16:26
 */

class PrintTransfer extends BasePrint{
    private $s;
    function __construct($trans_no)
    {
        $s = BankTrans::get_list_bank_transfer_by_trans_no($trans_no);
        $this->s = $s;
    }
    public function buildTxt()
    {
        $newLine = "\r\n";
        $raw = parent::setCenter(SysPrefs::get_val('receipt_header0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_header5'));
        $raw .= $newLine;
        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("No. Receipt", $this->s->ref);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Date", sql2date($this->s->trans_date, "dd-MMM-yyyy"));
        $raw .= $newLine;
        $from_bank = Bank::model()->findByPk($this->s->bank_act_asal);
        $raw .= parent::addHeaderSales("From Bank", $from_bank->nama_bank);
        $raw .= $newLine;
        $to_bank = Bank::model()->findByPk($this->s->bank_act_tujuan);
        $raw .= parent::addHeaderSales("To Bank", $to_bank->nama_bank);
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Amount", number_format($this->s->amount,2));
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Charge", number_format($this->s->charge,2));
        $raw .= $newLine;
        $raw .= parent::addHeaderSales("Memo", $this->s->memo);
        $raw .= $newLine;
        $user = Users::model()->findByPk(Yii::app()->getUser()->id);
        $raw .= parent::addHeaderSales("Employee", $user->name);
        $raw .= $newLine;
        $raw .= parent::fillWithChar("=");
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer0'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer1'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer2'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer3'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer4'));
        $raw .= $newLine;
        $raw .= parent::setCenter(SysPrefs::get_val('receipt_footer5'));
        $raw .= $newLine;
        U::save_file(ReportPath.$this->s->ref.'.txt',$raw);
        return $raw;
    }
} 