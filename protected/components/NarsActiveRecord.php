<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 2/16/2016
 * Time: 9:54 AM
 */
class NarsActiveRecord extends CActiveRecord
{
    private static $dbnars = null;
    protected static function getNarsDbConnection()
    {
        if (self::$dbnars !== null)
            return self::$dbnars;
        else {
            self::$dbnars = Yii::app()->dbnars;
            if (self::$dbnars instanceof CDbConnection) {
                self::$dbnars->setActive(true);
                return self::$dbnars;
            } else
                throw new CDbException(Yii::t('yii', 'Active Record requires a "db" CDbConnection application component.'));
        }
    }
}