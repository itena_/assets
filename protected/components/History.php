<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 27/12/2014
 * Time: 10:30
 */
class History
{
    private $_total_amount = 0;
    private $detil = array();
    public $_id = array();
    public function add_gl($type, $trans_id, $date_, $ref, $account, $memo_, $comment_,
                           $amount, $cf, $store = null, $person_id = null)
    {
        if ($person_id == null) {
            $person_id = Yii::app()->user->getId();
        }
//        $is_bank_to = $this->is_bank_account($account);
        if ($amount != 0) {
            $this->add_gl_trans($type, $trans_id, $date_, $account, $memo_, $amount,
                $person_id, $cf, $store);
        }
//        if ($is_bank_to) {
//            $bank = Bank::model()->find("account_code = :account_code",
//                array(":account_code" => $account));
//            if ($bank == null) {
//                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bank transaction')));
//            }
//            $this->add_bank_trans($type, $trans_id, $bank->bank_id, $ref, $date_,
//                $amount, $person_id, $store);
//        }
        if (strlen($comment_) > 0) {
            $this->add_comments($type, $trans_id, $date_, $comment_);
        }
        $this->_total_amount += $amount;
        $this->_id[] = [$account, $amount];
    }




    public function add_gl_asset($type, $trans_id, $date_, $ref, $account, $memo_, $comment_,
                                 $amount, $cf, $store = null)
    {
        $person_id = Yii::app()->user->getId();
        $this->add_gl_trans_asset($type, $trans_id, $date_, $account, $memo_, $amount,
            $person_id, $cf, $store);
        if (strlen($comment_) > 0) $this->add_comments($type, $trans_id, $date_, $comment_);
        $this->_total_amount += $amount;
    }
    public function add_gl_Perlengkapan($type, $trans_id, $date_, $ref, $account, $memo_, $comment_,
                                        $amount, $cf, $store = null)
    {
        $person_id = Yii::app()->user->getId();
        $this->add_gl_trans_perlengkapan($type, $trans_id, $date_, $account, $memo_, $amount,
            $person_id, $cf, $store);
        if (strlen($comment_) > 0) $this->add_comments($type, $trans_id, $date_, $comment_);
        $this->_total_amount += $amount;
    }
    public function is_bank_account($account_code)
    {
        $comm = Yii::app()->db->createCommand("SELECT account_code FROM nscc_chart_master WHERE
        account_code = :account_code AND (kategori = :kas OR kategori = :bank)");
        $bank_act = $comm->queryAll(true, array(
            ":account_code" => $account_code,
            ':kas' => COA_GRUP_KAS,
            ':bank' => COA_GRUP_BANK
        ));
        return count($bank_act) > 0;
    }
    public function add_gl_trans($type, $trans_id, $date_, $account, $memo_, $amount, $person_id, $cf, $store = null)
    {
        $gl_trans = new GlTrans();
        $gl_trans->type = $type;
        $gl_trans->type_no = $trans_id;
        $gl_trans->tran_date = $date_;
        $gl_trans->account_code = $account;
        $gl_trans->memo_ = $memo_;
        $gl_trans->id_user = $person_id;
        $gl_trans->amount = $amount;
        $gl_trans->cf = $cf;
        $gl_trans->store = $store;
        if (!$gl_trans->save()){
            throw new Exception(t('save.model.fail', 'app', array('{model}' => "General Ledger $trans_id")) . CHtml::errorSummary($gl_trans));
        }/* else{
            if(PUSH_PUSAT){
                U::runCommand('gltrans', '--id=' . $gl_trans->counter, 'gl_'.$gl_trans->counter.'.log');
            }
        } */
    }
    public function add_gl_trans_asset($type, $trans_id, $date_, $account, $memo_, $amount, $person_id, $cf, $store = null)
    {
        $gl_trans = new GlTransAsset();
        $gl_trans->type = $type;
        $gl_trans->type_no = $trans_id;
        $gl_trans->tran_date = $date_;
        $gl_trans->account_code = $account;
        $gl_trans->memo_ = $memo_;
        $gl_trans->id_user = $person_id;
        $gl_trans->amount = $amount;
        $gl_trans->cf = $cf;
        $gl_trans->store = $store;
        if (!$gl_trans->save())
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'General Ledger')) . CHtml::errorSummary($gl_trans));
    }
    public function add_gl_trans_perlengkapan($type, $trans_id, $date_, $account, $memo_, $amount, $person_id, $cf, $store = null)
    {
        $gl_trans = new GlTransPerlengkapan();
        $gl_trans->type = $type;
        $gl_trans->type_no = $trans_id;
        $gl_trans->tran_date = $date_;
        $gl_trans->account_code = $account;
        $gl_trans->memo_ = $memo_;
        $gl_trans->id_user = $person_id;
        $gl_trans->amount = $amount;
        $gl_trans->cf = $cf;
        $gl_trans->store = $store;
        if (!$gl_trans->save())
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'General Ledger')) . CHtml::errorSummary($gl_trans));
    }
    public function add_bank_trans($type, $trans_no, $bank_act, $ref, $date_,
                                   $amount, $person_id, $store = null)
    {
        $bank_trans = new BankTrans;
        $bank_trans->type_ = $type;
        $bank_trans->trans_no = $trans_no;
        $bank_trans->bank_id = $bank_act;
        $bank_trans->ref = $ref;
        $bank_trans->tgl = $date_;
        $bank_trans->amount = $amount;
        $bank_trans->id_user = $person_id;
        $bank_trans->store = $store;
        if (!$bank_trans->save()){ 
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Bank transaction')) . CHtml::errorSummary($bank_trans));
        }/* else{
            if(PUSH_PUSAT){
                U::runCommand('banktrans', '--id=' . $bank_trans->bank_trans_id,  'banktrans_'.$bank_trans->bank_trans_id.'.log');
            }
        } */
    }
    public function validate()
    {
//        $this->_total_amount = array_sum(array_column($this->detil,'amount'));
        if (round($this->_total_amount, 2) != 0.00)
            throw new Exception("Gagal menyimpan jurnal karena tidak balance. Total GL = " .
                number_format($this->_total_amount, 2) . " \n" . CJSON::encode($this->_id));
    }
    public function add_comments($type, $type_no, $date_, $memo_)
    {
        if ($memo_ != null && $memo_ != "") {
            $comment = Comments::model()->findByAttributes([
                'type' => $type,
                'type_no' => $type_no
            ]);
            if ($comment == null) {
                $comment = new Comments();
            }
            $comment->type = $type;
            $comment->type_no = $type_no;
            $comment->date_ = $date_;
            $comment->memo_ = $memo_;
            if (!$comment->save())
                throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Comments')) . CHtml::errorSummary($comment));
        }
    }


    public function add_history_status($businessunit_id, $masterassetid, $asset_id, $docref, $ati,
                                       $name, $branch, $price, $newprice, $class, $tariff, $period,
                                       $penyusutanperbulan, $penyusutanpertahun, $desc, $status)
    {
        $history = new AssetHistory;
        $history->businessunit_id = $businessunit_id;
        $history->masterassetid = $masterassetid;
        $history->asset_id = $asset_id;
        $history->docref = $docref;
        $history->ati = $ati;
        $history->name = $name;
        $history->branch = $branch;
        $history->price = $price;
        $history->newprice = $newprice;
        $history->class = $class;
        $history->tariff = $tariff;
        $history->period = $period;
        $history->penyusutanperbulan = $penyusutanperbulan;
        $history->penyusutanpertahun = $penyusutanpertahun;
        $history->desc =  $desc;
        $history->status =  $status;
        $history->sdate = new CDbExpression('NOW()');
        $history->created_at = new CDbExpression('NOW()');
        $history->updated_at = new CDbExpression('NOW()');

        if (!$history->save()) {
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'AssetHistory')) . CHtml::errorSummary($history));
        }
    }
}