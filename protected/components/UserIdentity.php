<?php
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	private $_id;
	private $_session;
//	private $_browser;
	public $user;
	/**
	 * @return mixed
	 */
	public function getSession() {
		return $this->_session;
	}
	/**
	 * @return mixed
	 */
//	public function getBrowser() {
//		return $this->_browser;
//	}
	/**
	 * @param mixed $browser
	 */
//	public function setBrowser( $browser ) {
//		$this->_browser = $browser;
//	}
	public function authenticate() {
//		$users = array(
//			// username => password
//			'demo'  => 'demo',
//			'admin' => 'admin',
//		);
		/** @var Users $user */
		$user = Users::model()->findByAttributes( array(
			'user_id' => $this->username,
			//'store'   => STOREID,
			'active'  => 1
		) );
		if ( $user === null ) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		}
		//if(!isset($users[$this->username]))
		//			$this->errorCode=self::ERROR_USERNAME_INVALID;
		else if ( ! bCrypt::verify( $this->password, $user->password ) ) {
//            $pass = $user->password;
//            $pass2 = $this->password;
//            $very = $this->encrypt($this->password);
//            $encrypt = $this->encrypt("nove");
//            $enc = NEW bCrypt();
//            $veryvied = $enc->verify($this->password, $user->password);
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
			LogTrans::saveLog( '', '', 'LOGIN GAGAL', '' );
		} else {
			$this->_id      = $user->id;
			$this->username = $user->user_id;
			// $this->username = 'admin';
			$this->errorCode       = self::ERROR_NONE;
			$user->last_visit_date = get_date_today( 'yyyy-MM-dd HH:mm:ss' );
			$user_agent            = $user->session_hash_string( $_SERVER['HTTP_USER_AGENT'], $this->id );
			$user->session         = $user_agent;
			$this->_session        = $user->session;
//			$_SESSION              = array();
//			$_SESSION['INIT']      = true;
			// Set hashed http user agent
//			$_SESSION['USER_AGENT_KEY'] = $user_agent;
//			$user->session_validate();
			$user->save();
			$this->setUser( $user );
//			LogTrans::saveLog( '', '', 'LOGIN SUKSES', '' );
//			$user = Users::model()->findByPk( Yii::app()->user->getId() );
		}
		return ! $this->errorCode;
	}
	public function getId() {
		return $this->_id;
	}
	public function encrypt( $value ) {
		$enc = NEW bCrypt();
		return $enc->hash( $value );
	}
	public function getUser() {
		return $this->user;
	}
	public function setUser( Users $user ) {
		$_arr['id']       = $this->_id;
		$_arr['username'] = $user->user_id;
		$_arr['name']     = $user->name;
		$_arr['session']  = $user->session;
//		$_arr['browser']  = $this->_browser;
		$this->user       = $_arr;
	}
}
