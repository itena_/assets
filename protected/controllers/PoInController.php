<?php
class PoInController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $is_new = $_POST['mode'] == 0;
            $po_id = $_POST['id'];
            $detils = CJSON::decode($_POST['detil']);
            $type = (isset($_POST['type_']) && $_POST['type_'] == 1)? PO_IN : PO_OUT;
//            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new Po : $this->loadModel($po_id, "Po");
                $doc_ref = $model->doc_ref;
                if ($is_new) {
                    if(defined('CUSTOM_DOCREF_PO') && CUSTOM_DOCREF_PO){
                        /* Custom format (Naavagreen)*/
                        $doc_ref = U::createReference($type);
                    }else{
                        /* Standard format */
                        $ref = new Reference();
                        $doc_ref = $ref->get_next_reference($type);
                    }
                } else {
                    if ($model->status == PO_CLOSED) {
                        throw new Exception("Purchase Order tidak bisa diedit karena status bukan open.");
                    }
                    PoDetails::model()->model()->updateAll(array('visible' => 0), 'po_id = :po_id', array(':po_id' => $po_id));
                }
                $_POST['PoIn']['ppn'] = 'N';
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') {
                        continue;
                    }
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['PoIn'][$k] = $v;
                }
                $_POST['PoIn']['status'] = 0;
                $_POST['PoIn']['doc_ref'] = $doc_ref;
                $model->attributes = $_POST['PoIn'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'PO item')) . CHtml::errorSummary($model));
                }
                foreach ($detils as $detil) {
                    $item_details = new PoDetails;
                    $_POST['PoDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['PoDetails']['description'] = $detil['description'];
                    $_POST['PoDetails']['qty'] = get_number($detil['qty']);
                    $_POST['PoDetails']['price'] = get_number($detil['price']);
                    $_POST['PoDetails']['sub_total'] = get_number($detil['sub_total']);
                    $_POST['PoDetails']['disc'] = get_number($detil['disc']);
                    $_POST['PoDetails']['disc_rp'] = get_number($detil['disc_rp']);
                    $_POST['PoDetails']['total'] = get_number($detil['total']);
                    $_POST['PoDetails']['ppn'] = get_number($detil['ppn']);
                    $_POST['PoDetails']['ppn_rp'] = get_number($detil['ppn_rp']);
                    $_POST['PoDetails']['pph_id'] = get_number($detil['pph_id']);
                    $_POST['PoDetails']['pph'] = get_number($detil['pph']);
                    $_POST['PoDetails']['pph_rp'] = get_number($detil['pph_rp']);
//                    $_POST['PoDetails']['total_disc'] = get_number($detil['total_disc']);
                    $_POST['PoDetails']['total_dpp'] = get_number($detil['total_dpp']);
                    $_POST['PoDetails']['charge'] = get_number($detil['charge']);
                    $_POST['PoDetails']['seq'] = get_number($detil['seq']);
                    $_POST['PoDetails']['item_id'] = isset($detil['item_id'])?$detil['item_id']:null;
                    $_POST['PoDetails']['po_id'] = $model->po_id;
                    $item_details->attributes = $_POST['PoDetails'];
                    if (!$item_details->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'PO item detail')) . CHtml::errorSummary($item_details));
                    }
                }
                if ($is_new) {
                    if(!(defined('CUSTOM_DOCREF_PO') && CUSTOM_DOCREF_PO)){
                        $ref->save($type, $model->po_id, $doc_ref);
                    }
                }
                
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionClose()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data gagal disimpan";
            $model = $this->loadModel($_POST['po_id'], 'Po');
            
            if ($model->status == PO_CLOSED) {
                $msg = "Tidak dapat menutup Purchase Order.<br>Purchase Order <b>" . $model->doc_ref . "</b> telah berstatus CLOSED.";
                $status = false;
            } else {
                $model->status = PO_CLOSED;
                $model->closed_id_user = User()->getId();
                $model->closed_tdate = new CDbExpression('NOW()');
                if ($model->save()) {
                    $status = true;
                    $msg = "Purchase Order <b>" . $model->doc_ref . "</b> telah ditutup.";
                } else {
                    $msg .= " " . implode(", ", $model->getErrors());
                    $status = false;
                }
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        $cmd = DbCmd::instance()
            ->addSelect('{{po}}.*')
            ->addFrom('{{po}}')
            ->addOrder('{{po}}.tdate DESC')
            ;
        if (isset($_POST['tgl'])) {
            $cmd->addCondition('{{po}}.tgl = :tgl')
                ->addParam(':tgl', $_POST['tgl']);
        }
        if (isset($_POST['doc_ref'])) {
            $cmd->addCondition('{{po}}.doc_ref LIKE :doc_ref')
                ->addParam(':doc_ref', '%' . $_POST['doc_ref'] . '%');
        }
        if (isset($_POST['doc_ref_pr'])) {
            $cmd->addCondition('{{po}}.doc_ref_pr LIKE :doc_ref_pr')
                ->addParam(':doc_ref_pr', '%' . $_POST['doc_ref_pr'] . '%');
        }
        if (isset($_POST['supp_company'])) {
            $cmd->addCondition('{{po}}.supp_company LIKE :supp_company')
                ->addParam(':supp_company', '%' . $_POST['supp_company'] . '%');
        }
        if (isset($_POST['nama'])) {
            $cmd->addCondition('{{po}}.nama LIKE :nama')
                ->addParam(':nama', '%' . $_POST['nama'] . '%');
        }
        if (isset($_POST['divisi'])) {
            $cmd->addCondition('{{po}}.divisi LIKE :divisi')
                ->addParam(':divisi', '%' . $_POST['divisi'] . '%');
        }
        if (isset($_POST['acc_charge'])) {
            $cmd->addCondition('{{po}}.acc_charge LIKE :acc_charge')
                ->addParam(':acc_charge', '%' . $_POST['acc_charge'] . '%');
        }
        if (isset($_POST['store'])) {
            $cmd->addCondition('{{po}}.store LIKE :store')
                ->addParam(':store', '%' . $_POST['store'] . '%');
        }
        
        if (isset($_POST['po_id'])) {
            $cmd->addCondition('{{po}}.po_id = :po_id')
                ->addParam(':po_id', $_POST['po_id']);
        }
        if (isset($_POST['query'])) {
            $cmd->addCondition('{{po}}.doc_ref LIKE :query OR {{po}}.supp_company LIKE :query')
                ->addParam(':query', '%' . $_POST['query'] . '%')
                ->addCondition('received = 0');
        }
        
        if (isset($_POST['terimaBarang']) && (int)$_POST['terimaBarang'] == 1) {
            /*
            * filter saat penerimaan barang
            * hanya PO dengan status OPEN dan PARTIALLY_RECEIVED yang tampil
            */
            $cmd->addInCondition("{{po}}.status", [PO_OPEN, PO_PARTIALLY_RECEIVED]);
        }
        if (isset($_POST['status']) && $_POST['status'] !== 'all') {
            /* filter grid berdasar status */
            $cmd->addCondition('{{po}}.status = :status')
                ->addParam(':status', $_POST['status']);
        }

        $count = $cmd->queryCount();
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $cmd->setLimit(array_key_exists('limit', $_POST)? $_POST['limit'] : 20, array_key_exists('start', $_POST)? $_POST['start'] : 0);
        }
        $model = $cmd->queryAll();
        $this->renderJsonArrWithTotal($model, $count);
    }
    
    public function actionHistoryHarga() {
        /*
         * history harga untuk ditampilkan saat membuat PO
         */
        $model = Po::get_historyHarga($_POST['barang_id']);
        $this->renderJsonArr($model);
    }
}