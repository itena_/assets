<?php
class PaymentController extends GxController
{
    public function actionCreate()
    {
        $model = new Payment;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Payment'][$k] = $v;
            }
            $model->attributes = $_POST['Payment'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->payment_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Payment');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Payment'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Payment'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->payment_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->payment_id));
            }
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $criteria->select = "np.payment_id,np.bank_id,np.card_number,abs(np.kembali) kembali,
        np.salestrans_id,abs(np.amount) amount";
        $criteria->alias = "np";
        $criteria->addCondition('salestrans_id = :salestrans_id');
        $criteria->params = array(":salestrans_id"=>$_POST['salestrans_id']);
        $model = Payment::model()->findAll($criteria);
        $total = Payment::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}