<?php
class StoreController extends GxController
{
    public function actionCreate()
    {
        $model = new Store;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Store'][$k] = $v;
            }
            $businessunit_id = $_COOKIE['businessunitid'];
//            $_POST['Store']['conn'] = Encrypt($_POST['Store']['conn']);
            $model->attributes = $_POST['Store'];
            $model->telp = $_POST['Store']['telp'];
            $model->email = $_POST['Store']['email'];
            $model->alamat = $_POST['Store']['alamat'];
            $model->wilayah_id= $_POST['Store']['wilayah'];
            $model->businessunit_id = $businessunit_id;
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan store " . $model->store_kode;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $businessunit_id = $_COOKIE['businessunitid'];
        $model = Store::model()->findByAttributes(array('store_kode' => $id,'businessunit_id'=> $businessunit_id));

        //$model = $this->loadModel($id, 'Store');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Store'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
//            $_POST['Store']['conn'] = Encrypt($_POST['Store']['conn']);
            //$model->attributes = $_POST['Store'];
            $model->alamat = $_POST['Store']['alamat'];
            $model->telp = $_POST['Store']['telp'];
            $model->email = $_POST['Store']['email'];
            $model->wilayah_id= $_POST['Store']['wilayah'];
            $model->businessunit_id = $businessunit_id;
            if ($model->save()) {
                $status = true;
                $msg =  $model->store_kode." berhasil di edit. " ;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->store_kode));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Store')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }

        $businessunit_id = $_COOKIE['businessunitid'];

        $criteria = new CDbCriteria();
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }

        $criteria->addCondition('businessunit_id = :businessunit_id');
        $param[':businessunit_id'] = $businessunit_id;

        if(isset($_POST['businessunit_id'])){
            $criteria->addCondition('businessunit_id = :businessunit_id');
            $param[':businessunit_id'] = $_POST['businessunit_id'];
        }

        $criteria->order = "id_cabang";
        $criteria->params = $param;


        $model = Store::model()->findAll($criteria);
        $total = Store::model()->count($criteria);
        $this->renderJson($model, $total);
//        $argh = array();
//        foreach ($model AS $dodol) {
//            $attrib = $dodol->getAttributes();
//            $attrib['conn'] = strlen($attrib['conn']) == 0 ? '' : Decrypt($attrib['conn']);
//            $argh[] = $attrib;
//        };
//        $jsonresult = '{"total":"' . $total . '","results":' . json_encode($argh) . '}';
        Yii::app()->end($jsonresult);
    }
}