<?php
class LahaImportController extends GxController {
	public function actionCreate() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$posting = false;
			if ( isset( $_POST['posting'] ) ) {
				$posting = $_POST['posting'] == 'true';
			}
			$docref               = '';
			$is_new               = $_POST['mode'] == 0;
			$bankfees             = CJSON::decode( $_POST['bankfee'] );
			$biayas               = CJSON::decode( $_POST['biaya'] );
			$setors               = CJSON::decode( $_POST['setor'] );
			app()->db->autoCommit = false;
			$transaction          = Yii::app()->db->beginTransaction();
			try {
				$model = $is_new ? new LahaImport : $this->loadModel( $_POST['id'], 'LahaImport' );
				if ( ! $is_new && ( $model == null ) ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'Kas' ) ) . "Fatal error, record not found." );
				}
				if ( $is_new ) {
					$ref    = new Reference();
					$docref = $ref->get_next_reference( IMPORTLAHA );
				} else {
					$docref = $model->doc_ref;
					LahaImportBankFee::model()->deleteAll( 'laha_import_id = :laha_import_id',
						[ ':laha_import_id' => $model->laha_import_id ] );
					LahaImportBiaya::model()->deleteAll( 'laha_import_id = :laha_import_id',
						[ ':laha_import_id' => $model->laha_import_id ] );
					GlTrans::model()->deleteAllByAttributes( [
						'type'    => IMPORTLAHA,
						'type_no' => $model->laha_import_id
					] );
					BankTrans::model()->deleteAllByAttributes( [
						'type_'    => IMPORTLAHA,
						'trans_no' => $model->laha_import_id
					] );
					$model->p = 0;
				}
				foreach ( $_POST as $k => $v ) {
					if ( is_angka( $v ) ) {
						$v = get_number( $v );
					}
					$_POST['LahaImport'][ $k ] = $v;
				}
				$model->attributes = $_POST['LahaImport'];
				$model->doc_ref    = $docref;
				$msg               = "Data gagal disimpan.";
				if ( ! $model->save() ) {
					throw new Exception( t( 'save.model.fail', 'app',
							array( '{model}' => 'Laha Import' ) ) . CHtml::errorSummary( $model ) );
				}
				$gl = new GL();
				if ( $posting ) {
					/** @var Bank $kas_cabang */
					$kas_cabang = Bank::model()->findByPk( $model->p_d_kas_bank_id );
					if ( $kas_cabang == null ) {
						throw new Exception( 'Kas Cabang tidak ditemukan.' );
					}
					# Kas Cabang - Debit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						$kas_cabang->account_code, 'Kas Cabang Penjualan', '',
						$model->p_d_kas_n, 0, $model->store );
					$gl->add_bank_trans( IMPORTLAHA, $model->laha_import_id, $model->p_d_kas_bank_id, $model->doc_ref,
						$model->tgl, $model->p_d_kas_n, $model->id_user, $model->store );
					# Piutang Card - Debit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_PIUTANG_CARD, 'Piutang Card Penjualan', '',
						$model->p_d_piutangcard_n, 1, $model->store );
					# Penjualan - Debit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_SALES_DISK, 'Potongan Penjualan', '',
						$model->p_d_pot_jual_n, 1, $model->store );
					# Penjualan Obat - Kredit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_SALES_OBAT, 'Penjualan Obat', '',
						- ( $model->p_k_obat_n ), 1, $model->store );
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_SALES_OBAT_PPN, 'Ppn Penjualan Obat', '',
						- ( $model->p_k_ppn_obat_n ), 1, $model->store );
					# Penjualan Apotik - Kredit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_SALES_APOTIK, 'Penjualan Apotik', '',
						- ( $model->p_k_apotik_n ), 1, $model->store );
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_SALES_APOTIK_PPN, 'Ppn Penjualan Apotik', '',
						- ( $model->p_k_ppn_apotik_n ), 1, $model->store );
					# Penjualan Perawatan - Kredit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_SALES_JASA, 'Penjualan Perawatan', '',
						- ( $model->p_k_jasa_n ), 1, $model->store );
					# Pendapatan Lain Lain - Kredit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_PENDAPATAN_LAIN, 'Pendapatan Lain Lain', '',
						- ( $model->p_k_lain_lain_n ), 1, $model->store );
					# Pendapatan Di Muka - Kredit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_PENDAPATAN_DIMUKA, 'Pendapatan Di Muka', '',
						- ( $model->p_k_pendmuka_n ), 1, $model->store );
					# Pembulatan - Kredit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_ROUNDING, 'Pembulatan', '',
						- ( $model->p_k_round_n ), 1, $model->store );
				}
				foreach ( $bankfees as $rows ) {
//                    if ($posting){
//                        $id_fee_lama = $rows['laha_import_id'];
//                        LahaImportBankFee::model()->deleteAll('laha_import_id = :laha_import_id',
//                        [':laha_import_id' => $id_fee_lama]);
//                    }
					$bankfee = new LahaImportBankFee;
					unset( $_POST['LahaImportBankFee'] );
					foreach ( $rows as $k => $v ) {
						if ( is_angka( $v ) ) {
							$v = get_number( $v );
						}
						$_POST['LahaImportBankFee'][ $k ] = $v;
					}
					$msg                     = "Data gagal disimpan";
					$bankfee->attributes     = $_POST['LahaImportBankFee'];
					$bankfee->laha_import_id = $model->laha_import_id;
					$bankfee->type_          = 0;
					if ( ! $bankfee->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'Fee Card' ) ) . CHtml::errorSummary( $bankfee ) );
					}
					if ( $posting ) {
						# Bank - Debit #
						$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
							$bankfee->bank0->account_code, $bankfee->bank0->nama_bank, '',
							( floatval( $bankfee->bank ) ), 0, $model->store );
						$gl->add_bank_trans( IMPORTLAHA, $model->laha_import_id, $bankfee->bank_id, $model->doc_ref,
							$model->tgl, ( floatval( $bankfee->bank ) ), $model->id_user, $model->store );
					}
				}
				if ( $posting ) {
					# Fee Card - Debit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_FEE_CARD, 'Fee Card', '',
						( $model->piu_d_fee_n ), 1, $model->store );
					# Piutang Card - Kredit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_PIUTANG_CARD, 'Piutang Card Penjualan', '',
						- ( $model->p_d_piutangcard_n ), 1, $model->store );
				}
				foreach ( $biayas as $row1 ) {
//                    if ($posting){
//                        $id_biaya_lama = $rows['laha_import_id'];
//                        LahaImportBiaya::model()->deleteAll('laha_import_id = :laha_import_id',
//                        [':laha_import_id' => $id_biaya_lama]);
//                    }
					$biaya = new LahaImportBiaya;
					unset( $_POST['LahaImportBiaya'] );
					foreach ( $row1 as $k => $v ) {
						if ( is_angka( $v ) ) {
							$v = get_number( $v );
						}
//                        if ($k == 'store_') $k = 'store';
						$_POST['LahaImportBiaya'][ $k ] = $v;
					}
					$msg                   = "Data gagal disimpan";
					$biaya->attributes     = $_POST['LahaImportBiaya'];
					$biaya->laha_import_id = $model->laha_import_id;
					if ( ! $biaya->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'Biaya' ) ) . CHtml::errorSummary( $biaya ) );
					}
					if ( $posting ) {
						# Biaya - Debit #
						$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
							$biaya->account_code, $biaya->note_, $biaya->note_,
							( floatval( $biaya->amount ) ), 1, $biaya->store );
					}
				}
				if ( $posting ) {
					# Kas Cabang - Kredit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						$kas_cabang->account_code, 'Kas Cabang Biaya', '',
						- ( floatval( $model->b_k_kas_n ) ), 0, $model->store );
					$gl->add_bank_trans( IMPORTLAHA, $model->laha_import_id, $model->p_d_kas_bank_id,
						$model->doc_ref, $model->tgl, - ( floatval( $model->b_k_kas_n ) ), $model->id_user, $model->store );
					# Pph - Kredit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_HUTANG_PPH21, $model->b_k_pph21_note, '',
						- ( $model->b_k_pph21_n ), 1, $model->store );
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_HUTANG_PPH22, $model->b_k_pph22_note, '',
						- ( $model->b_k_pph22_n ), 1, $model->store );
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_HUTANG_PPH23, $model->b_k_pp23_note, '',
						- ( $model->b_k_pp23_n ), 1, $model->store );
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_HUTANG_PPH42, $model->b_k_pph4_2_note, '',
						- ( $model->b_k_pph4_2_n ), 1, $model->store );
					# Pembulatan - Kredit #
					$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
						COA_ROUNDING, 'Pembulatan Biaya', '',
						- ( $model->b_k_round_n ), 1, $model->store );
				}
				foreach ( $setors as $rows2 ) {
					$bankfee = new LahaImportBankFee;
					unset( $_POST['LahaImportBankFee'] );
					foreach ( $rows2 as $k => $v ) {
						if ( is_angka( $v ) ) {
							$v = get_number( $v );
						}
						$_POST['LahaImportBankFee'][ $k ] = $v;
					}
					$msg                     = "Data gagal disimpan";
					$bankfee->attributes     = $_POST['LahaImportBankFee'];
					$bankfee->laha_import_id = $model->laha_import_id;
					$bankfee->type_          = 1;
					if ( ! $bankfee->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'Bank Setor' ) ) . CHtml::errorSummary( $bankfee ) );
					}
					if ( $posting ) {
						# Bank - Debit #
						$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
							$bankfee->bank0->account_code, 'Bank Fee Card', '',
							( floatval( $bankfee->bank ) ), 0, $model->store );
						$gl->add_bank_trans( IMPORTLAHA, $model->laha_import_id, $bankfee->bank_id, $model->doc_ref,
							$model->tgl, ( floatval( $bankfee->bank ) ), $model->id_user, $model->store );
						# Kas Cabang - Kredit #
						$gl->add_gl( IMPORTLAHA, $model->laha_import_id, $model->tgl, $model->doc_ref,
							$kas_cabang->account_code, 'Kas Setor', '',
							- ( floatval( $bankfee->bank ) ), 0, $model->store );
						$gl->add_bank_trans( IMPORTLAHA, $model->laha_import_id, $model->p_d_kas_bank_id, $model->doc_ref,
							$model->tgl, - ( floatval( $bankfee->bank ) ), $model->id_user, $model->store );
					}
				}
				if ( $posting ) {
					$gl->validate();
					$model->p = 1;
					$model->save();
					$gl->add_comments( IMPORTLAHA, $model->laha_import_id, $model->tgl,
						'LAHA IMPORT ' . $model->doc_ref );
				}
				if ( $is_new ) {
					$ref->save( IMPORTLAHA, $model->laha_import_id, $docref );
				}
				$msg = t( 'save.success', 'app' );
				$transaction->commit();
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$status = false;
				$msg    = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode( array(
				'success' => $status,
				'id'      => $docref,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionUnposting( $id ) {
		/** @var LahaImport $model */
		$model = $this->loadModel( $id, 'LahaImport' );
		$msg   = 'Laha Import berhasil diunposting';
//        if (isset($_POST) && !empty($_POST)) {
		app()->db->autoCommit = false;
		$transaction          = Yii::app()->db->beginTransaction();
		try {
			GlTrans::model()->deleteAllByAttributes( [
				'type'    => IMPORTLAHA,
				'type_no' => $id
			] );
			BankTrans::model()->deleteAllByAttributes( [
				'type_'    => IMPORTLAHA,
				'trans_no' => $id
			] );
			if ( ! $model->saveAttributes( [ 'p' => 0 ] ) ) {
				throw new Exception( t( 'save.model.fail', 'app',
						array( '{model}' => 'Import Laha' ) ) . CHtml::errorSummary( $model ) );
			}
			$transaction->commit();
			$status = true;
		} catch ( Exception $e ) {
			$transaction->rollback();
			$status = false;
			$msg    = $e->getMessage();
		}
		app()->db->autoCommit = true;
		echo CJSON::encode( array(
			'success' => $status,
			'msg'     => $msg
		) );
		Yii::app()->end();
//        }
	}
	public function actionCekBalance() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$bankfees = CJSON::decode( $_POST['bankfee'] );
			$biayas   = CJSON::decode( $_POST['biaya'] );
			$setors   = CJSON::decode( $_POST['setor'] );
			$status   = true;
			try {
				$cekBalance = [];
				foreach ( $_POST as $k => $v ) {
					if ( is_angka( $v ) ) {
						$v = get_number( $v );
					}
					$_POST['LahaImport'][ $k ] = $v;
				}
				$kas_cabang = Bank::model()->findByPk( $_POST['LahaImport']['p_d_kas_bank_id'] );
				if ( $kas_cabang != null ) {
					if ( $_POST['LahaImport']['p_d_kas_n'] != 0 ) {
						$cekBalance[] = [
							'grup' => 'Penjualan',
							'coa'  => $kas_cabang->account_code,
							'd'    => $_POST['LahaImport']['p_d_kas_n'],
							'k'    => 0
						];
					}
				}
				if ( $_POST['LahaImport']['p_d_piutangcard_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Penjualan',
						'coa'  => COA_PIUTANG_CARD,
						'd'    => $_POST['LahaImport']['p_d_piutangcard_n'],
						'k'    => 0
					];
				}
				if ( $_POST['LahaImport']['p_d_pot_jual_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Penjualan',
						'coa'  => COA_SALES_DISK,
						'd'    => $_POST['LahaImport']['p_d_pot_jual_n'],
						'k'    => 0
					];
				}
				if ( $_POST['LahaImport']['p_k_obat_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Penjualan',
						'coa'  => COA_SALES_OBAT,
						'd'    => 0,
						'k'    => $_POST['LahaImport']['p_k_obat_n']
					];
				}
				if ( $_POST['LahaImport']['p_k_ppn_obat_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Penjualan',
						'coa'  => COA_SALES_OBAT_PPN,
						'd'    => 0,
						'k'    => $_POST['LahaImport']['p_k_ppn_obat_n']
					];
				}
				if ( $_POST['LahaImport']['p_k_apotik_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Penjualan',
						'coa'  => COA_SALES_APOTIK,
						'd'    => 0,
						'k'    => $_POST['LahaImport']['p_k_apotik_n']
					];
				}
				if ( $_POST['LahaImport']['p_k_ppn_apotik_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Penjualan',
						'coa'  => COA_SALES_APOTIK_PPN,
						'd'    => 0,
						'k'    => $_POST['LahaImport']['p_k_ppn_apotik_n']
					];
				}
				if ( $_POST['LahaImport']['p_k_jasa_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Penjualan',
						'coa'  => COA_SALES_JASA,
						'd'    => 0,
						'k'    => $_POST['LahaImport']['p_k_jasa_n']
					];
				}
				if ( $_POST['LahaImport']['p_k_lain_lain_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Penjualan',
						'coa'  => COA_PENDAPATAN_LAIN,
						'd'    => 0,
						'k'    => $_POST['LahaImport']['p_k_lain_lain_n']
					];
				}
				if ( $_POST['LahaImport']['p_k_pendmuka_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Penjualan',
						'coa'  => COA_PENDAPATAN_DIMUKA,
						'd'    => 0,
						'k'    => $_POST['LahaImport']['p_k_pendmuka_n']
					];
				}
				if ( $_POST['LahaImport']['p_k_round_n'] < 0 ) {
					$cekBalance[] = [
						'grup' => 'Penjualan',
						'coa'  => COA_ROUNDING,
						'd'    => abs( $_POST['LahaImport']['p_k_round_n'] ),
						'k'    => 0
					];
				} elseif ( $_POST['LahaImport']['p_k_round_n'] > 0 ) {
					$cekBalance[] = [
						'grup' => 'Penjualan',
						'coa'  => COA_ROUNDING,
						'd'    => 0,
						'k'    => abs( $_POST['LahaImport']['p_k_round_n'] )
					];
				}
				foreach ( $bankfees as $rows ) {
					unset( $_POST['LahaImportBankFee'] );
					foreach ( $rows as $k => $v ) {
						if ( is_angka( $v ) ) {
							$v = get_number( $v );
						}
						$_POST['LahaImportBankFee'][ $k ] = $v;
					}
					$bank_fee = Bank::model()->findByPk( $_POST['LahaImportBankFee']['bank_id'] );
					if ( $bank_fee != null ) {
						if ( $_POST['LahaImportBankFee']['bank'] != 0 ) {
							$cekBalance[] = [
								'grup' => 'Fee Card',
								'coa'  => $bank_fee->account_code,
								'd'    => $_POST['LahaImportBankFee']['bank'],
								'k'    => 0
							];
						}
					}
				}
				if ( $_POST['LahaImport']['piu_d_fee_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Fee Card',
						'coa'  => COA_PIUTANG_CARD,
						'd'    => $_POST['LahaImport']['piu_d_fee_n'],
						'k'    => 0
					];
				}
				if ( $_POST['LahaImport']['p_d_piutangcard_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Fee Card',
						'coa'  => COA_PIUTANG_CARD,
						'd'    => 0,
						'k'    => $_POST['LahaImport']['p_d_piutangcard_n']
					];
				}
				foreach ( $biayas as $row1 ) {
					unset( $_POST['LahaImportBiaya'] );
					foreach ( $row1 as $k => $v ) {
						if ( is_angka( $v ) ) {
							$v = get_number( $v );
						}
//                        if ($k == 'store_') $k = 'store';
						$_POST['LahaImportBiaya'][ $k ] = $v;
					}
					if ( $_POST['LahaImportBiaya']['amount'] != 0 ) {
						$cekBalance[] = [
							'grup' => 'Biaya',
							'coa'  => $_POST['LahaImportBiaya']['account_code'],
							'd'    => $_POST['LahaImportBiaya']['amount'],
							'k'    => 0
						];
					}
				}
				if ( $kas_cabang != null ) {
					if ( $_POST['LahaImport']['b_k_kas_n'] != 0 ) {
						$cekBalance[] = [
							'grup' => 'Biaya',
							'coa'  => $kas_cabang->account_code,
							'd'    => 0,
							'k'    => $_POST['LahaImport']['b_k_kas_n']
						];
					}
				}
				if ( $_POST['LahaImport']['b_k_pph21_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Biaya',
						'coa'  => COA_HUTANG_PPH21,
						'd'    => 0,
						'k'    => $_POST['LahaImport']['b_k_pph21_n']
					];
				}
				if ( $_POST['LahaImport']['b_k_pph22_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Biaya',
						'coa'  => COA_HUTANG_PPH22,
						'd'    => 0,
						'k'    => $_POST['LahaImport']['b_k_pph22_n']
					];
				}
				if ( $_POST['LahaImport']['b_k_pp23_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Biaya',
						'coa'  => COA_HUTANG_PPH23,
						'd'    => 0,
						'k'    => $_POST['LahaImport']['b_k_pp23_n']
					];
				}
				if ( $_POST['LahaImport']['b_k_pph4_2_n'] != 0 ) {
					$cekBalance[] = [
						'grup' => 'Biaya',
						'coa'  => COA_HUTANG_PPH42,
						'd'    => 0,
						'k'    => $_POST['LahaImport']['b_k_pph4_2_n']
					];
				}
				if ( $_POST['LahaImport']['b_k_round_n'] < 0 ) {
					$cekBalance[] = [
						'grup' => 'Biaya',
						'coa'  => COA_ROUNDING,
						'd'    => abs( $_POST['LahaImport']['b_k_round_n'] ),
						'k'    => 0
					];
				} elseif ( $_POST['LahaImport']['b_k_round_n'] > 0 ) {
					$cekBalance[] = [
						'grup' => 'Biaya',
						'coa'  => COA_ROUNDING,
						'd'    => 0,
						'k'    => abs( $_POST['LahaImport']['b_k_round_n'] )
					];
				}
				foreach ( $setors as $rows2 ) {
					unset( $_POST['LahaImportBankFee'] );
					foreach ( $rows2 as $k => $v ) {
						if ( is_angka( $v ) ) {
							$v = get_number( $v );
						}
						$_POST['LahaImportBankFee'][ $k ] = $v;
					}
					$bank_fee = Bank::model()->findByPk( $_POST['LahaImportBankFee']['bank_id'] );
					if ( $bank_fee != null ) {
						if ( $_POST['LahaImportBankFee']['bank'] != 0 ) {
							$cekBalance[] = [
								'grup' => 'Setoran',
								'coa'  => $bank_fee->account_code,
								'd'    => $_POST['LahaImportBankFee']['bank'],
								'k'    => 0
							];
						}
						if ( $kas_cabang != null ) {
							if ( $_POST['LahaImportBankFee']['bank'] != 0 ) {
								$cekBalance[] = [
									'grup' => 'Setoran',
									'coa'  => $kas_cabang->account_code,
									'd'    => 0,
									'k'    => $_POST['LahaImportBankFee']['bank']
								];
							}
						}
					}
				}
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex->getMessage();
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => [
					'total'   => count( $cekBalance ),
					'results' => $cekBalance
				]
			) );
			Yii::app()->end();
		}
	}
	public function actionUpdate( $id ) {
		$model = $this->loadModel( $id, 'LahaImport' );
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			foreach ( $_POST as $k => $v ) {
				if ( is_angka( $v ) ) {
					$v = get_number( $v );
				}
				$_POST['LahaImport'][ $k ] = $v;
			}
			$msg               = "Data gagal disimpan";
			$model->attributes = $_POST['LahaImport'];
			if ( $model->save() ) {
				$status = true;
				$msg    = "Data berhasil di simpan dengan id " . $model->laha_import_id;
			} else {
				$msg    .= " " . CHtml::errorSummary( $model );
				$status = false;
			}
			if ( Yii::app()->request->isAjaxRequest ) {
				echo CJSON::encode( array(
					'success' => $status,
					'msg'     => $msg
				) );
				Yii::app()->end();
			} else {
				$this->redirect( array( 'view', 'id' => $model->laha_import_id ) );
			}
		}
	}
	public function actionDelete( $id ) {
		if ( Yii::app()->request->isPostRequest ) {
			$msg    = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel( $id, 'LahaImport' )->delete();
			} catch ( Exception $ex ) {
				$status = false;
				$msg    = $ex;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		} else {
			throw new CHttpException( 400,
				Yii::t( 'app', 'Invalid request. Please do not repeat this request again.' ) );
		}
	}
	public function actionIndex() {
		if ( isset( $_POST['limit'] ) ) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if ( isset( $_POST['start'] ) ) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$param    = array();
		$criteria = new CDbCriteria();
		if ( ( isset ( $_POST['mode'] ) && $_POST['mode'] == 'grid' ) ||
		     ( isset( $_POST['limit'] ) && isset( $_POST['start'] ) )
		) {
			$criteria->limit  = $limit;
			$criteria->offset = $start;
		}
		if ( isset( $_POST['store'] ) ) {
			$criteria->addCondition( 'store like :store' );
			$param[':store'] = '%' . $_POST['store'] . '%';
		}
		if ( isset( $_POST['tgl'] ) ) {
			$tgl = substr( $_POST['tgl'], 0, 10 );
			$criteria->addCondition( 'tgl = date(:tgl)' );
			$param[':tgl'] = $tgl;
		}
		if ( isset( $_POST['p'] ) && $_POST['p'] != 'all' ) {
			$criteria->addCondition( 'p = :p' );
			$param[':p'] = $_POST['p'];
		}
		$criteria->order  = 'doc_ref';
		$criteria->params = $param;
		$model            = LahaImport::model()->findAll( $criteria );
		$total            = LahaImport::model()->count( $criteria );
		$this->renderJson( $model, $total );
	}
}