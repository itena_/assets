<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
Yii::import('application.components.GL');
Yii::import('application.components.PrintCashOut');
class KasController extends GxController
{
    /*
     * CASHIN atau KAS/BANK MASUK
     * ==========================
     * Fields:
     * 'amount' : nominal uang masuk
     * 'total' : dulu sebagai field 'amount', sekarang disamakan dengan 'amount'
     * 'total_debit' : total detail debit + 'amount'
     * 'total_kredit' : total detail kredit
     *
     * -----------------+-------
     * Field            | +/-
     * -----------------|-------
     * amount           | +
     * total            | +
     * total_debit      | +
     * total_kredit     | -
     * -----------------|-------
     * detail debit     | +
     * detail kredit    | -
     *
     * CASHOUT atau KAS/BANK KELUAR
     * ============================
     * Fields:
     * 'amount' : nominal uang keluar
     * 'total' : dulu sebagai field 'amount', sekarang disamakan dengan 'amount'
     * 'total_debit' : total detail debit
     * 'total_kredit' : total detail kredit + 'amount'
     *
     * -----------------+-------
     * Field            | +/-
     * -----------------|-------
     * amount           | -
     * total            | -
     * total_debit      | +
     * total_kredit     | -
     * -----------------|-------
     * detail debit     | +
     * detail kredit    | -
     *
     * catatan :
     * yang dulu (sebelum dirubah),
     * yaitu :
     * - KAS Masuk  -> total (debit)  (+)
     *              -> detail (kredit) (+)
     *
     * - KAS Keluar -> total (kredit) (-)
     *              -> detail (debit)  (-)
     *
     */
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
//            if ($_POST['bank_id'] == SysPrefs::get_val('kas_cabang') &&
//                Tender::is_exist($_POST['tgl'])
//            )
            if (Tender::is_exist($_POST['tgl'])) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Tender Declaration already created'
                ));
                Yii::app()->end();
            }
            $is_new = $_POST['mode'] == 0;
            $is_in = $_POST['arus'] == 1;
            $msg = "Data gagal disimpan.";
            $print = "";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $is_new ? new Kas : $this->loadModel($_POST['id'], 'Kas');
                if (!$is_new && ($model == null)) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Kas')) . "Fatal error, record not found.");
                }
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference($is_in ? CASHIN : CASHOUT);
                } else {
                    $docref = $model->doc_ref;
                    KasDetail::delete__($model->kas_id);
                    $type = $model->arus == 1 ? CASHIN : CASHOUT;
                    $type_no = $model->kas_id;
                    $this->delete_bank_trans($type, $type_no);
                    $this->delete_gl_trans($type, $type_no);
                }
                $_POST['Kas']['all_store'] = 0;
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') {
                        continue;
                    }
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['Kas'][$k] = $v;
                }
                $_POST['Kas']['doc_ref'] = $docref;
                $_POST['Kas']['total_debit'] = $_POST['Kas']['total_debit'];
                $_POST['Kas']['total_kredit'] = -$_POST['Kas']['total_kredit'];
                $_POST['Kas']['amount'] = $is_in ? $_POST['Kas']['amount'] : -$_POST['Kas']['amount'];
                $_POST['Kas']['total'] = $_POST['Kas']['amount'];
                if ($_POST['Kas']['all_store'] == 1) {
                    $_POST['Kas']['jml_cabang'] = Store::countStoreBebanAcc();
                    if ($_POST['Kas']['jml_cabang'] == 0) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Kas')) . " Divide by Zero.");
                    }
                }
                $_POST['Kas']['up'] = 0;
                $model->attributes = $_POST['Kas'];
                /*
                 * cek ketersediaan dana
                 */
//                $balance = BankTrans::get_balance($_POST['bank_id'], $_POST['tgl']);
//                if ($_POST['arus'] == -1 )&& ($balance - abs($_POST['Kas']['amount'])) < 0) {
//                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Cash')) .
//                        "Kurang dana. Tersedia " . number_format($balance, 2) . ' diperlukan ' . number_format($_POST['Kas']['amount'], 2) .
//                        "\nTotal Kekurangan " . number_format($balance - $_POST['Kas']['amount'], 2)
//                    );
//                }
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Cash')) . CHtml::errorSummary($model));
                }/* else{
                    U::runCommand('kas', '--id=' . $model->kas_id, 'kas_'.$model->kas_id.'.log'); 
                } */
                $gl = new GL();
                /*
                 * Bank Amount
                 */
                $gl->add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref, $model->bank->account_code,
                    $model->keperluan, $is_in ? "Cash In" : "Cash Out", $model->amount, 0,
                    $model->store);
                $gl->add_bank_trans($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->bank_id, $docref, $model->tgl,
                    $model->amount, $model->user_id, $model->store);
                /*
                 * detail
                 */
                $store_acc = Store::getStoreBebanAcc();
                $total_nominal = 0;
                $current_branch = '';
                foreach ($detils as $detil) {
                    $total = $detil['debit'] > 0 ? $detil['debit'] : -$detil['kredit'];
                    $kas_detail = new KasDetail;
                    $_POST['KasDetail']['kas_id'] = $model->kas_id;
                    $_POST['KasDetail']['item_name'] = $detil['item_name'];
                    $_POST['KasDetail']['total'] = $total;
                    $_POST['KasDetail']['account_code'] = $detil['account_code'];
                    $_POST['KasDetail']['store'] = $detil['store'];
                    $kas_detail->attributes = $_POST['KasDetail'];
                    if (!$kas_detail->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Detail Cash')) . CHtml::errorSummary($kas_detail));
                    }
                    if ($model->all_store == 1) {
                        $nominal_item = round($kas_detail->total / $model->jml_cabang, 2);
                        foreach ($store_acc as $key => $row) {
                            $current_branch = $row['store_kode'];
                            $gl->add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref,
                                $kas_detail->account_code, $kas_detail->item_name, "",
                                $nominal_item, 1, $current_branch);
                            $total_nominal += $nominal_item;
                        }
                    } else {
                        $gl->add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref,
                            $kas_detail->account_code, $kas_detail->item_name, '',
                            $kas_detail->total, 1, $kas_detail->store);
                    }
                }
                if ($model->all_store == 1) {
                    $total = abs($is_in ? $model->total_debit : $model->total_kredit) - abs($model->amount);
                    $selisih = $total - abs($total_nominal);
                    if ($selisih != 0.00) {
                        $gl->add_gl($is_in ? CASHIN : CASHOUT, $model->kas_id, $model->tgl, $docref,
                            COA_ROUNDING, 'Pembulatan sisa bagi beban all cabang', '',
                            ($is_in ? $selisih : -$selisih), 1, $current_branch);
                    }
                }
                $gl->validate();
                if ($is_new) {
                    $ref->save($is_in ? CASHIN : CASHOUT, $model->kas_id, $docref);
                }
                $msg = t('save.success', 'app');
                $transaction->commit();
                $status = true;
                $prt = new PrintCashOut($model);
                $print = $prt->buildTxt();
                if (PUSH_PUSAT) {

                    U::runCommand('checkdata', 'gltrans', '--tno=' . $model->kas_id, 'kas_gl_tno' . $model->kas_id . '.log');
                    U::runCommand('checkdata', 'banktrans', '--tno=' . $model->kas_id, 'kas_bt_tno' . $model->kas_id . '.log');
                    U::runCommand('soap', 'kas', '--id=' . $model->kas_id, 'kas_' . $model->kas_id . '.log');
                    U::runCommand('checkdata', 'ref', '--tno=' . $model->kas_id, 'kas_ref_tno' . $model->kas_id . '.log');
                }
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg//,
                //'print' => $print
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Kas');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['Kas'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Kas'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->kas_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->kas_id));
            }
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        if (isset ($_POST['arus'])) {
//            $param = array(':tgl' => $_POST['tgl']);
            $criteria->select = "
                    kas_id,
                    doc_ref,
                    no_kwitansi,
                    keperluan,
                    IF(total>=0,total,-total) total,
                    bank_id,tgl,user_id,tdate,type_,store,arus,
                    IF(total_debit>=0,total_debit,-total_debit) total_debit,
                    IF(total_kredit>=0,total_kredit,-total_kredit) total_kredit,
                    IF(amount>=0,amount,-amount) amount
                ";
            $criteria->addCondition(($_POST['arus'] == 'masuk') || ($_POST['arus'] == 'masuk_pusat') ? "arus = 1" : "arus = -1");
//            $criteria->addCondition("visible = 1 AND tgl = '" . substr($_POST['tgl'], 0, 10) . "'");
            $criteria->addCondition("visible = 1 AND tgl = :tgl");
//            $criteria->addCondition('visible = 1');
            $criteria->params = array(':tgl' => substr($_POST['tgl'], 0, 10));
//            if (($_POST['arus'] == 'masuk') || ($_POST['arus'] == 'masuk_pusat')) {
//                $criteria->addInCondition('bank_id', explode(',', SysPrefs::get_val('filter_payment')));
//            } else {
//                $criteria->addInCondition('bank_id', explode(',', SysPrefs::get_val('petty_cash')));
//            }
        }
        if ((isset($_POST['doc_ref']))) {
            $criteria->addCondition("doc_ref like '%" . $_POST['doc_ref'] . "%'");
        }
        if ((isset($_POST['tgl']))) {
            $criteria->addCondition("tgl like '%" . $_POST['tgl'] . "%'");
        }
        if ((isset($_POST['no_kwitansi']))) {
            $criteria->addCondition("no_kwitansi like '%" . $_POST['no_kwitansi'] . "%'");
        }
        if ((isset($_POST['keperluan']))) {
            $criteria->addCondition("keperluan like '%" . $_POST['keperluan'] . "%'");
        }
        if ((isset($_POST['total']))) {
            $criteria->addCondition("total like '%" . $_POST['total'] . "%'");
        }
        if ((isset($_POST['store']))) {
            $criteria->addCondition("store like '%" . $_POST['store'] . "%'");
        }
        $model = Kas::model()->findAll($criteria);
        $total = Kas::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionPrint()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $sls = Kas::model()->findByPk($_POST['id']);
            $prt = new PrintCashOut($sls);
            $prt_string = $prt->buildTxt();
            echo CJSON::encode(array(
                'success' => $sls != null,
                'msg' => $prt_string
            ));
            Yii::app()->end();
        }
    }
    public function actionCreateTransfer()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = 'Transfer has been entered.';
            $id = -1;
            $bank_asal = $_POST['bank_act_asal'];
            $bank_tujuan = $_POST['bank_act_tujuan'];
            $trans_date = $_POST['trans_date'];
            $store = $_POST['store'];
            $memo = $_POST['memo'];
            $amount = get_number($_POST['amount']);
            $charge = get_number($_POST['charge']);
            Yii::import('application.components.U');
            Yii::import('application.components.Reference');
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
	            $limit = Bank::get_minimal($bank_asal);
                $balance = BankTrans::get_balance($bank_asal, $trans_date);
                if ($amount < 0) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Bank Transfer')) . "Insufficient funds");
                }
                if (($balance - abs($amount)) < $limit) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Bank Transfer')) . "Insufficient funds");
                }
                $ref = new Reference();
                $docref = $ref->get_next_reference(BANKTRANSFER);
                $bank_model_asal = Bank::model()->findByPk($bank_asal);
                $bank_model_tujuan = Bank::model()->findByPk($bank_tujuan);
                $bank_account_asal = $bank_model_asal->account_code;
                $bank_account_tujuan = $bank_model_tujuan->account_code;
                $command = Yii::app()->db->createCommand("SELECT UUID();");
                $uuid = $command->queryScalar();
                $trans_no = $uuid;
                $user = Yii::app()->user->getId();
                //debet kode bank tujuan - kredit kode bank asal
                $gl = new GL();
                $gl->add_gl(BANKTRANSFER, $trans_no, $trans_date, $docref, $bank_account_tujuan,
                    $memo, $memo, $amount, 0, $store);
                $gl->add_bank_trans(BANKTRANSFER, $trans_no, $bank_model_tujuan->bank_id, $docref, $trans_date,
                    $amount, $user, $store);
                $gl->add_gl(BANKTRANSFER, $trans_no, $trans_date, $docref, $bank_account_asal,
                    $memo, $memo, -$amount, 0, $store);
                $gl->add_bank_trans(BANKTRANSFER, $trans_no, $bank_model_asal->bank_id, $docref, $trans_date,
                    -$amount, $user, $store);
                if ($charge > 0) {
                    $gl->add_gl(BANKTRANSFERCHARGE, $trans_no, $trans_date, $docref, COA_BIAYA_ADM_BANK,
                        $memo, $memo, $charge, 1, $store);
                    $gl->add_gl(BANKTRANSFERCHARGE, $trans_no, $trans_date, $docref, $bank_account_asal,
                        $memo, $memo, -$charge, 0, $store);
                    $gl->add_bank_trans(BANKTRANSFERCHARGE, $trans_no, $bank_model_asal->bank_id, $docref, $trans_date,
                        -$charge, $user, $store);
                }
                $gl->validate();
                $ref->save(BANKTRANSFER, $trans_no, $docref);
                $id = $docref;
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $id,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionDelete()
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $this->loadModel($_POST['id'], 'Kas');
//                KasDetail::model()->deleteAll('kas_id = :kas_id', array(':kas_id' => $model->kas_id));
                $type = $model->arus == 1 ? CASHIN : CASHOUT;
                $type_no = $model->kas_id;
                $this->delete_bank_trans($type, $type_no);
                $this->delete_gl_trans($type, $type_no);
                $model->visible = 0;
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Cash')) . CHtml::errorSummary($model));
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                )
            );
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
}