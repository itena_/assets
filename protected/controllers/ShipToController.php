<?php

class ShipToController extends GxController {

    public function actionCreate() {
        $model = new ShipTo;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['ShipTo'][$k] = $v;
            }
            $model->attributes = $_POST['ShipTo'];
            $msg = "Data gagal disimpan.";

            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->ship_to_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'ShipTo');


        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['ShipTo'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['ShipTo'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->ship_to_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->ship_to_id));
            }
        }
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'ShipTo')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionIndex() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }

        $criteria = new CDbCriteria();
        $param = array();
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }

        if (isset($_POST['nama'])) {
            $criteria->addCondition("nama like :nama");
            $param[':nama'] = "%" . $_POST['nama'] . "%";
        }
        if (isset($_POST['ship_to_company'])) {
            $criteria->addCondition("ship_to_company like :ship_to_company");
            $param[':ship_to_company'] = "%" . $_POST['ship_to_company'] . "%";
        }
        if (isset($_POST['ship_to_address'])) {
            $criteria->addCondition("ship_to_address like :ship_to_address");
            $param[':ship_to_address'] = "%" . $_POST['ship_to_address'] . "%";
        }
        if (isset($_POST['ship_to_city'])) {
            $criteria->addCondition("ship_to_city like :ship_to_city");
            $param[':ship_to_city'] = "%" . $_POST['ship_to_city'] . "%";
        }
        if (isset($_POST['ship_to_country'])) {
            $criteria->addCondition("ship_to_country like :ship_to_country");
            $param[':ship_to_country'] = "%" . $_POST['ship_to_country'] . "%";
        }

        $criteria->params = $param;
        $model = ShipTo::model()->findAll($criteria);
        $total = ShipTo::model()->count($criteria);

        $this->renderJson($model, $total);
    }

}
