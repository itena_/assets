<?php
class PriceController extends GxController
{
    public function actionCreate()
    {
//        $model = new Price;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Price'][$k] = $v;
            }
            $msg = t('save.fail','app');
            $result = Price::save_price($_POST['Price']['barang_id'], $_POST['Price']['gol_id'], $_POST['Price']['value']);
            if ($result > 0) {
                $status = true;
                $msg = t('save.success','app'); // . $model->price_id;
            } else {
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Price'][$k] = $v;
            }
            $msg = t('save.fail','app');
            $result = Price::save_price($_POST['Price']['barang_id'], $_POST['Price']['gol_id'], $_POST['Price']['value']);
            if ($result > 0) {
                $status = true;
                $msg = t('save.success','app');
            } else {
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['kode_barang'])) {
            $criteria->alias = "np";
            $criteria->join = "INNER JOIN nscc_barang nb ON np.barang_id = nb.barang_id";
            $criteria->addCondition("kode_barang like :kode_barang");
            $param[':kode_barang'] = "%" . $_POST['kode_barang'] . "%";
        }
        if (isset($_POST['nama_barang'])) {
            $criteria->alias = "np";
            $criteria->join = "INNER JOIN nscc_barang nb ON np.barang_id = nb.barang_id";
            $criteria->addCondition("nama_barang like :nama_barang");
            $param[':nama_barang'] = "%" . $_POST['nama_barang'] . "%";
        }
        if (isset($_POST['gol_id'])) {
            $criteria->alias = "np";
            $criteria->join = "INNER JOIN nscc_gol ng ON np.gol_id = ng.gol_id";
            $criteria->addCondition("nama_gol like :gol_id");
            $param[':gol_id'] = "%" . $_POST['gol_id'] . "%";
        }
        if (isset($_POST['store'])) {
            $criteria->addCondition("store like :store");
            $param[':store'] = "%" . $_POST['store'] . "%";
        }
        if (isset($_POST['value'])) {
            $criteria->addCondition("value like :value");
            $param[':value'] = "%" . $_POST['value'] . "%";
        }
        $criteria->params = $param;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Price::model()->findAll($criteria);
        $total = Price::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionCreateByGrup()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Price'][$k] = $v;
            }
            $msg = t('save.fail','app');
            $result = Price::save_price_by_grup($_POST['Price']['grup_id'], $_POST['Price']['gol_id'], $_POST['Price']['value']);
            if ($result > 0) {
                $status = true;
                $msg = t('save.success','app'); // . $model->price_id;
            } else {
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
}