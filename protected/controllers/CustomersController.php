<?php
ini_set('soap.wsdl_cache_enabled', 0);
ini_set('soap.wsdl_cache_ttl', 0);
Yii::import('application.components.SoapClientYii');
class CustomersController extends GxController
{
    public function actionCreate()
    {
        $model = new Customers;
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $ref = new Reference();
//                $docref = $ref->get_next_reference(CUSTOMER);
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    if ($k == 'photo' || $k == 'thumb' ||
                        $k == 'photo_kanan' || $k == 'thumb_kanan' ||
                        $k == 'photo_kiri' || $k == 'thumb_kiri'
                    ) {
                        if (($k == 'photo' || $k == 'thumb') && !$_POST['photo']) continue;
                        if (($k == 'photo_kanan' || $k == 'thumb_kanan') && !$_POST['photo_kanan']) continue;
                        if (($k == 'photo_kiri' || $k == 'thumb_kiri') && !$_POST['photo_kiri']) continue;
                        $v = gzcompress($v, 9);
                    }
                    $_POST['Customers'][$k] = $v;
                }
//                $_POST['Customers']['customer_id'] = $docref;
                if ($_POST['no_customer'] == '') {
                    $_POST['Customers']['no_customer'] = Customers::generete_no_customer(STOREID);
                    $_POST['Customers']['store'] = STOREID;
                }
                $_POST['Customers']['awal'] = new CDbExpression('NOW()');
                $_POST['Customers']['akhir'] = new CDbExpression('NOW()');
                $model->attributes = $_POST['Customers'];
                $msg = t('save.fail', 'app');
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Sales')) . CHtml::errorSummary($model));
                }
//                $ref->save(CUSTOMER, $model->customer_id, $docref);
                $transaction->commit();
                $msg = t('save.success', 'app') . "<br>Customer No : " . $model->no_customer;
                $status = true;
                if(PUSH_PUSAT){
                    U::runCommand('soap','customer', '--id=' . $model->customer_id,  'customer_'.$model->customer_id.'.log');
                }
                if (PUSH_ALL_CABANG) {
                    U::runCommand('soap','customerall', '--id=' . $model->customer_id,  'customerall_'.$model->customer_id.'.log');
                }
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'Customers' => $model->getAttributes([
                    'customer_id', 'nama_customer', 'no_customer'
                ]),
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Customers');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                if ($k == 'photo' || $k == 'thumb' ||
                    $k == 'photo_kanan' || $k == 'thumb_kanan' ||
                    $k == 'photo_kiri' || $k == 'thumb_kiri'
                ) {
                    if (($k == 'photo' || $k == 'thumb') && !$_POST['photo']) continue;
                    if (($k == 'photo_kanan' || $k == 'thumb_kanan') && !$_POST['photo_kanan']) continue;
                    if (($k == 'photo_kiri' || $k == 'thumb_kiri') && !$_POST['photo_kiri']) continue;
                    $v = gzcompress($v, 9);
                }
                $_POST['Customers'][$k] = $v;
            }
            $msg = t('save.fail', 'app');
            $model->attributes = $_POST['Customers'];
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id', 'app', array('{id}' => $model->customer_id));
                if(PUSH_PUSAT){
                    U::runCommand('soap','customer', '--id=' . $model->customer_id,  'customer_'.$model->customer_id.'.log');
                }
                if (PUSH_ALL_CABANG) {
                    U::runCommand('soap','customerall', '--id=' . $model->customer_id,  'customerall_'.$model->customer_id.'.log');
                }
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->customer_id));
            }
        }
    }
    public function actionSoapcopy()
    {
        try {
            $client = new SoapClient(SOAP_CUSTOMER, array(
                'soap_version' => SOAP_1_1, // or try SOAP_1_1
                'cache_wsdl' => WSDL_CACHE_NONE, // WSDL_CACHE_BOTH in production
                'trace' => 1
            ));
            $result = $client->copyCustomers($_POST['no_base'], STOREID);
//            $result = $client->getPrice('IBM');
            echo CJSON::encode(array(
                'success' => $result['status'],
                'msg' => $result['msg']
            ));
        } catch (SoapFault $e) {
            echo CJSON::encode(array(
                'success' => false,
                'msg' => $e->getMessage()
            ));
        }
        Yii::app()->end();
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $select = "SELECT nc.customer_id,nc.nama_customer,nc.no_customer,nc.tempat_lahir,nc.tgl_lahir,
          nc.email,nc.telp,nc.alamat,nc.kecamatan_id,nc.status_cust_id,nc.sex,nc.kerja,nc.store,nc.kota_id,
          nc.provinsi_id,nc.negara_id,nc.friend_id,nc.info_id FROM nscc_customers AS nc";
        $limit = " LIMIT $start,$limit";
        $where = "";
        $order = " ORDER BY nc.awal";
        $param = array();
        $wheretgl = "";
        if (isset($_POST['customer_id'])) {
            unset($_POST['query']);
        }
        if (isset($_POST['query'])) {
            $param = array(
                ':nama_customer' => "%" . $_POST['query'] . "%",
//                ':alamat' => "%" . $_POST['query'] . "%",
                ':no_customer' => "%" . $_POST['query'] . "%",
//                ':telp' => "%" . $_POST['query'] . "%"
            );
	        if (NATASHA_CUSTOM)
		        $param = array(
			        ':nama_customer' => "%" . $_POST['query'] . "%",
			        ':no_customer' => $_POST['query'],
		        );

            if (isset($_POST['mode']) && $_POST['mode'] == 'master') {
                $wheretgl = ' AND tgl_lahir = :tgl';
                $param[':tgl'] = $_POST['tgl'];
            }
            $where = " WHERE (nc.nama_customer LIKE :nama_customer OR no_customer LIKE :no_customer) " . $wheretgl;
	        if (NATASHA_CUSTOM)
		        $where = " WHERE (nc.nama_customer LIKE :nama_customer OR no_customer = :no_customer) " . $wheretgl;

            $total_cmd = Yii::app()->db->createCommand($select . $where);
            $total = $total_cmd->query($param)->rowCount;
            $comm = Yii::app()->db->createCommand($select . $where . $order . $limit);
            $row = $comm->queryAll(true, $param);
            $this->renderJsonArrWithTotal($row, $total);
            Yii::app()->end();
        }
        if (isset($_POST['no_customer'])) {
            $where = " WHERE no_customer LIKE :no_customer ";
	        $param[':no_customer'] = "%" . $_POST['no_customer'] . "%";

	        if (NATASHA_CUSTOM) {
		        $where = " WHERE no_customer = :no_customer ";
		        $param[':no_customer'] = $_POST['no_customer'];
	        }
        }
        if (isset($_POST['nama_customer'])) {
            if ($where == "") {
                $where = " WHERE ";
            } else {
                $where .= " AND ";
            }
            $where .= " nama_customer like :nama_customer ";
            $param[':nama_customer'] = "%" . $_POST['nama_customer'] . "%";
        }
        if (isset($_POST['tgl_lahir'])) {
            if ($where == "") {
                $where = " WHERE ";
            } else {
                $where .= " AND ";
            }
            $where .= " tgl_lahir = :tgl_lahir ";
            $param[':tgl_lahir'] = $_POST['tgl_lahir'];
        }
        if (isset($_POST['telp'])) {
            if ($where == "") {
                $where = " WHERE ";
            } else {
                $where .= " AND ";
            }
            $where .= " telp like :telp ";
            $param[':telp'] = "%" . $_POST['telp'] . "%";
        }
        if (isset($_POST['alamat'])) {
            if ($where == "") {
                $where = " WHERE ";
            } else {
                $where .= " AND ";
            }
            $where .= " alamat like :alamat ";
            $param[':alamat'] = "%" . $_POST['alamat'] . "%";
        }
        if (isset($_POST['customer_id'])) {
            if ($where == "") {
                $where = " WHERE ";
            } else {
                $where .= " AND ";
            }
            $where .= " customer_id = :customer_id ";
            $param[':customer_id'] = $_POST['customer_id'];
        }
        $total_cmd = Yii::app()->db->createCommand($select . $where);
        $total = $total_cmd->query($param)->rowCount;
        $comm = Yii::app()->db->createCommand($select . $where . $order . $limit);
//        $comm = Yii::app()->db->createCommand($select . $where . $order);
        $row = $comm->queryAll(true, $param);
        $this->renderJsonArrWithTotal($row, $total);
    }
    public function actionImport()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $stat = StatusCust::model()->find("nama_status = :nama_status", array(':nama_status' => 'REG'));
                $ref = new Reference();
                $docref = $ref->get_next_reference(CUSTOMER);
                $arr_key = array_keys($detils);
                $duplicate_no_cust = array();
                foreach ($detils[$arr_key[0]] as $row) {
                    $arr_key = array_keys($row);
                    $arr_idx = array();
                    foreach ($arr_key as $key) {
                        if (strpos($key, 'NOBASE') !== false) {
                            $arr_idx[0] = $key;
                        }
                        if (strpos($key, 'NAMACUS') !== false) {
                            $arr_idx[1] = $key;
                        }
                        if (strpos($key, 'ALAMAT') !== false) {
                            $arr_idx[2] = $key;
                        }
                        if (strpos($key, 'TELP') !== false) {
                            $arr_idx[3] = $key;
                        }
                        if (strpos($key, 'TGLLH') !== false) {
                            $arr_idx[4] = $key;
                        }
                        if (strpos($key, 'SEX') !== false) {
                            $arr_idx[5] = $key;
                        }
                        if (strpos($key, 'KERJA') !== false) {
                            $arr_idx[6] = $key;
                        }
                        if (strpos($key, 'AWAL') !== false) {
                            $arr_idx[7] = $key;
                        }
                        if (strpos($key, 'AKHIR') !== false) {
                            $arr_idx[8] = $key;
                        }
                    }
                    $cust = Customers::model()->find('no_customer = :no_customer',
                        array(':no_customer' => $row[$arr_key[1]]));
                    if ($cust != null) {
                        $duplicate_no_cust[] = array(
                            $row[$arr_idx[0]],
                            $row[$arr_idx[1]],
                            $row[$arr_idx[2]],
                            $row[$arr_idx[3]],
                            $row[$arr_idx[4]],
                            $row[$arr_idx[5]],
                            $row[$arr_idx[6]],
                            $row[$arr_idx[7]],
                            $row[$arr_idx[8]]
                        );
                        continue;
                    }
                    $model = new Customers;
                    $model->customer_id = $docref;
                    $model->no_customer = $row[$arr_idx[0]];
                    $model->nama_customer = $row[$arr_idx[1]];
                    $model->alamat = $row[$arr_idx[2]];
                    $model->telp = $row[$arr_idx[3]];
                    $tgl_lahir = strtotime($row[$arr_idx[4]]);
                    $model->tgl_lahir = $tgl_lahir ? date('Y-m-d', $tgl_lahir) : null;
                    $model->sex = $row[$arr_idx[5]];
                    $model->kerja = $row[$arr_idx[6]];
                    $model->status_cust_id = $stat->status_cust_id;
                    $awal = strtotime($row[$arr_idx[7]]);
                    $model->awal = $awal ? date('Y-m-d', $awal) : null;
                    $akhir = strtotime($row[$arr_idx[8]]);
                    $model->akhir = $akhir ? date('Y-m-d', $akhir) : null;
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Customers')) . CHtml::errorSummary($model));
                    }
                    $docref = $ref->_customers($docref);
                }
//                $msg = t('save.fail', 'app');
                $ref->save(CUSTOMER, $model->customer_id, $docref);
                $transaction->commit();
                $msg = '<table cellspacing="5" style="border:1px solid #BBBBBB;border-collapse:collapse;padding:5px; font-size: 10pt;">
                        <thead>
                            <tr>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;" >No. Customers</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Customer Name</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Address</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Telp</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">BirthDate</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Sex</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Occuption</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">First</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Last</th>
                            </tr>
                        </thead><tbody >';
                foreach ($duplicate_no_cust as $r) {
                    $msg .= "<tr>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[0]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[1]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[2]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[3]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[4]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[5]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[6]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[7]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[8]</td>
                            </tr>";
                }
                $msg .= "</tbody></table>";
//                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionPrintCard()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $model = $this->loadModel($_POST['customer_id'], 'Customers');
            $tgl = date('Y-m-d');
            $yawal = date('Y', strtotime($tgl));
            $mawal = date('m', strtotime($tgl));
            $yakhir = date('y', strtotime(date("Y-m-d", strtotime($tgl)) . " + 5 year"));
            $type = "EC";
            if ($model != null) {
                $model->cardtype = $type;
                $model->validcard = date('Y-m-d', strtotime(date("Y-m-d", strtotime($model->awal)) . " + 5 year"));
                $model->printcarddate = date("Y-m-d");
                $model->locprintcard = STOREID;
                $model->save();
                echo CJSON::encode(array(
                    'success' => true,
                    'Customers' => $model->getAttributes([
                        'customer_id', 'nama_customer', 'no_customer'
                    ]),
                    'yawal' => $yawal,
                    'yakhir' => $yakhir,
                    'mawal' => $mawal,
                    'msg' => 'Saved successfully'
                ));
            } else {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Customer not valid'
                ));
            }
            Yii::app()->end();
        }
    }
    public function actionSaveLogCard()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $model = $this->loadModel($_POST['customer_id'], 'Customers');
            if ($model != null) {
                $model->cardtype = $_POST['cardtype'];
                $model->validcard = $_POST['validcard'];
                $model->printcarddate = $_POST['printcarddate'];
                $model->locprintcard = $_POST['locprintcard'];
                $model->save();
                echo CJSON::encode(array(
                    'success' => true,
                    'msg' => 'Saved successfully'
                ));
            } else {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Customer not valid'
                ));
            }
            Yii::app()->end();
        }
    }
    public function actionCreateLog()
    {
        $cust = Customers::get_backup();
        echo CJSON::encode(array(
            'url' => SysPrefs::get_val('url_nars'),
            'username' => SysPrefs::get_val('username_nars'),
            'password' => SysPrefs::get_val('password_nars'),
            'cabang' => STOREID,
            'tanggal' => date("Y-m-d"),
            'jenis' => 'pasien',
            'data' => CJSON::encode($cust)
        ));
    }
    public function actionFind()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('no_customer = :customer');
        $param[':customer'] = $_POST['customer'];
        $criteria->params = $param;
        $model = Customers::model()->find($criteria);
        echo CJSON::encode($model);
    }
    public function actionTestHistory($nobase)
    {
        if (isset($_POST) && !empty($_POST)) {
            if (isset($_POST['nobase'])) {
                $nobase = $_POST['nobase'];
            }
        }
        $fakturs = Pasien::get_detail_pasien($nobase);
        $items = $fakturs;
        $fak = $comm = $note = $tgl = $loc = null;
        $newarr = array();
        foreach ($fakturs as $detil) {
            if ($detil['no_faktur'] != $fak) {
                $fak = $detil['no_faktur'];
                $tgl = $detil['tgl'];
                $comm = $detil['ketdisc'];
                $loc = $detil['nama'];
                $item = array();
                foreach ($items as $key1 => $row) {
                    if ($row['no_faktur'] == $fak) {
                        $item[] = array(
                            'kd_brng' => $row['kd_brng'],
                            'qty' => $row['qty'],
                            'satuan' => $row['satuan'],
                            'kode' => $row['kode'],
                            'nama' => $row['nama'],
                            'note' => $row['ketpot']
                        );
                    }
                }
                $newarr[] = array('no_faktur' => $fak, 'tgl' => $tgl, 'comment' => $comm, 'loc' => $loc, 'item' => $item);
            }
        }
        $this->render('GetHistory', array('pasien' => $newarr, 'scale' => 0.8));
    }
    public function actionSimpleHistory($nobase)
    {
        $height = '100%';
        if (isset($_POST) && !empty($_POST)) {
            if (isset($_POST['nobase'])) {
                $nobase = $_POST['nobase'];
            }
            if (isset($_POST['height'])) {
                $height = $_POST['height'];
            }
        }
        /*
         *  kalau isset local; request history local 
         */
        $local = isset($_POST['local']);
        $fakturs = $local ? Pasien::get_detail_pasien_local($_POST['customer_id']) : Pasien::get_detail_pasien($nobase);
//        $fakturs = Pasien::get_detail_pasien($nobase);
        if(isset($_POST['customer_id'])){
            $diags = $local ? Pasien::get_diagnosa_pasien_local($_POST['customer_id']) : Pasien::get_diagnosa_pasien($_POST['customer_id']);
            $arr1 = array_merge($fakturs, $diags);
            $arr = $this->aasort($arr1, 'tgli');
        } else {
            $arr = $fakturs;
        }
        $this->render('SimpleHistory', array('pasien' => $arr, 'height' => $height));
    }
    public function aasort($array, $key) {
        $sorter=array();
        $ret=array();
        reset($array);
        foreach ($array as $ii => $va) {
            $sorter[$ii]=$va[$key];
        }
        arsort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii]=$array[$ii];
        }
        return $ret;
    }
    
    public function actionGetHistory($nobase) 
    {
        if (isset($_POST) && !empty($_POST)) {
            if (isset($_POST['nobase'])) {
                $nobase = $_POST['nobase'];
            }
        }
        $pas = Pasien::get_detail_pasien($nobase);
        $counter = 0;
        $fak = '';
//        $index = -1;
        $newarr = $arrspan = array();
        foreach ($pas as $key => $detil) {
            if ($fak != $detil['no_faktur']) {
                $fak = $detil['no_faktur'];
//                $index = $key;
                if (count($arrspan) > 0) {
                    $arrspan[count($arrspan) - 1]['attr'] =
                        array('no_faktur' => array('rowspan' => "$counter"));
                }
                $arrspan[] = array(
                    'index' => $key
                );
                $counter = 0;
            }
            $arr = array(
                'no_faktur' => $detil['no_faktur'],
                'tgl' => $detil['tgl'],
                'kd_brng' => $detil['kd_brng'],
                'qty' => $detil['qty'],
                'satuan' => $detil['satuan'],
                'nama' => $detil['nama'],
                'kode' => $detil['kode'],
                'attr' => array('no_faktur' => array('display' => "none"))
            );
            $counter++;
            $newarr[] = $arr;
        }
        if (count($arrspan) > 0 && $counter > 1) {
            $arrspan[count($arrspan) - 1]['attr'] =
                array('no_faktur' => array('rowspan' => "$counter"));
        }
        foreach ($arrspan as $row) {
            $newarr[$row['index']]['attr'] = $row['attr'];
        }
        Yii::app()->end(json_encode($newarr));
//        $row = array('rows' => $newpas);
//        echo json_encode($row);
//        echo CJSON::encode(array(
//            'success' => false,
//            'msg' => 'Customer not valid'
//        ));
//        var_dump($pas);
//        if (!Yii::app()->request->isAjaxRequest) {
//            return;
//        }
//        if (isset($_POST) && !empty($_POST)) {
//            $pas = Pasien::get_detail_pasien($_POST['nobase']);
//
//        }
    }
    public function actionByNoPasien()
    {
        if (isset($_POST) && !empty($_POST)) {
            if (isset($_POST['no_customer'])) {
                $nobase = $_POST['no_customer'];
                $criteria = new CDbCriteria();
                $criteria->addCondition('no_customer = :no_customer');
                $criteria->params = array(':no_customer' => $nobase);
                $model = Customers::model()->find($criteria);
                $yearnow = date('Y');
                if ($model == null) {
                    $jsonresult = '{"total":"0","data":' . json_encode(null) . '}';
                } else {
//                    $total = count($model);
                    $arr = $model->getAttributes([
                        'nama_customer', 'no_customer', 'telp', 'alamat', 'tgl_lahir', 'kerja'
                    ]);
                    $age['age'] = abs($yearnow - substr($arr['tgl_lahir'],0,4));
//                    $jsonresult = '{"total":"1","data":' . (count($arr) > 0 ? json_encode($arr) :
//                            '{"nama_customer":"","no_customer":"","telp":null,"alamat":""}') . '}';
                    $jsonresult = '{"total":"1","data":' . json_encode($arr+$age) . '}';
                }
                Yii::app()->end($jsonresult);
            }
        }
    }
    public function actionGetPhoto()
    {
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = '';
            /** @var Customers $cust */
            $cust = Customers::model()->findByPk($_POST['id']);
            if ($cust == null) {
                $status = false;
                $msg = 'Customer tidak ditemukan';
            } else {
                $status = true;
                $tmp = false;
                if ($_POST['mode'] == 'thumb') {
                    $tmp['thumb_kanan'] = @gzuncompress($cust->thumb_kanan);
                    $tmp['thumb_kiri'] = @gzuncompress($cust->thumb_kiri);
                    $tmp['thumb'] = @gzuncompress($cust->thumb);
                } elseif ($_POST['mode'] == 'photo') {
                    $tmp['photo_kanan'] = @gzuncompress($cust->photo_kanan);
                    $tmp['photo_kiri'] = @gzuncompress($cust->photo_kiri);
                    $tmp['photo'] = @gzuncompress($cust->photo);
                }
                $tmp === false ? $msg = '' : $msg = json_encode($tmp);
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
//    public function actionUploadHistoryManual()
//    {
//        if (isset($_POST) && !empty($_POST)) {
//            $msg = "Proses kirim ulang gagal.";
//
//            try
//            {
//                $from = $_POST['tglfrom'];
//                $to = $_POST['tglto'];
//
//
//                $msg = "History tgl <b>$from - $to</b> telah diupload..";
//                $status = true;
//
//                U::runHistory('uploadnars', 'UploadHistory', '--from=' . $from, '--to=' . $to, "$from-$to history.log");
//
//            }
//            catch (Exception $ex) {
//                $status = false;
//                $msg = $ex->getMessage();
//            }
//            echo CJSON::encode(array(
//                'success' => $status,
//                'id' => '',
//                'msg' => $msg
//            ));
//            Yii::app()->end();
//        }
//    }
}