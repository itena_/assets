<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
/*
 * GL TIDAK DIPAKAI !!!!
 * 
Yii::import('application.components.GL');
 */
class TransferItemController extends GxController
{
    /*
     * Invoice
     * type_ = 0
     */
    public function actionCreateIn()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $transfer_item_id = "";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $is_new = $_POST['mode'] == 0;
                $model = $is_new ? new TransferItem : $this->loadModel($_POST['transfer_item_id'], 'TransferItem');
                if (!$is_new && ($model == null)) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'TransferItem')) . "Fatal error, record not found.");
                }
                
                $tb = TerimaBarang::model()->findByPk($_POST['terima_barang_id']);
                if ($tb == null) {
                    throw new Exception("Data Penerimaan Barang tidak ditemukan!");
                }
                
                $type_po = $tb->po->type_;
                
                if ($is_new) {
                    if ($tb->status == TB_INVOICED) {
                        throw new Exception("Data invoice untuk Penerimaan Barang ini telah diinput.");
                    }
                    $ref = new Reference();
                    $docref = $ref->get_next_reference($type_po==1?SUPPIN:SUPPOUT);
                } else {
                    if ($model->status == STATUS_CLOSE) {
                        throw new Exception("Edit Invoice tidak bisa dilakukan <br />status invoice CLOSED.");
                    }
                    $docref = $model->doc_ref;
                    TransferItemDetails::model()->updateAll(array('visible' => 0), 'transfer_item_id = :transfer_item_id',
                        array(':transfer_item_id' => $model->transfer_item_id));
                }
                
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['TransferItem'][$k] = $v;
                }
                
                $_POST['TransferItem']['doc_ref'] = $docref;
                $_POST['TransferItem']['user_id'] = app()->user->getId();
                $_POST['TransferItem']['type_'] = $type_po == 1? 0 : 1;
                $model->attributes = $_POST['TransferItem'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item')) . CHtml::errorSummary($model));

                foreach ($detils as $detil) {
                    $item_details = new TransferItemDetails;
                    $_POST['TransferItemDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['TransferItemDetails']['charge'] = $detil['charge'];
                    $_POST['TransferItemDetails']['qty'] = $type_po * get_number($detil['qty']);
                    $_POST['TransferItemDetails']['price'] = get_number($detil['price']);
                    $_POST['TransferItemDetails']['sub_total'] = $type_po * get_number($detil['sub_total']);
                    $_POST['TransferItemDetails']['disc'] = get_number($detil['disc']);
                    $_POST['TransferItemDetails']['disc_rp'] = $type_po * get_number($detil['disc_rp']);
                    $_POST['TransferItemDetails']['total_disc'] = $type_po * get_number($detil['total_disc']);
                    $_POST['TransferItemDetails']['total_dpp'] = $type_po * get_number($detil['total_dpp']);
                    $_POST['TransferItemDetails']['ppn'] = get_number($detil['ppn']);
                    $_POST['TransferItemDetails']['ppn_rp'] = $type_po * get_number($detil['ppn_rp']);
                    $_POST['TransferItemDetails']['pph_id'] = get_number($detil['pph_id']);
                    $_POST['TransferItemDetails']['pph'] = get_number($detil['pph']);
                    $_POST['TransferItemDetails']['pph_rp'] = $type_po * get_number($detil['pph_rp']);
                    $_POST['TransferItemDetails']['total'] = $type_po * get_number($detil['total']);
                    $_POST['TransferItemDetails']['item_id'] = $detil['item_id'];

                    $_POST['TransferItemDetails']['transfer_item_id'] = $model->transfer_item_id;
                    $item_details->attributes = $_POST['TransferItemDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    
                    /*
                     * set Harga Rata-rata
                     */
                    if ($item_details->barang->grup->kategori->is_have_stock()) {
                        $tbDetail = TerimaBarangDetails::model()->findByAttributes(array(
                            'terima_barang_id' => $tb->terima_barang_id,
                            'barang_id' => $detil['barang_id'],
                            'item_id' => $detil['item_id']
                        ));
                        $total = round($item_details->total_dpp, 2);
                        $item_details->barang->count_hpp($tbDetail->stock_before, $item_details->barang->get_cost($model->store), $item_details->qty, $total, $model->store);

                        switch ($item_details->barang->tipe_barang_id){
                            case TIPE_FINISH_GOODS:
                            case TIPE_RAW_MATERIAL:
                                StockMoves::model()->updateByPk($tbDetail->stock_moves_id, array('price' => $item_details->barang->get_cost($model->store))); break;
                            case TIPE_PERLENGKAPAN:
                                StockMovesPerlengkapan::model()->updateByPk($tbDetail->stock_moves_id, array('price' => $item_details->barang->get_cost($model->store)));; break;
                        }
                    }
                }
                
                if ($is_new) {
                    $tb->status = TB_INVOICED;
                    if (!$tb->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'TerimaBarang')) .
                            CHtml::errorSummary($tb));
                    
                    
                    $ref->save($type_po==1?SUPPIN:SUPPOUT, $model->transfer_item_id, $docref);
                }

                /*
                 * jika 'Save & Print'
                 * maka respon akan disipkan 'transfer_item_id' sebagai parameter print Invoice
                 */
                if (isset($_POST['print']) && $_POST['print'] == 'true')
                    $transfer_item_id = $model->transfer_item_id;

                        $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg,
                'transfer_item_id' => $transfer_item_id
            ));
            Yii::app()->end();
        }
    }
    /*
     * Return
     * type_ = 1
     */
    public function actionCreateOut()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            $docref = "";
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $is_new = $_POST['mode'] == 0;
                $model = $is_new ? new TransferItem : $this->loadModel($_POST['transfer_item_id'], 'TransferItem');
                if (!$is_new && ($model == null)) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'TransferItem')) . "Fatal error, record not found.");
                }
                
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(SUPPOUT);
                } else {
                    if ($model->status == STATUS_CLOSE) {
                        throw new Exception("Edit Return Supplier Item tidak bisa dilakukan <br />status return CLOSED.");
                    }
                    $docref = $model->doc_ref;
                    TransferItemDetails::model()->updateAll(array('visible' => 0), 'transfer_item_id = :transfer_item_id',
                        array(':transfer_item_id' => $model->transfer_item_id));
                    
                    $this->delete_stock_moves(SUPPOUT, $model->transfer_item_id);
                }
                
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['TransferItem'][$k] = $v;
                }
                
                $_POST['TransferItem']['sub_total'] = 0;
                $_POST['TransferItem']['total_disc_rp'] = 0;
                $_POST['TransferItem']['total_dpp'] = 0;
                $_POST['TransferItem']['ppn_rp'] = 0;
                $_POST['TransferItem']['total_pph_rp'] = 0;
                $_POST['TransferItem']['total'] = 0;
                
                $new_details = array();
                foreach ($detils as $detil) {
                    $qty = -get_number($detil['qty']);
                    $price = get_number($detil['price']);
                    $disc = get_number($detil['disc']);
                    $ppn = get_number($detil['ppn']);
                    $pph = get_number($detil['pph']);
                    
                    $sub_total = round($qty * $price,2);
                    $disc_rp = round($sub_total * ($disc / 100),2);
                    $total_disc = $disc_rp;
                    $total_dpp = $sub_total - $total_disc;
                    $ppn_rp = round($total_dpp * ($ppn / 100),2);
                    $pph_rp = round($total_dpp * ($pph / 100),2);
                    $total = $total_dpp + $ppn_rp - $pph_rp;
                    
                    $detil['qty'] = $qty;
                    $detil['price'] = $price;
                    $detil['sub_total'] = $sub_total;
                    $detil['disc'] = $disc;
                    $detil['disc_rp'] = $disc_rp;
                    $detil['total_disc'] = $total_disc;
                    $detil['total_dpp'] = $total_dpp;
                    $detil['ppn'] = $ppn;
                    $detil['ppn_rp'] = $ppn_rp;
                    $detil['pph'] = $pph;
                    $detil['pph_rp'] = $pph_rp;
                    $detil['total'] = $total;
                    
                    $_POST['TransferItem']['sub_total'] += $sub_total;
                    $_POST['TransferItem']['total_disc_rp'] += $total_disc;
                    $_POST['TransferItem']['total_dpp'] += $total_dpp;
                    $_POST['TransferItem']['ppn_rp'] += $ppn_rp;
                    $_POST['TransferItem']['total_pph_rp'] += $pph_rp;
                    $_POST['TransferItem']['total'] += $total;
                    $new_details[] = $detil;
                    
                    $itemOut = TerimaBarangDetails::model()->find("barang_id = :barang_id AND terima_barang_id = :terima_barang_id", array(":barang_id" => $detil['barang_id'], ":terima_barang_id" => $_POST['terima_barang_id']));
                    if($itemOut->qty < abs($qty)){
                        throw new Exception("Quantity retur lebih besar dari quantity terima.");
                    }
                }
                $_POST['TransferItem']['sub_total'] = $_POST['TransferItem']['sub_total'];
                $_POST['TransferItem']['total_disc_rp'] = $_POST['TransferItem']['total_disc_rp'];
                $_POST['TransferItem']['total_dpp'] = $_POST['TransferItem']['total_dpp'];
                $_POST['TransferItem']['tax_rp'] = $_POST['TransferItem']['ppn_rp'];
                $_POST['TransferItem']['total_pph_rp'] = $_POST['TransferItem']['total_pph_rp'];
                $_POST['TransferItem']['total'] = $_POST['TransferItem']['total'];
                $_POST['TransferItem']['doc_ref'] = $docref;
                $model->attributes = $_POST['TransferItem'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Return item')) . CHtml::errorSummary($model));
                
                foreach ($new_details as $detil) {
                    $item_details = new TransferItemDetails;
                    $_POST['TransferItemDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['TransferItemDetails']['charge'] = $detil['charge'];
                    $_POST['TransferItemDetails']['qty'] = get_number($detil['qty']);
                    $_POST['TransferItemDetails']['price'] = get_number($detil['price']);
                    $_POST['TransferItemDetails']['sub_total'] = get_number($detil['sub_total']);
                    $_POST['TransferItemDetails']['disc'] = get_number($detil['disc']);
                    $_POST['TransferItemDetails']['disc_rp'] = get_number($detil['disc_rp']);
                    $_POST['TransferItemDetails']['total_disc'] = get_number($detil['total_disc']);
                    $_POST['TransferItemDetails']['total_dpp'] = get_number($detil['total_dpp']);
                    $_POST['TransferItemDetails']['ppn'] = get_number($detil['ppn']);
                    $_POST['TransferItemDetails']['ppn_rp'] = get_number($detil['ppn_rp']);
                    $_POST['TransferItemDetails']['pph'] = get_number($detil['pph']);
                    $_POST['TransferItemDetails']['pph_rp'] = get_number($detil['pph_rp']);
                    $_POST['TransferItemDetails']['total'] = get_number($detil['total']);
                    
                    $_POST['TransferItemDetails']['transfer_item_id'] = $model->transfer_item_id;
                    $item_details->attributes = $_POST['TransferItemDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Return item detail')) . CHtml::errorSummary($item_details));
                    
                    if ($item_details->barang->grup->kategori->is_have_stock()) {
                        U::add_stock_moves(SUPPOUT, $model->transfer_item_id, $model->tgl,
                            $item_details->barang_id, $item_details->qty, $model->doc_ref,
                            0, $model->store);
                    }
                }
                if ($is_new) {
                    $ref->save(SUPPOUT, $model->transfer_item_id, $docref);
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    
    public function actionClose()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Data gagal disimpan";
            $model = $this->loadModel($_POST['transfer_item_id'], 'TransferItem');
            $user = Users::model()->findByAttributes(array('id' => User()->getId()));
            $menuTree = new MenuTree($user->security_roles_id);
            
            if ($model->status == STATUS_CLOSE) {
                $msg = "Tidak dapat set LUNAS data Invoice.<br>Nomor invoice <b>" . $model->doc_ref_other . "</b> telah berstatus LUNAS.";
                $status = false;
            } else if (!$menuTree->getState(2051)) {
                $msg = "Anda tidak dapat set LUNAS data Invoice.";
                $status = false;
            } else {
                $model->status = STATUS_CLOSE;
                $model->closed_id_user = User()->getId();
                $model->closed_tdate = new CDbExpression('NOW()');
                if ($model->save()) {
                    $status = true;
                    $msg = "Nomor invoice <b>" . $model->doc_ref_other . "</b> telah LUNAS.";
                } else {
                    $msg .= " " . implode(", ", $model->getErrors());
                    $status = false;
                }
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        
        $criteria = new CDbCriteria();
        $criteria->params = array();
        
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $criteria->params[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['doc_ref'])) {
            $criteria->addCondition('doc_ref LIKE :doc_ref');
            $criteria->params[':doc_ref'] = '%' . $_POST['doc_ref'] . '%';
        }
        if (isset($_POST['doc_ref_other'])) {
            $criteria->addCondition('doc_ref_other LIKE :doc_ref_other');
            $criteria->params[':doc_ref_other'] = '%' . $_POST['doc_ref_other'] . '%';
        }
        if (isset($_POST['note'])) {
            $criteria->addCondition('note LIKE :note');
            $criteria->params[':note'] = '%' . $_POST['note'] . '%';
        }
        if (isset($_POST['doc_ref_po'])) {
            $criteria->addCondition('doc_ref_po LIKE :doc_ref_po');
            $criteria->params[':doc_ref_po'] = '%' . $_POST['doc_ref_po'] . '%';
        }
        if (isset($_POST['no_sj'])) {
            $criteria->addCondition('no_sj LIKE :no_sj');
            $criteria->params[':no_sj'] = '%' . $_POST['no_sj'] . '%';
        }
        if (isset($_POST['store'])) {
            $criteria->addCondition('store LIKE :store');
            $criteria->params[':store'] = '%' . $_POST['store'] . '%';
        }
        if (isset($_POST['type_']) && $_POST['type_'] !== 'all') {
            $criteria->addCondition('type_ LIKE :type_');
            $criteria->params[':type_'] = '%' . $_POST['type_'] . '%';
        }
        if (isset($_POST['status']) && $_POST['status'] !== 'all') {
            /* filter grid berdasar status */
            $criteria->addCondition('status = :status');
            $criteria->params[':status'] = $_POST['status'];
        }
        
        $criteria->order = "tdate DESC";
        $model = TransferItemView::model()->findAll($criteria);
        $total = TransferItemView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndexOut()
    {
        $criteria = new CDbCriteria();
        $criteria->select = "transfer_item_id,tgl,doc_ref,note,tdate,doc_ref_other,
        user_id,type_,store,supplier_id,-total total,disc,-discrp discrp,-bruto bruto,-vat vat";
        $criteria->addCondition("type_ = 1 AND DATE(tgl) = :tgl");
        $criteria->params = array(':tgl' => $_POST['tgl']);
        $model = TransferItem::model()->findAll($criteria);
        $total = TransferItem::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}