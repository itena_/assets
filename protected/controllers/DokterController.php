<?php
class DokterController extends GxController
{
    public function actionCreate()
    {
        $model = new Employees;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $tipe = TipeEmployee::model()->findByAttributes(array('nama_' => 'Doctor'));
                if ($tipe == null) {
                    throw new Exception('Tipe employee untuk dokter tidak ditemukan');
                }
                $_POST['tipe'] = $tipe->tipe_employee_id;
                $_POST['kode_employee'] = $_POST['kode_dokter'];
                $_POST['nama_employee'] = $_POST['nama_dokter'];
                $_POST['store'] = STOREID;
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Dokter'][$k] = $v;
                }
                $model->attributes = $_POST['Dokter'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Beauty')) . CHtml::errorSummary($model));
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Dokter');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Dokter'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Dokter'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->dokter_id;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->dokter_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['f']) && $_POST['f'] == 'cmp') {
            $criteria->addCondition("store = :store");
            $param[':store'] = STOREID;
            $criteria->params = $param;
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        } else {
            if (isset ($_POST['mode']) && $_POST['mode'] != 'lib' && $_POST['mode'] != 'grid') {
                $criteria->addCondition("active = 1");
            }
        }
        if ((isset($_POST['active']))) {
            $criteria->addCondition("active = 1");
        }
        if ((isset($_POST['nama_dokter']))) {
            $criteria->addCondition("nama_dokter like '%" . $_POST['nama_dokter'] . "%'");
        }
        if ((isset($_POST['store']))) {
            $criteria->addCondition("store like '%" . $_POST['store'] . "%'");
        }
        //$criteria->addCondition("active = 1" );
        $model = Dokter::model()->findAll($criteria);
        $total = Dokter::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}