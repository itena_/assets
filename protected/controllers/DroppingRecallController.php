<?php
class DroppingRecallController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $docref = "";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $is_new = $_POST['mode'] == 0;
                $model = $is_new ? new DroppingRecall : $this->loadModel($_POST['dropping_recall_id'], 'DroppingRecall');
                if (!$is_new && ($model == null)) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'DroppingRecall')) . "Fatal error, record not found.");
                }

                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(DROPPING_RECALL);
                } else {
                    if ($model->store != STOREID) {
                        throw new Exception("Anda tidak diperbolehkan mengubah data ini.");
                    }

                    if ($model->status != PR_OPEN) {
                        throw new Exception("Data tidak bisa diubah.");
                    }

                    /*
                     * Rollback hpp
                     */
                    $dataDetail = DroppingRecallDetails::model()->findAll('dropping_recall_id = :dropping_recall_id AND visible = 1',
                        array(':dropping_recall_id' => $model->dropping_recall_id));
                    foreach ($dataDetail as $dt){
                        $qtyDt = -$dt->qty;
                        $totalDt = $qtyDt * $dt->price;
                        $dt->barang->count_biaya_beli($qtyDt, $totalDt, $model->store);
                    }

                    $docref = $model->doc_ref;

                    /*
                     * Hapus detai + stockmoves
                     */
                    DroppingRecallDetails::model()->updateAll(array('visible' => 0), 'dropping_recall_id = :dropping_recall_id',
                        array(':dropping_recall_id' => $model->dropping_recall_id));
                    U::delete_stock_moves_all(DROPPING_RECALL, $model->dropping_recall_id);
                }

                /*
                 * Simpan Header
                 */
                $header = [];
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $header[$k] = $v;
                }

                $model->attributes = $header;
                $model->up = DR_SEND;

                $model->doc_ref = $docref;
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'DroppingRecall')) . CHtml::errorSummary($model));

                /*
                 * Simpan Detail
                 */
                $histories = [];
                foreach ($detils as $detil) {
                    $barang = Barang::model()->findByPk($detil['barang_id']);

                    /*
                     * recall tidak boleh lebih dari transfer dikurangi receive
                     * transfer = receive + recall
                     */
                    $dropInfo = StockMoves::queryStockInTransit(array(
                        'barang_id' => $detil['barang_id'],
                        'dropping_id' => $detil['dropping_id']
                    ))->queryRow();
                    $qty_ditarik = get_number($detil['qty']);
                    if ($dropInfo['qty'] < $qty_ditarik) {
                        throw new Exception("Jumlah penarikan (Recall) <b>$barang->kode_barang</b> lebih besar dari jumlah yang tersisa di INTRANSIT."
                            ."<br>Qty Transfer (<span style='color: #0A246A;'>".$dropInfo['store_pengirim']."</span>) : ".$dropInfo['qty_dropping']
                            ."<br>Qty Received (<span style='color: #0A246A;'>".$dropInfo['store_penerima']."</span>) : ".$dropInfo['qty_received']
                            ."<br>Qty Recall : ".$dropInfo['qty_recalled']
                            ."<br>Qty di INTRANSIT : ".$dropInfo['qty']
                            ."<br>Qty akan ditarik : ".$qty_ditarik);
                    }


                    /*
                     * Simpan data
                     */
                    $item_details = new DroppingRecallDetails;
                    $item_details->dropping_recall_id = $model->dropping_recall_id;
                    $item_details->dropping_id = $detil['dropping_id'];
                    $item_details->seq = $detil['seq'];
                    $item_details->barang_id = $detil['barang_id'];
                    $item_details->qty = get_number($detil['qty']);
                    $item_details->price = $detil['price'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'DroppingRecall Details')) . CHtml::errorSummary($item_details));

                    if ($barang->grup->kategori->is_have_stock()) {
                        /*
                         * update hpp
                         */
                        $total = $item_details->qty * $item_details->price;
                        $item_details->barang->count_biaya_beli($item_details->qty, $total, $model->store);

                        /*
                         * Simpan stock moves
                         */
                        U::add_stock_moves_all(
                            null,
                            DROPPING_RECALL,
                            $model->dropping_recall_id,
                            $model->tgl,
                            $item_details->barang_id,
                            -($item_details->qty),
                            $model->doc_ref,
                            $barang->get_cost($model->store),
                            STORE_TRANSIT
                        );
                        U::add_stock_moves_all(
                            null,
                            DROPPING_RECALL,
                            $model->dropping_recall_id,
                            $model->tgl,
                            $item_details->barang_id,
                            $item_details->qty,
                            $model->doc_ref,
                            $barang->get_cost($model->store),
                            $model->store
                        );
                    }

                    /*
                     * update status Dropping/Transfer
                     */
                    $model_number = DroppingSisa::model()->countByAttributes(array('dropping_id' => $item_details->dropping_id));
                    $dropping = Dropping::model()->findByPk($item_details->dropping_id);
                    if ($dropping == null) {
                        throw new Exception("Data Transfer tidak ditemukan!");
                    } else {
                        $dropping->lunas = $model_number <= 0? PR_CLOSED : PR_PROCESS;
                        if (!$dropping->save())
                            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Dropping')) .
                                CHtml::errorSummary($dropping));
                    }

                    /*
                     * Dropping History
                     */
                    $namauser = Yii::app()->user->getName();
                    $iduser = Yii::app()->user->getId();

                    $histories[] = DroppingHistory::add(
                        $dropping->order_dropping_id ? $dropping->order_dropping_id : $dropping->dropping_id,
                        $model->doc_ref,
                        $dropping->store,
                        $dropping->store_penerima,
                        'RECALLED',
                        $item_details->barang->kode_barang." (".$item_details->qty.$item_details->barang->sat.")"." telah di recall oleh ".$namauser
                    );
                    //$dropping->lunas == PR_CLOSED? 'CLOSED': ($dropping->lunas == PR_PROCESS? 'PROCESS':'OPEN')


                }
                if ($is_new) {
                    $ref->save(DROPPING_RECALL, $model->dropping_recall_id, $docref);
                }

                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;

                /*
                 * Sync Dropping Recall
                 */
                U::runCommand('soap','droppingrecallall', '--id=' . $model->dropping_recall_id, 'DroppingRecallAll_' . $model->dropping_recall_id . '.log');

                foreach ($model->droppingRecallDetails as $item_details){
                    U::runCommand('soap','droppingall', '--id=' . $item_details->dropping_id, 'droppingall_' . $item_details->dropping_id . '.log');
                }
                /*
                 * Sync Dropping History
                 */
                foreach ($histories as $history){
                    U::runCommand('soap','droppinghistory', '--id=' . $history->id_history,  'droppinghistory_'.$history->id_history.'.log');
                }

                StockMoves::push($model->dropping_recall_id);
                StockMovesPerlengkapan::push($model->dropping_recall_id);
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionClose()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $model = $this->loadModel($_POST['dropping_recall_id'], 'DroppingRecall');

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if ($model->status == PR_CLOSED) {
                    throw new Exception("Transfer Recall telah ditutup.");
                }
                if ($model->store != STOREID){
                    throw new Exception("Anda tidak diperbolehkan untuk menutup data Transfer Recall ini.");
                }
                $model->status = PR_CLOSED;
                $model->up = DR_CLOSE;
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'DroppingRecall')) . CHtml::errorSummary($model));
                }

                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
                /*
                 * Sync
                 */
                U::runCommand('soap','DroppingRecallAll', '--id=' . $model->dropping_recall_id, 'DroppingRecallAll_' . $model->dropping_recall_id . '.log');

            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionIndex()
    {
        $cmd = DbCmd::instance();

        if (isset($_POST['tgl'])) {
            $cmd->addCondition('t.tgl = :tgl');
            $cmd->addParam(':tgl', $_POST['tgl']);
        }
        if (isset($_POST['doc_ref'])) {
            $cmd->addCondition('t.doc_ref LIKE :doc_ref');
            $cmd->addParam(':doc_ref', '%'.$_POST['doc_ref'].'%');
        }
        if (isset($_POST['note'])) {
            $cmd->addCondition('c.note LIKE :note');
            $cmd->addParam(':note', '%'.$_POST['note'].'%');
        }
        if (isset($_POST['store'])) {
            $cmd->addCondition('t.store = :store');
            $cmd->addParam(':store', $_POST['store']);
        }
        if (isset($_POST['status']) && $_POST['status'] !== 'all') {
            $cmd->addCondition('t.status = :status');
            $cmd->addParam(':status', $_POST['status']);
        }

        $cmd->addFrom('{{dropping_recall}} t')
            ->addOrder('t.tdate DESC');

        $count = $cmd->queryCount();
        if (isset($_POST['mode']) && $_POST['mode'] == 'grid') {
            $cmd->setLimit(array_key_exists('limit', $_POST)? $_POST['limit'] : 20, array_key_exists('start', $_POST)? $_POST['start'] : 0);
        }
        $model = $cmd->queryAll();
        $this->renderJsonArrWithTotal($model, $count);
    }

    public function actionDetail()
    {
        $cmd = Barang::queryFilter(DbCmd::instance())
            ->addSelect(array(
                't.*',
                'd.doc_ref dropping_doc_ref, d.store store_pengirim, d.store_penerima'
            ))
            ->addFrom('{{dropping_recall_details}} t')
            ->addLeftJoin('{{dropping}} d', ' d.dropping_id = t.dropping_id ')
            ->addCondition('t.visible = 1')
            ->addOrder ('t.seq ASC');

        if (isset($_POST['dropping_recall_id'])) {
            $cmd->addCondition('t.dropping_recall_id = :dropping_recall_id');
            $cmd->addParam(':dropping_recall_id', $_POST['dropping_recall_id']);
        }

        $model = $cmd->queryAll();
        $this->renderJsonArr($model);
    }

    public function actionAllDetail()
    {
        $params = [];
        foreach ($_POST as $k => $v) {
            if ($k == 'detil') continue;
            if (is_angka($v)) $v = get_number($v);
            $params[$k] = $v;
        }

        $cmd = StockMoves::queryStockInTransit($params);

        if (isset($_POST['dropping_recall_id'])) {
            $cmd->setSelect(array(
                    'qty_recalled' => '(IFNULL(t2.qty_recalled,0)-IFNULL(drd.qty,0)) qty_recalled',
                    'qty' => '(t.qty-IFNULL(t2.qty_received,0)-IFNULL(t2.qty_recalled,0)+IFNULL(drd.qty,0)) qty',
                    'selected' => 'IFNULL(drd.qty, 0) selected'
                ))
                ->addLeftJoin('{{dropping_recall_details}} drd',
                'drd.dropping_id = t.dropping_id AND drd.barang_id = t.barang_id AND drd.dropping_recall_id = :dropping_recall_id AND drd.visible = 1')
                ->addParam(':dropping_recall_id', $_POST['dropping_recall_id'])
                ->clearHaving()->addHaving('qty > 0 OR selected != 0');
        }
        $cmd = Barang::queryFilter($cmd, $params)
            ->addOrder ('{{barang}}.nama_barang ASC');

        $model = $cmd->queryAll();
        $this->renderJsonArr($model);
    }

    public function actionResend()
    {
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Proses kirim ulang gagal.";

            try {
                $model = $this->loadModel($_POST['dropping_recall_id'], 'DroppingRecall');
                $msg = "Nomor Transfer <b>".$model->doc_ref."</b> telah di kirim ulang..";
                $status = true;

                $model->up = DR_SEND;

                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'DroppingRecall')) . CHtml::errorSummary($model));
                }

                U::runCommand('soap','DroppingRecallAll', '--id=' . $model->dropping_recall_id, 'DroppingRecallAll_' . $model->dropping_recall_id . '.log');

                foreach ($model->droppingRecallDetails as $item_details){
                    U::runCommand('soap','droppingall', '--id=' . $item_details->dropping_id, 'droppingall_' . $item_details->dropping_id . '.log');

                    /* Dropping History */
                    $order_dropping_id = $item_details->dropping->order_dropping_id;
                    $trans_id = $order_dropping_id? $order_dropping_id : $item_details->dropping_id;
                    U::runCommand('soap','droppinghistoryresend', '--id=' .  $trans_id,  'droppinghistory_'.$trans_id.'.log');
                }
            }
            catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $model->doc_ref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
}