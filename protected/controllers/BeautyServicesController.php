<?php

class BeautyServicesController extends GxController {

    public function actionCreate() {
        if (Yii::app()->request->isPostRequest) {
            $status = false;
            $msg = 'Failed update data.';
            if (isset($_POST) && !empty($_POST)) {
                $id = $_POST['salestrans_id'];
                $detils = CJSON::decode($_POST['detil']);
                $mode = $_POST['mode'];
                app()->db->autoCommit = false;
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    /** @var Salestrans $model */
                    $model = Salestrans::model()->findByPk($id);
                    if ($model == null) {
                        throw new Exception('Transaksi penjualan tidak ditemukan');
                    }
                    switch ($mode) {
                        case 0 :
                            $model->start_at = new CDbExpression('NOW()');
                            $model->end_estimate = new CDbExpression(
                                    "DATE_ADD(DATE_ADD(NOW(), INTERVAL $model->total_durasi second), INTERVAL $model->add_durasi second)"
                            );
                            break;
                        case 1:
                            $model->end_at = new CDbExpression('NOW()');
                            break;
                        case 2:
                            $model->add_durasi = get_number($_POST['add_durasi']) * 60;
                            $model->add_jasa = $_POST['add_jasa'];
                            $model->end_estimate = new CDbExpression(
                                    "DATE_ADD(DATE_ADD(NOW(), INTERVAL $model->total_durasi second), INTERVAL $model->add_durasi second)"
                            );
                            break;
                    }
                    if (!$model->save()) {
                        throw new Exception(CHtml::errorSummary($model));
                    } else {
                        $msg = 'Success update.';
                    }
                    foreach ($detils as $detil) {
                        $dokter_id = $detil['dokter_id'];
                        $beauty_id = $detil['beauty_id'];
                        $beauty2_id = $detil['beauty2_id'];
                        $beauty3_id = $detil['beauty3_id'];
                        $beauty4_id = $detil['beauty4_id'];
                        $beauty5_id = $detil['beauty5_id'];
                        /** @var $salestransDetails SalestransDetails */
                        $salestransDetails = $this->loadModel($detil['salestrans_details'], 'SalestransDetails');
	                    if (KMT_PERAWATAN)
	                    {
		                    $kmt = KmtMaster::model()->findByAttributes(array(
			                    'barang_id'=>$salestransDetails->barang_id
	                        ));
		                    if ($kmt != null)
		                    	$jasa_dokter = $kmt->amount;
		                    else
		                    	$jasa_dokter = 0;
	                    }
	                    else
	                    {
		                    $jasa_dokter = SysPrefs::get_val("kmt_nominal");
		                    if ($jasa_dokter == null) {
			                    $jasa_dokter = 0;
		                    }
	                    }
                        $salestransDetails->dokter_id = $dokter_id;
                        $salestransDetails->jasa_dokter = $jasa_dokter;
                        if (!$salestransDetails->save()) {
                            throw new Exception(CHtml::errorSummary($salestransDetails));
                        }

                        if (METHODE_BONUS == 1) {
                            /*
                             * Hapus data bonus sebelumnya (jika ada), agar tidak double
                             */
                            Bonus::deleteDataBonus($salestransDetails->salestrans_details, 'service');

                            /*
                             * Bonus Aishaderm
                             * DOKTER TINDAKAN
                             */
                            $salestrans = Salestrans::model()->find('salestrans_id = :salestrans_id', array(':salestrans_id' => $salestransDetails->salestrans_id));
                            Bonus::serviceDokter(PENJUALAN, $salestrans, $salestransDetails, $dokter_id);
                        }

                        $salestransDetails->save_beauty_tips($beauty_id, $beauty2_id, $beauty3_id, $beauty4_id, $beauty5_id);
                        $beautyService = BeautyServices::model()->findAllByAttributes(array(
                            'salestrans_details' => $salestransDetails->salestrans_details,
                            'visible' => 1,
                        ));
                        foreach ($beautyService as $bs) {
                            $beOnStatus = BeautyOndutyStatus::model()->findByAttributes(array('beauty_id' => $bs->beauty_id));
                            if ($beOnStatus == null) {
                                throw new Exception('Beauty On Duty Status tidak ditemukan!!!');
                            }
                            $beOn = BeautyOnduty::model()->findByPk($beOnStatus->beauty_onduty_id);
                            if ($beOn != null) {
                                switch ($mode) {
                                    case 0 :
                                        $beOn->time_start = $model->start_at;
                                        $beOn->estimate_end = $model->end_estimate;
                                        if ($beOn->note_ == 'KOSONG' || $beOn->note_ == 'ISOMA') {
                                            $beOn->note_ = $salestransDetails->barang->kode_barang;
                                        } else {
                                            $beOn->note_ .= ', ' . $salestransDetails->barang->kode_barang;
                                        }
                                        break;
                                    case 1:
                                        $beOn->note_ = 'KOSONG';
                                        $beOn->time_start = new CDbExpression('NOW()');
                                        break;
                                    case 2:
                                        $beOn->estimate_end = $model->end_estimate;
                                        if ($beOn->note_ == 'KOSONG' || $beOn->note_ == 'INSOMA') {
                                            $beOn->note_ = $model->add_jasa;
                                        } else {
                                            $beOn->note_ .= ', ' . $model->add_jasa;
                                        }
                                        break;
                                }
                                if (!$beOn->save()) {
                                    throw new Exception(CHtml::errorSummary($beOn));
                                }
                            } else {
                                throw new Exception('Beauty On Duty tidak ditemukan');
                            }
                        }
                    }
                    $transaction->commit();
                    $status = true;
                } catch (Exception $ex) {
                    $transaction->rollback();
                    $status = false;
                    $msg = $ex->getMessage();
                }
                app()->db->autoCommit = true;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

//    public function actionIndex()
//    {
//        if (isset($_POST['limit'])) {
//            $limit = $_POST['limit'];
//        } else {
//            $limit = 20;
//        }
//        if (isset($_POST['start'])) {
//            $start = $_POST['start'];
//        } else {
//            $start = 0;
//        }
//        $criteria = new CDbCriteria();
//        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
//        $model = BeautyServices::model()->findAll($criteria);
//        $total = BeautyServices::model()->count($criteria);
//        $this->renderJson($model, $total);
//    }
    public function actionCheckFinal() {
        if (Yii::app()->request->isPostRequest) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $arr = U::get_date_service_no_final($from, $to, $store);
            echo CJSON::encode(array(
                'success' => count(array_column($arr, 'tgl')) == 0,
                'msg' => "You have not set final service : <br>" . implode("<br>", array_column($arr, 'tgl'))));
            Yii::app()->end();
        } else
            throw new CHttpException(403, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionAntrian() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
//        $criteria->select = 'nsdb.customer_id,nsdb.doc_ref,nsdb.salestrans_id,
//            nsdb.pending,nsdb.end_at,nsdb.start_at,nsdb.end_estimate,
//            nsdb.total_durasi,nsdb.nama_customer,nsdb.no_customer,
//            time(nsdb.tdate) tdate,TIMEDIFF(now(),nsdb.tdate) up,
//			nsdb.kategori_id';
//        $criteria->alias = 'nsdb';
//        $criteria->addCondition("start_at is null");
        $criteria->addCondition("counter != 'E' or counter is null");
        $criteria->addCondition("hold = 0");
//        $criteria->addCondition("DATE(tanggal)=DATE(NOW())");
//        $criteria->addCondition("bagian!='kasir'");
        $criteria->addCondition("bagian!='perawatan'");
        $criteria->addCondition("bagian!='selesai'");
        $criteria->having = "DATE(tanggal)=DATE(NOW())";
//        $criteria->addCondition("end_=1");
//        $criteria->addCondition("nsdb.kategori_id = 1");
//        $criteria->group = 'salestrans_id';
//        $criteria->having = 'nsdb.kategori_id = 1';
        $criteria->order = 'tanggal ASC';
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = AntrianPerawatan::model()->findAll($criteria);
        $total = AntrianPerawatan::model()->count($criteria);
        $this->renderJson($model, $total);
    }

    public function actionPanggilAntrian() {
        if (isset($_POST) && !empty($_POST)) {
            /*
             * Untuk menandakan perawatan VIP, id_antrian dikirim dengan nilai vip
             * $_POST['id_antrian'] = 'vip' 
             */
            $id_antrian = $_POST['id_antrian'];
            $criteria = new CDbCriteria();
            $param = array();
            if ($id_antrian == 'vip') {
                $salestrans_id = $_POST['salestrans_id'];
                $criteria->select = 'nsdb.customer_id,nsdb.doc_ref,nsdb.salestrans_id,
                        nsdb.pending,nsdb.end_at,nsdb.start_at,nsdb.end_estimate,
                        nsdb.total_durasi,nsdb.nama_customer,nsdb.no_customer,
                        time(nsdb.tdate) tdate,TIMEDIFF(now(),nsdb.tdate) up,
			nsdb.kategori_id,(nsdb.add_durasi/60) add_durasi';
                $criteria->alias = 'nsdb';
                $criteria->addCondition("counter != 'E' or counter is null");
                $criteria->addCondition("start_at is null");
                $criteria->addCondition("pending = 0");
                $criteria->addCondition("tgl=DATE(NOW())");
                $criteria->addCondition("nsdb.kategori_id = 1");
                $criteria->addCondition("nsdb.salestrans_id = :salestrans_id");
//                $param['id_antrian'] = $id_antrian;
                $param['salestrans_id'] = $salestrans_id;
                $criteria->group = 'nsdb.salestrans_id';
//        $criteria->having = 'nsdb.kategori_id = 1';
                $criteria->order = 'tdate DESC';
                $criteria->params = $param;
            } else {
                $criteria->select = 'nsdb.customer_id,nsdb.doc_ref,nsdb.salestrans_id,
                        nsdb.pending,nsdb.end_at,nsdb.start_at,nsdb.end_estimate,
                        nsdb.total_durasi,nsdb.nama_customer,nsdb.no_customer,
                        time(nsdb.tdate) tdate,TIMEDIFF(now(),nsdb.tdate) up,
			nsdb.kategori_id,(nsdb.add_durasi/60) add_durasi';
                $criteria->alias = 'nsdb';
                $criteria->addCondition("counter != 'E' or counter is null");
                $criteria->addCondition("start_at is null");
                $criteria->addCondition("pending = 0");
                $criteria->addCondition("tgl=DATE(NOW())");
                $criteria->addCondition("nsdb.kategori_id = 1");
                $criteria->addCondition("nsdb.id_antrian = :id_antrian");
                $param['id_antrian'] = $id_antrian;
                $criteria->group = 'nsdb.salestrans_id';
//        $criteria->having = 'nsdb.kategori_id = 1';
                $criteria->order = 'tdate DESC';
                $criteria->params = $param;
            }
            $model = SalestransDetailsBarang::model()->find($criteria);
//            $total = count($model);
            if ($model == null) {
                $jsonresult = '{"total":"0","results":""}';
            } else {
                $jsonresult = '{"total":"1","results":' . json_encode($model->getAttributes()) . '}';
            }
//            if ($total > 0) {
//                $jsonresult = '{"total":"' . $total . '","results":' . json_encode($model->getAttributes()) . '}';
//            } else {
//                $jsonresult = '{"total":"' . $total . '","results":""}';
//            }
            Yii::app()->end($jsonresult);
        }
    }

    public function actionAntrianPending() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
//        $criteria->select = 'nsdb.customer_id,nsdb.doc_ref,nsdb.salestrans_id,
//            nsdb.pending,nsdb.end_at,nsdb.start_at,nsdb.end_estimate,
//            nsdb.total_durasi,nsdb.nama_customer,nsdb.no_customer,
//			nsdb.kategori_id';
//        $criteria->alias = 'nsdb';
        $criteria->addCondition("counter != 'E' or counter is null");
        $criteria->addCondition("hold = 1");
//        $criteria->addCondition("DATE(tanggal)=DATE(NOW())");
//        $criteria->addCondition("bagian!='perawatan'");
        $criteria->addCondition("bagian!='perawatan'");
        $criteria->addCondition("bagian!='selesai'");
        $criteria->having = "DATE(tanggal)=DATE(NOW())";
//        $criteria->addCondition("end_=1");
//        $criteria->group = 'nsdb.salestrans_id';
//        $criteria->having = 'nsdb.kategori_id = 1';
        $criteria->order = 'tanggal ASC';
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = AntrianPerawatan::model()->findAll($criteria);
        $total = AntrianPerawatan::model()->count($criteria);
        $this->renderJson($model, $total);
    }

    public function actionProses() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        $criteria->select = 'nsdb.tgl,nsdb.customer_id,nsdb.doc_ref,nsdb.salestrans_id,
            nsdb.pending,nsdb.end_at,time(nsdb.start_at) start_at,time(nsdb.end_estimate) end_estimate,
            nsdb.total_durasi,nsdb.nama_customer,nsdb.no_customer,TIMEDIFF(nsdb.end_estimate,now()) up,
            nsdb.add_jasa,(nsdb.add_durasi/60) add_durasi,nsdb.kategori_id';
        $criteria->alias = 'nsdb';
        $criteria->addCondition("start_at is not null");
        $criteria->addCondition("end_at is null");
//        $criteria->addCondition("tgl=DATE(NOW())");
//        $criteria->having = "tgl=DATE(NOW())";
        $criteria->group = 'nsdb.salestrans_id,nsdb.kategori_id';
        $criteria->having = 'nsdb.kategori_id = 1 AND tgl=DATE(NOW())';
        $criteria->order = 'end_estimate DESC';
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = SalestransDetailsBarang::model()->findAll($criteria);
        $total = SalestransDetailsBarang::model()->count($criteria);
        $this->renderJson($model, $total);
    }

    public function actionTambahan() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        $criteria->addCondition("start_at is null");
//        $criteria->addCondition("tgl=DATE(NOW())");
        $criteria->having = "tgl=DATE(NOW())";
        $criteria->order = 'tdate DESC';
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = PerawatanTambahan::model()->findAll($criteria);
        $total = PerawatanTambahan::model()->count($criteria);
        $this->renderJson($model, $total);
    }

    public function actionBeautyOnduty() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
//        $criteria->select = 'nbo.salestrans_id,nbo.doc_ref,nbo.kategori_id,nbo.tgl,nbo.salestrans_details,
//            nbo.barang_id,nbo.qty,nbo.jasa_dokter,nbo.dokter_id,nbo.active,nbo.kode_barang,
//            nbo.nama_barang,nbo.store,nbo.nama_customer,nbo.no_customer,nbo.beauty_id,
//            nbo.kode_beauty,nbo.nama_beauty,time(nbo.start_at) start_at,nbo.end_at,nbo.pending,
//            time(nbo.end_estimate) end_estimate,nbo.total_durasi,nbo.customer_id,nbo.remain';
//        $criteria->alias = 'nbo';
//        $criteria->addCondition("start_at is not null");
//        $criteria->addCondition("end_at is null");
//        $criteria->addCondition("tgl=DATE(NOW())");
//        $criteria->order = 'end_estimate ASC';
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = BeautyOndutyStatus::model()->findAll($criteria);
        $total = BeautyOndutyStatus::model()->count($criteria);
        $this->renderJson($model, $total);
    }

    public function actionUpdate() {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            if (!isset($_POST['salestrans_id'])) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Data id antrian tidak ada.'
                ));
                Yii::app()->end();
            }
            $id = $_POST['salestrans_id'];
            $mode = $_POST['mode'];
            /** @var Salestrans $model */
            $model = Salestrans::model()->findByPk($id);
            $status = false;
            $msg = '';
            if (!$model) {
                $msg = 'Data antrian tidak ditemukan.';
            } else {
                switch ($mode) {
                    case 'pending':
                        $model->pending = 1;
                        break;
                    case 'unpending':
                        $model->pending = 0;
                        break;
                    default :
                        break;
                }
            }
            if (!$model->save()) {
                $msg = CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionIndex() {
        $criteria = new CDbCriteria();
        $param = array();
//        if (isset($_POST['limit'])) {
//            $limit = $_POST['limit'];
//        } else {
//            $limit = 20;
//        }
//        if (isset($_POST['start'])) {
//            $start = $_POST['start'];
//        } else {
//            $start = 0;
//        }
        if (isset($_POST['salestrans_id'])) {
            $criteria->addCondition('salestrans_id = :salestrans_id');
            $param[':salestrans_id'] = $_POST['salestrans_id'];
        }
//        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
//            (isset($_POST['limit']) && isset($_POST['start']))
//        ) {
//            $criteria->limit = $limit;
//            $criteria->offset = $start;
//        }
        $criteria->params = $param;
        $model = BeautyServiceView::model()->findAll($criteria);
        $total = BeautyServiceView::model()->count($criteria);
        $this->renderJson($model, $total);
        Yii::app()->end();
    }

}
