<?php

class UsersController extends GxController {

    public function actionCreate() {
        $model = new Users;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try
            {
                foreach ($_POST as $k => $v) {
                    if (is_angka($v))
                        $v = get_number($v);
                    $_POST['Users'][$k] = $v;
                }

                $user_id = $_POST['Users']['user_id'];
                $businessunit_id = $_POST['Users']['businessunit_id'];
                $checkuserid = Users::model()->findByAttributes(array('user_id' => $user_id, 'businessunit_id' => $businessunit_id));

                if(!$checkuserid)
                {
                    $model->store = $_POST['Users']['branch'];
                    $model->attributes = $_POST['Users'];
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Users')) . CHtml::errorSummary($model));
                    }
                    $msg = t('save.success', 'app');
                }
                else
                {
                    $msg = "Username sudah ada di database !!!";
                }

                /*
                 * START Create User Employee
                 */
                /*if ($_POST['employee_id']) {
                    $ue = new UserEmployee;
                    $ue->employee_id = $_POST['employee_id'];
                    $ue->id = $model->id;
                    $ue->save();
                }*/
                /*
                 * END Create User Employee
                 */
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdatePass() {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $passold = $_POST['passwordold'];
            $passnew = $_POST['password'];
            $model = $this->loadModel(Yii::app()->user->getId(), 'Users');
            $msg = t('pass.wrong.old', 'app');
            $status = false;
            if (bCrypt::verify($passold, $model->password)) {
//                $crypt = new bCrypt();
//                $pass = $crypt->hash($passnew);
                $model->password = $passnew;
                if ($model->save()) {
                    $status = true;
                    $msg = t('pass.success', 'app');
                } else {
                    $status = false;
                    $msg = t('pass.fail', 'app');
                }
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdateRole() {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['id'];
            $role = $_POST['security_roles_id'];
            $username = $_POST['user_id'];
            $name = $_POST['name'];
            $store = $_POST['branch'];
            $businessunit_id = $_POST['businessunit_id'];
            $model = $this->loadModel($id, 'Users');
            $msg = t('save.success', 'app');
            $status = true;
            $model->security_roles_id = $role;
            $model->user_id = $username;
            $model->name = $name;
            $model->businessunit_id = $businessunit_id;
            $model->store = $store;
            if (!$model->save()) {
                $status = false;
                $msg = t('save.fail', 'app');
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'Users');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['Users'][$k] = $v;
            }
            $msg = t('save.fail', 'app');
            $model->attributes = $_POST['Users'];
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id', 'app', array('{id}' => $model->id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }
    }

    public function actionIndex() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }

        $user_id = Yii::app()->user->getId();
        $user = Users::model()->findByPk( $user_id );
        $businessunit_id = $_COOKIE['businessunitid'];
        $role = $user->security_roles_id ;

        $criteria = new CDbCriteria();
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }

        if($role != "2")
        {
            $criteria->addCondition('businessunit_id = :businessunit_id');
            $param[':businessunit_id'] = $businessunit_id;
            $criteria->params = $param;
            $criteria->order = "user_id ASC";
        }
        /*if($role != "5")
        {
            $criteria->addCondition('businessunit_id = :businessunit_id');
            $param[':businessunit_id'] = $businessunit_id;
            $criteria->params = $param;
            $criteria->order = "user_id ASC";
        }*/

        $model = UserView::model()->findAll($criteria);
        $total = UserView::model()->count($criteria);
        $this->renderJson($model, $total);
    }

}
