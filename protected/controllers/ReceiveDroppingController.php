<?php
class ReceiveDroppingController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            if (Tender::is_exist($_POST['tgl'])) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Tender Declaration already created'
                ));
                Yii::app()->end();
            }
            $status = false;
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $is_new = $_POST['mode'] == 0;
                $model = $is_new ? new ReceiveDropping : $this->loadModel($_POST['id'], 'ReceiveDropping');
                if (!$is_new && ($model == null)) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'ReceiveDropping')) . "Fatal error, record not found.");
                }
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(RECEIVE_DROPPING);
                } else {
                    if ($model->store != STOREID) {
                        throw new Exception("Anda tidak diperbolehkan mengubah data ini.");
                    }
                    if ($model->lunas == PR_CLOSED) {
                        throw new Exception("Transfer Receive tidak bisa diubah.");
                    }
                    $docref = $model->doc_ref;

                    /*
                     * Rollback hpp
                     * todo : akan diganti pakai cara COGS
                     */
                    $dataDetail = ReceiveDroppingDetails::model()->findAll('receive_dropping_id = :receive_dropping_id AND visible = 1',
                        array(':receive_dropping_id' => $model->receive_dropping_id));
                    foreach ($dataDetail as $dt){
                        $qtyDt = -$dt->qty;
                        $totalDt = $qtyDt * $dt->price;
                        $dt->barang->count_biaya_beli($qtyDt, $totalDt, $model->store);
                    }

                    ReceiveDroppingDetails::model()->updateAll(array('visible' => 0), 'receive_dropping_id = :receive_dropping_id',
                        array(':receive_dropping_id' => $model->receive_dropping_id));
                    
                    //$this->delete_stock_moves(RECEIVE_DROPPING, $model->receive_dropping_id);
                    U::delete_stock_moves_all(RECEIVE_DROPPING, $model->receive_dropping_id);
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['ReceiveDropping'][$k] = $v;
                }
                $_POST['ReceiveDropping']['doc_ref'] = $docref;
                $model->attributes = $_POST['ReceiveDropping'];
                $model->up = DR_SEND;
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive Dropping')) . CHtml::errorSummary($model));
                foreach ($detils as $detil) {
                    $barang = Barang::model()->findByPk($detil['barang_id']);

                    /*
                     * Receive tidak boleh lebih dari transfer dikurangi Recall
                     * transfer = receive + recall
                     */
                    $dropInfo = StockMoves::queryStockInTransit(array(
                        'barang_id' => $detil['barang_id'],
                        'dropping_id' => $model->dropping_id
                    ))->queryRow();
                    $qty_diterima = get_number($detil['qty']);
                    if ($dropInfo['qty'] < $qty_diterima) {
                        throw new Exception("Jumlah penerimaan (Receive) <b>$barang->kode_barang</b> lebih besar dari jumlah yang tersisa di INTRANSIT."
                            ."<br>Qty Transfer (<span style='color: #0A246A;'>".$dropInfo['store_pengirim']."</span>) : ".$dropInfo['qty_dropping']
                            ."<br>Qty Received (<span style='color: #0A246A;'>".$dropInfo['store_penerima']."</span>) : ".$dropInfo['qty_received']
                            ."<br>Qty Recall : ".$dropInfo['qty_recalled']
                            ."<br>Qty di INTRANSIT : ".$dropInfo['qty']
                            ."<br>Qty akan diterima : ".$qty_diterima);
                    }


                    $droppingDetail = DroppingDetails::model()->find(
                            'barang_id = :barang_id AND visible = 1 AND dropping_id = :dropping_id',
                            array(
                                ':barang_id' => $detil['barang_id'],
                                ':dropping_id' => $model->dropping_id
                            )
                        );
                    
                    $item_details = new ReceiveDroppingDetails;
                    $_POST['ReceiveDroppingDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['ReceiveDroppingDetails']['qty'] = get_number($detil['qty']);
                    $_POST['ReceiveDroppingDetails']['receive_dropping_id'] = $model->receive_dropping_id;
                    $_POST['ReceiveDroppingDetails']['price'] = $droppingDetail->price;
                    $item_details->attributes = $_POST['ReceiveDroppingDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive Dropping detail')) . CHtml::errorSummary($item_details));

                    
                    /*
                     * penerimaan tidak boleh lebih dari pengiriman
                     */
//                    $qty_terima = ReceiveDropping::get_total_received($item_details->barang_id, $model->store, $model->dropping_id);
//                    if ($droppingDetail->qty < $qty_terima) {
//                        throw new Exception("Jumlah penerimaan <b>$barang->kode_barang</b> lebih besar dari jumlah pengiriman.<br>Total Penerimaan : $qty_terima<br>Jumlah Pengiriman : $droppingDetail->qty");
//                    }
                    
                    if ($barang->grup->kategori->is_have_stock()) {
                        $total = $item_details->qty * $item_details->price;
                        $item_details->barang->count_biaya_beli($item_details->qty, $total, $model->store);
                    
//                        U::add_stock_moves(RECEIVE_DROPPING, $model->receive_dropping_id, $model->tgl,
//                            $item_details->barang_id, $item_details->qty, $model->doc_ref,
//                            $barang->get_cost($model->store), $model->store);
//
//                        U::add_stock_moves(RECEIVE_DROPPING, $model->receive_dropping_id, $model->tgl,
//                            $item_details->barang_id, -$item_details->qty, $model->doc_ref,
//                            $barang->get_cost($model->store), STORE_TRANSIT);

                        U::add_stock_moves_all(
                            null,
                            RECEIVE_DROPPING,
                            $model->receive_dropping_id,
                            $model->tgl,
                            $item_details->barang_id,
                            $item_details->qty,
                            $model->doc_ref,
                            $barang->get_cost($model->store),
                            $model->store
                        );
                        U::add_stock_moves_all(
                            null,
                            RECEIVE_DROPPING,
                            $model->receive_dropping_id,
                            $model->tgl,
                            $item_details->barang_id,
                            -$item_details->qty,
                            $model->doc_ref,
                            $barang->get_cost($model->store),
                            STORE_TRANSIT
                        );
                    }
                }
                if ($is_new) {
                    $ref->save(RECEIVE_DROPPING, $model->receive_dropping_id, $docref);
                }
                
                $model_number = DroppingSisa::model()
                    ->countByAttributes(array('dropping_id' => $model->dropping_id));
                $dropping = Dropping::model()->findByPk($model->dropping_id);
                if ($dropping == null) {
                    throw new Exception("Data Transfer tidak ditemukan!");
                } else {
                    $dropping->lunas = $model_number <= 0? PR_CLOSED : PR_PROCESS;
                    //$dropping->up = DR_RECEIVE;
                    if (!$dropping->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Dropping')) .
                            CHtml::errorSummary($dropping));
                }


                $orderid = $dropping->order_dropping_id ? $dropping->order_dropping_id  : $dropping->dropping_id;


                $namauser = Yii::app()->user->getName();
                $iduser = Yii::app()->user->getId();

                $history = new DroppingHistory;
                $history->order_dropping_id = $orderid;
                $history->user_id = $iduser;
                $history->user = $namauser;
                $history->doc_ref = $docref;
                $history->store = $model->store;
                $history->store_pengirim = $model->store_pengirim;
                $history->tgl = new CDbExpression('NOW()');
                if ($model_number <= 0)
                {
                    $history->status = "RECEIVED";
                    $history->desc = $model->store." telah menerima barang dari ".$model->store_pengirim;
                }
                else
                {
                    $history->status = "RECEIVING";
                    $history->desc = $model->store." telah menerima sebagian barang dari ".$model->store_pengirim;
                }

                if (!$history->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'DroppingHistory')) . CHtml::errorSummary($history));

                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
                //app()->db->autoCommit = true;
//                if(PUSH_PUSAT){
//                    U::runCommand('soap','receivedropping', '--id=' . $model->receive_dropping_id,  'receivedropping_'.$model->receive_dropping_id.'.log');
//                }
//                if (PUSH_ALL_CABANG) {
                    U::runCommand('soap','receivedroppingall', '--id=' . $model->receive_dropping_id,  'receivedroppingall_'.$model->receive_dropping_id.'.log');
//                }

//                if(PUSH_PUSAT){
//                    U::runCommand('soap','dropping', '--id=' . $dropping->dropping_id,  'dropping_'.$dropping->dropping_id.'.log');
//                }
//                if (PUSH_ALL_CABANG) {
                    U::runCommand('soap','droppingall', '--id=' . $dropping->dropping_id,  'droppingall_'.$dropping->dropping_id.'.log');
//                }
                //U::runCommand('soap','orderdroppingall', '--id=' . $orderDropping->order_dropping_id, 'orderdroppingall_' . $orderDropping->order_dropping_id . '.log');

                U::runCommand('soap','droppinghistory', '--id=' . $history->id_history,  'droppinghistory_'.$history->id_history.'.log');



                StockMoves::push($model->receive_dropping_id);
                StockMovesPerlengkapan::push($model->receive_dropping_id);

            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionClose()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $model = $this->loadModel($_POST['id'], 'ReceiveDropping');

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if ($model->lunas == PR_CLOSED) {
                    throw new Exception("Transfer Receive telah ditutup.");
                }
                if ($model->store != STOREID){
                    throw new Exception("Anda tidak diperbolehkan untuk menutup data Transfer Receive ini.");
                }
                $model->lunas = PR_CLOSED;
                $model->up = DR_CLOSE;
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'ReceiveDropping')) . CHtml::errorSummary($model));
                }

                $dropping = Dropping::model()->findByPk($model->dropping_id);
                //$orderDropping = OrderDropping::model()->findByPk($dropping->order_dropping_id);

                $msg = "Data Transfer Receive telah ditutup.";
                $status = true;

                $namauser = Yii::app()->user->getName();
                $iduser = Yii::app()->user->getId();

                $history = new DroppingHistory();
                $history->order_dropping_id = $dropping->order_dropping_id ? $dropping->order_dropping_id  : $dropping->dropping_id;//$orderDropping->order_dropping_id;
                $history->user_id = $iduser;
                $history->user = $namauser;
                $history->doc_ref = $model->doc_ref;
                $history->store = $model->store;
                $history->store_pengirim = $model->store_pengirim;
                $history->tgl = new CDbExpression('NOW()');
                $history->status = "CLOSED";
                $history->desc = "Transfer receive ditutup oleh ".$namauser;

                if (!$history->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'DroppingHistory')) . CHtml::errorSummary($history));


                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
                //app()->db->autoCommit = true;

                if(PUSH_PUSAT){
                    U::runCommand('soap','receivedropping', '--id=' . $model->receive_dropping_id,  'receivedropping_'.$model->receive_dropping_id.'.log');
                }
                if (PUSH_ALL_CABANG) {
                    U::runCommand('soap','receivedroppingall', '--id=' . $model->receive_dropping_id,  'receivedroppingall_'.$model->receive_dropping_id.'.log');
                }

                U::runCommand('soap','droppinghistory', '--id=' . $history->id_history,  'droppinghistory_'.$history->id_history.'.log');


            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'ReceiveDropping')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }

    public function actionResend()
    {
        if (isset($_POST) && !empty($_POST)) {
            $msg = "Proses kirim ulang gagal.";

            try
            {
                $model = $this->loadModel($_POST['id'], 'ReceiveDropping');
                if($model->lunas != 2)
                {
                    $model->up = DR_SEND;
                }
                else
                {
                    $model->up = DR_CLOSE;
                }

                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'OrderDropping')) . CHtml::errorSummary($model));
                }

                $msg = "Nomor Transfer Receive <b>".$model->doc_ref."</b> telah di kirim ulang..";
                $status = true;

                U::runCommand('soap','receivedroppingall', '--id=' . $model->receive_dropping_id,  'receivedroppingall_'.$model->receive_dropping_id.'.log');


                $model_number = DroppingSisa::model()
                    ->countByAttributes(array('dropping_id' => $model->dropping_id));
                $dropping = Dropping::model()->findByPk($model->dropping_id);
                if ($dropping == null) {
                    throw new Exception("Data Transfer tidak ditemukan!");
                } else {
                    $dropping->lunas = $model_number <= 0? PR_CLOSED : PR_PROCESS;
                    //$dropping->up = DR_SEND;
                    if (!$dropping->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Dropping')) .
                            CHtml::errorSummary($dropping));
                }

                $orderDropping = OrderDropping::model()->findByPk($dropping->order_dropping_id);

                U::runCommand('soap','droppingall', '--id=' . $dropping->dropping_id,  'droppingall_'.$dropping->dropping_id.'.log');

                U::runCommand('soap','droppinghistoryresend', '--id=' . $orderDropping->order_dropping_id,  'droppinghistory_'.$orderDropping->order_dropping_id.'.log');

                StockMoves::push($model->receive_dropping_id);
                StockMovesPerlengkapan::push($model->receive_dropping_id);
            }
            catch (Exception $ex) {
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $model->doc_ref,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }

    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $param[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['doc_ref'])) {
            $criteria->addCondition('doc_ref LIKE :doc_ref');
            $param[':doc_ref'] = '%' . $_POST['doc_ref'] . '%';
        }
        if (isset($_POST['note'])) {
            $criteria->addCondition('note LIKE :note');
            $param[':note'] = '%' . $_POST['note'] . '%';
        }
        if (isset($_POST['store'])) {
            $criteria->addCondition('store LIKE :store');
            $param[':store'] = '%' . $_POST['store'] . '%';
        }
        if (isset($_POST['store_pengirim'])) {
            $criteria->addCondition('store_pengirim LIKE :store_pengirim');
            $param[':store_pengirim'] = '%' . $_POST['store_pengirim'] . '%';
        }
        if (isset($_POST['lunas']) && $_POST['lunas'] != 'all') {
            $criteria->addCondition('lunas = :lunas');
            $param[':lunas'] = $_POST['lunas'];
        }
        if (isset($_POST['up']) && $_POST['up'] != 'all') {
            $criteria->addCondition('up = :up');
            $param[':up'] = $_POST['up'];
        }
        
            /*
             * Filter saat menampilkan Menu Receive Dropping
             * hanya menampilkan :
             * - Data Receive yang dibuat oleh cabang bersangkutan
             * - Data Receive yang dari cabang penerima Dropping
             */
        $criteria->addCondition('store = :branch OR (store_pengirim = :branch AND lunas != :draft)');
        $param[':branch'] = STOREID;
        $param[':draft'] = PR_DRAFT;
        
        $criteria->order = 'tgl DESC, tdate DESC';
        
        $criteria->params = $param;
        $model = ReceiveDropping::model()->findAll($criteria);
        $total = ReceiveDropping::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}