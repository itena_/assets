<?php
class PelunasanPiutangController extends GxController
{
	public function actionCreate()
	{
		if (!Yii::app()->request->isAjaxRequest)
			$this->redirect(url('/'));
		if (isset($_POST) && !empty($_POST)) {
			$msg = "Data gagal disimpan.";
			$status = false;
			$detils = CJSON::decode($_POST['detil']);
			$is_new = $_POST['mode'] == 0;
			app()->db->autoCommit = false;
			$transaction = Yii::app()->db->beginTransaction();
			try {
				$model = $is_new ? new PelunasanPiutang : $this->loadModel($_POST['id'], 'PelunasanPiutang');
				if ($is_new && ($model == null)) {
					throw new Exception(t('save.model.fail', 'app', array('{model}' => 'PelunasanPiutang')) .
						"Fatal error, record not found.");
				}
				if ($is_new) {
					$ref = new Reference();
					$docref = $ref->get_next_reference(PELUNASANPIUTANG);
				} else {
					$docref = $model->doc_ref;
					PelunasanPiutangDetil::model()->deleteAll('pelunasan_piutang_id = :pelunasan_piutang_id',
						array(':pelunasan_piutang_id' => $model->pelunasan_piutang_id));
					$type = PELUNASANPIUTANG;
					$type_no = $model->pelunasan_piutang_id;
					$this->delete_bank_trans($type, $type_no);
					$this->delete_gl_trans($type, $type_no);
				}
				foreach ($_POST as $k => $v) {
					if (is_angka($v)) $v = get_number($v);
					$_POST['PelunasanPiutang'][$k] = $v;
				}
				$_POST['PelunasanPiutang']['doc_ref'] = $docref;
				$model->attributes = $_POST['PelunasanPiutang'];
//				-----------------------------------------
//				INI PUNYANYA HUTANG
//				$balance = BankTrans::get_balance($_POST['bank_id'], $_POST['tgl']);
//				if (($balance - $_POST['PelunasanPiutang']['total']) < 0) {
//					throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Cash')) . "Insufficient funds");
//				}
//				-----------------------------------------
				if (!$model->save()) {
					throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Piutang')) . CHtml::errorSummary($model));
				}
				foreach ($detils as $detil) {
					$pelunasan_detil = new PelunasanPiutangDetil;
					$_POST['PelunasanPiutangDetil']['kas_dibayar'] = get_number($detil['kas_dibayar']);
					$_POST['PelunasanPiutangDetil']['no_faktur'] = $detil['no_faktur'];
					$_POST['PelunasanPiutangDetil']['transfer_item_id'] = $detil['transfer_item_id'];
					$_POST['PelunasanPiutangDetil']['sisa'] = get_number($detil['sisa']);
					$_POST['PelunasanPiutangDetil']['pelunasan_piutang_id'] = $model->pelunasan_piutang_id;
					$pelunasan_detil->attributes = $_POST['PelunasanPiutangDetil'];
					if (!$pelunasan_detil->save())
						throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Detail Piutang')) . CHtml::errorSummary($pelunasan_detil));
					if ($pelunasan_detil->sisa == 0) {
						$transferItem = $pelunasan_detil->transferItem;
						$transferItem->lunas = 1;
						if (!$transferItem->save())
							throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Purchase')) . CHtml::errorSummary($transferItem));
					}
				}
				$gl = new GL();
				$gl->add_gl(PELUNASANPIUTANG, $model->pelunasan_piutang_id, $model->tgl, $docref, $model->supplier->account_code,
					"Pelunasan Hutang", "Pelunasan Piutang", $model->total, 1, $model->store);
				$gl->add_gl(PELUNASANPIUTANG, $model->pelunasan_piutang_id, $model->tgl, $docref, $model->bank->account_code,
					"Pelunasan Hutang", "Pelunasan Piutang", -$model->total, 0, $model->store);
				$gl->add_bank_trans(PELUNASANPIUTANG, $model->pelunasan_piutang_id, $model->bank_id, $docref, $model->tgl,
					-$model->total, $model->user_id, $model->store);
				$gl->validate();
				if ($is_new) {
					$ref->save(PELUNASANPIUTANG, $model->pelunasan_piutang_id, $docref);
				}
				$transaction->commit();
				$msg = t('save.success', 'app');
				$status = true;
				/* if(PUSH_PUSAT){
                    
                    U::runCommand('checkdata', 'gltrans', '--tno='.$model->pelunasan_piutang_id , 'pp_gl_tno'.$model->pelunasan_piutang_id.'.log');

                    U::runCommand('checkdata', 'banktrans', '--tno='.$model->pelunasan_piutang_id,  'pp_bt_tno'.$model->pelunasan_piutang_id.'.log');
                    
                    U::runCommand('checkdata', 'ref', '--tno='.$model->pelunasan_piutang_id,  'pp_ref_tno'.$model->pelunasan_piutang_id.'.log');
                } */
			} catch (Exception $ex) {
				$transaction->rollback();
				$status = false;
				$msg = $ex->getMessage();
			}
			app()->db->autoCommit = true;
			echo CJSON::encode(array(
				'success' => $status,
				'msg' => $msg
			));
		}
	}
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id, 'PelunasanPiutang');
		if (isset($_POST) && !empty($_POST)) {
			foreach ($_POST as $k => $v) {
				if (is_angka($v)) $v = get_number($v);
				$_POST['PelunasanPiutang'][$k] = $v;
			}
			$msg = "Data gagal disimpan";
			$model->attributes = $_POST['PelunasanPiutang'];
			if ($model->save()) {
				$status = true;
				$msg = "Data berhasil di simpan dengan id " . $model->pelunasan_piutang_id;
			} else {
				$msg .= " " . implode(", ", $model->getErrors());
				$status = false;
			}
			if (Yii::app()->request->isAjaxRequest) {
				echo CJSON::encode(array(
					'success' => $status,
					'msg' => $msg
				));
				Yii::app()->end();
			} else {
				$this->redirect(array('view', 'id' => $model->pelunasan_piutang_id));
			}
		}
	}
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$msg = 'Data berhasil dihapus.';
			$status = true;
			try {
				$this->loadModel($id, 'PelunasanPiutang')->delete();
			} catch (Exception $ex) {
				$status = false;
				$msg = $ex;
			}
			echo CJSON::encode(array(
				'success' => $status,
				'msg' => $msg));
			Yii::app()->end();
		} else
			throw new CHttpException(400,
				Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
	}
	public function actionIndex()
	{
		if (isset($_POST['limit'])) {
			$limit = $_POST['limit'];
		} else {
			$limit = 20;
		}
		if (isset($_POST['start'])) {
			$start = $_POST['start'];
		} else {
			$start = 0;
		}
		$criteria = new CDbCriteria();
		if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
			(isset($_POST['limit']) && isset($_POST['start']))
		) {
			$criteria->limit = $limit;
			$criteria->offset = $start;
		}
		$criteria->addCondition("up = 0 AND DATE(tgl) = :tgl");
		$criteria->params = array(':tgl' => $_POST['tgl']);
		$model = PelunasanPiutang::model()->findAll($criteria);
		$total = PelunasanPiutang::model()->count($criteria);
		$this->renderJson($model, $total);
	}
}