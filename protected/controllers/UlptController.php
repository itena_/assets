<?php
class UlptController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                Ulpt::create_ulpt(new Ulpt(), $_POST['salestrans_id'], $_POST['tgl'],
                    get_number($_POST['amount']), $_POST['bank_id'], $_POST['card_id'], $_POST['note']);
//                $model = new Ulpt;
//                $ref = new Reference();
//                $docref = $ref->get_next_reference(ULPT);
//                foreach ($_POST as $k => $v) {
//                    if (is_angka($v)) {
//                        $v = get_number($v);
//                    }
//                    $_POST['Ulpt'][$k] = $v;
//                }
//                $_POST['Ulpt']['store'] = STOREID;
//                $_POST['Ulpt']['tipe'] = TIPE_ULPT;
//                $_POST['Ulpt']['doc_ref'] = $docref;
//                $model->attributes = $_POST['Ulpt'];
//                if (!$model->save()) {
//                    throw new Exception(t('save.model.fail', 'app',
//                            array('{model}' => 'ULPT')) . CHtml::errorSummary($model));
//                }
//                $ref->save(ULPT, $model->ulpt_id, $docref);
//                $gl = new GL();
//                $bank = Bank::model()->findByPk($model->bank_id);
//                $gl->add_gl(ULPT, $model->ulpt_id, $model->tgl, $docref, $bank->account_code,
//                    "ULPT", "ULPT", $model->amount, 0, $model->store);
//                if($gl->is_bank_account($bank->account_code)){
//                    $gl->add_bank_trans(ULPT,$model->ulpt_id, $model->bank_id, $docref,  $model->tgl,
//                        $model->amount, $model->id_user,  $model->store);
//                }
//                $gl->add_gl(ULPT, $model->ulpt_id, $model->tgl, $docref, COA_ULPT,
//                    "ULPT", "ULPT", -$model->amount, 1, $model->store);
//                $gl->validate();
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        /** @var $model Ulpt */
        $model = $this->loadModel($id, 'Ulpt');
        if (isset($_POST) && !empty($_POST)) {
            if (Tender::is_exist($_POST['tgl'])) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'ULPT revision not allowed.<br>Tender Declaration already created'
                ));
                Yii::app()->end();
            }
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                if ($model == null) {
                    throw new Exception("ULPT tidak ditemukan!");
                }
//                $this->delete_refs(ULPT,$model->ulpt_id);
                $this->delete_bank_trans(ULPT, $model->ulpt_id);
                $this->delete_gl_trans(ULPT, $model->ulpt_id);
                Ulpt::create_ulpt($model, $_POST['salestrans_id'], $_POST['tgl'],
                    get_number($_POST['amount']), $_POST['bank_id'], $_POST['card_id'], $_POST['note']);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Ulpt')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionPrint()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $sls = UlptView::model()->findByPk($_POST['id']);
            $prt = new PrintULPT($sls);
            echo CJSON::encode(array(
                'success' => $sls != null,
                'msg' => $prt->buildTxt()
            ));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        if (isset($_POST['status'])) {
            switch ($_POST['status']) {
                case 1 :
                    $criteria->addCondition('lunas = 0');
                    break;
                case 2 :
                    $criteria->addCondition('lunas = 1');
                    break;
            }
        }
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->order = 'doc_ref DESC, tgl DESC';
        
        $model = UlptView::model()->findAll($criteria);
        $total = UlptView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}