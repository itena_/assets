<?php

class RacikanDetailsController extends GxController {

    public function actionIndex() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }

        $criteria = new CDbCriteria();
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        if ((isset($_POST['racikan_id']))){
            $criteria->addCondition("racikan_id = '".$_POST['racikan_id']."'");
        }
        
        $model = RacikanDetails::model()->findAll($criteria);
        $total = RacikanDetails::model()->count($criteria);

        $this->renderJson($model, $total);
    }

}
