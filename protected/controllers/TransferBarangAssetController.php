<?php
Yii::import('application.components.Reference');
Yii::import('application.components.U');
Yii::import('application.components.GL');
class TransferBarangAssetController extends GxController {
	public function actionCreateIn()
    {
        if (!Yii::app()->request->isAjaxRequest) 
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new TransferBarangAsset;
                $ref = new Reference();
                $docref = $ref->get_next_reference(SUPPIN_ASSETS);
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['TransferBarangAsset'][$k] = $v;
                }
//                $new_details = array();
//                foreach ($detils as $detil) {
//                    $new_details[] = $detil;
//                }
                $_POST['TransferBarangAsset']['doc_ref'] = $docref;
                $_POST['TransferBarangAsset']['user_id'] = app()->user->getId();
                $model->attributes = $_POST['TransferBarangAsset'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive Barang Asset')) . CHtml::errorSummary($model));
                $gl = new GL();
                $tipe_beli = "supplier asset " . $model->supplier->supplier_name;
                $coa_hutang = $model->supplier->account_code;
                foreach ($detils as $detil) {
                    $item_details = new TransferBarangAssetDetails;
                    $_POST['TransferBarangAssetDetails']['barang_asset_id'] = $detil['barang_asset_id'];
                    $_POST['TransferBarangAssetDetails']['qty'] = get_number($detil['qty']);
                    $_POST['TransferBarangAssetDetails']['price'] = get_number($detil['price']);
                    $_POST['TransferBarangAssetDetails']['bruto'] = get_number($detil['bruto']);
                    $_POST['TransferBarangAssetDetails']['vat'] = get_number($detil['vat']);
                    $_POST['TransferBarangAssetDetails']['vatrp'] = get_number($detil['vatrp']);
                    $_POST['TransferBarangAssetDetails']['total'] = get_number($detil['total']);
                    $_POST['TransferBarangAssetDetails']['transfer_asset_id'] = $model->transfer_asset_id;
                    $item_details->attributes = $_POST['TransferBarangAssetDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive item detail')) . CHtml::errorSummary($item_details));
                    $total = $item_details->total;
                    if ($item_details->total != 0) {
                        $gl->add_gl_asset(SUPPIN_ASSETS, $model->transfer_asset_id, $model->tgl, $docref, $item_details->barangAsset->groupAsset->accountCode->account_code,
                            "Purchase $tipe_beli", "Purchase $tipe_beli", $item_details->total, $tipe_beli == 'CASH' ? 1 : 0);
                        $gl->add_gl_asset(SUPPIN_ASSETS, $model->transfer_asset_id, $model->tgl, $docref, $coa_hutang,
                            "Purchase $tipe_beli", "Purchase $tipe_beli", -$item_details->total, 0);
                    }
                }
                $gl->validate();
                $ref->save(SUPPIN_ASSETS, $model->transfer_asset_id, $docref);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionCreateOut()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new TransferBarangAsset;
                $ref = new Reference();
                $docref = $ref->get_next_reference(SUPPOUT);
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = ($k == 'total' || $k == 'discrp' || $k == 'bruto' || $k == 'vat') ? -get_number($v) : get_number($v);
                    $_POST['TransferBarangAsset'][$k] = $v;
                }
                $_POST['TransferBarangAsset']['bruto'] = 0;
                $_POST['TransferBarangAsset']['total'] = 0;
                $_POST['TransferBarangAsset']['vat'] = 0;
                $_POST['TransferBarangAsset']['lunas'] = 1;
                $new_details = array();
                foreach ($detils as $detil) {
                    $BarangAsset = BarangAsset::model()->findByPk($detil['barang_asset_id']);
                    $qty = get_number($detil['qty']);
                    $harga_beli = Beli::model()->findByAttributes(array('barang_asset_id' => $BarangAsset->barang_asset_id, 'store' => STOREID));
                    if ($harga_beli == null) {
                        throw new Exception("Default purchase price not define, please contact accounting person");
                    }
                    $price = $harga_beli->price;
                    $vat = $harga_beli->tax != 0 ? round($harga_beli->tax / 100,2) : 0;
                    $bruto = round($price * $qty,2);
                    $vatrp = round($vat * $bruto,2);
                    $total = $bruto;
                    $detil['price'] = $price;
                    $detil['bruto'] = $bruto;
                    $detil['total'] = $total;
                    $detil['vat'] = $vat;
                    $detil['vatrp'] = $vatrp;
                    $_POST['TransferBarangAsset']['vat'] += $vatrp;
                    $_POST['TransferBarangAsset']['bruto'] += $bruto;
                    $_POST['TransferBarangAsset']['total'] += $total;
                    $new_details[] = $detil;
                }
                $_POST['TransferBarangAsset']['vat'] = -$_POST['TransferBarangAsset']['vat'];
                $_POST['TransferBarangAsset']['bruto'] = -$_POST['TransferBarangAsset']['bruto'];
                $_POST['TransferBarangAsset']['total'] = -$_POST['TransferBarangAsset']['total'];
                $_POST['TransferBarangAsset']['doc_ref'] = $docref;
                $model->attributes = $_POST['TransferBarangAsset'];
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Return item')) . CHtml::errorSummary($model));
                /* @var $itemOut TransferBarangAsset */
                $itemOut = TransferBarangAsset::model()->find("doc_ref = :doc_ref", array(":doc_ref" => $model->doc_ref_other));
                if($itemOut == null){
                    throw new Exception("Purchase number not found.");
                }elseif($itemOut->total < abs($model->total)){
                    throw new Exception("Purchase value is smaller than the purchase returns.");
                }elseif($itemOut->total == abs($model->total)){
                    $itemOut->lunas = 1;
                    if (!$itemOut->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Purchase')) . CHtml::errorSummary($itemOut));
                    }
                }
                $gl = new GL();
                $coa_hutang = "";
                $tipe_beli = "";
                if ($model->supplier_id == null) {
                    $tipe_beli = "CASH";
                    $bank = Bank::get_bank_cash();
                    $coa_hutang = $bank->account_code;
                } else {
                    $tipe_beli = "supplier " . $model->supplier->supplier_name;
                    $coa_hutang = $model->supplier->account_code;
                }
                foreach ($new_details as $detil) {
                    $item_details = new TransferBarangAssetDetails;
                    $_POST['TransferBarangAssetDetails']['barang_asset_id'] = $detil['barang_asset_id'];
                    $_POST['TransferBarangAssetDetails']['qty'] = -get_number($detil['qty']);
                    $_POST['TransferBarangAssetDetails']['transfer_asset_id'] = $model->transfer_asset_id;
                    $_POST['TransferBarangAssetDetails']['price'] = get_number($detil['price']);
                    $_POST['TransferBarangAssetDetails']['bruto'] = -get_number($detil['bruto']);
//                    $_POST['TransferBarangAssetDetails']['disc'] = get_number($detil['disc']);
//                    $_POST['TransferBarangAssetDetails']['discrp'] = -get_number($detil['discrp']);
                    $_POST['TransferBarangAssetDetails']['total'] = -get_number($detil['total']);
                    $_POST['TransferBarangAssetDetails']['vat'] = get_number($detil['vat']);
                    $_POST['TransferBarangAssetDetails']['vatrp'] = -get_number($detil['vatrp']);
//                    $_POST['TransferBarangAssetDetails']['disc1'] = get_number($detil['disc1']);
//                    $_POST['TransferBarangAssetDetails']['discrp1'] = -get_number($detil['discrp1']);
//                    $_POST['TransferBarangAssetDetails']['total_pot'] = -get_number($detil['total_pot']);
                    $item_details->attributes = $_POST['TransferBarangAssetDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Return item detail')) . CHtml::errorSummary($item_details));
                    if ($item_details->barang->grup->kategori->is_have_stock()) {
                        U::add_stock_moves(SUPPOUT, $model->transfer_asset_id, $model->tgl,
                            $item_details->barang_asset_id, $item_details->qty, $model->doc_ref,
                            $item_details->barang->get_cost($model->store), $model->store);
                    }
                    //GL Persediaan
                    //          Hutang / Kas
                    if ($item_details->total != 0) {
                        $gl->add_gl_asset(SUPPOUT, $model->transfer_asset_id, $model->tgl, $docref, $item_details->barang->tipeBarang->coa,
                            "Return Supplier $tipe_beli", "Return Supplier $tipe_beli", $item_details->total, $tipe_beli == 'CASH' ? 1 : 0);
                        $gl->add_gl_asset(SUPPOUT, $model->transfer_asset_id, $model->tgl, $docref, $coa_hutang,
                            "Return Supplier $tipe_beli", "Return Supplier $tipe_beli", -$item_details->total, 0);
                    }
                }
                $gl->validate();
                $ref->save(SUPPOUT, $model->transfer_asset_id, $docref);
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionIndexIn()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("type_ = 0 AND DATE(tgl) = :tgl");
        $criteria->params = array(':tgl' => $_POST['tgl']);
        $model = TransferBarangAsset::model()->findAll($criteria);
        $total = TransferBarangAsset::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndexOut()
    {
        $criteria = new CDbCriteria();
        $criteria->select = "transfer_asset_id,tgl,doc_ref,note,tdate,doc_ref_other,
        user_id,type_,store,supplier_id,-total total,disc,-discrp discrp,-bruto bruto,-vat vat";
        $criteria->addCondition("type_ = 1 AND DATE(tgl) = :tgl");
        $criteria->params = array(':tgl' => $_POST['tgl']);
        $model = TransferBarangAsset::model()->findAll($criteria);
        $total = TransferBarangAsset::model()->count($criteria);
        $this->renderJson($model, $total);
    }

}