<?php
/**
 * Created by PhpStorm.
 * User: LENOVO
 * Date: 07/01/2015
 * Time: 16:20
 */
Yii::import('application.modules.projection.models.Account');


class ChartController extends GxController
{
    public function actionCustAtt()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
//        $_POST['from'] = '2014-10-23';
//        $_POST['to'] = '2014-12-31';
//        $_POST['store'] = 'SUB01';
        if (isset($_POST) && !empty($_POST)) {
            $where = "";
            $param = array(':from' => $_POST['from'], ':to' => $_POST['to']);
            if ($_POST['store'] != null) {
                $where = "AND ns.store = :store";
                $param[':store'] = $_POST['store'];
            }
            $comm = app()->db->createCommand("
SELECT @row_number:=@row_number+1 AS x, b.* FROM (SELECT DATE_FORMAT(a.Date,'%d %b %y') label,COUNT(DISTINCT(ns.customer_id)) y
from (
    select CURDATE() - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date
    from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
    cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
    cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
) a LEFT JOIN nscc_salestrans AS ns ON a.Date = ns.tgl $where
WHERE a.Date >= :from AND a.Date <= :to GROUP BY a.Date
ORDER BY a.Date) b,(SELECT @row_number:=0) AS t");
//            $comm->setFetchMode(PDO::FETCH_OBJ);
            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('custatt', array(
                'data' => $data,
                'chart_title' => sql2date($_POST['from']) . " - " . sql2date($_POST['to'])
            ));
        }
    }
    public function actionNewCust()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
//        $_POST['from'] = '2014-10-23';
//        $_POST['to'] = '2014-12-31';
//        $_POST['store'] = 'SUB01';
        if (isset($_POST) && !empty($_POST)) {
            $where = "";
            $param = array(':from' => $_POST['from'], ':to' => $_POST['to']);
            if ($_POST['store'] != null) {
                $where = "AND nc.store = :store";
                $param[':store'] = $_POST['store'];
            }
            $comm = app()->db->createCommand("
SELECT @row_number:=@row_number+1 AS x, b.* FROM (select DATE_FORMAT(a.Date,'%d %b %y') label,COUNT(nc.awal) y
from (
    select CURDATE() - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date
    from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a
    cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b
    cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c
) a LEFT JOIN nscc_customers AS nc ON a.Date = DATE(nc.awal) $where
WHERE a.Date >= :from AND a.Date <= :to
GROUP BY a.Date
ORDER BY a.Date) b,(SELECT @row_number:=0) AS t");
            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('newCust', array(
                'data' => $data,
                'chart_title' => sql2date($_POST['from']) . " - " . sql2date($_POST['to'])
            ));
        }
    }
    public function actionSalesGrup()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $where = "";
            $param = array(':from' => $_POST['from'], ':to' => $_POST['to']);
            if ($_POST['store'] != null) {
                $where = "AND ns.store = :store";
                $param[':store'] = $_POST['store'];
            }
            $comm = app()->db->createCommand("
            SELECT ng.nama_grup indexLabel,ng.nama_grup legendText,Sum(nsd.total) AS y
        FROM nscc_grup AS ng
        INNER JOIN nscc_barang AS nb ON nb.grup_id = ng.grup_id
        INNER JOIN nscc_salestrans_details AS nsd ON nsd.barang_id = nb.barang_id
        INNER JOIN nscc_salestrans AS ns ON nsd.salestrans_id = ns.salestrans_id
        WHERE ns.tgl >= :from AND ns.tgl <= :to $where
        GROUP BY ng.nama_grup");
//            $comm->setFetchMode(PDO::FETCH_OBJ);
            $array = $comm->queryAll(true, $param);
            $total = array_sum(array_column($array, 'y'));
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('salesGrup', array(
                'data' => $data,
                'chart_title' => sql2date($_POST['from']) . " - " . sql2date($_POST['to']),
                'total' => $total
            ));
        }
    }
    public function actionTopCust()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $where = "";
            $limit = $_POST['limit'];
            $param = array(
                ':from' => $_POST['from'],
                ':to' => $_POST['to']
            );
            if ($_POST['store'] != null) {
                $where = "AND ns.store = :store";
                $param[':store'] = $_POST['store'];
            }
            $comm = app()->db->createCommand("
            SELECT nc.nama_customer label,Sum(ns.total) y
            FROM nscc_salestrans AS ns
            INNER JOIN nscc_customers AS nc ON ns.customer_id = nc.customer_id
            WHERE ns.tgl >= :from AND ns.tgl <= :to $where
            GROUP BY nc.nama_customer
            ORDER BY SUM(ns.total) DESC
            LIMIT $limit");
            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('topcust', array(
                'data' => $data,
                'chart_title' => sql2date($_POST['from'], 'dd MMM yy') . " - " . sql2date($_POST['to'], 'dd MMM yy'),
                'limit' => $limit
            ));
        }
    }
    public function actionTopSalesGrup()
    {
        if (Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $where = $grup = "";
            $limit = $_POST['limit'];
            $param = array(
                ':from' => $_POST['from'],
                ':to' => $_POST['to']
            );
            if ($_POST['grup_id'] != null) {
                $grup = "AND nb.grup_id = :grup_id";
                $param[':grup_id'] = $_POST['grup_id'];
            }
            if ($_POST['store'] != null) {
                $where = "AND ns.store = :store";
                $param[':store'] = $_POST['store'];
            }
            $comm = app()->db->createCommand("
            SELECT nb.kode_barang label,SUM(nsd.qty) y
            FROM nscc_salestrans AS ns
            INNER JOIN nscc_salestrans_details AS nsd ON nsd.salestrans_id = ns.salestrans_id
            INNER JOIN nscc_barang AS nb ON nsd.barang_id = nb.barang_id
            WHERE ns.tgl >= :from AND ns.tgl <= :to $grup $where
            GROUP BY nb.kode_barang
            ORDER BY SUM(nsd.qty) DESC
            LIMIT $limit");
            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            if ($_POST['grup_id'] != null) {
                $grup = Grup::model()->findByPk($_POST['grup_id']);
                $grup_name = $grup->nama_grup;
            } else {
                $grup_name = "All Group";
            }
            $this->render('topsalesgrup', array(
                'data' => $data,
                'chart_title' => sql2date($_POST['from'], 'dd MMM yy') . " - " . sql2date($_POST['to'], 'dd MMM yy'),
                'limit' => $limit,
                'grup_name' => $grup_name
            ));
        }
    }




    //////////////////////////////////PROJECTION

    public function getAccountCode($id)
    {
        //$user_id = Yii::app()->user->getId();
        //$user = Users::model()->findByPk( $user_id );
        //$businessunit_id = $user->businessunit_id;
        $businessunit_id = $_COOKIE['businessunitid'];

        $query = "select account_code, account_name from account
                  where account_id = '".$id."' 
                  AND businessunit_id = '$businessunit_id'
                  limit 1";

        $list= Yii::app()->db->createCommand($query)->queryAll();

        $rs=array();
        foreach($list as $item)
        {
            $rs[0]=$item['account_code'];
            $rs[1]=$item['account_name'];
        }
        return $rs;
    }

    public function actionTransaction()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            return;
        }
        if (isset($_POST) && !empty($_POST))
        {

            //$user_id = Yii::app()->user->getId();
            //$user = Users::model()->findByPk( $user_id );
            //$businessunit_id = $user->businessunit_id;
            $businessunit_id = $_COOKIE['businessunitid'];

            $query = "";
            $where = "";
            $wheredate = "";
            $comm = "";
            $param = array();
            $charttitle = "";
            $charttype = $_POST['type'];

            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $outlet = $_POST['outlet'];
            $grup = $_POST['groupproduk'];
            $produk = $_POST['produk'];
            $ShowBy = $_POST['showby'];

            $ShowByRpt = "";

            if ($ShowBy == "Outlet")
            {
                $charttitle = $ShowBy." (".$outlet.")";
            }
            if ($ShowBy == "GroupProduct")
            {
                $charttitle = $ShowBy." (".$grup.")";
            }
            if ($ShowBy == "Product")
            {
                $charttitle = $ShowBy." (".$produk.")";
            }
            /*else
            {
                $charttitle = "All";
            }*/


            $param = array(':from' => $from, ':to' => $to);

            if ($ShowBy == "Outlet")
            {
                $where = "AND kodeoutlet = :outlet ";
                $param[':outlet'] = $outlet;
            }
            if ($ShowBy == "GroupProduct") {
                $where = "AND kodegroup = :kodegroup ";
                $param[':kodegroup'] = $grup;
            }
            if ($ShowBy == "Product") {
                $where = "AND kodeproduk = :kodeproduk ";
                $param[':kodeproduk'] = $produk;
            }

            /*if ($from != undefined || $to != undefined) {
                $comm = Yii::app()->dbprojection
                    ->createCommand("select * from transaksi_view
                                    WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to
                                    order by docref, tdate DESC ");

            } else {
                $comm = Yii::app()->dbprojection->createCommand("select * from transaksi_view
                                    WHERE kodeoutlet = '$outlet'
                                    order by docref, tdate DESC ");
            }*/

            $comm = Yii::app()->db
                ->createCommand("select DATE_FORMAT(tdate, '%b %Y') as label, SUM(salesrp) as y from transaksi_detail_view
                                WHERE DATE(tdate) >= :from 
                                AND DATE(tdate) <= :to AND flag = '1'
                                $where
                                AND businessunit_id = '$businessunit_id'
                                group by MONTH(tdate)
                                order by tdate ASC ");




            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('transaksi', array(
                'data' => $data,
                'chart_title' => $charttitle,
                'chart_type' => $charttype,
            ));
        }
    }

    public function actionTransactionOverride()
    {
        if (Yii::app()->request->isAjaxRequest)
        {
            return;
        }
        if (isset($_POST) && !empty($_POST))
        {

            //$user_id = Yii::app()->user->getId();
            //$user = Users::model()->findByPk( $user_id );
            //$businessunit_id = $user->businessunit_id;
            $businessunit_id = $_COOKIE['businessunitid'];

            $query = "";
            $where = "";
            $wheredate = "";
            $comm = "";
            $param = array();
            $charttitle = "";
            $charttype = $_POST['type'];

            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            //$outlet = $_POST['outlet'];
            //$grup = $_POST['groupproduk'];
            //$produk = $_POST['produk'];
            //$ShowBy = $_POST['showby'];

            $ShowByRpt = "";

            /*if ($ShowBy == "Outlet")
            {
                $charttitle = $ShowBy." (".$outlet.")";
            }
            if ($ShowBy == "GroupProduct")
            {
                $charttitle = $ShowBy." (".$grup.")";
            }
            if ($ShowBy == "Product")
            {
                $charttitle = $ShowBy." (".$produk.")";
            }*/
            /*else
            {
                $charttitle = "All";
            }*/


            $param = array(':from' => $from, ':to' => $to);

            /*if ($ShowBy == "Outlet")
            {
                $where = "AND kodeoutlet = :outlet ";
                $param[':outlet'] = $outlet;
            }
            if ($ShowBy == "GroupProduct") {
                $where = "AND kodegroup = :kodegroup ";
                $param[':kodegroup'] = $grup;
            }
            if ($ShowBy == "Product") {
                $where = "AND kodeproduk = :kodeproduk ";
                $param[':kodeproduk'] = $produk;
            }*/

            /*if ($from != undefined || $to != undefined) {
                $comm = Yii::app()->dbprojection
                    ->createCommand("select * from transaksi_view
                                    WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to
                                    order by docref, tdate DESC ");

            } else {
                $comm = Yii::app()->dbprojection->createCommand("select * from transaksi_view
                                    WHERE kodeoutlet = '$outlet'
                                    order by docref, tdate DESC ");
            }*/

            $comm = Yii::app()->db
                ->createCommand("select DATE_FORMAT(tdate, '%b %Y') as label, SUM(amount) as y from transaksi_override
                                WHERE DATE(tdate) >= :from 
                                AND DATE(tdate) <= :to AND flag = '1'
                                $where
                                AND businessunit_id = '$businessunit_id'
                                group by MONTH(tdate)
                                order by tdate ASC ");


            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('transaksiOverride', array(
                'data' => $data,
                'chart_title' => $charttitle,
                'chart_type' => $charttype,
            ));
        }
    }

    //Budget Plan
    public function actionBudget()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }

        if (isset($_POST) && !empty($_POST))
        {
            //$user_id = Yii::app()->user->getId();
            //$user = Users::model()->findByPk( $user_id );
            //$businessunit_id = $user->businessunit_id;
            $businessunit_id = $_COOKIE['businessunitid'];

            $where = "";
            $wheredate = "";
            $param = array();
            $charttitle = "";
            $charttype = $_POST['type'];

            $getAccount = $this->getAccountCode($_POST['account_id']);

            //$limit = $_POST['limit'];
            if ($_POST['tglfrom'] != 'undefined' || $_POST['tglto'] != 'undefined') {
                $wheredate = "WHERE DATE(b.tdate) >= :from AND DATE(b.tdate) <= :to";
                //$param[':account_id'] = $_POST['account_id'];
                $param = array(
                    ':from' => $_POST['tglfrom'],
                    ':to' => $_POST['tglto']
                );
            }
            if ($_POST['account_id'] != null)
            {
                if($wheredate == "")
                {
                    $where = "where account_id = :account_id";
                }
                else {
                    $where = "and account_id = :account_id";
                }

                $param[':account_id'] = $_POST['account_id'];
            }

            if($wheredate != "" || $where != "")
            {
                $business = "and businessunit_id = :businessunit_id";
                $param[':businessunit_id'] = $businessunit_id;
            }
            else{
                $business = "where businessunit_id = :businessunit_id";
                $param[':businessunit_id'] = $businessunit_id;
            }



            if($_POST['account_id'] != null && $_POST['tglfrom'] == 'undefined' && $_POST['tglto'] == 'undefined')
            {
                $comm = app()->db->createCommand("select DATE_FORMAT(tdate, \"%M %Y\") as label, SUM(amount) as y from budget_view b
                                        WHERE account_id = :account_id
                                        AND businessunit_id = '$businessunit_id'
                                        group by MONTH(tdate)
                                        ORDER BY tdate asc");
                $param[':account_id'] = $_POST['account_id'];

                $charttitle = $getAccount[0].' - '.$getAccount[1];
            }
            else
            {
                $comm = app()->db->createCommand("select CONCAT(b.account_code,' - ',b.account_name) as label, SUM(b.amount) as y from budget_view b
                                                $wheredate $where $business
                                                group by account_id
                                                ORDER BY account_id ASC");

                $charttitle = sql2date($_POST['tglfrom'], 'dd MMM yy') . " - " . sql2date($_POST['tglto'], 'dd MMM yy');
            }

            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('budget', array(
                'data' => $data,
                'chart_title' => $charttitle,
                'chart_type' => $charttype,
            ));
        }
    }

    //Budget Realization
    public function actionRealization()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }

        if (isset($_POST) && !empty($_POST)) {

            //$user_id = Yii::app()->user->getId();
            //$user = Users::model()->findByPk( $user_id );
            //$businessunit_id = $user->businessunit_id;
            $businessunit_id = $_COOKIE['businessunitid'];

            $where = "";
            $wheredate = "";
            $param = array();
            $charttitle = "";
            $charttype = $_POST['type'];

            $getAccount = $this->getAccountCode($_POST['account_id']);

            if ($_POST['tglfrom'] != 'undefined' || $_POST['tglto'] != 'undefined') {
                $wheredate = "WHERE DATE(b.tdate) >= :from AND DATE(b.tdate) <= :to";
                //$param[':account_id'] = $_POST['account_id'];
                $param = array(
                    ':from' => $_POST['tglfrom'],
                    ':to' => $_POST['tglto']
                );
            }
            if ($_POST['account_id'] != null)
            {
                if($wheredate == "")
                {
                    $where = "where account_id = :account_id";
                }
                else {
                    $where = "and account_id = :account_id";
                }

                $param[':account_id'] = $_POST['account_id'];
            }

            if($_POST['account_id'] != null && $_POST['tglfrom'] == 'undefined' && $_POST['tglto'] == 'undefined')
            {
                $comm = app()->db->createCommand("select DATE_FORMAT(tdate, \"%M %Y\") as label, SUM(amount) as y from realization_view b
                                        WHERE account_id = :account_id
                                        AND businessunit_id = '$businessunit_id'
                                        group by MONTH(tdate)
                                        ORDER BY tdate asc");
                $param[':account_id'] = $_POST['account_id'];

                $charttitle = $getAccount[0].' - '.$getAccount[1];
            }
            else
            {
                $comm = app()->db->createCommand("select CONCAT(b.account_code,' - ',b.account_name) as label, SUM(b.amount) as y from realization_view b
                                                $wheredate $where
                                                AND businessunit_id = '$businessunit_id'
                                                group by account_id
                                                ORDER BY account_id ASC");


                $charttitle = sql2date($_POST['tglfrom'], 'dd MMM yy') . " - " . sql2date($_POST['tglto'], 'dd MMM yy');
            }


            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('realization', array(
                'data' => $data,
                'chart_title' => $charttitle,
                'chart_type' => $charttype,
            ));
        }
    }

    //Budget Analysis
    public function actionAnalysis()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }

        if (isset($_POST) && !empty($_POST)) {

            //$user_id = Yii::app()->user->getId();
            //$user = Users::model()->findByPk( $user_id );
            //$businessunit_id = $user->businessunit_id;
            $businessunit_id = $_COOKIE['businessunitid'];

            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $month = $_POST['month'];
            $year = $_POST['year'];
            $accid = $_POST['account_id'];
            $showby = $_POST['showby'];
            $charttype = $_POST['type'];

            $join1 = DbCmd::instance()
                ->addSelect("g.account_id , sum(g.amount) as amount")
                ->addFrom("realization g")
                ->addCondition("g.businessunit_id = :businessunit_id")
                ->addGroup("g.account_id");

            $join2 = DbCmd::instance()
                ->addSelect("b.account_id , sum(b.amount) as amount")
                ->addFrom("budget b")
                ->addCondition("b.businessunit_id = :businessunit_id")
                ->addGroup("b.account_id");

            $commBudget = DbCmd::instance()
                ->addSelect("a.account_name as 'label', SUM(b.amount) as y")
                ->addFrom( "account a")
                ->addGroup("a.account_code");

            $commRealization = DbCmd::instance()
                ->addSelect("a.account_name as 'label', SUM(gl.amount) as y")
                ->addFrom( "account a")
                ->addGroup("a.account_code");

            $commAchievement = DbCmd::instance()
                ->addSelect("a.account_name as 'label', SUM(b.amount - gl.amount) as y")
                ->addFrom( "account a")
                ->addGroup("a.account_code");

            if($showby == 'Year')
            {
                if ($year != "") {
                    $join1->addCondition("YEAR(g.tdate) = :year");
                    $join2->addCondition("YEAR(b.tdate) = :year");

                    $commBudget->addParams([':year' => $year]);
                    $commRealization->addParams([':year' => $year]);
                    $commAchievement->addParams([':year' => $year]);
                }

                $charttitle = $year;
            }
            elseif ($showby == 'Month & Year')
            {
                if ($month != "")
                {
                    $join1->addCondition("MONTH(g.tdate) = :month");
                    $join2->addCondition("MONTH(b.tdate) = :month");

                    $commBudget->addParams([':month' => $month]);
                    $commRealization->addParams([':month' => $month]);
                    $commAchievement->addParams([':month' => $month]);
                }
                if ($year != "") {
                    $join1->addCondition("YEAR(g.tdate) = :year");
                    $join2->addCondition("YEAR(b.tdate) = :year");

                    $commBudget->addParams([':year' => $year]);
                    $commRealization->addParams([':year' => $year]);
                    $commAchievement->addParams([':year' => $year]);
                }
                $charttitle = $month." - ".$year;
            }
            elseif($showby == 'Account')
            {
                if ($accid != "") {

                    $account = Account::model()->findByAttributes(['account_id' => $accid]);
                    $acccode = $account->account_code;
                    $accname = $account->account_name;

                    $join1->addCondition("g.account_id = :account_id");
                    $join2->addCondition("b.account_id = :account_id");

                    $commBudget->addCondition("a.account_id = :account_id")
                        ->addParams([':account_id' => $accid]);
                    $commRealization->addCondition("a.account_id = :account_id")
                        ->addParams([':account_id' => $accid]);
                    $commAchievement->addCondition("a.account_id = :account_id")
                        ->addParams([':account_id' => $accid]);
                }
                $charttitle = $accid ? $accname : "All Account";
            }
            elseif($showby == 'Date')
            {
                if ($from != "")
                {
                    $join1->addCondition("DATE(g.tdate) >= :date_start");
                    $join2->addCondition("DATE(b.tdate) >= :date_start");

                    $commBudget->addParams([':date_start' => $from]);
                    $commRealization->addParams([':date_start' => $from]);
                    $commAchievement->addParams([':date_start' => $from]);
                }

                if ($to != "")
                {
                    $join1->addCondition("DATE(g.tdate) <= :date_end");
                    $join2->addCondition("DATE(b.tdate) <= :date_end");

                    $commBudget->addParams([':date_end' => $to]);
                    $commRealization->addParams([':date_end' => $to]);
                    $commAchievement->addParams([':date_end' => $to]);
                }

                $charttitle = sql2date($_POST['tglfrom'], 'dd MMM yy') . " - " . sql2date($_POST['tglto'], 'dd MMM yy');
            }

            $commBudget ->addLeftJoin($join1->setAs("gl"),"a.account_id = gl.account_id")
                ->addLeftJoin($join2->setAs("b"),"a.account_id = b.account_id")
                ->addCondition("a.businessunit_id = :businessunit_id")
                ->addParams([':businessunit_id' => $businessunit_id]);


            $commRealization ->addLeftJoin($join1->setAs("gl"),"a.account_id = gl.account_id")
                ->addLeftJoin($join2->setAs("b"),"a.account_id = b.account_id")
                ->addCondition("a.businessunit_id = :businessunit_id")
                ->addParams([':businessunit_id' => $businessunit_id]);

            $commAchievement ->addLeftJoin($join1->setAs("gl"),"a.account_id = gl.account_id")
                ->addLeftJoin($join2->setAs("b"),"a.account_id = b.account_id")
                ->addCondition("a.businessunit_id = :businessunit_id")
                ->addParams([':businessunit_id' => $businessunit_id]);

            /*$commBudget->setConnection(Yii::app()->db);
            $commRealization->setConnection(Yii::app()->db);
            $commAchievement->setConnection(Yii::app()->db);*/

            $arrayBudget = $commBudget->queryAll();
            $dataBudget = json_encode($arrayBudget, JSON_NUMERIC_CHECK);

            $arrayRealization = $commRealization->queryAll();
            $dataRealization = json_encode($arrayRealization, JSON_NUMERIC_CHECK);

            $arrayAchievement = $commAchievement->queryAll();
            $dataAchievement = json_encode($arrayAchievement, JSON_NUMERIC_CHECK);

            $this->layout = 'chart';
            $this->render('analysis', array(
                'dataBudget' => $dataBudget,
                'dataRealization' => $dataRealization,
                'dataAchievement' => $dataAchievement,
                'chart_title' => $charttitle,
                'chart_type' => $charttype,
            ));
        }
    }

    //Invesment Plan
    public function actionInvestment()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }

        if (isset($_POST) && !empty($_POST)) {

            //$user_id = Yii::app()->user->getId();
            //$user = Users::model()->findByPk( $user_id );
            //$businessunit_id = $user->businessunit_id;
            $businessunit_id = $_COOKIE['businessunitid'];

            $where = "";
            $wheredate = "";
            //$limit = $_POST['limit'];
            $param = array();
            $charttype = $_POST['type'];

            if ($_POST['tglfrom'] != 'undefined' || $_POST['tglto'] != 'undefined') {
                $wheredate = "WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to";
                //$param[':account_id'] = $_POST['account_id'];
                $param = array(
                    ':from' => $_POST['tglfrom'],
                    ':to' => $_POST['tglto']
                );
            }
            if ($_POST['account_id'] != null)
            {
                if($wheredate == "")
                {
                    $where = "where account_id = :account_id";
                }
                else {
                    $where = "and account_id = :account_id";
                }

                $param[':account_id'] = $_POST['account_id'];
            }
            /*$comm = app()->dbprojection->createCommand("select CONCAT(account_code,' - ',account_name) as label, SUM(total) as y from investasi_view
                                                $wheredate $where
                                                group by account_id
                                                ORDER BY account_id ASC");*/
            $comm = app()->db->createCommand("select name as label, SUM(total) as y from investasi_view
                                                            $wheredate $where
                                                            AND businessunit_id = '$businessunit_id'
                                                            group by account_id, name
                                                            ORDER BY account_id ASC");


            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('investment', array(
                'data' => $data,
                'chart_title' => sql2date($_POST['tglfrom'], 'dd MMM yy') . " - " . sql2date($_POST['tglto'], 'dd MMM yy'),
                'chart_type' => $charttype,
            ));
        }
    }

    //Invesment Realization
    public function actionInvestmentRealization()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }

        if (isset($_POST) && !empty($_POST)) {

            //$user_id = Yii::app()->user->getId();
            //$user = Users::model()->findByPk( $user_id );
            //$businessunit_id = $user->businessunit_id;
            $businessunit_id = $_COOKIE['businessunitid'];

            $where = "";
            $wheredate = "";
            //$limit = $_POST['limit'];
            $param = array();
            $charttype = $_POST['type'];

            if ($_POST['tglfrom'] != 'undefined' || $_POST['tglto'] != 'undefined') {
                $wheredate = "WHERE DATE(tdate) >= :from AND DATE(tdate) <= :to";
                //$param[':account_id'] = $_POST['account_id'];
                $param = array(
                    ':from' => $_POST['tglfrom'],
                    ':to' => $_POST['tglto']
                );
            }
            if ($_POST['account_id'] != null)
            {
                if($wheredate == "")
                {
                    $where = "where account_id = :account_id";
                }
                else {
                    $where = "and account_id = :account_id";
                }

                $param[':account_id'] = $_POST['account_id'];
            }
            /*$comm = app()->dbprojection->createCommand("select CONCAT(account_code,' - ',account_name) as label, SUM(total) as y from investasi_view
                                                $wheredate $where
                                                group by account_id
                                                ORDER BY account_id ASC");*/
            $comm = app()->db->createCommand("select name as label, SUM(total) as y from investasi_realisasi_view
                                                            $wheredate $where
                                                            AND businessunit_id = '$businessunit_id'
                                                            group by account_id, name
                                                            ORDER BY account_id ASC");


            $array = $comm->queryAll(true, $param);
            $data = json_encode($array, JSON_NUMERIC_CHECK);
            $this->layout = 'chart';
            $this->render('investmentRealization', array(
                'data' => $data,
                'chart_title' => sql2date($_POST['tglfrom'], 'dd MMM yy') . " - " . sql2date($_POST['tglto'], 'dd MMM yy'),
                'chart_type' => $charttype,
            ));
        }
    }

    //Investment Analysis
    public function actionInvestmentAnalysis()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }

        if (isset($_POST) && !empty($_POST)) {

            //$user_id = Yii::app()->user->getId();
            //$user = Users::model()->findByPk( $user_id );
            //$businessunit_id = $user->businessunit_id;
            $businessunit_id = $_COOKIE['businessunitid'];

            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $month = $_POST['month'];
            $year = $_POST['year'];
            $accid = $_POST['account_id'];
            $showby = $_POST['showby'];
            $charttype = $_POST['type'];

            $join1 = DbCmd::instance()
                ->addSelect("g.account_id , sum(g.total) as amount")
                ->addFrom("investasi_realisasi g")
                ->addCondition("g.businessunit_id = :businessunit_id")
                ->addGroup("g.account_id");

            $join2 = DbCmd::instance()
                ->addSelect("b.account_id , sum(b.total) as amount")
                ->addFrom("investasi b")
                ->addCondition("b.businessunit_id = :businessunit_id")
                ->addGroup("b.account_id");

            $commBudget = DbCmd::instance()
                ->addSelect("a.account_name as 'label', SUM(b.amount) as y")
                ->addFrom( "account a")
                ->addGroup("a.account_code");

            $commRealization = DbCmd::instance()
                ->addSelect("a.account_name as 'label', SUM(gl.amount) as y")
                ->addFrom( "account a")
                ->addGroup("a.account_code");

            $commAchievement = DbCmd::instance()
                ->addSelect("a.account_name as 'label', SUM(b.amount - gl.amount) as y")
                ->addFrom( "account a")
                ->addGroup("a.account_code");

            if($showby == 'Year')
            {
                if ($year != "") {
                    $join1->addCondition("YEAR(g.tdate) = :year");
                    $join2->addCondition("YEAR(b.tdate) = :year");

                    $commBudget->addParams([':year' => $year]);
                    $commRealization->addParams([':year' => $year]);
                    $commAchievement->addParams([':year' => $year]);
                }

                $charttitle = $year;
            }
            elseif ($showby == 'Month & Year')
            {
                if ($month != "")
                {
                    $join1->addCondition("MONTH(g.tdate) = :month");
                    $join2->addCondition("MONTH(b.tdate) = :month");

                    $commBudget->addParams([':month' => $month]);
                    $commRealization->addParams([':month' => $month]);
                    $commAchievement->addParams([':month' => $month]);
                }
                if ($year != "") {
                    $join1->addCondition("YEAR(g.tdate) = :year");
                    $join2->addCondition("YEAR(b.tdate) = :year");

                    $commBudget->addParams([':year' => $year]);
                    $commRealization->addParams([':year' => $year]);
                    $commAchievement->addParams([':year' => $year]);
                }
                $charttitle = $month." - ".$year;
            }
            elseif($showby == 'Account')
            {
                if ($accid != "") {

                    $account = Account::model()->findByAttributes(['account_id' => $accid]);
                    $acccode = $account->account_code;
                    $accname = $account->account_name;

                    $join1->addCondition("g.account_id = :account_id");
                    $join2->addCondition("b.account_id = :account_id");

                    $commBudget->addCondition("a.account_id = :account_id")
                        ->addParams([':account_id' => $accid]);
                    $commRealization->addCondition("a.account_id = :account_id")
                        ->addParams([':account_id' => $accid]);
                    $commAchievement->addCondition("a.account_id = :account_id")
                        ->addParams([':account_id' => $accid]);
                }
                $charttitle = $accid ? $accname : "All Account";
            }
            elseif($showby == 'Date')
            {
                if ($from != "")
                {
                    $join1->addCondition("DATE(g.tdate) >= :date_start");
                    $join2->addCondition("DATE(b.tdate) >= :date_start");

                    $commBudget->addParams([':date_start' => $from]);
                    $commRealization->addParams([':date_start' => $from]);
                    $commAchievement->addParams([':date_start' => $from]);
                }

                if ($to != "")
                {
                    $join1->addCondition("DATE(g.tdate) <= :date_end");
                    $join2->addCondition("DATE(b.tdate) <= :date_end");

                    $commBudget->addParams([':date_end' => $to]);
                    $commRealization->addParams([':date_end' => $to]);
                    $commAchievement->addParams([':date_end' => $to]);
                }

                $charttitle = sql2date($_POST['tglfrom'], 'dd MMM yy') . " - " . sql2date($_POST['tglto'], 'dd MMM yy');
            }

            $commBudget ->addLeftJoin($join1->setAs("gl"),"a.account_id = gl.account_id")
                ->addLeftJoin($join2->setAs("b"),"a.account_id = b.account_id")
                ->addCondition("a.businessunit_id = :businessunit_id")
                ->addParams([':businessunit_id' => $businessunit_id]);


            $commRealization ->addLeftJoin($join1->setAs("gl"),"a.account_id = gl.account_id")
                ->addLeftJoin($join2->setAs("b"),"a.account_id = b.account_id")
                ->addCondition("a.businessunit_id = :businessunit_id")
                ->addParams([':businessunit_id' => $businessunit_id]);

            $commAchievement ->addLeftJoin($join1->setAs("gl"),"a.account_id = gl.account_id")
                ->addLeftJoin($join2->setAs("b"),"a.account_id = b.account_id")
                ->addCondition("a.businessunit_id = :businessunit_id")
                ->addParams([':businessunit_id' => $businessunit_id]);

            /*$commBudget->setConnection(Yii::app()->db);
            $commRealization->setConnection(Yii::app()->db);
            $commAchievement->setConnection(Yii::app()->db);*/

            $arrayBudget = $commBudget->queryAll();
            $dataBudget = json_encode($arrayBudget, JSON_NUMERIC_CHECK);

            $arrayRealization = $commRealization->queryAll();
            $dataRealization = json_encode($arrayRealization, JSON_NUMERIC_CHECK);

            $arrayAchievement = $commAchievement->queryAll();
            $dataAchievement = json_encode($arrayAchievement, JSON_NUMERIC_CHECK);

            $this->layout = 'chart';
            $this->render('investmentAnalysis', array(
                'dataBudget' => $dataBudget,
                'dataRealization' => $dataRealization,
                'dataAchievement' => $dataAchievement,
                'chart_title' => $charttitle,
                'chart_type' => $charttype,
            ));
        }
    }
}