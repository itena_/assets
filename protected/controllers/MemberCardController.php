<?php

class MemberCardController extends GxController {

    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;

        if (isset($_POST) && !empty($_POST))
        {
            $is_new = $_POST['mode'] == 0;
            $member_id = $_POST['id'];
            $customer_id = $_POST['customer_id'];

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();

            try
            {
                if($_POST['name'] == "" || $_POST['no_card'] == ""|| $_POST['store'] == "")
                {
                    echo CJSON::encode(array('msg'=> "field not complete." ));
                    return;
                }

                $model = $is_new ? new MemberCard : $this->loadModel($member_id, "MemberCard");

                if ($is_new)
                {
                    //check no card sudah ada belum
                    $check = $this->Check($_POST['no_card'], $_POST['store'],PT_CARD);
                    if($check > 0)
                    {
                        $member = MemberCard::model()->findByAttributes(array('customer_id' => $customer_id));
                        $msg = strval($_POST['no_card']).$_POST['store']." already in database.";
                        $this->printCard($member,false,$msg);

                        return;
                    }

                    $model->customer_id = $customer_id;
                    $model->created_at = new CDbExpression('NOW()');
                    $model->update_at = new CDbExpression('NOW()');
                    $model->counter = $model->counter +1;
                }
                else
                {
                    $model->update_at = new CDbExpression('NOW()');
                }

                foreach($_POST as $k=>$v){
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['MemberCard'][$k] = $v;
                }

                $model->attributes = $_POST['MemberCard'];

                $model->business_unit = PT_CARD;
                $model->since = $_POST['since'];
                $model->valid = $_POST['valid'];
                $model->active = $_POST['active'];

                $msg = "Data berhasil di simpan.";
                $status = true;

                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'MemberCard')) . CHtml::errorSummary($model));

                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;

                $this->printCard($model,true,$msg);

                /*if ($is_new) {
                    $this->printCard($model);
                }*/
            }
            catch (Exception $ex)
            {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }

            Yii::app()->end();
        }
    }

    /*public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;

        if (isset($_POST) && !empty($_POST))
        {

            $is_new = $_POST['mode'] == 0;
            $member_id = $_POST['id'];
            $customer_id = $_POST['customer_id'];

            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();

            try
            {
                if($_POST['name'] == "" || $_POST['no_card'] == ""|| $_POST['store'] == "")
                {
                    echo CJSON::encode(array('msg'=> "field not complete." ));
                    return;
                }

                $model = $is_new ? new MemberCard : $this->loadModel($member_id, "MemberCard");

                if ($is_new)
                {
                    //check no card sudah ada belum
                    $check = $this->Check($_POST['no_card'], $_POST['store'],PT_NEGARA);
                    if($check > 0)
                    {
                        echo CJSON::encode(array('msg'=>strval($_POST['no_card']).$_POST['store']." sudah ada didatabase." ));
                        return;
                    }

                    $model->customer_id = $customer_id;
                    $model->created_at = new CDbExpression('NOW()');
                    $model->update_at = new CDbExpression('NOW()');
                    $model->counter = +1;
                }
                else
                {
                    $model->update_at = new CDbExpression('NOW()');
                }

                foreach($_POST as $k=>$v){
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['MemberCard'][$k] = $v;
                }

                $model->attributes = $_POST['MemberCard'];

                $model->business_unit = PT_NEGARA;
                $model->since = $_POST['since'];
                $model->valid = $_POST['valid'];
                $model->active = $_POST['active'];


                $msg = "Data berhasil di simpan.";
                $status = true;

                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'MemberCard')) . CHtml::errorSummary($model));

                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;

                if ($is_new) {
                    $this->printCard($model);
                }

            }
            catch (Exception $ex)
            {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }

            Yii::app()->end();
        }
    }*/

    //untuk check card sudah ada belum
    public function Check($nocard, $store, $bu)
    {
        $criteria = new CDbCriteria;
        $criteria->compare('no_card',$nocard);
        $criteria->compare('store',$store);
        $criteria->compare('business_unit',$bu);
        $model = MemberCard::model()->findAll($criteria);
        $count = count($model);

        return $count;
    }

    public function
    printCard($model,$status,$msg)
    {
        if ($model != null) {

            $yawal = date('Y', strtotime($model->since));
            $mawal = date('m', strtotime($model->since));
            $yakhir = date('y', strtotime($model->valid));

            echo CJSON::encode(array(
                'success' => $status,
                'MemberCard' => $model->getAttributes([
                    'membercard_id', 'no_card', 'store', 'name'
                ]),
                'yawal' => $yawal,
                'yakhir' => $yakhir,
                'mawal' => $mawal,
                'msg' => $msg
            ));
        } else {
            echo CJSON::encode(array(
                'success' => false,
                'msg' => 'Customer not valid'
            ));
        }
    }

    public function actionUpdateDataCard()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        $id = $_POST['id'];
        $model = $this->loadModel($id, "MemberCard");
        $model->counter = $model->counter +1;
        if (!$model->save())
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'MemberCard')) . CHtml::errorSummary($model));

    }

    public function actionReprintCard()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }

        $id = $_POST['id'];
        $model = $this->loadModel($id, "MemberCard");
        /*$model->counter = $model->counter +1;
        if (!$model->save())
            throw new Exception(t('save.model.fail', 'app', array('{model}' => 'MemberCard')) . CHtml::errorSummary($model));*/

        if ($model != null) {

            $yawal = date('Y', strtotime($model->since));
            $mawal = date('m', strtotime($model->since));
            $yakhir = date('y', strtotime($model->valid));

            echo CJSON::encode(array(
                'success' => true,
                'MemberCard' => $model->getAttributes([
                    'membercard_id', 'no_card', 'store', 'name'
                ]),
                'yawal' => $yawal,
                'yakhir' => $yakhir,
                'mawal' => $mawal,
                'msg' => 'Saved successfully'
            ));
        } else {
            echo CJSON::encode(array(
                'success' => false,
                'msg' => 'Customer not valid'
            ));
        }
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'MemberCard');
        $msg = "Data gagal disimpan";

        if ($model != null)
        {
            foreach($_POST as $k=>$v){
                if (is_angka($v)) $v = get_number($v);
                $_POST['MemberCard'][$k] = $v;
            }

            $model->attributes = $_POST['MemberCard'];

            if ($model->save())
            {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->membercard_id;
            }
            else
            {
                $msg .= " ".implode(", ", $model->getErrors());
                $status = false;
            }
        }

        if (Yii::app()->request->isAjaxRequest)
        {
            echo CJSON::encode(array(
                'success'=>$status,
                'msg'=>$msg
            ));
            Yii::app()->end();
        }
        else
        {
            $this->redirect(array('view', 'id' => $id));
        }

    }

public function actionDelete($id) {
if (Yii::app()->request->isPostRequest) {
$msg = 'Data berhasil dihapus.';
$status = true;
try {
$this->loadModel($id, 'MemberCard')->delete();
} catch (Exception $ex) {
$status = false;
$msg = $ex;
}
echo CJSON::encode(array(
'success' => $status,
'msg' => $msg));
Yii::app()->end();
} else
throw new CHttpException(400,
Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
}


public function actionIndex() {
if(isset($_POST['limit'])) {
$limit = $_POST['limit'];
} else {
$limit = 20;
}

if(isset($_POST['start'])){
$start = $_POST['start'];

} else {
$start = 0;
}

$criteria = new CDbCriteria();
if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
(isset($_POST['limit']) && isset($_POST['start']))) {
$criteria->limit = $limit;
$criteria->offset = $start;
}

    if (isset($_POST['no_card'])) {
        $no_card = $_POST['no_card'];
        $criteria->addCondition('no_card like :no_card');
        $param[':no_card'] = "%$no_card%";
    }
    if (isset($_POST['name'])) {
        $name = $_POST['name'];
        $criteria->addCondition('name like :name');
        $param[':name'] = "%$name%";
    }
    if (isset($_POST['category'])) {
        $category = $_POST['category'];
        $criteria->addCondition('category like :category');
        $param[':category'] = "%$category%";
    }
    if (isset($_POST['type'])) {
        $type = $_POST['type'];
        $criteria->addCondition('type like :type');
        $param[':type'] = "%$type%";
    }
    if (isset($_POST['active'])) {
        $active = $_POST['active'];
        $criteria->addCondition('active like :active');
        $param[':active'] = "%$active%";
    }

    if (isset($_POST['since'])) {
        $since = $_POST['since'];
        $criteria->addCondition('since = date(:since)');
        $param[':since'] = "$since";
    }
    if (isset($_POST['valid'])) {
        $valid = $_POST['valid'];
        $criteria->addCondition('valid = date(:valid)');
        $param[':valid'] = "$valid";
    }
    if (isset($_POST['created_at'])) {
        $created_at = $_POST['created_at'];
        $criteria->addCondition('created_at = date(:created_at)');
        $param[':created_at'] = "$created_at";
    }
    if (isset($_POST['update_at'])) {
        $update_at = $_POST['update_at'];
        $criteria->addCondition('update_at = date(:update_at)');
        $param[':update_at'] = "$update_at";
    }

    $criteria->addCondition('business_unit = :business_unit');
    $param[':business_unit'] = PT_CARD;

    $criteria->params = $param;
    $criteria->order = "no_card, store ";

$model = MemberCard::model()->findAll($criteria);
$total = MemberCard::model()->count($criteria);

$this->renderJson($model, $total);

}

}