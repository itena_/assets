<?php
class StatusCustController extends GxController
{
    public function actionCreate()
    {
        $model = new StatusCust;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['StatusCust'][$k] = $v;
            }
            $model->attributes = $_POST['StatusCust'];
            $msg = t('save.fail', 'app');
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id', 'app', array('{id}' => $model->status_cust_id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'StatusCust');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['StatusCust'][$k] = $v;
            }
            $msg = t('save.fail', 'app');
            $model->attributes = $_POST['StatusCust'];
            if ($model->save()) {
                $status = true;
                $msg = t('save.success.id', 'app', array('{id}' => $model->status_cust_id));
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->status_cust_id));
            }
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = StatusCust::model()->findAll($criteria);
        $total = StatusCust::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}