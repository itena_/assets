<?php

class BonusJualController extends GxController {

    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $barang_id = $_POST['barang_id'];
                $store = $_POST['store'];
                foreach ($detils as $detil) {
                    //if exists the update
                    $bonus_name_id = $detil['bonus_name_id'];
                    $model = BonusJual::model()->find("barang_id = '$barang_id' AND store = '$store' AND bonus_name_id = '$bonus_name_id'");
                    if ($model == null){
                        $model = new BonusJual;
                    }
                    $model->barang_id = $barang_id;
                    $model->bonus_name_id = $detil['bonus_name_id'];
                    $model->persen_bonus = $detil['persen_bonus'];
                    $model->store = $store;
                    if (!$model->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'BonusJual')) . CHtml::errorSummary($model));
                }
//                $auw->save();
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }

    public function actionUpdate($id) {
        $model = $this->loadModel($id, 'BonusJual');


        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v))
                    $v = get_number($v);
                $_POST['BonusJual'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['BonusJual'];

            if ($model->save()) {

                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->bonus_jual_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->bonus_jual_id));
            }
        }
    }

    public function actionDelete($id) {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'BonusJual')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400, Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    
    public function actionGetBonus() {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $tempbn = [];
            $temppb = [];
            $index = 0;
            $barang = $_POST['barang_id'];
            $bj = BarangJualBonus::model()->findAll("barang_id = '$barang'");
//            for ($index = 0; $index <= count($bj); $index++) {
//                $tempbn[] = $bj->bonus_name_id;
//                $temppb[] = $bj->persen_bonus;
//            }    
            if(count($bj) > 0){
                foreach ($bj as $b){
                    $tempbn[] = $b->bonus_name_id;
                    $temppb[] = $b->persen_bonus;
                    $index++;
                }
            } else {
                echo CJSON::encode(array(
                    'success' => 'false',
                    'msg' => 'Tidak ada data bonus.'
                ));
                Yii::app()->end();
            }
            
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;    
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        app()->db->autoCommit = true;
        echo CJSON::encode(array(
            'tempbn' => $tempbn,
            'temppb' => $temppb,
            'count' => $index,
            'msg' => $msg
        ));
        Yii::app()->end();
    }

    public function actionIndex() {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }

        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }

        $criteria = new CDbCriteria();
        $criteria->params = [];
        if ((isset($_POST['mode']) && $_POST['mode'] == 'grid') ||
                (isset($_POST['limit']) && isset($_POST['start']))) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        
        if (isset($_POST['bonus_name_id'])) {
            $criteria->addCondition("bonus_name_id = :bonus_name_id");
            $criteria->params[':bonus_name_id'] = $_POST['bonus_name_id'];
        }
        if (isset($_POST['store'])) {
            $criteria->addCondition("store = :store");
            $criteria->params[':store'] = $_POST['store'];
        }
        if (isset($_POST['header']) && $_POST['header'] == 1 ) {
            $criteria->select = 'bonus_name_id, store';
            $criteria->group = 'bonus_name_id, store';
        }
        
        $model = BarangJualBonus::model()->findAll($criteria);
        $total = BarangJualBonus::model()->count($criteria);

        $this->renderJson($model, $total);
    }

}
