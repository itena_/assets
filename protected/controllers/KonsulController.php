<?php
class KonsulController extends GxController
{
    public function actionCreate()
    {
        $x = @fopen(KONSUL_LOCK, "w");
        if (!flock($x, LOCK_EX | LOCK_NB)) {
            echo CJSON::encode(array(
                'success' => false,
                'id' => '',
                'msg' => "Transaksi belum bisa dilakukan, silahkan coba lagi beberapa saat."
            ));
            Yii::app()->end();
        }
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            if (Tender::is_exist($_POST['tgl'])) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Tender Declaration already created'
                ));
                fflush($x);            // flush output before releasing the lock
                flock($x, LOCK_UN);
                fclose($x);
                Yii::app()->end();
            }
            app()->db->autoCommit = false;
            $model = null;
            $diagnosa = CJSON::decode($_POST['diagnosa']);
            $detils = CJSON::decode($_POST['detil']);
            $transaction = Yii::app()->db->beginTransaction();
            try {
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Konsul'][$k] = $v;
                }
                if (isset($_POST['id_antrian'])) {
                    if ($_POST['id_antrian'] != '') {
                        $model = Konsul::model()->findByAttributes(['id_antrian' => $_POST['id_antrian']]);
                        $antri = AishaAntrian::model()->findByAttributes(array('id_antrian' => $_POST['id_antrian']));
                        if ($antri == null) {
                            throw new Exception('Antrian tindak ditemukan!');
                        }
                        $_POST['Konsul']['counter'] = $antri->counter;
                    }
                }
                if ($model == null) {
                    $model = new Konsul;
                    $model->konsul_id = $this->generate_uuid();
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(KONSUL);
                    $ref->save(KONSUL, $model->konsul_id, $docref);
                    $_POST['Konsul']['doc_ref'] = $docref;
                } else {
                    Diagnosa::model()->deleteAllByAttributes(['konsul_id' => $model->konsul_id]);
                    KonsulDetil::model()->deleteAllByAttributes(['konsul_id' => $model->konsul_id]);
                }
                if (isset($_POST['no_customer'])) {
                    if ($_POST['no_customer']) {
                        $customer = Customers::model()->findByAttributes(array('no_customer' => $_POST['no_customer']));
                        if ($customer == null) {
                            throw new Exception('Pasien tindak ditemukan!');
                        }
                        $_POST['Konsul']['customer_id'] = $customer->customer_id;
                    }
                }
                /** @var UserEmployee $emp */
                $emp = UserEmployee::model()->findByAttributes(array('id' => Yii::app()->user->getId()));
                if ($emp == null) {
                    throw new Exception('Dokter tidak ditemukan dengan login ini!');
                }
                $_POST['Konsul']['dokter_id'] = $emp->employee_id;
                $model->attributes = $_POST['Konsul'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Konsul')) . CHtml::errorSummary($model));
                }
                foreach ($diagnosa as $detil) {
                    $diag = new Diagnosa;
                    $_POST['Diagnosa']['diag'] = $detil['diag'];
                    $_POST['Diagnosa']['note_'] = $detil['note_'];
                    $_POST['Diagnosa']['konsul_id'] = $model->konsul_id;
                    $diag->attributes = $_POST['Diagnosa'];
                    if (!$diag->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Diagnosa')) . CHtml::errorSummary($diag));
                    }
                }
                foreach ($detils as $detil) {
                    $konsulDetil = new KonsulDetil;
                    $qty = get_number($detil['qty']);
                    $_POST['KonsulDetil']['barang_id'] = $detil['barang_id'];
                    $_POST['KonsulDetil']['qty'] = $qty;
                    $_POST['KonsulDetil']['price'] = get_number($detil['price']);
                    $_POST['KonsulDetil']['bruto'] = get_number($detil['bruto']);
                    $_POST['KonsulDetil']['ketpot'] = $detil['ketpot'];
                    $_POST['KonsulDetil']['konsul_id'] = $model->konsul_id;
                    $konsulDetil->attributes = $_POST['KonsulDetil'];
                    if (!$konsulDetil->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Konsul Detail')) . CHtml::errorSummary($konsulDetil));
                    }
                }
                ###  BONUS   ###
                /** @var UserEmployee[] $emps */
//                $emps = UserEmployee::model()->findAllByAttributes(array('id' => $model->user_id));
//                if ($emps != null) {
//                    $jml = count($emps);
//                    foreach ($emps as $emp) {
//                        $bns = new Bonus;
//                        $bns->tgl = $model->tgl;
//                        $bns->employee_id = $emp->employee_id;
//                        $bns->type_ = KONSUL;
//                        $bns->trans_no = $model->konsul_id;
//                        $bns->bruto = 0.00;
//                        $bns->dpp = 0.00;
//                        $bns->total = 0.00;
//                        if (!$bns->save()) {
//                            throw new Exception(t('save.model.fail', 'app',
//                                    array('{model}' => 'Customers')) . CHtml::errorSummary($bns));
//                        }
//                    }
//                }
                ###  BONUS   ###
//                $prt = new PrintReceipt($model);
//                $msg = $prt->buildTxt();
                $transaction->commit();
                $status = true;
                $msg = 'Konsultasi berhasil disimpan.';    
//                SOAP DIAGNOSA
                if (UPLOAD_NARS) {
                    $d = Diagnosa::model()->findAllByAttributes(array('konsul_id' => $model->konsul_id));
                    foreach ($d as $ds){
                        U::runCommandNars('diagnosaPasien', '--id=' . $ds['diagnosa_id']);
                    }                    
                }
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            fflush($x);            // flush output before releasing the lock
            flock($x, LOCK_UN);
            fclose($x);
            Yii::app()->end();
        }
        fflush($x);            // flush output before releasing the lock
        flock($x, LOCK_UN);
        fclose($x);
    }
    public function actionTambahan()
    {
        $x = @fopen(KONSUL_LOCK, "w");
        if (!flock($x, LOCK_EX | LOCK_NB)) {
            echo CJSON::encode(array(
                'success' => false,
                'id' => '',
                'msg' => "Transaksi belum bisa dilakukan, silahkan coba lagi beberapa saat."
            ));
            Yii::app()->end();
        }
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $model = null;
            $detils = CJSON::decode($_POST['detil']);
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $id_user = null;
                if ($_POST['type_'] == 2) {
                    $id_user = Users::get_access($_POST["loginUsername"], $_POST["loginPassword"], 9997);
                }
                if ($id_user === false) {
                    throw new Exception('User login tidak memilik akses sebagai dokter.');
                }
                $model = Konsul::model()->findByAttributes([
                    'ref_id' => $_POST['ref_id'],
                    'type_' => $_POST['type_']
                ]);
                if ($model == null) {
                    $model = new Konsul;
                    $model->tgl = new CDbExpression('DATE(NOW())');
                    $model->konsul_id = $this->generate_uuid();
                    $ref = new Reference();
                    $docref = $ref->get_next_reference(KONSUL);
                    $ref->save(KONSUL, $model->konsul_id, $docref);
                    $_POST['Konsul']['doc_ref'] = $docref;
                } else {
                    KonsulDetil::model()->deleteAllByAttributes(['konsul_id' => $model->konsul_id]);
                }
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Konsul'][$k] = $v;
                }
                $model->attributes = $_POST['Konsul'];
                if ($model->type_ == 2) {
                    /** @var UserEmployee $emp */
                    $emp = UserEmployee::model()->findByAttributes(array('id' => $id_user));
                    if ($emp == null) {
                        throw new Exception('Dokter tidak ditemukan dengan login ini!');
                    }
                    $model->dokter_id = $emp->employee_id;
                }
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Konsul')) . CHtml::errorSummary($model));
                }
//                $total_waktu = 0;
//                $rawat_tambahan = null;
                foreach ($detils as $detil) {
                    $konsulDetil = new KonsulDetil;
                    $qty = get_number($detil['qty']);
                    $_POST['KonsulDetil']['barang_id'] = $detil['barang_id'];
                    $_POST['KonsulDetil']['qty'] = $qty;
                    $_POST['KonsulDetil']['ketpot'] = $detil['ketpot'];
                    $_POST['KonsulDetil']['konsul_id'] = $model->konsul_id;
                    $konsulDetil->attributes = $_POST['KonsulDetil'];
                    if (!$konsulDetil->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Konsul Detail')) . CHtml::errorSummary($konsulDetil));
                    }
                    /** @var Barang $barang */
//                    $barang = Barang::model()->findByPk($konsulDetil->barang_id);
//                    if ($barang != null) {
//                        $durasi = 0;
//                        /** @var Jual $jual */
//                        $jual = Jual::model()->findByAttributes(array(
//                            'barang_id' => $konsulDetil->barang_id,
//                            'store' => $model->store
//                        ));
//                        if ($jual != null) {
//                            $durasi = $jual->duration;
//                        }
//                        $total_waktu += $durasi;
//                        $rawat_tambahan =
//                            ($rawat_tambahan == null) ? $barang->kode_barang : $rawat_tambahan . ', ' . $barang->kode_barang;
//                    }
                }
//                if ($model->type_ == 2) {
//                    ###  BONUS   ###
//                    /** @var UserEmployee[] $emps */
//                    $emps = UserEmployee::model()->findAllByAttributes(array('id' => $id_user));
//                    Bonus::model()->deleteAllByAttributes([
//                        'type_' => KONSUL,
//                        'trans_no' => $model->konsul_id
//                    ]);
//                    if ($emps != null) {
//                        $jml = count($emps);
//                        foreach ($emps as $emp) {
//                            $bns = new Bonus;
//                            $bns->tgl = $model->tgl;
//                            $bns->employee_id = $emp->employee_id;
//                            $bns->type_ = KONSUL;
//                            $bns->trans_no = $model->konsul_id;
//                            $bns->bruto = 0.00;
//                            $bns->dpp = 0.00;
//                            $bns->total = 0.00;
//                            if (!$bns->save()) {
//                                throw new Exception(t('save.model.fail', 'app',
//                                        array('{model}' => 'Customers')) . CHtml::errorSummary($bns));
//                            }
//                        }
//                    }
//                    ###  BONUS   ###
//                }
                /** @var Salestrans $sales */
                $sales = Salestrans::model()->findByPk($model->ref_id);
                if ($sales != null) {
                    $sales->updateJasaDurasi($model->ref_id);
                }
                $transaction->commit();
                $status = true;
                $msg = 'Konsultasi berhasil disimpan.';
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            fflush($x);            // flush output before releasing the lock
            flock($x, LOCK_UN);
            fclose($x);
            Yii::app()->end();
        }
        fflush($x);            // flush output before releasing the lock
        flock($x, LOCK_UN);
        fclose($x);
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'Konsul');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['Konsul'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['Konsul'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->konsul_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->konsul_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Konsul')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();

        //if (isset($_POST['counter'])) {
        //    $criteria->addCondition('counter = :counter or counter = "" or counter is null');
        //    $param[':counter'] = $_POST['counter'];
        //}

        if (isset($_POST['counter'])) {
            $counter = $_POST['counter'];
            if($counter == "E")
            {
                $criteria->addCondition('counter = :counter or counter is null');
                $param[':counter'] = $counter;
            }
        }

        $criteria->select = 'konsul_id,no_antrian,id_antrian,k.customer_id,
        tgl,doc_ref,c.nama_customer user_id,c.no_customer tdate,dokter_id,k.type_,k.beauty_id';
        $criteria->alias = 'k';
        $criteria->join = 'LEFT JOIN nscc_customers c ON k.customer_id = c.customer_id';
        $criteria->addCondition('salestrans_id = "" or salestrans_id is null');
        $criteria->addCondition('tgl = DATE(now())');
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = Konsul::model()->findAll($criteria);
        $total = Konsul::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionGetKonsul()
    {
        $criteria = new CDbCriteria();
        $param = array();
        if (isset($_POST['ref_id'])) {
            $criteria->addCondition('ref_id = :ref_id');
            $param[':ref_id'] = $_POST['ref_id'];
        }
        if (isset($_POST['salestrans_id'])) {
            $criteria->addCondition('salestrans_id = :salestrans_id');
            $param[':salestrans_id'] = $_POST['salestrans_id'];
        }
        if (isset($_POST['type_'])) {
            $criteria->addCondition('type_ = :type_');
            $param[':type_'] = $_POST['type_'];
        }
        $criteria->params = $param;
        $model = Konsul::model()->findAll($criteria);
        $total = Konsul::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}