<?php

class TransferBarangPerlengkapanDetailsController extends GxController
{
    public function actionIndexIn()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("transfer_perlengkapan_id = :transfer_perlengkapan_id");
        $criteria->params = array(':transfer_perlengkapan_id'=>$_POST['transfer_perlengkapan_id']);
        $model = TransferBarangperlengkapanDetails::model()->findAll($criteria);
        $total = TransferBarangperlengkapanDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndexOut()
    {
        $criteria = new CDbCriteria();
        $criteria->select = "transfer_perlengkapan_details_id,-qty qty,barang_perlengkapan_id,transfer_perlengkapan_id,
        price,-total total,disc,-discrp discrp,-bruto bruto,vat,-vatrp vatrp,disc1,-discrp1 discrp,-total_pot total_pot";
        $criteria->addCondition("transfer_perlengkapan_id = :transfer_perlengkapan_id");
        $criteria->params = array(':transfer_perlengkapan_id'=>$_POST['transfer_perlengkapan_id']);
        $model = TransferBarangperlengkapanDetails::model()->findAll($criteria);
        $total = TransferBarangperlengkapanDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}