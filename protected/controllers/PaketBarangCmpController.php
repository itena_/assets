<?php
class PaketBarangCmpController extends GxController
{
    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $param = array();
        if(isset($_POST['paket_details_id'])){
            $criteria->addCondition('paket_details_id = :paket_details_id');
            $param['paket_details_id'] = $_POST['paket_details_id'];
        }
        $criteria->params = $param;
        $model = PaketBarangCmp::model()->findAll($criteria);
        $total = PaketBarangCmp::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}