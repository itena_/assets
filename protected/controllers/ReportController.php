<?php
Yii::import('application.components.U');
Yii::import("application.components.tbs_class_php5", true);
Yii::import("application.components.tbs_plugin_excel", true);
Yii::import("application.components.tbs_plugin_opentbs", true);
Yii::import("application.components.tbs_plugin_aggregate", true);
Yii::import("application.extensions.PHPExcel", true);
Yii::import('application.modules.asset.models._base.BaseAssetDetail');
Yii::import('application.modules.asset.models.AssetDetail');
Yii::import('application.modules.asset.models._base.BaseAssetCategory');
Yii::import('application.modules.asset.models.AssetCategory');
class ReportController extends GxController
{
    private $TBS;
    private $logo;
    private $format;
    public $is_excel;
    public function init()
    {
        parent::init();
        if (!isset($_POST) || empty($_POST)) {
            $this->redirect(url('/'));
        }
        $this->layout = "report";
        $this->logo = bu() . app()->params['url_logo'];
        $this->TBS = new clsTinyButStrong;
        $this->format = $_POST['format'];
        if ($this->format == 'excel') {
            $this->TBS->PlugIn(TBS_INSTALL, TBS_EXCEL);
            $this->is_excel = true;
        } elseif ($this->format == 'openOffice') {
            $this->TBS->PlugIn(TBS_INSTALL, OPENTBS_PLUGIN);
        }
        error_reporting(E_ALL & ~E_NOTICE);
    }
    private function create_laba_rugi($from, $to, $store = "")
    {
        $lr = ChartMaster::get_laba_rugi($from, $to, $store);
        $jual_sales = $jual_agen = $hpp = $beban_administrasi = $beban_umum = $beban_pemasaran
            = $pendapatan_lain = $beban_lain = $pajak = array();
        $total_sales = $total_agen = $total_hpp = $total_beban_administrasi = $total_beban_umum
            = $total_beban_pemasaran = $total_pendapatan_lain = $total_beban_lain
            = $total_pajak = 0;
        $total_sales_account_code = $total_sales_account_name = $total_hpp_account_code = $total_hpp_account_name =
        $beban_administrasi_account_code = $beban_administrasi_account_name = $beban_umum_account_code =
        $beban_umum_account_name = $beban_tax_account_code = $beban_tax_account_name =
        $pendapatan_lain_account_code = $pendapatan_lain_account_name = $beban_adv_account_code =
        $beban_adv_account_name = "";
        foreach ($lr as $chart) {
            if (preg_match('/^4.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '499999') {
                    $total_sales_account_code = $chart['account_code'];
                    $total_sales_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '400000') {
                    $chart['total'] = null;
                    $jual_sales[] = $chart;
                } else {
                    $chart['total'] = -$chart['total'];
                    $total_sales += $chart['total'];
                    $jual_sales[] = $chart;
                }
            } elseif (preg_match('/^5.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '529999') {
                    $total_hpp_account_code = $chart['account_code'];
                    $total_hpp_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '521000') {
                    $chart['total'] = null;
                    $hpp[] = $chart;
                } else {
                    $total_hpp += $chart['total'];
                    $hpp[] = $chart;
                }
            } elseif (preg_match('/^611.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '611999') {
                    $beban_administrasi_account_code = $chart['account_code'];
                    $beban_administrasi_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '611000') {
                    $chart['total'] = null;
                    $beban_administrasi[] = $chart;
                } else {
                    $total_beban_administrasi += $chart['total'];
                    $beban_administrasi[] = $chart;
                }
            } elseif (preg_match('/^612.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '612999') {
                    $beban_umum_account_code = $chart['account_code'];
                    $beban_umum_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '612000') {
                    $chart['total'] = null;
                    $beban_umum[] = $chart;
                } else {
                    $total_beban_umum += $chart['total'];
                    $beban_umum[] = $chart;
                }
            } elseif (preg_match('/^613.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '613999') {
                    $beban_adv_account_code = $chart['account_code'];
                    $beban_adv_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '613000') {
                    $chart['total'] = null;
                    $beban_pemasaran[] = $chart;
                } else {
                    $total_beban_pemasaran += $chart['total'];
                    $beban_pemasaran[] = $chart;
                }
            } elseif (preg_match('/^614.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '614999') {
                    $beban_tax_account_code = $chart['account_code'];
                    $beban_tax_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '614000') {
                    $chart['total'] = null;
                    $pajak[] = $chart;
                } else {
                    $total_pajak += $chart['total'];
                    $pajak[] = $chart;
                }
            } elseif (preg_match('/^7.*/', $chart['account_code'])) {
                if ($chart['account_code'] == '799999') {
                    $pendapatan_lain_account_code = $chart['account_code'];
                    $pendapatan_lain_account_name = $chart['account_name'];
                } elseif ($chart['account_code'] == '700000') {
                    $chart['total'] = null;
                    $pendapatan_lain[] = $chart;
                } else {
                    $chart['total'] = -$chart['total'];
                    $total_pendapatan_lain += $chart['total'];
                    $pendapatan_lain[] = $chart;
                }
            }
        }
        $laba_kotor = $total_sales - $total_hpp;
        $total_beban_operasional = $total_beban_administrasi + $total_beban_umum +
            $total_beban_pemasaran + $total_pajak;
        $laba_operasional = $laba_kotor - $total_beban_operasional;
        $laba_bersih = $total_pendapatan_lain + $laba_operasional;
        if ($store == '') {
            $store = 'ALL CABANG';
        }
        return array(
            'header' => array(
                array(
                    'from' => $from,
                    'to' => $to,
                    'store' => $store,
                    'total_sales' => $total_sales,
                    'total_sales_account_code' => $total_sales_account_code,
                    'total_sales_account_name' => $total_sales_account_name,
                    'total_hpp_account_code' => $total_hpp_account_code,
                    'total_hpp_account_name' => $total_hpp_account_name,
                    'beban_administrasi_account_code' => $beban_administrasi_account_code,
                    'beban_administrasi_account_name' => $beban_administrasi_account_name,
                    'total_beban_administrasi' => $total_beban_administrasi,
                    'beban_umum_account_code' => $beban_umum_account_code,
                    'beban_umum_account_name' => $beban_umum_account_name,
                    'total_beban_umum' => $total_beban_umum,
                    'beban_adv_account_code' => $beban_adv_account_code,
                    'beban_adv_account_name' => $beban_adv_account_name,
                    'total_beban_adv' => $total_beban_pemasaran,
                    'beban_tax_account_code' => $beban_tax_account_code,
                    'beban_tax_account_name' => $beban_tax_account_name,
                    'total_hpp' => $total_hpp,
                    'laba_kotor' => $laba_kotor,
                    'pendapatan_lain_account_code' => $pendapatan_lain_account_code,
                    'pendapatan_lain_account_name' => $pendapatan_lain_account_name,
                    'total_pendapatan_lain' => $total_pendapatan_lain,
                    'total_beban_pemasaran' => $total_beban_pemasaran,
                    'total_beban_operasional' => $total_beban_operasional,
                    'laba_operasional' => $laba_operasional,
                    'laba_bersih' => $laba_bersih,
                    'total_beban_tax' => $total_pajak
                )
            ),
            'sales' => $jual_sales,
            'hpp' => $hpp,
            'beban_administrasi' => $beban_administrasi,
            'beban_umum' => $beban_umum,
            'beban_tax' => $pajak,
            'beban_adv' => $beban_pemasaran,
            'pendapatan_lain' => $pendapatan_lain
        );
    }
    private function create_laba_rugi_new($from, $to, $store = "")
    {
        $lr = ChartMaster::get_laba_rugi_new($from, $to, $store);
        $income = ChartTypes::get_chart_types_by_class(CL_INCOME);
        $income_arr = array();
        foreach ($income as $row) {
            $income_arr[] = array(
                'id' => '',
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $income_arr[] = array(
                        'id' => $item['account_code'],
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $income_arr = array_merge($income_arr, ChartMaster::print_chart($row['id'], $lr, -1));
        }
        $total_income = array_sum(array_column($income_arr, 'total'));
        $hpp = ChartTypes::get_chart_types_by_class(CL_COGS);
        $hpp_arr = array();
        foreach ($hpp as $row) {
            $hpp_arr[] = array(
                'id' => '',
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $hpp_arr[] = array(
                        'id' => $item['account_code'],
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $hpp_arr = array_merge($hpp_arr, ChartMaster::print_chart($row['id'], $lr));
        }
        $total_hpp = array_sum(array_column($hpp_arr, 'total'));
        $laba_kotor = $total_income - $total_hpp;
        $cost = ChartTypes::get_chart_types_by_class(CL_EXPENSE);
        $cost_arr = array();
        foreach ($cost as $row) {
            $cost_arr[] = array(
                'id' => '',
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $cost_arr[] = array(
                        'id' => $item['account_code'],
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $cost_arr = array_merge($cost_arr, ChartMaster::print_chart($row['id'], $lr));
        }
        $total_biaya = array_sum(array_column($cost_arr, 'total'));
        $total_laba_bersih = $laba_kotor - $total_biaya;
        $other_income = ChartTypes::get_chart_types_by_class(CL_OTHER_INCOME);
        $other_income_arr = array();
        foreach ($other_income as $row) {
            $other_income_arr[] = array(
                'id' => '',
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $other_income_arr[] = array(
                        'id' => $item['account_code'],
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $other_income_arr = array_merge($other_income_arr, ChartMaster::print_chart($row['id'], $lr));
        }
        $total_other_income = array_sum(array_column($other_income_arr, 'total'));
        $laba_bersih_sebelum_pajak = $total_laba_bersih - $total_other_income;
        if ($store == '') {
            $store = 'ALL CABANG';
        }
        return array(
            'header' => array(
                array(
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store,
                    'income' => $total_income,
                    'hpp' => $total_hpp,
                    'laba_kotor' => $laba_kotor,
                    'biaya' => $total_biaya,
                    'laba_bersih' => $total_laba_bersih,
                    'other_income' => $total_other_income,
                    'laba_bersih_sebelum_pajak' => $laba_bersih_sebelum_pajak,
                    'income_label' => 'Income',
                    'hpp_label' => 'HPP'
                )
            ),
            'income' => $income_arr,
            'hpp' => $hpp_arr,
            'biaya' => $cost_arr,
            'other_income' => $other_income_arr
        );
    }
    private function create_laba_rugi_new_konsolidasi($from, $to, $store = array())
    {
        $new_arr = array();
        foreach ($store as $cab) {
            $new_arr[] = "Sum(IF(pgt.store = '$cab',pgt.amount,0)) AS $cab";
        }
        $select = implode(",", $new_arr);
        $command = "SELECT
       nct.`name` AS name_1,ncm.account_code,ncm.account_name,ncm.saldo_normal,
				$select,ncc.ctype,nct.id AS id_1
        FROM nscc_chart_master AS ncm
        LEFT JOIN nscc_chart_types AS nct ON ncm.kategori = nct.id
        LEFT JOIN nscc_chart_types AS nct1 ON nct.parent = nct1.id
        LEFT JOIN nscc_chart_types AS nct2 ON nct1.parent = nct2.id
        LEFT JOIN nscc_chart_class AS ncc ON COALESCE(nct2.class_id,nct1.class_id,nct.class_id) = ncc.cid
        LEFT JOIN nscc_gl_trans AS pgt ON ncm.account_code = pgt.account_code AND
        pgt.tran_date >= :from AND pgt.tran_date <= :to AND pgt.visible = 1
        WHERE ncc.ctype IN (4,5,6,7)
        GROUP BY ncm.account_code
        ORDER BY ncm.account_code";
        $comm = Yii::app()->db->createCommand($command);
        $param = array(':from' => $from, ':to' => $to);
        $lr = $comm->queryAll(true, $param);
        $income = ChartTypes::get_chart_types_by_class(CL_INCOME);
        $income_label = array();
        $income_arr = array();
        foreach ($income as $row) {
            $income_label[] = array(
                'id' => '',
                'account_code' => '',
                'account_name' => $row['name']
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $income_label[] = array(
                        'id' => $item['account_code'],
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name']
                    );
                    foreach ($store as $cab) {
                        $income_arr[$cab][$item['account_code']] =
                            (($item['saldo_normal'] == 'D' ? 1 : -1) * $item[$cab]);
                        $income_arr['TOTAL'][$item['account_code']] +=
                            $income_arr[$cab][$item['account_code']];
                    }
                    unset($lr[$key]);
                }
            }
            $result = ChartMaster::print_chart_konsolidasi($row['id'], $lr, $store);
            $income_arr = array_merge($income_arr, $result['arr']);
            $income_label = array_merge($income_label, $result['label']);
        }
        $hpp = ChartTypes::get_chart_types_by_class(CL_COGS);
        $hpp_label = array();
        $hpp_arr = array();
        foreach ($hpp as $row) {
            $hpp_label[] = array(
                'id' => '',
                'account_code' => '',
                'account_name' => $row['name']
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $hpp_label[] = array(
                        'id' => $item['account_code'],
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name']
                    );
                    foreach ($store as $cab) {
                        $hpp_arr[$cab][$item['account_code']] =
                            (($item['saldo_normal'] == 'D' ? 1 : -1) * $item[$cab]);
                        $hpp_arr['TOTAL'][$item['account_code']] +=
                            $hpp_arr[$cab][$item['account_code']];
                    }
                    unset($lr[$key]);
                }
            }
            $result = ChartMaster::print_chart_konsolidasi($row['id'], $lr, $store);
            $hpp_arr = array_merge($hpp_arr, $result['arr']);
            $hpp_label = array_merge($hpp_label, $result['label']);
        }
        $cost = ChartTypes::get_chart_types_by_class(CL_EXPENSE);
        $cost_label = array();
        $cost_arr = array();
        foreach ($cost as $row) {
            $cost_label[] = array(
                'id' => '',
                'account_code' => '',
                'account_name' => $row['name']
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $cost_label[] = array(
                        'id' => $item['account_code'],
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name']
                    );
                    foreach ($store as $cab) {
                        $cost_arr[$cab][$item['account_code']] =
                            (($item['saldo_normal'] == 'D' ? 1 : -1) * $item[$cab]);
                        $cost_arr['TOTAL'][$item['account_code']] +=
                            $cost_arr[$cab][$item['account_code']];
                    }
                    unset($lr[$key]);
                }
            }
            $result = ChartMaster::print_chart_konsolidasi($row['id'], $lr, $store);
            $cost_arr = array_merge($cost_arr, $result['arr']);
            $cost_label = array_merge($cost_label, $result['label']);
        }
        $other_income = ChartTypes::get_chart_types_by_class(CL_OTHER_INCOME);
        $other_income_label = array();
        $other_income_arr = array();
        foreach ($other_income as $row) {
            $other_income_label[] = array(
                'id' => '',
                'account_code' => '',
                'account_name' => $row['name']
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $other_income_label[] = array(
                        'id' => $item['account_code'],
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name']
                    );
                    foreach ($store as $cab) {
                        $other_income_arr[$cab][$item['account_code']] =
                            (($item['saldo_normal'] == 'D' ? 1 : -1) * $item[$cab]);
                        $other_income_arr['TOTAL'][$item['account_code']] +=
                            $other_income_arr[$cab][$item['account_code']];
                    }
                    unset($lr[$key]);
                }
            }
            $result = ChartMaster::print_chart_konsolidasi($row['id'], $lr, $store);
            $other_income_arr = array_merge($other_income_arr, $result['arr']);
            $other_income_label = array_merge($other_income_label, $result['label']);
        }
        $store_header = array();
        $income_label[] = array(
            'id' => 'income',
            'account_code' => '',
            'account_name' => 'Income'
        );
        $hpp_label[] = array(
            'id' => 'hpp',
            'account_code' => '',
            'account_name' => 'HPP'
        );
        $hpp_label[] = array(
            'id' => 'laba_kotor',
            'account_code' => '',
            'account_name' => 'Laba Kotor Penjualan'
        );
        $cost_label[] = array(
            'id' => 'biaya',
            'account_code' => '',
            'account_name' => 'Jumlah Biaya Usaha'
        );
        $cost_label[] = array(
            'id' => 'laba_bersih',
            'account_code' => '',
            'account_name' => 'Laba Bersih Usaha'
        );
        $other_income_label[] = array(
            'id' => 'total_other_income',
            'account_code' => '',
            'account_name' => 'Jumlah Pendapatan & Biaya Lain-Lain'
        );
        $other_income_label[] = array(
            'id' => 'laba_bersih_sebelum_pajak',
            'account_code' => '',
            'account_name' => 'Laba Bersih Usaha Sebelum Pajak'
        );
        $store[] = 'TOTAL';
        foreach ($store as $cab) {
            $store_header[] = array('store_kode' => $cab);
            $total_income = array_sum($income_arr[$cab]);
            $income_arr[$cab]['income'] = $total_income;
            $total_hpp = array_sum($hpp_arr[$cab]);
            $laba_kotor = $total_income - $total_hpp;
            $hpp_arr[$cab]['hpp'] = $total_hpp;
            $hpp_arr[$cab]['laba_kotor'] = $laba_kotor;
            $total_biaya = array_sum($cost_arr[$cab]);
            $total_laba_bersih = $laba_kotor - $total_biaya;
            $cost_arr[$cab]['biaya'] = $total_biaya;
            $cost_arr[$cab]['laba_bersih'] = $total_laba_bersih;
            $total_other_income = array_sum($other_income_arr[$cab]);
            $laba_bersih_sebelum_pajak = $total_laba_bersih - $total_other_income;
            $other_income_arr[$cab]['total_other_income'] = $total_other_income;
            $other_income_arr[$cab]['laba_bersih_sebelum_pajak'] = $laba_bersih_sebelum_pajak;
        }
        return array(
            'header' => array(
                array(
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy')
                )
            ),
            'd' => $store_header,
            'income_label' => $income_label,
            'hpp_label' => $hpp_label,
            'cost_label' => $cost_label,
            'other_income_label' => $other_income_label,
            'income' => $income_arr,
            'hpp' => $hpp_arr,
            'biaya' => $cost_arr,
            'other_income' => $other_income_arr
        );
    }
    private function createBalanceSheet($from, $to, $store = null)
    {
        $n = ChartMaster::getBalanceSheet($from, $to, $store);
        $current_assets = ChartTypes::get_chart_types_by_class(CL_CURRENT_ASSETS);
        $curent_assets_arr = array();
        foreach ($current_assets as $row) {
            foreach ($n as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $curent_assets_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'before' => $item['before'],
                        'debit' => $item['debit'],
                        'kredit' => $item['kredit'],
                        'after' => $item['after']
                    );
                    unset($n[$key]);
                }
            }
            $curent_assets_arr = array_merge($curent_assets_arr, ChartMaster::printBalanceSheet($row['id'], $n));
        }
//        $total_current_assets = array_sum(array_column($curent_assets_arr, 'after'));
        $fixed_assets = ChartTypes::get_chart_types_by_class(CL_FIXED_ASSETS);
        $fixed_assets_arr = array();
        foreach ($fixed_assets as $row) {
            foreach ($n as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $fixed_assets_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'before' => $item['before'],
                        'debit' => $item['debit'],
                        'kredit' => $item['kredit'],
                        'after' => $item['after']
                    );
                    unset($n[$key]);
                }
            }
            $fixed_assets_arr = array_merge($fixed_assets_arr, ChartMaster::printBalanceSheet($row['id'], $n));
        }
//        $total_fixed_assets = array_sum(array_column($fixed_assets_arr, 'total'));
        $current_liabilities = ChartTypes::get_chart_types_by_class(CL_CURRENT_LIABILITIES);
        $current_liabilities_arr = array();
        foreach ($current_liabilities as $row) {
            foreach ($n as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $current_liabilities_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'before' => $item['before'],
                        'debit' => $item['debit'],
                        'kredit' => $item['kredit'],
                        'after' => $item['after']
                    );
                    unset($n[$key]);
                }
            }
            $current_liabilities_arr = array_merge($current_liabilities_arr, ChartMaster::printBalanceSheet($row['id'], $n, -1));
        }
//        $total_current_liabilities = array_sum(array_column($current_liabilities_arr, 'total'));
        $fixed_liabilities = ChartTypes::get_chart_types_by_class(CL_LONGTERM_LIABILITIES);
        $fixed_liabilities_arr = array();
        foreach ($fixed_liabilities as $row) {
            foreach ($n as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $fixed_liabilities_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'before' => $item['before'],
                        'debit' => $item['debit'],
                        'kredit' => $item['kredit'],
                        'after' => $item['after']
                    );
                    unset($n[$key]);
                }
            }
            $fixed_liabilities_arr = array_merge($fixed_liabilities_arr, ChartMaster::printBalanceSheet($row['id'], $n, -1));
        }
//        $total_fixed_liabilities = array_sum(array_column($fixed_liabilities_arr, 'total'));
        $equiti = ChartTypes::get_chart_types_by_class(CL_EQUITY);
        $equiti_arr = array();
        foreach ($equiti as $row) {
            foreach ($n as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $equiti_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'before' => $item['before'],
                        'debit' => $item['debit'],
                        'kredit' => $item['kredit'],
                        'after' => $item['after']
                    );
                    unset($n[$key]);
                }
            }
            $equiti_arr = array_merge($equiti_arr, ChartMaster::printBalanceSheet($row['id'], $n, -1));
        }
//        $total_equiti = array_sum(array_column($equiti_arr, 'total'));
        $arr_activa = array_merge($curent_assets_arr, $fixed_assets_arr);
        $arr_pasiva = array_merge($current_liabilities_arr, $fixed_liabilities_arr, $equiti_arr);
        $arr_activa_total['before'] = array_sum(array_column($arr_activa, 'before'));
        $arr_activa_total['debit'] = array_sum(array_column($arr_activa, 'debit'));
        $arr_activa_total['kredit'] = array_sum(array_column($arr_activa, 'kredit'));
        $arr_activa_total['after'] = array_sum(array_column($arr_activa, 'after'));
        $arr_pasiva_total['before'] = array_sum(array_column($arr_pasiva, 'before'));
        $arr_pasiva_total['debit'] = array_sum(array_column($arr_pasiva, 'debit'));
        $arr_pasiva_total['kredit'] = array_sum(array_column($arr_pasiva, 'kredit'));
        $arr_pasiva_total['after'] = array_sum(array_column($arr_pasiva, 'after'));
        return array(
            'header' => array(
                array(
                    'title_pt' => app()->params['report_title_pt'],
                    'to' => $to,
                    'selisih' => $arr_activa_total['after'] - $arr_pasiva_total['after']
                )
            ),
            'total_activa' => $arr_activa_total,
            'total_passiva' => $arr_pasiva_total,
            'current_l' => $current_liabilities_arr,
            'fixed_l' => $fixed_liabilities_arr,
            'current' => $curent_assets_arr,
            'fixed' => $fixed_assets_arr,
            'equity' => $equiti_arr
        );
    }
    private function create_neraca_new($to, $store = "")
    {
        $lr = ChartMaster::get_neraca_new($to, $store);
        $current_assets = ChartTypes::get_chart_types_by_class(CL_CURRENT_ASSETS);
        $curent_assets_arr = array();
        foreach ($current_assets as $row) {
            $curent_assets_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $curent_assets_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $curent_assets_arr = array_merge($curent_assets_arr, ChartMaster::print_chart($row['id'], $lr));
        }
        $total_current_assets = array_sum(array_column($curent_assets_arr, 'total'));
        $fixed_assets = ChartTypes::get_chart_types_by_class(CL_FIXED_ASSETS);
        $fixed_assets_arr = array();
        foreach ($fixed_assets as $row) {
            $fixed_assets_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $fixed_assets_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $fixed_assets_arr = array_merge($fixed_assets_arr, ChartMaster::print_chart($row['id'], $lr));
        }
        $total_fixed_assets = array_sum(array_column($fixed_assets_arr, 'total'));
        $current_liabilities = ChartTypes::get_chart_types_by_class(CL_CURRENT_LIABILITIES);
        $current_liabilities_arr = array();
        foreach ($current_liabilities as $row) {
            $current_liabilities_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $current_liabilities_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $current_liabilities_arr = array_merge($current_liabilities_arr, ChartMaster::print_chart($row['id'], $lr, -1));
        }
        $total_current_liabilities = array_sum(array_column($current_liabilities_arr, 'total'));
        $fixed_liabilities = ChartTypes::get_chart_types_by_class(CL_LONGTERM_LIABILITIES);
        $fixed_liabilities_arr = array();
        foreach ($fixed_liabilities as $row) {
            $fixed_liabilities_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $fixed_liabilities_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $fixed_liabilities_arr = array_merge($fixed_liabilities_arr, ChartMaster::print_chart($row['id'], $lr, -1));
        }
        $total_fixed_liabilities = array_sum(array_column($fixed_liabilities_arr, 'total'));
        $equiti = ChartTypes::get_chart_types_by_class(CL_EQUITY);
        $equiti_arr = array();
        foreach ($equiti as $row) {
            $equiti_arr[] = array(
                'account_code' => '',
                'account_name' => $row['name'],
                'total' => ''
            );
            foreach ($lr as $key => $item) {
                if ($item['id_1'] == $row['id']) {
                    $equiti_arr[] = array(
                        'account_code' => $item['account_code'],
                        'account_name' => $item['account_name'],
                        'total' => (($item['saldo_normal'] == 'D' ? 1 : -1) * $item['total'])
                    );
                    unset($lr[$key]);
                }
            }
            $equiti_arr = array_merge($equiti_arr, ChartMaster::print_chart($row['id'], $lr, -1));
        }
        $total_equiti = array_sum(array_column($equiti_arr, 'total'));
        $laba_kotor = $total_current_assets - $total_fixed_assets;
        $total_laba_bersih = $laba_kotor - $total_current_liabilities;
        $laba_bersih_sebelum_pajak = $total_laba_bersih - $total_fixed_liabilities;
        $count_assets = count($curent_assets_arr);
        $count_current_liability = count($current_liabilities_arr);
        $count_fixed_assets = count($fixed_assets_arr);
        $count_longterm_liability = count($fixed_liabilities_arr);
        if ($count_assets >= $count_current_liability) {
            for ($i = 0; $i <= $count_assets; $i++) {
                $arr_current_assets[$i]['account_code_assets'] = $curent_assets_arr[$i]['account_code'];
                $arr_current_assets[$i]['account_assets'] = $curent_assets_arr[$i]['account_name'];
                $arr_current_assets[$i]['assets'] = $curent_assets_arr[$i]['total'];
                if ($i > $count_current_liability) {
                    $arr_current_assets[$i]['account_code_liabilities'] = null;
                    $arr_current_assets[$i]['account_liabilities'] = null;
                    $arr_current_assets[$i]['liabilities'] = null;
                } else {
                    $arr_current_assets[$i]['account_code_liabilities'] = $current_liabilities_arr[$i]['account_code'];
                    $arr_current_assets[$i]['account_liabilities'] = $current_liabilities_arr[$i]['account_name'];
                    $arr_current_assets[$i]['liabilities'] = $current_liabilities_arr[$i]['total'];
                }
            }
        } else {
            for ($i = 0; $i <= $count_current_liability; $i++) {
                $arr_current_assets[$i]['account_code_liabilities'] = $current_liabilities_arr[$i]['account_code'];
                $arr_current_assets[$i]['account_liabilities'] = $current_liabilities_arr[$i]['account_name'];
                $arr_current_assets[$i]['liabilities'] = $current_liabilities_arr[$i]['total'];
                if ($i > $count_assets) {
                    $arr_current_assets[$i]['account_code_assets'] = null;
                    $arr_current_assets[$i]['account_assets'] = null;
                    $arr_current_assets[$i]['assets'] = null;
                } else {
                    $arr_current_assets[$i]['account_code_assets'] = $curent_assets_arr[$i]['account_code'];
                    $arr_current_assets[$i]['account_assets'] = $curent_assets_arr[$i]['account_name'];
                    $arr_current_assets[$i]['assets'] = $curent_assets_arr[$i]['total'];
                }
            }
        }
        if ($count_fixed_assets >= $count_longterm_liability) {
            for ($i = 0; $i <= $count_fixed_assets; $i++) {
                $arr_fixed_assets[$i]['account_code_assets'] = $fixed_assets_arr[$i]['account_code'];
                $arr_fixed_assets[$i]['account_assets'] = $fixed_assets_arr[$i]['account_name'];
                $arr_fixed_assets[$i]['assets'] = $fixed_assets_arr[$i]['total'];
                if ($i > $count_longterm_liability) {
                    $arr_fixed_assets[$i]['account_code'] = null;
                    $arr_fixed_assets[$i]['account_liabilities'] = null;
                    $arr_fixed_assets[$i]['liabilities'] = null;
                } else {
                    $arr_fixed_assets[$i]['account_code'] = $fixed_liabilities_arr[$i]['account_code'];
                    $arr_fixed_assets[$i]['account_liabilities'] = $fixed_liabilities_arr[$i]['account_name'];
                    $arr_fixed_assets[$i]['liabilities'] = $fixed_liabilities_arr[$i]['total'];
                }
            }
        } else {
            for ($i = 0; $i <= $count_longterm_liability; $i++) {
                $arr_fixed_assets[$i]['account_code'] = $fixed_liabilities_arr[$i]['account_code'];
                $arr_fixed_assets[$i]['account_liabilities'] = $fixed_liabilities_arr[$i]['account_name'];
                $arr_fixed_assets[$i]['liabilities'] = $fixed_liabilities_arr[$i]['after'];
                if ($i > $count_fixed_assets) {
                    $arr_fixed_assets[$i]['account_code_assets'] = null;
                    $arr_fixed_assets[$i]['account_assets'] = null;
                    $arr_fixed_assets[$i]['assets'] = null;
                } else {
                    $arr_fixed_assets[$i]['account_code_assets'] = $fixed_assets_arr[$i]['account_code'];
                    $arr_fixed_assets[$i]['account_assets'] = $fixed_assets_arr[$i]['account_name'];
                    $arr_fixed_assets[$i]['assets'] = $fixed_assets_arr[$i]['after'];
                }
            }
        }
        return array(
            'header' => array(
                array(
                    'title_pt' => app()->params['report_title_pt'],
                    'to' => $to,
                    'total_current_assets' => $total_current_assets,
                    'total_fixed_assets' => $total_fixed_assets,
                    'total_current_liabilities' => $total_current_liabilities,
                    'total_longterm_liabilities' => $total_fixed_liabilities,
                    'total_equity' => $total_equiti,
                    'activa' => $total_current_assets + $total_fixed_assets,
                    'passiva' => $total_current_liabilities + $total_fixed_liabilities + $total_equiti
                )
            ),
            'current' => $arr_current_assets,
            'fixed' => $arr_fixed_assets,
            'equity' => $equiti_arr
        );
    }
    public function actionGenerateLabaRugi()
    {
        $path = Yii::app()->basePath . DS . 'runtime' . DS;
        $status = false;
        $msg = "Initial Error.";
        try {
            $month = $_POST['month'];
            $year = $_POST['year'];
            //        $store = $_POST['store'];
            $criteria = new CDbCriteria();
            if ($_POST['store'] != '') {
                $criteria->addCondition('store_kode = :store_kode');
                $criteria->params = array(':store_kode' => $_POST['store']);
            }
            $d = new DateTime("$year-$month-01");
            $from = $d->format('Y-m-d');
            $to = $d->format('Y-m-t');
            $branches = Store::model()->findAll($criteria);
            foreach ($branches as $key => $branch) {
                app()->db->autoCommit = false;
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    $store = $branch->store_kode;
                    $uuid = $this->generate_uuid();
//                    $result = self::create_laba_rugi_new($from, $to, $store);
                    $result = [];
                    U::runCommand('utils', 'createLabaRugi', '--uuid=' . $uuid .
                        ' --from=' . $from . ' --to=' . $to . ' --store=' . $store,
                        $uuid . '-' . LABARUGI . '.log');
                    for ($i = 0; $i <= 1000; $i++) {
                        if (file_exists($path . $uuid . '-LABARUGI')) {
                            $fileContents = file_get_contents($path . $uuid . '-LABARUGI');
                            $result = json_decode($fileContents, true);
                            unlink($path . $uuid . '-LABARUGI');
                            unlink($path . $uuid . '-' .LABARUGI. '.log');
                            break;
                        }
                        sleep(1);
                    }
                    $amount = $result['header'][0]['laba_bersih_sebelum_pajak'];
                    $amount = -$amount;
//                    $gl = GlTrans::model()->find('type = :type AND tran_date >= :from AND tran_date <= :to AND store = :store',
//                        array(':type' => LABARUGI, ':from' => $from, ':to' => $to, ':store' => $store));
                    GlTrans::model()->updateAll(['visible' => 0], 'type = :type AND tran_date >= :from AND tran_date <= :to AND store = :store',
                        array(':type' => LABARUGI, ':from' => $from, ':to' => $to, ':store' => $store));
                    $id = $this->generate_uuid();//U::get_next_trans_no_bank_trans(LABARUGI, $store);
                    Yii::import('application.components.GL');
                    $gl = new GL();
                    $gl->add_gl(LABARUGI, $id, $to, null, COA_LABA_RUGI, 'PROFIT LOST', '', $amount, 0, $store);
//                    if (!$gl->save()) {
//                        throw new Exception(t('save.model.fail', 'app',
//                                array('{model}' => 'GL Trans')) . CHtml::errorSummary($gl));
//                    }
//                    if ($gl == null) {
//
//                    } else {
//                        $gl->amount = $amount;
//                        if (!$gl->save()) {
//                            throw new Exception(t('save.model.fail', 'app',
//                                    array('{model}' => 'GL Trans')) . CHtml::errorSummary($gl));
//                        }
//                    }
                    $transaction->commit();
                    $msg = 'Profit Lost succesfully generated.';
                    $status = true;
                } catch (CDbException $dbex) {
                    $transaction->rollback();
                    $status = false;
                    $msg = $dbex->getMessage();
                }
                app()->db->autoCommit = true;
            }
        } Catch (Exception $ex) {
            $status = false;
            $msg = $ex->getMessage();
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
    }
    public function actionLabaRugiOld()
    {
        $from = $_POST['from_date'];
        $to = $_POST['to_date'];
        $result = self::create_laba_rugi($from, $to);
        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'LabaRugi.xml');
        $this->TBS->MergeBlock('header', $result['header']);
        $this->TBS->MergeBlock('sales', $result['sales']);
        $this->TBS->MergeBlock('hpp', $result['hpp']);
        $this->TBS->MergeBlock('beban_administrasi', $result['beban_administrasi']);
        $this->TBS->MergeBlock('beban_umum', $result['beban_umum']);
        $this->TBS->MergeBlock('beban_tax', $result['beban_tax']);
        $this->TBS->MergeBlock('beban_adv', $result['beban_adv']);
        $this->TBS->MergeBlock('pendapatan_lain', $result['pendapatan_lain']);
        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "LabaRugi" . $from . $to . ".xls");
    }
    public function actionLabaRugiKonsolidasi()
    {
        $path = Yii::app()->basePath . DS . 'runtime' . DS;
        $uuid = $this->generate_uuid();
        $from = $_POST['from_date'];
        $to = $_POST['to_date'];
        $store = CJSON::decode($_POST['store']);
        $store_arr = array();
        foreach ($store as $s) {
            if ($s['count']) {
                $store_arr[] = $s['store_kode'];
            }
        }
        if (count($store_arr) == 1) {
            $result = [];
            U::runCommand('utils', 'createLabaRugi', '--uuid=' . $uuid .
                ' --from=' . $from . ' --to=' . $to . ' --store=' . $store_arr[0],
                $uuid . '-' . LABARUGI . '.log');
            for ($i = 0; $i <= 1000; $i++) {
                if (file_exists($path . $uuid . '-LABARUGI')) {
                    $fileContents = file_get_contents($path . $uuid . '-LABARUGI');
                    $result = json_decode($fileContents, true);
                    unlink($path . $uuid . '-LABARUGI');
                    unlink($path . $uuid . '-' .LABARUGI. '.log');
                    break;
                }
                sleep(1);
            }
//            $result = self::create_laba_rugi_new($from, $to, $store_arr[0]);
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'lr.xml');
            $this->TBS->MergeBlock('header', $result['header']);
            $this->TBS->MergeBlock('income', $result['income']);
            $this->TBS->MergeBlock('hpp', $result['hpp']);
            $this->TBS->MergeBlock('biaya', $result['biaya']);
            $this->TBS->MergeBlock('other_income', $result['other_income']);
        } else {
//            $res = self::create_laba_rugi_new_konsolidasi($from, $to, $store_arr);
            $res = [];
            U::runCommand('utils', 'CreateLabaRugiKonsolidasi', '--uuid=' . $uuid .
                ' --from=' . $from . ' --to=' . $to . ' --storeArr=' .implode(',',$store_arr),
                $uuid . '-' . LABARUGI . '.log');
            for ($i = 0; $i <= 1000; $i++) {
                if (file_exists($path . $uuid . '-LABARUGI')) {
                    $fileContents = file_get_contents($path . $uuid . '-LABARUGI');
                    $res = json_decode($fileContents, true);
                    unlink($path . $uuid . '-LABARUGI');
                    unlink($path . $uuid .'-' . LABARUGI. '.log');
                    break;
                }
                sleep(1);
            }
            $this->TBS->VarRef['income2'] = $res['income'];
            $this->TBS->VarRef['hpp2'] = $res['hpp'];
            $this->TBS->VarRef['biaya2'] = $res['biaya'];
            $this->TBS->VarRef['other_income2'] = $res['other_income'];
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'lr_konsolidasi.xml');
            $this->TBS->MergeBlock('d,income1,hpp1,biaya1,other_income1', $res['d']);
            $this->TBS->MergeBlock('income', $res['income_label']);
            $this->TBS->MergeBlock('hpp', $res['hpp_label']);
            $this->TBS->MergeBlock('biaya', $res['cost_label']);
            $this->TBS->MergeBlock('other_income', $res['other_income_label']);
            $this->TBS->MergeBlock('header', $res['header']);
        }
        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "LabaRugi" . $from . $to . ".xls");
    }
    public function actionLabaRugi()
    {
//        ini_set('xdebug.var_display_max_depth', 5);
//        ini_set('xdebug.var_display_max_children', 256);
//        ini_set('xdebug.var_display_max_data', 1024);
        $from = $_POST['from_date'];
        $to = $_POST['to_date'];
        $store = CJSON::decode($_POST['store']);
        $result = array();
        $store_rep = array();
        $income2 = array();
        $hpp2 = array();
        $biaya2 = array();
        $other_income2 = array();
        foreach ($store as $s) {
            if ($s['count']) {
                $store_rep[] = array('store_kode' => $s['store_kode']);
                $result[$s['store_kode']] = self::create_laba_rugi_new($from, $to, $s['store_kode']);
                foreach ($result[$s['store_kode']]['income'] as $cabang => $inc) {
                    $income2[$s['store_kode']][$inc['id']] = $inc['total'];
                    if ($inc['id'] !== '' && $inc['total'] !== '') {
                        $income2['TOTAL'][$inc['id']] += $inc['total'];
                    } else {
                        $income2['TOTAL'][$inc['id']] = '';
                    }
                }
                foreach ($result[$s['store_kode']]['hpp'] as $cabang => $inc) {
                    $hpp2[$s['store_kode']][$inc['id']] = $inc['total'];
                    if ($inc['id'] !== '' && $inc['total'] !== '') {
                        $hpp2['TOTAL'][$inc['id']] += $inc['total'];
                    } else {
                        $hpp2['TOTAL'][$inc['id']] = '';
                    }
                }
                foreach ($result[$s['store_kode']]['biaya'] as $cabang => $inc) {
                    $biaya2[$s['store_kode']][$inc['id']] = $inc['total'];
                    if ($inc['id'] !== '' && $inc['total'] !== '') {
                        $biaya2['TOTAL'][$inc['id']] += $inc['total'];
                    } else {
                        $biaya2['TOTAL'][$inc['id']] = '';
                    }
                }
                foreach ($result[$s['store_kode']]['other_income'] as $cabang => $inc) {
                    $other_income2[$s['store_kode']][$inc['id']] = $inc['total'];
                    if ($inc['id'] !== '' && $inc['total'] !== '') {
                        $other_income2['TOTAL'][$inc['id']] += $inc['total'];
                    } else {
                        $other_income2['TOTAL'][$inc['id']] = '';
                    }
                }
            }
        }
        $store_rep[] = array('store_kode' => 'TOTAL');
        $allKeys = array_keys($result);
        $this->TBS->VarRef['income2'] = $income2;
        $this->TBS->VarRef['hpp2'] = $hpp2;
        $this->TBS->VarRef['biaya2'] = $biaya2;
        $this->TBS->VarRef['other_income2'] = $other_income2;
        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'lr.xml');
        $this->TBS->MergeBlock('d,income1,hpp1,biaya1,other_income1', $store_rep);
        $this->TBS->MergeBlock('income', $result[$allKeys[0]]['income']);
        $this->TBS->MergeBlock('hpp', $result[$allKeys[0]]['hpp']);
        $this->TBS->MergeBlock('biaya', $result[$allKeys[0]]['biaya']);
        $this->TBS->MergeBlock('other_income', $result[$allKeys[0]]['other_income']);
        $this->TBS->MergeBlock('header', $result[$allKeys[0]]['header']);
        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "LabaRugi" . $from . $to . ".xls");
    }
    public function actionNeracaOld()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $to = $_POST['to_date'];
//            $dt = strtotime($_POST['to_date']);
            $dt_frm = date_create($to);
            $from = date_format($dt_frm, 'Y-01-01');
            $lr = ChartMaster::get_neraca($from, $to);
            $current_assets = $fixed_assets = $current_liability = $longterm_liability = $equity = 0;
            $arr_current_assets = $arr_fixed_assets = $arr_current_liability = $arr_longterm_liability = $arr_equity = array();
            foreach ($lr as $chart) {
                if (preg_match('/^1[0-1][0-1].*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '111999' || $chart['header'] == 1) {
                    } else {
                        $current_assets += $chart['after'];
                        $arr_current_assets[] = $chart;
                    }
                } elseif (preg_match('/^112.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '112999' || $chart['header'] == 1) {
                    } else {
                        $current_assets += $chart['after'];
                        $arr_current_assets[] = $chart;
                    }
                } elseif (preg_match('/^113.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '113999' || $chart['header'] == 1) {
                    } else {
                        $current_assets += $chart['after'];
                        $arr_current_assets[] = $chart;
                    }
                } elseif (preg_match('/^114.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '114199' || $chart['header'] == 1) {
                    } else {
                        $current_assets += $chart['after'];
                        $arr_current_assets[] = $chart;
                    }
                } elseif (preg_match('/^115.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '115999' || $chart['header'] == 1) {
                    } else {
                        $current_assets += $chart['after'];
                        $arr_current_assets[] = $chart;
                    }
                } elseif (preg_match('/^116.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '116999' || $chart['header'] == 1) {
                    } else {
                        $current_assets += $chart['after'];
                        $arr_current_assets[] = $chart;
                    }
                } elseif (preg_match('/^12.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '129999' || $chart['header'] == 1) {
                    } else {
                        $fixed_assets += $chart['after'];
                        $arr_fixed_assets[] = $chart;
                    }
                } elseif (preg_match('/^2[0-1][0-1].*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '211999' || $chart['header'] == 1) {
                    } else {
                        $chart['after'] = -$chart['after'];
                        $current_liability += $chart['after'];
                        $arr_current_liability[] = $chart;
                    }
                } elseif (preg_match('/^212.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '212999' || $chart['header'] == 1) {
                    } else {
                        $chart['after'] = -$chart['after'];
                        $current_liability += $chart['after'];
                        $arr_current_liability[] = $chart;
                    }
                } elseif (preg_match('/^2[2-9].*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '229999' || $chart['account_code'] == '299999'
                        || $chart['header'] == 1
                    ) {
                    } else {
                        $chart['after'] = -$chart['after'];
                        $longterm_liability += $chart['after'];
                        $arr_longterm_liability[] = $chart;
                    }
                } elseif (preg_match('/^3.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '399999' || $chart['header'] == 1) {
                    } else {
                        $chart['after'] = -$chart['after'];
                        $equity += $chart['after'];
                        $arr_equity[] = $chart;
                    }
                }
            }
            $count_assets = count($arr_current_assets);
            $count_current_liability = count($arr_current_liability);
            $count_fixed_assets = count($arr_fixed_assets);
            $count_longterm_liability = count($arr_longterm_liability);
            if ($count_assets >= $count_current_liability) {
                for ($i = 0; $i <= $count_assets; $i++) {
                    $arr_current_assets[$i]['account_code_assets'] = $arr_current_assets[$i]['account_code'];
                    $arr_current_assets[$i]['account_assets'] = $arr_current_assets[$i]['account_name'];
                    $arr_current_assets[$i]['assets'] = $arr_current_assets[$i]['after'];
                    if ($i > $count_current_liability) {
                        $arr_current_assets[$i]['account_code'] = null;
                        $arr_current_assets[$i]['account_liabilities'] = null;
                        $arr_current_assets[$i]['liabilities'] = null;
                    } else {
                        $arr_current_assets[$i]['account_code'] = $arr_current_liability[$i]['account_code'];
                        $arr_current_assets[$i]['account_liabilities'] = $arr_current_liability[$i]['account_name'];
                        $arr_current_assets[$i]['liabilities'] = $arr_current_liability[$i]['after'];
                    }
                }
            } else {
                for ($i = 0; $i <= $count_current_liability; $i++) {
                    $arr_current_assets[$i]['account_code'] = $arr_current_liability[$i]['account_code'];
                    $arr_current_assets[$i]['account_liabilities'] = $arr_current_liability[$i]['account_name'];
                    $arr_current_assets[$i]['liabilities'] = $arr_current_liability[$i]['after'];
                    if ($i > $count_assets) {
                        $arr_current_assets[$i]['account_code_assets'] = null;
                        $arr_current_assets[$i]['account_assets'] = null;
                        $arr_current_assets[$i]['assets'] = null;
                    } else {
                        $arr_current_assets[$i]['account_code_assets'] = $arr_current_assets[$i]['account_code'];
                        $arr_current_assets[$i]['account_assets'] = $arr_current_assets[$i]['account_name'];
                        $arr_current_assets[$i]['assets'] = $arr_current_assets[$i]['after'];
                    }
                }
            }
            if ($count_fixed_assets >= $count_longterm_liability) {
                for ($i = 0; $i <= $count_fixed_assets; $i++) {
                    $arr_fixed_assets[$i]['account_code_assets'] = $arr_fixed_assets[$i]['account_code'];
                    $arr_fixed_assets[$i]['account_assets'] = $arr_fixed_assets[$i]['account_name'];
                    $arr_fixed_assets[$i]['assets'] = $arr_fixed_assets[$i]['after'];
                    if ($i > $count_longterm_liability) {
                        $arr_fixed_assets[$i]['account_code'] = null;
                        $arr_fixed_assets[$i]['account_liabilities'] = null;
                        $arr_fixed_assets[$i]['liabilities'] = null;
                    } else {
                        $arr_fixed_assets[$i]['account_code'] = $arr_longterm_liability[$i]['account_code'];
                        $arr_fixed_assets[$i]['account_liabilities'] = $arr_longterm_liability[$i]['account_name'];
                        $arr_fixed_assets[$i]['liabilities'] = $arr_longterm_liability[$i]['after'];
                    }
                }
            } else {
                for ($i = 0; $i <= $count_longterm_liability; $i++) {
                    $arr_fixed_assets[$i]['account_code'] = $arr_longterm_liability[$i]['account_code'];
                    $arr_fixed_assets[$i]['account_liabilities'] = $arr_longterm_liability[$i]['account_name'];
                    $arr_fixed_assets[$i]['liabilities'] = $arr_longterm_liability[$i]['after'];
                    if ($i > $count_fixed_assets) {
                        $arr_fixed_assets[$i]['account_code_assets'] = null;
                        $arr_fixed_assets[$i]['account_assets'] = null;
                        $arr_fixed_assets[$i]['assets'] = null;
                    } else {
                        $arr_fixed_assets[$i]['account_code_assets'] = $arr_fixed_assets[$i]['account_code'];
                        $arr_fixed_assets[$i]['account_assets'] = $arr_fixed_assets[$i]['account_name'];
                        $arr_fixed_assets[$i]['assets'] = $arr_fixed_assets[$i]['after'];
                    }
                }
            }
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'Neraca.xlsx',
                OPENTBS_ALREADY_UTF8);
            $this->TBS->MergeField('header', array(
                'to' => date_format($dt_frm, 'd F Y'),
                'current_assets' => $current_assets,
                'current_liabilities' => $current_liability,
                'longterm_liabilities' => $longterm_liability,
                'fixed_assets' => $fixed_assets,
                'equity' => $equity,
                'activa' => $current_assets + $fixed_assets,
                'passiva' => $current_liability + $longterm_liability + $equity
            ));
            $this->TBS->MergeBlock('assets_liabilities', $arr_current_assets);
            $this->TBS->MergeBlock('assets_liabilities_fixed', $arr_fixed_assets);
            $this->TBS->MergeBlock('equity', $arr_equity);
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Neraca" . $to . ".xlsx");
        }
    }
    public function actionNeraca()
    {
        $to = $_POST['to_date'];
//            $dt = strtotime($_POST['to_date']);
//        $dt_frm = date_create($to);
//        $from = '0000-00-00';//date_format($dt_frm, 'Y-01-01');
        $store = $_POST['store'];
        $result = self::create_neraca_new($to, $store);
        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'nr.xlsx');
        $this->TBS->MergeBlock('header', $result['header']);
        $this->TBS->MergeBlock('current', $result['current']);
        $this->TBS->MergeBlock('fixed', $result['fixed']);
        $this->TBS->MergeBlock('equity', $result['equity']);
        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "NERACA" . $to . ".xls");
    }
    public function actionBalanceSheet()
    {
        $from = $_POST['from_date'];
        $to = $_POST['to_date'];
        $store = $_POST['store'];
        $result = self::createBalanceSheet($from, $to, $store);
        $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'BalanceSheetNew.xml');
        $this->TBS->MergeBlock('header', $result['header']);
        $this->TBS->MergeField('total_activa', $result['total_activa']);
        $this->TBS->MergeField('total_passiva', $result['total_passiva']);
        $this->TBS->MergeBlock('current_l', $result['current_l']);
        $this->TBS->MergeBlock('fixed_l', $result['fixed_l']);
        $this->TBS->MergeBlock('current', $result['current']);
        $this->TBS->MergeBlock('fixed', $result['fixed']);
        $this->TBS->MergeBlock('equity', $result['equity']);
        $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "BalanceSheet" . $from . $to . $store . ".xls");
    }
    public function actionBalanceSheetOld()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['from_date'];
            $to = $_POST['to_date'];
            $store = $_POST['store'];
            $lr = ChartMaster::get_neraca($from, $to, $store);
            $assets = $bank = $tr = $beban_administrasi = $supplies = $ptax
                = $fassets = $beban_lain = $pexpenses = $liability = $taxpay = $equity = $ltliability = array();
            $total_assets = $total_bank = $total_tr = $total_beban_administrasi = $total_supplies
                = $total_ptax = $total_fassets = $total_beban_lain = $total_pexpenses = $total_liability
                = $total_taxpay = $total_equity = $total_ltliability = 0;
            $debit_assets = $debit_bank = $debit_tr = $debit_beban_administrasi = $debit_supplies
                = $debit_ptax = $debit_fassets = $debit_beban_lain = $debit_pexpenses = $debit_liability
                = $debit_taxpay = $debit_equity = $debit_ltliability = 0;
            $kredit_assets = $kredit_bank = $kredit_tr = $kredit_beban_administrasi = $kredit_supplies
                = $kredit_ptax = $kredit_fassets = $kredit_beban_lain = $kredit_pexpenses = $kredit_liability
                = $kredit_taxpay = $kredit_equity = $kredit_ltliability = 0;
            $after_assets = $after_bank = $after_tr = $after_beban_administrasi = $after_supplies
                = $after_ptax = $after_fassets = $after_beban_lain = $after_pexpenses = $after_liability
                = $after_taxpay = $after_equity = $after_ltliability = 0;
            $total_cash_account_code = $total_cash_account_name = $total_bank_account_code = $total_bank_account_name =
            $total_tr_account_code = $total_tr_account_name = $total_supplies_account_code =
            $total_supplies_account_name = $total_pexpenses_account_code = $total_pexpenses_account_name =
            $total_fassets_account_code = $total_fassets_account_name = $total_ptax_account_code = $total_ptax_account_name =
            $total_crliability_account_code = $total_crliability_account_name = $total_liability_account_code =
            $total_liability_account_name = $total_taxpay_account_code = $total_taxpay_account_name =
            $total_equity_account_code = $total_equity_account_name = $total_ltliability_account_code =
            $total_ltliability_account_name = "";
            foreach ($lr as $chart) {
                if (preg_match('/^1[0-1][0-1].*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '111999') {
                        $total_cash_account_code = $chart['account_code'];
                        $total_cash_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $assets[] = $chart;
                    } else {
                        $total_assets += $chart['before'];
                        $debit_assets += $chart['debit'];
                        $kredit_assets += $chart['kredit'];
                        $after_assets += $chart['after'];
                        $assets[] = $chart;
                    }
                } elseif (preg_match('/^112.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '112999') {
                        $total_bank_account_code = $chart['account_code'];
                        $total_bank_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $bank[] = $chart;
                    } else {
                        $total_bank += $chart['before'];
                        $debit_bank += $chart['debit'];
                        $kredit_bank += $chart['kredit'];
                        $after_bank += $chart['after'];
                        $bank[] = $chart;
                    }
                } elseif (preg_match('/^113.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '113999') {
                        $total_tr_account_code = $chart['account_code'];
                        $total_tr_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $tr[] = $chart;
                    } else {
                        $total_tr += $chart['before'];
                        $debit_tr += $chart['debit'];
                        $kredit_tr += $chart['kredit'];
                        $after_tr += $chart['after'];
                        $tr[] = $chart;
                    }
                } elseif (preg_match('/^114.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '114199') {
                        $total_supplies_account_code = $chart['account_code'];
                        $total_supplies_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $supplies[] = $chart;
                    } else {
                        $total_supplies += $chart['before'];
                        $debit_supplies += $chart['debit'];
                        $kredit_supplies += $chart['kredit'];
                        $after_supplies += $chart['after'];
                        $supplies[] = $chart;
                    }
                } elseif (preg_match('/^115.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '115999') {
                        $total_ptax_account_code = $chart['account_code'];
                        $total_ptax_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $ptax[] = $chart;
                    } else {
                        $total_ptax += $chart['before'];
                        $debit_ptax += $chart['debit'];
                        $kredit_ptax += $chart['kredit'];
                        $after_ptax += $chart['after'];
                        $ptax[] = $chart;
                    }
                } elseif (preg_match('/^116.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '116999') {
                        $total_pexpenses_account_code = $chart['account_code'];
                        $total_pexpenses_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $pexpenses[] = $chart;
                    } else {
                        $total_pexpenses += $chart['before'];
                        $debit_pexpenses += $chart['debit'];
                        $kredit_pexpenses += $chart['kredit'];
                        $after_pexpenses += $chart['after'];
                        $pexpenses[] = $chart;
                    }
                } elseif (preg_match('/^12.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '129999') {
                        $total_fassets_account_code = $chart['account_code'];
                        $total_fassets_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $fassets[] = $chart;
                    } else {
                        $total_fassets += $chart['before'];
                        $debit_fassets += $chart['debit'];
                        $kredit_fassets += $chart['kredit'];
                        $after_fassets += $chart['after'];
                        $fassets[] = $chart;
                    }
                } elseif (preg_match('/^2[0-1][0-1].*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '211999') {
                        $total_crliability_account_code = $chart['account_code'];
                        $total_crliability_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $liability[] = $chart;
                    } else {
                        $total_liability += $chart['before'];
                        $debit_liability += $chart['debit'];
                        $kredit_liability += $chart['kredit'];
                        $after_liability += $chart['after'];
                        $liability[] = $chart;
                    }
                } elseif (preg_match('/^212.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '212999') {
                        $total_taxpay_account_code = $chart['account_code'];
                        $total_taxpay_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $taxpay[] = $chart;
                    } else {
                        $total_taxpay += $chart['before'];
                        $debit_taxpay += $chart['debit'];
                        $kredit_taxpay += $chart['kredit'];
                        $after_taxpay += $chart['after'];
                        $taxpay[] = $chart;
                    }
                } elseif (preg_match('/^2[2-9].*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '229999') {
                        $total_ltliability_account_code = $chart['account_code'];
                        $total_ltliability_account_name = $chart['account_name'];
                    } elseif ($chart['account_code'] == '299999') {
                        $total_liability_account_code = $chart['account_code'];
                        $total_liability_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $ltliability[] = $chart;
                    } else {
                        $total_ltliability += $chart['before'];
                        $debit_ltliability += $chart['debit'];
                        $kredit_ltliability += $chart['kredit'];
                        $after_ltliability += $chart['after'];
                        $ltliability[] = $chart;
                    }
                } elseif (preg_match('/^3.*/', $chart['account_code'])) {
                    if ($chart['account_code'] == '399999') {
                        $total_equity_account_code = $chart['account_code'];
                        $total_equity_account_name = $chart['account_name'];
                    } elseif ($chart['header'] == 1) {
                        $chart['before'] = $chart['debit'] = $chart['kredit'] = $chart['after'] = null;
                        $equity[] = $chart;
                    } else {
                        $total_equity += $chart['before'];
                        $debit_equity += $chart['debit'];
                        $kredit_equity += $chart['kredit'];
                        $after_equity += $chart['after'];
                        $equity[] = $chart;
                    }
                }
            }
            $total_activa = $after_assets + $after_bank + $after_tr + $after_supplies + $after_ptax
                + $after_pexpenses + $after_fassets;
            $after_li = $after_liability + $after_taxpay + $after_ltliability;
            $total_li = $total_liability + $total_taxpay + $total_ltliability;
            $debit_li = $debit_liability + $debit_taxpay + $debit_ltliability;
            $kredit_li = $kredit_liability + $kredit_taxpay + $kredit_ltliability;
            $total_pasiva = $after_li + $after_equity;
            $selisih = $total_activa + $total_pasiva;
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'BalanceSheet.xml');
            $this->TBS->MergeBlock('header', array(
                array(
                    'from' => $from,
                    'to' => $to,
                    'total_cash_before' => $total_assets,
                    'total_cash_debit' => $debit_assets,
                    'total_cash_kredit' => $kredit_assets,
                    'total_cash_after' => $after_assets,
                    'total_bank_before' => $total_bank,
                    'total_bank_debit' => $debit_bank,
                    'total_bank_kredit' => $kredit_bank,
                    'total_bank_after' => $after_bank,
                    'total_tr_before' => $total_tr,
                    'total_tr_debit' => $debit_tr,
                    'total_tr_kredit' => $kredit_tr,
                    'total_tr_after' => $after_tr,
                    'total_supplies_before' => $total_supplies,
                    'total_supplies_debit' => $debit_supplies,
                    'total_supplies_kredit' => $kredit_supplies,
                    'total_supplies_after' => $after_supplies,
                    'total_ptax_before' => $total_ptax,
                    'total_ptax_debit' => $debit_ptax,
                    'total_ptax_kredit' => $kredit_ptax,
                    'total_ptax_after' => $after_ptax,
                    'total_pexpenses_before' => $total_pexpenses,
                    'total_pexpenses_debit' => $debit_pexpenses,
                    'total_pexpenses_kredit' => $kredit_pexpenses,
                    'total_pexpenses_after' => $after_pexpenses,
                    'total_fassets_before' => $total_fassets,
                    'total_fassets_debit' => $debit_fassets,
                    'total_fassets_kredit' => $kredit_fassets,
                    'total_fassets_after' => $after_fassets,
                    'total_crliability_before' => $total_liability,
                    'total_crliability_debit' => $debit_liability,
                    'total_crliability_kredit' => $kredit_liability,
                    'total_crliability_after' => $after_liability,
                    'total_taxpay_before' => $total_taxpay,
                    'total_taxpay_debit' => $debit_taxpay,
                    'total_taxpay_kredit' => $kredit_taxpay,
                    'total_taxpay_after' => $after_taxpay,
                    'total_ltliability_before' => $total_ltliability,
                    'total_ltliability_debit' => $debit_ltliability,
                    'total_ltliability_kredit' => $kredit_ltliability,
                    'total_ltliability_after' => $after_ltliability,
                    'total_liability_before' => $total_li,
                    'total_liability_debit' => $debit_li,
                    'total_liability_kredit' => $kredit_li,
                    'total_liability_after' => $after_li,
                    'total_equity_before' => $total_equity,
                    'total_equity_debit' => $debit_equity,
                    'total_equity_kredit' => $kredit_equity,
                    'total_equity_after' => $after_equity,
                    'total_activa' => $total_activa,
                    'total_passiva' => $total_pasiva,
                    'selisih' => $selisih,
                    'total_cash_account_code' => $total_cash_account_code,
                    'total_cash_account_name' => $total_cash_account_name,
                    'total_bank_account_code' => $total_bank_account_code,
                    'total_bank_account_name' => $total_bank_account_name,
                    'total_tr_account_code' => $total_tr_account_code,
                    'total_tr_account_name' => $total_tr_account_name,
                    'total_supplies_account_code' => $total_supplies_account_code,
                    'total_supplies_account_name' => $total_supplies_account_name,
                    'total_ptax_account_code' => $total_ptax_account_code,
                    'total_ptax_account_name' => $total_ptax_account_name,
                    'total_pexpenses_account_code' => $total_pexpenses_account_code,
                    'total_pexpenses_account_name' => $total_pexpenses_account_name,
                    'total_fassets_account_code' => $total_fassets_account_code,
                    'total_fassets_account_name' => $total_fassets_account_name,
                    'total_crliability_account_code' => $total_crliability_account_code,
                    'total_crliability_account_name' => $total_crliability_account_name,
                    'total_taxpay_account_code' => $total_taxpay_account_code,
                    'total_taxpay_account_name' => $total_taxpay_account_name,
                    'total_ltliability_account_code' => $total_ltliability_account_code,
                    'total_ltliability_account_name' => $total_ltliability_account_name,
                    'total_liability_account_code' => $total_liability_account_code,
                    'total_liability_account_name' => $total_liability_account_name,
                    'total_equity_account_code' => $total_equity_account_code,
                    'total_equity_account_name' => $total_equity_account_name
                )
            ));
            $this->TBS->MergeBlock('cash', $assets);
            $this->TBS->MergeBlock('bank', $bank);
            $this->TBS->MergeBlock('tr', $tr);
            $this->TBS->MergeBlock('supplies', $supplies);
            $this->TBS->MergeBlock('ptax', $ptax);
            $this->TBS->MergeBlock('pexpenses', $pexpenses);
            $this->TBS->MergeBlock('fassets', $fassets);
            $this->TBS->MergeBlock('liability', $liability);
            $this->TBS->MergeBlock('taxpay', $taxpay);
            $this->TBS->MergeBlock('ltliability', $ltliability);
            $this->TBS->MergeBlock('equity', $equity);
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "BalanceSheet" . $from . $to . ".xls");
        }
    }
    public function actionViewCustomerHistory()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $customer_id = $_POST['customer_id'];
            $tgl = $_POST['tglfrom'];
            $cust = U::report_view_customer_history($customer_id, $tgl);
            $customer = Customers::model()->findByPk($customer_id);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'newcustomers.xml');
            } else {
//                $this->renderJsonArr($cust);
//                Yii::app()->end();
                $dataProvider = new CArrayDataProvider($cust, array(
                    'id' => 'user',
                    'pagination' => false
                ));
                $this->render('ViewCustomerHistory',
                    array('dp' => $dataProvider, 'start' => sql2date($tgl, 'dd MMM yyyy'),'pt_negara' => PT_NEGARA, 'cust' => $customer));
                Yii::app()->end();
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'view_customer_history.htm');
            }
            $this->TBS->MergeBlock('header',
                array(array('logo' => $this->logo, 'customer' => $customer->nama_customer)));
            $this->TBS->MergeBlock('history', $cust);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "ViewCustomerHistory_$customer->nama_customer.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionTenders()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $store = $_POST['store'];
            $tenders = U::report_tenders($from, $store);
            $sum_system = array_sum(array_column($tenders, 'system'));
            $sum_tenders = array_sum(array_column($tenders, 'tenders'));
            $selisih = 0;
            $status = "";
            if ($sum_system >= $sum_tenders) {
                $selisih = $sum_system - $sum_tenders;
                $status = "Short";
            } elseif ($sum_system < $sum_tenders) {
                $selisih = $sum_tenders - $sum_system;
                $status = "Over";
            }
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($tenders, array(
                'id' => 'Tenders',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Tenders_$from.xls");
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'tenders.xml');
                echo $this->render('Tenders', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'tender' => $status,
                    'selisih' => $selisih,
                    'store' => $store
                ), true);
            } else {
                $this->render('Tenders', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'tender' => $status,
                    'selisih' => $selisih,
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'tenders.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'branch' => $store, 'start' => $from)));
//            $this->TBS->MergeBlock('tender', $tenders);
//            $this->TBS->MergeBlock('s', array(array('tender' => $status, 'selisih' => $selisih)));
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Tenders_$from.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionNewCustomers()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $cust = U::report_new_customers($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($cust, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=newcustomers$from-$to.xls");
                echo $this->render('NewCustomers', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ), true);
            } else {
                $this->render('NewCustomers', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ));
            }
        }
    }
    public function actionRealCustomers()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $cust = U::report_real_customers($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($cust, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=realcustomers$from-$to.xls");
                echo $this->render('RealCustomers', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ), true);
            } else {
                $this->render('RealCustomers', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ));
            }
        }
    }
    public function actionCategoryCustomers()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $store = $_POST['store'];
            $kategori = $_POST['kategori'];
            $kat_name = "";
            $cust = U::report_kategori_customer($kategori, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            switch ($kategori) {
                case 1 :
                    $kat_name = "MOST ACTIVE";
                    break;
                case 2 :
                    $kat_name = "VERY ACTIVE";
                    break;
                case 3 :
                    $kat_name = "ACTIVE PATIENT";
                    break;
                case 4 :
                    $kat_name = "PASSIVE PATIENT";
                    break;
            }
            $dataProvider = new CArrayDataProvider($cust, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=KategoriCustomer.xls");
                echo $this->render('KategoriCustomer', array(
                    'dp' => $dataProvider,
                    'kat' => $kat_name,
                    'store' => $store
                ), true);
            } else {
                $this->render('KategoriCustomer', array(
                    'dp' => $dataProvider,
                    'kat' => $kat_name,
                    'store' => $store
                ));
            }
        }
    }
    public function actionBirthDayCustomers()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $cust = U::report_birthday_customers($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($cust, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=BirthDayCustomers$from-$to.xls");
                echo $this->render('BirthDayCustomers', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ), true);
            } else {
                $this->render('BirthDayCustomers', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ));
            }
        }
    }
    public function actionCashOut()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'biaya.xml');
            } else {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'biaya.htm');
            }
            $biaya = U::report_biaya($from, $to);
            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
            $this->TBS->MergeBlock('cash', $biaya);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "cashout$from-$to.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionInventoryMovements()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $grup_id = $_POST['grup_id'];
            $tag_id = $_POST['tag_id'];
            $mutasi = U::report_mutasi_stok($from, $to, $store, $grup_id, $tag_id);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($store == "") {
                $store = "All Branch";
            }
            $this->pageTitle = 'Inventory Movements';
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryMovements$from-$to.xls");
                echo $this->render('InventoryMovements', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'mutasi_stok.xml');
            } else {
                $this->render('InventoryMovements', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ));
            }
        }
    }
    public function actionInventoryMovementsPerlengkapan()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $grup_id = $_POST['grup_id'];
            $tag_id = $_POST['tag_id'];
            $mutasi = U::report_mutasi_stok_perlengkapan($from, $to, $store, $grup_id, $tag_id);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'InventoryMovements',
                'pagination' => false
            ));
            if ($store == "") {
                $store = "All Branch";
            }
            $this->pageTitle = 'Inventory Movements Perlengkapan';
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryMovementsPerlengkapan$from-$to.xls");
                echo $this->render('InventoryMovements', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'mutasi_stok.xml');
            } else {
                $this->render('InventoryMovements', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ));
            }
        }
    }
	public function actionReportRekapPenjualanplusdiscamount()
	{
		if (Yii::app()->request->isAjaxRequest) {
			return;
		}
		if (isset($_POST) && !empty($_POST)) {
			$from = $_POST['tglfrom'];
			$to = $_POST['tglto'];
			$store = $_POST['store'];
			$grup_id = $_POST['grup_id'];
			$mutasi = U::report_rekap_penj_discamoount($from, $to, $grup_id, $store);
			$dataProvider = new CArrayDataProvider($mutasi, array(
				'id' => 'RekapPenjualanDiscAmount',
				'pagination' => false
			));
			if ($store == "") {
				$store = "All Branch";
			}
			$grup_name = '';
			if ($grup_id != null) {
				$grup = Grup::model()->findByPk($grup_id);
				$grup_name = $grup->nama_grup;
			}
			if ($grup_name == "") {
				$grup_name = "All Group";
			}
			if ($this->format == 'excel') {
				header('Content-type: application/vnd.xls');
				header("Content-Disposition: attachment; filename=RekapPenjualan$from-$to.xls");
				echo $this->render('RekapPerawatan', array(
					'dp' => $dataProvider,
					'from' => sql2date($from, 'dd MMM yyyy'),
					'to' => sql2date($to, 'dd MMM yyyy'),
					'grup' => $grup_name,
					'store' => $store
				), true);
			} else {
				$this->render('RekapPerawatan', array(
					'dp' => $dataProvider,
					'from' => sql2date($from, 'dd MMM yyyy'),
					'to' => sql2date($to, 'dd MMM yyyy'),
					'grup' => $grup_name,
					'store' => $store
				));
			}
		}
	}
    public function actionRekapPerawatan()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $grup_id = $_POST['grup_id'];
            $mutasi = U::report_rekap_perawatan($from, $to, $grup_id, $store);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'RekapPerawatan',
                'pagination' => false
            ));
            if ($store == "") {
                $store = "All Branch";
            }
            $grup_name = '';
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            if ($grup_name == "") {
                $grup_name = "All Group";
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapPenjualan$from-$to.xls");
                echo $this->render('RekapPerawatan', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'store' => $store
                ), true);
            } else {
                $this->render('RekapPerawatan', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'store' => $store
                ));
            }
        }
    }
    public function actionInventoryMovementsClinical()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $kategori_clinical_id = $_POST['kategori_clinical_id'];
            $grup_name = 'All Category Clinical';
            $mutasi = U::report_mutasi_stok_clinical($from, $to, $kategori_clinical_id, $store);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'InventoryMovementsClinical',
                'pagination' => false
            ));
            if ($store == "") {
                $store = "All Branch";
            }
            if ($kategori_clinical_id != null) {
                $grup = KategoriClinical::model()->findByPk($kategori_clinical_id);
                $grup_name = $grup->nama_kategori;
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryMovementsClinical$from-$to.xls");
                echo $this->render('InventoryMovementClinical', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store,
                    'grup_name' => $grup_name
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'mutasi_stok.xml');
            } else {
                $this->render('InventoryMovementClinical', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store,
                    'grup_name' => $grup_name
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'mutasi_stok.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
//            $this->TBS->MergeBlock('mutasi', $mutasi);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InventoryMovements$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionInventoryCard()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $barang_id = $_POST['barang_id'];
            $store = $_POST['store'];
            $barang = Barang::model()->findByPk($barang_id);
            $saldo_awal = StockMoves::get_saldo_item_before($barang->kode_barang, $from, $store);
            $row = U::report_kartu_stok($barang->kode_barang, $from, $to, $store);
            $stock_card = array();
            foreach ($row as $newrow) {
                $newrow['before'] = $saldo_awal;
                $newrow['after'] += $newrow['before'] + $newrow['qty'];
                $saldo_awal = $newrow['after'];
                $stock_card[] = $newrow;
            }
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($stock_card, array(
                'id' => 'InventoryCard',
                'pagination' => false
            ));
            $this->pageTitle = 'Inventory Card';
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryCard$from-$to.xls");
                echo $this->render('InventoryCard', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'pt_negara' => PT_NEGARA,
                    'item' => $barang,
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.xml');
            } else {
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.htm');
                $this->render('InventoryCard', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'pt_negara' => PT_NEGARA,
                    'item' => $barang,
                    'store' => $store
                ));
//                Yii::app()->end();
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to, 'item' => $barang->nama_barang)));
//            $this->TBS->MergeBlock('mutasi', $stock_card);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InventoryCard$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionInventoryCardPerlengkapan()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $barang_id = $_POST['barang_id'];
            $store = $_POST['store'];
            $barang = Barang::model()->findByPk($barang_id);
            $saldo_awal = StockMoves::get_saldo_item_before($barang->kode_barang, $from, $store);
            $row = U::report_kartu_stok_perlengkapan($barang->kode_barang, $from, $to, $store);
            $stock_card = array();
            foreach ($row as $newrow) {
                $newrow['before'] = $saldo_awal;
                $newrow['after'] += $newrow['before'] + $newrow['qty'];
                $saldo_awal = $newrow['after'];
                $stock_card[] = $newrow;
            }
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($stock_card, array(
                'id' => 'InventoryCard',
                'pagination' => false
            ));
            $this->pageTitle = 'Inventory Card Perlengkapan';
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InventoryCard$from-$to.xls");
                echo $this->render('InventoryCard', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'item' => $barang,
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.xml');
            } else {
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'kartu_stok.htm');
                $this->render('InventoryCard', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'item' => $barang,
                    'store' => $store
                ));
//                Yii::app()->end();
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to, 'item' => $barang->nama_barang)));
//            $this->TBS->MergeBlock('mutasi', $stock_card);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "InventoryCard$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionBeautySummary()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_beauty_summary($from, $to, $store);
            $total_tip = array_sum(array_column($summary, 'tip'));
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'BeautySummary',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=BeautySummary$from-$to.xls");
                echo $this->render('BeautySummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'tip' => $total_tip,
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'beauty_rekap.xml');
            } else {
                $this->render('BeautySummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'tip' => $total_tip,
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'beauty_rekap.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
//            $this->TBS->MergeBlock('beauty', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "BeautySummary$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionReportJasaBeauty()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $detail = U::report_beauty_detail($from, $to, $store);
            $totalitem = U::report_beauty_totalitem($from, $to, $store);
            $totalall = U::report_beauty_totalall($from, $to, $store);
            $totalkmr = U::report_kmr_total($from, $to, $store);
            $totalkmritem = U::report_kmritem($from, $to, $store);
            $totalkmt = U::report_kmt($from, $to, $store);
            $this->render('R_Jasabeauty', array(
                'detail' => $detail,
                'totalitem' => $totalitem,
                'totalall' => $totalall,
                'totalkmr' => $totalkmr,
                'totalkmritem' => $totalkmritem,
                'totalkmt' => $totalkmt,
                'from' => sql2date($from, 'dd MMM yyyy'),
                'to' => sql2date($to, 'dd MMM yyyy'),
                'store' => $store
            ));
        }
    }
    public function actionReportTransaksi()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $detail = U::report_transaksi($from, $to, $store);
            $cream = U::report_total_item($from, $to, false, $store);
            $prwt = U::report_total_item($from, $to, true, $store);
            $total = U::report_total_transaksi($from, $to, $store);
            $detail_all = U::report_detail_transaksi($from, $to, $store);
            $this->render('R_transaksi', array(
                'from' => sql2date($from, 'dd MMM yyyy'),
                'to' => sql2date($to, 'dd MMM yyyy'),
                'detail' => $detail,
                'detail_all' => $detail_all,
                'cream' => $cream,
                'perawatan' => $prwt,
                'total' => $total,
                'store' => $store
            ));
        }
    }
    public function actionDokterSummary()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_dokter_summary($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'DokterSummary',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=DokterSummary$from-$to.xls");
                echo $this->render('DokterSummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_rekap.xml');
            } else {
                $this->render('DokterSummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_rekap.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
//            $this->TBS->MergeBlock('dokter', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "DokterSummary$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionDokterDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_dokter_details($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'DokterDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=DoctorsDetails$from-$to.xls");
                echo $this->render('DokterDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'pt_negara' => PT_NEGARA,
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_details.xml');
            } else {
                $this->render('DokterDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'pt_negara' => PT_NEGARA,
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_details.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
//            $this->TBS->MergeBlock('dokter', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "DokterDetails$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionBeautyDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_beauty_details($from, $to, $store);
            $total_beauty_tip = array_sum(array_column($summary, 'beauty_tip'));
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'BeautyDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=BeautyDetails$from-$to.xls");
                echo $this->render('BeautyDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'total_beauty_tip' => $total_beauty_tip,
                    'pt_negara' => PT_NEGARA,
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'beauty_details.xml');
            } else {
                $this->render('BeautyDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'total_beauty_tip' => $total_beauty_tip,
                    'pt_negara' => PT_NEGARA,
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'beauty_details.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
//            $this->TBS->MergeBlock('beauty', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "BeautyDetails$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionSalesSummary()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $grup_id = $_POST['grup_id'];
            $store = $_POST['store'];
            $grup_name = '';
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            $summary = Users::is_audit() ? U::report_audit_summary($grup_id, $from,
                $to) : U::report_sales_summary($grup_name, $from, $to, $store);
            $total = array_sum(array_column($summary, 'total'));
            $qty = array_sum(array_column($summary, 'qty'));
            if ($grup_name == "") {
                $grup_name = "All Group";
            }
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'SalesSummary',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_rekap.xml');
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesSummary$from-$to.xls");
                echo $this->render('SalesSummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'pt_negara' => PT_NEGARA,
                    'qty' => $qty,
                    'total' => $total,
                    'store' => $store
                ), true);
            } else {
                $this->render('SalesSummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'pt_negara' => PT_NEGARA,
                    'total' => $total,
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_rekap.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to, 'grup' => $grup_name)));
//            $this->TBS->MergeBlock('sales', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummary$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionSalesSummaryReceipt()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_sales_summary_receipt($from, $to, $store);
            $query = U::report_sales_summary_receipt_amount_total($from, $to, $store);
            $total = $query->total;
            $amount = array_sum(array_column($summary, 'amount'));
            $kembali = array_sum(array_column($summary, 'kembali'));
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesSummaryReceipt$from.xls");
                echo $this->render('SalesSummaryReceipt',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'total' => $total,
                        'amount' => $amount,
                        'pt_negara' => PT_NEGARA,
                        'kembali' => $kembali,
                        'store' => $store
                    ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $this->render('SalesSummaryReceipt',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'total' => $total,
                        'amount' => $amount,
                        'kembali' => $kembali,
                        'pt_negara' => PT_NEGARA,
                        'store' => $store
                    ));
//                $this->renderJsonArr($summary);
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from,)));
//            $this->TBS->MergeBlock('sales', $summary);
//            $this->TBS->MergeBlock('s', array(array('total' => $total, 'amount' => $amount)));
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummaryReceipt$from.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionSalesSummaryReceiptDetails()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_sales_summary_receipt_details($from, $to, $store);
            $total = array_sum(array_column($summary, 'total'));
            $bruto = array_sum(array_column($summary, 'bruto'));
            $vatrp = array_sum(array_column($summary, 'vatrp'));
            $qty = array_sum(array_column($summary, 'qty'));
            $discrp = array_sum(array_column($summary, 'discrp'));
            $price = array_sum(array_column($summary, 'price'));    
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesSummaryReceiptDetails$from-$to.xls");
                echo $this->render('SalesSummaryReceiptDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'qty' => $qty,
                        'total' => $total,
                        'pt_negara' => PT_NEGARA,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $this->render('SalesSummaryReceiptDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'qty' => $qty,
                        'pt_negara' => PT_NEGARA,
                        'total' => $total,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ));
//                $this->renderJsonArr($summary);
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from,)));
//            $this->TBS->MergeBlock('sales', $summary);
//            $this->TBS->MergeBlock('s', array(array('total' => $total)));
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummaryReceiptDetails$from.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionReturSalesSummary()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $grup_id = $_POST['grup_id'];
            $store = $_POST['store'];
            $grup_name = '';
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            $summary = U::report_retursales_summary($grup_name, $from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            if ($grup_name == "") {
                $grup_name = "All Group";
            }
            $total = array_sum(array_column($summary, 'total'));
            $qty = array_sum(array_column($summary, 'qty'));
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'ReturSalesSummary',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesSummary$from-$to.xls");
                echo $this->render('ReturSalesSummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'pt_negara' => PT_NEGARA,
                    'qty' => $qty,
                    'total' => $total,
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'retursales_rekap.xml');
            } else {
                $this->render('ReturSalesSummary', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'pt_negara' => PT_NEGARA,
                    'total' => $total,
                    'store' => $store
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'retursales_rekap.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to, 'grup' => $grup_name)));
//            $this->TBS->MergeBlock('sales', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummary$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionSalesDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $grup_id = $_POST['grup_id'];
            $real = 0;
            if (isset($_POST['real'])) {
                $real = $_POST['real'];
            }
//            $grup_name = 'All Group';
            $store = $_POST['store'];
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            $summary = Users::is_audit() ? U::report_audit_details($grup_id, $from,
                $to) : U::report_sales_details($grup_name, $from, $to, $real, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            if ($grup_name == "") {
                $grup_name = "All Group";
            }
            $total = array_sum(array_column($summary, 'total'));
            $vatrp = array_sum(array_column($summary, 'vatrp'));
            $bruto = array_sum(array_column($summary, 'bruto'));
            $qty = array_sum(array_column($summary, 'qty'));
            $discrp = array_sum(array_column($summary, 'discrp'));
            $price = array_sum(array_column($summary, 'price'));
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'SalesDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesDetails$from-$to.xls");
                echo $this->render('SalesDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'total' => $total,
                    'discrp' => $discrp,
                    'pt_negara' => PT_NEGARA,
                    'price' => $price,
                    'vatrp' => $vatrp,
                    'store' => $store,
                    'bruto' => $bruto
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.xml');
            } else {
                $this->render('SalesDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'total' => $total,
                    'discrp' => $discrp,
                    'price' => $price,
                    'vatrp' => $vatrp,
                    'pt_negara' => PT_NEGARA,
                    'store' => $store,
                    'bruto' => $bruto
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => sql2date($to, 'dd MMM yyyy'), 'grup' => $grup_name)));
//            $this->TBS->MergeBlock('sales', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesDetails$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionSalesItemDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $barang_id = $_POST['barang_id'];
            $tcom= $_POST['tcom'];
            $icom = $_POST['icom'];
            $store = $_POST['store'];
            if ($barang_id != null) {
                $barang = Barang::model()->findByPk($barang_id);
                $barang_name = $barang->nama_barang;
            }
            $summary = U::report_sales_item_details($barang_id, $from, $to, $store, $tcom, $icom);
            if ($store == "") {
                $store = "All Branch";
            }
            if ($barang_name == "") {
                $barang_name = "All Product";
            }
            $total = array_sum(array_column($summary, 'total'));
            $vatrp = array_sum(array_column($summary, 'vatrp'));
            $bruto = array_sum(array_column($summary, 'bruto'));
            $qty = array_sum(array_column($summary, 'qty'));
            $discrp = array_sum(array_column($summary, 'discrp'));
            $price = array_sum(array_column($summary, 'price'));
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'SalesDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesDetails$from-$to.xls");
                echo $this->render('SalesItemDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'barang' => $barang_name,
                    'qty' => $qty,
                    'total' => $total,
                    'discrp' => $discrp,
                    'price' => $price,
                    'vatrp' => $vatrp,
                    'pt_negara' => PT_NEGARA,
                    'store' => $store,
                    'bruto' => $bruto
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.xml');
            } else {
                $this->render('SalesItemDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'barang' => $barang_name,
                    'qty' => $qty,
                    'total' => $total,
                    'discrp' => $discrp,
                    'price' => $price,
                    'vatrp' => $vatrp,
                    'pt_negara' => PT_NEGARA,
                    'store' => $store,
                    'bruto' => $bruto
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => sql2date($to, 'dd MMM yyyy'), 'grup' => $grup_name)));
//            $this->TBS->MergeBlock('sales', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesDetails$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionSalesnReturDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $grup_id = $_POST['grup_id'];
            $summary = U::report_sales_n_return_details($grup_id, $from, $to, $store);
            $total = array_sum(array_column($summary, 'total'));
            $bruto = array_sum(array_column($summary, 'bruto'));
            $vatrp = array_sum(array_column($summary, 'vatrp'));
            $qty = array_sum(array_column($summary, 'qty'));
            $discrp = array_sum(array_column($summary, 'discrp'));
            $price = array_sum(array_column($summary, 'price'));
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            if ($grup_name == "") {
                $grup_name = "All Group";
            }
            if ($store == "") {
                $store = "All Branch";
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesNReturnSalesDetails$from-$to.xls");
                echo $this->render('SalesnReturDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'grup' => $grup_name,
                        'qty' => $qty,
                        'total' => $total,
                        'pt_negara' => PT_NEGARA,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $this->render('SalesnReturDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'grup' => $grup_name,
                        'qty' => $qty,
                        'total' => $total,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'pt_negara' => PT_NEGARA,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ));
//                $this->renderJsonArr($summary);
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.htm');
            }
        }
    }
    public function actionReturSalesDetails()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $grup_id = $_POST['grup_id'];
            $grup_name = '';
            $store = $_POST['store'];
            if ($grup_id != null) {
                $grup = Grup::model()->findByPk($grup_id);
                $grup_name = $grup->nama_grup;
            }
            $summary = U::report_retur_sales_details($grup_name, $from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            if ($grup_name == "") {
                $grup_name = "All Group";
            }
            $total = array_sum(array_column($summary, 'total'));
            $bruto = array_sum(array_column($summary, 'bruto'));
            $vatrp = array_sum(array_column($summary, 'vatrp'));
            $qty = array_sum(array_column($summary, 'qty'));
            $discrp = array_sum(array_column($summary, 'discrp'));
            $price = array_sum(array_column($summary, 'price'));
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'ReturSalesDetails',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=ReturSalesDetails$from-$to.xls");
                echo $this->render('ReturSalesDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'total' => $total,
                    'discrp' => $discrp,
                    'pt_negara' => PT_NEGARA,
                    'price' => $price,
                    'vatrp' => $vatrp,
                    'store' => $store,
                    'bruto' => $bruto
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.xml');
            } else {
                $this->render('ReturSalesDetails', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'grup' => $grup_name,
                    'qty' => $qty,
                    'total' => $total,
                    'discrp' => $discrp,
                    'pt_negara' => PT_NEGARA,
                    'price' => $price,
                    'vatrp' => $vatrp,
                    'store' => $store,
                    'bruto' => $bruto
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales_details.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => sql2date($to, 'dd MMM yyyy'), 'grup' => $grup_name)));
//            $this->TBS->MergeBlock('sales', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesDetails$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
    public function actionLaha()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $user = Users::model()->findByPk(Yii::app()->getUser()->id);
            $tgl = $_POST['tglfrom'];
            $branch = $_POST['store'];
            $cash = U::report_cash_laha($tgl);
            $sum_ulpt = U::report_ulpt_laha($tgl);
            $sum_rpj = U::report_rpj_laha($tgl);
            $ulpt = U::report_ulpt_details_laha($tgl);
            $rpj = U::report_rpj_details_laha($tgl);
            $modal = U::report_modal($tgl);
            $rounding = U::report_sales_laha_rounding_total($tgl);
            $vat_sales = U::report_vat_sales($tgl);
            $vat_retursales = U::report_vat_retursales($tgl);
            $disc_sales = U::report_discount_sales($tgl);
            $disc_retursales = U::report_discount_returnsales($tgl);
            $noncash = U::report_noncash_laha($tgl);
            $noncashdetail = U::report_noncash_detail_laha($tgl);
            $noncashsales = U::report_noncash_sales($tgl);
//            $noncash_details = U::report_noncash_details_laha($tgl);
//            $noncash_details = U::report_noncash_detail_laha($tgl);
            $sales_laha = U::report_sales_laha($tgl);
            $retursales_laha = U::report_retursales_laha($tgl);
            $cashin = U::report_casin($tgl, $branch);
            $cashout = U::report_casout($tgl, 0, $branch);
            $cashoutother = U::report_casout($tgl, 1, $branch);
            $cash_before = U::get_balance_before_for_bank_account($tgl, Bank::get_bank_cash_id($branch));
            $sum_sales_laha = array_sum(array_column($sales_laha, 'total'));
            $sum_cashin = array_sum(array_column($cashin, 'total'));
            $sum_retursales = array_sum(array_column($retursales_laha, 'total'));
            $sum_cashout = array_sum(array_column($cashout, 'total'));
            $sum_cashoutother = array_sum(array_column($cashoutother, 'total'));
            $sumall = $sum_sales_laha + $sum_cashin - $sum_retursales - $sum_cashout -
                $sum_cashoutother - $disc_sales + $disc_retursales + $vat_sales + $rounding - $vat_retursales;
            $noncashsales = ($sum_sales_laha + $rounding + $vat_sales - $disc_sales) + $sum_cashin;
            $total_cashout = $sum_cashout + $sum_cashoutother;
//            $sum_noncash_details = array_sum(array_column($noncash_details, 'total'));
            $sum_noncash_details = array_sum(array_column($noncashdetail, 'amount'));
            $cash_transfer = U::report_laha_bank_transfer($tgl, $branch);
            $cash_transfer_setor = U::report_laha_bank_transfer_setor($tgl, $branch);
//            $pnl = $cash_before + $noncashsales - $sum_noncash_details - $total_cashout + $sum_ulpt - $sum_rpj;
            $pnl = $cash_before + ($sum_sales_laha + $vat_sales - $disc_sales + $rounding) +
                $sum_cashin - $sum_noncash_details - $total_cashout + $sum_ulpt - $sum_rpj;
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'laha.xlsx');
//            if ($this->format == 'excel') {
//
//            } else {
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'laha.htm');
//            }
            $this->TBS->MergeBlock('header', array(
                array(
                    'logo' => $this->logo,
                    'branch' => $branch,
                    'pt_negara' => PT_NEGARA,
                    'day' => date('l', strtotime($tgl)),
                    'date' => sql2date($tgl),
                    'daily' => $sumall,
                    'sum_sales' => $sum_sales_laha,
                    'cashdrawer' => $cash_before,
                    'modal' => $modal,
                    'cashdrawerafter' => $pnl - $cash_transfer,
                    'sales_vat_disc' => $sum_sales_laha + $vat_sales - $disc_sales + $rounding,
                    'noncashsales' => $noncashsales,
                    'total_cashout' => $total_cashout,
                    'pnl' => $pnl,
                    'sum_retursales' => $sum_retursales,
                    'sum_cashin' => $sum_cashin,
                    'sum_cashout' => $sum_cashout,
                    'sum_ulpt' => $sum_ulpt,
                    'sum_rpj' => $sum_rpj,
                    'sum_cashoutother' => $sum_cashoutother,
                    'cashnoncash' => $cash + $noncash,
                    'noncashdetails' => $sum_noncash_details,
                    'cashtransfer' => $cash_transfer,
                    'cashtransferbank' => $cash_transfer_setor,
                    'kacab' => $user->name,
                )
            ));
            $this->TBS->MergeBlock('ulpt', $ulpt);
            $this->TBS->MergeBlock('rpj', $rpj);
            $this->TBS->MergeBlock('sales', $sales_laha);
            $this->TBS->MergeBlock('retursales', $retursales_laha);
            $this->TBS->MergeBlock('cashin', $cashin);
            $this->TBS->MergeBlock('cashout', $cashout);
            $this->TBS->MergeBlock('cashoutother', $cashoutother);
            $this->TBS->MergeBlock('money', array(array('cash' => $cash, 'noncash' => $noncash)));
            $this->TBS->MergeBlock('noncash', $noncashdetail);
            $this->TBS->MergeBlock('d', array(array('sales' => $disc_sales, 'retursales' => $disc_retursales, 'rounding' => $rounding)));
            $this->TBS->MergeBlock('v', array(array('sales' => $vat_sales, 'retursales' => $vat_retursales)));
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Laha$branch$tgl.xlsx");
        }
    }
    public function actionGeneralLedger()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $coa = $_POST['account_code'];
            $store = $_POST['store'];
            $chart_account = ChartMaster::model()->findByPk($coa);
            $result = U::get_general_ledger($coa, $from, $to, $store);
            $begin = U::get_gl_before($coa, $from, $store);
            $begin_arr = array(
                'tgl' => '',
                'memo_' => 'INITIAL BALANCE',
                'Debit' => $begin > 0 ? $begin : '',
                'Credit' => $begin < 0 ? -$begin : '',
                'Balance' => $begin,
                'amount' => $begin
            );
            $gl[] = $begin_arr;
            foreach ($result as $newrow) {
                $move = $newrow['amount'];
                $newrow['Balance'] = $newrow['amount'] + $begin;
                $begin = $newrow['Balance'];
                $gl[] = $newrow;
            }
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($gl, array(
                'id' => 'GeneralLedger',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=GeneralLedger $from sd $to.xls");
            }
            $this->render('GeneralLedger', array(
                'dp' => $dataProvider,
                'from' => sql2date($from, 'dd MMM yyyy'),
                'to' => sql2date($to, 'dd MMM yyyy'),
                'account_code' => $chart_account->account_code,
                'account_name' => $chart_account->account_name,
                'store' => $store
            ));
        }
    }
    public function actionKartuPiutang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $customer_id = $_POST['customer_id'];
            $store = $_POST['store'];
            /* @var $supplier Supplier */
            $customer = Customers::model()->findByPk($customer_id);
            $result = $customer->get_pelunasan($from, $to, $store);
            $begin = $customer->total_piutang_before($from, $store);
            $begin_arr = array(
                'tgl' => '',
                'doc_ref' => '',
                'note' => 'INITIAL BALANCE',
                'no_faktur' => '',
                'piutang' => $begin >= 0 ? $begin : 0,
                'payment' => $begin < 0 ? -$begin : 0,
                'saldo' => $begin,
                'total' => $begin
            );
            $kartu_piutang[] = $begin_arr;
            foreach ($result as $newrow) {
                $newrow['saldo'] = $newrow['total'] + $begin;
                $begin = $newrow['saldo'];
                $kartu_piutang[] = $newrow;
            }
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($kartu_piutang, array(
                'id' => 'KartuPiutang',
                'pagination' => false
            ));
            $this->render('KartuPiutang', array(
                'dp' => $dataProvider,
                'from' => sql2date($from, 'dd MMM yyyy'),
                'to' => sql2date($to, 'dd MMM yyyy'),
                'customer' => $supplier->nama_customer,
                'store' => $store
            ));
        }
    }
    public function actionKartuHutang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $supplier_id = $_POST['supplier_id'];
            $store = $_POST['store'];
            /* @var $supplier Supplier */
            $supplier = Supplier::model()->findByPk($supplier_id);
            $result = $supplier->get_pelunasan($from, $to, $store);
            $begin = $supplier->total_hutang_before($from, $store);
            $begin_arr = array(
                'tgl' => '',
                'doc_ref' => '',
                'note' => 'INITIAL BALANCE',
                'no_faktur' => '',
                'hutang' => $begin >= 0 ? $begin : 0,
                'payment' => $begin < 0 ? -$begin : 0,
                'saldo' => $begin,
                'total' => $begin
            );
            $kartu_hutang[] = $begin_arr;
            foreach ($result as $newrow) {
                $newrow['saldo'] = $newrow['total'] + $begin;
                $begin = $newrow['saldo'];
                $kartu_hutang[] = $newrow;
            }
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($kartu_hutang, array(
                'id' => 'KartuHutang',
                'pagination' => false
            ));
            $this->render('KartuHutang', array(
                'dp' => $dataProvider,
                'from' => sql2date($from, 'dd MMM yyyy'),
                'to' => sql2date($to, 'dd MMM yyyy'),
                'supplier' => $supplier->supplier_name,
                'store' => $store
            ));
        }
    }
    public function actionR_ReportHutang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $summary = U::report_R_ReportHutang($from, $to);
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'R_ReportHutang',
                'pagination' => false
            ));
            $this->render('R_ReportHutang', array(
                'from' => sql2date($from, 'dd MMM yyyy'),
                'to' => sql2date($to, 'dd MMM yyyy'),
                'R_ReportHutang' => $dataProvider
            ));
        }
    }
    public function actionR_ReportPiutang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $summary = U::report_R_ReportPiutang($from, $to);
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'R_ReportPiutang',
                'pagination' => false
            ));
            $this->render('R_ReportPiutang', array(
                'from' => sql2date($from, 'dd MMM yyyy'),
                'to' => sql2date($to, 'dd MMM yyyy'),
                'R_ReportPiutang' => $dataProvider
            ));
        }
    }
    public function actionBukuPiutang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $row = U::piutang_due();
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'R_piutang_due.xml');
            $this->TBS->MergeBlock('header', array(
                array(
                    'title_pt' => app()->params['report_title_pt'],
                    'tgl_now' => get_date_today('dd MMM yyyy')
                )
            ));
            $this->TBS->MergeBlock('0', $row);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "piutang_due.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionBukuHutang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $row = U::hutang_due();
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'hutang_due.xml');
            $this->TBS->MergeBlock('header', array(
                array(
                    'title_pt' => app()->params['report_title_pt'],
                    'tgl_now' => get_date_today('dd MMM yyyy')
                )
            ));
            $this->TBS->MergeBlock('0', $row);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "hutang_due.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionGeneralJournal()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $result = U::get_general_journal($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($result, array(
                'id' => 'GeneralJournal',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=GeneralJournal$from-$to.xls");
            }
            $this->render('GeneralJournal', array(
                'dp' => $dataProvider,
                'from' => sql2date($from, 'dd MMM yyyy'),
                'to' => sql2date($to, 'dd MMM yyyy'),
                'store' => $store
            ));
        }
    }
    public function actionPayments()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $bank_id = $_POST['bank_id'];
            $summary = U::get_report_payment($from, $to, $bank_id, $store);
            $amount = array_sum(array_column($summary, 'amount'));
            $kembali = array_sum(array_column($summary, 'kembali'));
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($store == "") {
                $store = "All Branch";
            }
            $bank = Bank::model()->findByPk($bank_id);
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesNReturnSalesDetails$from-$to.xls");
                echo $this->render('Payment',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'amount' => $amount,
                        'kembali' => $kembali,
                        'store' => $store,
                        'pt_negara' => PT_NEGARA,
                        'bank' => $bank
                    ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $this->render('Payment',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'amount' => $amount,
                        'kembali' => $kembali,
                        'store' => $store,
                        'pt_negara' => PT_NEGARA,
                        'bank' => $bank
                    ));
//                $this->renderJsonArr($summary);
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.htm');
            }
        }
    }
    public function actionDaftarHargaJual()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $store = $_POST['store'];
            $summary = U::get_daftar_harga_jual($store);
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'SalesPrice.xml');
            $this->TBS->MergeBlock('brg', $summary);
            $this->TBS->MergeField('header', array('branch' => $store));
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesPrice-$store.xls");
        }
    }
    public function actionDaftarHargaBeli()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $store = $_POST['store'];
            $comm = Yii::app()->db->createCommand("
            SELECT nb.kode_barang,nbl.price,nb.nama_barang
            FROM nscc_beli AS nbl
            INNER JOIN nscc_barang AS nb ON nbl.barang_id = nb.barang_id
            WHERE nbl.store = :store");
            $summary = $comm->queryAll(true, array(':store' => $store));
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'PurchasePrice.xlsx');
            $this->TBS->MergeBlock('brg', $summary);
            $this->TBS->MergeField('header', array('branch' => $store));
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "PurchasePrice-$store.xlsx");
        }
    }
    public function actionRekeningKoran()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $bank_id = $_POST['bank_id'];
            $summary = U::get_bank_trans($from, $to, $bank_id);
            $bank = Bank::model()->findByPk($bank_id);
            $begin = U::get_balance_before_for_bank_account($from, $bank_id);
            $begin_arr = array(
                'tgl' => '',
                'ref' => 'INITIAL BALANCE',
                'Debit' => $begin > 0 ? $begin : '',
                'Credit' => $begin < 0 ? -$begin : '',
                'Balance' => $begin,
                'amount' => $begin,
                'memo_' => '',
                'comment_' => ''
            );
            $gl[] = $begin_arr;
            foreach ($summary as $newrow) {
                $move = $newrow['amount'];
                $newrow['Balance'] = $newrow['amount'] + $begin;
                $begin = $newrow['Balance'];
                $gl[] = $newrow;
            }
            $dataProvider = new CArrayDataProvider($gl, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekeningKoran$from-$to.xls");
                echo $this->render('RekeningKoran',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'bank' => $bank
                    ), true);
            } else {
                $this->render('RekeningKoran',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'bank' => $bank
                    ));
//                $this->renderJsonArr($summary);
            }
        }
    }
    public function actionRekeningKas()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $bank_id = $_POST['bank_id'];
            $summary = U::get_bank_trans_kas($from, $to, $bank_id);
            $bank = Bank::model()->findByPk($bank_id);
            $begin = U::get_balance_before_for_bank_account($from, $bank_id);
            $begin_arr = array(
                'tgl' => '',
                'ref' => 'INITIAL BALANCE',
                'Debit' => $begin > 0 ? $begin : '',
                'Credit' => $begin < 0 ? -$begin : '',
                'Balance' => $begin,
                'amount' => $begin,
                'memo_' => '',
                'comment_' => ''
            );
            $gl[] = $begin_arr;
            foreach ($summary as $newrow) {
                $move = $newrow['amount'];
                $newrow['Balance'] = $newrow['amount'] + $begin;
                $begin = $newrow['Balance'];
                $gl[] = $newrow;
            }
            $dataProvider = new CArrayDataProvider($gl, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekeningKoranKas$from-$to.xls");
                echo $this->render('RekeningKoranKas',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'bank' => $bank
                    ), true);
            } else {
                $this->render('RekeningKoranKas',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'bank' => $bank
                    ));
//                $this->renderJsonArr($summary);
            }
        }
    }    
    public function actionCustAtt()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $where = "";
            $param = array(':from' => $_POST['from'], ':to' => $_POST['to']);
            if ($_POST['store'] != null) {
                $where = "AND ns.store = :store";
                $param[':store'] = $_POST['store'];
            }
            $comm = app()->db->createCommand("
            SELECT DATE_FORMAT(ns.tgl,'%d %b %y') tgl,
            COUNT(DISTINCT(ns.customer_id)) visits
            FROM nscc_salestrans AS ns
            WHERE ns.tgl >= :from AND ns.tgl <= :to
            $where
            GROUP BY ns.tgl");
            $this->renderJsonArr($comm->queryAll(true, $param));
        }
    }




    public function actionCustomerSummaryDetails()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $customer = $_POST['customer'];
            if ($customer != "") {
                $cust = Customers::model()->findByAttributes(array('no_customer' => $customer));
                $customer_id = $cust->customer_id;
            } else $customer_id = "";
            $summary = U::report_customer_summary_details($from, $to, $customer_id);
            $total = array_sum(array_column($summary, 'total'));
            $bruto = array_sum(array_column($summary, 'bruto'));
            $vatrp = array_sum(array_column($summary, 'vatrp'));
            $qty = array_sum(array_column($summary, 'qty'));
            $discrp = array_sum(array_column($summary, 'discrp'));
            $price = array_sum(array_column($summary, 'price'));
            if ($customer == "") {
                $customer = "All Customers";
            } else $customer = $cust->no_customer . " - " . $cust->nama_customer;
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=CustomerSummaryDetails$from-$to.xls");
                echo $this->render('CustomerSummaryDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'qty' => $qty,
                        'total' => $total,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'pt_negara' => PT_NEGARA,
                        'price' => $price,
                        'bruto' => $bruto,
                        'customer' => $customer
                    ), true);
            } else {
                $this->render('CustomerSummaryDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'qty' => $qty,
                        'total' => $total,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'pt_negara' => PT_NEGARA,
                        'price' => $price,
                        'bruto' => $bruto,
                        'customer' => $customer
                    ));
            }
        }
    }
    public function actionFeeCard()
    {
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_fee_card($from, $to, $store);
            //$query = U::report_fee_card_total($from, $to, $store);
            //$total = $query->total;
            $amount = array_sum(array_column($summary, 'amount'));
            $fee = array_sum(array_column($summary, 'fee'));
            $piutang = array_sum(array_column($summary, 'piutang'));
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=FeeCard $from to $to.xls");
                echo $this->render('FeeCard',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'amount' => $amount,
                        'fee' => $fee,
                        'piutang' => $piutang,
                        'store' => $store
                    ), true);
            } else {
                $this->render('FeeCard',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'amount' => $amount,
                        'fee' => $fee,
                        'piutang' => $piutang,
                        'store' => $store
                    ));
            }
        }
    }
    public function actionInfoEfektivitas()
    {
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_InfoEfektivitas($from, $to, $store);
            //$query = U::report_fee_card_total($from, $to, $store);
            //$total = $query->total;
//            $amount = array_sum(array_column($summary, 'amount'));
//            $fee = array_sum(array_column($summary, 'fee'));
//            $piutang = array_sum(array_column($summary, 'piutang'));
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InfoEfektivitas $from to $to.xls");
                echo $this->render('InfoEfektivitas',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'store' => $store
                    ), true);
            } else {
                $this->render('InfoEfektivitas',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'store' => $store
                    ));
            }
        }
    }
    public function actionTerimaBarang()
    {
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_Transfer_Barang($from, $to, $store);
            //$query = U::report_fee_card_total($from, $to, $store);
            //$total = $query->total;
//            $amount = array_sum(array_column($summary, 'amount'));
//            $fee = array_sum(array_column($summary, 'fee'));
//            $piutang = array_sum(array_column($summary, 'piutang'));
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=InfoEfektivitas $from to $to.xls");
                echo $this->render('TerimaBarang',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'store' => $store
                    ), true);
            } else {
                $this->render('TerimaBarang',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'store' => $store
                    ));
            }
        }
    }
    public function actionEfektivitas()
    {
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_efektivitas($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'efektivitas.xml');
            $this->TBS->MergeBlock('r', $summary);
            $this->TBS->MergeField('header', array('store' => $store, 'periode' => sql2date($from, 'dd MMMM yyyy') .
                ' s.d ' . sql2date($to, 'dd MMMM yyyy')));
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Efektivitas-$store.xls");
        }
    }
    public function actionPasienBaruRiilFaktur()
    {
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_pasien_baru_riil_faktur($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR
                . 'pasienbaruriilfaktur.xml');
            $this->TBS->MergeBlock('r', $summary);
            $this->TBS->MergeField('header', array('store' => $store, 'periode' => sql2date($from, 'dd MMMM yyyy') .
                ' s.d ' . sql2date($to, 'dd MMMM yyyy')));
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "PasienBaruRiilFaktur-$store.xls");
        }
    }
    public function actionStockInTransit()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $tgl = sql2date($_POST['tgl'], 'dd MMM yyyy');
            $data = Barang::queryFilter(StockMoves::queryStockInTransit())->addOrder('d.tdate')->queryAll();
            $dataProvider = new CArrayDataProvider($data, array(
                'id' => 'StockInTransit',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=StockInTransit-$tgl.xls");
                echo $this->render('StockInTransit', array(
                    'dp' => $dataProvider,
                    'tgl' => $tgl
                ), true);
            } else {
                $this->render('StockInTransit', array(
                    'dp' => $dataProvider,
                    'tgl' => $tgl
                ));
            }
        }
    }
    public function actionPrintPurchaseRequisition()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $pr = $this->loadModel($_POST['pr_id'], 'Pr');
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'pr.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $prd = $pr->report();
            $items = array();
            $count_items = count($prd);
            for ($i = 0; $i < 17; $i++) {
                $items[] = array(
                    'nomor' => $i < $count_items ? ($i + 1) : '',
                    'deskripsi' => $i < $count_items ? $prd[$i]['deskripsi'] : '',
                    'qty' => $i < $count_items ? $prd[$i]['qty'] : '',
                    'unit' => $i < $count_items ? $prd[$i]['sat'] : ''
                );
            }
            $header_data = array(
                'no_pr' => $pr->doc_ref,
                'tgl' => date_format(date_create($pr->tgl), 'd F Y'),
                'tgl_kirim' => date_format(date_create($pr->tgl_kirim), 'd F Y'),
                'divisi' => $pr->divisi,
                'acc_charge' => $pr->acc_charge,
                'note' => $pr->note
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('item', $items);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "PurchaseRequisition $pr->doc_ref.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionPrintPurchaseOrder()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            $excel = $objReader->load(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'po.xls');
            $po = Po::model()->find('po_id = :po_id', array(':po_id' => $_POST['po_id']));
            $sjd = Po::get_po_details_to_print($_POST['po_id']);
            $excel->setActiveSheetIndex(0)
                ->setCellValue('J6', "'" . $po->doc_ref)
                ->setCellValue('J7', sql2date($po->tgl, 'dd MMMM yyyy'))
                ->setCellValue('J8', "'" . $po->doc_ref_pr)
                ->setCellValue('J9', "'" . $po->divisi)
                ->setCellValue('D11', "'" . $po->supp_nama)
                ->setCellValue('D12', "'" . $po->supp_company)
                ->setCellValue('D13', "'" . $po->supp_address)
                ->setCellValue('D15', "'" . $po->supp_city)
                ->setCellValue('D16', "'" . $po->supp_country)
                ->setCellValue('D17', "'" . $po->supp_phone)
                ->setCellValue('D18', "'" . $po->supp_fax)
                ->setCellValue('D19', "'" . $po->supp_email)
                ->setCellValue('M11', "'" . $po->nama)
                ->setCellValue('M12', "'" . $po->ship_to_company)
                ->setCellValue('M13', "'" . $po->ship_to_address)
                ->setCellValue('M15', "'" . $po->ship_to_city)
                ->setCellValue('M16', "'" . $po->ship_to_country)
                ->setCellValue('M17', "'" . $po->ship_to_phone)
                ->setCellValue('M18', "'" . $po->ship_to_fax)
                ->setCellValue('D22', "'" . $po->nama_rek)
                ->setCellValue('D23', "'" . $po->no_rek)
                ->setCellValue('D24', "'" . $po->bank_rek)
                ->setCellValue('G22', sql2date($po->tgl_delivery))
                ->setCellValue('J22', $po->termofpayment . " days")
                ->setCellValue('F40', $po->note)
                ->setCellValue('N39', $po->total_disc_rp)
                ->setCellValue('N40', $po->total_dpp)
                ->setCellValue('N41', $po->tax_rp)
                ->setCellValue('N42', $po->total_pph_rp)
                ->setCellValue('N43', $po->total)
                ->setCellValue('A40', $po->acc_charge)
                ->setCellValue('A49', "( " . SysPrefs::get_val('po_prepared_by') . " )")
                ->setCellValue('A50', SysPrefs::get_val('po_prepared_by_position'))
                ->setCellValue('M49', "( " . SysPrefs::get_val('po_approved_by') . " )")
                ->setCellValue('M50', SysPrefs::get_val('po_approved_by_position'));
            $rowID = 27;
            $default_row = 11;
            $count_row = count($sjd);
            $sisa = $count_row - $default_row;
            if ($sisa > 0) {
                $excel->getActiveSheet()->insertNewRowBefore($rowID + 1, $sisa);
                for ($i = $rowID + 1; $i < $rowID + 1 + $sisa; $i++) {
                    $excel->getActiveSheet()->mergeCells("B" . $i . ":E" . $i);
                    $excel->getActiveSheet()->mergeCells("H" . $i . ":I" . $i);
                    $excel->getActiveSheet()->mergeCells("L" . $i . ":M" . $i);
                }
            }
            $num = 0;
            foreach ($sjd as $arr) {
                $excel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $rowID, ++$num)
                    ->setCellValue('B' . $rowID, $arr['nama_barang'])
                    ->setCellValue('F' . $rowID, $arr['charge'])
                    ->setCellValue('G' . $rowID, $arr['qty'])
                    ->setCellValue('H' . $rowID, $arr['sat'])
                    ->setCellValue('J' . $rowID, $arr['price'])
                    ->setCellValue('L' . $rowID, $arr['disc'])
                    ->setCellValue('N' . $rowID, $arr['total']);
                $rowID++;
            }
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="PO(' . $po->doc_ref . ').xls"');
            header('Cache-Control: max-age=1');
            $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel5');
            $objWriter->save('php://output');
            Yii::app()->end();
        }
    }
    public function actionPrintSupplierInvoice()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $ti = TransferItem::model()->find('transfer_item_id = :transfer_item_id', array(':transfer_item_id' => $_POST['transfer_item_id']));
            $tid = TransferItemDetails::get_details_to_print($_POST['transfer_item_id']);
            $sup = Supplier::model()->find('supplier_id = :supplier_id', array(':supplier_id' => $ti->supplier_id));
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'invoice.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $header_data = array(
                'title' => $ti->type_ == 1 ? 'Return' : 'Invoice',
                'doc_ref' => $ti->doc_ref,
                'tgl' => date_format(date_create($ti->tgl), 'd F Y'),
                'tgl_jatuh_tempo' => date_format(date_create($ti->tgl_jatuh_tempo), 'd F Y'),
                'supplier_name' => $sup->supplier_name,
                'doc_ref_other' => $ti->doc_ref_other,
                'no_faktur_pajak' => $ti->no_fakturpajak,
                'note' => $ti->note,
                'sub_total' => $ti->sub_total,
                'total_disc' => $ti->total_disc_rp,
                'total_dpp' => $ti->total_dpp,
                'total_vat' => $ti->tax_rp,
                'total_pph' => $ti->total_pph_rp,
                'grand_total' => $ti->total
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('item', $tid);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Invoice $ti->doc_ref_other.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionPrintRekapPurchaseOrder()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $supplier_id = $_POST['supplier_id'];
            $sup = Supplier::model()->find('supplier_id = :supplier_id', array(':supplier_id' => $supplier_id));
            $store = Yii::app()->db->createCommand("
                SELECT DISTINCT po.store
                FROM nscc_po po
                WHERE po.tgl >= :from AND po.tgl <= :to AND po.supplier_id = :supplier_id AND po.type_ = 1
                ORDER BY po.store
            ")->queryAll(true, array(':from' => $from, ':to' => $to, ':supplier_id' => $supplier_id));
            $barang = Yii::app()->db->createCommand("
                SELECT b.barang_id, b.kode_barang, b.nama_barang, b.sat
                FROM nscc_barang b
                WHERE b.barang_id IN (
                        SELECT
                            pod.barang_id
                        FROM nscc_po_details pod
                            LEFT JOIN nscc_po po ON pod.po_id = po.po_id
                        WHERE
                            po.tgl >= :from AND po.tgl <= :to AND po.supplier_id = :supplier_id AND po.type_ = 1
                    )
                ORDER BY b.kode_barang
            ")->queryAll(true, array(':from' => $from, ':to' => $to, ':supplier_id' => $supplier_id));
            $pod = Yii::app()->db->createCommand("
                SELECT pod.barang_id, sum(pod.qty) qty, po.store
                FROM nscc_po_details pod
                    LEFT JOIN nscc_po po ON pod.po_id = po.po_id
                WHERE
                    po.tgl >= :from AND po.tgl <= :to AND po.supplier_id = :supplier_id AND po.type_ = 1
                GROUP BY pod.barang_id, po.store
            ")->queryAll(true, array(':from' => $from, ':to' => $to, ':supplier_id' => $supplier_id));
            $data = [];
            foreach ($barang as $b) {
                $b['total'] = 0;
                foreach ($store as $s) {
                    foreach ($pod as $d) {
                        if ($b['barang_id'] == $d['barang_id'] && $s['store'] == $d['store']) {
                            $b[$s['store']] = $d['qty'];
                            $b['total'] += $d['qty'];
                        }
                    }
                }
                $data[] = $b;
            }
            $dataProvider = new CArrayDataProvider($data, array(
                'id' => 'RekapPurchaseOrder',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapPO$from-$to.xls");
                echo $this->render('RekapPurchaseOrder', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier_name' => $sup->supplier_name,
                    'store' => $store
                ), true);
            } else {
                $this->render('RekapPurchaseOrder', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier_name' => $sup->supplier_name,
                    'store' => $store
                ));
            }
        }
    }
    public function actionRekapPurchaseOrder()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $supplier_id = $_POST['supplier_id'] == '' ? 'All' : $_POST['supplier_id'];
            $barang_id = $_POST['barang_id'] == '' ? 'All' : $_POST['barang_id'];
            $status = $_POST['status'] == '' ? 'All' : $_POST['status'];
            $show_detail = ((isset($_POST['show_detail']) ? TRUE : FALSE) || ($barang_id != 'All'));
            $data = $show_detail ?
                U::report_rekap_PurchaseOrder_details($from, $to, $supplier_id, $barang_id, $status)
                :
                U::report_rekap_PurchaseOrder($from, $to, $supplier_id, $status);
            $dataProvider = new CArrayDataProvider($data, array(
                'id' => 'ReportPurchaseOrder',
                'pagination' => false
            ));
            if ($supplier_id != 'All') {
                $modelSupplier = $this->loadModel($supplier_id, "Supplier");
                $supplier_id = $modelSupplier->supplier_name;
            }
            if ($barang_id != 'All') {
                $modelBarang = $this->loadModel($barang_id, "Barang");
                $barang_id = $modelBarang->kode_barang;
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapPO$from-$to.xls");
                echo $this->render('RekapPO', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_id,
                    'barang' => $barang_id,
                    'status' => $status,
                    'show_detail' => $show_detail
                ), true);
            } else {
                $this->render('RekapPO', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'supplier' => $supplier_id,
                    'barang' => $barang_id,
                    'status' => $status,
                    'show_detail' => $show_detail
                ));
            }
        }
    }
    public function actionPrintReturBarang()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $ti = TerimaBarang::model()->find('terima_barang_id = :terima_barang_id', array(':terima_barang_id' => $_POST['terima_barang_id']));
            $tid = TerimaBarang::get_details_to_print($_POST['terima_barang_id']);
            IF ($ti->po->type_ != -1) {
                return;
            }
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'tandaterima_retur_barang.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $header_data = array(
                'title' => 'TANDA TERIMA',
                'doc_ref' => $ti->doc_ref,
                'tgl' => date_format(date_create($ti->tgl), 'd F Y'),
                'supp_company' => $ti->po->supp_company,
                'supp_address' => $ti->po->supp_address
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('item', $tid);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Retur $ti->doc_ref.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionPrintTandaTerimaDropping()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $ti = Dropping::model()->find('dropping_id = :dropping_id', array(':dropping_id' => $_POST['dropping_id']));
            $tid = DroppingDetails::get_details_to_print($_POST['dropping_id']);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'tandaterima_dropping.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $header_data = array(
                'title' => 'TANDA TERIMA',
                'doc_ref' => $ti->doc_ref,
                'tgl' => date_format(date_create($ti->tgl), 'd F Y'),
                'dari' => $ti->store,
                'kepada' => $ti->store_penerima
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('item', $tid);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "TandaTerima $ti->doc_ref $ti->store-$ti->store_penerima.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
	public function actionPrintBuktiKirim()
	{
		if (Yii::app()->request->isAjaxRequest) {
			return;
		}
		if (isset($_POST) && !empty($_POST)) {
			$ti = TransferBarang::model()->find('doc_ref = :doc_ref', array(':doc_ref' => $_POST['doc_ref']));
			$tid = TransferBarang::get_details_to_print($_POST['doc_ref']);
			$this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'buktikirim.xml');
			$this->TBS->SetOption('noerr', true);
			$header_data = array(
				'title' => 'BUKTI PENGIRIMAN',
				'doc_ref' => $ti->doc_ref,
				'tgl' => date_format(date_create($ti->tgl), 'd F Y'),
				'dari' => $ti->store,
				'note' => $ti->note
			);

			$this->TBS->MergeField('header', $header_data);
			$this->TBS->MergeBlock('item', $tid);
			if ($this->format == 'excel') {
				$this->TBS->Show(TBS_EXCEL_DOWNLOAD, "TandaKirim $ti->doc_ref.xls");
			} else {
				$this->TBS->Show();
			}
		}
	}
	public function actionPrintExcelTransferBarangOut()
	{
		if (Yii::app()->request->isAjaxRequest) {
			return;
		}
		if (isset($_POST) && !empty($_POST)) {
                        $username = Yii::app()->user->name;
			$ti = TransferBarang::model()->find('doc_ref = :doc_ref', array(':doc_ref' => $_POST['doc_ref']));
			$tid = TransferBarang::get_details_to_print($_POST['doc_ref']);
			$this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'print_excel_transfer_barang_out.xml');
			$this->TBS->SetOption('noerr', true);
			$header_data = array(
				'title' => 'BUKTI BARANG KELUAR',
				'doc_ref' => $ti->doc_ref,
				'tgl' => date_format(date_create($ti->tgl), 'd F Y'),
				'dari' => $ti->store,
				'note' => $ti->note,
                                'invoice' => $ti->doc_ref_other,
                                'printed' => $username
                                //doc_ref_other   $username
			);

			$this->TBS->MergeField('header', $header_data);
			$this->TBS->MergeBlock('item', $tid);
			if ($this->format == 'excel') {
				$this->TBS->Show(TBS_EXCEL_DOWNLOAD, "TandaKirim $ti->doc_ref.xls");
			} else {
				$this->TBS->Show();
			}
		}
	}         
    public function actionPrintOrderDropping()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $ti = OrderDropping::model()->find('order_dropping_id = :order_dropping_id', array(':order_dropping_id' => $_POST['order_dropping_id']));
            $tid = OrderDroppingDetails::get_details_to_print($_POST['order_dropping_id']);
            if ($ti->store_pengirim != STOREID || $ti->lunas == PR_DRAFT) {
                return;
            }
            if ($ti->approved == 0) {
                return;
            }
            if ($ti->lunas == PR_OPEN) {
                /*
                 * jika status = OPEN
                 * maka setelah diprint status menjadi PROCESS
                 */
                $ti->lunas = PR_PROCESS;
                if (!$ti->save()) {
                    return CHtml::errorSummary($ti);
                }
            }
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'print_orderdropping.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $header_data = array(
                'title' => 'Order Dropping',
                'doc_ref' => $ti->doc_ref,
                'tgl' => date_format(date_create($ti->tgl), 'd F Y'),
                'pengirim' => $ti->store_pengirim,
                'penerima' => $ti->store
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('item', $tid);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "Order Dropping $ti->doc_ref $ti->store.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }
    public function actionRekapOrderDropping()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $pengirim = empty($_POST['store_pengirim'])?STOREID:$_POST['store_pengirim'];
            $show_ratarata = isset($_POST['show_ratarata']) ? TRUE : FALSE;
            $approved = $_POST['approved'] == "all" ? "" : "AND od.approved = ".$_POST['approved'];
            $queryStore = $_POST['store'] == "" ? "" : "AND od.store = '" . $_POST['store'] . "'";
            $store = Yii::app()->db->createCommand("
                SELECT DISTINCT od.store
                FROM nscc_order_dropping od
                WHERE od.tgl >= :from AND od.tgl <= :to $approved $queryStore AND od.store_pengirim = :pengirim
                ORDER BY od.store
            ")->queryAll(true, array(':from' => $from, ':to' => $to, ':pengirim' => $pengirim));
            $barang = Yii::app()->db->createCommand("
                SELECT b.barang_id, b.kode_barang, b.nama_barang, b.sat
                FROM nscc_barang b
                WHERE b.barang_id IN (
                        SELECT
                            odd.barang_id
                        FROM nscc_order_dropping_details odd
                            LEFT JOIN nscc_order_dropping od ON odd.order_dropping_id = od.order_dropping_id
                        WHERE
                            od.tgl >= :from AND od.tgl <= :to AND odd.visible = 1 AND od.approved = 1 $queryStore AND od.store_pengirim = :pengirim
                    )
                ORDER BY b.kode_barang
            ")->queryAll(true, array(':from' => $from, ':to' => $to, ':pengirim' => $pengirim));
            $ratarata = "";
            if ($show_ratarata) {
                $datetimeFrom = date_create($from);
                $datetimeTo = date_create($to);
                $interval = date_diff($datetimeFrom, $datetimeTo);
                $ratarata = '/' . ($interval->format('%m') + $interval->format('%d') / 30);
            }
            $odd = Yii::app()->db->createCommand("
                SELECT odd.barang_id, sum(ifnull(odd.qty,0))$ratarata qty, od.store
                FROM nscc_order_dropping_details odd
                    LEFT JOIN nscc_order_dropping od ON odd.order_dropping_id = od.order_dropping_id
                WHERE
                    od.tgl >= :from AND od.tgl <= :to AND odd.visible = 1 AND od.approved = 1 $queryStore AND od.store_pengirim = :pengirim
                GROUP BY odd.barang_id, od.store
            ")->queryAll(true, array(':from' => $from, ':to' => $to, ':pengirim' => $pengirim));
            $data = [];
            foreach ($barang as $b) {
                $b['total'] = 0;
                foreach ($store as $s) {
                    foreach ($odd as $d) {
                        if ($b['barang_id'] == $d['barang_id'] && $s['store'] == $d['store']) {
                            $b[$s['store']] = number_format($d['qty'], 2);
                            $b['total'] += $d['qty'];
                        }
                    }
                }
                $data[] = $b;
            }
            $dataProvider = new CArrayDataProvider($data, array(
                'id' => 'RekapOrderDropping',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapOrderDropping$from-$to.xls");
                echo $this->render('RekapOrderDropping', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'branch' => $_POST['store'] == "" ? "All branch" : $_POST['store'],
                    'store' => $store,
                    'ratarata' => $show_ratarata
                ), true);
            } else {
                $this->render('RekapOrderDropping', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'branch' => $_POST['store'] == "" ? "All branch" : $_POST['store'],
                    'store' => $store,
                    'ratarata' => $show_ratarata
                ));
            }
        }
    }
    public function actionRekapDropping()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            //$aaa = $_POST['store'];
            //$bbbb=    $_POST['store_pengirim'];
            $pengirim = empty($_POST['store_pengirim'])?STOREID:$_POST['store_pengirim'];
            $show_ratarata = isset($_POST['show_ratarata']) ? TRUE : FALSE;
            //$queryStore = $_POST['store'] == "" ? "" : "AND od.store = '" . $_POST['store'] . "'";
            $queryStore = $_POST['store'] == "" ? "" : "AND od.store_penerima = '" . $_POST['store'] . "'";
//            $store = Yii::app()->db->createCommand("
//                SELECT DISTINCT od.store
//                FROM nscc_order_dropping od
//                WHERE od.tgl >= :from AND od.tgl <= :to AND od.approved = 1 $queryStore AND od.store_pengirim = :pengirim
//                ORDER BY od.store
//            ")->queryAll(true, array(':from' => $from, ':to' => $to, ':pengirim' => $pengirim));
            
            $store = Yii::app()->db->createCommand("
                SELECT DISTINCT od.store_penerima
                FROM nscc_dropping od
                WHERE od.tgl >= :from AND od.tgl <= :to AND od.approved = 1 $queryStore AND od.store = :pengirim
                ORDER BY od.store_penerima;
            ")->queryAll(true, array(':from' => $from, ':to' => $to, ':pengirim' => $pengirim));
            
//            $barang = Yii::app()->db->createCommand("
//                SELECT b.barang_id, b.kode_barang, b.nama_barang, b.sat
//                FROM nscc_barang b
//                WHERE b.barang_id IN (
//                        SELECT
//                            odd.barang_id
//                        FROM nscc_order_dropping_details odd
//                            LEFT JOIN nscc_order_dropping od ON odd.order_dropping_id = od.order_dropping_id
//                        WHERE
//                            od.tgl >= :from AND od.tgl <= :to AND odd.visible = 1 AND od.approved = 1 $queryStore AND od.store_pengirim = :pengirim
//                    )
//                ORDER BY b.kode_barang
//            ")->queryAll(true, array(':from' => $from, ':to' => $to, ':pengirim' => $pengirim));
            $barang = Yii::app()->db->createCommand("
                   SELECT b.barang_id, b.kode_barang, b.nama_barang, b.sat
                FROM nscc_barang b
                WHERE b.barang_id IN (
                        SELECT
                            odd.barang_id
                        FROM nscc_dropping_details odd
                            LEFT JOIN nscc_dropping od ON odd.dropping_id = od.dropping_id
                        WHERE
                            od.tgl >= :from AND od.tgl <= :to AND odd.visible = 1 $queryStore AND od.approved = 1 AND od.store = :pengirim
                    )
                ORDER BY b.kode_barang;
            ")->queryAll(true, array(':from' => $from, ':to' => $to, ':pengirim' => $pengirim));
            
            $ratarata = "";
            if ($show_ratarata) {
                $datetimeFrom = date_create($from);
                $datetimeTo = date_create($to);
                $interval = date_diff($datetimeFrom, $datetimeTo);
                $ratarata = '/' . ($interval->format('%m') + $interval->format('%d') / 30);
            }
//            $odd = Yii::app()->db->createCommand("
//                SELECT odd.barang_id, sum(ifnull(odd.qty,0))$ratarata qty, od.store
//                FROM nscc_order_dropping_details odd
//                    LEFT JOIN nscc_order_dropping od ON odd.order_dropping_id = od.order_dropping_id
//                WHERE
//                    od.tgl >= :from AND od.tgl <= :to AND odd.visible = 1 AND od.approved = 1 $queryStore AND od.store_pengirim = :pengirim
//                GROUP BY odd.barang_id, od.store
//            ")->queryAll(true, array(':from' => $from, ':to' => $to, ':pengirim' => $pengirim));
            $odd = Yii::app()->db->createCommand("
                SELECT odd.barang_id, sum(ifnull(odd.qty,0))$ratarata qty, od.store_penerima
                FROM nscc_dropping_details odd
                    LEFT JOIN nscc_dropping od ON odd.dropping_id = od.dropping_id
                WHERE
                    od.tgl >= :from AND od.tgl <= :to AND odd.visible = 1 AND od.approved = 1 $queryStore AND od.store = :pengirim
                GROUP BY odd.barang_id, od.store_penerima
            ")->queryAll(true, array(':from' => $from, ':to' => $to, ':pengirim' => $pengirim));            
            $data = [];
            foreach ($barang as $b) {
                $b['total'] = 0;
                foreach ($store as $s) {
                    foreach ($odd as $d) {
                        if ($b['barang_id'] == $d['barang_id'] && $s['store_penerima'] == $d['store_penerima']) {
                            $b[$s['store_penerima']] = number_format($d['qty'], 2);
                            $b['total'] += $d['qty'];
                        }
                    }
                }
                $data[] = $b;
            }
            $dataProvider = new CArrayDataProvider($data, array(
                'id' => 'RekapDropping',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=RekapDropping$from-$to.xls");
                echo $this->render('RekapDropping', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'branch' => $_POST['store'] == "" ? "All branch" : $_POST['store'],
                    'store' => $store,
                    'ratarata' => $show_ratarata
                ), true);
            } else {
                $this->render('RekapDropping', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'branch' => $_POST['store'] == "" ? "All branch" : $_POST['store'],
                    'store' => $store,
                    'ratarata' => $show_ratarata
                ));
            }
        }
    }    
    public function actionReportSupplierInvoice()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $charge = $_POST['charge'];
            $supplier_id = $_POST['supplier_id'];
            $type_ = $_POST['type_'];
            $status = $_POST['status'];

            $show_detail = isset($_POST['show_detail']) ? TRUE : FALSE;
            $summary = U::report_SupplierInvoice($from, $to, $charge, $type_, $status, $supplier_id, $show_detail);
            $total = array_sum(array_column($summary, 'total'));

            $supplier = $supplier_id ?
                Supplier::model()->find('supplier_id = :supplier_id', array(':supplier_id' => $supplier_id))->supplier_name
                : 'All Supplier';
            $template = $show_detail ? 'SupplierInvoiceDetails' : 'SupplierInvoiceSummary';

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'SupplierInvoice',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SupplierInvoice$from-$to.xls");
                echo $this->render($template, array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'total' => $total,
                    'supplier' => $supplier
                ), true);
            } else {
                $this->render($template, array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'total' => $total,
                    'supplier' => $supplier
                ));
            }
        }
    }
    public function actionBonus()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $show_detail = isset($_POST['show_detail']) ? TRUE : FALSE;
            $summary = $show_detail ?
                U::report_bonus_details($from, $to, $store) :
                U::report_bonus_summary($from, $to, $store);
            $total_tip = array_sum(array_column($summary, 'amount_bonus'));
            $template = $show_detail ? 'BonusDetails' : 'BonusSummary';
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'Bonus',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Bonus$from-$to.xls");
                echo $this->render($template, array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'total_bonus' => $total_tip,
                    'store' => $store
                ), true);
            } else {
                $this->render($template, array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'total_bonus' => $total_tip,
                    'store' => $store
                ));
            }
        }
    }
	public function actionBonusDokter()
	{
		if (Yii::app()->request->isAjaxRequest) {
			return;
		}
		if (isset($_POST) && !empty($_POST))
		{
			$user = Users::model()->findByPk(Yii::app()->getUser()->id);
			$empl_id = UserEmployee::model()->findByAttributes(array(
				'id'=>$user->id
			));
			$nama = Employee::model()->findByPk($empl_id->employee_id);
			$from = $_POST['tglfrom'];
			$to = $_POST['tglto'];
			$store = $_POST['store'];
			$summary = U::report_bonus_summary_dokter($from, $to, $store,$empl_id->employee_id);
			$kmt = U::report_bonus_kmt_summary_dokter($from, $to, $store,$empl_id->employee_id);
			if ($store == "") {
				$store = "All Branch";
			}
			if ($this->format == 'excel') {
				header('Content-type: application/vnd.xls');
				header("Content-Disposition: attachment; filename=History_transaksi$from-$to.xls");
				echo $this->render('R_Bonus', array(
					'detail' => $summary,
					'detail_kmt' => $kmt,
					'nama' => $nama->nama_employee,
					'start' => sql2date($from, 'dd MMM yyyy'),
					'to' => sql2date($to, 'dd MMM yyyy'),
					'store' => $store
				), true);
			} else {
				$this->render('R_Bonus', array(
					'detail' => $summary,
					'detail_kmt' => $kmt,
					'nama' => $nama->nama_employee,
					'start' => sql2date($from, 'dd MMM yyyy'),
					'to' => sql2date($to, 'dd MMM yyyy'),
					'store' => $store
				));
			}
		}
	}
    public function actionProduksi()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $show_detail = isset($_POST['show_detail']) ? TRUE : FALSE;
            $show_retur = isset($_POST['show_retur']) ? TRUE : FALSE;
            $show_retur_only = isset($_POST['show_retur_only']) ? TRUE : FALSE;
            $show_total = isset($_POST['show_total']) ? TRUE : FALSE;
            if ($show_total){
                $summary = U::report_produksi_total($from, $to);
                $template = 'ProduksiTotal';
            } else {
                $summary = $show_detail ?
                U::report_produksi_details($from, $to, $store, $show_retur, $show_retur_only) :
                U::report_produksi_summary($from, $to, $store, $show_retur, $show_retur_only);
                $template = $show_detail ? 'ProduksiDetails' : 'ProduksiSummary';
            }            
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'Produksi',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Produksi$from-$to.xls");
                echo $this->render($template, array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
//                    'total_bonus' => $total_tip,
                    'store' => $store
                ), true);
            } else {
                $this->render($template, array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
//                    'total_bonus' => $total_tip,
                    'store' => $store
                ));
            }
        }
    }
    public function actionCashFlow()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $month = $_POST['month'];
            $year = $_POST['year'];
            $d = new DateTime("$year-$month-01");
            $from = $d->format('Y-m-d');
            $to = $d->format('Y-m-t');
            $store = $_POST['store'];
            $kasBank = U::posisi_saldo_before($from, $store);
            $terima = U::arus_kas_masuk_keluar($from, $to, -1, $store);
            $keluar = U::arus_kas_masuk_keluar($from, $to, 1, $store);
            $total_kasBank = array_sum(array_column($kasBank, 'amount'));
            $total_terima = array_sum(array_column($terima, 'amount'));
            $total_keluar = array_sum(array_column($keluar, 'amount'));
            $selish = $total_terima - $total_keluar;
            $saldoakhir = $total_kasBank + $selish;
            if ($store == "") {
                $store = "All Branch";
            }
            $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'cf.xml');
            $dateObj = DateTime::createFromFormat('!m', $month);
            $monthName = $dateObj->format('F');
            $this->TBS->MergeBlock('b', $kasBank);
            $this->TBS->MergeBlock('t', $terima);
            $this->TBS->MergeBlock('k', $keluar);
            $this->TBS->MergeField('h', [
                'kas' => $total_kasBank,
                'terima' => $total_terima,
                'keluar' => $total_keluar,
                'selisih' => $selish,
                'saldoakhir' => $saldoakhir,
                'month' => $monthName,
                'year' => $year
            ]);
            $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "CashFlow_$month-$year-$store.xls");
        }
    }
    public function actionOmsetKasir()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $kasir = $_POST['cashier'];
            $summary = U::report_omset_kasir($kasir, $from, $to, $store);
            $total = array_sum(array_column($summary, 'total'));
            $bruto = array_sum(array_column($summary, 'bruto'));
            $vatrp = array_sum(array_column($summary, 'vatrp'));
            $qty = array_sum(array_column($summary, 'qty'));
            $discrp = array_sum(array_column($summary, 'discrp'));
            $price = array_sum(array_column($summary, 'price'));
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($kasir != NULL) {
                $kasir_name = $_POST['cashier'];
            } else {
                $kasir_name = "All Cashier";
            }
            if ($store == "") {
                $store = "All Branch";
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=OmsetKasir$from-$to.xls");
                echo $this->render('OmsetKasir',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'kasir' => $kasir_name,
                        'qty' => $qty,
                        'total' => $total,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $this->render('OmsetKasir',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'kasir' => $kasir_name,
                        'qty' => $qty,
                        'total' => $total,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ));
//                $this->renderJsonArr($summary);
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.htm');
            }
        }
    }
    public function actionOmsetGroup()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_omset_group($from, $to, $store);
            $total = array_sum(array_column($summary, 'total'));
            $bruto = array_sum(array_column($summary, 'bruto'));
            $vatrp = array_sum(array_column($summary, 'vatrp'));
            $qty = array_sum(array_column($summary, 'qty'));
            $discrp = array_sum(array_column($summary, 'discrp'));
            $discrp1 = array_sum(array_column($summary, 'discrp1'));
            $total_pot = array_sum(array_column($summary, 'total_pot'));
            $price = array_sum(array_column($summary, 'price'));
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($store == "") {
                $store = "All Branch";
            }
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Omset$from-$to.xls");
                echo $this->render('OmsetGroup',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'qty' => $qty,
                        'total' => $total,
                        'discrp' => $discrp,
                        'discrp1' => $discrp1,
                        'total_pot' => $total_pot,
                        'vatrp' => $vatrp,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ), true);
            } else {
                $this->render('OmsetGroup',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'qty' => $qty,
                        'total' => $total,
                        'discrp' => $discrp,
                        'discrp1' => $discrp1,
                        'total_pot' => $total_pot,
                        'vatrp' => $vatrp,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ));
            }
        }
    }

    public function actionSalesSummaryReceiptDetailsCus()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $summary = U::report_sales_summary_receipt_details($from, $to, $store);
            $total = array_sum(array_column($summary, 'total'));
            $bruto = array_sum(array_column($summary, 'bruto'));
            $vatrp = array_sum(array_column($summary, 'vatrp'));
            $qty = array_sum(array_column($summary, 'qty'));
            $discrp = array_sum(array_column($summary, 'discrp'));
            $price = array_sum(array_column($summary, 'price'));
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesSummaryReceiptDetails$from-$to.xls");
                echo $this->render('SalesSummaryReceiptDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'qty' => $qty,
                        'total' => $total,
                        'pt_negara' => PT_NEGARA,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $this->render('SalesSummaryReceiptDetails',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'qty' => $qty,
                        'pt_negara' => PT_NEGARA,
                        'total' => $total,
                        'discrp' => $discrp,
                        'vatrp' => $vatrp,
                        'price' => $price,
                        'bruto' => $bruto,
                        'store' => $store
                    ));
//                $this->renderJsonArr($summary);
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from,)));
//            $this->TBS->MergeBlock('sales', $summary);
//            $this->TBS->MergeBlock('s', array(array('total' => $total)));
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummaryReceiptDetails$from.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }

        public function actionEfektivitasDokter() {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $dokterId = $_POST['dokter_id'];
            $summary = U::report_sales_summary_receipt_doctor($from, $to, $store,$dokterId);
            $query = U::report_sales_summary_receipt_amount_total_doctor($from, $to, $store,$dokterId);
            $total = $query->total;
            //$total = array_sum(array_column($summary, 'total'));
            $amount = array_sum(array_column($summary, 'amount'));
            $kembali = array_sum(array_column($summary, 'kembali'));
            if ($store == "") {
                $store = "All Branch";
            }
            if ($dokterId != null) {
                $dokter = Dokter::model()->findByPk($dokterId);
                $dokter_name = $dokter->nama_dokter;
            }      
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=SalesSummaryReceipt$from.xls");
                echo $this->render('EfektivitasDokter',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'dokter_name' => $dokter_name,
                        'total' => $total,
                        'amount' => $amount,
                        'pt_negara' => PT_NEGARA,
                        'kembali' => $kembali,
                        'store' => $store
                    ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.xml');
            } else {
                $this->render('EfektivitasDokter',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'dokter_name' => $dokter_name,
                        'total' => $total,
                        'amount' => $amount,
                        'kembali' => $kembali,
                        'pt_negara' => PT_NEGARA,
                        'store' => $store
                    ));
//                $this->renderJsonArr($summary);
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'sales.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from,)));
//            $this->TBS->MergeBlock('sales', $summary);
//            $this->TBS->MergeBlock('s', array(array('total' => $total, 'amount' => $amount)));
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "SalesSummaryReceipt$from.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }
   
 public function actionEfektivitasDoctorService() {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $dokterId = $_POST['dokter_id'];
            $summary = U::report_efektivitas_doctor_service($from, $to, $store, $dokterId);
            $total = array_sum(array_column($summary, 'total'));
            if ($store == "") {
                $store = "All Branch";
            }
            if ($dokterId != null) {
                $dokter = Dokter::model()->findByPk($dokterId);
                $dokter_name = $dokter->nama_dokter;
            }   
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'EfektivitasDoctorService',
                'pagination' => false
            ));
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=EfektivitasDoctorService$from-$to.xls");
                echo $this->render('EfektivitasDoctorService', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'dokter_name' => $dokter_name,
                    'store' => $store,
                        'total' => $total
                        ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_rekap.xml');
            } else {
                $this->render('EfektivitasDoctorService', array(
                    'dp' => $dataProvider,
                    'start' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'dokter_name' => $dokter_name,
                    'store' => $store,
                    'total' => $total
                ));
//                Yii::app()->end();
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'dokter_rekap.htm');
            }
//            $this->TBS->MergeBlock('header', array(array('logo' => $this->logo, 'start' => $from, 'to' => $to)));
//            $this->TBS->MergeBlock('dokter', $summary);
//            if ($this->format == 'excel') {
//                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "DokterSummary$from-$to.xls");
//            } else {
//                $this->TBS->Show();
//            }
        }
    }    

     public function actionReportDropping() {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $grup_id = $_POST['grup_id'];
            $group_name ="";
         
            $summary = U::report_dropping($from, $to, $store,$grup_id);
            //$kon = $cmd->getText();
 
            if ($store == "") {
                $store = "All Branch";
            }
            
            if ($grup_id != null) {
                $group = Grup::model()->findByPk($grup_id);
                $group_name = $group->nama_grup;
            }   
            
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'Dropping',
                'pagination' => false
            ));
            $this->pageTitle = 'Dropping';
             if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Dropping$from-$to.xls");
                echo $this->render('Dropping', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store,
                        'group_name' => $group_name
                       
                ), true);              
            } else {
                $this->render('Dropping', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store,
                    'group_name' => $group_name
                ));
            }
        }
    }    
    
     public function actionReportDroppingReturn() {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
           
            $store = $_POST['store_penerima'];
            $store_pengirim = $_POST['store_pengirm'];
           
            $grup_id = $_POST['grup_id'];
            
            
            $group_name ="";
         
            
            $summary = U::report_dropping($from, $to, $store,$grup_id,$store_pengirim);
                        
            if ($store == "") {
                $store = "All Branch";
            }
            
            if ($grup_id != null) {
                $group = Grup::model()->findByPk($grup_id);
                $group_name = $group->nama_grup;
            }   
            
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'Dropping',
                'pagination' => false
            ));
            $this->pageTitle = 'Dropping';
             if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Dropping$from-$to.xls");
                echo $this->render('Dropping', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store,
                        'group_name' => $group_name,
                        'store_pengirim' => $store_pengirim
                       
                ), true);              
            } else {
                $this->render('Dropping', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store,
                    'group_name' => $group_name,
                    'store_pengirim' => $store_pengirim
                ));
            }
        }
    }
    public function actionReportTransferBarangOut()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $store = $_POST['store'];
            $grup_id = $_POST['grup_id'];
            // $tag_id = $_POST['tag_id'];
            $mutasi = U::report_transfer_barang_out($from, $to, $store, $grup_id);
            $dataProvider = new CArrayDataProvider($mutasi, array(
                'id' => 'ReportTransferBarang',
                'pagination' => false
            ));
            if ($store == "") {
                $store = "All Branch";
            }
            $this->pageTitle = 'Transfer Barang In and Out';
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=ReportTransferBarang$from-$to.xls");
                echo $this->render('ReportTransferBarang', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ), true);
//                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'mutasi_stok.xml');
            } else {
                $this->render('ReportTransferBarang', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ));
            }
        }
    } 

    public function actionReportApotek()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];          
            $store = $_POST['store'];
            $rapotek = U::reportapotek($from, $to, $store);
            if ($store == "") {
                $store = "All Branch";
            }
            $dataProvider = new CArrayDataProvider($rapotek, array(
                'id' => 'Apotek',
                'pagination' => false
            ));
            $this->pageTitle = 'Report Apotek';
            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=ReportApotek$from-$to.xls");
                echo $this->render('ReportApotek', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ), true);
            } 
            else {
                $this->render('ReportApotek', array(
                    'dp' => $dataProvider,
                    'from' => sql2date($from, 'dd MMM yyyy'),
                    'to' => sql2date($to, 'dd MMM yyyy'),
                    'store' => $store
                ));
            }
        }
    }

    public function actionAssetDepriciation()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $ati = $_POST['ati'];
            $id = $_POST['asset_trans_id'];

            /* @var $sales_trans AssetDetail */
            //$sales_trans = AssetDetail::model()->findByPk($id);


            /*if ($docref != "") {
                $doc = AssetPeriode::model()->find(array('docref' => $docref));
                $docrefid = $doc->asset_trans_id;
            } else $docrefid = "";*/

            $summary = U::report_asset_depriciation($from, $to, $ati, $id);

            $total = array_sum(array_column($summary, 'penyusutanperbulan'));
            $totalprice = array_sum(array_column($summary, 'asset_trans_price'));
            $totalnewprice = array_sum(array_column($summary, 'asset_trans_new_price'));
            if($total !=0)
            {
                $totalbalance = min(array_column($summary, 'balance'));
            }


            /*if ($docref == "") {
                $docref = "All Customers";
            } else $customer = $cust->no_customer . " - " . $cust->nama_customer;*/

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=AssetDetails$from-$to.xls");
                echo $this->render('AssetDepriciation',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'total' => $total,
                        'totalprice' => $totalprice,
                        'totalnewprice' => $totalnewprice,
                        'totalbalance' => $totalbalance,
                        'ati' => $ati
                    ), true);
            }
            else
            {
                $this->render('AssetDepriciation',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'total' => $total,
                        'totalprice' => $totalprice,
                        'totalnewprice' => $totalnewprice,
                        'totalbalance' => $totalbalance,
                        'ati' => $ati
                    ));
            }
        }
    }

    public function actionAssetTotal()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cat_id = $_POST['category_id'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $branch = $_POST['branch'];

            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name.' ('.$category->category_code.')' : 'All Category';
            $summary = U::report_asset($from, $to, $cat_id, $businessunit_id, $branch);

            $total = array_sum(array_column($summary, 'total'));
            $totalbalance = array_sum(array_column($summary, 'balance'));
            //$totalprice = array_sum(array_column($summary, 'asset_trans_price'));
            //$totalnewprice = array_sum(array_column($summary, 'asset_trans_new_price'));
            if($branch == "")
            {
                $branch = 'All Branch/Store';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('Asset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'total' => $total,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ), true);
            }
            else
            {
                $this->render('Asset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'total' => $total,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ));
            }
        }
    }

    public function actionAssetTotalDetail()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cat_id = $_POST['category_id'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $branch = $_POST['branch'];

            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name.' ('.$category->category_code.')' : 'All Category';
            $summary = U::report_asset_detail($from, $to, $cat_id, $businessunit_id, $branch);

            //$total = array_sum(array_column($summary, 'total'));
            $totalprice = array_sum(array_column($summary, 'price'));
            $totalbalance = array_sum(array_column($summary, 'balance'));

            if($branch == "")
            {
                $branch = 'All Branch/Store';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('AssetTotalDetail',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totalprice' => $totalprice,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ), true);
            }
            else
            {
                $this->render('AssetTotalDetail',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totalprice' => $totalprice,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ));
            }
        }
    }

    public function actionAssetTotalNonActive()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cat_id = $_POST['category_id'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $branch = $_POST['branch'];

            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name.' ('.$category->category_code.')' : 'All Category';
            $summary = U::report_asset_nonactive($from, $to, $cat_id, $businessunit_id, $branch);

            $total = array_sum(array_column($summary, 'total'));
            $totalbalance = array_sum(array_column($summary, 'balance'));
            //$totalprice = array_sum(array_column($summary, 'asset_trans_price'));
            //$totalnewprice = array_sum(array_column($summary, 'asset_trans_new_price'));

            if($branch == "")
            {
                $branch = 'All Branch/Store';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('AssetNonActive',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'total' => $total,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ), true);
            }
            else
            {
                $this->render('AssetNonActive',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'total' => $total,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ));
            }
        }
    }

    public function actionAssetTotalDetailNonActive()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cat_id = $_POST['category_id'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $branch = $_POST['branch'];

            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name.' ('.$category->category_code.')' : 'All Category';
            $summary = U::report_asset_detail_nonactive($from, $to, $cat_id, $businessunit_id, $branch);

            //$total = array_sum(array_column($summary, 'total'));
            $totalprice = array_sum(array_column($summary, 'price'));
            $totalbalance = array_sum(array_column($summary, 'balance'));

            if($branch == "")
            {
                $branch = 'All Branch/Store';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('AssetTotalDetailNonActive',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totalprice' => $totalprice,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ), true);
            }
            else
            {
                $this->render('AssetTotalDetailNonActive',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totalprice' => $totalprice,
                        'totalbalance' => $totalbalance,
                        'category' => $categoryname
                    ));
            }
        }
    }

    public function actionJurnalAsset()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $branch = $_POST['branch'];
            //$cat_id = $_POST['category_id'];
            $businessunit_id = $_COOKIE['businessunitid'];

            //$category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            //$categoryname = $cat_id ? $category->category_name.' ('.$category->category_code.')' : 'All Category';
            $summary = U::report_jurnalasset($from, $to, $businessunit_id, $branch);

            //$total = array_sum(array_column($summary, 'total'));
            $totaldebit = array_sum(array_column($summary, 'debit'));
            $totalkredit = array_sum(array_column($summary, 'kredit'));
            $totalprofit = array_sum(array_column($summary, 'profit'));
            $totalloss = array_sum(array_column($summary, 'loss'));
            $totalppn = array_sum(array_column($summary, 'ppn'));
            $totalprice = array_sum(array_column($summary, 'price'));


            if($branch == "")
            {
                $branch = 'All Branch/Store';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('JurnalAsset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totaldebit' => $totaldebit,
                        'totalkredit' => $totalkredit,
                        'totalprofit' => $totalprofit,
                        'totalloss' => $totalloss,
                        'totalppn' => $totalppn,
                        'totalprice' => $totalprice
                    ), true);
            }
            else
            {
                $this->render('JurnalAsset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totaldebit' => $totaldebit,
                        'totalkredit' => $totalkredit,
                        'totalprofit' => $totalprofit,
                        'totalloss' => $totalloss,
                        'totalppn' => $totalppn,
                        'totalprice' => $totalprice
                    ));
            }
        }
    }

    public function actionRentAsset()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $cat_id = $_POST['category_id'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $branch = $_POST['branch'];

            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name.' ('.$category->category_code.')' : 'All Category';
            $summary = U::report_rentasset($from, $to, $cat_id, $businessunit_id, $branch);

            //$total = array_sum(array_column($summary, 'total'));
            $totalamount = array_sum(array_column($summary, 'amount'));

            if($branch == "")
            {
                $branch = 'All Branch/Store';
            }

            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('RentAsset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totalamount' => $totalamount,
                        'category' => $categoryname
                    ), true);
            }
            else
            {
                $this->render('RentAsset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'totalamount' => $totalamount,
                        'category' => $categoryname
                    ));
            }
        }
    }

    public function actionPrintAsset()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {


            $ti = AssetDetail::model()->find('asset_id = :asset_id', array(':asset_id' => $_POST['asset_id']));
            $tid = AssetDetail::get_details_to_print($_POST['asset_id']);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'asset_detail.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $header_data = array(
                'title' => 'ASSETS',
                'doc_ref' => $ti->docref_other,
                'tgl' => date_format(date_create($ti->asset_trans_date), 'd F Y'),
                'branch' => $ti->asset_trans_branch,
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('item', $tid);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "ASSET $ti->docref_other $ti->asset_trans_branch.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }

    public function actionPrintFakturJualAsset()
    {
        if (Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {


            $ti = AssetDetail::model()->find('asset_id = :asset_id', array(':asset_id' => $_POST['asset_id']));
            $tid = AssetDetail::get_details_to_print($_POST['asset_id']);
            if ($this->format == 'excel') {
                $this->TBS->LoadTemplate(Yii::getPathOfAlias('application.views.reports') . DIRECTORY_SEPARATOR . 'fakturjualasset.xml');
            }
            $this->TBS->SetOption('noerr', true);
            $header_data = array(
                'title' => 'ASSETS',
                'doc_ref' => $ti->docref_other,
                'tgl' => date_format(date_create($ti->asset_trans_date), 'd F Y'),
                'branch' => $ti->asset_trans_branch,
            );
            $this->TBS->MergeField('header', $header_data);
            $this->TBS->MergeBlock('item', $tid);
            if ($this->format == 'excel') {
                $this->TBS->Show(TBS_EXCEL_DOWNLOAD, "ASSET $ti->docref_other $ti->asset_trans_branch.xls");
            } else {
                $this->TBS->Show();
            }
        }
    }

    public function actionDeprisiasiAktif()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $branch = $_POST['branch'];
            $businessunit_id = $_COOKIE['businessunitid'];
            $cat_id = $_POST['category_id'];


            $category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            $categoryname = $cat_id ? $category->category_name.' ('.$category->category_code.')' : 'All Category';


            //$category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            //$categoryname = $cat_id ? $category->category_name.' ('.$category->category_code.')' : 'All Category';
            $summary = U::report_deprisiasiasset($from, $to, $businessunit_id, $branch, $cat_id);

            //$total = array_sum(array_column($summary, 'total'));
            $totaldep = array_sum(array_column($summary, 'depreciationpermonth'));
            $totalacc = array_sum(array_column($summary, 'depreciationaccumulation'));
            $totalnilaibuku = array_sum(array_column($summary, 'nilaibuku'));
            $totaldepbyparam = array_sum(array_column($summary, 'depreciationaccumulationbyparam'));
            $totalnilaibukubyparam = array_sum(array_column($summary, 'nilaibukubyparam'));
            $totalakumulasibyparam = array_sum(array_column($summary, 'akumulasibyparam'));

            if($branch == "")
            {
                $branch = 'All Branch/Store';
            }
            if($cat_id == "")
            {
                $categoryname = 'All Category';
            }
            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=Asset$from-$to.xls");
                echo $this->render('AssetReportDepreciation',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'categoryname' => $categoryname,
                        'totaldep' => $totaldep,
                        'totalacc' => $totalacc,
                        'totalnilaibuku' => $totalnilaibuku,
                        'totaldepbyparam' => $totaldepbyparam,
                        'totalnilaibukubyparam' => $totalnilaibukubyparam,
                        'totalakumulasibyparam' => $totalakumulasibyparam,
                    ), true);
            }
            else
            {
                $this->render('AssetReportDepreciation',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                        'categoryname' => $categoryname,
                        'totaldep' => $totaldep,
                        'totalacc' => $totalacc,
                        'totalnilaibuku' => $totalnilaibuku,
                        'totaldepbyparam' => $totaldepbyparam,
                        'totalnilaibukubyparam' => $totalnilaibukubyparam,
                        'totalakumulasibyparam' => $totalakumulasibyparam,
                    ));
            }
        }
    }

    public function actionShowAsset()
    {
//        if (Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $from = $_POST['tglfrom'];
            $to = $_POST['tglto'];
            $branch = $_POST['branch'];
            $categoryid = $_POST['category'];
            $businessunit_id = $_COOKIE['businessunitid'];

            $user_id = Yii::app()->user->getId();
            $Users = Users::model()->findByPk($user_id);

            if($Users->security_roles_id == 4)
            {
                $summary = U::report_showasset($from, $to, $businessunit_id, $Users->store, $categoryid);
                if($branch == "")
                {
                    $branch =  $Users->store;
                }
            }
            else{
                $summary = U::report_showasset($from, $to, $businessunit_id, $branch, $categoryid);
                if($branch == "")
                {
                    $branch = 'All Branch/Store';
                }
            }
            //$category = $cat_id ? AssetCategory::model()->findByPk($cat_id) : 'All Category';
            //$categoryname = $cat_id ? $category->category_name.' ('.$category->category_code.')' : 'All Category';


            //$total = array_sum(array_column($summary, 'total'));



            $dataProvider = new CArrayDataProvider($summary, array(
                'id' => 'user',
                'pagination' => false
            ));

            if ($this->format == 'excel') {
                header('Content-type: application/vnd.xls');
                header("Content-Disposition: attachment; filename=ShowAsset$branch$from-$to.xls");
                echo $this->render('ShowAsset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                    ), true);
            }
            else
            {
                $this->render('ShowAsset',
                    array(
                        'dp' => $dataProvider,
                        'start' => sql2date($from, 'dd MMM yyyy'),
                        'to' => sql2date($to, 'dd MMM yyyy'),
                        'branch' => $branch,
                    ));
            }
        }
    }
}

