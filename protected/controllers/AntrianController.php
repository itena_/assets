<?php
class AntrianController extends GxController
{
    public function actionCreate()
    {
        $this->redirect(url('/'));
//        $model = new Antrian;
//        if (!Yii::app()->request->isAjaxRequest)
//            return;
//        if (isset($_POST) && !empty($_POST)) {
//            foreach ($_POST as $k => $v) {
//                if (is_angka($v)) $v = get_number($v);
//                $_POST['Antrian'][$k] = $v;
//            }
//            $model->attributes = $_POST['Antrian'];
//            $msg = "Data gagal disimpan.";
//            if ($model->save()) {
//                $status = true;
//                $msg = "Data berhasil di simpan dengan id " . $model->id_antrian;
//            } else {
//                $msg .= " " . CHtml::errorSummary($model);
//                $status = false;
//            }
//            echo CJSON::encode(array(
//                'success' => $status,
//                'msg' => $msg));
//            Yii::app()->end();
//        }
    }
    public function actionSpesial()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        $comm = Yii::app()->antrian->createCommand("SELECT IFNULL(MAX(nomor)+1,1) AS antri FROM antrian 
            WHERE DATE(tanggal)=DATE(now()) AND spesial='1'");
        $numAntri = $comm->queryScalar();
        $comm = Yii::app()->antrian->createCommand("SELECT IFNULL(MAX(id_antrian)+1,1) AS antri FROM antrian");
        $id_antrian = $comm->queryScalar();
        $antri = new Antrian;
        $antri->id_antrian = $id_antrian;
        $antri->nomor = $numAntri;
        $antri->bagian = 'pendaftaran';
        $antri->tujuan = 'konsultasi';
        $antri->picked = 'pick';
        $antri->tanggal = new CDbExpression('NOW()');
        $antri->timestamp = new CDbExpression('NOW()');
        $antri->spesial = '1';
        $msg = "Data gagal disimpan.";
        if ($antri->save()) {
            $status = true;
            $msg = "Data berhasil di simpan dengan id " . $antri->nomor . 'C';
        } else {
            $msg .= " " . CHtml::errorSummary($antri);
            $status = false;
        }
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg));
        Yii::app()->end();
    }

    public function actionUlangi()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            $nomor = $_POST['nomor'];
            $bagian = $_POST['bagian'];
            $tujuan = $_POST['tujuan'];
            $id_antrian = $_POST['id_antrian'];
            $spesial = $_POST['spesial'];
            $datane = "$$nomor|$bagian|$tujuan|$id_antrian|$spesial!";
            $rsl = $this->send2server($datane);
            if ($rsl[0]) {
                $t = str_replace('$', '', $datane);
                $t = str_replace('!', '', $t);
                $arr = explode('|', $t);
                echo CJSON::encode(array(
                    'success' => true,
                    'msg' => $arr
                ));
            } else {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => $rsl[1]
                ));
            }
        }
        Yii::app()->end();
    }
    public function actionPanggil()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        $criteria = new CDbCriteria();
        $criteria->addCondition("DATE(tanggal)=DATE(now())");
        $criteria->addCondition("spesial='1'");
        $criteria->addCondition("bagian <> 'selesai'");
        $criteria->addCondition("bagian <> 'pending'");
        /** @var Antrian $antri */
        $antri = Antrian::model()->find($criteria);
        $datane = "";
        if ($antri == null) {
            $criteria = new CDbCriteria();
            $criteria->addCondition("DATE(tanggal)=DATE(now())");
            $criteria->addCondition("bagian = 'pendaftaran'");
            $criteria->addCondition("bagian <> 'pending'");
            $criteria->order = 'tanggal ASC';
            if ($_POST['bagian'] != 'all') {
                $criteria->addCondition("tujuan=:tujuan");
                $criteria->params = array(':tujuan' => $_POST['bagian']);
            }
            $antri = Antrian::model()->find($criteria);
            if ($antri == null) {
                $datane = "kosong";
            } else {
                $datane = "$$antri->nomor|$antri->bagian|$antri->tujuan|$antri->id_antrian|$antri->spesial!";
            }
        } else {
            $datane = "$$antri->nomor|$antri->bagian|$antri->tujuan|$antri->id_antrian|$antri->spesial!";
        }
        if ($datane == "kosong") {
            echo CJSON::encode(array(
                'success' => true,
                'msg' => 'Tidak ada antrian.'
            ));
        } else {
            $rsl = $this->send2server($datane);
            if ($rsl[0]) {
                $t = str_replace('$', '', $datane);
                $t = str_replace('!', '', $t);
                $arr = explode('|', $t);
                if ($antri != null) {
                    $antri->bagian = 'selesai';
                    if (!$antri->save()) {
                        echo CJSON::encode(array(
                            'success' => true,
                            'msg' => CHtml::errorSummary($antri)
                        ));
                    }
                    echo CJSON::encode(array(
                        'success' => true,
                        'msg' => $arr
                    ));
                }
            } else {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => $rsl[1]
                ));
            }
        }
        Yii::app()->end();
    }

    public function send2server($message)
    {
        //        $message = "$1|pendaftaran|konsultasi|id_antrian|0!";
//        $return = array();
        $sk = fsockopen(ANTRIAN_SERVER_HOST, ANTRIAN_SERVER_PORT, $errnum, $errstr, ANTRIAN_TIMEOUT);
        if (!is_resource($sk)) {
            return array(false, "connection fail: " . $errnum . " " . $errstr);
        }
        return array(true, fwrite($sk, $message));
    }

    public function actionUpdate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        $msg = 'Gagal update data';
        if (isset($_POST) && !empty($_POST)) {
            $id = $_POST['id'];
            $mode = $_POST['mode'];
            /** @var Antrian $model */
            $model = $this->loadModel($id, 'Antrian');
            switch ($mode) {
                case 'pending':
                    $model->bagian = 'pending';
                    break;
                case 'antrian':
                    $model->bagian = 'pendaftaran';
                    break;
                default :
                    break;
            }
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->id_antrian;
            } else {
                $msg .= " " . CHtml::errorSummary($model);
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            echo CJSON::encode(array(
                'success' => false,
                'msg' => 'Tidak ada data yang dikirim.'
            ));
            Yii::app()->end();
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Antrian')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Antrian::model()->findAll($criteria);
        $total = Antrian::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionAntrianIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        $criteria->select = "a.id_antrian,CONCAT(a.nomor,IF(a.spesial = 1,'C',IF(a.tujuan='konsultasi','A','B'))) nomor,
            a.no_member,a.kartu,a.bagian,a.tujuan,a.picked,a.tanggal,a.`timestamp`,a.spesial";
        $criteria->alias = 'a';
        $criteria->addCondition("a.bagian='pendaftaran'");
        $criteria->addCondition("DATE(a.tanggal)=DATE(NOW())");
        if (isset($_POST['tujuan']) && $_POST['tujuan'] != 'all') {
            $criteria->addCondition("a.tujuan=:tujuan");
            $param[':tujuan'] = $_POST['tujuan'];
        }
        $criteria->order = 'a.tanggal ASC';
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = Antrian::model()->findAll($criteria);
        $total = Antrian::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionAntrianPendingIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $param = array();
        $criteria->select = "a.id_antrian,CONCAT(a.nomor,IF(a.spesial = 1,'C',IF(a.tujuan='konsultasi','A','B'))) nomor,
            a.no_member,a.kartu,a.bagian,a.tujuan,a.picked,a.tanggal,a.`timestamp`,a.spesial";
        $criteria->alias = 'a';
        $criteria->addCondition("a.bagian='pending'");
        $criteria->addCondition("DATE(a.tanggal)=DATE(NOW())");
        if (isset($_POST['tujuan']) && $_POST['tujuan'] != 'all') {
            $criteria->addCondition("a.tujuan=:tujuan");
            $param[':tujuan'] = $_POST['tujuan'];
        }
        $criteria->order = 'a.tanggal ASC';
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $criteria->params = $param;
        $model = Antrian::model()->findAll($criteria);
        $total = Antrian::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}