<?php
class SouvenirOutController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            $this->redirect(url('/'));
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new SouvenirOut;
                $ref = new Reference();
                $docref = $ref->get_next_reference(SOUVENIR_OUT);
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) {
                        $v = get_number($v);
                    }
                    $_POST['SouvenirOut'][$k] = $v;
                }
                $_POST['SouvenirOut']['ref'] = $docref;
                $_POST['SouvenirOut']['store'] = STOREID;
                $model->attributes = $_POST['SouvenirOut'];
                if (!$model->save()) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Souvenir Out')) . CHtml::errorSummary($model));
                }
                SouvenirTrans::save_souvenir_trans(SOUVENIR_OUT,$model->souvenir_out_id,$model->doc_ref,
                    $model->tgl,-$model->qty_souvenir,$model->souvenir_id,$model->store);
                PointTrans::save_point_trans(SOUVENIR_OUT,$model->souvenir_out_id,$model->doc_ref,
                    $model->tgl,-$model->qty_point,$model->customer_id,$model->event_id,$model->store);
                $point_total = PointTrans::total_point($model->customer_id,$model->event_id);
                if($point_total < 0){
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Point Trans')) .
                        "Insufficient point, need " .abs($point_total). " more point");
                }
                $souvenir_total = SouvenirTrans::total_souvenir($model->souvenir_id);
                if($souvenir_total < 0){
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Souvenir Trans')) .
                        "Insufficient souvenir, need " .abs($souvenir_total). " more souvenir");
                }
                $ref->save(SOUVENIR_OUT, $model->souvenir_out_id, $docref);
                $msg = t('save.success', 'app');
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'SouvenirOut');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) {
                    $v = get_number($v);
                }
                $_POST['SouvenirOut'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['SouvenirOut'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->souvenir_out_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->souvenir_out_id));
            }
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'SouvenirOut')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        } else {
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = SouvenirOut::model()->findAll($criteria);
        $total = SouvenirOut::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}