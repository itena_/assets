<?php

class PembantuPelunasanUtangDetilController extends GxController {

    public function actionFaktur() {
        $supplier_id = $_POST['supplier_id'];
        $faktur = PembantuPelunasanUtangDetil::get_utang($supplier_id, STOREID);
        $this->renderJsonArr($faktur);
    }

    public function actionIndex() {
        $criteria = new CDbCriteria();
        if ((isset($_POST['pembantu_pelunasan_utang_id']))) {
            $criteria->addCondition("pembantu_pelunasan_utang_id = '" . $_POST['pembantu_pelunasan_utang_id'] . "'");
        }
        $model = PembantuPelunasanUtangDetil::model()->findAll($criteria);
        $total = PembantuPelunasanUtangDetil::model()->count($criteria);

        $this->renderJson($model, $total);
    }

}
