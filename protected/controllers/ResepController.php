<?php
class ResepController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = new Resep;
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Resep'][$k] = $v;
                }
                $model->attributes = $_POST['Resep'];
                $msg = "Data gagal disimpan.";
                if ($model->save()) {
                    $status = true;
                    $msg = "Data berhasil di simpan dengan id " . $model->resep_id;
                } else {
                    $msg .= " " . CHtml::errorSummary($model);
                    throw new Exception($msg);
                }
                foreach ($detils as $detil) {
                    $resepDetails = new ResepDetails;
//                    $_POST['ResepDetails']['kode_bahan'] = $detil['kode_bahan'];
                    $_POST['ResepDetails']['nama_bahan'] = $detil['nama_bahan'];
                    $_POST['ResepDetails']['sat'] = $detil['sat'];
                    $_POST['ResepDetails']['qty'] = get_number($detil['qty']);
                    $_POST['ResepDetails']['kode_bahan'] = $detil['nama_bahan'] . " " . number_format(get_number($detil['qty']),0) . $detil['sat'];
                    $resepDetails->attributes = $_POST['ResepDetails'];
                    $resepDetails->resep_id = $model->resep_id;
                    if (!$resepDetails->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Detail Resep')) . CHtml::errorSummary($resepDetails));
                    }
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $e) {
                $transaction->rollback();
                $status = false;
                $msg = $e->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        if (isset($_POST) && !empty($_POST)) {
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                /** @var Resep $model */
                $model = $this->loadModel($id, 'Resep');
                foreach ($_POST as $k => $v) {
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['Resep'][$k] = $v;
                }
                $msg = "Data gagal disimpan";
                $model->attributes = $_POST['Resep'];
                if ($model->save()) {
                    $status = true;
                    $msg = "Data berhasil di simpan dengan id " . $model->resep_id;
                } else {
                    $msg .= " " . Chtml::errorSummary($model);
                    throw new Exception($msg);
                }
                ResepDetails::model()->deleteAllByAttributes(['resep_id' => $model->resep_id]);
                foreach ($detils as $detil) {
                    $resepDetails = new ResepDetails;
//                    $_POST['ResepDetails']['kode_bahan'] = $detil['kode_bahan'];
                    $_POST['ResepDetails']['nama_bahan'] = $detil['nama_bahan'];
                    $_POST['ResepDetails']['sat'] = $detil['sat'];
                    $_POST['ResepDetails']['qty'] = get_number($detil['qty']);
                    $_POST['ResepDetails']['kode_bahan'] = $detil['nama_bahan'] . " " . number_format(get_number($detil['qty']),0) . $detil['sat'];
                    $resepDetails->attributes = $_POST['ResepDetails'];
                    $resepDetails->resep_id = $model->resep_id;
                    if (!$resepDetails->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Detail Resep')) . CHtml::errorSummary($resepDetails));
                    }
                }
                $transaction->commit();
                $status = true;
            } catch (Exception $e) {
                $transaction->rollback();
                $status = false;
                $msg = $e->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $msg = 'Data berhasil dihapus.';
            $status = true;
            try {
                $this->loadModel($id, 'Resep')->delete();
            } catch (Exception $ex) {
                $status = false;
                $msg = $ex;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(400,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = Resep::model()->findAll($criteria);
        $total = Resep::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}