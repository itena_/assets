<?php
class SalestransDetailsController extends GxController
{
    public function actionCreate()
    {
        $model = new SalestransDetails;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['SalestransDetails'][$k] = $v;
            }
            $model->attributes = $_POST['SalestransDetails'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->salestrans_details;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionKmrkmt()
    {
	    $status = false;
    	if(Yii::app()->request->isPostRequest)
	    {
	    	if(isset($_POST) && !empty($_POST))
		    {
			    $id = $_POST['salestrans_details'];
				$mode = $_POST['mode'];

				if ($mode == "KMR")
				{
					$kmr = $_POST['kmr'];
					$salestrans_id = $_POST['salestrans_id'];
					//HAPUS BONUS KMR
					$model = $this->loadModel($salestrans_id, 'Salestrans');
					if (METHODE_BONUS == 1) {
						$criteria = new CDbCriteria();
						$criteria->addCondition('salestrans_id=:sales');
						$param = array('sales' => $salestrans_id);
						$criteria->params = $param;
						$salestrans_details = SalestransDetails::model()->findAll($criteria);
						Yii::app()->db->createCommand("UPDATE nscc_bonus AS b
						INNER JOIN nscc_bonus_jual AS bj ON bj.bonus_jual_id=b.bonus_jual_id
						INNER JOIN nscc_salestrans_details AS sd ON sd.salestrans_details=b.trans_no
						SET b.visible=0
						WHERE bj.bonus_name_id=3 AND sd.salestrans_id='$salestrans_id'")->execute();
						foreach ($salestrans_details as $row) {
							$bj = BonusJual::model()->findByAttributes(array(
								'barang_id' => $row->barang_id,
								'bonus_name_id' => 3));
							Bonus::simpan_bonus_create('sales', 1, $model, $row, $bj, $kmr);
						}
					}
					//-------------------------------------------------------------------
					$model->dokter_id = $kmr;
					$msg = "Data gagal disimpan.";

					if ($model->save()) {
						$status = true;
						$msg = "Data berhasil di simpan";
					} else {
						$msg .= " " . implode(", ", $model->getErrors());
						$status = false;
					}

					echo CJSON::encode(array(
						'success' => $status,
						'msg' => $msg));
				}
				else
				{
					$kmt = $_POST['kmt'];
					$beauty = $_POST['beauty'];
					$salestrans_details = $_POST['salestrans_details'];

					$model = $this->loadModel($salestrans_details, 'SalestransDetails');

					$kmt_amount = KmtMaster::model()->findByAttributes(array(
						'barang_id'=>$model->barang_id
					));

					$model->dokter_id = $kmt;
					$model->jasa_dokter = $kmt_amount == null ? 0:$kmt_amount->amount;
					$msg = "Data gagal disimpan.";

					if ($model->save()) {
						//BONUS
						//delete bonus
						if(!empty($kmt) && METHODE_BONUS == 1) {
							$bj = BonusJual::model()->findByAttributes(array(
								'bonus_name_id' => 8,
								'barang_id' => $model->barang_id
							));
							Bonus::model()->updateAll(
								array('visible' => 0),
								'trans_no = :trans_no AND tipe_trans = :tipe_trans AND store = :store AND
							bonus_jual_id = :bj',
								array(
									':trans_no' => $salestrans_details,
									':tipe_trans' => 'service',
									':store' => STOREID,
									':bj' => $bj->bonus_jual_id
								)
							);
							$salestrans = Salestrans::model()->find('salestrans_id = :salestrans_id', array(':salestrans_id' => $model->salestrans_id));
							Bonus::serviceDokter(PENJUALAN, $salestrans, $model, $kmt);
						}
						//--------------------------------------------------
						app()->db->autoCommit = false;
						$transaction = Yii::app()->db->beginTransaction();
						try {
							if (!$model->update()) {
								throw new Exception(CHtml::errorSummary($model));
							} else {
								//HAPUS BONUS BONBET
								if (METHODE_BONUS == 1)
									Yii::app()->db->createCommand("UPDATE nscc_bonus AS b
										INNER JOIN nscc_bonus_jual AS bj ON bj.bonus_jual_id=b.bonus_jual_id
										SET b.visible=0
										WHERE bj.bonus_name_id=9 AND b.trans_no='$salestrans_details'")->execute();
								//-------------------------------------------------------------------
								$model->save_beauty_tips($beauty, '','', '', '');
								$msg = 'Success update.';
							}

							$transaction->commit();
							$status = true;
						} catch (Exception $ex) {
							$transaction->rollback();
							$status = false;
							$msg = $ex->getMessage();
						}
						app()->db->autoCommit = true;
					} else {
						$msg .= " " . implode(", ", $model->getErrors());
						$status = false;
					}

					echo CJSON::encode(array(
						'success' => $status,
						'msg' => $msg));
				}
		    }
	    }
	    Yii::app()->end();
    }

    public function actionUpdate()
    {
        if (Yii::app()->request->isPostRequest) {
            $status = false;
            $msg = 'Failed update data.';
            if (isset($_POST) && !empty($_POST)) {
                $id = $_POST['salestrans_details'];
                $dokter_id = $_POST['dokter_id'];
                $beauty_id = $_POST['beauty_id'];
                $beauty2_id = $_POST['beauty2_id'];
                $beauty3_id = $_POST['beauty3_id'];
                $beauty4_id = $_POST['beauty4_id'];
                $beauty5_id = $_POST['beauty5_id'];
	            $model = $this->loadModel($id, 'SalestransDetails');
	            if (KMT_PERAWATAN)
	            {
		            $kmt = KmtMaster::model()->findByAttributes(array(
			            'barang_id'=>$model->barang_id
		            ));
		            if ($kmt != null)
			            $jasa_dokter = $kmt->amount;
		            else
			            $jasa_dokter = 0;
	            }
	            else {
		            $jasa_dokter = SysPrefs::get_val("kmt_nominal");
		            if ($jasa_dokter == null) {
			            $jasa_dokter = 0;
		            }
	            }
                /** @var $model SalestransDetails */
                $model->dokter_id = $dokter_id;
                $model->jasa_dokter = $jasa_dokter;
                app()->db->autoCommit = false;
                $transaction = Yii::app()->db->beginTransaction();
                try {
                    if (!$model->update()) {
                        throw new Exception(CHtml::errorSummary($model));
                    } else {
                        
                        if(METHODE_BONUS == 1) {
                            /*
                             * Hapus data bonus sebelumnya (jika ada), agar tidak double
                             */
                            Bonus::deleteDataBonus($model->salestrans_details, 'service');

                            /*
                             * Bonus Aishaderm
                             * DOKTER TINDAKAN
                             */
                            $salestrans = Salestrans::model()->find('salestrans_id = :salestrans_id', array(':salestrans_id'=>$model->salestrans_id));
                            Bonus::serviceDokter(PENJUALAN, $salestrans, $model, $dokter_id);
                        }
                    
                        $model->save_beauty_tips($beauty_id, $beauty2_id,
                            $beauty3_id, $beauty4_id, $beauty5_id);
                        $msg = 'Success update.';
                    }
                    
                    $transaction->commit();
                    $status = true;
                } catch (Exception $ex) {
                    $transaction->rollback();
                    $status = false;
                    $msg = $ex->getMessage();
                }
                app()->db->autoCommit = true;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        } else
            throw new CHttpException(403,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionFinal()
    {
        if (Yii::app()->request->isPostRequest) {
            if (!Tender::is_exist($_POST['tgl'])) {
                echo CJSON::encode(array(
                    'success' => false,
                    'msg' => 'Post / Final bisa dilakukan setelah tender deklarasi dibuat di tgl yang sama.'
                ));
            } else {
                $tgl = $_POST['tgl'];
                $arr = Salestrans::set_final_service($tgl);
                echo CJSON::encode(array(
                    'success' => $arr > 0,
                    'msg' => "Final affected $arr row"));
            }
            Yii::app()->end();
        } else
            throw new CHttpException(403,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionGetDiskon()
    {
        if (Yii::app()->request->isPostRequest) {
            $customer_id = $_POST['customer_id'];
            $barang_id = $_POST['barang_id'];
            $arr = SalestransDetails::get_diskon($customer_id, $barang_id);
//            $jual = Jual::model()->findByAttributes(['barang_id' => $barang_id, 'store' => STOREID]);
            if (!$arr) {
                $arr['price'] = 0;
                $arr['value'] = 0;
                $arr['kode'] = null;
                $arr['nama'] = null;
            }
//            if ($jual == null) {
//                $arr['price'] = '0';
//            } else {
//                $arr['price'] = $jual->price;
//            }
            echo CJSON::encode(array(
                'success' => true,
                'msg' => $arr));
            Yii::app()->end();
        } else
            throw new CHttpException(403,
                Yii::t('app', 'Invalid request. Please do not repeat this request again.'));
    }
    public function actionIndex()
    {
        if (Yii::app()->request->isPostRequest) {
            $criteria = new CDbCriteria();
//            $this->renderJsonArr(SalestransDetails::get_sales_trans_details($_POST['salestrans_id']));
            if (isset($_POST['salestrans_id'])) {
                $criteria->addCondition("salestrans_id = :salestrans_id");
                $criteria->params = array(':salestrans_id' => $_POST['salestrans_id']);
                $model = SalestransDetailsView::model()->findAll($criteria);
                $total = SalestransDetailsView::model()->count($criteria);
                $this->renderJson($model, $total);
            } elseif (isset($_POST['konsul_id'])) {
                $criteria->addCondition("konsul_id = :konsul_id");
                $criteria->params = array(':konsul_id' => $_POST['konsul_id']);
                $model = KonsulSalestrans::model()->findAll($criteria);
                $total = KonsulSalestrans::model()->count($criteria);
                $this->renderJson($model, $total);
            }
        }
    }
}