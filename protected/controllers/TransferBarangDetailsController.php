<?php
class TransferBarangDetailsController extends GxController
{
    public function actionIndexIn()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("transfer_barang_id = :transfer_barang_id");
        $criteria->addCondition("visible = 1");
        $criteria->params = array(':transfer_barang_id' => $_POST['transfer_barang_id']);
        $model = TransferBarangDetails::model()->findAll($criteria);
        $total = TransferBarangDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
//    public function actionIndexOut()
//    {
//        $criteria = new CDbCriteria();
//        $criteria->select = "transfer_barang_detail_id,-qty qty,barang_id,transfer_barang_id,
//        -total total,disc,-discrp discrp,-bruto bruto,vat,-vatrp vatrp,disc1,-discrp1 discrp,-total_pot total_pot";
//        $criteria->addCondition("transfer_barang_id = :transfer_barang_id");
//        $criteria->params = array(':transfer_barang_id'=>$_POST['transfer_barang_id']);
//        $model = TransferBarangDetails::model()->findAll($criteria);
//        $total = TransferBarangDetails::model()->count($criteria);
//        $this->renderJson($model, $total);
//    }
    public function actionIndexOut()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition("transfer_barang_id = :transfer_barang_id");
        $criteria->addCondition("visible = 1");
        $criteria->params = array(':transfer_barang_id' => $_POST['transfer_barang_id']);
        $model = TransferBarangDetails::model()->findAll($criteria);
        $total = TransferBarangDetails::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}