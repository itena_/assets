<?php
Yii::import( 'application.components.U' );
class SiteController extends GxController {
//    public function accessRules() {
//        return array(
//            array('allow',
//                'users' => array('*'),
//                'actions' => array('login'),
//            )           
//        );
//    }
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class'     => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'    => array( 'class' => 'CViewAction', ),
		);
	}
	public function actionReDocRef() {
		$criteria = new CDbCriteria();
		$criteria->addCondition( 'type_ = 1' );
		$criteria->order      = 'tdate';
		app()->db->autoCommit = false;
		$transaction          = Yii::app()->db->beginTransaction();
		/** @var Salestrans[] $sales_all */
		$sales_all = Salestrans::model()->findAll( $criteria );
		try {
			foreach ( $sales_all as $sales ) {
				$ref            = new Reference();
				$doc_ref        = $ref->get_next_reference( PENJUALAN );
				$sales->doc_ref = $doc_ref;
				$sales->saveAttributes( [ 'doc_ref' => $doc_ref ] );
				BankTrans::model()->updateAll(
					[ 'ref' => $doc_ref ],
					'type_ = 1 AND trans_no = :trans_no',
					[ ':trans_no' => $sales->salestrans_id ]
				);
				$ref->save( PENJUALAN, $sales->salestrans_id, $doc_ref );
			}
			$transaction->commit();
		} catch ( Exception $ex ) {
			$transaction->rollback();
			var_dump( $ex );
		}
		app()->db->autoCommit = true;
		echo $doc_ref;
		echo $ref->get_next_reference( PENJUALAN );
	}
	public function actionDateDiff() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$date1 = $_POST['dari'];
			$date2 = $_POST['sampai'];
			$diff  = abs( strtotime( $date2 ) - strtotime( $date1 ) );
			$years = floor( $diff / ( 365 * 60 * 60 * 24 ) );
			echo CJSON::encode(
				array(
					'status' => true,
					'msg'    => $years
				) );
			Yii::app()->end();
		}
	}
	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex() {


		$this->layout = 'main';
		if ( ! isset( $_POST['app'] ) ) {

			$user = Users::model()->findByPk( user()->getId() );
            //$bsu = Businessunit::model()->findByPk( $user->businessunit_id );
			$tree = new MenuTree( $user->security_roles_id );
/*			if ( $tree->getState( 9996 ) ) {
				$this->redirect( url( 'site/counter' ) );
			} elseif ( $tree->getState( 9997 ) ) {
				$this->redirect( url( 'site/dokter' ) );
			} elseif ( $tree->getState( 9998 ) ) {
				$this->redirect( url( 'site/perawatan' ) );
			} */
			if ( $tree->getState( 9999 ) ) {
				$this->render( 'sales', array( 'state' => 0, 'username' => $user->name ) );
			} else {
                setcookie('businessunitid', $user->businessunit_id, time() + (86400 * 30), "/");
				$this->render( 'index' );
			}
		} else {
			$this->render( 'index' );
		}
	}
	public function actionMarket() {
		$this->layout = 'market';
//        if (!isset($_POST['app'])) {
//            $user = Users::model()->findByPk(user()->getId());
//            $tree = new MenuTree($user->security_roles_id);
//            $state = $tree->getState();
//            if ($state > 0) {
//                $this->render('sales', array('state' => $state, 'username' => $user->name));
//            }
//        }
		$this->render( 'market' );
	}
	public function actionDokter() {
		$this->layout = 'dokter';
//        if (!isset($_POST['app'])) {
//            $user = Users::model()->findByPk(user()->getId());
//            $tree = new MenuTree($user->security_roles_id);
//            $state = $tree->getState();
//            if ($state > 0) {
//                $this->render('sales', array('state' => $state, 'username' => $user->name));
//            }
//        }
		$this->render( 'dokter' );
	}
	public function actionServerAntrian() {
		$this->layout = 'dokter';
		$this->render( 'serverantrian' );
	}
	public function actionCounter() {
		$this->layout = 'main';
//        if (!isset($_POST['app'])) {
//            $user = Users::model()->findByPk(user()->getId());
//            $tree = new MenuTree($user->security_roles_id);
//            $state = $tree->getState();
//            if ($state > 0) {
//                $this->render('sales', array('state' => $state, 'username' => $user->name));
//            }
//        }
		$this->render( 'counter' );
	}
	public function actionCc() {
		var_dump( opcache_reset() );
	}
	public function actionPerawatan() {
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->layout = 'main';
		$this->render( 'perawatan' );
	}
	public function actionPosMode() {
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->layout = 'main';
		$this->render( 'sales' );
	}
	public function actionGetDateTime() {
		if ( Yii::app()->request->isAjaxRequest ) {
			echo CJSON::encode( array(
				'success'  => true,
				'datetime' => date( 'Y-m-d H:i:s' )
			) );
			Yii::app()->end();
		}
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ( $error = Yii::app()->errorHandler->error ) {
			if ( Yii::app()->request->isAjaxRequest ) {
//                if (is_array($error)) {
//                    $error = implode("\n", $error);
//                }
				throw new Exception( $error['message'] );
//                echo CJSON::encode(array(
//                    'success' => false,
//                    'msg' => $error
//                ));
//                Yii::app()->end();
			} else {
				$this->render( 'error', $error );
			}
		}
	}
	/**
	 * Displays the contact page
	 */
	public function actionContact() {
		$model = new ContactForm;
		if ( isset( $_POST['ContactForm'] ) ) {
			$model->attributes = $_POST['ContactForm'];
			if ( $model->validate() ) {
				$name    = '=?UTF-8?B?' . base64_encode( $model->name ) . '?=';
				$subject = '=?UTF-8?B?' . base64_encode( $model->subject ) . '?=';
				$headers = "From: $name <{$model->email}>\r\n" . "Reply-To: {$model->email}\r\n" .
				           "MIME-Version: 1.0\r\n" . "Content-type: text/plain; charset=UTF-8";
				mail( Yii::app()->params['adminEmail'], $subject, $model->body, $headers );
				Yii::app()->user->setFlash( 'contact',
					'Thank you for contacting us. We will respond to you as soon as possible.' );
				$this->refresh();
			}
		}
		$this->render( 'contact', array( 'model' => $model ) );
	}
	public function actionLogout()
    {
        if (isset($_COOKIE['businessunitid'])) {
            unset($_COOKIE['businessunitid']);
            setcookie('businessunitid', '', time() - 3600, '/'); // empty value and old timestamp
        }
        if (isset($_COOKIE['businessunitcode'])) {
            unset($_COOKIE['businessunitcode']);
            setcookie('businessunitcode', '', time() - 3600, '/'); // empty value and old timestamp
        }
		Yii::app()->user->logout();
		$this->redirect( Yii::app()->homeUrl );
	}

	public function actionLogin() {
		if ( ! Yii::app()->user->isGuest ) {
			$this->redirect( Yii::app()->homeUrl );
		} else {
			if ( ! Yii::app()->request->isAjaxRequest ) {
				$url_ = reset( $_SESSION );
				if ( $url_ == bu( 'login' ) ) {
					$url_ = bu();
				}
				$this->layout = 'login';
				$this->render( 'login', array( 'url_' => $url_ ) );
			} else {
				$model         = new LoginForm;
				$loginUsername = isset( $_POST["loginUsername"] ) ? $_POST["loginUsername"]
					: "";
				$loginPassword = isset( $_POST["loginPassword"] ) ? $_POST["loginPassword"]
					: "";
				if ( $loginUsername != "" ) {
					//$model->attributes = $_POST['LoginForm'];
					// validate user input and redirect to the previous page if valid
					$model->username = $loginUsername;
					$model->password = $loginPassword;
//				$model->browser  = $browser_id;
					if ( $model->validate() && $model->login() ) {
						$browser = Browser::saveInfo( $_POST['ip_local'], $_POST['ip_public'], $_POST['browserdata'], $_POST['fingerprint'], $_POST['useragent'],
							$_POST['browser'], $_POST['browserversion'], $_POST['browsermajorversion'], $_POST['ie'], $_POST['chrome'], $_POST['firefox'], $_POST['safari'],
							$_POST['opera'], $_POST['engine'], $_POST['engineversion'], $_POST['os'], $_POST['osversion'], $_POST['windows'], $_POST['mac_os'], $_POST['linux'], $_POST['ubuntu'],
							$_POST['solaris'], $_POST['device'], $_POST['devicetype'], $_POST['devicevendor'], $_POST['cpu'], $_POST['mobile'], $_POST['mobilemajor'], $_POST['mobileandroid'],
							$_POST['mobileopera'], $_POST['mobilewindows'], $_POST['mobileblackberry'], $_POST['mobileios'], $_POST['iphone'], $_POST['ipad'], $_POST['ipod'], $_POST['screenprint'],
							$_POST['colordepth'], $_POST['currentresolution'], $_POST['availableresolution'], $_POST['devicexdpi'], $_POST['deviceydpi'], $_POST['plugins'], $_POST['java'],
							$_POST['javaversion'], $_POST['flash'], $_POST['flashversion'], $_POST['silverlight'], $_POST['silverlightversion'], $_POST['mimetypes'], $_POST['ismimetypes'],
							$_POST['font'], $_POST['fonts'], $_POST['localstorage'], $_POST['sessionstorage'], $_POST['cookie'], $_POST['timezone'], $_POST['language'], $_POST['systemlanguage'], $_POST['canvas'] );
						Yii::app()->user->setBrowser( $browser->browser_id );
						LogTrans::saveLog( '', '', 'LOGIN BERHASIL', '' );
                        //setcookie('username', $loginUsername, time() - 3600, '/');
						echo "{success: true}";
					} else {
						echo "{success: false, errors: { reason: 'Login failed. Try again.' }}";
					}
				} else {
					echo "{success: false, errors: { reason: 'Login failed. Try again' }}";
				}
			}
		}
	}
	public function actionLoginOverride() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			$this->redirect( '/' );
		} else {
			$count = Users::get_override( $_POST["loginUsername"], $_POST["loginPassword"] );
			if ( $count ) {
				echo CJSON::encode( array(
					'success' => true,
					'msg'     => $count
				) );
			} else {
				echo CJSON::encode( array(
					'success' => false,
					'msg'     => "You don't have permission."
				) );
			}
		}
	}
	public function actionGenerate() {
		$templatePath = './css/silk_v013/icons';
		$files        = scandir( $templatePath );
		$txt          = "";
		foreach ( $files as $file ) {
			if ( is_file( $templatePath . '/' . $file ) ) {
				$basename = explode( ".", $file );
				$name     = $basename[0];
				$txt      .= ".silk13-$name { background-image: url(icons/$file) !important; background-repeat: no-repeat; }\n";
			}
		}
		$myFile = "silk013.css";
		$fh = fopen( $myFile, 'w' ) or die( "can't open file" );
		fwrite( $fh, $txt );
		fclose( $fh );
	}
	public function actionTree() {
		$user = Users::model()->findByPk( user()->getId() );
		$menu = new MenuTree( $user->security_roles_id );
		$data = $menu->get_menu();
		Yii::app()->end( $data );
	}
	public function actionLock() {
		$x = @fopen( SALES_LOCK, "w" );
		if ( ! flock( $x, LOCK_EX | LOCK_NB ) ) {
			echo CJSON::encode( array(
				'success' => false,
				'id'      => '',
				'msg'     => "Transaksi belum bisa dilakukan, silahkan coba lagi beberapa saat."
			) );
			Yii::app()->end();
		}
		sleep( 10 );
		fflush( $x );            // flush output before releasing the lock
		flock( $x, LOCK_UN );
		fclose( $x );
	}
	public function actionUnLock() {
		$x = @fopen( SALES_LOCK, "w" );
		if ( ! flock( $x, LOCK_EX | LOCK_NB ) ) {
			echo CJSON::encode( array(
				'success' => false,
				'id'      => '',
				'msg'     => "Transaksi belum bisa dilakukan, silahkan coba lagi beberapa saat."
			) );
			Yii::app()->end();
		}
		fflush( $x );            // flush output before releasing the lock
		flock( $x, LOCK_UN );
		fclose( $x );
	}
	public function actionBackupAll() {
		if ( ! Yii::app()->request->isPostRequest ) {
			$this->redirect( bu() );
		}
		ini_set( 'max_execution_time', 600 );
		$conn = Yii::app()->db;
		$user = $conn->username;
		$pass = $conn->password;
		if ( preg_match( '/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result ) ) {
			list( $all, $host, $db, $port ) = $result;
		}
		$dir_backup = DIR_BACKUP;
		$files      = scandir( $dir_backup );
		foreach ( $files as $file ) {
			if ( is_file( "$dir_backup\\$file" ) ) {
				unlink( "$dir_backup\\$file" );
			}
		}
		$filename    = $db . date( "Y-m-d-H-i-s" ) . '.pos';
		$backup_file = $dir_backup . $filename;
		$mysqldump   = MYSQLDUMP;
		$command     = "$mysqldump --opt -h $host -u $user --password=$pass -P $port --routines " .
		               "$db > $backup_file";
		system( $command );
		$size = filesize( $backup_file );
		if ( $size > 0 ) {
			$zip = new ZipArchive();
			if ( $zip->open( "$backup_file.zip", ZIPARCHIVE::CREATE ) ) {
				$zip->addFile( $backup_file, $filename );
				$zip->close();
				$finfo    = finfo_open( FILEINFO_MIME_TYPE );
				$mimeType = finfo_file( $finfo, "$backup_file.zip" );
				$size     = filesize( "$backup_file.zip" );
				$name     = basename( "$backup_file.zip" );
				if ( isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] == 'on' ) {
					// cache settings for IE6 on HTTPS
					header( 'Cache-Control: max-age=120' );
					header( 'Pragma: public' );
				} else {
					header( 'Cache-Control: private, max-age=120, must-revalidate' );
					header( "Pragma: no-cache" );
				}
				header( "Expires: Sat, 26 Jul 1997 05:00:00 GMT" ); // long ago
				header( "Content-Type: $mimeType" );
				header( 'Content-Disposition: attachment; filename="' . $name . '";' );
				header( "Accept-Ranges: bytes" );
				header( 'Content-Length: ' . filesize( "$backup_file.zip" ) );
				print readfile( "$backup_file.zip" );
				exit;
			}
//            $bu = $dir_backup . basename("$backup_file.zip");
//            header("Location: $bu");
		}
		ini_set( 'max_execution_time', 30 );
	}
	public function actionRestoreAll() {
		if ( ! Yii::app()->request->isPostRequest ) {
			$this->redirect( bu() );
		}
		$conn = Yii::app()->db;
		$user = $conn->username;
		$pass = $conn->password;
		if ( preg_match( '/^mysql:host=(.*);dbname=(.*);(?:port=(.*))?/', $conn->connectionString, $result ) ) {
			list( $all, $host, $db, $port ) = $result;
		}
		$dir_backup = dirname( Yii::app()->request->scriptFile ) . "\\backup\\";
		if ( isset( $_FILES["filename"] ) ) { // it is recommended to check file type and size here
			if ( $_FILES["filename"]["error"] > 0 ) {
				echo CJSON::encode( array(
					'success' => false,
					'msg'     => $_FILES["file"]["error"]
				) );
			} else {
				$backup_file = $dir_backup . $_FILES["filename"]["name"];
				move_uploaded_file( $_FILES["filename"]["tmp_name"], $backup_file );
				$mysql   = dirname( Yii::app()->request->scriptFile ) . "\\mysql";
				$gzip    = dirname( Yii::app()->request->scriptFile ) . "\\gzip";
				$command = "$gzip -d $backup_file";
				system( $command );
				$backup_file = substr( $backup_file, 0, - 3 );
				if ( ! file_exists( $backup_file ) ) {
					echo CJSON::encode( array(
						'success' => false,
						'msg'     => "Failed restore file " . $_FILES["file"]["name"]
					) );
				} else {
					Yii::app()->db->createCommand( "DROP DATABASE $db" )
					              ->execute();
					Yii::app()->db->createCommand( "CREATE DATABASE IF NOT EXISTS $db" )
					              ->execute();
					$command = "$mysql -h $host -u $user --password=$pass -P $port " .
					           "$db < $backup_file";
					system( $command );
					echo CJSON::encode( array(
						'success' => true,
						'msg'     => "Succefully restore file " . $_FILES["file"]["name"]
					) );
				}
				Yii::app()->end();
			}
		}
	}
	public function actionDeleteTransAll() {
		if ( ! Yii::app()->request->isPostRequest ) {
			$this->redirect( bu() );
		}
		Yii::app()->db->createCommand( "
        /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
        TRUNCATE TABLE nscc_bank_trans;
        TRUNCATE TABLE nscc_beauty_services;
        TRUNCATE TABLE nscc_kas_detail;
        TRUNCATE TABLE nscc_kas;
        TRUNCATE TABLE nscc_salestrans_details;
        TRUNCATE TABLE nscc_salestrans;
        TRUNCATE TABLE nscc_stock_moves;
        TRUNCATE TABLE nscc_tender_details;
        TRUNCATE TABLE nscc_tender;
        TRUNCATE TABLE nscc_transfer_item_details;
        TRUNCATE TABLE nscc_transfer_item;
        TRUNCATE TABLE nscc_gl_trans;
        TRUNCATE TABLE nscc_comments;
        TRUNCATE TABLE nscc_payment;
        TRUNCATE TABLE nscc_refs;
        TRUNCATE TABLE nscc_sync;
        TRUNCATE TABLE nscc_upload_log;
        TRUNCATE TABLE nscc_pelunasan_utang_detil;
        TRUNCATE TABLE nscc_pelunasan_utang;
        /*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
        " )->execute();
		echo CJSON::encode( array(
			'success' => true,
			'msg'     => "Succefully delete all transaction "
		) );
	}
	public function actionSync() {
		session_write_close();
		$stat = SysPrefs::model()->find( 'name_ = :name AND store =:store',
			array( ':name' => 'sync_active', ':store' => STOREID ) );
		if ( $stat->value_ != "0" ) {
			$date    = strtotime( $stat->value_ );
			$date2   = time();
			$subTime = $date2 - $date;
			$h       = $subTime / ( 60 * 60 );
			if ( $h > 1 ) {
				$stat->value_ = '0';
				$stat->save();
			}
			$msg = 'other client already sync. Diff time ' . $h;
		} else {
			$stat->value_ = date( 'Y-m-d H:i:s' );
			$stat->save();
			$s            = new SyncData();
			$msg          = $s->sync();
			$stat->value_ = '0';
			$stat->save();
		}
		$report = json_encode( array(
			'type' => 'event',
			'name' => 'message',
			'time' => date( 'g:i:s a' ),
			'data' => $msg
		),
			JSON_PRETTY_PRINT );
		echo $report;
	}
	public function actionTestEncrypt() {
		$global   = array(
			'SecurityRoles',
			'Gol',
			'TransTipe',
			'StatusCust',
			'ChartMaster',
			'Supplier',
			'Kategori',
			'Negara',
			'Provinsi',
			'Kota',
			'Kecamatan',
			'Store'
		);
		$criteria = new CDbCriteria;
		$criteria->addCondition( "up = 0" );
		$upload  = array();
		$content = array();
		foreach ( $global as $modelName ) {
			$model  = new $modelName();
			$tables = $model::model()->findAll( $criteria );
			if ( $tables != null ) {
				$upload[ $modelName ]  = $tables;
				$content[ $modelName ] = number_format( count( $tables ) ) . ' record';
			}
		}
		$to = yiiparam( 'Username' );
		echo mailsend( $to, $to, 'GLOBAL-' . sha1( STOREID . "^" . date( 'Y-m-d H:i:s' ) ),
			json_encode( array(
				'from'    => STOREID,
				'date'    => date( 'Y-m-d H:i:s' ),
				'content' => array( $content )
			), JSON_PRETTY_PRINT ),
			bzcompress( Encrypt( CJSON::encode( $upload ) ), 9 ) );
	}
	public function actionCheckEmail() {
		$username      = 'nscc.sync@gmail.com';
		$password      = 'zaq!@#$%';
		$imapmainbox   = "TSBK01";
		$messagestatus = "ALL";
		$imapaddress   = "{imap.gmail.com:993/imap/ssl}";
		$hostname      = $imapaddress . $imapmainbox;
		$connection = imap_open( $hostname, $username,
			$password ) or die( 'Cannot connect to Gmail: ' . imap_last_error() );
		$emails      = imap_search( $connection, $messagestatus );
		$totalemails = imap_num_msg( $connection );
		echo "Total Emails: " . $totalemails . "<br>";
		if ( $emails ) {
			//sort emails by newest first
			//rsort($emails);
			//loop through every email int he inbox
			foreach ( $emails as $email_number ) {
				//grab the overview and message
				$header = imap_fetch_overview( $connection, $email_number, 0 );
				//Because attachments can be problematic this logic will default to skipping the attachments
				$structure   = imap_fetchstructure( $connection, $email_number );
				$attachments = array();
				if ( isset( $structure->parts ) && count( $structure->parts ) ) {
					for ( $i = 0; $i < count( $structure->parts ); $i ++ ) {
						$attachments[ $i ] = array(
							'is_attachment' => false,
							'filename'      => '',
							'name'          => '',
							'attachment'    => ''
						);
						if ( $structure->parts[ $i ]->ifdparameters ) {
							foreach ( $structure->parts[ $i ]->dparameters as $object ) {
								if ( strtolower( $object->attribute ) == 'filename' ) {
									$attachments[ $i ]['is_attachment'] = true;
									$attachments[ $i ]['filename']      = $object->value;
								}
							}
						}
						if ( $structure->parts[ $i ]->ifparameters ) {
							foreach ( $structure->parts[ $i ]->parameters as $object ) {
								if ( strtolower( $object->attribute ) == 'name' ) {
									$attachments[ $i ]['is_attachment'] = true;
									$attachments[ $i ]['name']          = $object->value;
								}
							}
						}
						if ( $attachments[ $i ]['is_attachment'] ) {
							$attachments[ $i ]['attachment'] = imap_fetchbody( $connection, $email_number, $i + 1 );
							if ( $structure->parts[ $i ]->encoding == 3 ) { // 3 = BASE64
								$attachments[ $i ]['attachment'] = base64_decode( $attachments[ $i ]['attachment'] );
							} elseif ( $structure->parts[ $i ]->encoding == 4 ) { // 4 = QUOTED-PRINTABLE
								$attachments[ $i ]['attachment'] = quoted_printable_decode( $attachments[ $i ]['attachment'] );
							}
						}
					} // for($i = 0; $i < count($structure->parts); $i++)
				} // if(isset($structure->parts) && count($structure->parts))
				$status  = ( $header[0]->seen ? 'read' : 'unread' );
				$subject = $header[0]->subject;
				$from    = $header[0]->from;
				$date    = $header[0]->date;
				echo "status: " . $status . "<br>";
				echo "subject: " . $subject . "<br>";
				echo "from: " . $from . "<br>";
				echo "date: " . $date . "<br>";
				$message = imap_fetchbody( $connection, $email_number, 1.1 );
				if ( $message == "" ) { // no attachments is the usual cause of this
					$message = imap_fetchbody( $connection, $email_number, 1 );
				}
				echo "body: " . $message . "<br>";
				if ( count( $attachments ) != 0 ) {
					foreach ( $attachments as $at ) {
						if ( $at[ is_attachment ] == 1 ) {
//                            file_put_contents($at[filename], $at[attachment]);
							echo "Attachment: " . Decrypt( bzdecompress( $at[ attachment ] ) ) . "<br>";
						}
					}
				}
				echo "<hr><br>";
			}
		}
		imap_close( $connection );
	}
	public function actionPoll() {
		echo json_encode( array(
			'type' => 'event',
			'name' => 'message',
			'data' => 'Successfully polled at: ' . date( 'g:i:s a' )
		) );
	}
	public function actionDecrypt() {
		$this->render( 'Decrypt' );
		if ( isset( $_POST['content'] ) ) {
			echo Decrypt( $_POST['content'] );
		}
	}
	public function actionClosing() {
		if ( ! Yii::app()->request->isAjaxRequest ) {
			return;
		}
		if ( isset( $_POST ) && ! empty( $_POST ) ) {
			$year        = $_POST['year'];
			$tgl         = $year . '-12-31';
			$store       = $_POST['store'];
			$transaction = Yii::app()->db->beginTransaction();
			try {
				$gltrans   = Yii::app()->db->createCommand( "
                SELECT ngl.account_code,IFNULL(SUM(ngl.amount),0) amount
                FROM nscc_gl_trans ngl
                WHERE ngl.tran_date <= :tgl AND ngl.visible = 1 AND ngl.store = :store
                GROUP BY ngl.account_code" )
				                           ->queryAll( true, array( ':tgl' => $tgl, ':store' => $store ) );
				$banktrans = Yii::app()->db->createCommand( "
                SELECT nbt.bank_id,SUM(nbt.amount) amount
                FROM nscc_bank_trans nbt
                WHERE nbt.tgl <= :tgl AND nbt.visible = 1 AND nbt.store = :store
                GROUP BY nbt.bank_id" )
				                           ->queryAll( true, array( ':tgl' => $tgl, ':store' => $store ) );
				$stockmove = Yii::app()->db->createCommand( "
                SELECT nsm.barang_id,SUM(nsm.qty) qty
                FROM nscc_stock_moves nsm
                WHERE nsm.tran_date <= :tgl AND nsm.store = :store
                GROUP BY nsm.barang_id" )
				                           ->queryAll( true, array( ':tgl' => $tgl, ':store' => $store ) );
				Yii::app()->db->createCommand( "
                DELETE FROM nscc_audit WHERE tgl <= :tgl AND store = :store;
                DELETE FROM nscc_bank_trans WHERE tgl <= :tgl AND store = :store;
                DELETE FROM nscc_comments WHERE date_ <= :tgl;
                DELETE FROM nscc_gl_trans WHERE tran_date <= :tgl AND store = :store;
                DELETE FROM nscc_kas WHERE tgl <= :tgl AND store = :store;
                DELETE FROM nscc_salestrans WHERE tgl <= :tgl AND store = :store;
                DELETE FROM nscc_stock_moves WHERE tran_date <= :tgl AND store = :store;
                DELETE FROM nscc_tender WHERE tgl <= :tgl AND store = :store;
                DELETE FROM nscc_transfer_item WHERE tgl <= :tgl AND store = :store;" )
				              ->execute( array( ':tgl' => $tgl, ':store' => $store ) );
				foreach ( $gltrans as $row ) {
					if ( $row['amount'] == 0 ) {
						continue;
					}
					$gl               = new GlTrans;
					$gl->type         = SALDO_AWAL;
					$gl->type_no      = $year;
					$gl->tran_date    = $tgl;
					$gl->memo_        = "Closing $year";
					$gl->amount       = $row['amount'];
					$gl->id_user      = Yii::app()->user->getId();
					$gl->account_code = $row['account_code'];
					$gl->store        = $store;
					$gl->tdate        = new CDbExpression( 'NOW()' );
					$gl->cf           = 0;
					$gl->up           = 1;
					$gl->visible      = 1;
					if ( ! $gl->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'General Journal' ) ) . CHtml::errorSummary( $gl ) );
					}
				}
				foreach ( $banktrans as $row ) {
					if ( $row['amount'] == 0 ) {
						continue;
					}
					$bt           = new BankTrans;
					$bt->ref      = "-";
					$bt->type_    = SALDO_AWAL;
					$bt->trans_no = $year;
					$bt->tgl      = $tgl;
					$bt->amount   = $row['amount'];
					$bt->id_user  = Yii::app()->user->getId();
					$bt->tdate    = new CDbExpression( 'NOW()' );
					$bt->bank_id  = $row['bank_id'];
					$bt->store    = $store;
					$bt->up       = 1;
					$bt->visible  = 1;
					if ( ! $bt->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'Bank Trans' ) ) . CHtml::errorSummary( $bt ) );
					}
				}
				foreach ( $stockmove as $row ) {
					if ( $row['qty'] == 0 ) {
						continue;
					}
					$barang = Barang::model()->findByPk( $row['barang_id'] );
//                    $barang = new Barang();
					$stm            = new StockMoves;
					$stm->reference = "-";
					$stm->type_no   = SALDO_AWAL;
					$stm->trans_no  = $year;
					$stm->tran_date = $tgl;
					$stm->price     = $barang->get_price( $store );
					$stm->qty       = $row['qty'];
					$stm->id_user   = Yii::app()->user->getId();
					$stm->tdate     = new CDbExpression( 'NOW()' );
					$stm->barang_id = $row['barang_id'];
					$stm->store     = $store;
					$stm->up        = 1;
					if ( ! $stm->save() ) {
						throw new Exception( t( 'save.model.fail', 'app',
								array( '{model}' => 'Bank Trans' ) ) . CHtml::errorSummary( $stm ) );
					}
				}
				/* @var $pelunasan_utang PelunasanUtang[] */
				$pelunasan_utang = PelunasanUtang::model()->findAll( 'tgl <= :tgl AND store = :store',
					array( ":tgl" => $tgl, ':store' => $store ) );
				foreach ( $pelunasan_utang as $pelunasan ) {
					foreach ( $pelunasan->pelunasanUtangDetils as $detail ) {
						/* @var $retur_purchase TransferItem[] */
						$retur_purchase = TransferItem::model()->findAll( 'doc_ref_other = :doc_ref',
							array( ':doc_ref' => $detail->transferItem->doc_ref ) );
						foreach ( $retur_purchase as $retur ) {
							TransferItemDetails::model()->deleteAll( 'transfer_item_id = :transfer_item_id',
								array( ':transfer_item_id' => $retur->transfer_item_id ) );
							$retur->delete();
						}
						TransferItemDetails::model()->deleteAll( 'transfer_item_id = :transfer_item_id',
							array( ':transfer_item_id' => $detail->transferItem->transfer_item_id ) );
						$detail->transferItem->delete();
						$detail->delete();
					}
					$pelunasan->delete();
				}
				$transaction->commit();
				$msg    = 'Closing successfuly...';
				$status = true;
			} catch ( Exception $ex ) {
				$transaction->rollback();
				$msg    = $ex->getMessage();
				$status = false;
			}
			echo CJSON::encode( array(
				'success' => $status,
				'msg'     => $msg
			) );
			Yii::app()->end();
		}
	}
	public function actionTestlr() {
		$income = ChartTypes::get_chart_types_by_class( CL_INCOME );
		$lr     = ChartMaster::get_laba_rugi_new( '2015-01-01', '2015-12-04', 'JOG01' );
		var_dump( $lr );
		$income_arr = array();
		foreach ( $income as $row ) {
			$income_arr[] = array(
				'account_code' => '',
				'account_name' => $row['name'],
				'total'        => ''
			);
			foreach ( $lr as $key => $item ) {
				if ( $item['id_1'] == $row['id'] ) {
					$income_arr[] = array(
						'account_code' => $item['account_code'],
						'account_name' => $item['account_name'],
						'total'        => $item['total']
					);
					unset( $lr[ $key ] );
				}
			}
			$income_arr = array_merge( $income_arr, ChartMaster::print_chart( $row['id'], $lr ) );
		}
		var_dump( $income_arr );
		$hpp     = ChartTypes::get_chart_types_by_class( CL_COGS );
		$hpp_arr = array();
		foreach ( $hpp as $row ) {
			$hpp_arr[] = array(
				'account_code' => '',
				'account_name' => $row['name'],
				'total'        => ''
			);
			foreach ( $lr as $key => $item ) {
				if ( $item['id_1'] == $row['id'] ) {
					$hpp_arr[] = array(
						'account_code' => $item['account_code'],
						'account_name' => $item['account_name'],
						'total'        => $item['total']
					);
					unset( $lr[ $key ] );
				}
			}
			$hpp_arr = array_merge( $hpp_arr, ChartMaster::print_chart( $row['id'], $lr ) );
		}
		var_dump( $hpp_arr );
		$cost     = ChartTypes::get_chart_types_by_class( CL_EXPENSE );
		$cost_arr = array();
		foreach ( $cost as $row ) {
			$cost_arr[] = array(
				'account_code' => '',
				'account_name' => $row['name'],
				'total'        => ''
			);
			foreach ( $lr as $key => $item ) {
				if ( $item['id_1'] == $row['id'] ) {
					$cost_arr[] = array(
						'account_code' => $item['account_code'],
						'account_name' => $item['account_name'],
						'total'        => $item['total']
					);
					unset( $lr[ $key ] );
				}
			}
			$cost_arr = array_merge( $cost_arr, ChartMaster::print_chart( $row['id'], $lr ) );
		}
		var_dump( $cost_arr );
		$other_income     = ChartTypes::get_chart_types_by_class( CL_OTHER_INCOME );
		$other_income_arr = array();
		foreach ( $other_income as $row ) {
			$other_income_arr[] = array(
				'account_code' => '',
				'account_name' => $row['name'],
				'total'        => ''
			);
			foreach ( $lr as $key => $item ) {
				if ( $item['id_1'] == $row['id'] ) {
					$other_income_arr[] = array(
						'account_code' => $item['account_code'],
						'account_name' => $item['account_name'],
						'total'        => $item['total']
					);
					unset( $lr[ $key ] );
				}
			}
			$other_income_arr = array_merge( $other_income_arr, ChartMaster::print_chart( $row['id'], $lr ) );
		}
		var_dump( $other_income_arr );
		echo count( $lr );
	}
	public function actionDecryptEmail( $subject ) {
		$email      = Email::model()->findByAttributes( array( 'subject' => $subject ) );
		$uncompress = @gzuncompress( $email->attachments );
		echo $uncompress;
	}
	public function actionHighChart() {
		$this->render( 'highchart' );
	}
	public function actionTes() {
		echo RestrictDate::getPersonalRestDate( '1' );
		echo RestrictDate::getPersonalRestDate( '2' );
		$a = RestrictDate::model()->findAll();
		foreach ( $a as $val ) {
			//echo $val->user_id;
			echo $val->userid->user_id;
			echo '<br/>';
		}
	}
	public function actionBonusTest() {
		$from  = $to = '2018-01-29';
		$store = 'BAC01';
		try {
			$cmd   = Yii::app()->db->createCommand(
				"SELECT 
                b.bonus_id
                , t1.persen_bonus
                , b.amount
                , TRUNCATE((t1.persen_bonus /100),4) decimal_persen
                , TRUNCATE((b.amount * (t1.persen_bonus /100)),2) nilai
            FROM nscc_bonus b
            JOIN
                (
                    select bonus_jual_id, persen_bonus, store 
                    from nscc_bonus_jual
                    where bonus_name_id in ('1','2','3','8','9') AND store = 'SDA01'
                ) t1 on t1.bonus_jual_id in (b.bonus_jual_id)
            where tgl >= '2018-01-21' AND tgl <= '2018-02-20' AND b.store = 'BAC01'
            GROUP BY bonus_id"
			);
			$model = $cmd->queryAll();
			echo CJSON::encode( array(
				'success' => $model,
				'count'   => count( $model )
			) );
		} catch ( Exception $ex ) {
			var_dump( $ex );
		}
	}
	public function actionBonusTestNew() {
//        $APT = 'f7b9c38c-92a2-11e7-baa7-507b9d297990';
//	$PC = 'd1ef09d5-92a2-11e7-baa7-507b9d297990';
//	$ADM = 'c7b04ec8-92a2-11e7-baa7-507b9d297990';
//	$STK = '00f20248-92a3-11e7-baa7-507b9d297990';
		$from  = $to = '2018-02-01';
		$store = 'BAC01';
		$total = 0;
		try {
			$cmd   = Yii::app()->db->createCommand(
				"   SELECT 
                b.bonus_id
                , bj.persen_bonus
                ,	b.amount
                , t2.jum count
                , TRUNCATE((bj.persen_bonus /100),2) decimal_persen
                , TRUNCATE((b.amount * (bj.persen_bonus /100) / t2.jum),2) nilai
                FROM nscc_bonus b
                JOIN (	
                        SELECT t1.employee_id,ee.store,t1.jum, t1.tanggal
                        from nscc_employees ee
                        JOIN (
                                SELECT assign_employee_id,a.employee_id, a.tipe_employee_id, t.jum, t.tgl tanggal
                                FROM nscc_assign_employees a
                                LEFT JOIN (
                                                SELECT employee_id,count(tipe_employee_id) jum,tipe_employee_id, tgl
                                                from nscc_assign_employees
                                                where tgl >= '2018-01-21' AND tgl <= '2018-02-20'
                                                GROUP BY tgl,tipe_employee_id
                                ) as t on t.tipe_employee_id = a.tipe_employee_id
                                where a.tgl >= '2018-01-21' AND a.tgl <= '2018-02-20'
                        ) as t1 on t1.employee_id = ee.employee_id
                        WHERE ee.store = 'BAC01'
                ) as t2 on t2.employee_id = b.employee_id
                LEFT JOIN nscc_bonus_jual bj on bj.bonus_jual_id = b.bonus_jual_id
                where b.employee_id = t2.employee_id AND b.tgl = t2.tanggal
                GROUP BY bonus_id"
			);
			$model = $cmd->queryAll();
			if ( $model != null ) {
				foreach ( $model as $k ) {
					Bonus::model()->updateByPk( $k['bonus_id'], [ 'amount_bonus' => $k['nilai'] ] );
					$total ++;
				}
				echo $total . PHP_EOL;
				//file_put_contents('bonus.loc', $dump);
			} else {
				throw new Exception( 'Bonus tidak ditemukan' );
			}
			echo CJSON::encode( array(
				'success' => $total,
				'bon'     => 0
			) );
		} catch ( Exception $ex ) {
			var_dump( $ex );
		}
	}
        public function actionTesGenerateGl() {
//                $pk = 'd00bcd74-4ce3-11e8-9aae-f9b59ffe1b89';
//                $tglin = '2018-05-02';
//                $tglout = '2018-05-07';
//		$a = Kas::model()->findAll("
//                        kas_id in (
//                            '19bc929e-4d27-11e8-9aae-f9b59ffe1b89'                     
//                        )
//                        ");
//                SALES
//		$a = Salestrans::model()->findAll("
//                        tgl >= '$tglin' AND tgl <='$tglout'
//                        ");
//                KAS
//		$a = Kas::model()->findAll("
//                        tgl >= '$tglin' AND tgl <='$tglout'
//                        ");
//                ULPT
//		$a = Ulpt::model()->findAll("
//                        tgl >= '$tglin' AND tgl <='$tglout'
//                        ");
//                RPJ
//		$a = Rpg::model()->findAll("
//                        tgl >= '$tglin' AND tgl <='$tglout'
//                        ");
//                $sales = new Rpg;
//                $sales = new Kas;
//                $sales = new Ulpt;
//                $sales = new Kas;
//                $aus->save();
//		foreach ( $a as $val ) {
//                    $xx = $sales->generateGltransRpj($val);
//		}
	}
        public function actionTesGenerateGlBankTrans() {
//                $pk = 'd00bcd74-4ce3-11e8-9aae-f9b59ffe1b89';
//                $tglin = '2018-05-02';
//                $tglout = '2018-05-07';
//		$a = BankTrans::model()->findAll("
//                        tgl >= '$tglin' AND tgl <='$tglout'
//                            AND type_ = 11
//                        ");
//                $sales = new BankTrans;
//		foreach ( $a as $val ) {
			//echo $val->user_id;
//			echo $val->total;
//			echo '<br/>';
//                    $xx = $sales->generateGltransBankTransfer($a);
//                    echo $xx;
//		}
	}
}
