<?php
class StockMovesController extends GxController
{
    public function actionCreate()
    {
        $model = new StockMoves;
        if (!Yii::app()->request->isAjaxRequest)
            return;
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['StockMoves'][$k] = $v;
            }
            $model->attributes = $_POST['StockMoves'];
            $msg = "Data gagal disimpan.";
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->stock_moves_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'StockMoves');
        if (isset($_POST) && !empty($_POST)) {
            foreach ($_POST as $k => $v) {
                if (is_angka($v)) $v = get_number($v);
                $_POST['StockMoves'][$k] = $v;
            }
            $msg = "Data gagal disimpan";
            $model->attributes = $_POST['StockMoves'];
            if ($model->save()) {
                $status = true;
                $msg = "Data berhasil di simpan dengan id " . $model->stock_moves_id;
            } else {
                $msg .= " " . implode(", ", $model->getErrors());
                $status = false;
            }
            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array(
                    'success' => $status,
                    'msg' => $msg
                ));
                Yii::app()->end();
            } else {
                $this->redirect(array('view', 'id' => $model->stock_moves_id));
            }
        }
    }
    public function actionCreateSelisihStok()
    {
        if (!Yii::app()->request->isAjaxRequest) return;
        if (isset($_POST) && !empty($_POST)) {
            $msg = 'Selisih persediaan berhasil disimpan.';
            $tgl = $_POST['tran_date'];
            $ket = $_POST['note'];
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $trans_no = U::get_max_type_no_stock(SELISIHSTOK);
                $trans_no++;
                $ref = new Reference();
                $docref = $ref->get_next_reference(SELISIHSTOK);
                foreach ($detils as $detil) {
                    U::add_stock_moves(SELISIHSTOK, $trans_no, $tgl, $detil['barang_id'], GUDANG_PST, $detil['qty'], $docref, 0);
                    /* @var $barang Barang*/
                    $barang = $this->loadModel($detil['barang_id'], "Barang");
                    $total = $barang->price_buy * $detil['qty'];
                    Yii::import('application.components.Gl');
                    $gl = new Gl();
                    $gl->add_gl(SELISIHSTOK, $trans_no, $tgl, $docref, PERSEDIANBARANGDAGANG, '', '', $total);
                    $gl->add_gl(SELISIHSTOK, $trans_no, $tgl, $docref, SELISIHPERSEDIAAN, '', '', -$total);
                    $gl->validate();
                }
                U::add_comments(SELISIHSTOK, $trans_no, $tgl, $ket);
                $ref->save(SELISIHSTOK, $trans_no, $docref);
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function  actionSelisihStok()
    {
        $selisihStok = U::get_selisih_stok($_POST['tgl']);
        $total = count($selisihStok);
        $jsonresult = '{"total":"' . $total . '","results":' . json_encode($selisihStok) . '}';
        Yii::app()->end($jsonresult);
    }
    public function  actionSelisihStokDetil()
    {
        $trans_no = $_POST['trans_no'];
        $criteria = new CDbCriteria();
        $param = array();
        $criteria->params = $param;
        $criteria->addCondition("type = :type");
        $param[':type'] = SELISIHSTOK;
        $criteria->addCondition("trans_no = :trans_no");
        $param[':trans_no'] = $trans_no;
        $criteria->params = $param;
        $model = StockMoves::model()->findAll($criteria);
        $total = StockMoves::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = StockMoves::model()->findAll($criteria);
        $total = StockMoves::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    
    public function actionStockOpname()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $sheets = CJSON::decode($_POST['data']);
            $trans_date = $_POST['tgl'];
            $store = $_POST['store'];
            
            foreach ($sheets as $sheet) {
                /*
                 * Header kolom kode_barang dan qty
                 * ?????? Hanya sheet pertama yang tereksekusi
                 */
                U::stockOpname_input($sheet, $trans_date, $store);
            }
        }
    }
    
    public function actionHitungStock(){
        $barang_id = $_POST['barang_id'];
        $sm = StockMoves::get_saldo_item($barang_id, STOREID);
        $gr = StockMoves::get_grup($barang_id);
        $status = true;
        $msg = '';
//        if(!$sm){
//            $status = false;
//        }
        if($sm <= 0 && $gr == KATEGORI_PRODUK){
            $msg = 'Stock 0.';
        }
        echo CJSON::encode(array(
                'success' => $status,
//                'id' => $docref,
                'sm' => $sm,
                'msg' => $msg));
//            Yii::app()->end();
    }
}