<?php
class BarangController extends GxController
{
    public function actionCreate()
    {
        $model = new Barang;
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $brg = Barang::model()->find("kode_barang = :kode_barang", array(':kode_barang'=>$_POST['kode_barang']));
                if($brg){
                    $msg = 'Code Product/Treatment <span style="color: red; font-weight: bold;">'.$brg->kode_barang.'</span> already exists.';
                    $status = false;
                }else {
                    foreach ($_POST as $k => $v) {
                        if (is_angka($v)) {
                            $v = get_number($v);
                        }
                        $_POST['Barang'][$k] = $v;
                    }
                    $model->attributes = $_POST['Barang'];
                    $msg = t('save.fail', 'app');

                    if ($model->save()) {
                        $status = true;
                        $msg = t('save.success.id', 'app', array('{id}' => $model->barang_id));
                        if (PUSH_ALL_CABANG) {
                            U::runCommand('soap','barangall', '--id=' . $model->barang_id, 'barangall_' . $model->barang_id . '.log');
                        }
                    } else {
                        $msg .= " " . CHtml::errorSummary($model);
                        $status = false;
                    }
                }
            if(METHODE_BONUS == 1){
                //JADIKAN 0 DULU PERSENNYA
                $template = BonusTemplate::model()->findAllByAttributes(array('grup_id' => $model->grup_id));
                if($template){
                    foreach($template as $t){
                        $bonusjual = new BonusJual;
                        $bonusjual->barang_id = $model->barang_id;
                        $bonusjual->bonus_name_id = $t->bonus_name_id;
                        $bonusjual->persen_bonus = 0;
                        $bonusjual->store = STOREID;
                        $bonusjual->save();
                    }
                }
            }
            $transaction->commit();
            $msg = t('save.success', 'app');
            $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionUpdate($id)
    {
        app()->db->autoCommit = false;
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $model = $this->loadModel($id, 'Barang');
            if (isset($_POST) && !empty($_POST)) {
                $brg = Barang::model()->find("kode_barang = :kode_barang AND barang_id != :barang_id", array(':kode_barang'=>$_POST['kode_barang'], ':barang_id'=>$id));
                if($brg){
                    $msg = 'Code Product/Treatment <span style="color: red; font-weight: bold;">'.$brg->kode_barang.'</span> already exists.';
                    $status = false;
                }else {
                    foreach ($_POST as $k => $v) {
                        if (is_angka($v)) {
                            $v = get_number($v);
                        }
                        $_POST['Barang'][$k] = $v;
                    }
                    $msg = t('save.fail', 'app');
                    $model->attributes = $_POST['Barang'];
                    if ($model->save()) {
                        $status = true;
                        $msg = t('save.success.id', 'app', array('{id}' => $model->barang_id));
                        if (PUSH_ALL_CABANG) {
                            U::runCommand('soap','barangall', '--id=' . $model->barang_id, 'barangall_' . $model->barang_id . '.log');
                        }
                    } else {
                        $msg .= " " . CHtml::errorSummary($model);
                        $status = false;
                    }
                    if(METHODE_BONUS == 1){
                        $template = BonusTemplate::model()->findAllByAttributes(array('grup_id' => $model->grup_id));
                        if($template){
                            $bj = BonusJual::model()->findByAttributes(array('barang_id' => $model->barang_id));
                            if(!$bj){
                                foreach($template as $t){
                                    $bonusjual = new BonusJual;
                                    $bonusjual->barang_id = $model->barang_id;
                                    $bonusjual->bonus_name_id = $t->bonus_name_id;
                                    $bonusjual->persen_bonus = $t->persen_bonus;
                                    $bonusjual->store = STOREID;
                                    $bonusjual->save();
                                }
                            } else {
                                foreach($template as $t){
                                    $bonusjual = BonusJual::model()->findByAttributes(array('barang_id' => $model->barang_id, 'bonus_name_id' => $t->bonus_name_id));
                                    if ($bonusjual){
                                        $bonusjual->barang_id = $model->barang_id;
                                        $bonusjual->bonus_name_id = $t->bonus_name_id;
                                        $bonusjual->persen_bonus = $t->persen_bonus;
                                        $bonusjual->store = STOREID;
                                        $bonusjual->save();
                                    } else {
                                        $bonusjual = new BonusJual;
                                        $bonusjual->barang_id = $model->barang_id;
                                        $bonusjual->bonus_name_id = $t->bonus_name_id;
                                        $bonusjual->persen_bonus = $t->persen_bonus;
                                        $bonusjual->store = STOREID;
                                        $bonusjual->save();
                                    }              
                                }
                            }                            
                        } else {
                            $bj = BonusJual::model()->findAllByAttributes(array('barang_id' => $model->barang_id));                            
                            if($bj){
                                foreach ($bj as $k){                                    
                                    $k->barang_id = $model->barang_id;
                                    $k->bonus_name_id = $k->bonus_name_id;
                                    $k->persen_bonus = 0;
                                    $k->store = STOREID;
                                    $k->save();
                                }
                            }
                        }
                    }    
                }
            }
        $transaction->commit();
        $msg = t('save.success', 'app');
        $status = true;    
        } catch (Exception $ex) {
            $transaction->rollback();
            $status = false;
            $msg = $ex->getMessage();
        }
        app()->db->autoCommit = true;
        echo CJSON::encode(array(
            'success' => $status,
            'msg' => $msg
        ));
        Yii::app()->end();
        
    }
    public function actionGetTax()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $model = $this->loadModel($_POST['barang_id'], 'Barang');
            echo CJSON::encode(array(
                'success' => true,
                'msg' => $model->get_tax(STOREID)
            ));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) {
            $limit = $_POST['limit'];
        } else {
            $limit = 20;
        }
        if (isset($_POST['start'])) {
            $start = $_POST['start'];
        } else {
            $start = 0;
        }
        $criteria = new CDbCriteria();
        $criteria->alias = "nb";
        $param = array();
        if (isset($_POST['kode_barang'])) {
            $criteria->addCondition("kode_barang like :kode_barang");
            $param[':kode_barang'] = "%" . $_POST['kode_barang'] . "%";
        }
        if (isset($_POST['nama_barang'])) {
            $criteria->addCondition("nama_barang like :nama_barang");
            $param[':nama_barang'] = "%" . $_POST['nama_barang'] . "%";
        }
        if (isset($_POST['ket'])) {
            $criteria->addCondition("ket like :ket");
            $param[':ket'] = "%" . $_POST['ket'] . "%";
        }
//        if (isset($_POST['price'])) {
//            $criteria->join = "LEFT JOIN {{jual}} nj ON (nb.barang_id = nj.barang_id)";
//            $criteria->addCondition("nj.price <= :price");
//            $param[':price'] = $_POST['price'];
//        }
        if (isset ($_POST['mode']) && $_POST['mode'] == 'jasa') {
            $criteria->join = "INNER JOIN {{grup}} ng ON ( nb.grup_id = ng.grup_id  )";
            $criteria->addCondition("ng.kategori_id <> :kategori_id");
            $param[':kategori_id'] = KATEGORI_PRODUK;
        }
        if (isset ($_POST['mode']) && $_POST['mode'] == 'nojasamedis') {
            $criteria->addCondition("grup_id <> :grup_id");
            $param[':grup_id'] = TREATMENT_DOCTOR;
        }
        if (isset ($_POST['tipe_barang_id'])) {
            $criteria->addCondition("nb.tipe_barang_id = " . $_POST['tipe_barang_id']);
            //$param[':tipe_barang_id'] = $_POST['tipe_barang_id'];
        }
        if (isset ($_POST['order'])) {
            $criteria->order = "kode_barang " . $_POST['order']."";
            //$param[':tipe_barang_id'] = $_POST['tipe_barang_id'];
        }
        if (isset ($_POST['mode']) && $_POST['mode'] == 'nonjasa') {
            $criteria->join = "INNER JOIN {{grup}} ng ON ( nb.grup_id = ng.grup_id  ) INNER JOIN {{kategori}} nk ON nk.kategori_id = ng.kategori_id";
//            $criteria->addInCondition("ng.kategori_id",array('2','4'),'OR');
            $criteria->addCondition("nk.have_stock = 1");
//            $param[':product'] = KATEGORI_PRODUK;
//            $param[':other'] = KATEGORI_OTHER_INCOME;
        }
        if (isset ($_POST['mode']) && $_POST['mode'] == 'nonjasatransfer') {
            $user = Users::model()->findByPk(Yii::app()->user->getId());
            $all_tipe_barang = $user->is_available_role(263) ? TRUE : FALSE;
            if ($all_tipe_barang == FALSE){
                $criteria->addCondition("nb.tipe_barang_id = 2");
            }
            $criteria->join = "INNER JOIN {{grup}} ng ON ( nb.grup_id = ng.grup_id  ) INNER JOIN {{kategori}} nk ON nk.kategori_id = ng.kategori_id";
//            $criteria->addInCondition("ng.kategori_id",array('2','4'),'OR');
            $criteria->addCondition("nk.have_stock = 1");
//            $param[':product'] = KATEGORI_PRODUK;
//            $param[':other'] = KATEGORI_OTHER_INCOME;
        }
        if (isset ($_POST['mode']) && $_POST['mode'] == 'purchasable') {
            /*
             * barang yang dapat dibeli (dibuat PO)
             * note:
             *      nk.kategori_id = 6 -> Service/Layanan
             */
            $criteria->join = "INNER JOIN {{grup}} ng ON ( nb.grup_id = ng.grup_id  ) INNER JOIN {{kategori}} nk ON nk.kategori_id = ng.kategori_id";
            $criteria->addCondition("nk.have_stock = 1 OR nk.kategori_id = 6");
        }
        $criteria->addCondition('store = :store OR store is null');
        $param[':store'] = STOREID;
        $criteria->params = $param;
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') ||
            (isset($_POST['limit']) && isset($_POST['start']))
        ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        $model = BarangJual::model()->findAll($criteria);
        $total = BarangJual::model()->count($criteria);
        $this->renderJson($model, $total);
    }
    public function actionImport()
    {
        if (!Yii::app()->request->isAjaxRequest) {
            return;
        }
        if (isset($_POST) && !empty($_POST)) {
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $arr_key = array_keys($detils);
                $duplicate_no_cust = array();
                foreach ($detils[$arr_key[0]] as $row) {
                    $arr_key = array_keys($row);
                    $arr_idx = array();
                    foreach ($arr_key as $key) {
                        if (strpos($key, 'KODEBRG') !== false) {
                            $arr_idx[0] = $key;
                        }
                        if (strpos($key, 'JNSBRG') !== false) {
                            $arr_idx[1] = $key;
                        }
                        if (strpos($key, 'NAMABRG') !== false) {
                            $arr_idx[2] = $key;
                        }
                        if (strpos($key, 'SATUAN1') !== false) {
                            $arr_idx[3] = $key;
                        }
                        if (strpos($key, 'HJUAL') !== false) {
                            $arr_idx[4] = $key;
                        }
                    }
                    $cust = Barang::model()->find('kode_barang = :kode_barang',
                        array(':kode_barang' => $row[$arr_key[0]]));
                    if ($cust != null) {
                        $duplicate_no_cust[] = array(
                            $row[$arr_idx[0]],
                            $row[$arr_idx[1]],
                            $row[$arr_idx[2]],
                            $row[$arr_idx[3]],
                            $row[$arr_idx[4]]
                        );
                        continue;
                    }
                    $model = new Barang;
                    $model->kode_barang = $row[$arr_idx[0]];
                    $model->grup_id = $row[$arr_idx[1]];
                    $model->nama_barang = $row[$arr_idx[2]];
                    $model->sat = strtoupper($row[$arr_idx[3]]);
                    $model->price = $row[$arr_idx[4]];
                    if (!$model->save()) {
                        throw new Exception(t('save.model.fail', 'app',
                                array('{model}' => 'Barang')) . CHtml::errorSummary($model));
                    }
                }
                $msg = '<table cellspacing="5" style="border:1px solid #BBBBBB;border-collapse:collapse;padding:5px; font-size: 10pt;">
                        <thead>
                            <tr>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;" >Item Code</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Item Type</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Item Name</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Item Unit</th>
                            <th style="border:1px solid #BBBBBB;padding:5px;white-space: nowrap;">Item Price</th>
                            </tr>
                        </thead><tbody >';
                foreach ($duplicate_no_cust as $r) {
                    $msg .= "<tr>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[0]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[1]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[2]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[3]</td>
                            <td style='border:1px solid #BBBBBB;padding:5px;'>$r[4]</td>
                            </tr>";
                }
                $msg .= "</tbody></table>";
                $transaction->commit();
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg
            ));
            Yii::app()->end();
        }
    }
    public function actionFind()
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('kode_barang = :barang');
        $param[':barang'] = $_POST['barang'];
        $criteria->params = $param;
        $model = BarangJual::model()->find($criteria);
        echo CJSON::encode($model);
    }
}