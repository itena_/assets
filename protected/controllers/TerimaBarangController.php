<?php
class TerimaBarangController extends GxController
{
    public function actionCreate()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            $detils = CJSON::decode($_POST['detil']);
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $is_new = $_POST['mode'] == 0;
                $model = $is_new ? new TerimaBarang : $this->loadModel($_POST['id'], 'TerimaBarang');
                if (!$is_new && ($model == null)) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'TerimaBarang')) . "Fatal error, record not found.");
                }
                
                $modelPO = $this->loadModel($_POST['po_id'], 'Po');
                if (!$modelPO) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'Po')) . "Fatal error, record not found.");
                }
                /*
                 * type PO
                 * 1 : normal PO
                 * -1 : PO minus/retur
                 */
                $type_PO = $modelPO->type_;
                
                if ($is_new) {
                    $ref = new Reference();
                    $docref = $ref->get_next_reference($type_PO == 1? TERIMA_BARANG : RETUR_BARANG);
                } else {
                    $statusPO = $model->po->received;
                    if ($statusPO == PO_CLOSED || $statusPO == PO_RECEIVED) {
                        throw new Exception("Edit Penerimaan Barang tidak dapat dilakukan <br />karena 
                            barang untuk Purchase Order ini sudah diterima semua.");
                    }
                    if ($model->status == TB_INVOICED) {
                        throw new Exception("Edit Penerimaan Barang tidak bisa dilakukan <br />karena 
                            sudah dibuat invoice.");
                    }
                    if ($model->lock_edit == 1) {
                        throw new Exception("Edit Penerimaan Barang tidak dapat dilakukan.");
                    }
                    $docref = $model->doc_ref;
                    TerimaBarangDetails::model()->updateAll(array('visible' => 0), 'terima_barang_id = :terima_barang_id',
                        array(':terima_barang_id' => $model->terima_barang_id));
                    //$type = SUPPIN;
                    //$type_no = $model->terima_barang_id;
                    //$this->delete_stock_moves($type, $type_no);
                    U::delete_stock_moves_all(SUPPIN, $model->terima_barang_id);
                }
                foreach ($_POST as $k => $v) {
                    if ($k == 'detil') continue;
                    if (is_angka($v)) $v = get_number($v);
                    $_POST['TerimaBarang'][$k] = $v;
                }
                $_POST['TerimaBarang']['doc_ref'] = $docref;
                $model->attributes = $_POST['TerimaBarang'];
                $model->lock_edit = ($model->status == TB_DRAFT)?0:1;
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Penerimaan Barang')) . CHtml::errorSummary($model));
                
                
                foreach ($detils as $detil) {
                    $barang = Barang::model()->findByPk($detil['barang_id']);
                    
                    $poSisaTerima = PoSisaTerima::model()->findByAttributes(array(
                                    'barang_id' => $detil['barang_id'],
                                    'po_id' => $modelPO->po_id,
                                    'item_id' => $detil['item_id']
                                ));
                    if(!$poSisaTerima || ($poSisaTerima->qty-$detil['qty'])<0){
                        /*
                         * jika $poSisaTerima = null -> qty PO telah terpenuhi
                         * Jika qty terima lebih besar dari $poSisaTerima->qty -> penerimaan tidak boleh melebihi qty PO.
                         */
                        throw new Exception('Quantity terima tidak boleh melebihi quantity PO.'.
                                '<br>Kode : <b>'.$barang->kode_barang.'</b>'.
                                '<br>Nama : <b>'.$barang->nama_barang.'</b>'.
                                '<br>Quantity yang belum diterima : <b>'.(!$poSisaTerima?0:$poSisaTerima->qty).'</b>');
                    }

                    $stock_moves_id = $this->generate_uuid();
                    $stock_before = 0;
                    $price_before = 0;
                    if ($barang->grup->kategori->is_have_stock()) {
                        $stock_before = StockMoves::get_saldo_item($detil['barang_id'], $model->store);
                        $price_before = $barang->get_cost($model->store);
                    
//                        $stock_moves_id = U::add_stock_moves(
//                            $type_PO==1?SUPPIN:SUPPOUT,
//                            $model->terima_barang_id, $model->tgl, $detil['barang_id'],
//                            $type_PO==1?get_number($detil['qty']):-get_number($detil['qty']),
//                            $model->doc_ref,
//                            0,
//                            $model->store);
                        U::add_stock_moves_all(
                            $stock_moves_id,
                            $type_PO==1?SUPPIN:SUPPOUT,
                            $model->terima_barang_id,
                            $model->tgl,
                            $detil['barang_id'],
                            $type_PO==1?get_number($detil['qty']):-get_number($detil['qty']),
                            $model->doc_ref,
                            0,
                            $model->store
                        );
                    }else{
                        $stock_moves_id = 'no stock move';
                    }

                    $item_details = new TerimaBarangDetails;
                    $_POST['TerimaBarangDetails']['barang_id'] = $detil['barang_id'];
                    $_POST['TerimaBarangDetails']['seq'] = $detil['seq'];
                    $_POST['TerimaBarangDetails']['description'] = $detil['description'];
                    $_POST['TerimaBarangDetails']['qty'] = get_number($detil['qty']);
                    
                    $_POST['TerimaBarangDetails']['stock_moves_id'] = $stock_moves_id;
                    $_POST['TerimaBarangDetails']['terima_barang_id'] = $model->terima_barang_id;
                    $_POST['TerimaBarangDetails']['stock_before'] = $stock_before;
                    $_POST['TerimaBarangDetails']['price_before'] = $price_before;

                    $_POST['TerimaBarangDetails']['item_id'] = $detil['item_id'];
                    $item_details->attributes = $_POST['TerimaBarangDetails'];
                    if (!$item_details->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Receive barang detail')) .
                            CHtml::errorSummary($item_details));
                    
                    /*
                    $barang = Barang::model()->findByPk($item_details->barang_id);
                    if ($barang->grup->kategori->is_have_stock()) {
                        U::add_stock_moves(DROPPING, $model->terima_barang_id, $model->tgl,
                            $item_details->barang_id, $item_details->qty, $model->doc_ref,
                            $barang->get_cost($model->store), $model->store);
                    }
                    */
                }
                if ($is_new) {
                    $ref->save($type_PO == 1? TERIMA_BARANG : RETUR_BARANG, $model->terima_barang_id, $docref);
                }
                
                /*
                 * Update status PO
                 */
                $model_number = PoSisaTerima::model()->countByAttributes(array('po_id' => $model->po_id));
                $po = Po::model()->findByPk($model->po_id);
                if ($po == null) {
                    throw new Exception("Purchase Order tidak ditemukan!");
                } else {
                    if ($model_number <= 0) {
                        $po->received = STATUS_CLOSE;
                        $po->status = PO_RECEIVED;
                    }else{
                        $po->received = STATUS_OPEN;
                        $po->status = PO_PARTIALLY_RECEIVED;
                    }
                    if (!$po->save())
                        throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Purchase Order')) .
                            CHtml::errorSummary($po));
                }
                $transaction->commit();
                $msg = t('save.success', 'app');
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
//                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionOpen()
    {
        if (!Yii::app()->request->isAjaxRequest)
            $this->redirect(url('/'));
        if (isset($_POST) && !empty($_POST)) {
            $status = false;
            $msg = "Stored data failed.";
            app()->db->autoCommit = false;
            $transaction = Yii::app()->db->beginTransaction();
            try {
                $model = $this->loadModel($_POST['id'], 'TerimaBarang');
                if ($model == null) {
                    throw new Exception(t('save.model.fail', 'app',
                            array('{model}' => 'TerimaBarang')) . "Fatal error, record not found.");
                }
                if ($model->status != TB_DRAFT) {
                    throw new Exception("Purchase Request telah diposting sebelumnya.");
                }
                if (!($model->store == STOREID || HEADOFFICE)) {
                    throw new Exception("Anda tidak diperbolehkan untuk memposting Purchase Request ini.");
                }
                
                $model->status = TB_OPEN;
                $model->lock_edit = 1;
                if (!$model->save())
                    throw new Exception(t('save.model.fail', 'app', array('{model}' => 'Penerimaan Barang')) . CHtml::errorSummary($model));
                
                $transaction->commit();
                $msg = "Penerimaan Barang No. <b>".$model->doc_ref."</b> berhasil diposting.";
                $status = true;
            } catch (Exception $ex) {
                $transaction->rollback();
                $status = false;
                $msg = $ex->getMessage();
            }
            app()->db->autoCommit = true;
            echo CJSON::encode(array(
                'success' => $status,
//                'id' => $docref,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionUnlockEdit($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $model = $this->loadModel($id, 'TerimaBarang');
            $msg = "Unlock Penerimaan Barang No. <b>" . $model->doc_ref . "</b> tidak berhasil.<br>";
            $status = false;
            try{
                if ($model->status != TB_OPEN) {
                    throw new Exception("Tidak dapat unlock Penerimaan Barang No. <b>" . $model->doc_ref . "</b>.");
                }

                $model->lock_edit = 0;
                if ($model->save()) {
                    $status = true;
                    $msg = "Penerimaan Barang No. <b>" . $model->doc_ref . "</b> telah di-unlock. ";
                } else {
                    throw new Exception(
                            "<br>" . implode(", ", $model->getErrors())
                        );
                }
            } catch (Exception $ex){
                $status = false;
                $msg = $ex->getMessage();
            }
            echo CJSON::encode(array(
                'success' => $status,
                'msg' => $msg));
            Yii::app()->end();
        }
    }
    public function actionIndex()
    {
        if (isset($_POST['limit'])) { $limit = $_POST['limit']; } else { $limit = 20; }
        if (isset($_POST['start'])) { $start = $_POST['start']; } else { $start = 0; }
        
        $criteria = new CDbCriteria();
        $criteria->params = array();
        
        if ((isset ($_POST['mode']) && $_POST['mode'] == 'grid') || (isset($_POST['limit']) && isset($_POST['start'])) ) {
            $criteria->limit = $limit;
            $criteria->offset = $start;
        }
        
        if (isset($_POST['tgl'])) {
            $criteria->addCondition('tgl = :tgl');
            $criteria->params[':tgl'] = $_POST['tgl'];
        }
        if (isset($_POST['doc_ref'])) {
            $criteria->addCondition('doc_ref LIKE :doc_ref');
            $criteria->params[':doc_ref'] = '%' . $_POST['doc_ref'] . '%';
        }
        if (isset($_POST['no_sj'])) {
            $criteria->addCondition('no_sj LIKE :no_sj');
            $criteria->params[':no_sj'] = '%' . $_POST['no_sj'] . '%';
        }
        if (isset($_POST['note'])) {
            $criteria->addCondition('note LIKE :note');
            $criteria->params[':note'] = '%' . $_POST['note'] . '%';
        }
        if (isset($_POST['doc_ref_po'])) {
            $criteria->addCondition('doc_ref_po LIKE :doc_ref_po');
            $criteria->params[':doc_ref_po'] = '%' . $_POST['doc_ref_po'] . '%';
        }
        if (isset($_POST['store'])) {
            $criteria->addCondition('store LIKE :store');
            $criteria->params[':store'] = '%' . $_POST['store'] . '%';
        }
        
        if (isset($_POST['lock_edit']) && $_POST['lock_edit'] !== 'all') {
            $criteria->addCondition('lock_edit = :lock_edit');
            $criteria->params[':lock_edit'] = $_POST['lock_edit'];
        }
        if (isset($_POST['status']) && $_POST['status'] !== 'all') {
            /*filter grid berdasar status*/
            $criteria->addCondition('status = :status');
            $criteria->params[':status'] = $_POST['status'];
        }
        if (isset($_POST['viewForPurchasing']) && (int)$_POST['viewForPurchasing'] == 1) {
            $criteria->addCondition('status != :status');
            $criteria->params[':status'] = TB_DRAFT;
        }
        if (isset($_POST['dataInvoice']) && $_POST['dataInvoice'] == '1') {
            /*menampilkan data invoice*/
            $criteria->addCondition('status >= :status');
            $criteria->params[':status'] = TB_INVOICED;
        }
        
        $criteria->order = 'tgl DESC, tdate DESC';
        
        $model = TerimaBarangView::model()->findAll($criteria);
        $total = TerimaBarangView::model()->count($criteria);
        $this->renderJson($model, $total);
    }
}