<?php
require_once('store.php');
/**
 * This is the shortcut to DIRECTORY_SEPARATOR
 */
defined('DS') or define('DS', DIRECTORY_SEPARATOR);
define('SALES_LOCK', 'protected' . DS . 'runtime' . DS . 'sales.loc');
define('KONSUL_LOCK', 'protected' . DS . 'runtime' . DS . 'konsul.loc');
define('SYNC_LOCK', 'protected' . DS . 'runtime' . DS . 'sync.loc');
defined('JSON_PRETTY_PRINT') or define('JSON_PRETTY_PRINT', 128);
define('SALDO_AWAL', 0);
define('PENJUALAN', 1);
define('CASHOUT', 2);
define('CASHIN', 3);
define('RETURJUAL', 4);
define('SUPPIN', 5);
define('SUPPOUT', 6);
define('TENDER', 7);
define('AUDIT', 8);
define('JURNAL_UMUM', 10);
define('BANKTRANSFER', 11);
define('BANKTRANSFERCHARGE', 12);
define('LABARUGI', 13);
define('PELUNASANUTANG', 14);
define('PRODUKSI', 15);
define('CLINICAL_IN', 16);
define('CLINICAL_OUT', 17);
define('SOUVENIR_IN', 18);
define('SOUVENIR_OUT', 19);
define('POINT_IN', 20);
define('POINT_OUT', 21);
define('MGM', 22);
define('MSD', 23);
define('RETURN_PRODUKSI', 24);
define('ULPT', 25);
define('RPG', 26);
define('PO_IN', 27);
define('TERIMA_BARANG', 28);
define('ORDER_DROPPING', 29);
define('DROPPING', 30);
define('RECEIVE_DROPPING', 31);
define('STORE_TRANSIT', 'TRANS');
define('TIPE_ULPT', 'U');
define('TIPE_RPG', 'R');
define('PR', 32);
define('PO_OUT', 33);
define('RETUR_BARANG', 34);
//define('CHARLENGTHRECEIPT', 64);
define('CHARLENGTHRECEIPT', 48);
define('SUPPIN_ASSETS', 60);
define('PERLENGKAPAN_IN', 61);
define('PERLENGKAPAN_OUT', 62);
define('ITEM_IN', 63);
define('ITEM_OUT', 64);
define('KONSUL', 65);
define('RESEP', 66);
define('IMPORTLAHA', 67);
define('DROPPING_RECALL', 68);
define('STATUS_OPEN', 0);
define('STATUS_CLOSE', 1);
define('RUSERS', 100);
define('CUSTOMER', 101);
define('RBEAUTY', 102);
define('RGRUP', 103);
define('RDOKTER', 104);
define('RBARANG', 105);
define('RBANK', 106);
define('RKAS', 107);
define('RSALESTRANS', 108);
define('RTENDER', 109);
define('RTRANSFERITEM', 110);
define('RSALESTRANSDETAILS', 111);
define('RBELI', 112);
define('RJURNAL_UMUM', 113);
define('RPRINTZ', 114);
define('RBANKTRANSFER', 115);
define('RPELUNASANUTANG', 116);
define('INVOICE_JOURNAL', 201);
define('PAYMENT_JOURNAL', 202);
define('PIUTANG_VOUCHER', 203);
define('PELUNASANPIUTANG', 204);

define('ASSETBELI', 205);
define('ASSETJUAL', 206);
define('ASSETSEWA', 207);

global $systypes_array;
$systypes_array = array(
    SALDO_AWAL => "Saldo Awal",
    PENJUALAN => "Sales",
    CASHOUT => "Cash Out",
    CASHIN => "Cash In",
    RETURJUAL => "Return Sales",
    SUPPIN => "Receive Supplier Item",
    SUPPOUT => "Return Supplier Item",
    TENDER => "Tender Declaration",
    AUDIT => "Audit",
    CUSTOMER => "Customer",
    JURNAL_UMUM => "General Journal",
    BANKTRANSFER => "Cash/Bank Transfer",
);
//	GL account classes
//
define('CL_CURRENT_ASSETS', 1);
define('CL_CURRENT_LIABILITIES', 2);
define('CL_EQUITY', 3);
define('CL_INCOME', 4);
define('CL_COGS', 5);
define('CL_EXPENSE', 6);
define('CL_OTHER_INCOME', 7);
define('CL_FIXED_ASSETS', 8);
define('CL_LONGTERM_LIABILITIES', 9);
$class_types = array(
    CL_CURRENT_ASSETS => "Current Assets",
    CL_FIXED_ASSETS => "Fixed Assets",
    CL_CURRENT_LIABILITIES => "Current Liabilities",
    CL_LONGTERM_LIABILITIES => "Longterm Liabilities",
    CL_EQUITY => "Equity",
    CL_INCOME => "Income",
    CL_COGS => "Cost of Goods Sold",
    CL_EXPENSE => "Cost",
    CL_OTHER_INCOME => "Other Income and Expenses",
);
#### Antrian
define('AN_REGISTER', 'Pasien Registratsi');
define('AN_NOBASE', 'Isi Nomer Pasien');
define('AN_PANGGIL', 'Panggilan');
define('AN_PENDING', 'Pending');
define('AN_UNPENDING', 'Kembali ke Antrian');
define('AN_SELESAI', 'Selesai');
define('AN_PERAWATAN', 'Perawatan');
define('AN_BATAL', 'Batal');
define('AN_BACK', 'Kembali ke');
global $step_antrian;
$step_antrian = [
    'daftar' => ['label' => 'Pendaftaran', 'before' => '', 'after' => 'counter', 'kode' => 'CS'],
    'counter' => ['label' => 'Counter', 'before' => 'daftar', 'after' => 'medis', 'kode' => 'CS'],
    'medis' => ['label' => 'Konsultasi', 'before' => 'counter', 'after' => 'kasir', 'kode' => 'MEDIS'],
    'kasir' => ['label' => 'Kasir', 'before' => 'medis', 'after' => 'perawatan', 'kode' => 'CS'],
    'perawatan' => ['label' => 'Perawatan', 'before' => 'kasir', 'after' => 'selesai', 'kode' => 'PERAWATAN'],
    'selesai' => ['label' => 'Selesai', 'before' => 'kasir', 'after' => '', 'kode' => 'CS']
];
//STATUS PURCHASE Request
define('PR_NEED_SHIPMENT', -2);
define('PR_DRAFT', -1);
define('PR_OPEN', 0);
define('PR_PROCESS', 1);
define('PR_CLOSED', 2);
//STATUS PURCHASE ORDER
define('PO_OPEN', 0);
define('PO_PARTIALLY_RECEIVED', 1);
define('PO_RECEIVED', 2);
define('PO_CLOSED', 3);
//STATUS TERIMA BARANG
define('TB_DRAFT', -1);
define('TB_OPEN', 0);
define('TB_INVOICED', 1);
define('TB_CLOSED', 2);

//STATUS DROPPING
define('DR_PENDING', 0);
define('DR_SEND', 1);
define('DR_APPROVE', 2);
define('DR_PROCESS', 3);
define('DR_RECEIVE', 4);
define('DR_CLOSE', 5);

//STATUS SYNC
define('SYNC_FL', 4);
define('SYNC_MK', 3);
define('SYNC_PR', 2);
define('SYNC_OK', 1);
define('SYNC_NO', 0);


//STATUS SCAN
define('SCAN_FL', 4);
define('SCAN_MK', 3);
define('SCAN_PR', 2);
define('SCAN_OK', 1);
define('SCAN_NO', 0);

/*
 * GRUP BARANG
 */
//define('TREATMENT_DOCTOR', 3);
define('TREATMENT_SKIN_CARE', '6');

/*
 * TIPE BARANG
 * table 'nscc_tipe_barang'
 */
define('TIPE_FINISH_GOODS', 0);
define('TIPE_RAW_MATERIAL', 1);
define('TIPE_PERLENGKAPAN', 2);
/**
 * This is the shortcut to Yii::app()
 */
function app()
{
    return Yii::app();
}

/**
 * @param $number
 * @return int
 */
function is_angka($number)
{
    if ($number == null) {
        return false;
    }
    if (is_array($number)) {
        return false;
    }
    return preg_match("/^-?([\$]?)([0-9,\s]*\.?[0-9]{0,2})$/", $number);
}

/**
 * This is the shortcut to Yii::app()->clientScript
 */
function cs()
{
    // You could also call the client script instance via Yii::app()->clientScript
    // But this is faster
    return Yii::app()->getClientScript();
}

/**
 * This is the shortcut to Yii::app()->user.
 */
function user()
{
    return Yii::app()->getUser();
}

/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route, $params = array(), $ampersand = '&')
{
    return Yii::app()->createUrl($route, $params, $ampersand);
}

/**
 * This is the shortcut to CHtml::encode
 */
function h($text)
{
    return htmlspecialchars($text, ENT_QUOTES, Yii::app()->charset);
}

function dbTrans()
{
    return Yii::app()->db->beginTransaction();
}

/**
 * This is the shortcut to CHtml::link()
 */
function l($text, $url = '#', $htmlOptions = array())
{
    return CHtml::link($text, $url, $htmlOptions);
}

/**
 * This is the shortcut to Yii::t() with default category = 'stay'
 */
function t($message, $category = 'stay', $params = array(), $source = null, $language = null)
{
    return Yii::t($category, $message, $params, $source, $language);
}

/**
 * This is the shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 */
function bu($url = null)
{
    static $baseUrl;
    if ($baseUrl === null) {
        $baseUrl = Yii::app()->getRequest()->getBaseUrl();
    }
    return $url === null ? $baseUrl : $baseUrl . '/' . ltrim($url, '/');
}

/**
 * Returns the named application parameter.
 * This is the shortcut to Yii::app()->params[$name].
 */
function param($name)
{
    return Yii::app()->params[$name];
}

function Encrypt($string)
{
    return Yii::app()->aes256->Encrypt($string);
}

function Decrypt($string)
{
    return Yii::app()->aes256->Decrypt($string);
}

function generatePassword($length = 8)
{
    $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    $count = mb_strlen($chars);
    for ($i = 0, $result = ''; $i < $length; $i++) {
        $index = rand(0, $count - 1);
        $result .= mb_substr($chars, $index, 1);
    }
    return $result;
}

function date2longperiode($date, $format)
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    $formater = new CDateFormatter('id_ID');
    return $formater->format($format, $timestamp);
}

function period2date($month, $year)
{
    $timestamp = DateTime::createFromFormat('d/m/Y', "01/$month/$year");
    $start = $timestamp->format('Y-m-d');
    $end = $timestamp->format('Y-m-t');
    return array('start' => $start, 'end' => $end);
}

function get_number($number)
{
    return str_replace(",", "", $number);
}

function sql2date($date, $format = 'dd/MM/yyyy')
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    return Yii::app()->dateFormatter->format($format, $timestamp);
}

function date2sql($date, $format = 'dd/MM/yyyy')
{
    $timestamp = CDateTimeParser::parse($date, $format);
    return Yii::app()->dateFormatter->format('yyyy-MM-dd', $timestamp);
}

function sql2long_date($date)
{
    $timestamp = CDateTimeParser::parse($date, 'yyyy-MM-dd');
    $formater = new CDateFormatter('id_ID');
    return $formater->formatDateTime($timestamp, 'long', false);
}

function get_date_tomorrow()
{
    return Yii::app()->dateFormatter->format('yyyy-MM-dd', time() + (1 * 24 * 60 * 60));
}

function get_time_now()
{
    return Yii::app()->dateFormatter->format('HH:mm:ss', time());
}

function get_date_today($format = 'yyyy-MM-dd')
{
    return Yii::app()->dateFormatter->format($format, time());
}

function Now($formatDate = 'yyyy-MM-dd')
{
    return get_date_today($formatDate) . ' ' . get_time_now();
}

function percent_format($value, $decimal = 0)
{
    return number_format($value * 100, $decimal) . '%';
}

function curr_format($value, $decimal = 0)
{
    return "Rp" . number_format($value * 100, $decimal);
}

function acc_format($value, $decimal = 0)
{
    $normalize = $value < 0 ? -$value : $value;
    $print = number_format($normalize, $decimal);
    return $value < 0 ? "($print)" : $print;
}

function mailsend($to, $from, $subject, $message, $attachdata)
{
    $mail = Yii::app()->Smtpmail;
    $mail->SetFrom($from, 'NWIS');
    $mail->Subject = $subject;
    $mail->Body = $message;
    $mail->ClearAddresses();
    $mail->ClearAttachments();
    $mail->AddAddress($to);
    $mail->AddStringAttachment($attachdata, "$subject.bz2");
    if (!$mail->Send()) {
        return $mail->ErrorInfo;
    } else {
        return 'OK';
    }
}

function mailsend_remainder($to, $from, $subject, $message)
{
    $mail = Yii::app()->Smtpmail;
    $mail->SetFrom($from, 'NWIS');
    $mail->Subject = $subject;
    $mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
    $mail->MsgHTML($message);
    $mail->ClearAddresses();
    $mail->ClearAttachments();
    $mail->AddAddress($to);
    if (!$mail->Send()) {
        return $mail->ErrorInfo;
    } else {
        return 'OK';
    }
}

function yiiparam($name, $default = null)
{
    if (isset(Yii::app()->params[$name])) {
        return Yii::app()->params[$name];
    } else {
        return $default;
    }
}

function is_connected($url, $port)
{
    $connected = @fsockopen($url, $port);
    if ($connected) {
        $is_conn = true; //action when connected
        fclose($connected);
    } else {
        $is_conn = false; //action in connection failure
    }
    return $is_conn;
}

function format_number_report($num, $digit = 0)
{
    if (!is_angka($num)) {
        return $num;
    }
    return (isset($_POST['format']) && $_POST['format'] == 'excel') ? $num : number_format($num, $digit);
}

function is_report_excel()
{
    return (isset($_POST['format']) && $_POST['format'] == 'excel');
}


function check_in_range($start_date, $end_date, $date_from_user)
{
    // Convert to timestamp
    $start_ts = strtotime($start_date);
    $end_ts = strtotime($end_date);
    $user_ts = strtotime($date_from_user);
    // Check that user date is between start & end
    return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
}

function round_up($number, $precision = 2)
{
    $fig = pow(10, $precision);
    return (ceil($number * $fig) / $fig);
}

function round_up_to_nearest_n($int, $n)
{
    return ceil($int / $n) * $n;
}

function consoleSync($command, $action, $cabang = null, $start = null, $end = null, $log)
{
    $log = Yii::app()->basePath . DS . 'runtime' . DS . $log;
    $yiic = dirname(__file__) . DIRECTORY_SEPARATOR . 'yiic';
    $cmd = PHP_EXE . ' ' . $yiic . ' ' . $command . ' ' . $action . ' ' . $cabang . ' ' . $start . ' ' . $end . ' >> ' . $log . ' 2>&1';
    if (substr(php_uname(), 0, 7) == "Windows") {
        pclose(popen('start /B cmd /C "' . $cmd . '"', "r"));
    } else {
        exec($cmd . ' &');
    }
}

function console($command, $action, $option = null, $log)
{
    $log = Yii::app()->basePath . DS . 'runtime' . DS . $log;
    $yiic = dirname(__file__) . DIRECTORY_SEPARATOR . 'yiic';
    $cmd = PHP_EXE . ' ' . $yiic . ' ' . $command . ' ' . $action . ' ' . $option . ' >> ' . $log . ' 2>&1';
    if (substr(php_uname(), 0, 7) == "Windows") {
        pclose(popen('start /B cmd /C "' . $cmd . '"', "r"));
    } else {
        exec($cmd . ' &');
    }
}

function console1($command, $action, $option = null, $log)
{
    $yiic = dirname(__file__) . DIRECTORY_SEPARATOR . 'yiic';
    $cmd = PHP_EXE . ' ' . $yiic . ' ' . $command . ' ' . $action . ' ' . $option . ' >> ' . $log . ' 2>&1';
    if (substr(php_uname(), 0, 7) == "Windows") {
        pclose(popen('start /B cmd /C "' . $cmd . '"', "r"));
    } else {
        exec($cmd . ' &');
    }
}

function barcode($filepath = "", $text = "0", $size = "20", $orientation = "horizontal", $code_type = "code128", $print = true,
                 $SizeFactor = 1)
{
    $code_string = "";
    // Translate the $text into barcode the correct $code_type
    if (in_array(strtolower($code_type), array("code128", "code128b"))) {
        $chksum = 104;
        // Must not change order of array elements as the checksum depends on the array's key to validate final code
        $code_array = array(" " => "212222", "!" => "222122", "\"" => "222221", "#" => "121223", "$" => "121322", "%" => "131222", "&" => "122213", "'" => "122312", "(" => "132212", ")" => "221213", "*" => "221312", "+" => "231212", "," => "112232", "-" => "122132", "." => "122231", "/" => "113222", "0" => "123122", "1" => "123221", "2" => "223211", "3" => "221132", "4" => "221231", "5" => "213212", "6" => "223112", "7" => "312131", "8" => "311222", "9" => "321122", ":" => "321221", ";" => "312212", "<" => "322112", "=" => "322211", ">" => "212123", "?" => "212321", "@" => "232121", "A" => "111323", "B" => "131123", "C" => "131321", "D" => "112313", "E" => "132113", "F" => "132311", "G" => "211313", "H" => "231113", "I" => "231311", "J" => "112133", "K" => "112331", "L" => "132131", "M" => "113123", "N" => "113321", "O" => "133121", "P" => "313121", "Q" => "211331", "R" => "231131", "S" => "213113", "T" => "213311", "U" => "213131", "V" => "311123", "W" => "311321", "X" => "331121", "Y" => "312113", "Z" => "312311", "[" => "332111", "\\" => "314111", "]" => "221411", "^" => "431111", "_" => "111224", "\`" => "111422", "a" => "121124", "b" => "121421", "c" => "141122", "d" => "141221", "e" => "112214", "f" => "112412", "g" => "122114", "h" => "122411", "i" => "142112", "j" => "142211", "k" => "241211", "l" => "221114", "m" => "413111", "n" => "241112", "o" => "134111", "p" => "111242", "q" => "121142", "r" => "121241", "s" => "114212", "t" => "124112", "u" => "124211", "v" => "411212", "w" => "421112", "x" => "421211", "y" => "212141", "z" => "214121", "{" => "412121", "|" => "111143", "}" => "111341", "~" => "131141", "DEL" => "114113", "FNC 3" => "114311", "FNC 2" => "411113", "SHIFT" => "411311", "CODE C" => "113141", "FNC 4" => "114131", "CODE A" => "311141", "FNC 1" => "411131", "Start A" => "211412", "Start B" => "211214", "Start C" => "211232", "Stop" => "2331112");
        $code_keys = array_keys($code_array);
        $code_values = array_flip($code_keys);
        for ($X = 1; $X <= strlen($text); $X++) {
            $activeKey = substr($text, ($X - 1), 1);
            $code_string .= $code_array[$activeKey];
            $chksum = ($chksum + ($code_values[$activeKey] * $X));
        }
        $code_string .= $code_array[$code_keys[($chksum - (intval($chksum / 103) * 103))]];
        $code_string = "211214" . $code_string . "2331112";
    } elseif (strtolower($code_type) == "code128a") {
        $chksum = 103;
        $text = strtoupper($text); // Code 128A doesn't support lower case
        // Must not change order of array elements as the checksum depends on the array's key to validate final code
        $code_array = array(" " => "212222", "!" => "222122", "\"" => "222221", "#" => "121223", "$" => "121322", "%" => "131222", "&" => "122213", "'" => "122312", "(" => "132212", ")" => "221213", "*" => "221312", "+" => "231212", "," => "112232", "-" => "122132", "." => "122231", "/" => "113222", "0" => "123122", "1" => "123221", "2" => "223211", "3" => "221132", "4" => "221231", "5" => "213212", "6" => "223112", "7" => "312131", "8" => "311222", "9" => "321122", ":" => "321221", ";" => "312212", "<" => "322112", "=" => "322211", ">" => "212123", "?" => "212321", "@" => "232121", "A" => "111323", "B" => "131123", "C" => "131321", "D" => "112313", "E" => "132113", "F" => "132311", "G" => "211313", "H" => "231113", "I" => "231311", "J" => "112133", "K" => "112331", "L" => "132131", "M" => "113123", "N" => "113321", "O" => "133121", "P" => "313121", "Q" => "211331", "R" => "231131", "S" => "213113", "T" => "213311", "U" => "213131", "V" => "311123", "W" => "311321", "X" => "331121", "Y" => "312113", "Z" => "312311", "[" => "332111", "\\" => "314111", "]" => "221411", "^" => "431111", "_" => "111224", "NUL" => "111422", "SOH" => "121124", "STX" => "121421", "ETX" => "141122", "EOT" => "141221", "ENQ" => "112214", "ACK" => "112412", "BEL" => "122114", "BS" => "122411", "HT" => "142112", "LF" => "142211", "VT" => "241211", "FF" => "221114", "CR" => "413111", "SO" => "241112", "SI" => "134111", "DLE" => "111242", "DC1" => "121142", "DC2" => "121241", "DC3" => "114212", "DC4" => "124112", "NAK" => "124211", "SYN" => "411212", "ETB" => "421112", "CAN" => "421211", "EM" => "212141", "SUB" => "214121", "ESC" => "412121", "FS" => "111143", "GS" => "111341", "RS" => "131141", "US" => "114113", "FNC 3" => "114311", "FNC 2" => "411113", "SHIFT" => "411311", "CODE C" => "113141", "CODE B" => "114131", "FNC 4" => "311141", "FNC 1" => "411131", "Start A" => "211412", "Start B" => "211214", "Start C" => "211232", "Stop" => "2331112");
        $code_keys = array_keys($code_array);
        $code_values = array_flip($code_keys);
        for ($X = 1; $X <= strlen($text); $X++) {
            $activeKey = substr($text, ($X - 1), 1);
            $code_string .= $code_array[$activeKey];
            $chksum = ($chksum + ($code_values[$activeKey] * $X));
        }
        $code_string .= $code_array[$code_keys[($chksum - (intval($chksum / 103) * 103))]];
        $code_string = "211412" . $code_string . "2331112";
    } elseif (strtolower($code_type) == "code39") {
        $code_array = array("0" => "111221211", "1" => "211211112", "2" => "112211112", "3" => "212211111", "4" => "111221112", "5" => "211221111", "6" => "112221111", "7" => "111211212", "8" => "211211211", "9" => "112211211", "A" => "211112112", "B" => "112112112", "C" => "212112111", "D" => "111122112", "E" => "211122111", "F" => "112122111", "G" => "111112212", "H" => "211112211", "I" => "112112211", "J" => "111122211", "K" => "211111122", "L" => "112111122", "M" => "212111121", "N" => "111121122", "O" => "211121121", "P" => "112121121", "Q" => "111111222", "R" => "211111221", "S" => "112111221", "T" => "111121221", "U" => "221111112", "V" => "122111112", "W" => "222111111", "X" => "121121112", "Y" => "221121111", "Z" => "122121111", "-" => "121111212", "." => "221111211", " " => "122111211", "$" => "121212111", "/" => "121211121", "+" => "121112121", "%" => "111212121", "*" => "121121211");
        // Convert to uppercase
        $upper_text = strtoupper($text);
        for ($X = 1; $X <= strlen($upper_text); $X++) {
            $code_string .= $code_array[substr($upper_text, ($X - 1), 1)] . "1";
        }
        $code_string = "1211212111" . $code_string . "121121211";
    } elseif (strtolower($code_type) == "code25") {
        $code_array1 = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
        $code_array2 = array("3-1-1-1-3", "1-3-1-1-3", "3-3-1-1-1", "1-1-3-1-3", "3-1-3-1-1", "1-3-3-1-1", "1-1-1-3-3", "3-1-1-3-1", "1-3-1-3-1", "1-1-3-3-1");
        for ($X = 1; $X <= strlen($text); $X++) {
            for ($Y = 0; $Y < count($code_array1); $Y++) {
                if (substr($text, ($X - 1), 1) == $code_array1[$Y])
                    $temp[$X] = $code_array2[$Y];
            }
        }
        for ($X = 1; $X <= strlen($text); $X += 2) {
            if (isset($temp[$X]) && isset($temp[($X + 1)])) {
                $temp1 = explode("-", $temp[$X]);
                $temp2 = explode("-", $temp[($X + 1)]);
                for ($Y = 0; $Y < count($temp1); $Y++)
                    $code_string .= $temp1[$Y] . $temp2[$Y];
            }
        }
        $code_string = "1111" . $code_string . "311";
    } elseif (strtolower($code_type) == "codabar") {
        $code_array1 = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "-", "$", ":", "/", ".", "+", "A", "B", "C", "D");
        $code_array2 = array("1111221", "1112112", "2211111", "1121121", "2111121", "1211112", "1211211", "1221111", "2112111", "1111122", "1112211", "1122111", "2111212", "2121112", "2121211", "1121212", "1122121", "1212112", "1112122", "1112221");
        // Convert to uppercase
        $upper_text = strtoupper($text);
        for ($X = 1; $X <= strlen($upper_text); $X++) {
            for ($Y = 0; $Y < count($code_array1); $Y++) {
                if (substr($upper_text, ($X - 1), 1) == $code_array1[$Y])
                    $code_string .= $code_array2[$Y] . "1";
            }
        }
        $code_string = "11221211" . $code_string . "1122121";
    }
    // Pad the edges of the barcode
    $code_length = 20;
    if ($print) {
        $text_height = 30;
    } else {
        $text_height = 0;
    }
    for ($i = 1; $i <= strlen($code_string); $i++) {
        $code_length = $code_length + (integer)(substr($code_string, ($i - 1), 1));
    }
    if (strtolower($orientation) == "horizontal") {
        $img_width = $code_length * $SizeFactor;
        $img_height = $size;
    } else {
        $img_width = $size;
        $img_height = $code_length * $SizeFactor;
    }
    $image = imagecreate($img_width, $img_height + $text_height);
    $black = imagecolorallocate($image, 0, 0, 0);
    $white = imagecolorallocate($image, 255, 255, 255);
    imagefill($image, 0, 0, $white);
    if ($print) {
        imagestring($image, 5, 31, $img_height, $text, $black);
    }
    $location = 10;
    for ($position = 1; $position <= strlen($code_string); $position++) {
        $cur_size = $location + (substr($code_string, ($position - 1), 1));
        if (strtolower($orientation) == "horizontal")
            imagefilledrectangle($image, $location * $SizeFactor, 0, $cur_size * $SizeFactor, $img_height, ($position % 2 == 0 ? $white : $black));
        else
            imagefilledrectangle($image, 0, $location * $SizeFactor, $img_width, $cur_size * $SizeFactor, ($position % 2 == 0 ? $white : $black));
        $location = $cur_size;
    }
    // Draw barcode to the screen or save in a file
    if ($filepath == "") {
        header('Content-type: image/png');
        imagepng($image);
        imagedestroy($image);
    } else {
        imagepng($image, $filepath);
        imagedestroy($image);
    }
}